package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;

import com.ipanda.lanexangtourism.Adapter.ImagePlacesRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.SetThisPhotoList;
import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.Model.TouristAttractionsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class AddPhotoActivity extends AppCompatActivity implements View.OnClickListener , SetThisPhotoList {

    //variables
    private static String TAG = AddPhotoActivity.class.getSimpleName();

    private static final int REQUEST_GALLERY_PHOTO = 101;

    private static String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private ChangeLanguageLocale languageLocale;

    private ImageButton btnDismiss;

    private CardView btnAddImage, btnAddPhotoToPlaces;

    private RecyclerView mRecyclerView;

    private ImagePlacesRecyclerViewAdapter mAdapter;

    private ArrayList<PhotoTourist> photoTouristArrayList;

    private DatesTripModel datesTrip;

    private TouristAttractionsModel tourist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);
        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();


        //views
        btnDismiss = findViewById(R.id.imgBtn_dismiss);
        btnAddImage = findViewById(R.id.btn_add_image_places);
        btnAddPhotoToPlaces = findViewById(R.id.btn_add_photo_places);
        mRecyclerView = findViewById(R.id.recycler_view_list_image_places);

        //set on click
        btnDismiss.setOnClickListener(this);
        btnAddImage.setOnClickListener(this);
        btnAddPhotoToPlaces.setOnClickListener(this);
        tourist = new TouristAttractionsModel();
        datesTrip = new DatesTripModel();
        if (getIntent() != null){
            datesTrip = getIntent().getParcelableExtra("DATES_TRIP");
            tourist = getIntent().getParcelableExtra("TOURIST");
        }

        checkPermission();

        photoTouristArrayList = new ArrayList<>();

    }

    public boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(this, mPermission);
            if (result == PackageManager.PERMISSION_DENIED) return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBtn_dismiss:
                onBackPressed();
                break;
            case R.id.btn_add_image_places:
                onClickAddImage();
                break;
            case R.id.btn_add_photo_places:
                onClickAddPhotoPlaces();
                break;
        }
    }

    private void onClickAddPhotoPlaces() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    private void onClickAddImage() {
        Intent intent = new Intent();
        if (mAdapter != null) {
            if (mAdapter.returnArrayList() != null && mAdapter.returnArrayList().size() != 0) {
                intent.putExtra("PHOTO_TOURIST", mAdapter.returnArrayList());
                intent.putExtra("DATES_TRIP", datesTrip);
                intent.putExtra("TOURIST", tourist);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
           if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                String mPhotoPath = getRealPathFromUri(selectedImage);

                int count = photoTouristArrayList.size();
                PhotoTourist photoTourist = new PhotoTourist();
                photoTourist.setPhotoTouristId(count);
                photoTourist.setPhotoTouristPaths(mPhotoPath);
                photoTourist.setPaths(true);
                photoTourist.setPhotoState("1");
                photoTouristArrayList.add(photoTourist);

                mAdapter = new ImagePlacesRecyclerViewAdapter(this,photoTouristArrayList, languageLocale.getLanguage(), this::setThisPhotoList);
                mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
                mRecyclerView.setAdapter(mAdapter);
            }
        }
    }

    @Override
    public void setThisPhotoList(ArrayList<PhotoTourist> photoArrayList) {
        this.photoTouristArrayList = photoArrayList;


    }

    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
