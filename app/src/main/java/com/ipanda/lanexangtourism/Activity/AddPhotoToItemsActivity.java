package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Adapter.ImageAddPhotoToItemsAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class AddPhotoToItemsActivity extends AppCompatActivity implements View.OnClickListener {


    //variables
    private static final int REQUEST_GALLERY_PHOTO = 101;

    private static String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private ChangeLanguageLocale languageLocale;

    private ImageButton btnDismiss;

    private CardView btnAddImage, btnAddPhotoToPlaces;

    private RecyclerView mRecyclerView;

    private ImageAddPhotoToItemsAdapter mAdapter;

    private ArrayList<ItemsPhotoDetailModel> itemsPhotoArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo_to_items);
        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        btnDismiss = findViewById(R.id.imgBtn_dismiss);
        btnAddImage = findViewById(R.id.btn_add_image_places);
        btnAddPhotoToPlaces = findViewById(R.id.btn_add_photo_places);
        mRecyclerView = findViewById(R.id.recycler_view_list_image_places);

        //set on click
        btnDismiss.setOnClickListener(this);
        btnAddImage.setOnClickListener(this);
        btnAddPhotoToPlaces.setOnClickListener(this);


        if (getIntent() != null){

        }

        checkPermission();

        itemsPhotoArrayList = new ArrayList<>();


    }

    private boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(this, mPermission);
            if (result == PackageManager.PERMISSION_DENIED) return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBtn_dismiss:
                onBackPressed();
                break;
            case R.id.btn_add_image_places:
                onClickAddImage();
                break;
            case R.id.btn_add_photo_places:
                onClickAddPhotoPlaces();
                break;
        }
    }

    private void onClickAddPhotoPlaces() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    private void onClickAddImage() {
        Intent intent = new Intent();
        if (mAdapter.returnArrayList() != null && mAdapter.returnArrayList().size() != 0) {
            intent.putExtra("PHOTO_ITEMS", mAdapter.returnArrayList());
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                String mPhotoPath = getRealPathFromUri(selectedImage);

                int count = itemsPhotoArrayList.size();
                if (count > 2){
                    Toast.makeText(this, "3 maximum item", Toast.LENGTH_SHORT).show();
                }else {
                    ItemsPhotoDetailModel photo = new ItemsPhotoDetailModel();
                    photo.setPhotoId(count);
                    photo.setPhotoPaths(mPhotoPath);
                    itemsPhotoArrayList.add(photo);
                    mAdapter = new ImageAddPhotoToItemsAdapter(this,itemsPhotoArrayList, languageLocale.getLanguage());
                    mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
                    mRecyclerView.setAdapter(mAdapter);
                }

            }
        }
    }

    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}