package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.ipanda.lanexangtourism.Adapter.SearchPlacesRecyclerAdapter;
import com.ipanda.lanexangtourism.Adapter.SearchPlacesRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.SpinnerCategoryAdapter;
import com.ipanda.lanexangtourism.Adapter.SpinnerProvinceAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.SelectedPlacesClickListener;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.CategoryAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ProvinceAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.CategoryCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProvinceCallBack;
import com.ipanda.lanexangtourism.utils.PaginationListener;

import java.util.ArrayList;

import static com.ipanda.lanexangtourism.utils.PaginationListener.PAGE_START;

public class AddPlacesActivity extends AppCompatActivity implements View.OnClickListener, SelectedPlacesClickListener , ItemsCallBack, ProvinceCallBack, CategoryCallBack
, SwipeRefreshLayout.OnRefreshListener{

    //variables
    private ImageButton btnDismiss;

    private SearchView edtSearchForPlaces;

    private Spinner spinnerProvince, spinnerTypePlaces;

    private ConstraintLayout showSearchSelectedPlaces;

    private CardView showBtnAddNewPlaces, showBtnAddPlaces;

    private RecyclerView mRecyclerPlaces;

    private SearchPlacesRecyclerViewAdapter mSearchPlacesAdapter;

    private SpinnerCategoryAdapter spinnerCategoryAdapter;

    private String urlGetItemsAll = null, urlGetProvince = null, urlGetCategory = null;

    private ArrayList<ItemsModel> searchItemsList;

    private ArrayList<ProvincesModel> searchProvincesList;

    private SpinnerProvinceAdapter spinnerProvinceAdapter;

    private ItemsModel resultItems;

    private DatesTripModel datesTrip;

    private CircularSticksLoader loader_items_id;

    private ImageView img_load_field_id;

    private TextView txt_load_field_id;

    private ChangeLanguageLocale languageLocale;

    private SwipeRefreshLayout swipeRefresh;
    private SearchPlacesRecyclerAdapter adapter;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private int totalPage = 10;
    private boolean isLoading = false;
    int itemCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_places);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        onCreateView();
        datesTrip = new DatesTripModel();
        if (getIntent() != null){
            datesTrip = getIntent().getParcelableExtra("DATES_TRIP");
        }
    }

    public void onCreateView() {
        //views
        btnDismiss = findViewById(R.id.imgBtn_dismiss);
        edtSearchForPlaces = findViewById(R.id.edt_search_for_places);
        mRecyclerPlaces = findViewById(R.id.recyclerView_search_places);
        spinnerProvince = findViewById(R.id.spinner_province);
        spinnerTypePlaces = findViewById(R.id.spinner_type_places);
        showSearchSelectedPlaces = findViewById(R.id.showSearch_selected_places);
        showBtnAddNewPlaces = findViewById(R.id.showBtn_add_new_places);
        showBtnAddPlaces = findViewById(R.id.showBtn_add_places);

        //view refresh
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        mRecyclerPlaces.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerPlaces.setLayoutManager(layoutManager);

        adapter = new SearchPlacesRecyclerAdapter(this, new ArrayList<>(), languageLocale.getLanguage(), this);
        mRecyclerPlaces.setAdapter(adapter);
//        doApiCall();
//        mRecyclerPlaces.addOnScrollListener(new PaginationListener(layoutManager) {
//            @Override
//            protected void loadMoreItems() {
//                isLoading = true;
//                currentPage++;
//                doApiCall();
//            }
//
//            @Override
//            public boolean isLastPage() {
//                return isLastPage;
//            }
//
//            @Override
//            public boolean isLoading() {
//                return isLoading;
//            }
//        });

        //view verify
        loader_items_id = findViewById(R.id.loader_items_id);
        img_load_field_id = findViewById(R.id.img_load_field_id);
        txt_load_field_id = findViewById(R.id.txt_load_field_id);

        //set on click
        btnDismiss.setOnClickListener(this);
        showBtnAddNewPlaces.setOnClickListener(this);
        showBtnAddPlaces.setOnClickListener(this);

        //search view
        edtSearchForPlaces.setQuery("",false);
        edtSearchForPlaces.setQueryHint(getString(R.string.text_search_places));
        edtSearchForPlaces.setIconified(false);
        edtSearchForPlaces.setIconifiedByDefault(false);
        edtSearchForPlaces.setSubmitButtonEnabled(true);
        edtSearchForPlaces.setBackgroundColor(Color.TRANSPARENT);
        edtSearchForPlaces.clearFocus();

        String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        if (userId != null) {
            urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=" + userId;
        }else {
            urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=";
        }
        urlGetProvince = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/ProvinceGroup";
        urlGetCategory = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ProgramTour/getNameItemCategory";

        new ItemsAsyncTask(this).execute(urlGetItemsAll);
        new ProvinceAsyncTask(this).execute(urlGetProvince);
        new CategoryAsyncTask(this).execute(urlGetCategory);


    }

    private void setDataAdapterPlaces(ArrayList<ItemsModel> arrayList) {
        mSearchPlacesAdapter = new SearchPlacesRecyclerViewAdapter(this, arrayList, languageLocale.getLanguage(),this);
        mRecyclerPlaces.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerPlaces.setAdapter(mSearchPlacesAdapter);

        edtSearchForPlaces.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edtSearchForPlaces.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mSearchPlacesAdapter != null) {
                    mSearchPlacesAdapter.getFilter().filter(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mSearchPlacesAdapter != null) {
                    mSearchPlacesAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBtn_dismiss:
                onBackPressed();
                break;
            case R.id.showBtn_add_new_places:
                onClickNewPlaces();
                break;
            case R.id.showBtn_add_places:
                onClickAddPlaces();
                break;
        }
    }

    private void onClickNewPlaces() {
         startActivity(new Intent(this, NewPlacesActivity.class));
    }

    private void onClickAddPlaces() {
        if (resultItems != null) {
            Intent intent = new Intent();
            intent.putExtra("resultItems", resultItems);
            intent.putExtra("resultDatesTrip", datesTrip);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void itemClickedPlaces(ItemsModel itemsModel, ImageButton imageButton) {
        this.resultItems = itemsModel;
        ArrayList<ItemsModel> newItemsList = new ArrayList<>();
        if (itemsModel.isSelectedItemToPlaces()){
            showSearchSelectedPlaces.setVisibility(View.VISIBLE);
            showBtnAddPlaces.setVisibility(View.GONE);
            showBtnAddNewPlaces.setVisibility(View.VISIBLE);
            itemsModel.setSelectedItemToPlaces(false);
            imageButton.setImageResource(R.drawable.ic_add_light);
            setDataAdapterPlaces(searchItemsList);
        }else {
            showBtnAddPlaces.setVisibility(View.VISIBLE);
            showSearchSelectedPlaces.setVisibility(View.GONE);
            showBtnAddNewPlaces.setVisibility(View.VISIBLE);
            itemsModel.setSelectedItemToPlaces(true);
            imageButton.setImageResource(R.drawable.ic_delete_light);
            newItemsList.add(itemsModel);
            setDataAdapterPlaces(newItemsList);
        }



    }


    @Override
    public void onPreCallService() {
        showSearchSelectedPlaces.setVisibility(View.GONE);
        mRecyclerPlaces.setVisibility(View.GONE);
        loader_items_id.setVisibility(View.VISIBLE);
        img_load_field_id.setVisibility(View.GONE);
        txt_load_field_id.setVisibility(View.GONE);
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerCategory(ArrayList<CategoryModel> categoryArrayList) {
        if (categoryArrayList != null && categoryArrayList.size() != 0){
            Log.e("check data", categoryArrayList+"");
            categoryArrayList.add(0,new CategoryModel("ทั้งหมด","All","ທັງ ໝົດ","所有"));
            spinnerCategoryAdapter = new SpinnerCategoryAdapter(this, categoryArrayList, languageLocale.getLanguage());
            spinnerTypePlaces.setAdapter(spinnerCategoryAdapter);

        }else {

        }
    }

    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0) {
            Log.e("check data", itemsModelArrayList + "");
            this.searchItemsList = itemsModelArrayList;
            //set data adapter places
            setDataAdapterPlaces(searchItemsList);
            swipeRefresh.setRefreshing(false);
            mRecyclerPlaces.setVisibility(View.VISIBLE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.GONE);
            txt_load_field_id.setVisibility(View.GONE);
        }else {
            mRecyclerPlaces.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.VISIBLE);
            txt_load_field_id.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onRequestCompleteListenerProvinces(ArrayList<ProvincesModel> provincesList) {
        if (provincesList != null && provincesList.size() != 0) {
            Log.e("check data", provincesList + "");
            this.searchProvincesList = provincesList;
            provincesList.add(0,new ProvincesModel("ทั้งหมด","All","ທັງ ໝົດ","所有"));
            spinnerProvinceAdapter = new SpinnerProvinceAdapter(this, provincesList, languageLocale.getLanguage());
            spinnerProvince.setAdapter(spinnerProvinceAdapter);

            showSearchSelectedPlaces.setVisibility(View.VISIBLE);

        }else {

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }


    @Override
    public void onRefresh() {
        itemCount = 0;
        currentPage = PAGE_START;
        isLastPage = false;
        adapter.clear();
        doApiCall();
    }

    private void doApiCall() {
        final ArrayList<ItemsModel> items = new ArrayList<>();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    itemCount++;
                    ItemsModel item = new ItemsModel();
                    item.setTopicTH("topic" + itemCount);
                    items.add(item);
                }
                // do this all stuff on Success of APIs response
                /**
                 * manage progress view
                 */
                if (currentPage != PAGE_START) adapter.removeLoading();
                adapter.addItems(items);
                swipeRefresh.setRefreshing(false);

                // check weather is last page or not
                if (currentPage < totalPage) {
                    adapter.addLoading();
                } else {
                    isLastPage = true;
                }
                isLoading = false;
            }
        }, 1500);
    }

}
