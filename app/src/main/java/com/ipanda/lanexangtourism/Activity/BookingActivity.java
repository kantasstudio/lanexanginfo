package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.ipanda.lanexangtourism.Adapter.BookingItemsAdapter;
import com.ipanda.lanexangtourism.Adapter.FilterItemsMyBookingAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.BookingItemsClickListener;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.MenuBookingFragment.BookingBookingFragment;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.BookingItemsAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.BookingItemsCallBack;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class BookingActivity extends AppCompatActivity implements BookingItemsCallBack, BookingItemsClickListener, ExchangeRateCallBack, FilterSelectedClickListener {

    //Variables
    private static final String TAG = BookingActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private String userId;

    private CardView btnSortByGroup;

    private RecyclerView mRecyclerBookingItems;

    private BookingItemsAdapter mAdapter;

    private RateModel rateModel;

    private FilterItemsMyBookingAdapter mAdapterMyBooking;

    private ArrayList<SubCategoryModel> itemsMyBookingArrayList;

    private boolean isSortByGroup = false;

    private ArrayList<BookingModel> bookingList;

    private CircularSticksLoader loader_items_id;

    private ImageView img_load_field_id;

    private TextView txt_load_field_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //views
        mToolbar = findViewById(R.id.Settings_Toolbar);
        mRecyclerBookingItems = findViewById(R.id.recycler_booking_items);
        btnSortByGroup = findViewById(R.id.txt_sort_by_group);
        loader_items_id = findViewById(R.id.loader_items_id);
        img_load_field_id = findViewById(R.id.img_load_field_id);
        txt_load_field_id = findViewById(R.id.txt_load_field_id);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (userId != null) {
            String urlRate = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ExchangeRate";
            new ExchangeRateAsyncTask(this).execute(urlRate);

            String url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Booking?user_id=" + userId;
            new BookingItemsAsyncTask(this).execute(url);
        }

        itemsMyBookingArrayList = new ArrayList<>();
        itemsMyBookingArrayList.add(new SubCategoryModel(9,getString(R.string.text_tour_package),false));
        itemsMyBookingArrayList.add(new SubCategoryModel(10,getString(R.string.text_event_ticket),false));
        itemsMyBookingArrayList.add(new SubCategoryModel(11,getString(R.string.text_car_rental),false));
        itemsMyBookingArrayList.add(new SubCategoryModel(5,getString(R.string.txt_hotel),false));

        //setup on Click
        btnSortByGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupPopupFilter();
                if (isSortByGroup){
                    isSortByGroup = false;
                    setViewVisibility(R.id.scroll_view_id, View.GONE);
                }else {
                    isSortByGroup = true;
                    setViewVisibility(R.id.scroll_view_id, View.VISIBLE);
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void clickedPackage(BookingModel booking) {
        Intent intent = new Intent(this, DetailsPackageActivity.class);
        intent.putExtra("BOOKING", booking);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void clickedTicket(BookingModel booking) {
        Intent intent = new Intent(this, DetailsTicketActivity.class);
        intent.putExtra("BOOKING", booking);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void clickedCar(BookingModel booking) {
        Intent intent = new Intent(this, DetailsCarActivity.class);
        intent.putExtra("BOOKING", booking);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void clickedHotel(BookingModel booking) {
        Intent intent = new Intent(this, DetailsHotelActivity.class);
        intent.putExtra("BOOKING", booking);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void clickedSharePackage(ItemsModel items) {
        shareItems(items);
    }

    @Override
    public void clickedShareTicket(ItemsModel items) {
        shareItems(items);
    }

    private void shareItems(ItemsModel items) {
        String ShareSub = "";
        switch (languageLocale.getLanguage()){
            case "th":
                ShareSub = " "+items.getTopicTH()+"\n";
                break;
            case "en":
                ShareSub = " "+items.getTopicEN()+"\n";
                break;
            case "lo":
                ShareSub = " "+items.getTopicLO()+"\n";
                break;
            case "zh":
                ShareSub = " "+items.getTopicZH()+"\n";
                break;
        }
        String uri = "http://maps.google.com/maps?saddr=" +items.getLatitude()+","+items.getLongitude()+"&iwloc=A";
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, ShareSub);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, uri );
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    @Override
    public void getFilterSelectedTourist(ArrayList<SubCategoryModel> subCategoryArrayList) {
        ArrayList<BookingModel> newBookingList = new ArrayList<>();
        for (SubCategoryModel sub : subCategoryArrayList) {
            if (sub.isFilter()){
                for (BookingModel book : bookingList){
                    if (book.getItemsModel().getMenuItemModel().getMenuItemId() == sub.getCategoryId()){
                        newBookingList.add(book);
                    }
                }
            }
        }
        setupBooking(newBookingList);
    }

    @Override
    public void onPreCallService() {
        mRecyclerBookingItems.setVisibility(View.GONE);
        loader_items_id.setVisibility(View.VISIBLE);
        img_load_field_id.setVisibility(View.GONE);
        txt_load_field_id.setVisibility(View.GONE);
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }

    @Override
    public void onRequestCompleteListener(ArrayList<BookingModel> bookingArrayList) {
        if (bookingArrayList != null && bookingArrayList.size() != 0){
            Log.e("check data",bookingArrayList+"");
            this.bookingList = bookingArrayList;
            setupBooking(bookingList);

            mRecyclerBookingItems.setVisibility(View.VISIBLE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.GONE);
            txt_load_field_id.setVisibility(View.GONE);
            btnSortByGroup.setVisibility(View.VISIBLE);
        }else {
            btnSortByGroup.setVisibility(View.GONE);
            mRecyclerBookingItems.setVisibility(View.GONE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.VISIBLE);
            txt_load_field_id.setVisibility(View.VISIBLE);
        }
    }

    private void setupBooking(ArrayList<BookingModel> list) {
        if (list != null && list.size() != 0){
            mAdapter = new BookingItemsAdapter(this, list, languageLocale.getLanguage(), this, rateModel);
            mRecyclerBookingItems.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            mRecyclerBookingItems.setAdapter(mAdapter);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private void setViewVisibility(int id, int visibility) {
        View view = findViewById(id);
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    private void setupPopupFilter() {
        //views
        ConstraintLayout consMyBooking = findViewById(R.id.cons_my_booking);
        ExpandableLayout mShowContentMyBooking = findViewById(R.id.expandable_my_booking);
        ImageView mImgUpDownMyBooking = findViewById(R.id.img_arrow_my_booking);
        RecyclerView mRecyclerViewMyBooking = findViewById(R.id.recycler_filter_my_booking);

        consMyBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mShowContentMyBooking.toggle();
                if (mShowContentMyBooking.isExpanded()){
                    mImgUpDownMyBooking.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    mImgUpDownMyBooking.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        mAdapterMyBooking = new FilterItemsMyBookingAdapter(this,itemsMyBookingArrayList, languageLocale.getLanguage(), this);
        mRecyclerViewMyBooking.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewMyBooking.setAdapter(mAdapterMyBooking);

    }
}