package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.ItemsCarRentalRecyclerViewAdapter;
import com.ipanda.lanexangtourism.DialogFragment.SearchCarRentalDialogFragment;
import com.ipanda.lanexangtourism.FilterActivity.CarRentalFilterActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ItemsCarRentClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.SeatsModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsCarRentalAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsCarRentCallBack;
import com.ipanda.lanexangtourism.items_view.ViewCarRentalActivity;
import com.ipanda.lanexangtourism.post_search.CarRentalPostRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BookingCarRentalActivity extends AppCompatActivity implements View.OnClickListener , ItemsCarRentCallBack, ItemsCarRentClickListener , ExchangeRateCallBack {

    //variables
    private static final int REQUEST_CODE = 1001;

    private ChangeLanguageLocale languageLocale;

    private CardView btnSearchBooking, cardView_filter_car;

    private TextView txtTitleSearchBooking;

    private Toolbar mToolbar;

    private RecyclerView mRecycler;

    private ItemsCarRentalRecyclerViewAdapter mAdapter;

    private String userId;

    private RateModel rateModel;

    private ArrayList<SubCategoryModel> subCategoryArrayList;

    private ArrayList<SeatsModel> seatsArrayList;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    private ArrayList<ItemsModel> newItemsOffline = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_car_rental);
        isModeOnline = getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.menu_toolbar);
        btnSearchBooking = findViewById(R.id.btn_search_booking);
        txtTitleSearchBooking = findViewById(R.id.txt_title_search_booking);
        cardView_filter_car = findViewById(R.id.cardView_filter_car);
        mRecycler = findViewById(R.id.recycler_near_car);


        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }


        //set text
        txtTitleSearchBooking.setText(getString(R.string.text_search_for_car_rental));

        //set on click event
        btnSearchBooking.setOnClickListener(this);
        cardView_filter_car.setOnClickListener(this);


        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        String url ="";
        if (isModeOnline) {
            if (userId != null) {
                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Reservations/CarRent?user_id=" + userId;
            } else {
                url = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Reservations/CarRent";
            }

            String urlRate = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ExchangeRate";
            new ExchangeRateAsyncTask(this).execute(urlRate);

            new ItemsCarRentalAsyncTask(this).execute(url);
        }else {
            getVersion = getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
            if (getVersion != 0) {
                mDatabase = new DatabaseHelper(this, getVersion);
                mManager = new DatabaseManager(this, mDatabase);
                setItemsOfflineMode();
            }else {
                alertDialog();
            }
        }

    }

    private void setItemsOfflineMode() {
        newItemsOffline = mManager.getListCarRental();
        if (newItemsOffline != null && newItemsOffline.size() != 0){
            mAdapter = new ItemsCarRentalRecyclerViewAdapter(this, newItemsOffline, languageLocale.getLanguage(),this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            mRecycler.setLayoutManager(layoutManager);
            mRecycler.setAdapter(mAdapter);
            setViewVisibility(R.id.recycler_near_car, View.VISIBLE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_search_booking:
               /* DialogFragment dialogFragment = SearchCarRentalDialogFragment.newInstance();
                dialogFragment.show(getSupportFragmentManager(),"BookingCarRentalActivity");*/
                Intent intent = new Intent(this, SearchCarRentalActivity.class);
                intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
                if (isModeOnline) {
                    startActivity(intent);
                }
                break;
            case R.id.con_selected_items:
                startActivity(new Intent(this, ViewCarRentalActivity.class));
                break;
            case R.id.cardView_filter_car:
                Intent intentCar = new Intent(this, CarRentalFilterActivity.class);
                intentCar.putExtra("EXCHANGE_RATE_MODEL",rateModel);
                if (isModeOnline) {
                    startActivityForResult(intentCar, REQUEST_CODE);
                }else {

                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {
        setViewVisibility(R.id.recycler_near_car, View.GONE);
        setViewVisibility(R.id.loader_items_id, View.VISIBLE);
        setViewVisibility(R.id.img_load_field_id, View.GONE);
        setViewVisibility(R.id.txt_load_field_id, View.GONE);
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;

        }
    }

    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            Log.e("check data", itemsModelArrayList+"");

            mAdapter = new ItemsCarRentalRecyclerViewAdapter(this, itemsModelArrayList, languageLocale.getLanguage(),this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            mRecycler.setLayoutManager(layoutManager);
            mRecycler.setAdapter(mAdapter);

            setViewVisibility(R.id.recycler_near_car, View.VISIBLE);
            setViewVisibility(R.id.loader_items_id, View.GONE);
            setViewVisibility(R.id.img_load_field_id, View.GONE);
            setViewVisibility(R.id.txt_load_field_id, View.GONE);
        }else {
            setViewVisibility(R.id.recycler_near_car, View.GONE);
            setViewVisibility(R.id.loader_items_id, View.GONE);
            setViewVisibility(R.id.img_load_field_id, View.VISIBLE);
            setViewVisibility(R.id.txt_load_field_id, View.VISIBLE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClickedItems(ItemsModel items) {
        if (isModeOnline) {
            Intent intent = new Intent(this, ChooseDateCarRentalActivity.class);
            intent.putExtra("ITEMS_MODEL", items);
            intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
            startActivity(intent);
        }else {
            Intent intentOffline = new Intent(this, ViewCarRentalActivity.class);
            intentOffline.putExtra("ITEMS_MODEL", items);
            intentOffline.putExtra("EXCHANGE_RATE_MODEL", rateModel);
            startActivity(intentOffline);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_CODE:
                if (data != null) {
                    mAdapter.notifyDataSetChanged();
                    String getMaxMin = data.getStringExtra("getMaxMin");
                    subCategoryArrayList = data.getParcelableArrayListExtra("sub_category_list");
                    seatsArrayList = data.getParcelableArrayListExtra("seats_list");
                    methodPost(getMaxMin);
                }
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void methodPost(String getMaxMin) {
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Fillter/");
        HashMap<String, String> paramsPost = new HashMap<>();
        paramsPost.put("user_id","");
        paramsPost.put("category_id", "11");
        paramsPost.put("subcategory_id", getSubCategory());
        paramsPost.put("str_search", "");
        paramsPost.put("price_range", getMaxMin);
        paramsPost.put("number_of_seats", getSeats());
        httpCallPost.setParams(paramsPost);
        new CarRentalPostRequest(this).execute(httpCallPost);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getSubCategory(){
        List<String> subId = new ArrayList<>();
        if (subCategoryArrayList != null && subCategoryArrayList.size() != 0) {
            for (SubCategoryModel sub : subCategoryArrayList) {
                if (sub.isFilter()) {
                    subId.add(sub.getCategoryId() + "");
                }
            }
        }
        return String.join(",", subId);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getSeats(){
        List<String> seatsId = new ArrayList<>();
        if (seatsArrayList != null && seatsArrayList.size() != 0) {
            for (SeatsModel sub : seatsArrayList) {
                if (sub.isFilter()) {
                    seatsId.add(sub.getCarSeats() + "");
                }
            }
        }
        return String.join(",", seatsId);
    }

    private void setViewVisibility(int id, int visibility) {
        View view = findViewById(id);
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
