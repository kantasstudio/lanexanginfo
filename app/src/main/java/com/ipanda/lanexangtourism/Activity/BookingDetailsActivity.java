package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Adapter.DetailsTravelPeriodRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.BookingDetailsClickListener;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.PaymentTransactionModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BookingDetailsActivity extends AppCompatActivity implements View.OnClickListener , BookingDetailsClickListener {

    //Variables
    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private TextView txt_title_package_tour, txt_company, txt_scan_qr_code;

    private EditText edt_input_fistName, edt_input_lastName, edt_input_email, edt_input_phone, edt_input_weChat, edt_input_whatsApp, edt_input_discount;

    private String fistName, lastName, email, phone, weChat, whatsApp, period;

    private Button btn_confirm_booking;

    private ItemsModel items = new ItemsModel();

    private RecyclerView mRecycler;

    private DetailsTravelPeriodRecyclerViewAdapter mAdapterTravelPeriod;

    private TextView price_rest_up_to, price_single, price_add_bed, price_bed;

    private AppCompatEditText edt_rest_up_to, edt_single, edt_add_bed, edt_bed;

    private ImageButton imageButtonUpAdultSpecial, imageButtonDownAdultSpecial, imageButtonUpAdult, imageButtonDownAdult;

    private ImageButton imageButtonUpChildSpecial, imageButtonDownChildSpecial, imageButtonUpBed, imageButtonDownBed;

    private BookingModel booking;

    private PaymentTransactionModel paymentTransaction;

    private int defaultValueAdult = 0;
    private int defaultValueChild = 0;
    private int defaultValueSpecialAdult = 0;
    private int defaultValueSpecialChild = 0;

    private RateModel rateModel;

    private TextView txt_mark_location;

    private TextView txt_type_activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.toolbar);
        txt_title_package_tour = findViewById(R.id.txt_title_package_tour);
        txt_company = findViewById(R.id.txt_company);
        mRecycler = findViewById(R.id.recycler_time_schedule);
        btn_confirm_booking = findViewById(R.id.btn_confirm_booking_detail);
        txt_scan_qr_code = findViewById(R.id.txt_scan_qr_code);
        edt_input_fistName = findViewById(R.id.edt_input_fistName);
        edt_input_lastName = findViewById(R.id.edt_input_lastName);
        edt_input_email = findViewById(R.id.edt_input_email);
        edt_input_phone = findViewById(R.id.edt_input_phone);
        edt_input_weChat = findViewById(R.id.edt_input_weChat);
        edt_input_whatsApp = findViewById(R.id.edt_input_whatsApp);
        edt_input_discount = findViewById(R.id.edt_input_discount);
        edt_rest_up_to = findViewById(R.id.edt_rest_up_to);
        edt_single = findViewById(R.id.edt_single);
        edt_add_bed = findViewById(R.id.edt_add_bed);
        edt_bed = findViewById(R.id.edt_bed);;
        price_rest_up_to = findViewById(R.id.price_rest_up_to);
        price_single = findViewById(R.id.price_single);
        price_add_bed = findViewById(R.id.price_add_bed);
        price_bed = findViewById(R.id.price_bed);
        txt_mark_location = findViewById(R.id.txt_mark_location);
        txt_type_activity = findViewById(R.id.txt_type_activity);

        booking = new BookingModel();
        booking.setCheckIn(null);
        booking.setCheckOut(null);
        paymentTransaction = new PaymentTransactionModel();
        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null) {
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            switch (languageLocale.getLanguage()){
                case "th":
                    txt_title_package_tour.setText(items.getTopicTH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                    break;
                case "en":
                    txt_title_package_tour.setText(items.getTopicEN());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                    break;
                case "lo":
                    txt_title_package_tour.setText(items.getTopicLO());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                    break;
                case "zh":
                    txt_title_package_tour.setText(items.getTopicZH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                    break;
            }

            if (items.getProvincesModelArrayList() != null && items.getProvincesModelArrayList().size() != 0){
                String province = "";
                for (ProvincesModel pro : items.getProvincesModelArrayList())
                    switch (languageLocale.getLanguage()){
                        case "th":
                            province += pro.getProvincesTH()+" ";
                            break;
                        case "en":
                            province += pro.getProvincesEN()+" ";
                            break;
                        case "lo":
                            province += pro.getProvincesLO()+" ";
                            break;
                        case "zh":
                            province += pro.getProvincesZH()+" ";
                            break;
                    }
                txt_mark_location.setText(province);
            }

            if (items.getTravelDetailsModelArrayList() != null && items.getTravelDetailsModelArrayList().size() != 0) {
                int day = items.getTravelDetailsModelArrayList().size();
                TextView txt_period_title = findViewById(R.id.txt_period);
                txt_period_title.setText(day+ " " +getResources().getString(R.string.text_day) + " " + (day - 1) + " " + getResources().getString(R.string.text_night));
            }

            mAdapterTravelPeriod = new DetailsTravelPeriodRecyclerViewAdapter(this,items.getTravelPeriodModelArrayList(), languageLocale.getLanguage(),this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            mRecycler.setLayoutManager(layoutManager);
            mRecycler.setAdapter(mAdapterTravelPeriod);

            setupPrice(items);

        }

        //set on click
        setupEventOnClick();
        btn_confirm_booking.setOnClickListener(this);

        txt_scan_qr_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BookingDetailsActivity.this, ScanQRCodeActivity.class));
            }
        });

    }

    private void setupPrice(ItemsModel items) {

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String priceRest = null, priceSingle = null, priceAddBed = null, priceBed  = null;

        for (int i = 0; i < items.getTravelPeriodModelArrayList().size(); i++) {

            ExchangeRate exchangeRate = new ExchangeRate();
            if (rateModel != null) {
                String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
                switch (currency) {
                    case "THB":
                        priceRest = decimalFormat.format(items.getTravelPeriodModelArrayList().get(0).getAdultSpecialPrice());
                        priceSingle = decimalFormat.format(items.getTravelPeriodModelArrayList().get(0).getAdultPrice());
                        priceAddBed = decimalFormat.format(items.getTravelPeriodModelArrayList().get(0).getChildSpecialPrice());
                        priceBed = decimalFormat.format(items.getTravelPeriodModelArrayList().get(0).getChildPrice());
                        price_rest_up_to.setText(currency+""+priceRest);
                        price_single.setText(currency+""+priceSingle);
                        price_add_bed.setText(currency+""+priceAddBed);
                        price_bed.setText(currency+""+priceBed);
                        break;
                    case "USD":
                        priceRest = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getTravelPeriodModelArrayList().get(0).getAdultSpecialPrice(),rateModel.getRateUSD()));
                        priceSingle = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getTravelPeriodModelArrayList().get(0).getAdultPrice(),rateModel.getRateUSD()));
                        priceAddBed = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getTravelPeriodModelArrayList().get(0).getChildSpecialPrice(),rateModel.getRateUSD()));
                        priceBed = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getTravelPeriodModelArrayList().get(0).getChildPrice(),rateModel.getRateUSD()));
                        price_rest_up_to.setText(currency+""+priceRest);
                        price_single.setText(currency+""+priceSingle);
                        price_add_bed.setText(currency+""+priceAddBed);
                        price_bed.setText(currency+""+priceBed);
                        break;
                    case "CNY":
                        priceRest = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getTravelPeriodModelArrayList().get(0).getAdultSpecialPrice(),rateModel.getRateCNY()));
                        priceSingle = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getTravelPeriodModelArrayList().get(0).getAdultPrice(),rateModel.getRateCNY()));
                        priceAddBed = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getTravelPeriodModelArrayList().get(0).getChildSpecialPrice(),rateModel.getRateCNY()));
                        priceBed = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getTravelPeriodModelArrayList().get(0).getChildPrice(),rateModel.getRateCNY()));
                        price_rest_up_to.setText(currency+""+priceRest);
                        price_single.setText(currency+""+priceSingle);
                        price_add_bed.setText(currency+""+priceAddBed);
                        price_bed.setText(currency+""+priceBed);
                        break;
                }
            }else {
                price_rest_up_to.setText(items.getTravelPeriodModelArrayList().get(0).getAdultSpecialPrice()+"");
                price_single.setText(items.getTravelPeriodModelArrayList().get(0).getAdultPrice()+"");
                price_add_bed.setText(items.getTravelPeriodModelArrayList().get(0).getChildSpecialPrice()+"");
                price_bed.setText(items.getTravelPeriodModelArrayList().get(0).getChildPrice()+"");
            }

            booking.setPriceSpecialAdult(items.getTravelPeriodModelArrayList().get(0).getAdultSpecialPrice());
            booking.setPriceAdult(items.getTravelPeriodModelArrayList().get(0).getAdultPrice());
            booking.setPriceSpecialChild(items.getTravelPeriodModelArrayList().get(0).getChildSpecialPrice());
            booking.setPriceChild(items.getTravelPeriodModelArrayList().get(0).getChildPrice());

        }



    }

    private String setupPeriodStart() {
        String period = "";
        String pattern = "EEE dd MMM yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String[] subCheckIn = booking.getCheckIn().split("-");
        String sciDate = subCheckIn[2],sciMonth = subCheckIn[1],sciYear = subCheckIn[0];
        Date date = new Date();
        date.setDate(Integer.parseInt(sciDate));
        date.setMonth(Integer.parseInt(sciMonth));
        date.setYear(Integer.parseInt(sciYear)-1900);
        String mDate = simpleDateFormat.format(date);
        String[] subString = mDate.split(" ");
        String subDay = subString[0];
        String subDate = subString[1];
        String subMonth = subString[2];
        String subYear = subString[3];
        return period;
    }

    private void setupEventOnClick() {
        imageButtonUpAdultSpecial = findViewById(R.id.imageButtonUpAdultSpecial);
        imageButtonDownAdultSpecial = findViewById(R.id.imageButtonDownAdultSpecial);
        imageButtonUpAdult = findViewById(R.id.imageButtonUpAdult);
        imageButtonDownAdult = findViewById(R.id.imageButtonDownAdult);
        imageButtonUpChildSpecial = findViewById(R.id.imageButtonUpChildSpecial);
        imageButtonDownChildSpecial = findViewById(R.id.imageButtonDownChildSpecial);
        imageButtonUpBed = findViewById(R.id.imageButtonUpBed);
        imageButtonDownBed = findViewById(R.id.imageButtonDownBed);

        imageButtonUpAdultSpecial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlusSpecialAdult(1);
            }
        });

        imageButtonDownAdultSpecial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMinusSpecialAdult(1);
            }
        });

        imageButtonUpAdult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlusAdult(1);
            }
        });

        imageButtonDownAdult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMinusAdult(1);
            }
        });

        imageButtonUpChildSpecial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlusSpecialChild(1);
            }
        });

        imageButtonDownChildSpecial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMinusSpecialChild(1);
            }
        });

        imageButtonUpBed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlusChild(1);
            }
        });

        imageButtonDownBed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMinusChild(1);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_confirm_booking_detail:
                Intent intent = new Intent(BookingDetailsActivity.this, BookingDetailsConfirmActivity.class);
                booking.setSpecialAdult(Integer.parseInt(edt_rest_up_to.getText().toString()));
                booking.setGuestAdult(Integer.parseInt(edt_single.getText().toString()));
                booking.setSpecialChild(Integer.parseInt(edt_add_bed.getText().toString()));
                booking.setGuestChild(Integer.parseInt(edt_bed.getText().toString()));
                if (booking.getCheckIn()!= null && booking.getCheckOut() != null){
                    if(getTextBookingDetail()){
                        intent.putExtra("ITEMS_MODEL",items);
                        intent.putExtra("BOOKING",booking);
                        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
                        booking.setPaymentTransactionModel(paymentTransaction);
                        startActivity(intent);
                    }
                }else {
                    Toast.makeText(this, "Please select round-trip.", Toast.LENGTH_SHORT).show();
                }
            break;
        }
    }

    private boolean getTextBookingDetail() {
        if (TextUtils.isEmpty(edt_input_fistName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Fist Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setFirstName(edt_input_fistName.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_lastName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Last Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setLastName(edt_input_lastName.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_email.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Email", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setEmail(edt_input_email.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_phone.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, phone", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setPhone(edt_input_phone.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_weChat.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, WeChat", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setWeChatId(edt_input_weChat.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_whatsApp.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, WhatsApp", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setWhatsAppId(edt_input_whatsApp.getText().toString().replaceAll(" ", ""));
        }
        paymentTransaction.setDiscount(edt_input_discount.getText().toString().replaceAll(" ",""));
        return true;
    }

    @Override
    public void itemClicked(String periodStart, String periodEnd,int position) {
        booking.setCheckIn(periodStart);
        booking.setCheckOut(periodEnd);
        booking.setPosition(position);
    }

    private void setPlusAdult(int plusValue) {
        defaultValueAdult += plusValue;
        setValueAdult();
    }
    private void setPlusChild(int plusValue) {
        defaultValueChild += plusValue;
        setValueChild();
    }
    private void setPlusSpecialAdult(int plusValue) {
        defaultValueSpecialAdult += plusValue;
        setValueSpecialAdult();
    }
    private void setPlusSpecialChild(int plusValue) {
        defaultValueSpecialChild += plusValue;
        setValueSpecialChild();
    }

    private void setMinusAdult(int minusValue) {
        defaultValueAdult -= minusValue;
        if (defaultValueAdult <= 0){
            defaultValueAdult = 0;
        }
        setValueAdult();
    }
    private void setMinusChild(int minusValue) {
        defaultValueChild -= minusValue;
        if (defaultValueChild <= 0){
            defaultValueChild = 0;
        }
        setValueChild();
    }
    private void setMinusSpecialAdult(int minusValue) {
        defaultValueSpecialAdult -= minusValue;
        if (defaultValueSpecialAdult <= 0){
            defaultValueSpecialAdult = 0;
        }
        setValueSpecialAdult();
    }
    private void setMinusSpecialChild(int minusValue) {
        defaultValueSpecialChild -= minusValue;
        if (defaultValueSpecialChild <= 0){
            defaultValueSpecialChild = 0;
        }
        setValueSpecialChild();
    }

    private void setValueSpecialAdult() {
        edt_rest_up_to.setText(defaultValueSpecialAdult+"");
    }
    private void setValueAdult() {
        edt_single.setText(defaultValueAdult+"");
    }
    private void setValueSpecialChild() {
        edt_add_bed.setText(defaultValueSpecialChild+"");
    }
    private void setValueChild() {
        edt_bed.setText(defaultValueChild+"");
    }


}
