package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.PeriodPackagesAdapter;
import com.ipanda.lanexangtourism.DialogFragment.ConfirmOrdersDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.post_booking.HttpPostPackages;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;

public class BookingDetailsConfirmActivity extends AppCompatActivity {

    private static final String TAG = BookingDetailsConfirmActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private String userId;

    private ItemsModel items = new ItemsModel();

    private BookingModel booking = new BookingModel();

    private TextView txt_title_package_tour, txt_company;

    private TextView txt_date_and_time, booking_detail_name, booking_detail_email, booking_detail_phone, booking_detail_discount;

    private Button btn_confirm;

    private TextView btn_cancel;

    private RecyclerView recycler_booking_count;

    private PeriodPackagesAdapter packagesAdapter;

    private TextView txt_grand_total;

    private TextView txt_exchange_rate_id;

    private RateModel rateModel;

    private TextView txt_mark_location;

    private TextView txt_type_activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details_confirm);
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        //views
        txt_title_package_tour = findViewById(R.id.txt_title_package_tour);
        txt_company = findViewById(R.id.txt_company);
        txt_date_and_time = findViewById(R.id.txt_date_and_time);
        booking_detail_name = findViewById(R.id.booking_detail_name);
        booking_detail_email = findViewById(R.id.booking_detail_email);
        booking_detail_phone = findViewById(R.id.booking_detail_phone);
        booking_detail_discount = findViewById(R.id.booking_detail_discount);
        btn_confirm = findViewById(R.id.btn_confirm);
        btn_cancel = findViewById(R.id.btn_cancel);
        recycler_booking_count = findViewById(R.id.recycler_booking_count);
        txt_grand_total = findViewById(R.id.txt_grand_total);
        txt_exchange_rate_id = findViewById(R.id.txt_exchange_rate_id);
        txt_mark_location = findViewById(R.id.txt_mark_location);
        txt_type_activity = findViewById(R.id.txt_type_activity);

        //set event onClick
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupHttpPost();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getIntent() != null){
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            booking = getIntent().getParcelableExtra("BOOKING");
            booking_detail_name.setText(booking.getPaymentTransactionModel().getFirstName()+" "+booking.getPaymentTransactionModel().getLastName());
            booking_detail_email.setText(booking.getPaymentTransactionModel().getEmail());
            booking_detail_phone.setText(booking.getPaymentTransactionModel().getPhone());
            booking.setItemsModel(items);
            txt_date_and_time.setText(booking.getCheckIn()+" - "+booking.getCheckOut());

            if (booking.getPaymentTransactionModel().getDiscount()!= null && booking.getPaymentTransactionModel().getDiscount() != "" && !booking.getPaymentTransactionModel().getDiscount().equals("")) {
                booking_detail_discount.setText(booking.getPaymentTransactionModel().getDiscount());
            }

            switch (languageLocale.getLanguage()){
                case "th":
                    txt_title_package_tour.setText(items.getTopicTH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                    break;
                case "en":
                    txt_title_package_tour.setText(items.getTopicEN());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                    break;
                case "lo":
                    txt_title_package_tour.setText(items.getTopicLO());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                    break;
                case "zh":
                    txt_title_package_tour.setText(items.getTopicZH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                    break;
            }
            setupPeriodPackages(booking);
        }

        if (items.getProvincesModelArrayList() != null && items.getProvincesModelArrayList().size() != 0){
            String province = "";
            for (ProvincesModel pro : items.getProvincesModelArrayList())
                switch (languageLocale.getLanguage()){
                    case "th":
                        province += pro.getProvincesTH()+" ";
                        break;
                    case "en":
                        province += pro.getProvincesEN()+" ";
                        break;
                    case "lo":
                        province += pro.getProvincesLO()+" ";
                        break;
                    case "zh":
                        province += pro.getProvincesZH()+" ";
                        break;
                }
            txt_mark_location.setText(province);
        }

        if (items.getTravelDetailsModelArrayList() != null && items.getTravelDetailsModelArrayList().size() != 0) {
            int day = items.getTravelDetailsModelArrayList().size();
            TextView txt_period_title = findViewById(R.id.txt_period);
            txt_period_title.setText(day+ " " +getResources().getString(R.string.text_day) + " " + (day - 1) + " " + getResources().getString(R.string.text_night));
        }

    }

    private void setupHttpPost() {
        if (userId != null && items != null){
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Booking");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("Items_items_id", String.valueOf(items.getItemsId()));
            paramsPost.put("paymentTransaction_firstName", booking.getPaymentTransactionModel().getFirstName());
            paramsPost.put("paymentTransaction_lastName", booking.getPaymentTransactionModel().getLastName());
            paramsPost.put("paymentTransaction_email", booking.getPaymentTransactionModel().getEmail());
            paramsPost.put("paymentTransaction_phone", booking.getPaymentTransactionModel().getPhone());
            paramsPost.put("paymentTransaction_weChatId", booking.getPaymentTransactionModel().getWeChatId());
            paramsPost.put("paymentTransaction_whatsAppId", booking.getPaymentTransactionModel().getWhatsAppId());
            paramsPost.put("booking_guestAdult", String.valueOf(booking.getSpecialAdult()));
            paramsPost.put("booking_guestAdultSingle", String.valueOf(booking.getGuestAdult()));
            paramsPost.put("booking_guestChild", String.valueOf(booking.getGuestChild()));
            paramsPost.put("booking_guestChildBed", String.valueOf(booking.getSpecialChild()));
            paramsPost.put("booking_duration", String.valueOf(booking.getPosition()));
            httpCallPost.setParams(paramsPost);
            new HttpPostPackages().execute(httpCallPost);
            Bundle bundle = new Bundle();
            bundle.putString("TITLE","กำลังทำการส่งข้อมูลเพิ่อจองแพคเกจทัวร์");
            bundle.putString("DETAILS","กรุณารอสักครู่ระบบกำส่งข้อมูลเบื่องต้นไปยังพนักงานของการจองแพคเกจทัวร์เมื่อพนักงาน ได้รับข้อมูลขอท่านแล้ว จะมีเจ้าหน้าที่ติดต่อกลับ");
            if (items.getBusinessModel() != null) {
                bundle.putString("PHONE", items.getBusinessModel().getPhone());
            }else {
                bundle.putString("PHONE", "-");
            }
            DialogFragment dialogFragment = ConfirmOrdersDialogFragment.newInstance();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getSupportFragmentManager(),TAG);
        }
    }

    private void setupPeriodPackages(BookingModel booking) {
        ArrayList<BookingModel> modelArrayList = new ArrayList<>();
        BookingModel book = null;
        int totals = 0;
        if (booking.getSpecialAdult() != 0 && booking.getSpecialAdult() > 1){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_adult));
            book.setSpecialAdult(booking.getSpecialAdult());
            book.setPriceSpecialAdult(booking.getPriceSpecialAdult());
            modelArrayList.add(book);
            totals += (booking.getPriceSpecialAdult() * booking.getSpecialAdult());
        }else {
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_adult));
            book.setSpecialAdult(booking.getSpecialAdult());
            book.setPriceSpecialAdult(booking.getPriceAdult());
            modelArrayList.add(book);
            totals += (booking.getPriceAdult() * booking.getSpecialAdult());
        }
        if (booking.getGuestAdult() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_single_stay));
            book.setSpecialAdult(booking.getGuestAdult());
            book.setPriceSpecialAdult(booking.getPriceAdult());
            modelArrayList.add(book);
            totals += (booking.getPriceAdult() * booking.getGuestAdult());
        }
        if (booking.getSpecialChild() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_add_bed));
            book.setSpecialAdult(booking.getSpecialChild());
            book.setPriceSpecialAdult(booking.getPriceSpecialChild());
            modelArrayList.add(book);
            totals += (booking.getPriceSpecialChild() * booking.getSpecialChild());
        }
        if (booking.getGuestChild() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_child));
            book.setSpecialAdult(booking.getGuestChild());
            book.setPriceSpecialAdult(booking.getPriceChild());
            modelArrayList.add(book);
            totals += (booking.getPriceChild() * booking.getGuestChild());
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(totals);
                    txt_exchange_rate_id.setText(currency);
                    txt_grand_total.setText(price);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                    txt_exchange_rate_id.setText(currency);
                    txt_grand_total.setText(price);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateCNY()));
                    txt_exchange_rate_id.setText(currency);
                    txt_grand_total.setText(price);
                    break;
            }
        }else {
            price = decimalFormat.format(totals);
            txt_grand_total.setText(price);
        }

        packagesAdapter = new PeriodPackagesAdapter(this, modelArrayList, languageLocale.getLanguage(), rateModel);
        recycler_booking_count.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycler_booking_count.setAdapter(packagesAdapter);


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
