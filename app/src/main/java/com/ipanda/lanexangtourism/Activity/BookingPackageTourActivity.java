package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.ItemsPackageTourRecyclerViewAdapter;
import com.ipanda.lanexangtourism.DialogFragment.SearchPackageTourDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ItemsPackageTourClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsPackageTourAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsLastSearchPackageTourCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsPackageTourCallBack;
import com.ipanda.lanexangtourism.items_view.ViewPackageTourActivity;
import com.ipanda.lanexangtourism.post_search.LastSearchPackageTour;

import java.util.ArrayList;
import java.util.HashMap;

public class BookingPackageTourActivity extends AppCompatActivity implements View.OnClickListener, ItemsPackageTourCallBack, ItemsPackageTourClickListener , ExchangeRateCallBack ,
        ItemsLastSearchPackageTourCallBack {

    //variables
    private static String TAG = BookingPackageTourActivity.class.getSimpleName();

    private CardView btnSearchBooking;

    private TextView txtTitleSearchBooking, txt_recommend_package_tour;

    private Toolbar mToolbar;

    private String userId;

    private ChangeLanguageLocale languageLocale;

    private RecyclerView recyclerPackageTour;

    private ItemsPackageTourRecyclerViewAdapter mPackageTourAdapter;

    private RateModel rateModel;

    private RecyclerView recycler_last_view;

    private ItemsPackageTourRecyclerViewAdapter lastSearchPackageTourAdapter;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    private ArrayList<ItemsModel> newItemsOffline = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_package_tour);
        isModeOnline = getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);

        //views
        mToolbar = findViewById(R.id.menu_toolbar);
        btnSearchBooking = findViewById(R.id.btn_search_booking);
        txtTitleSearchBooking = findViewById(R.id.txt_title_search_booking);
        txt_recommend_package_tour = findViewById(R.id.txt_recommend_package_tour);
        recyclerPackageTour = findViewById(R.id.recycler_near_package_tour);
        recycler_last_view = findViewById(R.id.recycler_last_view);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //set text
        txtTitleSearchBooking.setText(getString(R.string.text_search_for_tour));

        //set on click event
        btnSearchBooking.setOnClickListener(this);
        txt_recommend_package_tour.setOnClickListener(this);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        String url ="";
        if (isModeOnline) {
            if (userId != null) {
                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Reservations/PackageTours?user_id=" + userId;
            } else {
                url = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Reservations/PackageTours";
            }

            String urlRate = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ExchangeRate";
            new ExchangeRateAsyncTask(this).execute(urlRate);

            new ItemsPackageTourAsyncTask(this).execute(url);

            getLastSearch();
        }else {
            getVersion = getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
            if (getVersion != 0) {
                mDatabase = new DatabaseHelper(this, getVersion);
                mManager = new DatabaseManager(this, mDatabase);
                setItemsOfflineMode();
            }else {
                alertDialog();
            }
        }
    }

    private void setItemsOfflineMode() {
        newItemsOffline = mManager.getListPackageTours();
        if (newItemsOffline != null && newItemsOffline.size() != 0) {
            mPackageTourAdapter = new ItemsPackageTourRecyclerViewAdapter(this,newItemsOffline, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recyclerPackageTour.setLayoutManager(layoutManager);
            recyclerPackageTour.setAdapter(mPackageTourAdapter);
            setViewVisibility(R.id.recycler_near_package_tour, View.VISIBLE);
        }
    }

    private void getLastSearch(){
        if (isModeOnline) {
            String getLastSearch = getSharedPreferences("PREF_APP_VIEW_PACKAGE_TOUR", Context.MODE_PRIVATE).getString("APP_VIEW_PACKAGE_TOUR", null);
            if (getLastSearch != null && !getLastSearch.equals("")) {
                postRequestLastSearch(getLastSearch);
            }
        }
    }

    private void postRequestLastSearch(String getLastSearch){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/LastSearch");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("category_id", "9");
        paramsPost.put("last_id", getLastSearch);
        paramsPost.put("latitude", "");
        paramsPost.put("longitude", "");
        httpCallPost.setParams(paramsPost);
        new LastSearchPackageTour(this).execute(httpCallPost);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_search_booking:
                onClickSearchPackage();
                break;
            case R.id.txt_recommend_package_tour:
//                startActivity(new Intent(this, ViewPackageTourActivity.class));
                break;
        }
    }

    private void onClickSearchPackage(){
       /* DialogFragment dialogFragment = SearchPackageTourDialogFragment.newInstance();
        dialogFragment.show(getSupportFragmentManager(), TAG);*/
       Intent intent = new Intent(this, SearchPackageTourActivity.class);
       intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
       if (isModeOnline) {
           startActivity(intent);
       }else {

       }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {
        setViewVisibility(R.id.recycler_near_package_tour, View.GONE);
        setViewVisibility(R.id.loader_items_id, View.VISIBLE);
        setViewVisibility(R.id.img_load_field_id, View.GONE);
        setViewVisibility(R.id.txt_load_field_id, View.GONE);
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerLastSearchPackageTour(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            lastSearchPackageTourAdapter = new ItemsPackageTourRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recycler_last_view.setLayoutManager(layoutManager);
            recycler_last_view.setAdapter(lastSearchPackageTourAdapter);
        }
    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;

        }
    }

    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            Log.e("check data ", itemsModelArrayList + "");

            mPackageTourAdapter = new ItemsPackageTourRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recyclerPackageTour.setLayoutManager(layoutManager);
            recyclerPackageTour.setAdapter(mPackageTourAdapter);

            setViewVisibility(R.id.recycler_near_package_tour, View.VISIBLE);
            setViewVisibility(R.id.loader_items_id, View.GONE);
            setViewVisibility(R.id.img_load_field_id, View.GONE);
            setViewVisibility(R.id.txt_load_field_id, View.GONE);

        }else {
            setViewVisibility(R.id.recycler_near_package_tour, View.GONE);
            setViewVisibility(R.id.loader_items_id, View.GONE);
            setViewVisibility(R.id.img_load_field_id, View.VISIBLE);
            setViewVisibility(R.id.txt_load_field_id, View.VISIBLE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClickedItems(ItemsModel items) {
        Intent intent = new Intent(this, ViewPackageTourActivity.class);
        intent.putExtra("ITEMS_MODEL", items);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    private void setViewVisibility(int id, int visibility) {
        View view = findViewById(id);
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
