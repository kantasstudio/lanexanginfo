package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.ipanda.lanexangtourism.Adapter.BookmarksRecyclerViewAdapter;
import com.ipanda.lanexangtourism.DialogFragment.InterestedDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.BookmarksClickListener;
import com.ipanda.lanexangtourism.MenuFragment.HomeFragment;
import com.ipanda.lanexangtourism.Model.BookMarksModel;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsHotelModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.BookmarksAsyncTask;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.BookmarksCallBack;
import com.ipanda.lanexangtourism.items_view.ViewTourismActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class BookmarksActivity extends AppCompatActivity implements View.OnClickListener, BookmarksCallBack, BookmarksClickListener {

    //Variables
    private static String TAG = BookmarksActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ImageView btnBackPressed, btnBookmarksSettings;

    private ChangeLanguageLocale languageLocale;

    private RecyclerView recycler_bookmark;

    private BookmarksRecyclerViewAdapter bookmarksAdapter;

    private String userId = null;

    private FusedLocationProviderClient fusedLocationClient;

    private Location getLocation;

    private ArrayList<BookMarksModel> bookMarksArrayList;

    private String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.bookmarks_toolbar);
        btnBackPressed = findViewById(R.id.btn_back_pressed);
        btnBookmarksSettings = findViewById(R.id.btn_bookmarks_settings);
        recycler_bookmark = findViewById(R.id.recycler_bookmark);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null) {
            userId = getIntent().getStringExtra("USER_ID");
        }


        //setup on click button back
        btnBackPressed.setOnClickListener(this);
        btnBookmarksSettings.setOnClickListener(this);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            getLocation = location;
                            if (userId != null) {
                                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/BookMarks/" + userId+"?latitude="+getLocation.getLatitude()+ "&longitude=" + getLocation.getLongitude();
                                new BookmarksAsyncTask(BookmarksActivity.this).execute(url);
                            }
                        }
                    }
                });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back_pressed:
                onBackPressed();
                break;
            case R.id.btn_bookmarks_settings:
                onClickBookmarksSettings(v);
                break;
        }
    }

    private void onClickBookmarksSettings(View v) {
        PopupMenu menu = new PopupMenu(this,v);
        menu.getMenuInflater().inflate(R.menu.menu_bookmarks_setting,menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_distance:
                        Toast.makeText(BookmarksActivity.this, "Distance", Toast.LENGTH_SHORT).show();
                        Collections.sort(bookMarksArrayList,new BookmarksActivity.DistanceSorterBookmarks());
                        setBookMarkAdapter();
                        break;
                    case R.id.action_recording:
                        Toast.makeText(BookmarksActivity.this, "Recording", Toast.LENGTH_SHORT).show();
                        Collections.sort(bookMarksArrayList, Comparator.comparing(BookMarksModel::getBookMarkTimestamp));
                        setBookMarkAdapter();
                        break;
                }

                return true;
            }
        });
        menu.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<BookMarksModel> bookMarksModelArrayList) {
        if (bookMarksModelArrayList != null && bookMarksModelArrayList.size() != 0){
            Log.e("check data", bookMarksModelArrayList+"");
            this.bookMarksArrayList = bookMarksModelArrayList;
            setBookMarkAdapter();

        }
    }

    private void setBookMarkAdapter() {
        if (bookMarksArrayList != null && bookMarksArrayList.size() != 0) {
            bookmarksAdapter = new BookmarksRecyclerViewAdapter(this, bookMarksArrayList, languageLocale.getLanguage(), this);
            recycler_bookmark.setLayoutManager(new LinearLayoutManager(this));
            recycler_bookmark.setAdapter(bookmarksAdapter);
        }
    }

    private class DistanceSorterBookmarks implements Comparator<BookMarksModel> {
        public int compare(BookMarksModel o1, BookMarksModel o2) {
            return o1.getDistance() - o2.getDistance();
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClickedInterested(BookMarksModel bookMarksModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("BOOKMARKS_MODEL",bookMarksModel);
        DialogFragment dialogFragment = InterestedDialogFragment.newInstance();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(getSupportFragmentManager(),"BookmarksActivity");
    }

    @Override
    public void itemClickedItems(ItemsModel itemsModel) {
        Intent intent = new Intent(this, ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL", itemsModel);
        intent.putExtra("CATEGORY",itemsModel.getMenuItemModel().getMenuItemId());
        startActivity(intent);

    }

    @Override
    public void itemClickedBookmarks(BookMarksModel bookMarksModel) {
        if (bookMarksModel != null && userId != null){
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/BookMarks");
            HashMap<String,String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id",userId);
            paramsPost.put("Items_items_id", String.valueOf(bookMarksModel.getItemsModelArrayList().get(0).getItemsId()));
/*            paramsPost.put("latitude", String.valueOf(getLocation.getLatitude()));
            paramsPost.put("longitude", String.valueOf(getLocation.getLongitude()));*/
            httpCallPost.setParams(paramsPost);
            new HttpPostRequestAsyncTask().execute(httpCallPost);
            recreate();
        }else {
            new AlertDialog.Builder(this)
                    .setTitle("กรุณาล็อกอินก่อน")
                    .setMessage("....")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .create().show();
        }
    }

}
