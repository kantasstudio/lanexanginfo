package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Adapter.StickerAdapter;
import com.ipanda.lanexangtourism.Adapter.StickerImageAdapter;
import com.ipanda.lanexangtourism.BuildConfig;
import com.ipanda.lanexangtourism.Helper.CameraPreview;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.StickerImageClickListener;
import com.ipanda.lanexangtourism.Interface_click.StickerTitleClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.StickerModel;
import com.ipanda.lanexangtourism.Model.StickerPathsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.StickerAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.StickerCallBack;
import com.ipanda.lanexangtourism.sticker.StickerImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static androidx.core.content.FileProvider.getUriForFile;


public class CameraActivity extends AppCompatActivity implements StickerCallBack , StickerTitleClickListener , StickerImageClickListener {

    //Variables
    private static final String TAG = CameraActivity.class.getSimpleName();

    private String[] neededPermissions = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};

    private static final int REQUEST_GALLERY_PHOTO = 101;

    private static final int REQUEST_CODE = 100;

    private static final int MEDIA_TYPE_IMAGE = 1;

    private FrameLayout mFrameLayout;

    private Camera mCamera;

    private CameraPreview mPreview;

    private File pictureFile = null;

    private byte[] dataFile = null;

    private Uri contentUri = null;

    private ItemsModel items;

    private ChangeLanguageLocale languageLocale;

    private RecyclerView recyclerTitleSticker;

    private StickerAdapter stickerAdapter;

    private RecyclerView recyclerItemsSticker;

    private StickerImageAdapter stickerImageAdapter;

    private boolean isShowSticker = false;

    private Bitmap rotatedBitmap;

    private ImageView imageView;

    private ArrayList<StickerImageView> listSticker = new ArrayList<>();

    private ImageView img_previews;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        if (getIntent() != null){
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
        }

        imageView = findViewById(R.id.iv_image);
        img_previews = findViewById(R.id.img_previews);

        boolean result = checkPermission();
        if (result) {
            setupCamera();
            // Setup event listener
            setupButtonOnClickListener();
        }

        checkCameraHardware(this);



        String url = "";
        new StickerAsyncTask(this).execute(url);

    }

    private void setupCamera(){
        // Create an instance of Camera
        mCamera = getCameraInstance();
        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        mFrameLayout = (FrameLayout) findViewById(R.id.camera_preview);
        mFrameLayout.addView(mPreview);

    }

    private void setupButtonOnClickListener() {
        setViewVisibility(R.id.button_capture, View.VISIBLE);
        // Add a listener to the Capture button
        ImageButton captureButton = (ImageButton) findViewById(R.id.button_capture);
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get an image from the camera
                        mCamera.takePicture(null, null, mPicture);
                        setViewVisibility(R.id.button_capture, View.GONE);

                    }
                }
        );

        // Add a listener to the save as picture
        ImageButton saveButton = (ImageButton) findViewById(R.id.button_save);
        saveButton.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePhotoToDirectory();
                setViewVisibility(R.id.toolbar_control_sticker, View.VISIBLE);
                setViewVisibility(R.id.toolbar_back_camera, View.VISIBLE);
                setViewVisibility(R.id.toolbar_save_camera, View.GONE);
                setViewVisibility(R.id.button_capture, View.GONE);
            }
        });

        // Add a listener to the cancel
        ImageButton cancelButton = (ImageButton) findViewById(R.id.button_cancel);
        cancelButton.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setViewVisibility(R.id.toolbar_save_camera, View.GONE);
                setViewVisibility(R.id.button_capture, View.VISIBLE);
                recreate();
            }
        });

        // Add a listener on back page
        ImageButton backButton = (ImageButton) findViewById(R.id.button_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setViewVisibility(R.id.toolbar_back_camera, View.GONE);
                onBackPressed();
            }
        });

        // Add a listener to the show sticker

        CardView showStickerButton = (CardView) findViewById(R.id.cv_show_sticker);
        showStickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isShowSticker){
                    isShowSticker = false;
                    setViewVisibility(R.id.recycler_title_sticker, View.GONE);
                    setViewVisibility(R.id.recycler_items_sticker, View.GONE);
                }else {
                    isShowSticker = true;
                    setViewVisibility(R.id.recycler_title_sticker, View.VISIBLE);
                    setViewVisibility(R.id.recycler_items_sticker, View.VISIBLE);
                }

            }
        });

        // Add a listener to the share sticker
        CardView shareStickerButton = (CardView) findViewById(R.id.cv_share_sticker);
        shareStickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharePhotoToSocial();
            }
        });

        // Add a listener to the save sticker
        CardView saveStickerButton = (CardView) findViewById(R.id.cv_save_sticker);
        saveStickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAsSticker();
            }
        });

    }

    private static Camera getCameraInstance(){
        Camera camera = null;
        try {
            camera = Camera.open();
            camera.setDisplayOrientation(90);
        }
        catch (Exception e){
            Log.e("camera null",e.getMessage());
        }
        return camera;
    }

    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    private boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            ArrayList<String> permissionsNotGranted = new ArrayList<>();
            for (String permission : neededPermissions) {
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsNotGranted.add(permission);
                }
            }
            if (permissionsNotGranted.size() > 0) {
                boolean shouldShowAlert = false;
                for (String permission : permissionsNotGranted) {
                    shouldShowAlert = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                }
                if (shouldShowAlert) {
                    showPermissionAlert(permissionsNotGranted.toArray(new String[permissionsNotGranted.size()]));
                } else {
                    requestPermissions(permissionsNotGranted.toArray(new String[permissionsNotGranted.size()]));
                }
                return false;
            }
        }
        return true;
    }

    private void showPermissionAlert(final String[] permissions) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle(R.string.permission_required);
        alertBuilder.setMessage(R.string.permission_message);
        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions(permissions);
            }
        });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    private void requestPermissions(String[] permissions) {
        ActivityCompat.requestPermissions(CameraActivity.this, permissions, REQUEST_CODE);
    }

    private void setViewVisibility(int id, int visibility) {
        View view = findViewById(id);
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            dataFile = data;
            setViewVisibility(R.id.toolbar_save_camera, View.VISIBLE);
            if (pictureFile == null){
                Log.d(TAG, "Error creating media file, check storage permissions");
                return;
            }

        }
    };

    //Create a File for saving an image or video
    private File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "LANEXANG");
//        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES)+File.separator, "LANEXANG");

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (mediaStorageDir == null || ! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private void savePhotoToDirectory(){
        try {
            if (pictureFile != null) {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                if (dataFile != null) {
                    fos.write(dataFile);
                    fos.close();
                    Log.d(TAG, "save path : " + pictureFile);
                }
                rotateImage(setReducedImageSize());
            }
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    private void galleryAddPic(File currentPhotoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(currentPhotoPath);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);

    }


    private boolean checkSDCardStatus() {
        String SDCardStatus = Environment.getExternalStorageState();

        switch (SDCardStatus) {
            case Environment.MEDIA_MOUNTED:
                return true;
            case Environment.MEDIA_MOUNTED_READ_ONLY:
                Toast.makeText(this, "SD card is ready only.", Toast.LENGTH_LONG).show();
                return false;
            default:
                Toast.makeText(this, "SD card is not available.", Toast.LENGTH_LONG).show();
                return false;
        }
    }

    private void writeDataToPath(File path, String fileName, String data) {
        File targetFilePath = new File(data, fileName);
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(targetFilePath);
            fos.write(data.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            if (fos != null) {
                try {
                    Toast.makeText(this, "Write to <" + targetFilePath.getAbsolutePath() + "> successfully!", Toast.LENGTH_LONG).show();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, "Failed to write!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private Bitmap setReducedImageSize(){
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pictureFile.toString(),bmOptions);
        int cameraImageWidth = bmOptions.outWidth;
        int cameraImageHeight = bmOptions.outHeight;

        int scaleFactor = Math.min(cameraImageWidth/targetW,cameraImageHeight/targetH);
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(pictureFile.toString(),bmOptions);
    }

    private void rotateImage(Bitmap bitmap){
        ExifInterface exifInterface = null;
        byte[] bitMapData = null;
        try {
            exifInterface = new ExifInterface(pictureFile.toString());
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation){
                case ExifInterface.ORIENTATION_NORMAL:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(270);
                    break;
                default:
            }

            rotatedBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
            imageView.setImageBitmap(rotatedBitmap);
            mFrameLayout.removeView(mPreview);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bitMapData = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(pictureFile.toString());
            fos.write(bitMapData);
            fos.flush();
            fos.close();



            galleryAddPic(pictureFile);


        }catch (IOException e){
            e.printStackTrace();
        }

    }

    private void sharePhotoToSocial(){
        saveAsSticker();
        Uri imageUri = getUriForFile(CameraActivity.this, BuildConfig.APPLICATION_ID + ".provider", pictureFile);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        shareIntent.setType("image/jpeg");
        if (items != null) {
            String topic = "";
            switch (languageLocale.getLanguage()){
                case "th":
                    topic = items.getTopicTH();
                    break;
                case "en":
                    topic = items.getTopicEN();
                    break;
                case "lo":
                    topic = items.getTopicLO();
                    break;
                case "zh":
                    topic = items.getTopicZH();
                    break;
            }
            startActivity(Intent.createChooser(shareIntent, topic));
        }


    }

    private void saveAsSticker() {

        for (StickerImageView sk : listSticker){
            sk.setControlItemsHidden(true);
        }
        listSticker.clear();

        mFrameLayout.setDrawingCacheEnabled(true);
        mFrameLayout.buildDrawingCache();
        Bitmap cache = mFrameLayout.getDrawingCache();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(pictureFile);
            cache.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            getBitmapSetShow(pictureFile);
            Log.d(TAG, "save sticker path : " + pictureFile);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mFrameLayout.destroyDrawingCache();
            galleryAddPic(pictureFile);
        }

    }

    public void getBitmapSetShow(File pictureFile) {
        Bitmap bitmap = null;
        try {
            File f = new File(pictureFile.toString());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            img_previews.setImageBitmap(bitmap);

            setViewVisibility(R.id.camera_preview, View.GONE);
            setViewVisibility(R.id.toolbar_back_camera, View.GONE);
            setViewVisibility(R.id.toolbar_control_sticker, View.GONE);
            setViewVisibility(R.id.recycler_title_sticker, View.GONE);
            setViewVisibility(R.id.img_previews, View.VISIBLE);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<StickerModel> stickerArrayList) {
        if (stickerArrayList != null && stickerArrayList.size() != 0){

        }else {
            ArrayList<StickerModel> newSticker = new ArrayList<>();
            newSticker.add(new StickerModel("อารมณ์"));
            newSticker.add(new StickerModel("เทศกาล"));
            newSticker.add(new StickerModel("คำติดตลก"));
            newSticker.add(new StickerModel("อื่น ๆ"));
            recyclerTitleSticker = findViewById(R.id.recycler_title_sticker);
            stickerAdapter = new StickerAdapter(this, newSticker, languageLocale.getLanguage(), this);
            recyclerTitleSticker.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            recyclerTitleSticker.setAdapter(stickerAdapter);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onClickedSticker(StickerModel sticker) {
        recyclerItemsSticker = findViewById(R.id.recycler_items_sticker);
        setViewVisibility(R.id.recycler_items_sticker, View.VISIBLE);

        ArrayList<StickerPathsModel> newSticker = new ArrayList<>();
        newSticker.add(new StickerPathsModel("sticker01"));
        newSticker.add(new StickerPathsModel("sticker02"));
        newSticker.add(new StickerPathsModel("frame01"));
        newSticker.add(new StickerPathsModel("frame02"));

        stickerImageAdapter = new StickerImageAdapter(this, newSticker, languageLocale.getLanguage(), this);
        recyclerItemsSticker.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerItemsSticker.setAdapter(stickerImageAdapter);

    }

    @Override
    public void onClickedStickerImage(StickerPathsModel sticker) {

        int resId = getResourceByFilename(this, sticker.getStickerPaths());
        Bitmap imageBitmap = BitmapFactory.decodeResource(this.getResources(), resId);

        StickerImageView iv_sticker = new StickerImageView(this);
        ImageView img = (ImageView) findViewById(R.id.iv_sticker);
        img.setImageBitmap(imageBitmap);

        iv_sticker.setImageDrawable(((ImageView) findViewById(R.id.iv_sticker)).getDrawable());
        iv_sticker.setImageBitmap(imageBitmap);

        mFrameLayout.addView(iv_sticker);
        listSticker.add(iv_sticker);

    }



    public static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE:
                for (int result : grantResults) {
                    if (result == PackageManager.PERMISSION_DENIED) {
                        Toast.makeText(CameraActivity.this, R.string.permission_warning, Toast.LENGTH_LONG).show();
                        setViewVisibility(R.id.showPermissionMsg, View.VISIBLE);
                        return;
                    }
                }
                setupCamera();
                // Setup event listener
                setupButtonOnClickListener();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



}
