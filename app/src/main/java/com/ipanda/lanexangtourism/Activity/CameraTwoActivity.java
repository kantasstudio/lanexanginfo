package com.ipanda.lanexangtourism.Activity;


import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.ipanda.lanexangtourism.Adapter.StickerAdapter;
import com.ipanda.lanexangtourism.Adapter.StickerImageAdapter;
import com.ipanda.lanexangtourism.BuildConfig;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.StickerImageClickListener;
import com.ipanda.lanexangtourism.Interface_click.StickerTitleClickListener;
import com.ipanda.lanexangtourism.Interface_utils.ImageContract;
import com.ipanda.lanexangtourism.Interface_utils.ImagePresenter;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.StickerModel;
import com.ipanda.lanexangtourism.Model.StickerPathsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.StickerAsyncTask;
import com.ipanda.lanexangtourism.asynctask.StickerPathsAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.StickerCallBack;
import com.ipanda.lanexangtourism.interface_callback.StickerPathsCallBack;
import com.ipanda.lanexangtourism.sticker.StickerImageView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;


import org.apache.tools.ant.taskdefs.Java;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;

import static androidx.core.content.FileProvider.getUriForFile;
import static org.spongycastle.asn1.x509.GeneralName.directoryName;

public class CameraTwoActivity extends AppCompatActivity implements ImageContract.View, StickerCallBack, StickerTitleClickListener, StickerImageClickListener , StickerPathsCallBack {

    //Variables
    private static final String TAG = CameraActivity.class.getSimpleName();

    private static String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private static final int REQUEST_TAKE_PHOTO = 101;

    private static final int REQUEST_GALLERY_PHOTO = 102;

    private ChangeLanguageLocale languageLocale;

    private Uri photoURI;

    private ConstraintLayout con_iv_previews;

    private TextView txt_previews;

    private ImageView iv_image;

    private ImageView iv_previews;

    private ImagePresenter mPresenter;

    private Bitmap bitmap;

    private Bitmap bitmapSticker;

    private Bitmap rotatedBitmap = null;

    private ExifInterface exifInterface = null;

    private byte[] bitMapData = null;

    private File pictureFile = null;

    private File pFile = null;

    private FrameLayout mFrameLayout;

    private ItemsModel items;

    private RecyclerView recyclerTitleSticker;

    private StickerAdapter stickerAdapter;

    private RecyclerView recyclerItemsSticker;

    private StickerImageAdapter stickerImageAdapter;

    private ArrayList<StickerImageView> listSticker = new ArrayList<>();

    private boolean isShowSticker = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_two);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        iv_image = findViewById(R.id.iv_image);
        iv_previews = findViewById(R.id.iv_previews);
        txt_previews = findViewById(R.id.txt_previews);
        con_iv_previews = findViewById(R.id.con_iv_previews);
        mFrameLayout = (FrameLayout) findViewById(R.id.camera_preview_two);
        recyclerTitleSticker = findViewById(R.id.recycler_title_sticker);
        recyclerItemsSticker = findViewById(R.id.recycler_items_sticker);

        if (getIntent() != null){
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
        }


        ButterKnife.bind(this);
        mPresenter = new ImagePresenter(this);
        mPresenter.cameraClick();

        setupButtonOnClickListener();

        String url = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Sticker/getStickerType";
        new StickerAsyncTask(this).execute(url);

    }

    private void setupButtonOnClickListener() {
        // Add a listener to the save as picture
        ImageButton saveButton = (ImageButton) findViewById(R.id.button_save);
        saveButton.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    savePhotoToDirectory();
                    setViewVisibility(R.id.toolbar_control_sticker, View.VISIBLE);
                    setViewVisibility(R.id.toolbar_back_camera, View.VISIBLE);
                    setViewVisibility(R.id.toolbar_save_camera, View.GONE);
                }
            });

        // Add a listener to the cancel
        ImageButton cancelButton = (ImageButton) findViewById(R.id.button_cancel);
        cancelButton.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

        // Add a listener on back page
        ImageButton backButton = (ImageButton) findViewById(R.id.button_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // Add a listener to the show sticker
        CardView showStickerButton = (CardView) findViewById(R.id.cv_show_sticker);
        showStickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isShowSticker){
                    isShowSticker = false;
                    setViewVisibility(R.id.recycler_title_sticker, View.GONE);
                    setViewVisibility(R.id.recycler_items_sticker, View.GONE);
                }else {
                    isShowSticker = true;
                    setViewVisibility(R.id.recycler_title_sticker, View.VISIBLE);
                    setViewVisibility(R.id.recycler_items_sticker, View.VISIBLE);
                }

            }
        });

        // Add a listener to the share sticker
        CardView shareStickerButton = (CardView) findViewById(R.id.cv_share_sticker);
        shareStickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharePhotoToSocial();
            }
        });

        // Add a listener to the save sticker
        CardView saveStickerButton = (CardView) findViewById(R.id.cv_save_sticker);
        saveStickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAsSticker();
            }
        });

    }

    private void savePhotoToDirectory(){
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bitMapData = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(bitMapData);
            fos.flush();
            fos.close();

        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    @Override
    public boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(this, mPermission);
            if (result == PackageManager.PERMISSION_DENIED) return false;
        }
        return true;
    }

    @Override
    public void showPermissionDialog() {
        Dexter.withActivity(this).withPermissions(permissions)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(error -> showErrorDialog())
                .onSameThread()
                .check();
    }

    @Override
    public File getFilePath() {
        return getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public void showSettingsDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.message_need_permission));
        builder.setMessage(getString(R.string.message_grant_permission));
        builder.setPositiveButton(getString(R.string.label_setting), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    @Override
    public void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void startCamera(File file) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            if (file != null) {
                photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider", file);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }

    @Override
    public void chooseGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    @Override
    public void showNoSpaceDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.error_message_no_more_space));
        builder.setMessage(getString(R.string.error_message_insufficient_space));
        builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    @Override
    public int availableDisk() {
        File mFilePath = getFilePath();
        long freeSpace = mFilePath.getFreeSpace();
        return Math.round(freeSpace / 1048576);
    }

    @Override
    public File newFile() {
/*        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "LANEXANG");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        try {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg");
            pictureFile = mediaFile;
            pFile = mediaFile;
            return mediaFile;

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "LANEXANG");
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }

        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTimeInMillis();
        String mFileName = String.valueOf(timeInMillis) + ".jpeg";
        File mFilePath = getFilePath();
        try {
            File newFile = new File(mFilePath.getAbsolutePath(), mFileName);
            File test = new File(mediaStorageDir.getAbsolutePath(), mFileName);
            newFile.createNewFile();
            test.createNewFile();

            pictureFile = test;
            pFile = test;
            return test;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void showErrorDialog() {
        Toast.makeText(getApplicationContext(), getString(R.string.error_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayImagePreview(String mFilePath) {
        Glide.with(CameraTwoActivity.this).load(mFilePath).apply(new RequestOptions().centerCrop().placeholder(R.drawable.avatar)).into(iv_image);
    }

    @Override
    public void displayImagePreview(Uri mFileUri) {
        Glide.with(CameraTwoActivity.this).load(mFileUri).apply(new RequestOptions().centerCrop().placeholder(R.drawable.avatar)).into(iv_image);
    }

    @Override
    public String getRealPathFromUri(Uri contentUri) {
        return null;
    }

    private void setViewVisibility(int id, int visibility) {
        View view = findViewById(id);
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), photoURI);
                    rotateImage(setReducedImageSize());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else {
            onBackPressed();
        }
    }

    private Bitmap setReducedImageSize(){
        int targetW = iv_image.getWidth();
        int targetH = iv_image.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pictureFile.toString(),bmOptions);
        int cameraImageWidth = bmOptions.outWidth;
        int cameraImageHeight = bmOptions.outHeight;

        int scaleFactor = Math.min(cameraImageWidth/targetW,cameraImageHeight/targetH);
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(pictureFile.toString(),bmOptions);
    }

    private void rotateImage(Bitmap bitmap){
        try {
            exifInterface = new ExifInterface(pictureFile.toString());
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation){
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(270);
                    break;
                default:
            }

            rotatedBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);

            iv_image.setImageBitmap(rotatedBitmap);

            setViewVisibility(R.id.toolbar_save_camera, View.VISIBLE);

        }catch (IOException e){
            e.printStackTrace();
        }

    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<StickerModel> stickerArrayList) {
        if (stickerArrayList != null && stickerArrayList.size() != 0){
            stickerAdapter = new StickerAdapter(this, stickerArrayList, languageLocale.getLanguage(), this);
            recyclerTitleSticker.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            recyclerTitleSticker.setAdapter(stickerAdapter);
        }else {

        }
    }

    @Override
    public void onClickedSticker(StickerModel sticker) {
        String url = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Sticker/"+sticker.getStickerId();
        new StickerPathsAsyncTask(this).execute(url);

    }

    @Override
    public void onRequestCompleteListenerStickerPaths(ArrayList<StickerPathsModel> stickerPathsArrayList) {
        if (stickerPathsArrayList != null && stickerPathsArrayList.size() != 0){

            stickerImageAdapter = new StickerImageAdapter(this, stickerPathsArrayList, languageLocale.getLanguage(), this);
            recyclerItemsSticker.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            recyclerItemsSticker.setAdapter(stickerImageAdapter);
            setViewVisibility(R.id.recycler_items_sticker, View.VISIBLE);
        }
    }

    @Override
    public void onClickedStickerImage(StickerPathsModel sticker) {
        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile_Sticker/"+sticker.getStickerPaths();
        String testPath = "https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png";

        int resId = getResourceByFilename(this, sticker.getStickerPaths());
        Bitmap imageBitmap = BitmapFactory.decodeResource(this.getResources(), resId);

        StickerImageView iv_sticker = new StickerImageView(this);
        ImageView img = (ImageView) findViewById(R.id.iv_sticker);
        img.setImageBitmap(imageBitmap);


        Glide.with(this)
                .asBitmap()
                .load(paths)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        img.setImageBitmap(resource);
                        iv_sticker.setImageDrawable(((ImageView) findViewById(R.id.iv_sticker)).getDrawable());
                        iv_sticker.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });



        mFrameLayout.addView(iv_sticker);
        listSticker.add(iv_sticker);

    }

    public static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void saveAsSticker() {

        for (StickerImageView sk : listSticker){
            sk.setControlItemsHidden(true);
        }
        listSticker.clear();

        mFrameLayout.setDrawingCacheEnabled(true);
        mFrameLayout.buildDrawingCache();
        Bitmap cache = mFrameLayout.getDrawingCache();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(pictureFile);
            cache.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            galleryAddPic(pictureFile);
            getBitmapSetShow(pictureFile);
            Log.d(TAG, "save sticker path : " + pictureFile);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mFrameLayout.destroyDrawingCache();
        }

    }

    private void galleryAddPic(File currentPhotoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(currentPhotoPath);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
/*        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.fromFile(currentPhotoPath));
            sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(currentPhotoPath.getAbsolutePath())));
        }*/

    }
    private void sharePhotoToSocial(){
        saveAsSticker();
        Uri imageUri = getUriForFile(CameraTwoActivity.this, BuildConfig.APPLICATION_ID + ".provider", pFile);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        shareIntent.setType("image/jpeg");
        if (items != null) {
            String topic = "";
            switch (languageLocale.getLanguage()){
                case "th":
                    topic = items.getTopicTH();
                    break;
                case "en":
                    topic = items.getTopicEN();
                    break;
                case "lo":
                    topic = items.getTopicLO();
                    break;
                case "zh":
                    topic = items.getTopicZH();
                    break;
            }
            startActivity(Intent.createChooser(shareIntent, topic));
        }
    }

    public void getBitmapSetShow(File pictureFile) {
        Bitmap bitmap = null;
        try {
            File f = new File(pictureFile.toString());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            iv_previews.setImageBitmap(bitmap);

            if (items != null) {
                String topic = "";
                switch (languageLocale.getLanguage()){
                    case "th":
                        topic = items.getTopicTH();
                        break;
                    case "en":
                        topic = items.getTopicEN();
                        break;
                    case "lo":
                        topic = items.getTopicLO();
                        break;
                    case "zh":
                        topic = items.getTopicZH();
                        break;
                }
                txt_previews.setText(topic);
            }

            setViewVisibility(R.id.camera_preview_two, View.GONE);
            setViewVisibility(R.id.toolbar_control_sticker, View.GONE);
            setViewVisibility(R.id.recycler_title_sticker, View.GONE);
            setViewVisibility(R.id.toolbar_back_camera, View.VISIBLE);
            setViewVisibility(R.id.con_iv_previews, View.VISIBLE);
            setViewVisibility(R.id.txt_title_back_camera, View.VISIBLE);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}