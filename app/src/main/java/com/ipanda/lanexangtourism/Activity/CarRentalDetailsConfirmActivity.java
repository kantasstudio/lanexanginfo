package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.DialogFragment.ConfirmOrdersDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.post_booking.HttpPostCarRental;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;

public class CarRentalDetailsConfirmActivity extends AppCompatActivity {

    //variables
    private static final String TAG = CarRentalDetailsConfirmActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private ItemsModel items = new ItemsModel();

    private BookingModel bookingModel = new BookingModel();

    private String startDate, endDate;

    private String startDay, startMonth, startTime;

    private String endDay, endMonth, endTime;

    private TextView txt_scan_qr_code;

    private String fistName, lastName, email, phone, weChat, whatsApp, period;

    private String startYears, endYears;

    private Button btn_confirm;

    private TextView btn_cancel;

    private TextView txt_exchange_rate_totals_id;

    private String userId;

    private RateModel rateModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_rental_details_confirm);

        //views
        mToolbar = findViewById(R.id.toolbar);
        btn_confirm = findViewById(R.id.btn_confirm);
        txt_exchange_rate_totals_id = findViewById(R.id.txt_exchange_rate_totals_id);
        btn_cancel = findViewById(R.id.btn_cancel);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        String url ="";
        if (getIntent() != null) {
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            bookingModel = getIntent().getParcelableExtra("BOOKING");
            startDate = getIntent().getStringExtra("START_DATE");
            startDay = getIntent().getStringExtra("START_DAY");
            startMonth = getIntent().getStringExtra("START_MONTH");
            startTime = getIntent().getStringExtra("START_TIME");
            startYears = getIntent().getStringExtra("START_YEAR");
            endDate = getIntent().getStringExtra("END_DATE");
            endDay = getIntent().getStringExtra("END_DAY");
            endMonth = getIntent().getStringExtra("END_MONTH");
            endTime = getIntent().getStringExtra("END_TIME");
            endYears = getIntent().getStringExtra("END_YEAR");
            fistName = getIntent().getStringExtra("FIST_NAME");
            lastName = getIntent().getStringExtra("LAST_NAME");
            email = getIntent().getStringExtra("EMAIL");
            phone = getIntent().getStringExtra("PHONE");
            weChat = getIntent().getStringExtra("WC_CHAT");
            whatsApp = getIntent().getStringExtra("WHATS_APP");
            period = getIntent().getStringExtra("PERIOD");
            setUpTitle(items);

        }

        //set event onClick
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupHttpPost();
            }
        });

    }
    private void setupHttpPost() {
        if (userId != null && items != null){
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Booking");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("Items_items_id", String.valueOf(items.getItemsId()));
            paramsPost.put("paymentTransaction_firstName", fistName);
            paramsPost.put("paymentTransaction_lastName", lastName);
            paramsPost.put("paymentTransaction_email", email);
            paramsPost.put("paymentTransaction_phone", phone);
            paramsPost.put("paymentTransaction_weChatId", weChat);
            paramsPost.put("paymentTransaction_whatsAppId", whatsApp);
            paramsPost.put("booking_checkIn", bookingModel.getCheckIn());
            paramsPost.put("booking_checkOut", bookingModel.getCheckOut());
            paramsPost.put("booking_checkInTime", bookingModel.getScheduleCheckIn());
            paramsPost.put("booking_checkOutTime", bookingModel.getScheduleCheckOut());
            httpCallPost.setParams(paramsPost);
            new HttpPostCarRental().execute(httpCallPost);
            Bundle bundle = new Bundle();
            bundle.putString("TITLE","กำลังทำการส่งข้อมูลเพิ่อจองรถเช่า");
            bundle.putString("DETAILS","กรุณารอสักครู่ระบบกำส่งข้อมูลเบื่องต้นไปยังพนักงานของการจองรถเช่าเมื่อพนักงาน ได้รับข้อมูลขอท่านแล้ว จะมีเจ้าหน้าที่ติดต่อกลับ");
            if (items.getBusinessModel() != null) {
                bundle.putString("PHONE", items.getBusinessModel().getPhone());
            }else {
                bundle.putString("PHONE", "-");
            }
            DialogFragment dialogFragment = ConfirmOrdersDialogFragment.newInstance();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getSupportFragmentManager(),TAG);
        }
    }

    private void setUpTitle(ItemsModel items) {
        ImageView img = findViewById(R.id.img_items);
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        TextView txt_name_car = findViewById(R.id.txt_name_car);
        TextView txt_type_car = findViewById(R.id.txt_type_car);
        TextView txt_transmission_car = findViewById(R.id.txt_transmission_car);
        TextView txt_passenger = findViewById(R.id.txt_passenger);
        TextView txt_price_car = findViewById(R.id.txt_price_car);
        TextView txt_company_name = findViewById(R.id.txt_company_name);
        TextView txt_delivery_fee = findViewById(R.id.txt_delivery_fee);
        TextView txt_pick_up_fee = findViewById(R.id.txt_pick_up_fee);
        TextView txt_car_rental_fee_per_day = findViewById(R.id.txt_car_rental_fee_per_day);
        TextView txt_car_delivery_fee = findViewById(R.id.txt_car_delivery_fee);
        TextView txt_grand_total = findViewById(R.id.txt_grand_total);
        TextView txt_date_and_time = findViewById(R.id.txt_date_and_time);
        TextView booking_detail_name = findViewById(R.id.booking_detail_name);
        TextView booking_detail_email = findViewById(R.id.booking_detail_email);
        TextView booking_detail_phone = findViewById(R.id.booking_detail_phone);
        TextView booking_detail_discount = findViewById(R.id.booking_detail_discount);

        TextView txt_delivery_fee_ex = findViewById(R.id.txt_delivery_fee_ex);
        TextView txt_pick_up_fee_ex = findViewById(R.id.txt_pick_up_fee_ex);
        TextView txt_car_rental_fee_per_day_ex = findViewById(R.id.txt_car_rental_fee_per_day_ex);
        TextView txt_exchange_rate_title_id = findViewById(R.id.txt_exchange_rate_title_id);


        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+items.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img);

        txt_passenger.setText(getResources().getString(R.string.text_passenger)+" : "+items.getCarModel().getSeats()+"");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = decimalFormat.format(items.getCarModel().getTotals(Integer.parseInt(startDate),Integer.parseInt(endDate)));


        ExchangeRate exchangeRate = new ExchangeRate();
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    txt_exchange_rate_totals_id.setText("THB");
                    txt_exchange_rate_title_id.setText(currency);
                    txt_delivery_fee_ex.setText(currency);
                    txt_pick_up_fee_ex.setText(currency);
                    txt_car_rental_fee_per_day_ex.setText(currency);
                    txt_price_car.setText(items.getCarModel().getPricePerDay()+"");
                    txt_delivery_fee.setText(items.getCarModel().getReturnPrice()+"");
                    txt_pick_up_fee.setText(items.getCarModel().getPickUpPrice()+"");
                    txt_car_rental_fee_per_day.setText(items.getCarModel().getPricePerDay()+"");
                    txt_grand_total.setText(price);
                    break;
                case "USD":
                    txt_exchange_rate_totals_id.setText("USD");
                    txt_exchange_rate_title_id.setText(currency);
                    txt_delivery_fee_ex.setText(currency);
                    txt_pick_up_fee_ex.setText(currency);
                    txt_car_rental_fee_per_day_ex.setText(currency);
                    txt_price_car.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateUSD())));
                    txt_delivery_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getReturnPrice(),rateModel.getRateUSD())));
                    txt_pick_up_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPickUpPrice(),rateModel.getRateUSD())));
                    txt_car_rental_fee_per_day.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateUSD())));
                    txt_grand_total.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getTotals(Integer.parseInt(startDate),Integer.parseInt(endDate)),rateModel.getRateUSD())));
                    break;
                case "CNY":
                    txt_exchange_rate_totals_id.setText("CNY");
                    txt_exchange_rate_title_id.setText(currency);
                    txt_delivery_fee_ex.setText(currency);
                    txt_pick_up_fee_ex.setText(currency);
                    txt_car_rental_fee_per_day_ex.setText(currency);
                    txt_price_car.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateCNY())));
                    txt_delivery_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getReturnPrice(),rateModel.getRateCNY())));
                    txt_pick_up_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPickUpPrice(),rateModel.getRateCNY())));
                    txt_car_rental_fee_per_day.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateCNY())));
                    txt_grand_total.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getTotals(Integer.parseInt(startDate),Integer.parseInt(endDate)),rateModel.getRateCNY())));
                    break;
            }
        }else {
            txt_exchange_rate_totals_id.setText("THB");
            txt_price_car.setText(items.getCarModel().getPricePerDay()+"");
            txt_delivery_fee.setText(items.getCarModel().getReturnPrice()+"");
            txt_pick_up_fee.setText(items.getCarModel().getPickUpPrice()+"");
            txt_car_rental_fee_per_day.setText(items.getCarModel().getPricePerDay()+"");
            txt_grand_total.setText(price);
        }




        txt_date_and_time.setText(startDay+" "+startDate+" "+startMonth+" - "+endDay+" "+endDate+" "+endMonth+" "+endYears);
        booking_detail_name.setText(fistName+" "+lastName);
        booking_detail_email.setText(email);
        booking_detail_phone.setText(phone+"");
        booking_detail_discount.setText("");
        switch (languageLocale.getLanguage()){
            case "th":
                toolbar_title.setText(items.getTopicTH());
                txt_name_car.setText(items.getTopicTH());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeTH());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemTH());
                txt_car_delivery_fee.setText("ฟรี!");
                break;
            case "en":
                toolbar_title.setText(items.getTopicEN());
                txt_name_car.setText(items.getTopicEN());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeEN());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemEN());
                txt_car_delivery_fee.setText("Not free!");
                break;
            case "lo":
                toolbar_title.setText(items.getTopicLO());
                txt_name_car.setText(items.getTopicLO());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeLO());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemLO());
                txt_car_delivery_fee.setText("ບໍ່ເສຍຄ່າ!");
                break;
            case "zh":
                toolbar_title.setText(items.getTopicZH());
                txt_name_car.setText(items.getTopicZH());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeZH());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemZH());
                txt_car_delivery_fee.setText("不免费!");
                break;
        }

        if (items.getBusinessModel() != null){
            switch (languageLocale.getLanguage()){
                case "th":
                    txt_company_name.setText(items.getBusinessModel().getNameTH());
                    break;
                case "en":
                    txt_company_name.setText(items.getBusinessModel().getNameEN());
                    break;
                case "lo":
                    txt_company_name.setText(items.getBusinessModel().getNameLO());
                    break;
                case "zh":
                    txt_company_name.setText(items.getBusinessModel().getNameZH());
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
