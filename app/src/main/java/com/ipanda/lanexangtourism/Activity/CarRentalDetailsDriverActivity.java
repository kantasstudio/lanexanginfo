package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;

public class CarRentalDetailsDriverActivity extends AppCompatActivity implements View.OnClickListener {

    //variables
    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private CardView cardView_verify;

    private ItemsModel items = new ItemsModel();

    private BookingModel bookingModel = new BookingModel();

    private String userId;

    private String startDate, startDay, startMonth, startTime;
    private String endDate, endDay, endMonth, endTime;

    private TextView txt_scan_qr_code;

    private String fistName, lastName, email, phone, weChat, whatsApp, period;

    private EditText edt_input_fistName, edt_input_lastName, edt_input_email, edt_input_phone, edt_input_weChat, edt_input_whatsApp, edt_input_discount;

    private String startYears, endYears;

    private RateModel rateModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_rental_details_driver);


        //views
        mToolbar = findViewById(R.id.toolbar);
        cardView_verify = findViewById(R.id.cardView_verify);
        txt_scan_qr_code = findViewById(R.id.txt_scan_qr_code);
        edt_input_fistName = findViewById(R.id.edt_input_fistName);
        edt_input_lastName = findViewById(R.id.edt_input_lastName);
        edt_input_email = findViewById(R.id.edt_input_email);
        edt_input_phone = findViewById(R.id.edt_input_phone);
        edt_input_weChat = findViewById(R.id.edt_input_weChat);
        edt_input_whatsApp = findViewById(R.id.edt_input_whatsApp);
        edt_input_discount = findViewById(R.id.edt_input_discount);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //set event onclick
        cardView_verify.setOnClickListener(this);

        String url ="";
        if (getIntent() != null) {
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            bookingModel = getIntent().getParcelableExtra("BOOKING");
            startDate = getIntent().getStringExtra("START_DATE");
            startDay = getIntent().getStringExtra("START_DAY");
            startMonth = getIntent().getStringExtra("START_MONTH");
            startTime = getIntent().getStringExtra("START_TIME");
            startYears = getIntent().getStringExtra("START_YEAR");
            endDate = getIntent().getStringExtra("END_DATE");
            endDay = getIntent().getStringExtra("END_DAY");
            endMonth = getIntent().getStringExtra("END_MONTH");
            endTime = getIntent().getStringExtra("END_TIME");
            endYears = getIntent().getStringExtra("END_YEAR");
        }

        txt_scan_qr_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CarRentalDetailsDriverActivity.this, ScanQRCodeActivity.class));
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cardView_verify:
                Intent intent = new Intent(this, CarRentalDetailsConfirmActivity.class);
                if (getTextBookingDetail()) {
                    intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
                    intent.putExtra("ITEMS_MODEL", items);
                    intent.putExtra("BOOKING", bookingModel);
                    intent.putExtra("START_DATE", startDate);
                    intent.putExtra("START_DAY", startDay);
                    intent.putExtra("START_MONTH", startMonth);
                    intent.putExtra("START_TIME", startTime);
                    intent.putExtra("START_YEAR", startYears);
                    intent.putExtra("END_DATE", endDate);
                    intent.putExtra("END_DAY", endDay);
                    intent.putExtra("END_MONTH", endMonth);
                    intent.putExtra("END_TIME", endTime);
                    intent.putExtra("END_YEAR", endYears);
                    intent.putExtra("FIST_NAME", fistName);
                    intent.putExtra("LAST_NAME", lastName);
                    intent.putExtra("EMAIL", email);
                    intent.putExtra("PHONE", phone);
                    intent.putExtra("WC_CHAT", weChat);
                    intent.putExtra("WHATS_APP", whatsApp);
                    intent.putExtra("PERIOD", period);
                    startActivity(intent);
                }
                break;
        }
    }

    private boolean getTextBookingDetail() {
        if (TextUtils.isEmpty(edt_input_fistName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Fist Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            fistName = edt_input_fistName.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(edt_input_lastName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Last Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            lastName = edt_input_lastName.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(edt_input_email.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Email", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            email = edt_input_email.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(edt_input_phone.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, phone", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            phone = edt_input_phone.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(edt_input_weChat.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, WeChat", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            weChat = edt_input_weChat.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(edt_input_whatsApp.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, WhatsApp", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            whatsApp = edt_input_whatsApp.getText().toString().replaceAll(" ", "");
        }
        return true;
    }
}
