package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.widget.NestedScrollView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ipanda.lanexangtourism.Adapter.ChatMessageAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.GetChatRoomAsyncTask;
import com.ipanda.lanexangtourism.asynctask.MessageAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.GetChatRoomCallBack;
import com.ipanda.lanexangtourism.interface_callback.MessageCallBack;
import com.ipanda.lanexangtourism.interface_callback.MessageReceivedCallBack;
import com.ipanda.lanexangtourism.interface_callback.MessageSendCallBack;
import com.ipanda.lanexangtourism.message.Config;
import com.ipanda.lanexangtourism.message.MyFirebaseInstanceIDService;
import com.ipanda.lanexangtourism.message.MyFirebaseMessagingService;
import com.ipanda.lanexangtourism.message.MyWorker;
import com.ipanda.lanexangtourism.post_booking.HttpPostMessageRequest;

import java.util.ArrayList;
import java.util.HashMap;

public class ChatMessageActivity extends AppCompatActivity implements View.OnClickListener , MessageCallBack , MessageSendCallBack, GetChatRoomCallBack {

    //variables
    private static String TAG = OrderDetailsActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private TextView toolbar_title;

    private ChangeLanguageLocale languageLocale;

    private EditText edtMessage;

    private ImageView imgSendMessage;

    private NestedScrollView nestedScrollView;

    private RecyclerView recyclerMessage;

    private ChatMessageAdapter messageAdapter;

    private String userId;

    private String urlChatMessages;

    private String urlGetRoomId;

    private String chatRoomId = null;

    private ArrayList<MessageModel> newMessageArrayList;

    private String sendToUserId;

    private boolean sendDialog;

    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_message);
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.message_toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        edtMessage = findViewById(R.id.edt_message_id);
        imgSendMessage = findViewById(R.id.img_send_message_id);
        recyclerMessage = findViewById(R.id.recycler_chat_message);
        nestedScrollView = findViewById(R.id.nestedScrollView);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("BackPressed","BackPressed");
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //get intent
        if (getIntent() != null){
            sendToUserId = getIntent().getStringExtra("Send_To_UserId");
            toolbar_title.setText(getIntent().getStringExtra("Send_To_Name"));
            chatRoomId = getIntent().getStringExtra("Chat_Room_Id");
            sendDialog = getIntent().getBooleanExtra("Send_Dialog", false);
            urlGetRoomId = getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/getChatRoom?from_user_id="+userId+"&to_user_id="+sendToUserId;
            new GetChatRoomAsyncTask(this).execute(urlGetRoomId);
        }

        newMessageArrayList = new ArrayList<>();

        // set event onClick
        imgSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupPostMessage();
            }
        });


        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    GetChatMessage();
                }
            }
        };

    }

    private void GetChatMessage() {
        urlChatMessages = getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/getChatMessages/" + chatRoomId;
        new MessageAsyncTask(ChatMessageActivity.this).execute(urlChatMessages);
    }


    private void setupPostMessage(){
        String edtGetMessage = edtMessage.getText().toString();
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/sendingMessage");
        HashMap<String, String> paramsPost = new HashMap<>();
        paramsPost.put("to_user_id", String.valueOf(sendToUserId));
        paramsPost.put("from_user_id", userId);
        paramsPost.put("Items_items_id", "-1");
        paramsPost.put("message", edtGetMessage);
        httpCallPost.setParams(paramsPost);
        new HttpPostMessageRequest(this).execute(httpCallPost);
        edtMessage.setText("");
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerSendMessage(ArrayList<MessageModel> messageArrayList) {
        if (messageArrayList != null && messageArrayList.size() != 0) {
            GetChatMessage();
        }
    }

    @Override
    public void onRequestCompleteListenerChatRoom(String roomId) {
        if (roomId != null && !roomId.equals("")){
            this.chatRoomId = roomId;
            GetChatMessage();
        }
    }

    @Override
    public void onRequestCompleteListener(ArrayList<MessageModel> messageArrayList) {
        if (messageArrayList != null && messageArrayList.size() != 0) {
            this.newMessageArrayList.clear();
            this.newMessageArrayList = messageArrayList;
            setMessageToAdapter();
        }
    }

    private void setMessageToAdapter(){
        recyclerMessage.setHasFixedSize(true);
        messageAdapter = new ChatMessageAdapter(this, newMessageArrayList, languageLocale.getLanguage(), Integer.parseInt(userId), "Person");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerMessage.setLayoutManager(linearLayoutManager);
        recyclerMessage.setItemAnimator(new DefaultItemAnimator());
        recyclerMessage.setAdapter(messageAdapter);
    }

    @Override
    public void onRequestFailed(String result) {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }
}
