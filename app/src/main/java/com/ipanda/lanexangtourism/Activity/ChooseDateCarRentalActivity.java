package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.archit.calendardaterangepicker.customviews.CalendarListener;
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.items_view.ViewCarRentalActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ChooseDateCarRentalActivity extends AppCompatActivity {

    //Variables
    private static final String TAG = ChooseDateCarRentalActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private DateRangeCalendarView calendar;

    private ItemsModel items;

    private BookingModel bookingModel = new BookingModel();

    private TextView btnFinish;

    private TimePicker timePicker;

    private TextView txt_the_date_pick_up, txt_month_pick_up, txt_day_pick_up,txt_time_pick_up;

    private TextView txt_the_date_return, txt_month_return, txt_day_return, txt_time_return;

    private String hourAndMinute = null, startYears, endYears;

    private RateModel rateModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_date_car_rental);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.toolbar);
        btnFinish = findViewById(R.id.txt_finish);
        txt_the_date_pick_up = findViewById(R.id.txt_the_date_pick_up);
        txt_month_pick_up = findViewById(R.id.txt_month_pick_up);
        txt_day_pick_up = findViewById(R.id.txt_day_pick_up);
        txt_time_pick_up = findViewById(R.id.txt_time_pick_up);
        txt_the_date_return = findViewById(R.id.txt_the_date_return);
        txt_month_return = findViewById(R.id.txt_month_return);
        txt_day_return = findViewById(R.id.txt_day_return);
        txt_time_return = findViewById(R.id.txt_time_return);
        timePicker = findViewById(R.id.time_picker);


        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null) {
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
        }

        //setup Calendar
        setUpCalendar();

        //setup TimePicker
        setUpTimePicker();

        //setup current date
        getCurrentDate();


        //set event onClick
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChooseDateCarRentalActivity.this, ViewCarRentalActivity.class);


                String date_pick_up = txt_the_date_pick_up.getText().toString();
                String day_pick_up = txt_day_pick_up.getText().toString();
                String month_pick_up = txt_month_pick_up.getText().toString();
                String time_pick_up = txt_time_pick_up.getText().toString();
                String date_return = txt_the_date_return.getText().toString();
                String day_return = txt_day_return.getText().toString();
                String month_return = txt_month_return.getText().toString();
                String time_return = txt_time_return.getText().toString();

                intent.putExtra("ITEMS_MODEL", items);
                intent.putExtra("BOOKING", bookingModel);
                intent.putExtra("START_DATE", date_pick_up);
                intent.putExtra("START_DAY", day_pick_up);
                intent.putExtra("START_MONTH", month_pick_up);
                intent.putExtra("START_TIME", time_pick_up);
                intent.putExtra("START_YEAR", startYears);
                intent.putExtra("END_DATE", date_return);
                intent.putExtra("END_DAY", day_return);
                intent.putExtra("END_MONTH", month_return);
                intent.putExtra("END_TIME", time_return);
                intent.putExtra("END_YEAR", endYears);
                intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
                startActivity(intent);

            }
        });

    }

    private void setUpCalendar() {
        calendar = findViewById(R.id.cdrvCalendar);
        calendar.setNavLeftImage(ContextCompat.getDrawable(this,R.drawable.ic_left));
        calendar.setNavRightImage(ContextCompat.getDrawable(this,R.drawable.ic_right));

        Calendar current = Calendar.getInstance();
        calendar.setCurrentMonth(current);
        calendar.setSelectedDateRange(current, current);

        int day = current.get(Calendar.DAY_OF_MONTH);
        int month = (current.get(Calendar.MONTH)+1);
        int year = current.get(Calendar.YEAR);
        bookingModel.setCheckIn(day+"-"+month+"-"+year);
        bookingModel.setCheckOut(day+"-"+month+"-"+year);

        calendar.setCalendarListener(new CalendarListener() {
            @Override
            public void onFirstDateSelected(Calendar startDate) {
                String[] subStart = startDate.getTime().toString().split(" ");
                int day = startDate.get(Calendar.DAY_OF_MONTH);
                int month = (startDate.get(Calendar.MONTH)+1);
                int year = startDate.get(Calendar.YEAR);
                txt_the_date_pick_up.setText(subStart[2]);
                txt_day_pick_up.setText(subStart[0]);
                txt_month_pick_up.setText(subStart[1]);
                startYears = subStart[5];
                bookingModel.setCheckIn(day+"-"+month+"-"+year);
                Toast.makeText(ChooseDateCarRentalActivity.this, "Start Date: " + subStart[2], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDateRangeSelected(Calendar startDate, Calendar endDate) {
                String[] subStart = startDate.getTime().toString().split(" ");
                String[] subEnd = endDate.getTime().toString().split(" ");
                int startDay = startDate.get(Calendar.DAY_OF_MONTH);
                int startMonth = (startDate.get(Calendar.MONTH)+1);
                int startYear = startDate.get(Calendar.YEAR);
                int endDay = endDate.get(Calendar.DAY_OF_MONTH);
                int endMonth = (endDate.get(Calendar.MONTH)+1);
                int endYear = endDate.get(Calendar.YEAR);
                txt_the_date_pick_up.setText(subStart[2]);
                txt_day_pick_up.setText(subStart[0]);
                txt_month_pick_up.setText(subStart[1]);
                startYears = subStart[5];
                txt_the_date_return.setText(subEnd[2]);
                txt_day_return.setText(subEnd[0]);
                txt_month_return.setText(subEnd[1]);
                endYears = subEnd[5];
                bookingModel.setCheckIn(startDay+"-"+startMonth+"-"+startYear);
                bookingModel.setCheckOut(endDay+"-"+endMonth+"-"+endYear);
                Toast.makeText(ChooseDateCarRentalActivity.this, "Start Date: "+subStart[2]+"  End Date: "+ subEnd[2], Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpTimePicker(){
        timePicker.setIs24HourView(true);
        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute();
        bookingModel.setScheduleCheckIn(String.format("%02d:%02d", hour, minute));
        bookingModel.setScheduleCheckOut(String.format("%02d:%02d", hour, minute));
        txt_time_pick_up.setText(String.format("%02d:%02d", hour, minute)+" น.");
        txt_time_return.setText(String.format("%02d:%02d", hour, minute)+" น.");
        // perform set on time changed listener event
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                // display a toast with changed values of time picker
                hourAndMinute = String.format("%02d:%02d", hourOfDay, minute);
                txt_time_pick_up.setText(hourAndMinute+" น.");
                txt_time_return.setText(hourAndMinute+" น.");
                bookingModel.setScheduleCheckIn(hourAndMinute);
                bookingModel.setScheduleCheckOut(hourAndMinute);
//                Toast.makeText(getApplicationContext(), hourOfDay + " " + minute, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCurrentDate(){
        String pattern = "E dd MMM yyyy HH:mm";
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String mDate = simpleDateFormat.format(date);
        String[] subString = mDate.split(" ");
        String subDay = subString[0];
        String subDate = subString[1];
        String subMonth = subString[2];
        String subYear = subString[3];
        String subTime = subString[4];

        txt_the_date_pick_up.setText(subDate);
        txt_day_pick_up.setText(subDay);
        txt_month_pick_up.setText(subMonth);

        txt_the_date_return.setText(subDate);
        txt_day_return.setText(subDay);
        txt_month_return.setText(subMonth);
        startYears = subYear;
        endYears = subYear;
    }

    private boolean isCheckDateTime(){
        if (hourAndMinute != null){
            return true;
        }else {
            return false;
        }
    }


}
