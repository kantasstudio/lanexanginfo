package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.archit.calendardaterangepicker.customviews.CalendarListener;
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ChooseDateHotelMainActivity extends AppCompatActivity {

    //variables
    private static String TAG = ChooseDateHotelMainActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ImageView img_dismiss_hotel;

    private TextView toolbar_title, toolbar_title_right;

    private TextView txt_check_in_day, txt_check_in_date, txt_check_in_month;

    private TextView txt_check_out_day, txt_check_out_date, txt_check_out_month;

    private String sDate = null, eDate = null ,showDate = null, showMonth = null;

    private boolean isStartDate = false, isEndDate = false;

    private DateRangeCalendarView cdrvCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_date_hotel_main);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        // Views
        img_dismiss_hotel = findViewById(R.id.img_dismiss_hotel);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title_right = findViewById(R.id.toolbar_title_right);
        txt_check_in_day = findViewById(R.id.txt_check_in_day);
        txt_check_in_date = findViewById(R.id.txt_check_in_date);
        txt_check_in_month = findViewById(R.id.txt_check_in_month);
        txt_check_out_day = findViewById(R.id.txt_check_out_day);
        txt_check_out_date = findViewById(R.id.txt_check_out_date);
        txt_check_out_month = findViewById(R.id.txt_check_out_month);
        cdrvCalendar = findViewById(R.id.cdrvCalendar);

        //setup calendar
        setUpCalendar();
        getCurrentDate();

        //set event onClick
        img_dismiss_hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        toolbar_title_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStartDate && isEndDate && sDate != null && eDate != null && showDate != null && showMonth != null){
                    Intent intent = new Intent();
                    intent.putExtra("CHOOSE_START_DATE", sDate);
                    intent.putExtra("CHOOSE_END_DATE", eDate);
                    intent.putExtra("CHOOSE_SHOW_DATE", showDate);
                    intent.putExtra("CHOOSE_SHOW_MONTH", showMonth);
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }else {
                    Toast.makeText(ChooseDateHotelMainActivity.this, "Please select Date.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void setUpCalendar() {
        cdrvCalendar.setNavLeftImage(ContextCompat.getDrawable(this,R.drawable.ic_left));
        cdrvCalendar.setNavRightImage(ContextCompat.getDrawable(this,R.drawable.ic_right));
        Calendar current = Calendar.getInstance();
        cdrvCalendar.setCurrentMonth(current);
        cdrvCalendar.setSelectedDateRange(current, current);
        cdrvCalendar.setCalendarListener(new CalendarListener() {
            @Override
            public void onFirstDateSelected(@NotNull Calendar startDate) {
                isStartDate = false;
                isEndDate = false;
            }

            @Override
            public void onDateRangeSelected(@NotNull Calendar startDate, @NotNull Calendar endDate) {
                String[] subStart = startDate.getTime().toString().split(" ");
                String[] subEnd = endDate.getTime().toString().split(" ");
                isStartDate = true;
                isEndDate = true;
                int startDay = startDate.get(Calendar.DAY_OF_MONTH);
                int startMonth = (startDate.get(Calendar.MONTH)+1);
                int startYear = startDate.get(Calendar.YEAR);
                int endDay = endDate.get(Calendar.DAY_OF_MONTH);
                int endMonth = (endDate.get(Calendar.MONTH)+1);
                int endYear = endDate.get(Calendar.YEAR);
                txt_check_in_day.setText(subStart[2]);
                txt_check_in_date.setText(subStart[0]);
                txt_check_in_month.setText(subStart[1]);
                txt_check_out_day.setText(subEnd[2]);
                txt_check_out_date.setText(subEnd[0]);
                txt_check_out_month.setText(subEnd[1]);
                sDate = startDay+"-"+startMonth+"-"+startYear;
                eDate = endDay+"-"+endMonth+"-"+endYear;
                showDate = subStart[2]+"-"+subEnd[2]+" "+subStart[1];
                showMonth = subStart[2]+"-"+subStart[1]+"-"+startYear+" - "+subEnd[2]+"-"+subEnd[1]+"-"+endYear;
            }
        });
    }


    private void getCurrentDate(){
        String pattern = "EEEE dd MMM yyyy HH:mm";
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String mDate = simpleDateFormat.format(date);
        String[] subString = mDate.split(" ");
        String subDate = subString[0];
        String subDay = subString[1];
        String subMonth = subString[2];
        String subYear = subString[3];
        String subTime = subString[4];
        txt_check_in_day.setText(subDay);
        txt_check_in_date.setText(subDate);
        txt_check_in_month.setText(subMonth);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}