package com.ipanda.lanexangtourism.Activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Adapter.ScheduleTimeRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ScheduleTimeClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.SimpleDateFormat;
import java.util.Date;


public class ChooseDateTimeActivity extends AppCompatActivity implements ScheduleTimeClickListener {

    //Variables
    private static final String TAG = ChooseDateTimeActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private CalendarView calendarView;

    private TextView confirm, txt_date_and_time,txt_date_schedule_and_time;

    private ConstraintLayout ex_con_datetime, ex_con_schedule;

    private ExpandableLayout expandable_calendar, expandable_schedule;

    private ImageView img_arrow_date_times, img_arrow_schedule_date_times;

    private RecyclerView mRecycler;

    private ScheduleTimeRecyclerViewAdapter mAdapter;

    private ItemsModel items = new ItemsModel();

    private String curDate, schedule = null, curMonth, curYear, curStart, curEnd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_date_time);

        //views
        confirm = findViewById(R.id.toolbar_title_right);
        txt_date_and_time = findViewById(R.id.txt_date_and_time);
        txt_date_schedule_and_time = findViewById(R.id.txt_date_schedule_and_time);
        ex_con_datetime = findViewById(R.id.ex_con_datetime);
        ex_con_schedule = findViewById(R.id.ex_con_schedule);
        calendarView = findViewById(R.id.calendarView);
        mRecycler = findViewById(R.id.recycler_schedule);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        if (getIntent() != null){
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            mAdapter = new ScheduleTimeRecyclerViewAdapter(this, items.getPeriodTimeModelArrayList(),this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            mRecycler.setLayoutManager(layoutManager);
            mRecycler.setAdapter(mAdapter);
        }

        getCurrentDate();

        //set on click event
        ex_con_datetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickExpandableDateTime();
            }
        });

        ex_con_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickExpandableSchedule();
            }
        });

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                curDate = String.valueOf(dayOfMonth);
                curMonth = String.valueOf(month+1);
                curYear = String.valueOf(year);
                setSelectedDate(dayOfMonth, month, year);
            }
        });
        String pattern = "dd MM yyyy";
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String mDate = simpleDateFormat.format(date);
        String[] subString = mDate.split(" ");
        curDate = subString[0];
        curMonth = subString[1];
        curYear = subString[2];

        calendarView.setMinDate(System.currentTimeMillis() - 1000);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("CURRENT_START",curStart);
                intent.putExtra("CURRENT_END",curEnd);
                intent.putExtra("CURRENT_DATE",curDate);
                intent.putExtra("CURRENT_MONTH",curMonth);
                intent.putExtra("CURRENT_YEAR",curYear);
                if (schedule != null) {
                    setResult(RESULT_OK, intent);
                    finish();
                }else {
                    Toast.makeText(ChooseDateTimeActivity.this, "Please select a schedule.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void clickExpandableDateTime(){
        expandable_calendar = findViewById(R.id.expandable_calendar);
        img_arrow_date_times = findViewById(R.id.img_arrow_date_times);
        expandable_calendar.toggle();
        if (expandable_calendar.isExpanded()){
            img_arrow_date_times.setImageResource(R.drawable.ic_arrow_up);
        }else {
            img_arrow_date_times.setImageResource(R.drawable.ic_arrow_right);
        }
    }

    private void clickExpandableSchedule(){
        expandable_schedule = findViewById(R.id.expandable_schedule);
        img_arrow_schedule_date_times = findViewById(R.id.img_arrow_schedule_date_times);

        expandable_schedule.toggle();
        if (expandable_schedule.isExpanded()){
            img_arrow_schedule_date_times.setImageResource(R.drawable.ic_arrow_up);
        }else {
            img_arrow_schedule_date_times.setImageResource(R.drawable.ic_arrow_right);
        }
    }


    @Override
    public void itemClicked(String timeStart, String timeEnd) {
        this.schedule = timeStart+" - "+timeEnd;
        this.curStart = timeStart;
        this.curEnd = timeEnd;
        Toast.makeText(this, schedule+"", Toast.LENGTH_SHORT).show();
    }

    private void getCurrentDate(){
        String pattern = "EEE dd MMM yyyy HH:mm";
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String mDate = simpleDateFormat.format(date);
        String[] subString = mDate.split(" ");
        String subDay = subString[0];
        String subDate = subString[1];
        String subMonth = subString[2];
        String subYear = subString[3];
        String subTime = subString[4];

        txt_date_and_time.setText(subDay+" "+subMonth+" "+subYear);
        txt_date_schedule_and_time.setText(subDay+" "+subMonth+" "+subYear);
    }

    private void setSelectedDate(int dayOfMonth, int month, int year){
        String pattern = "EEE dd MMM yyyy";
        Date date = new Date();
        date.setDate(dayOfMonth);
        date.setMonth(month);
        date.setYear(year-1900);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String mDate = simpleDateFormat.format(date);
        String[] subString = mDate.split(" ");
        String subDay = subString[0];
        String subDate = subString[1];
        String subMonth = subString[2];
        String subYear = subString[3];

        txt_date_and_time.setText(subDay+" "+subMonth+" "+subYear);
        txt_date_schedule_and_time.setText(subDay+" "+subMonth+" "+subYear);
    }
}
