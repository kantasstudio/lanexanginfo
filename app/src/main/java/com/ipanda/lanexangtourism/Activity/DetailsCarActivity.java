package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.ItemsCarRentalConditionPetrolAdapter;
import com.ipanda.lanexangtourism.Adapter.ItemsCarRentalDocumentAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class DetailsCarActivity extends AppCompatActivity {

    //variables
    private static final String TAG = DetailsCarActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private String userId;

    private ItemsModel items = new ItemsModel();

    private BookingModel booking = new BookingModel();

    private RateModel rateModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_car);
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //views
        mToolbar = findViewById(R.id.menu_toolbar);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null){
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            booking = getIntent().getParcelableExtra("BOOKING");
            items = booking.getItemsModel();
            setUpTitle(items);
//            setUpPrice(items);
            setUpPetrol(items);
            setUpDocument(items);
        }


        String url ="";
        if (userId != null && booking != null) {
            url = getString(R.string.app_api_ip)+ "dasta_thailand/api/mobile/user/Booking?user_id="+userId+"&payment_transaction_id=";
        }
    }

    private void setUpPrice(ItemsModel items) {
        TextView txt_delivery_fee = findViewById(R.id.txt_delivery_fee);
        TextView txt_pick_up_fee = findViewById(R.id.txt_pick_up_fee);
        TextView txt_car_rental_fee_per_day = findViewById(R.id.txt_car_rental_fee_per_day);
        TextView txt_car_delivery_fee = findViewById(R.id.txt_car_delivery_fee);

        txt_delivery_fee.setText(items.getCarModel().getReturnPrice()+"");
        txt_pick_up_fee.setText(items.getCarModel().getPickUpPrice()+"");
        txt_car_rental_fee_per_day.setText(items.getCarModel().getPricePerDay()+"");
        txt_car_delivery_fee.setText("ฟรี!");
    }

    private void setUpTitle(ItemsModel items) {
        ImageView img = findViewById(R.id.img_items);
        TextView txt_name_car = findViewById(R.id.txt_name_car);
        TextView txt_type_car = findViewById(R.id.txt_type_car);
        TextView txt_transmission_car = findViewById(R.id.txt_transmission_car);
        TextView txt_passenger = findViewById(R.id.txt_passenger);
        TextView txt_price_car = findViewById(R.id.txt_price_car);
        TextView txt_company_name = findViewById(R.id.txt_company_name);
        TextView txt_delivery_fee = findViewById(R.id.txt_delivery_fee);
        TextView txt_pick_up_fee = findViewById(R.id.txt_pick_up_fee);
        TextView txt_car_rental_fee_per_day = findViewById(R.id.txt_car_rental_fee_per_day);
        TextView txt_car_delivery_fee = findViewById(R.id.txt_car_delivery_fee);
        TextView txt_grand_total = findViewById(R.id.txt_grand_total);
        TextView txt_date_and_time = findViewById(R.id.txt_date_and_time);
        TextView booking_detail_name = findViewById(R.id.booking_detail_name);
        TextView booking_detail_email = findViewById(R.id.booking_detail_email);
        TextView booking_detail_phone = findViewById(R.id.booking_detail_phone);
        TextView booking_detail_discount = findViewById(R.id.booking_detail_discount);
        TextView txt_insurance_fee = findViewById(R.id.txt_insurance_fee);

        TextView txt_delivery_fee_ex = findViewById(R.id.txt_delivery_fee_ex);
        TextView txt_pick_up_fee_ex = findViewById(R.id.txt_pick_up_fee_ex);
        TextView txt_car_rental_fee_per_day_ex = findViewById(R.id.txt_car_rental_fee_per_day_ex);
        TextView txt_exchange_rate_title_id = findViewById(R.id.txt_exchange_rate_title_id);

        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+items.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img);
        txt_passenger.setText(getResources().getString(R.string.text_passenger)+" : "+items.getCarModel().getSeats()+"");

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = decimalFormat.format(booking.getPaymentTransactionModel().getTotal());
        String priceFee = decimalFormat.format(items.getCarModel().getDepositPrice());

        ExchangeRate exchangeRate = new ExchangeRate();
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    txt_exchange_rate_title_id.setText(currency);
                    txt_delivery_fee_ex.setText(currency);
                    txt_pick_up_fee_ex.setText(currency);
                    txt_car_rental_fee_per_day_ex.setText(currency);
                    txt_price_car.setText(items.getCarModel().getPricePerDay()+"");
                    txt_delivery_fee.setText(items.getCarModel().getReturnPrice()+"");
                    txt_pick_up_fee.setText(items.getCarModel().getPickUpPrice()+"");
                    txt_car_rental_fee_per_day.setText(items.getCarModel().getPricePerDay()+"");
                    txt_grand_total.setText(price);
                    txt_insurance_fee.setText(priceFee);
                    break;
                case "USD":
                    txt_exchange_rate_title_id.setText(currency);
                    txt_delivery_fee_ex.setText(currency);
                    txt_pick_up_fee_ex.setText(currency);
                    txt_car_rental_fee_per_day_ex.setText(currency);
                    txt_price_car.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateUSD())));
                    txt_delivery_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getReturnPrice(),rateModel.getRateUSD())));
                    txt_pick_up_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPickUpPrice(),rateModel.getRateUSD())));
                    txt_car_rental_fee_per_day.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateUSD())));
                    txt_grand_total.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(booking.getPaymentTransactionModel().getTotal(),rateModel.getRateUSD())));
                    txt_insurance_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getDepositPrice(),rateModel.getRateUSD())));
                    break;
                case "CNY":
                    txt_exchange_rate_title_id.setText(currency);
                    txt_delivery_fee_ex.setText(currency);
                    txt_pick_up_fee_ex.setText(currency);
                    txt_car_rental_fee_per_day_ex.setText(currency);
                    txt_price_car.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateCNY())));
                    txt_delivery_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getReturnPrice(),rateModel.getRateCNY())));
                    txt_pick_up_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPickUpPrice(),rateModel.getRateCNY())));
                    txt_car_rental_fee_per_day.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateCNY())));
                    txt_grand_total.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(booking.getPaymentTransactionModel().getTotal(),rateModel.getRateCNY())));
                    txt_insurance_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getDepositPrice(),rateModel.getRateCNY())));
                    break;
            }
        }else {
            txt_price_car.setText(items.getCarModel().getPricePerDay()+"");
            txt_delivery_fee.setText(items.getCarModel().getReturnPrice()+"");
            txt_pick_up_fee.setText(items.getCarModel().getPickUpPrice()+"");
            txt_car_rental_fee_per_day.setText(items.getCarModel().getPricePerDay()+"");
            txt_grand_total.setText(price);
            txt_insurance_fee.setText(priceFee);
        }

        txt_date_and_time.setText(booking.getCheckIn()+" - "+booking.getCheckOut());
        booking_detail_name.setText(booking.getPaymentTransactionModel().getFirstName()+" "+booking.getPaymentTransactionModel().getLastName());
        booking_detail_email.setText(booking.getPaymentTransactionModel().getEmail());
        booking_detail_phone.setText(booking.getPaymentTransactionModel().getPhone());
        booking_detail_discount.setText("");

        switch (languageLocale.getLanguage()){
            case "th":
                txt_name_car.setText(items.getTopicTH());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeTH());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemTH());
                txt_car_delivery_fee.setText("ฟรี!");
                break;
            case "en":
                txt_name_car.setText(items.getTopicEN());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeEN());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemEN());
                txt_car_delivery_fee.setText("Not free!");
                break;
            case "lo":
                txt_name_car.setText(items.getTopicLO());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeLO());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemLO());
                txt_car_delivery_fee.setText("ບໍ່ເສຍຄ່າ!");
                break;
            case "zh":
                txt_name_car.setText(items.getTopicZH());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeZH());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemZH());
                txt_car_delivery_fee.setText("不免费!");
                break;
        }

        if (items.getBusinessModel() != null){
            switch (languageLocale.getLanguage()){
                case "th":
                    txt_company_name.setText(items.getBusinessModel().getNameTH());
                    break;
                case "en":
                    txt_company_name.setText(items.getBusinessModel().getNameEN());
                    break;
                case "lo":
                    txt_company_name.setText(items.getBusinessModel().getNameLO());
                    break;
                case "zh":
                    txt_company_name.setText(items.getBusinessModel().getNameZH());
                    break;
            }
        }
    }

    private void setUpPetrol(ItemsModel items) {
        ArrayList<BookingConditionModel> arrayList = new ArrayList<>();
        for (BookingConditionModel booking : items.getBookingConditionModelArrayList()){
            if (booking.getConditionId() == 13){
                arrayList.add(booking);
            }
        }
        RecyclerView recycler_condition_petrol = findViewById(R.id.recycler_condition_petrol);
        ItemsCarRentalConditionPetrolAdapter adapter = new ItemsCarRentalConditionPetrolAdapter(this, arrayList, languageLocale.getLanguage());
        recycler_condition_petrol.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycler_condition_petrol.setAdapter(adapter);

    }

    private void setUpDocument(ItemsModel items) {
        ArrayList<BookingConditionModel> arrayList = new ArrayList<>();
        for (BookingConditionModel booking : items.getBookingConditionModelArrayList()){
            if (booking.getConditionId() == 14){
                arrayList.add(booking);
            }
        }
        RecyclerView  recycler_document = findViewById(R.id.recycler_document);
        ItemsCarRentalDocumentAdapter adapter = new ItemsCarRentalDocumentAdapter(this, arrayList, languageLocale.getLanguage());
        recycler_document.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycler_document.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}