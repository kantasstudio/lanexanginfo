package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class DetailsHotelActivity extends AppCompatActivity {

    //variables
    private static final String TAG = DetailsHotelActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private String userId;

    private ItemsModel items = new ItemsModel();

    private BookingModel booking = new BookingModel();

    private TextView booking_detail_name, booking_detail_email, booking_detail_phone, booking_detail_discount;
    private TextView txt_item_topic_hotel, txt_item_date_hotel;
    private ImageView img_ex_hotel_id;
    private TextView txt_ex_title_hotel_id,txt_ex_description_hotel_id,txt_ex_status_breakFast_id,txt_ex_price_hotel_id;
    private RelativeLayout relativeLayout_booking_exHotel_id;

    private TextView txt_exchange_rate_id;

    private RateModel rateModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_hotel);
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //views
        mToolbar = findViewById(R.id.menu_toolbar);
        img_ex_hotel_id = findViewById(R.id.img_ex_hotel_id);
        txt_item_topic_hotel = findViewById(R.id.txt_item_topic_hotel);
        txt_item_date_hotel = findViewById(R.id.txt_item_date_hotel);
        txt_ex_title_hotel_id = findViewById(R.id.txt_ex_title_hotel_id);
        txt_ex_description_hotel_id = findViewById(R.id.txt_ex_description_hotel_id);
        txt_ex_status_breakFast_id = findViewById(R.id.txt_ex_status_breakFast_id);
        txt_ex_price_hotel_id = findViewById(R.id.txt_ex_price_hotel_id);
        relativeLayout_booking_exHotel_id = findViewById(R.id.relativeLayout_booking_exHotel_id);
        booking_detail_name = findViewById(R.id.booking_detail_name);
        booking_detail_email = findViewById(R.id.booking_detail_email);
        booking_detail_phone = findViewById(R.id.booking_detail_phone);
        txt_exchange_rate_id = findViewById(R.id.txt_exchange_rate_id);



        relativeLayout_booking_exHotel_id.setVisibility(View.GONE);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null){
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            booking = getIntent().getParcelableExtra("BOOKING");
            items = booking.getItemsModel();

            setupHotel();
        }


        String url ="";
        if (userId != null && booking != null) {
            url = getString(R.string.app_api_ip)+ "dasta_thailand/api/mobile/user/Booking?user_id="+userId+"&payment_transaction_id=";
        }
    }

    private void setupHotel() {
        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+ items.getRoomModel().getRoomPictureModelArrayList().get(0).getPicturePaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_ex_hotel_id);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String price  = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(booking.getPaymentTransactionModel().getTotal());
                    txt_exchange_rate_id.setText(currency);
                    txt_ex_price_hotel_id.setText(price);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(booking.getPaymentTransactionModel().getTotal(),rateModel.getRateUSD()));
                    txt_exchange_rate_id.setText(currency);
                    txt_ex_price_hotel_id.setText(price);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(booking.getPaymentTransactionModel().getTotal(),rateModel.getRateCNY()));
                    txt_exchange_rate_id.setText(currency);
                    txt_ex_price_hotel_id.setText(price);
                    break;
            }
        }else {
            price = decimalFormat.format(booking.getPaymentTransactionModel().getTotal());
            txt_ex_price_hotel_id.setText(price);
        }


        booking_detail_name.setText(booking.getPaymentTransactionModel().getFirstName()+" "+booking.getPaymentTransactionModel().getLastName());
        booking_detail_email.setText(booking.getPaymentTransactionModel().getEmail());
        booking_detail_phone.setText(booking.getPaymentTransactionModel().getPhone());
        txt_item_date_hotel.setText(booking.getCheckIn()+" - "+booking.getCheckOut());
        switch (languageLocale.getLanguage()){
            case "th":
                txt_item_topic_hotel.setText(booking.getItemsModel().getTopicTH());
                txt_ex_title_hotel_id.setText(items.getRoomModel().getTopicTH());
                txt_ex_description_hotel_id.setText(items.getRoomModel().getDescriptionTH());
                if (items.getRoomModel().getBreakFast() == 0){
                    txt_ex_status_breakFast_id.setText("ไม่มีอาหารเช้า");
                }else {
                    txt_ex_status_breakFast_id.setText("อาหารเช้า");
                }
                break;
            case "en":
                txt_item_topic_hotel.setText(booking.getItemsModel().getTopicEN());
                txt_ex_title_hotel_id.setText(items.getRoomModel().getTopicEN());
                txt_ex_description_hotel_id.setText(items.getRoomModel().getDescriptionEN());
                if (items.getRoomModel().getBreakFast() == 0){
                    txt_ex_status_breakFast_id.setText("No breakfast");
                }else {
                    txt_ex_status_breakFast_id.setText("Breakfast");
                }
                break;
            case "lo":
                txt_item_topic_hotel.setText(booking.getItemsModel().getTopicLO());
                txt_ex_title_hotel_id.setText(items.getRoomModel().getTopicLO());
                txt_ex_description_hotel_id.setText(items.getRoomModel().getDescriptionLO());
                if (items.getRoomModel().getBreakFast() == 0){
                    txt_ex_status_breakFast_id.setText("ບໍ່ມີອາຫານເຊົ້າ");
                }else {
                    txt_ex_status_breakFast_id.setText("ອາຫານເຊົ້າ");
                }
                break;
            case "zh":
                txt_item_topic_hotel.setText(booking.getItemsModel().getTopicZH());
                txt_ex_title_hotel_id.setText(items.getRoomModel().getTopicZH());
                txt_ex_description_hotel_id.setText(items.getRoomModel().getDescriptionZH());
                if (items.getRoomModel().getBreakFast() == 0){
                    txt_ex_status_breakFast_id.setText("没有早餐");
                }else {
                    txt_ex_status_breakFast_id.setText("早餐");
                }
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}