package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.PeriodPackagesAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class DetailsPackageActivity extends AppCompatActivity {

    //variables
    private static final String TAG = DetailsPackageActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private String userId;

    private ItemsModel items = new ItemsModel();

    private BookingModel booking = new BookingModel();

    private TextView txt_title_package_tour, txt_company;

    private TextView txt_date_and_time, booking_detail_name, booking_detail_email, booking_detail_phone, booking_detail_discount;

    private Button btn_confirm, btn_cancel;

    private RecyclerView recycler_booking_count;

    private PeriodPackagesAdapter packagesAdapter;

    private TextView txt_grand_total;

    private TextView txt_type_activity;

    private TextView txt_mark_location;

    private RateModel rateModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_package);
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //views
        mToolbar = findViewById(R.id.menu_toolbar);
        txt_title_package_tour = findViewById(R.id.txt_title_package_tour);
        txt_company = findViewById(R.id.txt_company);
        txt_date_and_time = findViewById(R.id.txt_date_and_time);
        booking_detail_name = findViewById(R.id.booking_detail_name);
        booking_detail_email = findViewById(R.id.booking_detail_email);
        booking_detail_phone = findViewById(R.id.booking_detail_phone);
        booking_detail_discount = findViewById(R.id.booking_detail_discount);
        btn_confirm = findViewById(R.id.btn_confirm);
        btn_cancel = findViewById(R.id.btn_cancel);
        recycler_booking_count = findViewById(R.id.recycler_booking_count);
        txt_grand_total = findViewById(R.id.txt_grand_total);
        txt_type_activity = findViewById(R.id.txt_type_activity);
        txt_mark_location = findViewById(R.id.txt_mark_location);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null){
            booking = getIntent().getParcelableExtra("BOOKING");
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            items = booking.getItemsModel();
            booking_detail_name.setText(booking.getPaymentTransactionModel().getFirstName()+" "+booking.getPaymentTransactionModel().getLastName());
            booking_detail_email.setText(booking.getPaymentTransactionModel().getEmail());
            booking_detail_phone.setText(booking.getPaymentTransactionModel().getPhone());
            booking.setItemsModel(items);
            txt_date_and_time.setText(booking.getCheckIn()+" - "+booking.getCheckOut());

            if (booking.getPaymentTransactionModel().getDiscount()!= null && booking.getPaymentTransactionModel().getDiscount() != "" && !booking.getPaymentTransactionModel().getDiscount().equals("")) {
                booking_detail_discount.setText(booking.getPaymentTransactionModel().getDiscount());
            }

            switch (languageLocale.getLanguage()){
                case "th":
                    txt_title_package_tour.setText(items.getTopicTH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                    break;
                case "en":
                    txt_title_package_tour.setText(items.getTopicEN());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                    break;
                case "lo":
                    txt_title_package_tour.setText(items.getTopicLO());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                    break;
                case "zh":
                    txt_title_package_tour.setText(items.getTopicZH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                    break;
            }

            setupPeriodPackages(booking);

            if (items.getProvincesModelArrayList() != null && items.getProvincesModelArrayList().size() != 0){
                String province = "";
                for (ProvincesModel pro : items.getProvincesModelArrayList())
                    switch (languageLocale.getLanguage()){
                        case "th":
                            province += pro.getProvincesTH()+" ";
                            break;
                        case "en":
                            province += pro.getProvincesEN()+" ";
                            break;
                        case "lo":
                            province += pro.getProvincesLO()+" ";
                            break;
                        case "zh":
                            province += pro.getProvincesZH()+" ";
                            break;
                    }
                txt_mark_location.setText(province);
            }

            if (items.getTravelDetailsModelArrayList() != null && items.getTravelDetailsModelArrayList().size() != 0) {
                int day = items.getTravelDetailsModelArrayList().size();
                TextView txt_period_title = findViewById(R.id.txt_period);
                txt_period_title.setText(day+ " " +getResources().getString(R.string.text_day) + " " + (day - 1) + " " + getResources().getString(R.string.text_night));
            }
        }


        String url ="";
        if (userId != null && booking != null) {
            url = getString(R.string.app_api_ip)+ "dasta_thailand/api/mobile/user/Booking?user_id="+userId+"&payment_transaction_id=";
        }

    }

    private void setupPeriodPackages(BookingModel booking) {
        ArrayList<BookingModel> modelArrayList = new ArrayList<>();
        BookingModel book = null;
        int totals = 0;
        if (booking.getSpecialAdult() != 0 && booking.getSpecialAdult() > 1){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_adult));
            book.setSpecialAdult(booking.getSpecialAdult());
            book.setPriceSpecialAdult(booking.getPriceSpecialAdult());
            modelArrayList.add(book);
            totals += (booking.getPriceSpecialAdult() * booking.getSpecialAdult());
        }else {
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_adult));
            book.setSpecialAdult(booking.getSpecialAdult());
            book.setPriceSpecialAdult(booking.getPriceAdult());
            modelArrayList.add(book);
            totals += (booking.getPriceAdult() * booking.getSpecialAdult());
        }
        if (booking.getGuestAdult() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_single_stay));
            book.setSpecialAdult(booking.getGuestAdult());
            book.setPriceSpecialAdult(booking.getPriceAdult());
            modelArrayList.add(book);
            totals += (booking.getPriceAdult() * booking.getGuestAdult());
        }
        if (booking.getSpecialChild() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_add_bed));
            book.setSpecialAdult(booking.getSpecialChild());
            book.setPriceSpecialAdult(booking.getPriceSpecialChild());
            modelArrayList.add(book);
            totals += (booking.getPriceSpecialChild() * booking.getSpecialChild());
        }
        if (booking.getGuestChild() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_child));
            book.setSpecialAdult(booking.getGuestChild());
            book.setPriceSpecialAdult(booking.getPriceChild());
            modelArrayList.add(book);
            totals += (booking.getPriceChild() * booking.getGuestChild());
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        TextView txt_exchange_rate_id = findViewById(R.id.txt_exchange_rate_id);
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(totals);
                    txt_exchange_rate_id.setText(currency);
                    txt_grand_total.setText(price);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                    txt_exchange_rate_id.setText(currency);
                    txt_grand_total.setText(price);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateCNY()));
                    txt_exchange_rate_id.setText(currency);
                    txt_grand_total.setText(price);
                    break;
            }
        }else {
            price = decimalFormat.format(totals);
            txt_grand_total.setText(price);
        }
        packagesAdapter = new PeriodPackagesAdapter(this, modelArrayList, languageLocale.getLanguage(),rateModel);
        recycler_booking_count.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycler_booking_count.setAdapter(packagesAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}