package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.PeriodTicketAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DetailsTicketActivity extends AppCompatActivity {

    //variables
    private static final String TAG = DetailsTicketActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private String userId;

    private ItemsModel items = new ItemsModel();

    private BookingModel booking = new BookingModel();

    private TextView txt_title_package_tour, txt_company,txt_grand_total;

    private TextView txt_date_and_time, booking_detail_name, booking_detail_email, booking_detail_phone, booking_detail_discount;

    private RecyclerView recycler_booking_count;

    private PeriodTicketAdapter periodTicketAdapter;

    private RateModel rateModel;

    private TextView txt_type_activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_ticket);
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //views
        mToolbar = findViewById(R.id.menu_toolbar);
        txt_title_package_tour = findViewById(R.id.txt_title_package_tour);
        txt_company = findViewById(R.id.txt_company);
        txt_date_and_time = findViewById(R.id.txt_date_and_time);
        booking_detail_name = findViewById(R.id.booking_detail_name);
        booking_detail_email = findViewById(R.id.booking_detail_email);
        booking_detail_phone = findViewById(R.id.booking_detail_phone);
        booking_detail_discount = findViewById(R.id.booking_detail_discount);
        recycler_booking_count = findViewById(R.id.recycler_booking_count);
        txt_grand_total = findViewById(R.id.txt_grand_total);
        txt_type_activity = findViewById(R.id.txt_type_activity);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null){
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            booking = getIntent().getParcelableExtra("BOOKING");
            items = booking.getItemsModel();
            String name = booking.getPaymentTransactionModel().getFirstName()+" "+booking.getPaymentTransactionModel().getLastName();
            booking_detail_name.setText(name);
            booking_detail_email.setText(booking.getPaymentTransactionModel().getEmail());
            booking_detail_phone.setText(booking.getPaymentTransactionModel().getPhone());
            booking_detail_discount.setText("");
            subDate();
            setupAdapter();
            switch (languageLocale.getLanguage()){
                case "th":
                    txt_title_package_tour.setText(items.getTopicTH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                    break;
                case "en":
                    txt_title_package_tour.setText(items.getTopicEN());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                    break;
                case "lo":
                    txt_title_package_tour.setText(items.getTopicLO());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                    break;
                case "zh":
                    txt_title_package_tour.setText(items.getTopicZH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                    break;
            }
        }


        String url ="";
        if (userId != null && booking != null) {
            url = getString(R.string.app_api_ip)+ "dasta_thailand/api/mobile/user/Booking?user_id="+userId+"&payment_transaction_id=";
        }
    }

    private void setupAdapter() {
        ArrayList<BookingModel> modelArrayList = new ArrayList<>();
        BookingModel book = null;
        int totals = 0;
        if (booking.getGuestAdult() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_adult));
            book.setGuestAdult(booking.getGuestAdult());
            book.setPriceAdult(booking.getPriceAdult());
            modelArrayList.add(book);
            totals += (booking.getPriceAdult() * booking.getGuestAdult());
        }
        if (booking.getGuestChild() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_child));
            book.setGuestChild(booking.getGuestChild());
            book.setPriceChild(booking.getPriceChild());
            modelArrayList.add(book);
            totals += (booking.getPriceChild() * booking.getGuestChild());
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = decimalFormat.format(booking.getPaymentTransactionModel().getTotal());
        txt_grand_total.setText(price);
        periodTicketAdapter = new PeriodTicketAdapter(this,modelArrayList ,languageLocale.getLanguage(), rateModel);
        recycler_booking_count.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycler_booking_count.setAdapter(periodTicketAdapter);
    }

    private void subDate() {
        if (booking.getScheduleCheckIn()!= null) {
            String subDATE = booking.getScheduleCheckIn();
            String[] mData = subDATE.split("-");
            String getDate = mData[0], getMonth = mData[1], getYear = mData[2];
            String pattern = "EEEE dd MMMM yyyy";
            Date date = new Date();
            date.setDate(Integer.parseInt(getDate));
            date.setMonth(Integer.parseInt(getMonth) - 1);
            date.setYear(Integer.parseInt(getYear) - 1900);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String mDate = simpleDateFormat.format(date);
            String[] subString = mDate.split(" ");
            String subDay = subString[0];
            String subDate = subString[1];
            String subMonth = subString[2];
            String subYear = subString[3];
            txt_date_and_time.setText(subDay + " " + subDate + " " + subMonth + " เวลา " + booking.getCheckIn() + " - " + booking.getCheckOut() + " น.");
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}