package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ipanda.lanexangtourism.Adapter.GenderSpinnerAdapter;
import com.ipanda.lanexangtourism.Adapter.SpinnerCountryAdapter;
import com.ipanda.lanexangtourism.BuildConfig;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_utils.ImageContract;
import com.ipanda.lanexangtourism.Interface_utils.ImagePresenter;
import com.ipanda.lanexangtourism.Model.CountryModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.CountryAsyncTask;
import com.ipanda.lanexangtourism.asynctask.PersonalInfoPostRequest;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.CountryCallBack;
import com.ipanda.lanexangtourism.interface_callback.IsStatePersonalInfoCallBack;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;

public class EditProfileActivity extends AppCompatActivity implements ImageContract.View, View.OnClickListener , CountryCallBack, IsStatePersonalInfoCallBack {

    //variables
    private Toolbar mToolbar;
    private static final int REQUEST_TAKE_PHOTO = 101;
    private static final int REQUEST_GALLERY_PHOTO = 102;
    private static String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private ImagePresenter mPresenter;
    private Uri photoURI;
    private ImageView userProfilePhoto;
    private TextView txt_save_edit_profile;
    private EditText txt_identification,txt_first_name,txt_last_name,txt_age;
    private String citizenId, firstName, lastName, age, gender ,country, loginWith;
    private UserModel userModel;
    private Spinner mSpinnerCountry, mSpinnerGender;
    private SpinnerCountryAdapter spinnerCountryAdapter;
    private ChangeLanguageLocale languageLocale;
    private Bitmap bitmap;
    private String isPicState = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        //views
        mToolbar = findViewById(R.id.settings_Toolbar);
        userProfilePhoto = findViewById(R.id.userProfilePhoto);
        txt_save_edit_profile = findViewById(R.id.txt_save_edit_profile);
        txt_identification = findViewById(R.id.txt_identification);
        txt_first_name = findViewById(R.id.txt_first_name);
        txt_last_name = findViewById(R.id.txt_last_name);
        txt_age = findViewById(R.id.txt_age);
        mSpinnerCountry = findViewById(R.id.spinner_country);
        mSpinnerGender = findViewById(R.id.spinner_gender);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLanguage();
        //check intent
        if (getIntent() != null){
            userModel = getIntent().getParcelableExtra("USER_MODEL");
            loginWith = getSharedPreferences("PREF_APP_UserLoginType", Context.MODE_PRIVATE).getString("APP_UserLoginType", "");
            Picasso.get().load(userModel.getProfilePicUrl()).error(R.drawable.img_laceholder_error).placeholder(R.drawable.img_placeholder).into(userProfilePhoto);
            txt_identification.setText(userModel.getIdentification());
            txt_first_name.setText(userModel.getFirstName());
            txt_last_name.setText(userModel.getLastName());
            txt_age.setText(userModel.getAge());
            userModel.setPicState(isPicState);
        }

        //set on click event
        txt_save_edit_profile.setOnClickListener(this);

        ButterKnife.bind(this);
        mPresenter = new ImagePresenter(this);

        //set spinner gender
        setSpinnerGender();
        if (userModel.getGender().equals("Male")){
            mSpinnerGender.setSelection(1);
        }else if (userModel.getGender().equals("Female")){
            mSpinnerGender.setSelection(2);
        }else {
            mSpinnerGender.setSelection(0);
        }

        //get country
        String url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/countries/Country";
        new CountryAsyncTask(this).execute(url);

    }

    private void setSpinnerGender() {

        ArrayList<String> gen = new ArrayList<>();
        gen.add(0,getResources().getString(R.string.text_sex));
        gen.add("Male");
        gen.add("Female");
        GenderSpinnerAdapter adapter = new GenderSpinnerAdapter(this,gen);
        mSpinnerGender.setAdapter(adapter);
        mSpinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender = gen.get(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
    public void onClickEditImageProfile(View view){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.choose_photo)
                .setItems(R.array.choose_photo_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which){
                            case 0:
                                mPresenter.cameraClick();
                                Toast.makeText(EditProfileActivity.this, "Take Photo", Toast.LENGTH_SHORT).show();
                                break;
                            case 1:
                                mPresenter.ChooseGalleryClick();
                                Toast.makeText(EditProfileActivity.this, "Choose from Gallery", Toast.LENGTH_SHORT).show();
                                break;
                            case 2:
                                dialog.dismiss();
                                Toast.makeText(EditProfileActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
                                break;

                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(this, mPermission);
            if (result == PackageManager.PERMISSION_DENIED) return false;
        }
        return true;
    }

    @Override
    public void showPermissionDialog() {
        Dexter.withActivity(this).withPermissions(permissions)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(error -> showErrorDialog())
                .onSameThread()
                .check();
    }

    @Override
    public File getFilePath() {
        return getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public void showSettingsDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.message_need_permission));
        builder.setMessage(getString(R.string.message_grant_permission));
        builder.setPositiveButton(getString(R.string.label_setting), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    @Override
    public void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void startCamera(File file) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            if (file != null) {
                photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider", file);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }

    @Override
    public void chooseGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    @Override
    public void showNoSpaceDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.error_message_no_more_space));
        builder.setMessage(getString(R.string.error_message_insufficient_space));
        builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    @Override
    public int availableDisk() {
        File mFilePath = getFilePath();
        long freeSpace = mFilePath.getFreeSpace();
        return Math.round(freeSpace / 1048576);
    }

    @Override
    public File newFile() {
        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTimeInMillis();
        String mFileName = String.valueOf(timeInMillis) + ".jpeg";
        File mFilePath = getFilePath();
        try {
            File newFile = new File(mFilePath.getAbsolutePath(), mFileName);
            newFile.createNewFile();
            return newFile;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void showErrorDialog() {
        Toast.makeText(getApplicationContext(), getString(R.string.error_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayImagePreview(String mFilePath) {
        Glide.with(EditProfileActivity.this).load(mFilePath).apply(new RequestOptions().centerCrop().placeholder(R.drawable.avatar)).into(userProfilePhoto);
    }

    @Override
    public void displayImagePreview(Uri mFileUri) {
        Glide.with(EditProfileActivity.this).load(mFileUri).apply(new RequestOptions().centerCrop().placeholder(R.drawable.avatar)).into(userProfilePhoto);
    }

    @Override
    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                mPresenter.showPreview(photoURI);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), photoURI);
                    ImageUploadToServer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                String mPhotoPath = getRealPathFromUri(selectedImage);
                mPresenter.showPreview(mPhotoPath);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    ImageUploadToServer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void ImageUploadToServer(){
        isPicState = "1";
        ByteArrayOutputStream byteArrayOutputStreamObject ;
        byteArrayOutputStreamObject = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
        if (userModel != null){
            userModel.setProfilePicUrl(ConvertImage);
            userModel.setPicState(isPicState);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_save_edit_profile:
                if (getTextPersonalInfo()){
                    methodPost();
                }
                break;
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(boolean state, String userId) {

    }

    @Override
    public void onRequestCompleteListenerCountry(ArrayList<CountryModel> countryModel) {
        if (countryModel != null && countryModel.size() != 0) {
            CountryModel con = new CountryModel(this);
            con.setCountryTH("ประเทศที่ของอยู่ท่าน");
            con.setCountryEN("Your country of residence");
            con.setCountryLO("ປະເທດທີ່ທ່ານອາໄສຢູ່");
            con.setCountryZH("您的居住国");
            countryModel.add(0, con);
            spinnerCountryAdapter = new SpinnerCountryAdapter(this, countryModel, languageLocale.getLanguage());
            mSpinnerCountry.setAdapter(spinnerCountryAdapter);
            for (int i = 0; i < countryModel.size(); i++){
                if (userModel.getCountryModel().getCountryId() == countryModel.get(i).getCountryId()){
                    mSpinnerCountry.setSelection(i);
                }
            }
            mSpinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    if (userModel != null) {
                        userModel.setCountryModel(countryModel.get(position));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private boolean getTextPersonalInfo() {
        if (TextUtils.isEmpty(txt_identification.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            citizenId = txt_identification.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(txt_first_name.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            firstName = txt_first_name.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(txt_last_name.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            lastName = txt_last_name.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(txt_age.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            age = txt_age.getText().toString().replaceAll(" ", "");
        }
        return true;
    }

    private void methodPost(){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/User/updateUserData");
        HashMap<String,String> paramsPost = new HashMap<>();
//        paramsPost.put("user_identification",citizenId);
        paramsPost.put("user_id", String.valueOf(userModel.getUserId()));
        paramsPost.put("user_email",userModel.getEmail());
        paramsPost.put("user_firstName",firstName);
        paramsPost.put("user_lastName",lastName);
        paramsPost.put("user_age",age);
        paramsPost.put("user_gender",gender);
        paramsPost.put("profile_pic_state",isPicState);
//        paramsPost.put("user_loginWith",loginWith);
        paramsPost.put("Country_country_id", String.valueOf(userModel.getCountryModel().getCountryId()));
        paramsPost.put("user_profile_pic_url",userModel.getProfilePicUrl());

        if (getTextPersonalInfo()){
            httpCallPost.setParams(paramsPost);
            new PersonalInfoPostRequest(this).execute(httpCallPost);
            startActivity(new Intent(this, ProfileActivity.class));
        }

    }
}
