package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.PaymentTransactionModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventTicketDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    //Variables
    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private ConstraintLayout con_choose_datetime;

    private ItemsModel items = new ItemsModel();

    private String fistName, lastName, email, phone, weChat, whatsApp, period;

    private TextView txt_the_date, txt_day, txt_month,txt_time;

    private TextView txt_title_event_ticket, txt_scan_qr_code;

    private TextView txt_type_activity;

    private EditText edt_input_fistName, edt_input_lastName, edt_input_email, edt_input_phone, edt_input_weChat, edt_input_whatsApp, edt_input_discount;

    private Button btn_confirm_event_ticket;

    private static final int REQ_CODE = 123;

    private AppCompatEditText edt_adult, edt_child;

    private ImageButton imageButtonUpAdult, imageButtonDownAdult, imageButtonUpChild, imageButtonDownChild;

    private BookingModel bookingModel = new BookingModel();

    private PaymentTransactionModel paymentTransaction = new PaymentTransactionModel();

    private boolean isSelectDateTime = false;

    private int defaultValueAdult = 0;
    private int defaultValueChild = 0;

    private RateModel rateModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_ticket_details);

        //views
        mToolbar = findViewById(R.id.toolbar);
        con_choose_datetime = findViewById(R.id.con_choose_datetime);
        txt_title_event_ticket = findViewById(R.id.txt_title_event_ticket);
        txt_scan_qr_code = findViewById(R.id.txt_scan_qr_code_event);
        edt_input_fistName = findViewById(R.id.edt_input_fistName_event);
        edt_input_lastName = findViewById(R.id.edt_input_lastName_event);
        edt_input_email = findViewById(R.id.edt_input_email_event);
        edt_input_phone = findViewById(R.id.edt_input_phone_event);
        edt_input_weChat = findViewById(R.id.edt_input_weChat_event);
        edt_input_whatsApp = findViewById(R.id.edt_input_whatsApp_event);
        edt_input_discount = findViewById(R.id.edt_input_discount_event);
        txt_the_date = findViewById(R.id.txt_the_date);
        txt_day = findViewById(R.id.txt_day);
        txt_month = findViewById(R.id.txt_month);
        txt_time = findViewById(R.id.txt_time);
        btn_confirm_event_ticket = findViewById(R.id.btn_confirm_event_ticket);
        edt_adult = findViewById(R.id.edt_adult);
        edt_child = findViewById(R.id.edt_child);
        imageButtonUpAdult = findViewById(R.id.imageButtonUpAdult);
        imageButtonDownAdult = findViewById(R.id.imageButtonDownAdult);
        imageButtonUpChild = findViewById(R.id.imageButtonUpChild);
        imageButtonDownChild = findViewById(R.id.imageButtonDownChild);
        txt_type_activity = findViewById(R.id.txt_type_activity);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //set onclick event
        con_choose_datetime.setOnClickListener(this);
        btn_confirm_event_ticket.setOnClickListener(this);
        imageButtonUpAdult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlusAdult(1);
            }
        });
        imageButtonDownAdult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMinusAdult(1);
            }
        });
        imageButtonUpChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlusChild(1);
            }
        });
        imageButtonDownChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMinusChild(1);
            }
        });



        if (getIntent() != null){
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            setupTitle();
            getCurrentDate();
        }

        txt_scan_qr_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EventTicketDetailsActivity.this, ScanQRCodeActivity.class));
            }
        });
    }

    private void setupTitle() {
        TextView price_adult = findViewById(R.id.price_adult);
        TextView price_child = findViewById(R.id.price_child);
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String adult = null;
        String child = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    adult = decimalFormat.format(items.getPriceGuestAdult());
                    child = decimalFormat.format(items.getPriceGuestChild());
                    price_adult.setText(currency+""+adult);
                    price_child.setText(currency+""+child);
                    break;
                case "USD":
                    adult = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getPriceGuestAdult(),rateModel.getRateUSD()));
                    child = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getPriceGuestChild(),rateModel.getRateUSD()));
                    price_adult.setText(currency+""+adult);
                    price_child.setText(currency+""+child);
                    break;
                case "CNY":
                    adult = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getPriceGuestAdult(),rateModel.getRateCNY()));
                    child = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getPriceGuestChild(),rateModel.getRateCNY()));
                    price_adult.setText(currency+""+adult);
                    price_child.setText(currency+""+child);
                    break;
            }
        }else {
            adult = decimalFormat.format(items.getPriceGuestAdult());
            child = decimalFormat.format(items.getPriceGuestChild());
            price_adult.setText(adult);
            price_child.setText(child);
        }

        switch (languageLocale.getLanguage()){
            case "th":
                txt_title_event_ticket.setText(items.getTopicTH());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                break;
            case "en":
                txt_title_event_ticket.setText(items.getTopicEN());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                break;
            case "lo":
                txt_title_event_ticket.setText(items.getTopicLO());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                break;
            case "zh":
                txt_title_event_ticket.setText(items.getTopicZH());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.con_choose_datetime:
                Intent intent = new Intent(this, ChooseDateTimeActivity.class);
                intent.putExtra("ITEMS_MODEL",items);
                startActivityForResult(intent, REQ_CODE);
                break;
            case R.id.btn_confirm_event_ticket:
                Intent intentEvent = new Intent(this, EventTicketDetailsConfirmActivity.class);
                if(getTextEventTicketDetail()){
                    if (isSelectDateTime) {
                        intentEvent.putExtra("ITEMS_MODEL", items);
                        intentEvent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
                        bookingModel.setGuestAdult(Integer.parseInt(edt_adult.getText().toString()));
                        bookingModel.setGuestChild(Integer.parseInt(edt_child.getText().toString()));
                        bookingModel.setPriceAdult(items.getPriceGuestAdult());
                        bookingModel.setPriceChild(items.getPriceGuestChild());
                        bookingModel.setPaymentTransactionModel(paymentTransaction);
                        intentEvent.putExtra("BOOKING", bookingModel);
                        startActivity(intentEvent);
                    }else {
                        Toast.makeText(this, "Please select SCHEDULE.", Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }
    }


    private void getCurrentDate(){
        String pattern = "EEEE dd MMM yyyy HH:mm";
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String mDate = simpleDateFormat.format(date);
        String[] subString = mDate.split(" ");
        String subDay = subString[0];
        String subDate = subString[1];
        String subMonth = subString[2];
        String subYear = subString[3];
        String subTime = subString[4];

        txt_the_date.setText(subDate);
        txt_day.setText(subDay);
        txt_month.setText(subMonth);
        txt_time.setText(subTime+" น.");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE && resultCode == RESULT_OK){
            isSelectDateTime = true;
            String getStart = data.getStringExtra("CURRENT_START");
            String getEnd = data.getStringExtra("CURRENT_END");
            String getDate = data.getStringExtra("CURRENT_DATE");
            String getMonth = data.getStringExtra("CURRENT_MONTH");
            String getYear = data.getStringExtra("CURRENT_YEAR");
            period = data.getStringExtra("SCHEDULE");

            String pattern = "EEE dd MMM yyyy";
            Date date = new Date();
            date.setDate(Integer.parseInt(getDate));
            date.setMonth(Integer.parseInt(getMonth)-1);
            date.setYear(Integer.parseInt(getYear)-1900);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String mDate = simpleDateFormat.format(date);
            String[] subString = mDate.split(" ");
            String subDay = subString[0];
            String subDate = subString[1];
            String subMonth = subString[2];
            String subYear = subString[3];


            txt_the_date.setText(getDate+"");
            txt_day.setText(subDay);
            txt_month.setText(subMonth);
            txt_time.setText(getStart+" น.");
            bookingModel.setCheckIn(getStart);
            bookingModel.setCheckOut(getEnd);
            bookingModel.setScheduleCheckIn(getDate+"-"+getMonth+"-"+getYear);
        }

    }

    private boolean getTextEventTicketDetail() {
        if (TextUtils.isEmpty(edt_input_fistName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Fist Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setFirstName(edt_input_fistName.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_lastName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Last Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setLastName(edt_input_lastName.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_email.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Email", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setEmail(edt_input_email.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_phone.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, phone", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setPhone(edt_input_phone.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_weChat.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, WeChat", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setWeChatId(edt_input_weChat.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_whatsApp.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, WhatsApp", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            paymentTransaction.setWhatsAppId(edt_input_whatsApp.getText().toString().replaceAll(" ", ""));
        }
        return true;
    }



    private void setPlusAdult(int plusValue) {
        defaultValueAdult += plusValue;
        setValueAdult();
    }

    private void setMinusAdult(int minusValue) {
        defaultValueAdult -= minusValue;
        if (defaultValueAdult <= 0){
            defaultValueAdult = 0;
        }
        setValueAdult();
    }

    private void setValueAdult() {
        edt_adult.setText(String.valueOf(defaultValueAdult));
    }

    private void setPlusChild(int plusValue) {
        defaultValueChild += plusValue;
        setValueChild();
    }

    private void setMinusChild(int minusValue) {
        defaultValueChild -= minusValue;
        if (defaultValueChild <= 0){
            defaultValueChild = 0;
        }
        setValueChild();
    }

    private void setValueChild() {
        edt_child.setText(String.valueOf(defaultValueChild));
    }

}
