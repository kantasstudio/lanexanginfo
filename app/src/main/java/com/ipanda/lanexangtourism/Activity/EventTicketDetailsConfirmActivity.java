package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.PeriodTicketAdapter;
import com.ipanda.lanexangtourism.DialogFragment.ConfirmOrdersDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.post_booking.HttpPostTickets;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class EventTicketDetailsConfirmActivity extends AppCompatActivity {

    private static final String TAG = EventTicketDetailsConfirmActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ItemsModel items = new ItemsModel();

    private BookingModel booking = new BookingModel();

    private TextView txt_title_package_tour, txt_company,txt_grand_total;

    private TextView txt_date_and_time, booking_detail_name, booking_detail_email, booking_detail_phone, booking_detail_discount;

    private Button btn_confirm;

    private TextView btn_cancel;

    private String userId;

    private RecyclerView recycler_booking_count;

    private PeriodTicketAdapter periodTicketAdapter;

    private RateModel rateModel;

    private TextView txt_type_activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_ticket_details_confirm);
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        txt_title_package_tour = findViewById(R.id.txt_title_package_tour);
        txt_company = findViewById(R.id.txt_company);
        txt_date_and_time = findViewById(R.id.txt_date_and_time);
        booking_detail_name = findViewById(R.id.booking_detail_name);
        booking_detail_email = findViewById(R.id.booking_detail_email);
        booking_detail_phone = findViewById(R.id.booking_detail_phone);
        booking_detail_discount = findViewById(R.id.booking_detail_discount);
        recycler_booking_count = findViewById(R.id.recycler_booking_count);
        txt_grand_total = findViewById(R.id.txt_grand_total);
        btn_confirm = findViewById(R.id.btn_confirm);
        btn_cancel = findViewById(R.id.btn_cancel);
        txt_type_activity = findViewById(R.id.txt_type_activity);


        if (getIntent() != null){
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            booking = getIntent().getParcelableExtra("BOOKING");
            String name = booking.getPaymentTransactionModel().getFirstName()+" "+booking.getPaymentTransactionModel().getLastName();
            booking_detail_name.setText(name);
            booking_detail_email.setText(booking.getPaymentTransactionModel().getEmail());
            booking_detail_phone.setText(booking.getPaymentTransactionModel().getPhone());
            booking_detail_discount.setText("");
            subDate();
            setupAdapter();
            switch (languageLocale.getLanguage()){
                case "th":
                    txt_title_package_tour.setText(items.getTopicTH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                    break;
                case "en":
                    txt_title_package_tour.setText(items.getTopicEN());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                    break;
                case "lo":
                    txt_title_package_tour.setText(items.getTopicLO());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                    break;
                case "zh":
                    txt_title_package_tour.setText(items.getTopicZH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                    break;
            }

        }

        //set onclick event
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupHttpPost();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setupHttpPost() {
        if (userId != null && items != null){
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Booking");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("Items_items_id", String.valueOf(items.getItemsId()));
            paramsPost.put("paymentTransaction_firstName", booking.getPaymentTransactionModel().getFirstName());
            paramsPost.put("paymentTransaction_lastName", booking.getPaymentTransactionModel().getLastName());
            paramsPost.put("paymentTransaction_email", booking.getPaymentTransactionModel().getEmail());
            paramsPost.put("paymentTransaction_phone", booking.getPaymentTransactionModel().getPhone());
            paramsPost.put("paymentTransaction_weChatId", booking.getPaymentTransactionModel().getWeChatId());
            paramsPost.put("paymentTransaction_whatsAppId", booking.getPaymentTransactionModel().getWhatsAppId());
            paramsPost.put("booking_guestAdult", String.valueOf(booking.getGuestAdult()));
            paramsPost.put("booking_guestChild", String.valueOf(booking.getGuestChild()));
            paramsPost.put("booking_checkIn", booking.getScheduleCheckIn());
            paramsPost.put("booking_duration", booking.getCheckIn()+" "+booking.getCheckOut());
            httpCallPost.setParams(paramsPost);
            new HttpPostTickets().execute(httpCallPost);
            Bundle bundle = new Bundle();
            bundle.putString("TITLE","กำลังทำการส่งข้อมูลเพิ่อจองตั๋วกิจกรรม");
            bundle.putString("DETAILS","กรุณารอสักครู่ระบบกำส่งข้อมูลเบื่องต้นไปยังพนักงานของการจองตั๋วกิจกรรมเมื่อพนักงาน ได้รับข้อมูลขอท่านแล้ว จะมีเจ้าหน้าที่ติดต่อกลับ");
            if (items.getBusinessModel() != null) {
                bundle.putString("PHONE", items.getBusinessModel().getPhone());
            }else {
                bundle.putString("PHONE", "-");
            }
            DialogFragment dialogFragment = ConfirmOrdersDialogFragment.newInstance();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getSupportFragmentManager(),TAG);
        }
    }

    private void setupAdapter() {
        ArrayList<BookingModel> modelArrayList = new ArrayList<>();
        BookingModel book = null;
        int totals = 0;
        if (booking.getGuestAdult() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_adult));
            book.setGuestAdult(booking.getGuestAdult());
            book.setPriceAdult(booking.getPriceAdult());
            modelArrayList.add(book);
            totals += (booking.getPriceAdult() * booking.getGuestAdult());
        }
        if (booking.getGuestChild() != 0){
            book = new BookingModel();
            book.setTraveler(getResources().getString(R.string.text_child));
            book.setGuestChild(booking.getGuestChild());
            book.setPriceChild(booking.getPriceChild());
            modelArrayList.add(book);
            totals += (booking.getPriceChild() * booking.getGuestChild());
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        TextView txt_exchange_rate_id = findViewById(R.id.txt_exchange_rate_id);
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(totals);
                    txt_exchange_rate_id.setText(currency);
                    txt_grand_total.setText(price);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                    txt_exchange_rate_id.setText(currency);
                    txt_grand_total.setText(price);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateCNY()));
                    txt_exchange_rate_id.setText(currency);
                    txt_grand_total.setText(price);
                    break;
            }
        }else {
            price = decimalFormat.format(totals);
            txt_grand_total.setText(price);
        }


        periodTicketAdapter = new PeriodTicketAdapter(this,modelArrayList ,languageLocale.getLanguage(), rateModel);
        recycler_booking_count.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycler_booking_count.setAdapter(periodTicketAdapter);
    }

    private void subDate() {
        String subDATE = booking.getScheduleCheckIn();
        String[] mData = subDATE.split("-");
        String getDate = mData[0], getMonth = mData[1], getYear = mData[2];
        String pattern = "EEEE dd MMMM yyyy";
        Date date = new Date();
        date.setDate(Integer.parseInt(getDate));
        date.setMonth(Integer.parseInt(getMonth)-1);
        date.setYear(Integer.parseInt(getYear)-1900);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String mDate = simpleDateFormat.format(date);
        String[] subString = mDate.split(" ");
        String subDay = subString[0];
        String subDate = subString[1];
        String subMonth = subString[2];
        String subYear = subString[3];
        txt_date_and_time.setText(subDay+" "+ subDate+" "+subMonth+" เวลา "+booking.getCheckIn()+" - "+booking.getCheckOut()+" น.");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
