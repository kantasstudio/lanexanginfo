package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;

public class EventTicketMapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    //Variables
    private static String TAG = EventTicketMapsActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private String userId;

    private ChangeLanguageLocale languageLocale;

    private GoogleMap googleMap;

    private MapView mapView;

    private TextView txt_location_info;

    private Button btn_navigation;

    private ItemsModel items = new ItemsModel();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_ticket_maps);
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.toolbar);
        txt_location_info = findViewById(R.id.txt_location_info);
        btn_navigation = findViewById(R.id.btn_navigation);
        mapView = findViewById(R.id.mapView_event_ticket);


        //setup Toolbar
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        languageLocale = new ChangeLanguageLocale(this);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);


        if (getIntent() != null) {
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            setupTitle();
        }

        btn_navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (items != null) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + items.getLatitude() + "," + items.getLongitude() + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            }
        });
    }

    private void setupMakerLocation() {
        float Latitude = Float.parseFloat(items.getLatitude());
        float Longitude = Float.parseFloat(items.getLongitude());
        LatLng sydney = new LatLng(Latitude, Longitude);
        googleMap.addMarker(new MarkerOptions()
                .position(sydney));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        LatLng location = new LatLng(Latitude, Longitude);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 16), 2000, null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setupTitle() {
        switch (languageLocale.getLanguage()) {
            case "th":
                txt_location_info.setText(items.getContactTH());
                break;
            case "en":
                txt_location_info.setText(items.getContactEN());
                break;
            case "lo":
                txt_location_info.setText(items.getContactLO());
                break;
            case "zh":
                txt_location_info.setText(items.getContactZH());
                break;
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);

        setupMakerLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
