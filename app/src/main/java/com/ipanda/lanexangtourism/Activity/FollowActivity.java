package com.ipanda.lanexangtourism.Activity;



import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Adapter.FollowRecyclerViewAdapter;
import com.ipanda.lanexangtourism.DialogFragment.ViewProfileDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.FollowClickListener;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.FollowAsyncTask;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.FollowCallBack;

import java.util.ArrayList;
import java.util.HashMap;


public class FollowActivity extends AppCompatActivity implements View.OnClickListener , FollowCallBack, FollowClickListener {

    //Variables
    private static String TAG = FollowActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private Toolbar mToolbar;

    private TextView txtFollowToolbar;

    private String mTextFollow;

    private RecyclerView recycler_follow;

    private FollowRecyclerViewAdapter followAdapter;

    private String userId = "";

    private String type = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.follow_toolbar);
        txtFollowToolbar = findViewById(R.id.toolbar_follow);
        recycler_follow = findViewById(R.id.recycler_follow);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //get intent
        if (getIntent() != null) {
            mTextFollow = getIntent().getStringExtra("TEXT_FOLLOWER");
            userId = getIntent().getStringExtra("USER_ID");
            type = getIntent().getStringExtra("TYPE");
        }

        //set text
        txtFollowToolbar.setText(mTextFollow);


        String url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Followers/"+userId+"?type="+type;
        if (userId != null && type != null) {
            new FollowAsyncTask(this).execute(url);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }


    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<UserModel> userModelArrayList) {
        if (userModelArrayList != null && userModelArrayList.size() != 0){
            Log.e("check data", userModelArrayList+"");

            followAdapter = new FollowRecyclerViewAdapter(this, userModelArrayList,this);
            recycler_follow.setLayoutManager(new LinearLayoutManager(this));
            recycler_follow.setAdapter(followAdapter);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClickedProfile(UserModel user) {
        Bundle bundle = new Bundle();
        bundle.putString("USER_ID",String.valueOf(user.getUserId()));
        DialogFragment dialogFragment = ViewProfileDialogFragment.newInstance();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(getSupportFragmentManager(),"FollowActivity");
    }

    @Override
    public void itemClickedFollow(TextView txt_follow_title, RelativeLayout rl_btn_follow, UserModel user) {
        if (user != null) {
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Followers");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("followers_user_id", String.valueOf(user.getUserId()));
            httpCallPost.setParams(paramsPost);
            new HttpPostRequestAsyncTask().execute(httpCallPost);

            if (user.isFollowState()){
                user.setFollowState(false);
                txt_follow_title.setText("ติดตาม");
                txt_follow_title.setTextColor(getResources().getColor(R.color.white));
                rl_btn_follow.setBackground(getResources().getDrawable(R.drawable.shape_button));

            }else {
                user.setFollowState(true);
                txt_follow_title.setText("ติดตามอยู่");
                txt_follow_title.setTextColor(getResources().getColor(R.color.colorTextInputPersonnelInfo));
                rl_btn_follow.setBackground(getResources().getDrawable(R.drawable.shape_button_follow));
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
