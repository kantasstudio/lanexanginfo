package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.loaderspack.loaders.MultipleRippleLoader;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.ipanda.lanexangtourism.Adapter.ItemsHomeSearchAdapter;
import com.ipanda.lanexangtourism.Adapter.ProvinceAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ItemsSearchClickListener;
import com.ipanda.lanexangtourism.Interface_click.ProvinceClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ItemsHomeSearchAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsHomeSearchPostRequest;
import com.ipanda.lanexangtourism.asynctask.ItemsViewAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ProvinceAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ItemsHomeSearchCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsHomeSearchPostCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsLastSearchHomeCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProvinceCallBack;
import com.ipanda.lanexangtourism.items_view.ViewTourismActivity;
import com.ipanda.lanexangtourism.post_search.LastSearchItems;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class HomeSearchActivity extends AppCompatActivity implements View.OnClickListener, ProvinceCallBack, ItemsHomeSearchCallBack,
        ItemsHomeSearchPostCallBack, ProvinceClickListener, ItemsSearchClickListener , ItemsLastSearchHomeCallBack {

    //Variables
    private static final String TAG = HomeSearchActivity.class.getSimpleName();

    private String url = null , userId = null;

    private ChangeLanguageLocale languageLocale;

    private AutoCompleteTextView autoCompleteSearch;

    private RecyclerView recyclerProvince;

    private ProvinceAdapter provinceAdapter;

    private RecyclerView recyclerItems;

    private ItemsHomeSearchAdapter itemsAdapter;

    private RelativeLayout btn_back;

    private ImageView btnSearch;

    private MultipleRippleLoader mLoaderProvince, mLoaderItems;

    private LinearLayout showProvince ,showItems;

    private ConstraintLayout conItemMenu, conLastSearch, conProvince;

    private FusedLocationProviderClient fusedLocationProviderClient;

    private List<Address> addresses;

    private double latitude;
    private double longitude;

    private RecyclerView recycler_last_search;

    private ItemsHomeSearchAdapter lastSearchItemsAdapter;

    private Collection<String> lastSearchId = new ArrayList<>();

    private List<String> distinctLastSearchId = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_search);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //Views
        btn_back = findViewById(R.id.btn_back);
        btnSearch = findViewById(R.id.btn_search);
        autoCompleteSearch = findViewById(R.id.autoCompleteTextSearch);
        recyclerProvince = findViewById(R.id.recycler_province);
        mLoaderProvince = findViewById(R.id.multipleRippleLoader_province);
        showProvince = findViewById(R.id.show_not_found_province);
        conItemMenu = findViewById(R.id.con_item_menu_id);
        conLastSearch = findViewById(R.id.con_home_last_search_id);
        conProvince = findViewById(R.id.con_home_province_id);
        recyclerItems = findViewById(R.id.recycler_home_search);
        mLoaderItems = findViewById(R.id.multipleRippleLoader_items);
        showItems = findViewById(R.id.show_not_found_items);

        recycler_last_search = findViewById(R.id.recycler_last_search);

        //set on click
        btn_back.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        onClickMenuItemId();

        url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/ProvinceGroup";
        if (url != null){
            new ProvinceAsyncTask(this).execute(url);
        }

        String textSearch = autoCompleteSearch.getText().toString().replaceAll(" ", "");
        Toast.makeText(this, textSearch+"", Toast.LENGTH_SHORT).show();
        autoCompleteSearch.setOnEditorActionListener(editorActionListener);


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        addresses = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getLocation();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Location location = task.getResult();
                if (location != null) {
                    try {
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                        if (addresses != null && addresses.size() != 0) {
                            latitude =  addresses.get(0).getLatitude();
                            longitude = addresses.get(0).getLongitude();
                            getLastSearch();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }



    private void methodPostHomeSearch(){

        String textSearch = autoCompleteSearch.getText().toString().replaceAll(" ", "");
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SearchItem");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("str_search",textSearch);
        paramsPost.put("user_id",userId);
        paramsPost.put("latitude", String.valueOf(latitude));
        paramsPost.put("longitude", String.valueOf(longitude));

        if (userId != null){
            httpCallPost.setParams(paramsPost);
            new ItemsHomeSearchPostRequest(this).execute(httpCallPost);
        }
        getLastSearch();
    }

    private void onClickMenuItemId() {
        ConstraintLayout menuTwo = findViewById(R.id.menu_tourism_location);
        ConstraintLayout menuThree = findViewById(R.id.menu_restaurant);
        ConstraintLayout menuFour = findViewById(R.id.menu_shopping);
        ConstraintLayout menuFive = findViewById(R.id.menu_hotel);
        ConstraintLayout menuSix = findViewById(R.id.menu_contact);

        menuTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupItemsHomeSearch(2);
            }
        });

        menuThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupItemsHomeSearch(3);
            }
        });

        menuFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupItemsHomeSearch(4);
            }
        });

        menuFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupItemsHomeSearch(5);
            }
        });

        menuSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupItemsHomeSearch(6);
            }
        });
    }

    private void setupItemsHomeSearch(int menu){
        if (userId != null) {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Search/SearchItemByMenuItemId/" + menu
                    + "?user_id=" + userId + "&latitude=" + latitude + "&longitude=" + longitude;
        }else {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Search/SearchItemByMenuItemId/" + menu
                    + "?user_id=&latitude=" + latitude + "&longitude=" + longitude;
        }

        new ItemsHomeSearchAsyncTask(this).execute(url);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_search:
                methodPostHomeSearch();
                break;
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerLastSearch(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0) {
            lastSearchItemsAdapter = new ItemsHomeSearchAdapter(this, itemsModelArrayList, languageLocale.getLanguage(), this);
            recycler_last_search.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            recycler_last_search.setAdapter(lastSearchItemsAdapter);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0) {

            itemsAdapter = new ItemsHomeSearchAdapter(this, itemsModelArrayList, languageLocale.getLanguage(), this);
            recyclerItems.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            recyclerItems.setAdapter(itemsAdapter);

            mLoaderItems.setVisibility(View.GONE);
            showItems.setVisibility(View.GONE);
            recyclerItems.setVisibility(View.VISIBLE);

            for (ItemsModel item : itemsModelArrayList) {
                lastSearchId.add(item.getItemsId()+"");
            }
            distinctLastSearchId = lastSearchId.stream().distinct().collect(Collectors.toList());
            setLastSearch();
            getLastSearch();

        }else {
            mLoaderItems.setVisibility(View.GONE);
            showItems.setVisibility(View.VISIBLE);
            recyclerItems.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestCompleteListenerProvinces(ArrayList<ProvincesModel> provincesList) {
        if (provincesList != null && provincesList.size() != 0) {
            Log.e("check data", provincesList + "");

            provinceAdapter = new ProvinceAdapter(this, provincesList, languageLocale.getLanguage(),this);
            recyclerProvince.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            recyclerProvince.setAdapter(provinceAdapter);

            mLoaderProvince.setVisibility(View.GONE);
            showProvince.setVisibility(View.GONE);
            recyclerProvince.setVisibility(View.VISIBLE);
        }else {
            mLoaderProvince.setVisibility(View.GONE);
            showProvince.setVisibility(View.VISIBLE);
            recyclerProvince.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onPreCallServicePost() {

    }

    @Override
    public void onCallServicePost() {

    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestCompleteListenerPost(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0) {
            Log.e("check data", itemsModelArrayList + "");

            itemsAdapter = new ItemsHomeSearchAdapter(this, itemsModelArrayList, languageLocale.getLanguage(),this);
            recyclerItems.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            recyclerItems.setAdapter(itemsAdapter);

            mLoaderItems.setVisibility(View.GONE);
            showItems.setVisibility(View.GONE);
            recyclerItems.setVisibility(View.VISIBLE);

            for (ItemsModel item : itemsModelArrayList) {
                lastSearchId.add(item.getItemsId()+"");
            }
            distinctLastSearchId = lastSearchId.stream().distinct().collect(Collectors.toList());
            setLastSearch();
            getLastSearch();

        }else {
            mLoaderItems.setVisibility(View.GONE);
            showItems.setVisibility(View.VISIBLE);
            recyclerItems.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestFailedPost(String result) {

    }

    @Override
    public void onClickedProvince(ProvincesModel provinces) {
        if (userId != null) {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Search/SearchItemByProvince/"+provinces.getProvincesId()+"?user_id=" + userId
                    +"&latitude="+latitude+"&longitude="+longitude;
        }else {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Search/SearchItemByProvince/"+provinces.getProvincesId()+"?user_id="
                    +"&latitude="+latitude+"&longitude="+longitude;
        }
        new ItemsHomeSearchAsyncTask(this).execute(url);
        conItemMenu.setVisibility(View.GONE);
        conProvince.setVisibility(View.GONE);
        conLastSearch.setVisibility(View.GONE);
        mLoaderItems.setVisibility(View.VISIBLE);
        showItems.setVisibility(View.GONE);
        recyclerItems.setVisibility(View.GONE);
        setLastSearch();
    }

    @Override
    public void onClickedItemSearch(ItemsModel items) {
        Intent intent = new Intent(this, ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL", items);
        intent.putExtra("CATEGORY",items.getMenuItemModel().getMenuItemId());
        startActivity(intent);
    }

    private TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

            switch (actionId){
                case EditorInfo.IME_ACTION_SEND:
                    methodPostHomeSearch();
                    break;
            }
            return false;
        }
    };

    private void setLastSearch(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String result = String.join(",", distinctLastSearchId);
            getSharedPreferences("PREF_APP_LAST_SEARCH_HOME", Context.MODE_PRIVATE).edit().putString("APP_LAST_SEARCH_HOME", result).apply();
        }
    }

    private void getLastSearch(){
        String getLastSearch = getSharedPreferences("PREF_APP_LAST_SEARCH_HOME", Context.MODE_PRIVATE).getString("APP_LAST_SEARCH_HOME",null);
        if (getLastSearch != null && !getLastSearch.equals("")){
            postRequestLastSearch(getLastSearch);
        }
    }

    private void postRequestLastSearch(String getLastSearch){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/LastSearch/IndexLastSearch");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("last_id", getLastSearch);
        if (latitude != 0 && longitude != 0) {
            paramsPost.put("latitude", String.valueOf(latitude));
            paramsPost.put("longitude", String.valueOf(longitude));
        }else {
            paramsPost.put("latitude", "");
            paramsPost.put("longitude", "");
        }
        httpCallPost.setParams(paramsPost);
        new LastSearchItems(this).execute(httpCallPost);
    }

}
