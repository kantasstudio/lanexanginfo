package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.DialogFragment.ConfirmOrdersDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.PaymentTransactionModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.post_booking.HttpPostHotel;

import java.util.HashMap;

public class HotelDetailsActivity extends AppCompatActivity {

    //Variables
    private static final String TAG = HotelDetailsActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private ItemsModel items = new ItemsModel();

    private RoomModel rooms = new RoomModel();

    private BookingModel booking = new BookingModel();

    private PaymentTransactionModel payment = new PaymentTransactionModel();

    private String userId;

    private TextView txt_scan_qr_code;

    private EditText edt_input_fistName, edt_input_lastName, edt_input_email, edt_input_phone, edt_input_weChat, edt_input_whatsApp, edt_input_discount;

    private ImageButton imageButtonUpAdult, imageButtonDownAdult, imageButtonUpChild, imageButtonDownChild;

    private AppCompatEditText edt_adult, edt_child;

    private Button btn_confirm_hotel;

    private int defaultValueAdult = 0;

    private int defaultValueChild = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_details);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //Views
        txt_scan_qr_code = findViewById(R.id.txt_scan_qr_code_event);
        edt_input_fistName = findViewById(R.id.edt_input_fistName_event);
        edt_input_lastName = findViewById(R.id.edt_input_lastName_event);
        edt_input_email = findViewById(R.id.edt_input_email_event);
        edt_input_phone = findViewById(R.id.edt_input_phone_event);
        edt_input_weChat = findViewById(R.id.edt_input_weChat_event);
        edt_input_whatsApp = findViewById(R.id.edt_input_whatsApp_event);
        edt_input_discount = findViewById(R.id.edt_input_discount_event);
        edt_adult = findViewById(R.id.edt_adult);
        edt_child = findViewById(R.id.edt_child);
        imageButtonUpAdult = findViewById(R.id.imageButtonUpAdult);
        imageButtonDownAdult = findViewById(R.id.imageButtonDownAdult);
        imageButtonUpChild = findViewById(R.id.imageButtonUpChild);
        imageButtonDownChild = findViewById(R.id.imageButtonDownChild);
        btn_confirm_hotel = findViewById(R.id.btn_confirm_hotel);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null){
            items = getIntent().getParcelableExtra("ITEMS");
            rooms = getIntent().getParcelableExtra("ROOMS");
            booking = getIntent().getParcelableExtra("BOOKING");

            setContentTitle();
        }

        //set event onClick
        imageButtonUpAdult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlusAdult(1);
            }
        });
        imageButtonDownAdult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMinusAdult(1);
            }
        });
        imageButtonUpChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlusChild(1);
            }
        });
        imageButtonDownChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMinusChild(1);
            }
        });
        btn_confirm_hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getTextHotelDetail()){
                    setupHttpPost();
                }
            }
        });

    }

    private void setContentTitle() {
        TextView txt_item_topic_hotel = findViewById(R.id.txt_item_topic_hotel);
        TextView txt_items_rating = findViewById(R.id.txt_items_rating);
        TextView txt_item_date_hotel = findViewById(R.id.txt_item_date_hotel);

        switch (languageLocale.getLanguage()){
            case "th":
                txt_item_topic_hotel.setText(items.getTopicTH());
                break;
            case "en":
                txt_item_topic_hotel.setText(items.getTopicEN());
                break;
            case "lo":
                txt_item_topic_hotel.setText(items.getTopicLO());
                break;
            case "zh":
                txt_item_topic_hotel.setText(items.getTopicZH());
                break;
        }

        if (booking != null){
            txt_item_date_hotel.setText(booking.getScheduleCheckIn());
        }
        txt_items_rating.setText(items.getRatingStarModel().getAverageReview()+"");
    }

    private boolean getTextHotelDetail() {
        if (TextUtils.isEmpty(edt_input_fistName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Fist Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setFirstName(edt_input_fistName.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_lastName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Last Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setLastName(edt_input_lastName.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_email.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, Email", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setEmail(edt_input_email.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_phone.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, phone", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setPhone(edt_input_phone.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_weChat.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, WeChat", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setWeChatId(edt_input_weChat.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_input_whatsApp.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!, WhatsApp", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setWhatsAppId(edt_input_whatsApp.getText().toString().replaceAll(" ", ""));
        }
        booking.setPaymentTransactionModel(payment);
        return true;
    }


    private void setPlusAdult(int plusValue) {
        defaultValueAdult += plusValue;
        setValueAdult();
    }

    private void setMinusAdult(int minusValue) {
        defaultValueAdult -= minusValue;
        if (defaultValueAdult <= 0){
            defaultValueAdult = 0;
        }
        setValueAdult();
    }

    private void setValueAdult() {
        edt_adult.setText(String.valueOf(defaultValueAdult));
        booking.setGuestAdult(Integer.parseInt(edt_adult.getText().toString()));
    }

    private void setPlusChild(int plusValue) {
        defaultValueChild += plusValue;
        setValueChild();
    }

    private void setMinusChild(int minusValue) {
        defaultValueChild -= minusValue;
        if (defaultValueChild <= 0){
            defaultValueChild = 0;
        }
        setValueChild();
    }

    private void setValueChild() {
        edt_child.setText(String.valueOf(defaultValueChild));
        booking.setGuestChild(Integer.parseInt(edt_child.getText().toString()));
    }

    private void setupHttpPost() {
        if (userId != null ){
            if (items != null && booking != null) {
                HttpCall httpCallPost = new HttpCall();
                httpCallPost.setMethodType(HttpCall.POST);
                httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Booking");
                HashMap<String, String> paramsPost = new HashMap<>();
                paramsPost.put("User_user_id", userId);
                paramsPost.put("Items_items_id", String.valueOf(items.getItemsId()));
                paramsPost.put("paymentTransaction_firstName", booking.getPaymentTransactionModel().getFirstName());
                paramsPost.put("paymentTransaction_lastName", booking.getPaymentTransactionModel().getLastName());
                paramsPost.put("paymentTransaction_email", booking.getPaymentTransactionModel().getEmail());
                paramsPost.put("paymentTransaction_phone", booking.getPaymentTransactionModel().getPhone());
                paramsPost.put("paymentTransaction_weChatId", booking.getPaymentTransactionModel().getWeChatId());
                paramsPost.put("paymentTransaction_whatsAppId", booking.getPaymentTransactionModel().getWhatsAppId());
                paramsPost.put("booking_checkIn", booking.getCheckIn());
                paramsPost.put("booking_checkOut", booking.getCheckOut());
                paramsPost.put("booking_guestAdult", String.valueOf(booking.getGuestAdult()));
                paramsPost.put("booking_guestChild", String.valueOf(booking.getGuestChild()));
                paramsPost.put("Room_room_id", String.valueOf(rooms.getRoomId()));
                httpCallPost.setParams(paramsPost);
                new HttpPostHotel().execute(httpCallPost);
                Bundle bundle = new Bundle();
                bundle.putString("TITLE", "กำลังทำการส่งข้อมูลเพิ่อทำการจองห้องพัก");
                bundle.putString("DETAILS", "กรุณารอสักครู่ระบบกำส่งข้อมูลเบื่องต้นไปยังพนักงานของทางที่พัก เมื่อพนักงานทางโรงแรมได้รับข้อมูลขอท่านแล้ว จะมีเจ้าหน้าที่ทางโรงแรมที่พักโทรหาท่าน");
                if (items.getBusinessModel() != null) {
                    bundle.putString("PHONE", items.getBusinessModel().getPhone());
                }else {
                    bundle.putString("PHONE", "-");
                }
                DialogFragment dialogFragment = ConfirmOrdersDialogFragment.newInstance();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(getSupportFragmentManager(), TAG);
            }
        }else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }
}