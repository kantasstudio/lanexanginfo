package com.ipanda.lanexangtourism.Activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ipanda.lanexangtourism.Adapter.IntroViewPagerAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.ScreenItemModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class IntroActivity extends AppCompatActivity implements View.OnClickListener {

    //Variables
    private static String TAG = IntroActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ViewPager screenPager;

    private ArrayList<ScreenItemModel> listScreen;

    private IntroViewPagerAdapter mAdapter;

    private TabLayout tabIndicator;

    private Button btnBack,btnForward;

    private CheckBox mCheckBox;

    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        screenPager = findViewById(R.id.screen_viewpager);
        tabIndicator = findViewById(R.id.tab_indicator);
        btnBack = findViewById(R.id.btn_action_back);
        btnForward = findViewById(R.id.btn_action_forward);
        mCheckBox = findViewById(R.id.intro_checkBox);

        //set data list screen
        listScreen = new ArrayList<>();
        listScreen.add(new ScreenItemModel("เปิดการใช้งานอินเทอร์เน็ต","เพื่อการเชื่อมต่อใช้งานที่ครบถ้วนในด้านข้อมูล",R.drawable.info1));
        listScreen.add(new ScreenItemModel("ให้การค้นหาสถานที่เป็นเรื่องง่าย","ค้นหาสถานที่ท่องเที่ยว กิจกรรมการท่องเที่ยว \nน่าสนใจ ร้านอาหารและโรงแรมที่ใช่เพื่อคุณ \nได้อย่างง่ายดาย",R.drawable.info2));
        listScreen.add(new ScreenItemModel("เติมเต็มความต้องการของนักท่องเที่ยว","แหล่งรวบรวม ค้นหาร้านรถเช่าและแพคเกจทัวร์ \nที่คัดสรรมาให้คุณภายในแอปเดียว",R.drawable.info3));

        //setup viewpager
        mAdapter = new IntroViewPagerAdapter(this,listScreen);
        screenPager.setAdapter(mAdapter);

        //setup tabLayout with viewpager
        tabIndicator.setupWithViewPager(screenPager);

        //set on click button
        btnForward.setOnClickListener(this);
        btnBack.setOnClickListener(this);

        //check event button and checkbox
        if (position == 0){
            btnBack.setVisibility(View.GONE);
            mCheckBox.setVisibility(View.VISIBLE);
        }else {
            mCheckBox.setVisibility(View.GONE);
            btnBack.setVisibility(View.VISIBLE);
        }

        //get value position tab
        getTabIndicatorPosition();

        //set on clicked checkbox
        mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCheckBox.isChecked()){
                    Toast.makeText(IntroActivity.this, "Check", Toast.LENGTH_SHORT).show();
                    getSharedPreferences("PREF_APP_CHECKBOX", Context.MODE_PRIVATE).edit().putBoolean("APP_CHECKBOX",true).apply();
                }else {
                    Toast.makeText(IntroActivity.this, "Uncheck", Toast.LENGTH_SHORT).show();
                    getSharedPreferences("PREF_APP_CHECKBOX", Context.MODE_PRIVATE).edit().putBoolean("APP_CHECKBOX",false).apply();
                }
            }
        });


    }

    private void getTabIndicatorPosition() {
        tabIndicator.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
            @Override
            public void onTabSelected(TabLayout.Tab tab){
                position = tab.getPosition();
                if (position == 0){
                    btnBack.setVisibility(View.GONE);
                    mCheckBox.setVisibility(View.VISIBLE);
                }else{
                    mCheckBox.setVisibility(View.GONE);
                    btnBack.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //click forward
            case R.id.btn_action_forward:
                position = screenPager.getCurrentItem();
                if (position < listScreen.size()){
                    position ++;
                    screenPager.setCurrentItem(position);
                    if (position == 0){
                        btnBack.setVisibility(View.GONE);
                        mCheckBox.setVisibility(View.VISIBLE);
                    }else if (position == 3){
                        startActivity(new Intent(IntroActivity.this,LanguageActivity.class));
                    } else {
                        mCheckBox.setVisibility(View.GONE);
                        btnBack.setVisibility(View.VISIBLE);
                    }
                }
            break;
            //click back
            case R.id.btn_action_back:
                position = screenPager.getCurrentItem();
                if (position < listScreen.size()){
                    position --;
                    screenPager.setCurrentItem(position);
                    if (position == 0){
                        btnBack.setVisibility(View.GONE);
                        mCheckBox.setVisibility(View.VISIBLE);
                    }else {
                        mCheckBox.setVisibility(View.GONE);
                        btnBack.setVisibility(View.VISIBLE);
                    }
                }
            break;
        }
    }
}
