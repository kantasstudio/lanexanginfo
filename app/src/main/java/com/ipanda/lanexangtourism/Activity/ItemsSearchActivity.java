package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.agrawalsuneet.loaderspack.loaders.MultipleRippleLoader;
import com.ipanda.lanexangtourism.Adapter.ProgramTourRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ProvinceAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ProgramTourClickListener;
import com.ipanda.lanexangtourism.Interface_click.ProvinceClickListener;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsHomeSearchPostRequest;
import com.ipanda.lanexangtourism.asynctask.ProvinceAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.LastSearchProgramTourCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProgramTourCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProvinceCallBack;
import com.ipanda.lanexangtourism.items_view.ViewProgramTourActivity;
import com.ipanda.lanexangtourism.post_search.LastSearchProgramTour;
import com.ipanda.lanexangtourism.post_search.ProgramTourPostRequest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ItemsSearchActivity extends AppCompatActivity implements View.OnClickListener , ProvinceCallBack , ProvinceClickListener ,
        ProgramTourCallBack , ProgramTourClickListener , LastSearchProgramTourCallBack {

    //Variables
    private static String TAG = ItemsSearchActivity.class.getSimpleName();

    private AutoCompleteTextView autoCompleteSearch;

    private RelativeLayout btnBackPressed;

    private ImageView btnSearch;

    private ImageView imgItemSearch;

    private TextView txtItemMenu;

    private CardView cv_near_me;

    private String userId;

    private ChangeLanguageLocale languageLocale;

    private ConstraintLayout searchAllItems;

    private RecyclerView recyclerProvince;

    private ProvinceAdapter provinceAdapter;

    private MultipleRippleLoader mLoaderProvince;

    private LinearLayout showProvince;

    private RecyclerView recyclerHomeSearch;

    private ProgramTourRecyclerViewAdapter tourAdapter;

    private LinearLayout showNotFoundItems;

    private MultipleRippleLoader mLoaderNotFoundItems;

    private Collection<String> lastSearchId = new ArrayList<>();

    private List<String> distinctLastSearchId = new ArrayList<>();

    private RecyclerView recyclerLastSearch;

    private ProgramTourRecyclerViewAdapter lastSearchAdapter;

    private CircularSticksLoader loader_items_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_search);
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        
        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //View
        autoCompleteSearch = findViewById(R.id.autoCompleteTextSearch);
        btnBackPressed = findViewById(R.id.btn_back);
        btnSearch = findViewById(R.id.btn_search);
        cv_near_me = findViewById(R.id.cv_near_me);
        imgItemSearch = findViewById(R.id.img_item_search_id);
        txtItemMenu = findViewById(R.id.txt_item_menu_id);
        recyclerProvince = findViewById(R.id.recycler_province);
        mLoaderProvince = findViewById(R.id.multipleRippleLoader_province);
        showProvince = findViewById(R.id.show_not_found_province);
        searchAllItems = findViewById(R.id.con_search_menu_id);
        recyclerHomeSearch = findViewById(R.id.recycler_home_search);
        mLoaderNotFoundItems = findViewById(R.id.multipleRippleLoader_items);
        showNotFoundItems = findViewById(R.id.show_not_found_items);
        recyclerLastSearch = findViewById(R.id.recycler_last_search);

        //set on click
        btnBackPressed.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        searchAllItems.setOnClickListener(this);

        imgItemSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu1));
        txtItemMenu.setText(getResources().getString(R.string.txt_program_tour));

        cv_near_me.setVisibility(View.GONE);

        //set
        String urlProvince = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/ProvinceGroup";
        new ProvinceAsyncTask(this).execute(urlProvince);

        autoCompleteSearch.setOnEditorActionListener(editorActionListener);
        getLastSearch();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_search:
                methodPostProgramTourSearch();
                break;
            case R.id.con_search_menu_id:
                startActivity(new Intent(this, ProgramTourActivity.class));
                break;
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerLastSearchProgramTour(ArrayList<ProgramTourModel> programTourModelArrayList) {
        if (programTourModelArrayList != null && programTourModelArrayList.size() != 0){
            lastSearchAdapter = new ProgramTourRecyclerViewAdapter(this,programTourModelArrayList,languageLocale.getLanguage(),this);
            recyclerLastSearch.setLayoutManager(new LinearLayoutManager(this));
            recyclerLastSearch.setAdapter(lastSearchAdapter);
        }
    }

    @Override
    public void onRequestCompleteListenerProvinces(ArrayList<ProvincesModel> provincesList) {
        if (provincesList != null && provincesList.size() != 0) {
            Log.e("check data", provincesList + "");

            provinceAdapter = new ProvinceAdapter(this, provincesList, languageLocale.getLanguage(),this);
            recyclerProvince.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            recyclerProvince.setAdapter(provinceAdapter);

            mLoaderProvince.setVisibility(View.GONE);
            showProvince.setVisibility(View.GONE);
            recyclerProvince.setVisibility(View.VISIBLE);
        }else {
            mLoaderProvince.setVisibility(View.GONE);
            showProvince.setVisibility(View.VISIBLE);
            recyclerProvince.setVisibility(View.GONE);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestCompleteListener(ArrayList<ProgramTourModel> programTourModelArrayList) {
        if (programTourModelArrayList != null && programTourModelArrayList.size() != 0){
            Log.e("check data",programTourModelArrayList+"");

            tourAdapter = new ProgramTourRecyclerViewAdapter(this,programTourModelArrayList,languageLocale.getLanguage(),this);
            recyclerHomeSearch.setLayoutManager(new LinearLayoutManager(this));
            recyclerHomeSearch.setAdapter(tourAdapter);

            recyclerHomeSearch.setVisibility(View.VISIBLE);
            mLoaderNotFoundItems.setVisibility(View.GONE);
            showNotFoundItems.setVisibility(View.GONE);

            for (ProgramTourModel p : programTourModelArrayList) {
                lastSearchId.add(p.getProgramTourId()+"");
            }
            distinctLastSearchId = lastSearchId.stream().distinct().collect(Collectors.toList());
            setLastSearch();
            getLastSearch();

        }else {
            recyclerHomeSearch.setVisibility(View.GONE);
            mLoaderNotFoundItems.setVisibility(View.GONE);
            showNotFoundItems.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onClickedProvince(ProvincesModel provinces) {
        String textSearch = "";
        if (provinces != null){
            switch (languageLocale.getLanguage()){
                case "th":
                    textSearch = provinces.getProvincesTH();
                    break;
                case "en":
                    textSearch = provinces.getProvincesEN();
                    break;
                case "lo":
                    textSearch = provinces.getProvincesLO();
                    break;
                case "zh":
                    textSearch = provinces.getProvincesZH();
                    break;
            }
        }
        methodPostByProvinceSearch(textSearch);

    }

    private TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

            switch (actionId){
                case EditorInfo.IME_ACTION_SEND:
                    methodPostProgramTourSearch();
                    break;
            }
            return false;
        }
    };


    private void methodPostProgramTourSearch(){
        String textSearch = autoCompleteSearch.getText().toString().replaceAll(" ", "");
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ProgramTour/SearchProgramTour");
        HashMap<String,String> paramsPost = new HashMap<>();
        if (userId != null) {
            paramsPost.put("User_user_id", userId);
        }else {
            paramsPost.put("User_user_id", "");
        }
        paramsPost.put("str_search",textSearch);
        httpCallPost.setParams(paramsPost);
        new ProgramTourPostRequest(this).execute(httpCallPost);
    }

    private void methodPostByProvinceSearch(String textSearch){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ProgramTour/SearchProgramTour");
        HashMap<String,String> paramsPost = new HashMap<>();
        if (userId != null) {
            paramsPost.put("User_user_id", userId);
        }else {
            paramsPost.put("User_user_id", "");
        }
        paramsPost.put("str_search",textSearch);
        httpCallPost.setParams(paramsPost);
        new ProgramTourPostRequest(this).execute(httpCallPost);
    }

    @Override
    public void itemClicked(ProgramTourModel tourModel) {
        Intent intent = new Intent(this, ViewProgramTourActivity.class);
        intent.putExtra("PROGRAM_TOUR", tourModel);
        startActivity(intent);
    }

    @Override
    public void itemClickedLikeProgramTour(ImageView img_like, TextView txt_count_like, ProgramTourModel tourModel) {
        String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Likes");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("User_user_id",userId);
        paramsPost.put("ProgramTour_programTour_id", String.valueOf(tourModel.getProgramTourId()));
        httpCallPost.setParams(paramsPost);
        if (userId != null){
            new HttpPostRequestAsyncTask().execute(httpCallPost);
            finish();
            startActivity(getIntent());
        }
    }


    private void setLastSearch(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String result = String.join(",", distinctLastSearchId);
            getSharedPreferences("PREF_APP_LAST_SEARCH", Context.MODE_PRIVATE).edit().putString("APP_LAST_SEARCH", result).apply();
        }
    }

    private void getLastSearch(){
        String getLastSearch = getSharedPreferences("PREF_APP_LAST_SEARCH", Context.MODE_PRIVATE).getString("APP_LAST_SEARCH",null);
        if (getLastSearch != null){
            postRequestLastSearch(getLastSearch);
        }
    }

    private void postRequestLastSearch(String getLastSearch){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/LastSearch");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("category_id", "1");
        paramsPost.put("last_id", getLastSearch);
        paramsPost.put("latitude", "");
        paramsPost.put("longitude", "");
        httpCallPost.setParams(paramsPost);
        new LastSearchProgramTour(this).execute(httpCallPost);
    }
}
