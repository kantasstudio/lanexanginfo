package com.ipanda.lanexangtourism.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;

public class LanguageActivity extends AppCompatActivity implements View.OnClickListener {

    //variables
    private static String TAG = LanguageActivity.class.getSimpleName();

    private ImageView btnThai,btnEnglish,btnLaos,btnChina;

    private ChangeLanguageLocale languageLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_language);

        //views
        btnThai = findViewById(R.id.btn_language_th);
        btnEnglish = findViewById(R.id.btn_language_eng);
        btnLaos = findViewById(R.id.btn_language_lao);
        btnChina = findViewById(R.id.btn_language_chin);

        //set on click button
        btnThai.setOnClickListener(this);
        btnEnglish.setOnClickListener(this);
        btnLaos.setOnClickListener(this);
        btnChina.setOnClickListener(this);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_language_th:
                languageLocale.setLocale("th");
                break;
            case R.id.btn_language_eng:
                languageLocale.setLocale("en");
                break;
            case R.id.btn_language_lao:
                languageLocale.setLocale("lo");
                break;
            case R.id.btn_language_chin:
                languageLocale.setLocale("zh");
                break;
        }
        onClickLanguageIntentLogin();
        getSharedPreferences("PREF_APP_STATE_LANGUAGE", Context.MODE_PRIVATE).edit().putBoolean("APP_STATE_LANGUAGE", true).apply();
    }

    private void onClickLanguageIntentLogin() {
        startActivity(new Intent(LanguageActivity.this,LoginActivity.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }
}
