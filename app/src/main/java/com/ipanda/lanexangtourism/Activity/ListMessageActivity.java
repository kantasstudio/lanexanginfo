package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.ipanda.lanexangtourism.Adapter.ChatMessageListAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ChatMessageClickListener;
import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.MessageListAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.MessageListCallBack;
import com.ipanda.lanexangtourism.message.Config;
import com.ipanda.lanexangtourism.post_booking.HttpPostReadMessages;

import java.util.ArrayList;
import java.util.HashMap;

public class ListMessageActivity extends AppCompatActivity implements View.OnClickListener , MessageListCallBack , ChatMessageClickListener{

    //Variables
    private static String TAG = ListMessageActivity.class.getSimpleName();

    private static final int REQUEST_GET_MESSAGE = 1001;

    private ChangeLanguageLocale languageLocale;

    private Toolbar mToolbar;

    private String userId;

    private RecyclerView recycler_list_message;

    private ChatMessageListAdapter chatMessageListAdapter;

    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_message);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //views
        mToolbar = findViewById(R.id.message_toolbar);
        recycler_list_message = findViewById(R.id.recycler_list_message);


        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }
        //setup on click button back

        getMessageList();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.PUSH_LIST)) {
                    getMessageList();
                }
            }
        };

    }

    private void getMessageList() {
        if (userId != null) {
            String url = getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/getListMessage/" + userId + "?type=1";
            new MessageListAsyncTask(this).execute(url);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getMessageList();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<MessageModel> messageArrayList) {
        if (messageArrayList != null && messageArrayList.size() != 0){

            chatMessageListAdapter = new ChatMessageListAdapter(this, messageArrayList, languageLocale.getLanguage(), userId, this);
            recycler_list_message.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            recycler_list_message.setAdapter(chatMessageListAdapter);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onClickChatMessage(MessageModel message, RelativeLayout notification) {
        String name = "";
        String id = "";
        if (message.getUserModel().getUserId() != Integer.parseInt(userId)){
            name = message.getUserModel().getFirstName()+" "+message.getUserModel().getLastName();
            id = String.valueOf(message.getUserModel().getUserId());
        }

        if (message.getUserSend().getUserId() != Integer.parseInt(userId)){
            name = message.getUserSend().getFirstName()+" "+message.getUserSend().getLastName();
            id = String.valueOf(message.getUserSend().getUserId());
        }

        Intent intent = new Intent(this, ChatMessageActivity.class);
        intent.putExtra("Send_To_Name", name);
        intent.putExtra("Send_To_UserId", id);
        intent.putExtra("Chat_Room_Id", message.getRoomId()+"");
        startActivityForResult(intent, REQUEST_GET_MESSAGE);
        message.setRead(false);
        notification.setVisibility(View.GONE);
        updateReadMessages(message.getRoomId()+"");

    }

    private void updateReadMessages(String roomId){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/updateReadMessages");
        HashMap<String, String> paramsPost = new HashMap<>();
        paramsPost.put("chatroom_id", roomId);
        paramsPost.put("user_id", userId);
        httpCallPost.setParams(paramsPost);
        new HttpPostReadMessages().execute(httpCallPost);
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(Config.PUSH_LIST));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_GET_MESSAGE:
                if (data != null) {
                    String test = data.getStringExtra("BackPressed");
                    getMessageList();
                }
                break;
        }
    }
}
