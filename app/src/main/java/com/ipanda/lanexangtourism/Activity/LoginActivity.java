package com.ipanda.lanexangtourism.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

//import com.google.android.gms.auth.api.signin.GoogleSignIn;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.auth.api.signin.GoogleSignInClient;
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
//import com.google.android.gms.common.api.ApiException;
//import com.google.android.gms.tasks.Task;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.ipanda.lanexangtourism.DialogFragment.CopyrightDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.ItemsSearchHomeDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.LineLogin;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.IsStateLoginAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.IsStateLoginCallBack;
import com.linecorp.linesdk.LoginDelegate;
//import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.api.LineApiClientBuilder;
import com.linecorp.linesdk.auth.LineAuthenticationParams;
import com.linecorp.linesdk.auth.LineLoginApi;
import com.linecorp.linesdk.auth.LineLoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener , IsStateLoginCallBack {

    //Variables
    private static String TAG = LoginActivity.class.getSimpleName();

    private static final int RC_SIGN_IN = 101;
    private static final int RC_SIGN_IN_FACEBOOK = 102;
    private static final int RC_SIGN_IN_LINE = 103;

    private TextView btnFacebook,btnGoogle,btnLine;

    private ChangeLanguageLocale languageLocale;

    private CallbackManager callbackManager;

    private AccessTokenTracker accessTokenTracker;

    private GoogleSignInClient googleSignInClient;

    private LoginDelegate loginDelegate;

    private static LineApiClient lineApiClient;

    private String url;

    private UserModel user;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        //views
        btnFacebook = findViewById(R.id.btn_login_facebook);
        btnGoogle = findViewById(R.id.btn_login_google);
        btnLine = findViewById(R.id.btn_login_line);

        //set on click button
        callbackManager = CallbackManager.Factory.create();

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        user = new UserModel();


        //Login Google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(getResources().getString(R.string.google_login_app_id))
                .requestIdToken("247075743210-6hc6u18fb87g4e19tdkpgkenjh1rrtig.apps.googleusercontent.com")
//                .requestScopes(new Scope(Scopes.PROFILE))
//                .requestServerAuthCode("640642346948-hlfqpd94ifv9jogpajm2fnm3khkpn0ui.apps.googleusercontent.com")
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);

        //Login Line
        LineApiClientBuilder apiClientBuilder = new LineApiClientBuilder(getApplicationContext(), LineLogin.CHANNEL_ID);
        lineApiClient = apiClientBuilder.build();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    public void setLoginFacebook(View view) {
        Toast.makeText(this, "Facebook", Toast.LENGTH_SHORT).show();
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email,public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Toast.makeText(LoginActivity.this, "onSuccess", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Log.e("LoginActivity", "cancel");
                        Toast.makeText(LoginActivity.this, "cancel", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e("LoginActivity", exception.toString());
                        Toast.makeText(LoginActivity.this, "FacebookException : ", Toast.LENGTH_SHORT).show();
                    }
                });
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken != null){
                    loadUserProfile(currentAccessToken);
                }else {

                }
            }
        };

    }

    private void loadUserProfile(AccessToken newAccessToken){
        GraphRequest request = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
                    String email = object.getString("email");
                    String profile_id = object.getString("id");
                    String image_url = "https://graph.facebook.com/"+profile_id+"/picture?type=normal";

                    UserModel userModel = new UserModel();
                    userModel.setToken(profile_id);
                    userModel.setFirstName(first_name);
                    userModel.setLastName(last_name);
                    userModel.setEmail(email);
                    userModel.setProfilePicUrl(image_url);
                    updateProfileLogin(userModel);
                    getSharedPreferences("PREF_APP_UserLoginType", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "facebook").apply();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Login FAILED! from FaceBook.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void setLoginGoogle(View view){
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //Login Line
    public void setLoginLine(View view) {
        try {

            loginDelegate = LoginDelegate.Factory.create();
            Intent loginIntent = LineLoginApi.getLoginIntent(this,
                    LineLogin.CHANNEL_ID, new LineAuthenticationParams.Builder()
                            .scopes(Arrays.asList(com.linecorp.linesdk.Scope.PROFILE))
                            .build());
            startActivityForResult(loginIntent, RC_SIGN_IN_LINE);
        } catch (Exception e) {
            Log.e("ERROR", e.toString());
        }
    }


    public void onClickCloseLoginActivity(View view) {
        startActivity(new Intent(LoginActivity.this,MainActivity.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(LoginActivity.this,SplashActivity.class));
    }

    //Get Profile Google
    @SuppressLint("LongLogTag")
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            UserModel userModel = new UserModel();
            if (account != null) {
                String idToken = account.getIdToken();
                String personEmail = account.getEmail();
                String personId = account.getId();
                Uri personPhoto = account.getPhotoUrl();
                userModel.setToken(personId);
                userModel.setEmail(personEmail);
                userModel.setProfilePicUrl(String.valueOf(personPhoto));
                updateProfileLogin(userModel);
                getSharedPreferences("PREF_APP_UserLoginType", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "google").apply();
            }

        } catch (ApiException e) {
            Log.e("TAG", "signInResult:failed code=" + e.getStatusCode());
            updateProfileLogin(null);
            Toast.makeText(this, "Login FAILED! from Google.", Toast.LENGTH_SHORT).show();
        }
    }

    //Get Profile Line
    private void loadProfileLine(LineLoginResult result){
        switch (result.getResponseCode()) {
            case SUCCESS:
                // Login successful
                String accessToken = result.getLineCredential().getAccessToken().getTokenString();
//                Toast.makeText(this, accessToken+"", Toast.LENGTH_SHORT).show();
                UserModel users = new UserModel();
                users.setToken(result.getLineProfile().getUserId());
                users.setProfilePicUrl(result.getLineProfile().getPictureUrl().toString());
                users.setFirstName(result.getLineProfile().getDisplayName().replace(" ","||"));
                users.setEmail("");
                getSharedPreferences("PREF_APP_UserLoginType", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "line").apply();
                updateProfileLogin(users);

                break;
            case CANCEL:
                Log.e("ERROR", "LINE Login Canceled by user.");
                break;
            default:
                Log.e("ERROR", "Login FAILED!");
                Log.e("ERROR", result.getErrorData().toString());
                Toast.makeText(this, "Login FAILED! from Line.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }else if (requestCode == RC_SIGN_IN_LINE){
            LineLoginResult result = LineLoginApi.getLoginResultFromIntent(data);
            try {
                if (result != null){
                    loadProfileLine(result);
                }
            }catch (Exception e){
                Log.v("LoginLine",e.toString());
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void updateProfileLogin(UserModel userModel){
        this.user = userModel;
        if (user != null) {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/User/" + user.getToken();
            new IsStateLoginAsyncTask(this).execute(url);
        }else {

        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(boolean state, String userId) {
        if (state){
            getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", true).apply();
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).edit().putString("APP_USER_ID", userId).apply();
        }else {
            Bundle bundle = new Bundle();
            DialogFragment dialogFragment = CopyrightDialogFragment.newInstance();
            bundle.putParcelable("SEND_USER_MODEL", user);
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getSupportFragmentManager(),TAG);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }
}
