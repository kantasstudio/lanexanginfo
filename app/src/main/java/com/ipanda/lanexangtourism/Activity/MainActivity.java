package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.MenuFragment.BookingFragment;
import com.ipanda.lanexangtourism.MenuFragment.ContactFragment;
import com.ipanda.lanexangtourism.MenuFragment.HomeFragment;
import com.ipanda.lanexangtourism.MenuFragment.HotelFragment;
import com.ipanda.lanexangtourism.MenuFragment.ProgramTourFragment;
import com.ipanda.lanexangtourism.MenuFragment.RestaurantFragment;
import com.ipanda.lanexangtourism.MenuFragment.ShoppingFragment;
import com.ipanda.lanexangtourism.MenuFragment.TourismLocationFragment;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.post_booking.HttpPostTokenChat;
import com.google.firebase.messaging.FirebaseMessagingService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    //Variables
    private static final String TAG = MainActivity.class.getSimpleName();

    private int navItemIndex = 0;

    private String getNavItemIndex = null;

    private static final int REQUEST_LOCATION = 101 ;

    private static final int RSS_JOB_ID = 1000;

    private ChangeLanguageLocale languageLocale;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getNavItemIndex = getIntent().getStringExtra("NavItemIndex");
        //debugHashKey();
        if(getNavItemIndex != null){
            navItemIndex = Integer.parseInt(getNavItemIndex);
            loadFragment();
        }else {
            loadFragment();
        }

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();


        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();

                        // Log and toast
                        String msg = getResources().getString(R.string.msg_token_fmt)+token;
                        Log.d(TAG, msg);
                        setupHttpPost(token);
                    }
                });
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);



    }

    private void setupHttpPost(String token) {
        String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        if (userId != null) {
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/updateCurrentToken");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("user_id", userId);
            paramsPost.put("currentToken", token);
            httpCallPost.setParams(paramsPost);
            new HttpPostTokenChat().execute(httpCallPost);
        }else {

        }
    }



    private void loadFragment() {
        Fragment fragment = getHomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.FrameLayout_Menu, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                ProgramTourFragment tourFragment = new ProgramTourFragment();
                return tourFragment;
            case 2:
                TourismLocationFragment locationFragment = new TourismLocationFragment();
                return locationFragment;
            case 3:
                RestaurantFragment restaurantFragment = new RestaurantFragment();
                return restaurantFragment;
            case 4:
                ShoppingFragment shoppingFragment = new ShoppingFragment();
                return shoppingFragment;
            case 5:
                HotelFragment hotelFragment = new HotelFragment();
                return hotelFragment;
            case 6:
                ContactFragment contactFragment = new ContactFragment();
                return contactFragment;
            case 7:
                BookingFragment bookingFragment = new BookingFragment();
                return bookingFragment;
            default:
                return new HomeFragment();
        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("LANEXANG INFO");
            builder.setIcon(R.mipmap.ic_lanexang_round);
            builder.setMessage("Do you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                          finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
//            AlertDialog alert = builder.create();
//            alert.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}


