package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NativeActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.LoadMenuFragmentHelper;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.NavigationFragment.ListViewNavigationFragment;
import com.ipanda.lanexangtourism.NavigationFragment.MapsViewNavigationFragment;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ItemsAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.ItemsCallBack;
import com.ipanda.lanexangtourism.utils.SetMarkerGoogleUtils;

import java.util.ArrayList;

public class NavigateActivity extends AppCompatActivity implements OnMapReadyCallback , ItemsCallBack {

    //variables
    private static final String TAG = NavigateActivity.class.getSimpleName();

    private static final int REQUEST_LOCATION = 101;

    private Toolbar mToolbar;

    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle toggle;

    private GoogleMap googleMap;

    private MapView mapView_mark_icon;

    private Marker marker;

    private String urlGetItemsAll = "";

    private ChangeLanguageLocale languageLocale;

    private FusedLocationProviderClient mFusedLocationClient;

    private SetMarkerGoogleUtils setMarkerGoogleUtils;

    private String userId;

    private ItemsModel items;

    public static int navItemIndex = 0;

    private int getCategory;

    private ImageView actionBackMaps;

    private ImageView btnBack, btnOpenBurger, btnCloseBurger, btn_my_location, img_cover_items, img_items_bookmarks;

    private ImageView btnSearchFilterIcon;

    private CheckBox check_map_hotel, check_map_restaurant, check_map_tourism, check_map_shopping, check_map_contact;

    private CheckBox check_open_close;

    private SearchView edt_title_search_filter;

    private RecyclerView recycler_maps_items_sliding;

    private ProgressDialog alertDialog = null;

    private ArrayList<ItemsModel> itemsList;

    private LatLng location;

    //Location
    private static final int PERMISSION_ID = 1;
    private FusedLocationProviderClient client;
    private Location locationResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigate);

        //views
        setupViews();

        //set event on click
        setupEventOnClick();
        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        if (getIntent() != null) {
            items = new ItemsModel();
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            getCategory = getIntent().getIntExtra("CATEGORY_ID",0);
            double latitude = Double.parseDouble(items.getLatitude());
            double longitude = Double.parseDouble(items.getLongitude());
            location = new LatLng(latitude, longitude);
        }

        //setup ActionBarDrawerToggle
        toggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //new array list
        itemsList = new ArrayList<>();

        client = LocationServices.getFusedLocationProviderClient(this);
        getPermissionLocation();

        mapView_mark_icon.onCreate(savedInstanceState);


        if (userId != null) {
            urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=" + userId;

        }else {
            urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=";
        }

        String countString = getString(R.string.text_search) + getString(R.string.text_in_the_map);
        String textSearch = ""+getString(R.string.text_search);
       /* if (countString.length() > 16){
            textSearch = countString.substring(0,16)+"...";
        }else {
            textSearch = getString(R.string.text_search) + getString(R.string.text_in_the_map);
        }*/

        edt_title_search_filter.setQuery("",false);
        edt_title_search_filter.setQueryHint(textSearch);
        edt_title_search_filter.setIconified(false);
        edt_title_search_filter.setIconifiedByDefault(false);
        edt_title_search_filter.setSubmitButtonEnabled(true);
        edt_title_search_filter.setBackgroundColor(Color.TRANSPARENT);
        edt_title_search_filter.clearFocus();

    }

    private void getAsyncTask() {
        new ItemsAsyncTask(this).execute(urlGetItemsAll);
    }

    @SuppressLint("WrongViewCast")
    private void setupViews() {
        mToolbar = findViewById(R.id.toolbar);
        mapView_mark_icon = findViewById(R.id.mapView_mark_icon);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        btnOpenBurger = findViewById(R.id.action_drawer_id);
        btnCloseBurger = findViewById(R.id.btn_burger_close);
        btn_my_location = findViewById(R.id.btn_my_location);
        edt_title_search_filter = findViewById(R.id.edt_title_search_filter);
        recycler_maps_items_sliding = findViewById(R.id.recycler_maps_items_sliding);
        check_map_hotel = findViewById(R.id.check_map_hotel);
        check_map_restaurant = findViewById(R.id.check_map_restaurant);
        check_map_tourism = findViewById(R.id.check_map_tourism);
        check_map_shopping = findViewById(R.id.check_map_shopping);
        check_map_contact = findViewById(R.id.check_map_contact);
        check_open_close = findViewById(R.id.check_open_close);
        actionBackMaps = findViewById(R.id.action_back_maps_id);
    }

    private void setupEventOnClick() {
        btnOpenBurger.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.END);
                drawerMenu();
            }
        });
        btnCloseBurger.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
            }
        });
        actionBackMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_my_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                googleMap.setMyLocationEnabled(false);
                setMyLocation();
            }
        });
    }

    //Menu Drawer
    public void drawerMenu() {
        ImageView menuHome = findViewById(R.id.menu_home);
        ConstraintLayout menuProgramTour = findViewById(R.id.menu_program_tour);
        ConstraintLayout menuTourismLocation = findViewById(R.id.menu_tourism_location);
        ConstraintLayout menuRestaurant = findViewById(R.id.menu_restaurant);
        ConstraintLayout menuShopping = findViewById(R.id.menu_shopping);
        ConstraintLayout menuHotel = findViewById(R.id.menu_hotel);
        ConstraintLayout menuContact = findViewById(R.id.menu_contact);
        ConstraintLayout menuBooking = findViewById(R.id.menu_booking);
        Intent intent = new Intent(this, MainActivity.class);
        menuHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("NavItemIndex","0");
                startActivity(intent);
            }
        });
        menuProgramTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("NavItemIndex","1");
                startActivity(intent);
            }
        });
        menuTourismLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 2;
                intent.putExtra("NavItemIndex","2");
                startActivity(intent);
            }
        });
        menuRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 3;
                intent.putExtra("NavItemIndex","3");
                startActivity(intent);
            }
        });
        menuShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 4;
                intent.putExtra("NavItemIndex","4");
                startActivity(intent);
            }
        });
        menuHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 5;
                intent.putExtra("NavItemIndex","5");
                startActivity(intent);
            }
        });
        menuContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 6;
                intent.putExtra("NavItemIndex","6");
                startActivity(intent);
            }
        });
        menuBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 7;
                intent.putExtra("NavItemIndex","7");
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.clear();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        setMarkerGoogleUtils = new SetMarkerGoogleUtils(this,googleMap,marker,itemsList,languageLocale.getLanguage(),recycler_maps_items_sliding ,check_open_close,edt_title_search_filter,btnSearchFilterIcon);
        setMarkerGoogleUtils.setShowIconMarkerMaps();
        setMarkerGoogleUtils.setShowIconMarkerMaps(String.valueOf(getCategory));
        dismissProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView_mark_icon.onResume();
    }
    @Override
    public void onStart() {
        super.onStart();
        mapView_mark_icon.onStart();
    }
    @Override
    public void onStop() {
        super.onStop();
        mapView_mark_icon.onStop();
    }
    @Override
    public void onPause() {
        mapView_mark_icon.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        mapView_mark_icon.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView_mark_icon.onLowMemory();
    }


    @Override
    public void onPreCallService() {
        showProgressDialog();
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            this.itemsList = itemsModelArrayList;

            mapView_mark_icon.getMapAsync(this::onMapReady);

            //check map default menu
            checkMapDefaultMenu();
            //get check maps
            checkMapIsChecked();

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private void checkMapDefaultMenu() {
        switch (getCategory) {
            case 2:
                check_map_tourism.setChecked(true);
                break;
            case 3:
                check_map_restaurant.setChecked(true);
                break;
            case 4:
                check_map_shopping.setChecked(true);
                break;
            case 5:
                check_map_hotel.setChecked(true);
                break;
            case 6:
                check_map_contact.setChecked(true);
                break;
        }
    }

    private void checkMapIsChecked() {
        check_map_tourism.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_tourism.isChecked()) {
                    setShowIconMarkerMaps(2);
                } else {
                    setHideIconMarkerMaps(2);
                }
            }
        });

        check_map_restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_restaurant.isChecked()) {
                    setShowIconMarkerMaps(3);
                } else {
                    setHideIconMarkerMaps(3);
                }
            }
        });

        check_map_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_shopping.isChecked()) {
                    setShowIconMarkerMaps(4);
                } else {
                    setHideIconMarkerMaps(4);
                }
            }
        });

        check_map_hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_hotel.isChecked()) {
                    setShowIconMarkerMaps(5);
                } else {
                    setHideIconMarkerMaps(5);
                }
            }
        });

        check_map_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_contact.isChecked()) {
                    setShowIconMarkerMaps(6);
                } else {
                    setHideIconMarkerMaps(6);
                }
            }
        });

    }

    private void setShowIconMarkerMaps(int category){
        setMarkerGoogleUtils.setShowIconMarkerMaps(String.valueOf(category));
    }

    private void setHideIconMarkerMaps(int category){
        setMarkerGoogleUtils.setHideIconMarkerMaps(String.valueOf(category));
    }

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(this);
        alertDialog.setMessage("Loading screen for Google Maps ...");
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
            setMarkerGoogleUtils.moveOnZoomToLocationByItems(location, items);
        }
    }

    public void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("\t message.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void setMyLocation() {
        Toast.makeText(this, "Wait 3 second...", Toast.LENGTH_SHORT).show();
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();

            double latitude = mLastLocation.getLatitude();
            double longitude =  mLastLocation.getLongitude();
            LatLng latLng = new LatLng(latitude,longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10), 2000, null);
 /*           int resId = getResourceByFilename(this, "ic_here");
            Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resId);
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 145, 160, false);*/
            MarkerOptions options = new MarkerOptions()
                    .title(null)
                    .position(latLng)
                    .zIndex(1.0f)
//                    .icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap))
                    .draggable(false);
            marker = googleMap.addMarker(options);
        }
    };

    private void getPermissionLocation(){
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            getCurrentLocation();
        }else {
            requestLocationPermission();
        }
    }

    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            client.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location != null){
                        locationResult = location;
                        String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
                        if (userId != null) {
                            urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=" + userId + "&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();

                        }else {
                            urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=" + "&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                        }
                        getAsyncTask();
                    }else {
                        LocationRequest locationRequest = new LocationRequest()
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setInterval(10000)
                                .setFastestInterval(1000)
                                .setNumUpdates(1);
                        LocationCallback locationCallback = new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                super.onLocationResult(locationResult);
                                Location location1 = locationResult.getLastLocation();
                                Toast.makeText(NavigateActivity.this, location1.getLatitude()+"  "+location1.getLongitude(), Toast.LENGTH_SHORT).show();
                            }
                        };
                        client.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper());
                        getAsyncTask();
                    }
                }
            });
        }else {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }


    private void requestLocationPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_COARSE_LOCATION)
                && ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)){
            new AlertDialog.Builder(this)
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed because of this and that")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(NavigateActivity.this,new String[] {Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_LOCATION);
                        }
                    })
                    .create().show();
        }else {
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show();
                getCurrentLocation();
            }else {
                requestLocationPermission();
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }
}