package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.ipanda.lanexangtourism.Adapter.SpinnerCategoryAdapter;
import com.ipanda.lanexangtourism.Adapter.SpinnerCategorySubAdapter;
import com.ipanda.lanexangtourism.Adapter.SpinnerDistrictAdapter;
import com.ipanda.lanexangtourism.Adapter.SpinnerProvinceAdapter;
import com.ipanda.lanexangtourism.Adapter.SpinnerSubDistrictAdapter;
import com.ipanda.lanexangtourism.DialogFragment.AwaitingReviewDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.DistrictsModel;
import com.ipanda.lanexangtourism.Model.ItemsDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.SubDistrictsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.CategoryAsyncTask;
import com.ipanda.lanexangtourism.asynctask.CategorySubAsyncTask;
import com.ipanda.lanexangtourism.asynctask.DistrictsAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ProvinceAsyncTask;
import com.ipanda.lanexangtourism.asynctask.SubDistrictsAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.httpcall.HttpJsonCall;
import com.ipanda.lanexangtourism.interface_callback.CategoryCallBack;
import com.ipanda.lanexangtourism.interface_callback.CategorySubCallBack;
import com.ipanda.lanexangtourism.interface_callback.DistrictCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProvinceCallBack;
import com.ipanda.lanexangtourism.interface_callback.SubDistrictCallBack;
import com.ipanda.lanexangtourism.post_booking.HttpPostProgramTour;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class NewPlacesActivity extends AppCompatActivity implements CategoryCallBack, CategorySubCallBack, ProvinceCallBack, DistrictCallBack, SubDistrictCallBack, OnMapReadyCallback {

    //variables
    private static String TAG = NewPlacesActivity.class.getSimpleName();

    private static final int REQUEST_GALLERY_PHOTO = 101;

    private static final int REQUEST_CODE = 102;

    private static String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};

    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    private boolean isChangedLatitude = false;

    private boolean isChangedLongitude = false;

    private LatLng lng;

    private double latitude;

    private double longitude;

    private MapView mapView;

    private GoogleMap googleMap;

    private ItemsModel itemsModel;

    private ItemsDetailModel itemsDetail;

    private ArrayList<ItemsDetailModel> detailModelArrayList;

    private ArrayList<ItemsPhotoDetailModel> photoArrayList;

    private ArrayList<ItemsPhotoDetailModel> photoInterestingArrayList;

    private ChangeLanguageLocale languageLocale;

    private Bitmap bitmap;

    private String userId = null;

    private String urlGetCategory = null, urlGetSubCategory = null, urlGetProvince = null, urlGetDistricts = null, urlGetSubDistricts;

    private EditText edtTopicItems;

    private EditText edt_latitude;

    private EditText edt_longitude;

    private Spinner spinnerCategory;

    private Spinner spinnerSubCategory;

    private Spinner spinnerProvince;

    private Spinner spinnerDistrict;

    private Spinner spinnerSubDistrict;

    private SpinnerCategoryAdapter spinnerCategoryAdapter;

    private SpinnerCategorySubAdapter spinnerSubCategoryAdapter;

    private SpinnerProvinceAdapter spinnerProvinceAdapter;

    private SpinnerDistrictAdapter spinnerDistrictAdapter;

    private SpinnerSubDistrictAdapter spinnerSubDistrictAdapter;

    private ConstraintLayout con_open_id, con_closed_id;

    private TextView timeOpen, timeClosed;

    private EditText edt_details_items;

    private EditText edt_contact_id;

    private EditText edt_phone;

    private EditText edt_contact_phone;

    private EditText edt_contact_phone_social;

    private EditText edt_line;

    private EditText edt_facebook;

    private EditText edt_interesting_detail;

    private EditText edt_travel_detail;

    private String isSaveItems = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_places);
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        checkPermission();

        //views
        mapView = findViewById(R.id.mapView_location_id);
        spinnerCategory = findViewById(R.id.spinner_new_places_category);
        spinnerSubCategory = findViewById(R.id.spinner_new_places_sub_category);
        spinnerProvince = findViewById(R.id.spinner_location_of_province);
        spinnerDistrict = findViewById(R.id.spinner_district_location);
        spinnerSubDistrict = findViewById(R.id.spinner_subdistrict_location);
        edtTopicItems = findViewById(R.id.edt_topic_items);
        edt_latitude = findViewById(R.id.edt_latitude);
        edt_longitude = findViewById(R.id.edt_longitude);
        con_open_id = findViewById(R.id.con_open_id);
        con_closed_id = findViewById(R.id.con_closed_id);
        timeOpen = findViewById(R.id.txt_time_open_id);
        timeClosed = findViewById(R.id.txt_time_closed_id);
        edt_contact_id = findViewById(R.id.edt_contact_id);
        edt_contact_phone = findViewById(R.id.edt_contact_phone);
        edt_contact_phone_social = findViewById(R.id.edt_contact_phone_social);
        edt_line = findViewById(R.id.edt_line);
        edt_facebook = findViewById(R.id.edt_facebook);
        edt_phone = findViewById(R.id.edt_phone);
        edt_interesting_detail = findViewById(R.id.edt_interesting_detail);
        edt_travel_detail = findViewById(R.id.edt_travel_detail_id);
        edt_details_items = findViewById(R.id.edt_details_items);


        //new object
        itemsModel = new ItemsModel();
        detailModelArrayList = new ArrayList<>();
        photoArrayList = new ArrayList<>();
        photoInterestingArrayList = new ArrayList<>();

        //setup asyncTask
        urlGetCategory = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ProgramTour/getNameItemCategory";
        urlGetProvince = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/ProvinceGroup";

        new CategoryAsyncTask(this).execute(urlGetCategory);
        new ProvinceAsyncTask(this).execute(urlGetProvince);

        //setup google maps
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

        //get lat long
        getLatitudeLongitudeEditText();


        // Initialize the SDK
        String apiKey = "AIzaSyDrdQG5bfvPftYnMMdd8dOdgcl1GvODN6U";
        Places.initialize(getApplicationContext(), apiKey);

        // Create a new PlacesClient instance
        PlacesClient placesClient = Places.createClient(this);

        // Setup Search Places
        setupSearchPlaces();

        // Setup event onClick TimeOpen and TimeClose
        setupEventOnClickTimeOpenAndClose();

    }

    private void setupTextToItems() {
        itemsModel.setContactTH(edt_contact_id.getText().toString());
        itemsModel.setContactEN("");
        itemsModel.setContactLO("");
        itemsModel.setContactZH("");
        itemsModel.setTimeOpen(timeOpen.getText().toString());
        itemsModel.setTimeClose(timeClosed.getText().toString());
        itemsModel.setPhone(edt_contact_phone.getText().toString());
        itemsModel.setEmail(edt_contact_phone_social.getText().toString());
        itemsModel.setLine(edt_line.getText().toString());
        itemsModel.setFacebookPage(edt_facebook.getText().toString());
    }

    private void setupSearchPlaces() {
        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NotNull Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() + " Lat Long "+ place.getLatLng());

                lng = place.getLatLng();
                edt_latitude.setText(lng.latitude + "");
                edt_longitude.setText(lng.longitude + "");
                itemsModel.setLatitude(edt_latitude.getText().toString());
                itemsModel.setLongitude(edt_longitude.getText().toString());
                setupMarkerLocation(lng);
            }


            @Override
            public void onError(@NotNull Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });


    }

    private void getLatitudeLongitudeEditText() {

        edt_latitude.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isChangedLatitude) {
                    // do stuff
                    if (editable.toString().equals("")) {
                        latitude = 0;
                    }else {
                        latitude = Double.parseDouble(editable.toString());
                    }
                } else {
                    isChangedLatitude = false;
                }
            }
        });

        edt_longitude.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isChangedLongitude) {
                    // do stuff
                    if (editable.toString().equals("")) {
                        longitude = 0;
                    }else {
                        longitude = Double.parseDouble(editable.toString());
                        if (latitude != 0 && longitude != 0) {
                            lng = new LatLng(latitude, longitude);
                            if (lng != null) {
                                setupMarkerLocation(lng);
                            }
                        }
                    }
                } else {
                    isChangedLongitude = false;
                }
            }
        });

        itemsModel.setLatitude(edt_latitude.getText().toString());
        itemsModel.setLongitude(edt_longitude.getText().toString());
    }

    private void setupMarkerLocation(LatLng lng){
        if (lng != null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(lng);
            googleMap.addMarker(markerOptions);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(lng));
        }
    }

    private void setupEventOnClickTimeOpenAndClose() {

        con_open_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new  TimePickerOpen(timeOpen);
                newFragment.show(getSupportFragmentManager(), "timePickerOpen");
            }
        });

        con_closed_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new TimePickerClosed(timeClosed);
                newFragment.show(getSupportFragmentManager(), "timePickerClosed");
            }
        });

    }


    public void itemsClickedAddPhotoToItems(View view){
        int REQ_CODE = 2001;
        Intent intent = new Intent(this, AddPhotoToItemsActivity.class);
        startActivityForResult(intent,REQ_CODE);
    }

    public void  itemsClickAddPhotoToInterestingDetail(View view){
        int REQ_CODE = 2002;
        Intent intent = new Intent(this, AddPhotoToItemsDetailsActivity.class);
        startActivityForResult(intent,REQ_CODE);
    }

    public void onClickBackPressedNewPlaces(View view){
        onBackPressed();
    }

    public void onClickBrowseImageToCoverItems(View view){
        if (checkPermission()){
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
        }else {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(this, mPermission);
            if (result == PackageManager.PERMISSION_DENIED){
                ActivityCompat.requestPermissions(this, new String[] {mPermission}, REQUEST_CODE);
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2001) {
            if (resultCode == Activity.RESULT_OK) {
                photoArrayList = data.getParcelableArrayListExtra("PHOTO_ITEMS");
            }
        }else if (requestCode == REQUEST_GALLERY_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uriImg = data.getData();
                ImageView imageView = findViewById(R.id.img_cover_items);
                Glide.with(this).load(uriImg).apply(new RequestOptions()).into(imageView);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriImg);
                    imageView.setImageBitmap(bitmap);
                    ByteArrayOutputStream byteArrayOutputStreamObject ;
                    byteArrayOutputStreamObject = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
                    byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
                    final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
                    CoverItemsModel cover = new CoverItemsModel();
                    cover.setCoverPaths(ConvertImage);
                    itemsModel.setCoverItemsModel(cover);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else if (requestCode == 2002){
            if (resultCode == Activity.RESULT_OK){
                photoInterestingArrayList = data.getParcelableArrayListExtra("PHOTO_ITEMS");
            }
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerProvinces(ArrayList<ProvincesModel> provincesList) {
        if (provincesList != null && provincesList.size() != 0) {
            Log.e("check data", provincesList + "");
            provincesList.add(0,new ProvincesModel("จังหวัด *","Provinces *","ບັນດາແຂວງ *","省份 *"));
            spinnerProvinceAdapter = new SpinnerProvinceAdapter(this, provincesList, languageLocale.getLanguage());
            spinnerProvince.setAdapter(spinnerProvinceAdapter);
            spinnerProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    urlGetDistricts = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/countries/Districts?provinces_id="+provincesList.get(i).getProvincesId();
                    itemsModel.setProvincesModel(new ProvincesModel(provincesList.get(i).getProvincesId()));
                    new DistrictsAsyncTask(NewPlacesActivity.this).execute(urlGetDistricts);

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


        }else {

        }
    }

    @Override
    public void onRequestCompleteListenerDistrict(ArrayList<DistrictsModel> districtsList) {
        if (districtsList != null && districtsList.size() != 0){
            Log.e("check data", districtsList + "");
            districtsList.add(0,new DistrictsModel("อำเภอ *","Districts","ເມືອງຕ່າງໆ","地区"));
            spinnerDistrictAdapter = new SpinnerDistrictAdapter(this, districtsList, languageLocale.getLanguage());
            spinnerDistrict.setAdapter(spinnerDistrictAdapter);
            spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    urlGetSubDistricts =  getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/countries/Subdistricts?districts_id="+districtsList.get(i).getDistrictsId();
                    itemsModel.getProvincesModel().setDistrictsModel(new DistrictsModel(districtsList.get(i).getDistrictsId()));
                    new SubDistrictsAsyncTask(NewPlacesActivity.this).execute(urlGetSubDistricts);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }else {

        }
    }

    @Override
    public void onRequestCompleteListenerSubDistricts(ArrayList<SubDistrictsModel> subDistrictsList) {
        if (subDistrictsList != null && subDistrictsList.size() != 0){
            Log.e("check data", subDistrictsList + "");
            subDistrictsList.add(0,new SubDistrictsModel("อำเภอ *","Districts","ເມືອງຕ່າງໆ","地区"));
            spinnerSubDistrictAdapter = new SpinnerSubDistrictAdapter(this, subDistrictsList, languageLocale.getLanguage());
            spinnerSubDistrict.setAdapter(spinnerSubDistrictAdapter);
            spinnerSubDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    itemsModel.getProvincesModel().getDistrictsModel().setSubDistrictsModel(new SubDistrictsModel(subDistrictsList.get(i).getSubDistrictsId()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }else {

        }
    }

    @Override
    public void onRequestCompleteListenerCategory(ArrayList<CategoryModel> categoryArrayList) {
        if (categoryArrayList != null && categoryArrayList.size() != 0){
            Log.e("check data", categoryArrayList+"");
            categoryArrayList.add(0,new CategoryModel("ทั้งหมด","All","ທັງ ໝົດ","所有"));
            spinnerCategoryAdapter = new SpinnerCategoryAdapter(this, categoryArrayList, languageLocale.getLanguage());
            spinnerCategory.setAdapter(spinnerCategoryAdapter);
            spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    urlGetSubCategory = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ProgramTour/getNameSubCategoryById?category_id="+categoryArrayList.get(position).getCategoryId();
                    new CategorySubAsyncTask(NewPlacesActivity.this).execute(urlGetSubCategory);
                    itemsModel.setCategoryModel(new CategoryModel(categoryArrayList.get(position).getCategoryId()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }else {

        }
    }

    @Override
    public void onRequestCompleteListenerCategorySub(CategoryModel categoryModel) {
        if (categoryModel != null){
            Log.e("check data" , categoryModel+"");
            spinnerSubCategoryAdapter = new SpinnerCategorySubAdapter(this, categoryModel.getSubCategoryModelArrayList(), languageLocale.getLanguage());
            spinnerSubCategory.setAdapter(spinnerSubCategoryAdapter);
            spinnerSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    itemsModel.getCategoryModel().setSubCategoryModel(new SubCategoryModel(categoryModel.getSubCategoryModelArrayList().get(i).getCategoryId()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }else {

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        googleMap.setMinZoomPreference(12);

        googleMap.setIndoorEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(false);

    }


    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public static class TimePickerOpen extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        private TextView timeOpen;

        public TimePickerOpen(TextView timeOpen) {
            this.timeOpen = timeOpen;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }


        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onTimeSet(TimePicker timePicker, int i, int i1) {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            final Date date = new Date();
            date.setHours(timePicker.getHour());
            date.setMinutes(timePicker.getMinute());

            String formatted = format.format(date);
            timeOpen.setText(formatted);
        }

    }

    public static class TimePickerClosed extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        private TextView timeClosed;

        public TimePickerClosed(TextView timeClosed) {
            this.timeClosed = timeClosed;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }


        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onTimeSet(TimePicker timePicker, int i, int i1) {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            final Date date = new Date();
            date.setHours(timePicker.getHour());
            date.setMinutes(timePicker.getMinute());

            String formatted = format.format(date);
            timeClosed.setText(formatted);
        }

    }

    private boolean getEditText() {
        if (TextUtils.isEmpty(edtTopicItems.getText().toString())) {
            Toast.makeText(this, "Empty field not allowed!", Toast.LENGTH_SHORT).show();
            Selection.setSelection((Editable) edtTopicItems.getText(),edtTopicItems.getSelectionStart());
            edtTopicItems.requestFocus();
            return false;
        } else {
            itemsModel.setTopicTH(edtTopicItems.getText().toString());
            itemsModel.setTopicEN("");
            itemsModel.setTopicLO("");
            itemsModel.setTopicZH("");
        }
        return true;
    }

    private void getEditTextSetItemsModel(){
        itemsDetail = new ItemsDetailModel();
        itemsDetail.setDetailId(1);
        itemsDetail.setDetailTH(edt_details_items.getText().toString());
        itemsDetail.setDetailEN("");
        itemsDetail.setDetailLO("");
        itemsDetail.setDetailZH("");
        if (photoArrayList != null && photoArrayList.size() != 0){
            itemsDetail.setItemsPhotoDetailModels(photoArrayList);
        }
        detailModelArrayList.add(itemsDetail);
    }

    private void getEditTextSetItemsInterestingDetail(){
        itemsDetail = new ItemsDetailModel();
        itemsDetail.setDetailId(2);
        itemsDetail.setDetailTH(edt_interesting_detail.getText().toString());
        itemsDetail.setDetailEN("");
        itemsDetail.setDetailLO("");
        itemsDetail.setDetailZH("");
        if (photoInterestingArrayList != null && photoInterestingArrayList.size() != 0){
            itemsDetail.setItemsPhotoDetailModels(photoInterestingArrayList);
        }
        detailModelArrayList.add(itemsDetail);
    }

    private void getEditTextSetItemsTravelDetail(){
        itemsDetail = new ItemsDetailModel();
        itemsDetail.setDetailId(3);
        itemsDetail.setDetailTH(edt_travel_detail.getText().toString());
        itemsDetail.setDetailEN("");
        itemsDetail.setDetailLO("");
        itemsDetail.setDetailZH("");
        detailModelArrayList.add(itemsDetail);
    }



    public void btnSaveDaftNewPlaces(View view){
        isSaveItems = "1";
        getEditTextSetItemsModel();
        getEditTextSetItemsInterestingDetail();
        getEditTextSetItemsTravelDetail();
        setupTextToItems();
        itemsModel.setItemsDetailModelArrayList(detailModelArrayList);
        if (getEditText()){
            setupHttpPost();
        }else {

        }
    }

    public void btnSaveAsNewPlaces(View view){
        isSaveItems = "0";
    }


    private void setupHttpPost(){
        if (userId != null){
            HttpJsonCall httpCallPost = new HttpJsonCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "");

            JSONObject arrayNull = new JSONObject();
            if (itemsModel != null) {

                JSONObject jsonItems = new JSONObject();

                try {
                    JSONObject jsItems = new JSONObject();
                    jsItems.put("itmes_topicThai",itemsModel.getTopicTH());
                    jsItems.put("itmes_topicEnglish",itemsModel.getTopicEN());
                    jsItems.put("itmes_topicLaos",itemsModel.getTopicLO());
                    jsItems.put("itmes_topicChinese",itemsModel.getTopicZH());
                    jsItems.put("items_latitude",itemsModel.getLatitude());
                    jsItems.put("items_longitude",itemsModel.getLongitude());
                    jsItems.put("items_contactThai",itemsModel.getContactTH());
                    jsItems.put("items_contactEnglish",itemsModel.getContactEN());
                    jsItems.put("items_contactLaos",itemsModel.getContactLO());
                    jsItems.put("items_contactChinese",itemsModel.getContactZH());
                    jsItems.put("items_timeOpen",itemsModel.getTimeOpen());
                    jsItems.put("items_timeClose",itemsModel.getTimeClose());
                    jsItems.put("items_isSaveDraft", isSaveItems);
                    if (itemsModel.getCoverItemsModel() != null) {
                        jsItems.put("coverItem_paths", itemsModel.getCoverItemsModel().getCoverPaths());
                    }else {
                        jsItems.put("coverItem_paths", "");
                    }
                    jsItems.put("items_email",itemsModel.getEmail());
                    jsItems.put("items_line",itemsModel.getLine());
                    jsItems.put("items_facebookPage",itemsModel.getFacebookPage());
                    jsItems.put("items_phone",itemsModel.getPhone());
                    jsItems.put("provinces_id",itemsModel.getProvincesModel().getProvincesId());
                    jsItems.put("districts_id",itemsModel.getProvincesModel().getDistrictsModel().getDistrictsId());
                    jsItems.put("subdistricts_id",itemsModel.getProvincesModel().getDistrictsModel().getSubDistrictsModel().getSubDistrictsId());


                    if (itemsModel.getItemsDetailModelArrayList() != null && itemsModel.getItemsDetailModelArrayList().size() != 0){
                        JSONArray jsonDetail = new JSONArray();
                        for (ItemsDetailModel id : itemsModel.getItemsDetailModelArrayList()){
                            JSONObject jsDetail = new JSONObject();
                            jsDetail.put("detail_id", id.getDetailId());
                            if(id.getDetailTH() != null){
                                jsDetail.put("detail_textThai",id.getDetailTH());
                            }else {
                                jsDetail.put("detail_textThai","");
                            }
                            jsDetail.put("detail_textEnglish", "");
                            jsDetail.put("detail_textLaos", "");
                            jsDetail.put("detail_textChinese", "");

                            if (id.getItemsPhotoDetailModels() != null && id.getItemsPhotoDetailModels().size() != 0) {
                                JSONArray jsonPhoto = new JSONArray();
                                for (ItemsPhotoDetailModel ipd : id.getItemsPhotoDetailModels()) {
                                    JSONObject jsPhoto = new JSONObject();

                                    try {
                                        ByteArrayOutputStream byteArrayOutputStreamObject;
                                        byteArrayOutputStreamObject = new ByteArrayOutputStream();
                                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(ipd.getPhotoPaths())));
                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
                                        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
                                        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
                                        jsPhoto.put("photo_paths", ConvertImage);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

//                                    jsPhoto.put("photo_paths", ipd.getPhotoPaths());

                                    if (ipd.getPhotoTextTH() != null){
                                        jsPhoto.put("photo_textThai", ipd.getPhotoTextTH());
                                    }else {
                                        jsPhoto.put("photo_textThai", "");
                                    }
                                    jsPhoto.put("photo_textEnglish", "");
                                    jsPhoto.put("photo_textLaos", "");
                                    jsPhoto.put("photo_textChinese", "");
                                    jsonPhoto.put(jsPhoto);
                                }
                                jsDetail.put("Photo", jsonPhoto);
                            }else {
                                jsDetail.put("Photo", arrayNull );
                            }
                            jsonDetail.put(jsDetail);
                        }
                        jsItems.put("Detail", jsonDetail);
                    }else {
                        jsItems.put("Detail", arrayNull );
                    }


                    jsonItems.put("Items", jsItems);
                    Log.e("data json", jsonItems.toString());
                    httpCallPost.setParams(jsonItems);
                    new HttpPostProgramTour().execute(httpCallPost);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                DialogFragment dialogFragment = AwaitingReviewDialogFragment.newInstance();
                dialogFragment.show(getSupportFragmentManager(), TAG);
            }

        }else {
            startActivity(new Intent(this, LoginActivity.class));
        }

    }
}

