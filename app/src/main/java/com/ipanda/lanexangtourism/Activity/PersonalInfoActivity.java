package com.ipanda.lanexangtourism.Activity;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ipanda.lanexangtourism.Adapter.CountySpinnerAdapter;
import com.ipanda.lanexangtourism.Adapter.GenderSpinnerAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.CountryModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.CountryAsyncTask;
import com.ipanda.lanexangtourism.asynctask.PersonalInfoPostRequest;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.CountryCallBack;
import com.ipanda.lanexangtourism.interface_callback.IsStatePersonalInfoCallBack;

import java.util.ArrayList;
import java.util.HashMap;

public class PersonalInfoActivity extends AppCompatActivity implements View.OnClickListener , IsStatePersonalInfoCallBack , CountryCallBack  {

    //Variables
    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private EditText edtCitizenId, edtFirstName, edtLastName, edtAge,edt_email_id;
    private TextView txtGender, txtCountry;
    private Button btnConfirm;

    private UserModel userModel;

    private String citizenId, firstName, lastName, age, gender ,country, loginWith, email;

    private String url;

    private Spinner mSpinnerCountry, mSpinnerGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.menu_toolbar);
        btnConfirm = findViewById(R.id.btn_confirm);
        edtCitizenId = findViewById(R.id.edt_citizen_id);
        edtFirstName = findViewById(R.id.edt_first_name);
        edtLastName = findViewById(R.id.edt_last_name);
        edt_email_id = findViewById(R.id.edt_email_id);
        edtAge = findViewById(R.id.edt_age);
        mSpinnerCountry = findViewById(R.id.spinner_country);
        mSpinnerGender = findViewById(R.id.spinner_gender);


        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //set on click button
        btnConfirm.setOnClickListener(this);

        //set spinner gender
        setSpinnerGender();

        url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/countries/Country";
        new CountryAsyncTask(this).execute(url);

        if (getIntent() != null){
            userModel = getIntent().getParcelableExtra("SEND_USER_MODEL");
            loginWith = getSharedPreferences("PREF_APP_UserLoginType", Context.MODE_PRIVATE).getString("APP_UserLoginType", "");
        }
    }

    private void setSpinnerGender() {
        ArrayList<String> gen = new ArrayList<>();
        gen.add(0,getResources().getString(R.string.text_sex));
        gen.add("Male");
        gen.add("Female");
        GenderSpinnerAdapter adapter = new GenderSpinnerAdapter(this,gen);
        mSpinnerGender.setAdapter(adapter);
        mSpinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender = gen.get(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_confirm:
                getTextPersonalInfo();
                methodPost();
                break;
        }
    }

    private boolean getTextPersonalInfo() {
        if (TextUtils.isEmpty(edtCitizenId.getText().toString())){
            Toast.makeText(this, "Empty field not allowed! Citizen ID", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            citizenId = edtCitizenId.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(edt_email_id.getText().toString())){
            Toast.makeText(this, "Empty field not allowed! Email", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            email = edt_email_id.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(edtFirstName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed! First Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            firstName = edtFirstName.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(edtLastName.getText().toString())){
            Toast.makeText(this, "Empty field not allowed! Last Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            lastName = edtLastName.getText().toString().replaceAll(" ", "");
        }
        if (TextUtils.isEmpty(edtAge.getText().toString())){
            Toast.makeText(this, "Empty field not allowed! Age", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            age = edtAge.getText().toString().replaceAll(" ", "");
        }
        return true;
    }

    private void methodPost(){

        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/User");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("user_identification",citizenId);
        paramsPost.put("user_token",userModel.getToken());
        paramsPost.put("user_email",email);
        paramsPost.put("user_firstName",firstName);
        paramsPost.put("user_lastName",lastName);
        paramsPost.put("user_age",age);
        paramsPost.put("user_gender",gender);
        paramsPost.put("user_loginWith",loginWith);
        paramsPost.put("Country_country_id",country);
        paramsPost.put("user_profile_pic_url",userModel.getProfilePicUrl());

        if (getTextPersonalInfo()){
            httpCallPost.setParams(paramsPost);
            new PersonalInfoPostRequest(this).execute(httpCallPost);
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(boolean state, String userId) {
        if (state){
            Toast.makeText(this, "True", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, MainActivity.class));
            getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", true).apply();
            getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).edit().putString("APP_USER_ID", userId).apply();
        }else {
            Toast.makeText(this, "False", Toast.LENGTH_SHORT).show();
            getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", false).apply();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("ข้อมูลผิดพลาด")
                    .setTitle("เลขที่บัตรจำประตัวประชาชน / พาสปอร์ต (ซํ้า) ")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            builder.show();
        }
    }

    @Override
    public void onRequestCompleteListenerCountry(ArrayList<CountryModel> countryModel) {
        if (countryModel != null && countryModel.size() != 0){
            CountryModel con = new CountryModel(this);
            switch (languageLocale.getLanguage()){
                case "th":
                    con.setCountryTH(getResources().getString(R.string.text_your_country));
                    break;
                case "en":
                    con.setCountryEN(getResources().getString(R.string.text_your_country));
                    break;
                case "lo":
                    con.setCountryLO(getResources().getString(R.string.text_your_country));
                    break;
                case "zh":
                    con.setCountryZH(getResources().getString(R.string.text_your_country));
                    break;

            }
            countryModel.add(0,con);

            CountySpinnerAdapter adapter = new CountySpinnerAdapter(this,countryModel);
            mSpinnerCountry.setAdapter(adapter);
            mSpinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    country = String.valueOf(countryModel.get(position).getCountryId());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }




    @Override
    public void onRequestFailed(String result) {

    }


}
