package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.ipanda.lanexangtourism.Adapter.ProgramTabViewPagerAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.ProgramFragment.ProgramAllFragment;
import com.ipanda.lanexangtourism.ProgramFragment.ProgramDraftFragment;
import com.ipanda.lanexangtourism.ProgramFragment.ProgramPendingFragment;
import com.ipanda.lanexangtourism.ProgramFragment.ProgramPublishFragment;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.MyProgramTourAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.MyProgramTourCallBack;

import java.util.ArrayList;

public class PersonnelProgramActivity extends AppCompatActivity implements View.OnClickListener , MyProgramTourCallBack {

    //Variables
    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private TabLayout mTabLayout;

    private ViewPager mViewPager;

    private String userId;

    private ProgramTabViewPagerAdapter mAdapter;

    private Fragment programAllFragment = new ProgramAllFragment();
    private Fragment programDraftFragment = new ProgramDraftFragment();
    private Fragment programPendingFragment = new ProgramPendingFragment();
    private Fragment programPublishFragment = new ProgramPublishFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personnel_program);

        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.personnel_program_toolbar);
        mTabLayout = findViewById(R.id.program_tabLayout);
        mViewPager = findViewById(R.id.program_viewPager);


        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (userId != null) {
            String url = getString(R.string.app_api_ip)+ "dasta_thailand/api/mobile/user/ProgramTour/getEditProgramTour?user_id="+userId;
            new MyProgramTourAsyncTask(this).execute(url);
        }


    }

    private void setupViewPager(ViewPager viewPager, ArrayList<ProgramTourModel> programTourModelArrayList){
        mAdapter = new ProgramTabViewPagerAdapter(getSupportFragmentManager(),0);
        mAdapter.addFragment(programAllFragment,getString(R.string.text_all), programTourModelArrayList);
        mAdapter.addFragment(programDraftFragment,getString(R.string.text_draft),programTourModelArrayList);
        mAdapter.addFragment(programPendingFragment,getString(R.string.text_pending_approval),programTourModelArrayList);
        mAdapter.addFragment(programPublishFragment,getString(R.string.text_published),programTourModelArrayList);
        viewPager.setAdapter(mAdapter);

    }

    public void onClickCreateProgramTour(View view){
        startActivity(new Intent(this, ProgramTourCreateActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ProgramTourModel> programTourModelArrayList) {
        if (programTourModelArrayList != null && programTourModelArrayList.size() != 0){

            //setup ViewPager
            setupViewPager(mViewPager, programTourModelArrayList);

            //setup TableLayout
            mTabLayout.setupWithViewPager(mViewPager);

            mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });


        }else {

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

}
