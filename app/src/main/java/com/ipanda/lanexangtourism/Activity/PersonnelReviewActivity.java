package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ipanda.lanexangtourism.Adapter.ReviewsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ReviewClickListener;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ReviewsAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.ReviewsCallBack;

import java.util.ArrayList;

public class PersonnelReviewActivity extends AppCompatActivity implements View.OnClickListener, ReviewsCallBack , ReviewClickListener {

    //Variables
    private Toolbar mToolbar;

    private RecyclerView recycler_reviews;

    private ReviewsRecyclerViewAdapter reviewAdapter;

    private ChangeLanguageLocale languageLocale;

    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personnel_review);

        //views
        mToolbar = findViewById(R.id.personnel_review_toolbar);

        recycler_reviews = findViewById(R.id.recycler_reviews);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }


        if (getIntent() != null){
            userId = getIntent().getStringExtra("USER_ID");
        }

        if (userId != null) {
            String url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Review?user_id="+userId;
            new ReviewsAsyncTask(this).execute(url);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteReviews(ArrayList<ReviewModel> reviewArrayList) {
        if (reviewArrayList != null && reviewArrayList.size() != 0){
            Log.e("check data", reviewArrayList+"");

            reviewAdapter = new ReviewsRecyclerViewAdapter(this, reviewArrayList, languageLocale.getLanguage(),this, true);
            recycler_reviews.setLayoutManager(new LinearLayoutManager(this));
            recycler_reviews.setAdapter(reviewAdapter);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClicked(View view, ReviewModel reviewModel) {

    }

    @Override
    public void itemClickedDelete(View view, ReviewModel reviewModel, int position) {

    }

    @Override
    public void itemClickedProfile(View view, ReviewModel reviewModel) {

    }
}
