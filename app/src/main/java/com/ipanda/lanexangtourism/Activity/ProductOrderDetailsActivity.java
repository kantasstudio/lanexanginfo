package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.ProductListAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;

public class ProductOrderDetailsActivity extends AppCompatActivity implements ExchangeRateCallBack {

    //variables
    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private TextView toolbar_title;

    private BookingModel booking;

    private RecyclerView recyclerProduct;

    private ProductListAdapter productListAdapter;

    private ImageView img_booking_status;

    private TextView txt_booking_status;

    private TextView txt_product_price_id;

    private TextView txt_contact_phone_id;

    private TextView txt_exchange_rate_detail;

    private RateModel rateModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_order_details);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        String urlRate = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ExchangeRate";
        new ExchangeRateAsyncTask(this).execute(urlRate);

        if (getIntent() != null){
            booking = getIntent().getParcelableExtra("Booking");
        }


        //views
        mToolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        recyclerProduct = findViewById(R.id.recycler_product_list_id);
        img_booking_status = findViewById(R.id.img_booking_status);
        txt_booking_status = findViewById(R.id.txt_booking_status);
        txt_product_price_id = findViewById(R.id.txt_product_price_id);
        txt_contact_phone_id = findViewById(R.id.txt_contact_phone_id);
        txt_exchange_rate_detail = findViewById(R.id.txt_exchange_rate_id);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;

            if (booking != null){
                switch (languageLocale.getLanguage()){
                    case "th":
                        toolbar_title.setText(booking.getItemsModel().getTopicTH());
                        break;
                    case "en":
                        toolbar_title.setText(booking.getItemsModel().getTopicEN());
                        break;
                    case "lo":
                        toolbar_title.setText(booking.getItemsModel().getTopicLO());
                        break;
                    case "zh":
                        toolbar_title.setText(booking.getItemsModel().getTopicZH());
                        break;
                }
            }


            productListAdapter = new ProductListAdapter(this,booking.getProductModelArrayList() ,languageLocale.getLanguage(),txt_product_price_id,rateModel, txt_exchange_rate_detail);
            recyclerProduct.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            recyclerProduct.setAdapter(productListAdapter);

            if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 1){
                img_booking_status.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_booked));
                txt_booking_status.setTextColor(getResources().getColor(R.color.status_booked));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 2){
                img_booking_status.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_wait));
                txt_booking_status.setTextColor(getResources().getColor(R.color.status_wait));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 3){
                img_booking_status.setImageDrawable(getResources().getDrawable(R.drawable.ic_status_refuse));
                txt_booking_status.setTextColor(getResources().getColor(R.color.status_refuse));
            }else {

            }

            txt_contact_phone_id.setText(getResources().getString(R.string.text_contact_number)+" "+booking.getItemsModel().getPhone());
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }
}