package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import com.ipanda.lanexangtourism.Adapter.ProductOrderListAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.OrderDetailsClickListener;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class ProductOrderListActivity extends AppCompatActivity implements OrderDetailsClickListener {

    //variables

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private ArrayList<BookingModel> bookingArrayList;

    private RecyclerView recyclerOrdersList;

    private ProductOrderListAdapter orderListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_order_list);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        if (getIntent() != null){
            bookingArrayList = getIntent().getParcelableArrayListExtra("Booking_Order");
        }

        //views
        mToolbar = findViewById(R.id.toolbar);
        recyclerOrdersList = findViewById(R.id.recycler_orders_list);

        orderListAdapter = new ProductOrderListAdapter(this, bookingArrayList, languageLocale.getLanguage(), this);
        recyclerOrdersList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerOrdersList.setAdapter(orderListAdapter);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void itemClickedOrder(BookingModel booking) {
        Intent intent = new Intent(this, ProductOrderDetailsActivity.class);
        intent.putExtra("Booking", booking);
        startActivity(intent);
    }
}