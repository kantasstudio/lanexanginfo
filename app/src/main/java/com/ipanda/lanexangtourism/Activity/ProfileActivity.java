package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.ipanda.lanexangtourism.Adapter.ProfileTabViewPagerAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.ProfileFragment.MyBookingFragment;
import com.ipanda.lanexangtourism.ProfileFragment.ProfileFragment;
import com.ipanda.lanexangtourism.R;

public class ProfileActivity extends AppCompatActivity {

    //Variables
    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private UserModel userModel;
    private ChangeLanguageLocale languageLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.Profile_Toolbar);
        mTabLayout = findViewById(R.id.Profile_Tablayout);
        mViewPager = findViewById(R.id.Profile_Viewpager);


        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }



        if (getIntent() != null){
            userModel = getIntent().getParcelableExtra("USER_MODEL");
        }

        //setup ViewPager
        setupViewPager(mViewPager);

        //setup TableLayout
        mTabLayout.setupWithViewPager(mViewPager);

        //set on click button
    }


    private void setupViewPager(ViewPager viewPager){
        Bundle bundle = new Bundle();
        bundle.putParcelable("USER_MODEL",userModel);
        ProfileTabViewPagerAdapter mAdapter = new ProfileTabViewPagerAdapter(getSupportFragmentManager(),0);

        ProfileFragment profileFragment = new ProfileFragment();
//        profileFragment.setArguments(bundle);

        MyBookingFragment myBookingFragment = new MyBookingFragment();
//        myBookingFragment.setArguments(bundle);

        mAdapter.addFragment(profileFragment,getString(R.string.text_my_profile));
        mAdapter.addFragment(myBookingFragment,getString(R.string.txt_setting_booking));
        viewPager.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
