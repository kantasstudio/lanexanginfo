package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agrawalsuneet.loaderspack.loaders.MultipleRippleLoader;
import com.ipanda.lanexangtourism.Adapter.ProgramTourRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ProgramTourClickListener;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ProgramTourAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ProgramTourCallBack;
import com.ipanda.lanexangtourism.items_view.ViewProgramTourActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class ProgramTourActivity extends AppCompatActivity implements ProgramTourCallBack , ProgramTourClickListener {

    //Variables
    private Toolbar mToolbar;

    private RecyclerView recycler_program_tour;

    private ProgramTourRecyclerViewAdapter tourAdapter;

    private TextView txt_title_search_filter;

    private ChangeLanguageLocale languageLocale;

    private MultipleRippleLoader multipleRippleLoader;

    private LinearLayout show_not_found;

    private String userId;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    private ArrayList<ProgramTourModel> newProgramTourOffline = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_tour);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.program_tour_toolbar);
        recycler_program_tour = findViewById(R.id.recycler_program_tour);
        txt_title_search_filter = findViewById(R.id.txt_title_search_filter);
        multipleRippleLoader = findViewById(R.id.multipleRippleLoader);
        show_not_found = findViewById(R.id.show_not_found);
        txt_title_search_filter.setText(getString(R.string.text_search)+""+  getString(R.string.txt_program_tour));

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }



        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        isModeOnline = getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        if (isModeOnline) {
            String url = "";
            if (userId != null) {
                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ProgramTour?user_id=" + userId;
            } else {
                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ProgramTour/";
            }
            new ProgramTourAsyncTask(this).execute(url);
        }else {
            getVersion = getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
            if (getVersion != 0) {
                mDatabase = new DatabaseHelper(this, getVersion);
                mManager = new DatabaseManager(this, mDatabase);
                setItemsOfflineMode();
            }else {
                alertDialog();
            }
        }

    }

    private void setItemsOfflineMode() {
        newProgramTourOffline = mManager.getListProgramTours();
        if (newProgramTourOffline != null && newProgramTourOffline.size() != 0){
            tourAdapter = new ProgramTourRecyclerViewAdapter(this,newProgramTourOffline,languageLocale.getLanguage(),this);
            recycler_program_tour.setLayoutManager(new LinearLayoutManager(this));
            recycler_program_tour.setAdapter(tourAdapter);
            recycler_program_tour.setVisibility(View.VISIBLE);
        }
    }


    public void onClickSearchProgramTour(View view){
        if (isModeOnline) {
            startActivity(new Intent(this, ItemsSearchActivity.class));
        }
    }

    public void onClickCreateProgramTour(View view){
        if (isModeOnline) {
            startActivity(new Intent(this, ProgramTourCreateActivity.class));
        }
    }

    public void onClickBackPressedProgramTour(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {
        multipleRippleLoader.setVisibility(View.VISIBLE);
        show_not_found.setVisibility(View.GONE);
        recycler_program_tour.setVisibility(View.GONE);
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ProgramTourModel> programTourModelArrayList) {
        if (programTourModelArrayList != null && programTourModelArrayList.size() != 0){

            tourAdapter = new ProgramTourRecyclerViewAdapter(this,programTourModelArrayList,languageLocale.getLanguage(),this);
            recycler_program_tour.setLayoutManager(new LinearLayoutManager(this));
            recycler_program_tour.setAdapter(tourAdapter);

            multipleRippleLoader.setVisibility(View.GONE);
            show_not_found.setVisibility(View.GONE);
            recycler_program_tour.setVisibility(View.VISIBLE);

        }else {

            multipleRippleLoader.setVisibility(View.GONE);
            show_not_found.setVisibility(View.VISIBLE);
            recycler_program_tour.setVisibility(View.GONE);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClicked(ProgramTourModel tourModel) {
        Intent intent = new Intent(this, ViewProgramTourActivity.class);
        intent.putExtra("PROGRAM_TOUR", tourModel);
        startActivity(intent);
    }

    @Override
    public void itemClickedLikeProgramTour(ImageView img_like, TextView txt_count_like, ProgramTourModel tourModel) {
        if (isModeOnline) {
            String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Likes");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("ProgramTour_programTour_id", String.valueOf(tourModel.getProgramTourId()));
            httpCallPost.setParams(paramsPost);
            if (userId != null) {
                new HttpPostRequestAsyncTask().execute(httpCallPost);
                finish();
                startActivity(getIntent());
            }
        }
    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }


}
