package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ipanda.lanexangtourism.Adapter.AddDatesTripAdapter;
import com.ipanda.lanexangtourism.Adapter.SpinnerCategorySubAdapter;
import com.ipanda.lanexangtourism.DialogFragment.AwaitingReviewDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.AddPhotoClickListener;
import com.ipanda.lanexangtourism.Interface_click.DatesTripClickListener;
import com.ipanda.lanexangtourism.Interface_click.PlacesItemClickListener;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.TouristAttractionsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.CategorySubAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ProgramTourItemsAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.httpcall.HttpJsonCall;
import com.ipanda.lanexangtourism.interface_callback.CategorySubCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProgramTourItemsCallBack;
import com.ipanda.lanexangtourism.post_booking.HttpPostProgramTour;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ProgramTourEditMainActivity extends AppCompatActivity implements PlacesItemClickListener, DatesTripClickListener
        , CategorySubCallBack, AddPhotoClickListener , ProgramTourItemsCallBack {

    //variables
    private static final String TAG = "ProgramTourCreateActivity";

    private static final int REQUEST_GALLERY_PHOTO = 101;

    private static final int REQUEST_CODE = 102;

    private static String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};

    private ChangeLanguageLocale languageLocale;


    private Toolbar mToolbar;

    private String userId = null;

    private Spinner mSpinner;

    private SpinnerCategorySubAdapter categorySubAdapter;

    private TextView txtCountProgramName;

    private EditText edtProgramName;

    private TextView txtCountInteresting;

    private EditText edtInteresting;

    private ConstraintLayout conMenuSaveProgramTour;

    private RecyclerView mRecyclerViewListDates;

    private AddDatesTripAdapter mDatesTripAdapter;

    private ProgramTourModel programTour;

    private ArrayList<DatesTripModel> datesTripModelsArrayList;

    private DatesTripModel tripModel;

    private ItemsModel itemsModel;

    private ArrayList<PhotoTourist> photoTouristArrayList;

    private String urlGetCategory = null;

    private Bitmap bitmap;

    private String isSaveProgramTour = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_tour_edit_main);
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.program_tour_toolbar);
        mSpinner = findViewById(R.id.spinner_type_program_tour);
        mRecyclerViewListDates = findViewById(R.id.recycler_view_list_dates);
        txtCountProgramName = findViewById(R.id.txt_count_program_name);
        edtProgramName = findViewById(R.id.edt_program_tour_name);
        txtCountInteresting = findViewById(R.id.txt_count_interesting);
        edtInteresting = findViewById(R.id.edt_interesting);
        conMenuSaveProgramTour = findViewById(R.id.con_menu_save_program_tour);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null){
            ProgramTourModel getProgramTour = getIntent().getParcelableExtra("PROGRAM_TOUR");
            if (getProgramTour != null){
                String url = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ProgramTour/getEditProgramTour?user_id="+userId+"&programTour_id="+getProgramTour.getProgramTourId();
                new ProgramTourItemsAsyncTask(this).execute(url);
            }
        }

    }

    public boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(this, mPermission);
            if (result == PackageManager.PERMISSION_DENIED){
                ActivityCompat.requestPermissions(this, new String[] {mPermission}, REQUEST_CODE);
                return false;
            }
        }
        return true;
    }


    public void onClickEventAddDatesInProgramTour(View view){
        int size =  datesTripModelsArrayList.size();
        tripModel = new DatesTripModel();
        tripModel.setDatesTripId(size);
        tripModel.setDatesTripTopicThai("");
        tripModel.setDatesTripTopicEnglish("");
        tripModel.setDatesTripTopicLaos("");
        tripModel.setDatesTripTopicChinese("");
        tripModel.setDatesTripState("1");
        datesTripModelsArrayList.add(tripModel);
        setupRecyclerViewDatesTrip();
    }

    private void setupRecyclerViewDatesTrip() {
        if (datesTripModelsArrayList != null && datesTripModelsArrayList.size() != 0) {
            conMenuSaveProgramTour.setVisibility(View.VISIBLE);
            mDatesTripAdapter = new AddDatesTripAdapter(this, datesTripModelsArrayList, languageLocale.getLanguage(), this, this, this);
            mRecyclerViewListDates.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            mRecyclerViewListDates.setAdapter(mDatesTripAdapter);
            mDatesTripAdapter.notifyDataSetChanged();
        }else {
            conMenuSaveProgramTour.setVisibility(View.GONE);
        }
    }

    @Override
    public void itemClickedPlaces(DatesTripModel trip) {
        int REQ_CODE = 1001;
        Intent intent = new Intent(this, AddPlacesActivity.class);
        intent.putExtra("DATES_TRIP",trip);
        startActivityForResult(intent, REQ_CODE);

    }

    @Override
    public void itemClickedPhoto(DatesTripModel date, TouristAttractionsModel tourist) {
        int REQ_CODE = 1002;
        Intent intent = new Intent(this, AddPhotoActivity.class);
        intent.putExtra("DATES_TRIP",date);
        intent.putExtra("TOURIST",tourist);
        startActivityForResult(intent,REQ_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001) {
            if(resultCode == Activity.RESULT_OK){
                ItemsModel resultItems = data.getParcelableExtra("resultItems");
                DatesTripModel resultDatesTrip = data.getParcelableExtra("resultDatesTrip");

                if (datesTripModelsArrayList != null && datesTripModelsArrayList.size() != 0) {
                    for (DatesTripModel dt : datesTripModelsArrayList) {
                        if (resultDatesTrip.getDatesTripId() == dt.getDatesTripId()) {
                            if (dt.getTouristAttractionsModelArrayList() != null && dt.getTouristAttractionsModelArrayList().size() != 0) {
                                int countId = dt.getTouristAttractionsModelArrayList().size();
                                TouristAttractionsModel attractionsModel = new TouristAttractionsModel();
                                attractionsModel.setTitleTH("สถานที่ที่" + (countId + 1));
                                attractionsModel.setAttractionsId(countId);
                                attractionsModel.setAttractionsTopicTH("");
                                attractionsModel.setAttractionsTopicEN("");
                                attractionsModel.setAttractionsTopicLO("");
                                attractionsModel.setAttractionsTopicZH("");
                                attractionsModel.setState("1");
                                attractionsModel.setItemsModel(resultItems);
                                dt.getTouristAttractionsModelArrayList().add(attractionsModel);
                            } else {
                                ArrayList<TouristAttractionsModel> attractionsArrayList = new ArrayList<>();
                                TouristAttractionsModel attractionsModel = new TouristAttractionsModel();
                                int countId = attractionsArrayList.size();
                                attractionsModel.setTitleTH("สถานที่ที่" + (countId + 1));
                                attractionsModel.setAttractionsId(countId);
                                attractionsModel.setAttractionsTopicTH("");
                                attractionsModel.setAttractionsTopicEN("");
                                attractionsModel.setAttractionsTopicLO("");
                                attractionsModel.setAttractionsTopicZH("");
                                attractionsModel.setState("1");
                                attractionsModel.setItemsModel(resultItems);
                                attractionsArrayList.add(attractionsModel);
                                dt.setTouristAttractionsModelArrayList(attractionsArrayList);
                            }
                        } else {

                        }
                    }
                }

                setupRecyclerViewDatesTrip();

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "CANCELED", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == 1002){
            if(resultCode == Activity.RESULT_OK){
                photoTouristArrayList = new ArrayList<>();
                photoTouristArrayList = data.getParcelableArrayListExtra("PHOTO_TOURIST");
                DatesTripModel dates = data.getParcelableExtra("DATES_TRIP");
                TouristAttractionsModel trip = data.getParcelableExtra("TOURIST");

                for (int i = 0; i < datesTripModelsArrayList.size(); i++){

                    if (datesTripModelsArrayList.get(i).getDatesTripId() == dates.getDatesTripId()) {

                        for (int j = 0; j < datesTripModelsArrayList.get(i).getTouristAttractionsModelArrayList().size(); j++) {

                            if (datesTripModelsArrayList.get(i).getTouristAttractionsModelArrayList() != null) {

                                if (datesTripModelsArrayList.get(i).getTouristAttractionsModelArrayList().get(j).getPhotoTouristArrayList() != null && datesTripModelsArrayList.get(i).getTouristAttractionsModelArrayList().get(j).getPhotoTouristArrayList().size() != 0) {

                                    if (datesTripModelsArrayList.get(i).getTouristAttractionsModelArrayList().get(j).getAttractionsId() == trip.getAttractionsId()) {

                                        datesTripModelsArrayList.get(i).getTouristAttractionsModelArrayList().get(j).getPhotoTouristArrayList().addAll(photoTouristArrayList);
                                    }


                                } else {
                                    if (datesTripModelsArrayList.get(i).getTouristAttractionsModelArrayList().get(j).getAttractionsId() == trip.getAttractionsId()) {

                                        datesTripModelsArrayList.get(i).getTouristAttractionsModelArrayList().get(j).setPhotoTouristArrayList(photoTouristArrayList);

                                    }

                                }


                            }

                        }

                    }
                }

                setupRecyclerViewDatesTrip();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "CANCELED", Toast.LENGTH_SHORT).show();
            }
        }else if (requestCode == REQUEST_GALLERY_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uriImg = data.getData();
                ImageView imageView = findViewById(R.id.img_cover_program_tour);
                Glide.with(this).load(uriImg).apply(new RequestOptions()).into(imageView);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriImg);
                    imageView.setImageBitmap(bitmap);
                    programTour.setCoverItem_state("1");
                    ImageUploadToServer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void ImageUploadToServer(){
        ByteArrayOutputStream byteArrayOutputStreamObject ;
        byteArrayOutputStreamObject = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
        programTour.setPathCovers(ConvertImage);
    }


    public void onClickSaveProgramTour(View view){
        isSaveProgramTour = "0";
        if (getEditText()) {
            if (programTour.getDatesTripModelArrayList() != null && programTour.getDatesTripModelArrayList().size() != 0){
//                programTour.getDatesTripModelArrayList().addAll(mDatesTripAdapter.returnDatesTopic());
            }else {
                programTour.setDatesTripModelArrayList(mDatesTripAdapter.returnDatesTopic());
            }
            if (programTour.getSubCategoryModel().getCategoryId() != 0){
                setupHttpPost();
            }else {
                Toast.makeText(this, "Select "+ getResources().getString(R.string.text_travel_program_types), Toast.LENGTH_SHORT).show();
            }
        }else {

        }
    }

    public void onClickSaveDraftProgramTour(View view){
        isSaveProgramTour = "1";

        if (getEditText()) {
            if (programTour.getDatesTripModelArrayList() != null && programTour.getDatesTripModelArrayList().size() != 0){
//                programTour.getDatesTripModelArrayList().addAll(mDatesTripAdapter.returnDatesTopic());
            }else {
                programTour.setDatesTripModelArrayList(mDatesTripAdapter.returnDatesTopic());
            }
            if (programTour.getSubCategoryModel().getCategoryId() != 0){
                setupHttpPost();
            }else {
                Toast.makeText(this, "Select "+ getResources().getString(R.string.text_travel_program_types), Toast.LENGTH_SHORT).show();
            }
        }else {

        }
    }


    public void onClickBrowseImageToCoverProgramTour(View view){
        if (checkPermission()){
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
        }else {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onClickEventDatesTrip(DatesTripModel datesTrip,int position, String event) {
        switch (event){
            case "edit":
                break;
            case "delete":
                if (datesTripModelsArrayList != null && datesTripModelsArrayList.size() != 0) {
                    datesTripModelsArrayList.remove(position);
                    mDatesTripAdapter.returnDatesTopic().remove(position);
                    mDatesTripAdapter.notifyDataSetChanged();
                }else {
                    conMenuSaveProgramTour.setVisibility(View.GONE);
                }
                break;
        }
        Toast.makeText(this, datesTrip.getDatesTripTopicThai()+" "+event, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ProgramTourModel programTourModel) {
        if (programTourModel != null){

            //setup Array list dates trip
            this.programTour = programTourModel;
            datesTripModelsArrayList = programTour.getDatesTripModelArrayList();
            itemsModel = new ItemsModel();

            //set static id
            programTour.setMenuItemModel(new MenuItemModel(1));
            programTour.setCategoryModel(new CategoryModel(1));

            checkPermission();

            //set event show
            conMenuSaveProgramTour.setVisibility(View.GONE);

            txtCountProgramName.setText("0/50");
            edtProgramName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    txtCountProgramName.setText(charSequence.length()+"/50");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            txtCountInteresting.setText("0/300");
            edtInteresting.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    txtCountInteresting.setText(charSequence.length()+"/300");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            urlGetCategory = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ProgramTour/getNameSubCategoryById?category_id=1";
            new CategorySubAsyncTask(this).execute(urlGetCategory);

            setupContent();

            if (datesTripModelsArrayList != null && datesTripModelsArrayList.size() != 0){
                setupRecyclerViewDatesTrip();
            }
        }
    }

    private void setupContent() {
        ImageView coverProgramTour = findViewById(R.id.img_cover_program_tour);
        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+programTour.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(coverProgramTour);

        switch (languageLocale.getLanguage()){
            case "th":
                edtProgramName.setText(programTour.getProgramTourNameTH());
                edtInteresting.setText(programTour.getInterestingThingsTH());
                break;
            case "en":
                edtProgramName.setText(programTour.getProgramTourNameEN());
                edtInteresting.setText(programTour.getInterestingThingsEN());
                break;
            case "lo":
                edtProgramName.setText(programTour.getProgramTourNameLO());
                edtInteresting.setText(programTour.getInterestingThingsLO());
                break;
            case "zh":
                edtProgramName.setText(programTour.getProgramTourNameZH());
                edtInteresting.setText(programTour.getInterestingThingsZH());
                break;
        }

        String testURL = paths+programTour.getCoverItemsModel().getCoverPaths();


        String encodedURL = Base64.encodeToString(testURL.getBytes(),Base64.DEFAULT);



    }



    @Override
    public void onRequestCompleteListenerCategorySub(CategoryModel categoryModel) {
        if (categoryModel != null){
            Log.e("check data" , categoryModel+"");
            if (categoryModel.getSubCategoryModelArrayList() != null) {
                categoryModel.getSubCategoryModelArrayList().add(0, new SubCategoryModel("เลือกหนึ่งตัวเลือก", "Choose an option", "ເລືອກຕົວເລືອກ", "选择一个选项"));
            }
            categorySubAdapter = new SpinnerCategorySubAdapter(this, categoryModel.getSubCategoryModelArrayList(), languageLocale.getLanguage());
            mSpinner.setAdapter(categorySubAdapter);
            for (int i = 0; i < categoryModel.getSubCategoryModelArrayList().size() ; i++) {
                if (programTour.getSubCategoryModel().getCategoryId() == categoryModel.getSubCategoryModelArrayList().get(i).getCategoryId()){
                    mSpinner.setSelection(i);
                }
            }

            mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    programTour.setSubCategoryModel(new SubCategoryModel(categoryModel.getSubCategoryModelArrayList().get(position).getCategoryId()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private boolean getEditText() {
        if (TextUtils.isEmpty(edtProgramName.getText().toString())) {
            Toast.makeText(this, "Empty field not allowed!", Toast.LENGTH_SHORT).show();
            Selection.setSelection((Editable) edtProgramName.getText(),edtProgramName.getSelectionStart());
            edtProgramName.requestFocus();
            return false;
        } else {
            programTour.setProgramTourNameTH(edtProgramName.getText().toString());
        }
        if (TextUtils.isEmpty(edtInteresting.getText().toString())) {
            Toast.makeText(this, "Empty field not allowed!", Toast.LENGTH_SHORT).show();
            Selection.setSelection((Editable) edtInteresting.getText(),edtInteresting.getSelectionStart());
            edtInteresting.requestFocus();
            return false;
        } else {
            programTour.setInterestingThingsTH(edtInteresting.getText().toString());
        }
        return true;
    }

    private void setupHttpPost(){
        if (userId != null){
            HttpJsonCall httpCallPost = new HttpJsonCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ProgramTour/saveEditProgramTour");

            JSONObject arrayNull = new JSONObject();

            if (programTour.getDatesTripModelArrayList() != null && programTour.getDatesTripModelArrayList().size() != 0) {

                JSONObject jsonProgramTour = new JSONObject();
                JSONObject jsProgramTour = new JSONObject();
                try {
                    jsProgramTour.put("programTour_id", String.valueOf(programTour.getProgramTourId()));
                    jsProgramTour.put("menuItem_id", String.valueOf(programTour.getMenuItemModel().getMenuItemId()));
                    jsProgramTour.put("category_id", String.valueOf(programTour.getCategoryModel().getCategoryId()));
                    jsProgramTour.put("user_id", userId);
                    jsProgramTour.put("programTour_nameThai", programTour.getProgramTourNameTH());
                    jsProgramTour.put("programTour_nameEnglish", "");
                    jsProgramTour.put("programTour_nameChinese", "");
                    jsProgramTour.put("programTour_nameLaos", "");
                    jsProgramTour.put("programTour_interestingThingsThai", programTour.getInterestingThingsTH());
                    jsProgramTour.put("programTour_interestingThingsEnglish", "");
                    jsProgramTour.put("programTour_interestingThingsChinese", "");
                    jsProgramTour.put("programTour_interestingThingsLaos", "");
                    jsProgramTour.put("programTour_isSaveDraft", isSaveProgramTour);
                    jsProgramTour.put("coverItem_state",programTour.getCoverItem_state());
                    jsProgramTour.put("subcategory_id", String.valueOf(programTour.getSubCategoryModel().getCategoryId()));
                    if (programTour.getPathCovers() != null) {
                        jsProgramTour.put("coverItem_paths", programTour.getPathCovers());
                    }else {
                        jsProgramTour.put("coverItem_paths", "");
                    }

                    if (programTour.getDatesTripModelArrayList() != null) {
                        JSONArray jsonDatesTrip = new JSONArray();
                        if (programTour.getDatesTripModelArrayList() != null && programTour.getDatesTripModelArrayList().size() != 0) {
                            for (DatesTripModel dates : programTour.getDatesTripModelArrayList()) {
                                JSONObject jsDatesTrip = new JSONObject();
                                jsDatesTrip.put("datesTrip_id", String.valueOf(dates.getDatesTripId()));
                                jsDatesTrip.put("datesTrip_topicThai", dates.getDatesTripTopicThai());
                                jsDatesTrip.put("datesTrip_topicEnglish", "");
                                jsDatesTrip.put("datesTrip_topicChinese", "");
                                jsDatesTrip.put("datesTrip_topicLaos", "");
                                jsDatesTrip.put("datesTrip_state", dates.getDatesTripState());


                                if (dates.getTouristAttractionsModelArrayList() != null) {
                                    JSONArray jsonTourist = new JSONArray();
                                    for (TouristAttractionsModel tourist : dates.getTouristAttractionsModelArrayList()) {
                                        JSONObject jsTourist = new JSONObject();
                                        jsTourist.put("touristAttractions_id", String.valueOf(tourist.getAttractionsId()));
                                        jsTourist.put("detail_Thai", tourist.getAttractionsTopicTH());
                                        jsTourist.put("detail_English", "");
                                        jsTourist.put("detail_Chinese", "");
                                        jsTourist.put("detail_Laos", "");
                                        jsTourist.put("touristAttractions_state", tourist.getState());
                                        jsTourist.put("items_id", tourist.getItemsModel().getItemsId());

                                        if (tourist.getPhotoTouristArrayList() != null) {
                                            JSONArray jsonPhoto = new JSONArray();
                                            for (PhotoTourist photo : tourist.getPhotoTouristArrayList()) {
                                                JSONObject jsPhoto = new JSONObject();
                                                jsPhoto.put("photo_id", photo.getPhotoTouristId());

                                                if (photo.getPhotoState().equals("0")) {
                                                    jsPhoto.put("photo_paths", photo.getPhotoTouristPaths());
                                                } else {
                                                    try {
                                                        ByteArrayOutputStream byteArrayOutputStreamObject;
                                                        byteArrayOutputStreamObject = new ByteArrayOutputStream();
                                                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(photo.getPhotoTouristPaths())));
                                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
                                                        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
                                                        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
                                                        jsPhoto.put("photo_paths", ConvertImage);
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                jsPhoto.put("photo_state", photo.getPhotoState());
                                                jsPhoto.put("photo_textThai", photo.getPhotoTouristTH());
                                                jsPhoto.put("photo_textEnglish", "");
                                                jsPhoto.put("photo_textChinese", "");
                                                jsPhoto.put("photo_textLaos", "");
                                                jsonPhoto.put(jsPhoto);
                                            }
                                            jsTourist.put("Photo", jsonPhoto);
                                        } else {
                                            jsTourist.put("Photo", arrayNull);
                                        }

                                        jsonTourist.put(jsTourist);
                                    }
                                    jsDatesTrip.put("TouristAttractions", jsonTourist);
                                    jsonDatesTrip.put(jsDatesTrip);
                                } else {
                                    jsDatesTrip.put("TouristAttractions", arrayNull);
                                }
                            }
                        }
                        jsProgramTour.put("DatesTrip", jsonDatesTrip);
                    }else {
                        jsProgramTour.put("DatesTrip", arrayNull);
                    }
                    jsonProgramTour.put("ProgramTour", jsProgramTour);
                    Log.e("data json", jsonProgramTour.toString());
                    httpCallPost.setParams(jsonProgramTour);
                    new HttpPostProgramTour().execute(httpCallPost);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                DialogFragment dialogFragment = AwaitingReviewDialogFragment.newInstance();
                dialogFragment.show(getSupportFragmentManager(), TAG);
            }

        }else {
            startActivity(new Intent(this, LoginActivity.class));
        }

    }


}