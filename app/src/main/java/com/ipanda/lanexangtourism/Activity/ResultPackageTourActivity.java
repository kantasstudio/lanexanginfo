package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.ipanda.lanexangtourism.Adapter.ItemsPackageTourRecyclerViewAdapter;
import com.ipanda.lanexangtourism.FilterActivity.PackageTourFilterActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ItemsPackageTourClickListener;
import com.ipanda.lanexangtourism.Model.DurationPackageTourModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MonthModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ItemsPackageTourAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ItemsPackageTourCallBack;
import com.ipanda.lanexangtourism.items_view.ViewPackageTourActivity;
import com.ipanda.lanexangtourism.post_booking.HttpPostPackages;
import com.ipanda.lanexangtourism.post_search.PackageTourPostRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ResultPackageTourActivity extends AppCompatActivity implements View.OnClickListener , ItemsPackageTourCallBack , ItemsPackageTourClickListener {

    //Variables
    private static final int REQUEST_CODE = 1001;

    private Toolbar mToolbar;

    private TextView toolbar_title;

    private ChangeLanguageLocale languageLocale;

    private ConstraintLayout con_filter_package_tour;

    private RateModel rateModel;

    private MonthModel monthModel;

    private boolean isGetAll = false;

    private RecyclerView recycler_package_tour;

    private ItemsPackageTourRecyclerViewAdapter mPackageTourAdapter;

    private ArrayList<SubCategoryModel> subCategoryArrayList;

    private ArrayList<DurationPackageTourModel> durationArrayList;

    private CircularSticksLoader loader_items_id;

    private ImageView img_load_field_id;

    private TextView txt_load_field_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_package_tour);

        //views
        mToolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        con_filter_package_tour = findViewById(R.id.con_filter_package_tour);
        recycler_package_tour = findViewById(R.id.recycler_package_tour);
        loader_items_id = findViewById(R.id.loader_items_id);
        img_load_field_id = findViewById(R.id.img_load_field_id);
        txt_load_field_id = findViewById(R.id.txt_load_field_id);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (getIntent() != null){
            isGetAll = getIntent().getBooleanExtra("GET_ALL",false);
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            monthModel = getIntent().getParcelableExtra("MONTH_MODEL");
        }

        txt_load_field_id.setText(getResources().getString(R.string.text_find_not_found));

        //set on click event
        con_filter_package_tour.setOnClickListener(this);

        if (isGetAll){
            String url = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Reservations/PackageTours";
            new ItemsPackageTourAsyncTask(this).execute(url);
        }else {
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Fillter/");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("category_id", "9");
            paramsPost.put("subcategory_id", "");
            paramsPost.put("str_search", getIntent().getStringExtra("TEXT_SEARCH"));
            paramsPost.put("travel_period", "");
            paramsPost.put("tour_package_duration", "");
            paramsPost.put("price_range", "");
            httpCallPost.setParams(paramsPost);
            new PackageTourPostRequest(this).execute(httpCallPost);
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.con_filter_package_tour:
                Intent intent = new Intent(this, PackageTourFilterActivity.class);
                startActivityForResult(intent,REQUEST_CODE);
                break;
        }
    }

    @Override
    public void onPreCallService() {
        recycler_package_tour.setVisibility(View.GONE);
        loader_items_id.setVisibility(View.VISIBLE);
        img_load_field_id.setVisibility(View.GONE);
        txt_load_field_id.setVisibility(View.GONE);
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            mPackageTourAdapter = new ItemsPackageTourRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recycler_package_tour.setLayoutManager(layoutManager);
            recycler_package_tour.setAdapter(mPackageTourAdapter);

            recycler_package_tour.setVisibility(View.VISIBLE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.GONE);
            txt_load_field_id.setVisibility(View.GONE);

        }else {
            mPackageTourAdapter = new ItemsPackageTourRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recycler_package_tour.setLayoutManager(layoutManager);
            recycler_package_tour.setAdapter(mPackageTourAdapter);

            recycler_package_tour.setVisibility(View.GONE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.VISIBLE);
            txt_load_field_id.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClickedItems(ItemsModel items) {
        Intent intent = new Intent(this, ViewPackageTourActivity.class);
        intent.putExtra("ITEMS_MODEL", items);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_CODE:
                if (data != null) {
                    mPackageTourAdapter.notifyDataSetChanged();
                    String getMaxMin = data.getStringExtra("getMaxMin");
                    subCategoryArrayList = data.getParcelableArrayListExtra("sub_category_list");
                    durationArrayList = data.getParcelableArrayListExtra("duration_list");
                    methodPost(getMaxMin);
                }
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void methodPost(String getMaxMin) {
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Fillter/");
        HashMap<String, String> paramsPost = new HashMap<>();
        paramsPost.put("category_id", "9");
        paramsPost.put("subcategory_id", getSubCategory());
        paramsPost.put("str_search", "");
        paramsPost.put("travel_period", "");
        paramsPost.put("tour_package_duration", getDuration());
        paramsPost.put("price_range", getMaxMin);
        httpCallPost.setParams(paramsPost);
        new PackageTourPostRequest(this).execute(httpCallPost);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getSubCategory(){
        List<String> subId = new ArrayList<>();
        if (subCategoryArrayList != null && subCategoryArrayList.size() != 0) {
            for (SubCategoryModel sub : subCategoryArrayList) {
                if (sub.isFilter()) {
                    subId.add(sub.getCategoryId() + "");
                }
            }
        }
        return String.join(",", subId);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getDuration(){
        List<String> durationId = new ArrayList<>();
        if (durationArrayList != null && durationArrayList.size() != 0) {
            for (DurationPackageTourModel dur : durationArrayList) {
                if (dur.isFilter()) {
                    durationId.add(dur.getTextValue());
                }
            }
        }
        return String.join(",", durationId);
    }
}
