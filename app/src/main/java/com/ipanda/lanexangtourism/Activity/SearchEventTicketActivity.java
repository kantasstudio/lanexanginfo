package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.ipanda.lanexangtourism.Adapter.ItemsTicketEventRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ItemsTicketEventClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ItemsLastSearchTicketEventCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsTicketEventTourCallBack;
import com.ipanda.lanexangtourism.items_view.ViewEventTicketActivity;
import com.ipanda.lanexangtourism.post_search.LastSearchTicket;
import com.ipanda.lanexangtourism.post_search.PackageTourPostRequest;
import com.ipanda.lanexangtourism.post_search.TicketPostRequest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SearchEventTicketActivity extends AppCompatActivity implements View.OnClickListener , ItemsTicketEventTourCallBack,
        ItemsTicketEventClickListener, ItemsLastSearchTicketEventCallBack {

    //variables
    private ChangeLanguageLocale languageLocale;

    private RelativeLayout btn_back;

    private ImageView btn_search;

    private ImageView img_last_search;

    private TextView txt_last_search;

    private AutoCompleteTextView autoCompleteTextSearch;

    private ConstraintLayout con_search_all_package_tour;

    private RateModel rateModel;

    private RecyclerView recycler_search_booking;

    private RecyclerView recycler_last_search_booking;

    private ItemsTicketEventRecyclerViewAdapter ticketAdapter;

    private ItemsTicketEventRecyclerViewAdapter lastSearchTicketAdapter;

    private CircularSticksLoader loader_items_id;

    private ImageView img_load_field_id;

    private TextView txt_load_field_id;

    private Collection<String> lastSearchId = new ArrayList<>();

    private List<String> distinctLastSearchId = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_event_ticket);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        btn_back = findViewById(R.id.btn_back);
        btn_search = findViewById(R.id.btn_search);
        autoCompleteTextSearch = findViewById(R.id.autoCompleteTextSearch);
        img_last_search = findViewById(R.id.img_last_search);
        txt_last_search = findViewById(R.id.txt_last_search);
        con_search_all_package_tour = findViewById(R.id.con_search_all_package_tour);
        recycler_search_booking = findViewById(R.id.recycler_search_booking);
        recycler_last_search_booking = findViewById(R.id.recycler_last_search_booking);
        loader_items_id = findViewById(R.id.loader_items_id);
        img_load_field_id = findViewById(R.id.img_load_field_id);
        txt_load_field_id = findViewById(R.id.txt_load_field_id);

        if (getIntent() != null){
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
        }

        txt_load_field_id.setText(getResources().getString(R.string.text_find_not_found));

        //set onclick event
        btn_back.setOnClickListener(this);
        btn_search.setOnClickListener(this);
        con_search_all_package_tour.setOnClickListener(this);

        //set img
        img_last_search.setImageResource(R.drawable.ic_style);

        //set text
        txt_last_search.setText(getResources().getString(R.string.text_event_ticket_all));
        autoCompleteTextSearch.setOnEditorActionListener(editorActionListener);
        getLastSearch();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_search:
//                searchByTicket();
                searchByItemTicket();
                break;
            case R.id.con_search_all_package_tour:
                searchTicket();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void searchByItemTicket(){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Fillter/");
        HashMap<String, String> paramsPost = new HashMap<>();
        paramsPost.put("user_id","");
        paramsPost.put("category_id", "10");
        paramsPost.put("subcategory_id", "");
        paramsPost.put("str_search", autoCompleteTextSearch.getText().toString());
        paramsPost.put("price_range", "");
        httpCallPost.setParams(paramsPost);
        new TicketPostRequest(this).execute(httpCallPost);
    }

    private void searchByTicket(){
        Intent intent = new Intent(this, ResultEventTicketActivity.class);
        intent.putExtra("GET_ALL", false);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        if(autoCompleteTextSearch.getText() != null && autoCompleteTextSearch.getText().toString().length() > 0){
            intent.putExtra("TEXT_SEARCH",autoCompleteTextSearch.getText().toString());
        }
        startActivity(intent);

    }

    private void searchTicket() {
        Intent intent = new Intent(this, ResultEventTicketActivity.class);
        intent.putExtra("GET_ALL", true);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void itemClickedItems(ItemsModel items) {
        Intent intent = new Intent(this, ViewEventTicketActivity.class);
        intent.putExtra("ITEMS_MODEL", items);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerLastSearchTicket(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            lastSearchTicketAdapter = new ItemsTicketEventRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recycler_last_search_booking.setLayoutManager(layoutManager);
            recycler_last_search_booking.setAdapter(lastSearchTicketAdapter);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            Log.d("check data", itemsModelArrayList+"");
            ticketAdapter = new ItemsTicketEventRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recycler_search_booking.setLayoutManager(layoutManager);
            recycler_search_booking.setAdapter(ticketAdapter);

            recycler_search_booking.setVisibility(View.VISIBLE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.GONE);
            txt_load_field_id.setVisibility(View.GONE);

            for (ItemsModel item : itemsModelArrayList) {
                lastSearchId.add(item.getItemsId()+"");
            }
            distinctLastSearchId = lastSearchId.stream().distinct().collect(Collectors.toList());
            setLastSearch();
            getLastSearch();

        }else {
            ticketAdapter = new ItemsTicketEventRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recycler_search_booking.setLayoutManager(layoutManager);
            recycler_search_booking.setAdapter(ticketAdapter);

            recycler_search_booking.setVisibility(View.GONE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.VISIBLE);
            txt_load_field_id.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

            switch (actionId){
                case EditorInfo.IME_ACTION_SEND:
                    searchByItemTicket();
                    break;
            }
            return false;
        }
    };

    private void setLastSearch(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String result = String.join(",", distinctLastSearchId);
            getSharedPreferences("PREF_APP_LAST_SEARCH_TICKET", Context.MODE_PRIVATE).edit().putString("APP_LAST_SEARCH_TICKET", result).apply();
        }
    }

    private void getLastSearch(){
        String getLastSearch = getSharedPreferences("PREF_APP_LAST_SEARCH_TICKET", Context.MODE_PRIVATE).getString("APP_LAST_SEARCH_TICKET",null);
        if (getLastSearch != null && !getLastSearch.equals("")){
            postRequestLastSearch(getLastSearch);
        }
    }

    private void postRequestLastSearch(String getLastSearch){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/LastSearch");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("category_id", "10");
        paramsPost.put("last_id", getLastSearch);
        paramsPost.put("latitude", "");
        paramsPost.put("longitude", "");
        httpCallPost.setParams(paramsPost);
        new LastSearchTicket(this).execute(httpCallPost);
    }
}