package com.ipanda.lanexangtourism.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.ipanda.lanexangtourism.Adapter.ItemsPackageTourRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ItemsPackageTourClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MonthModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ItemsLastSearchPackageTourCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsPackageTourCallBack;
import com.ipanda.lanexangtourism.items_view.ViewPackageTourActivity;
import com.ipanda.lanexangtourism.post_search.LastSearchPackageTour;
import com.ipanda.lanexangtourism.post_search.PackageTourPostRequest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SearchPackageTourActivity extends AppCompatActivity implements View.OnClickListener , ItemsPackageTourCallBack, ItemsPackageTourClickListener
    , ItemsLastSearchPackageTourCallBack {

    //variables

    private ChangeLanguageLocale languageLocale;

    private ConstraintLayout con_calendar_view;

    private ConstraintLayout con_filter_calendar_view;

    private ConstraintLayout con_last_search_booking_view;

    private ConstraintLayout con_search_all_package_tour;

    private AutoCompleteTextView autoCompleteTextSearch;

    private boolean isFilterCalendar = false;

    private RelativeLayout btn_back;

    private ImageView  btn_search;

    private RateModel rateModel;

    private MonthModel monthModel;

    private RecyclerView recycler_search_booking;

    private ItemsPackageTourRecyclerViewAdapter mPackageTourAdapter;

    private RecyclerView recycler_last_search_booking;

    private ItemsPackageTourRecyclerViewAdapter lastSearchPackageTourAdapter;


    private CircularSticksLoader loader_items_id;

    private ImageView img_load_field_id;

    private TextView txt_load_field_id;

    private Collection<String> lastSearchId = new ArrayList<>();

    private List<String> distinctLastSearchId = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_package_tour);

        //views
        con_calendar_view = findViewById(R.id.con_calendar_view);
        con_filter_calendar_view = findViewById(R.id.con_filter_calendar_view);
        con_last_search_booking_view = findViewById(R.id.con_last_search_booking_view);
        con_search_all_package_tour = findViewById(R.id.con_search_all_package_tour);
        btn_back = findViewById(R.id.btn_back);
        btn_search = findViewById(R.id.btn_search);
        autoCompleteTextSearch = findViewById(R.id.autoCompleteTextSearch);
        recycler_search_booking = findViewById(R.id.recycler_search_booking);
        recycler_last_search_booking = findViewById(R.id.recycler_last_search_booking);
        loader_items_id = findViewById(R.id.loader_items_id);
        img_load_field_id = findViewById(R.id.img_load_field_id);
        txt_load_field_id = findViewById(R.id.txt_load_field_id);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        monthModel = new MonthModel();

        if (getIntent() != null){
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
        }

        txt_load_field_id.setText(getResources().getString(R.string.text_find_not_found));

        //set on click event
        con_filter_calendar_view.setOnClickListener(this);
        con_search_all_package_tour.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_search.setOnClickListener(this);
        autoCompleteTextSearch.setOnEditorActionListener(editorActionListener);

        setupOnClickMonth();
        getLastSearch();

    }

    private void setupOnClickMonth() {
        CheckBox check_january = findViewById(R.id.check_january);
        CheckBox check_february = findViewById(R.id.check_february);
        CheckBox check_march = findViewById(R.id.check_march);
        CheckBox check_april = findViewById(R.id.check_april);
        CheckBox check_may = findViewById(R.id.check_may);
        CheckBox check_june = findViewById(R.id.check_june);
        CheckBox check_july = findViewById(R.id.check_july);
        CheckBox check_august = findViewById(R.id.check_august);
        CheckBox check_september = findViewById(R.id.check_september);
        CheckBox check_october = findViewById(R.id.check_october);
        CheckBox check_november = findViewById(R.id.check_november);
        CheckBox check_december = findViewById(R.id.check_december);

        check_january.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_january.isChecked()){
                    monthModel.setJanuary(true);
                }else {
                    monthModel.setJanuary(false);
                }
            }
        });
        check_february.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_february.isChecked()){
                    monthModel.setFebruary(true);
                }else {
                    monthModel.setFebruary(false);
                }
            }
        });
        check_march.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_march.isChecked()){
                    monthModel.setMarch(true);
                }else {
                    monthModel.setMarch(false);
                }
            }
        });
        check_april.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_april.isChecked()){
                    monthModel.setApril(true);
                }else {
                    monthModel.setApril(false);
                }
            }
        });
        check_may.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_may.isChecked()){
                    monthModel.setMay(true);
                }else {
                    monthModel.setMay(false);
                }
            }
        });
        check_june.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_june.isChecked()){
                    monthModel.setJune(true);
                }else {
                    monthModel.setJune(false);
                }
            }
        });
        check_july.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_july.isChecked()){
                    monthModel.setJuly(true);
                }else {
                    monthModel.setJuly(false);
                }
            }
        });
        check_august.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_august.isChecked()){
                    monthModel.setAugust(true);
                }else {
                    monthModel.setAugust(false);
                }
            }
        });
        check_september.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_september.isChecked()){
                    monthModel.setSeptember(true);
                }else {
                    monthModel.setSeptember(false);
                }
            }
        });
        check_october.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_october.isChecked()){
                    monthModel.setOctober(true);
                }else {
                    monthModel.setOctober(false);
                }
            }
        });
        check_november.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_november.isChecked()){
                    monthModel.setNovember(true);
                }else {
                    monthModel.setNovember(false);
                }
            }
        });
        check_december.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_december.isChecked()){
                    monthModel.setDecember(true);
                }else {
                    monthModel.setDecember(false);
                }
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.con_filter_calendar_view:
                if (isFilterCalendar){
                    con_calendar_view.setVisibility(View.GONE);
                    con_last_search_booking_view.setVisibility(View.VISIBLE);
                    isFilterCalendar = false;
                }else {
                    con_calendar_view.setVisibility(View.VISIBLE);
                    con_last_search_booking_view.setVisibility(View.GONE);
                    isFilterCalendar = true;
                }
                break;
            case R.id.con_search_all_package_tour:
                searchAllPackageTour();
                break;
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_search:
//                searchByPackageTour();
                con_calendar_view.setVisibility(View.GONE);
                con_last_search_booking_view.setVisibility(View.VISIBLE);
                searchByItemPackageTour();
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void searchByItemPackageTour(){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Fillter/");
        HashMap<String, String> paramsPost = new HashMap<>();
        paramsPost.put("category_id", "9");
        paramsPost.put("subcategory_id", "");
        paramsPost.put("str_search", autoCompleteTextSearch.getText().toString());
        paramsPost.put("travel_period", getMonth());
        paramsPost.put("tour_package_duration","" );
        paramsPost.put("user_id","");
        paramsPost.put("price_range", "");
        httpCallPost.setParams(paramsPost);
        new PackageTourPostRequest(this).execute(httpCallPost);

    }

    private void searchByPackageTour(){
        Intent intent = new Intent(this, ResultPackageTourActivity.class);
        intent.putExtra("GET_ALL", false);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        intent.putExtra("MONTH_MODEL",monthModel);
        if(autoCompleteTextSearch.getText() != null && autoCompleteTextSearch.getText().toString().length() > 0){
            intent.putExtra("TEXT_SEARCH",autoCompleteTextSearch.getText().toString());
        }
        startActivity(intent);

    }

    private void searchAllPackageTour() {
        Intent intent = new Intent(this, ResultPackageTourActivity.class);
        intent.putExtra("GET_ALL", true);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    private TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

            switch (actionId){
                case EditorInfo.IME_ACTION_SEND:
                    searchByItemPackageTour();
                    break;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void itemClickedItems(ItemsModel items) {
        Intent intent = new Intent(this, ViewPackageTourActivity.class);
        intent.putExtra("ITEMS_MODEL", items);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerLastSearchPackageTour(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            lastSearchPackageTourAdapter = new ItemsPackageTourRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recycler_last_search_booking.setLayoutManager(layoutManager);
            recycler_last_search_booking.setAdapter(lastSearchPackageTourAdapter);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            mPackageTourAdapter = new ItemsPackageTourRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recycler_search_booking.setLayoutManager(layoutManager);
            recycler_search_booking.setAdapter(mPackageTourAdapter);

            recycler_search_booking.setVisibility(View.VISIBLE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.GONE);
            txt_load_field_id.setVisibility(View.GONE);

            for (ItemsModel item : itemsModelArrayList) {
                lastSearchId.add(item.getItemsId()+"");
            }
            distinctLastSearchId = lastSearchId.stream().distinct().collect(Collectors.toList());
            setLastSearch();
            getLastSearch();

        }else {
            mPackageTourAdapter = new ItemsPackageTourRecyclerViewAdapter(this,itemsModelArrayList, languageLocale.getLanguage(), this, rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recycler_search_booking.setLayoutManager(layoutManager);
            recycler_search_booking.setAdapter(mPackageTourAdapter);

            recycler_search_booking.setVisibility(View.VISIBLE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.VISIBLE);
            txt_load_field_id.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getMonth(){
        List<String> monthId = new ArrayList<>();
        if (monthModel.isJanuary()){
            monthId.add("1");
        }
        if (monthModel.isFebruary()){
            monthId.add("2");
        }
        if (monthModel.isMarch()){
            monthId.add("3");
        }
        if (monthModel.isApril()){
            monthId.add("4");
        }
        if (monthModel.isMay()){
            monthId.add("5");
        }
        if (monthModel.isJune()){
            monthId.add("6");
        }
        if (monthModel.isJuly()){
            monthId.add("7");
        }
        if (monthModel.isAugust()){
            monthId.add("8");
        }
        if (monthModel.isSeptember()){
            monthId.add("9");
        }
        if (monthModel.isOctober()){
            monthId.add("10");
        }
        if (monthModel.isNovember()){
            monthId.add("11");
        }
        if (monthModel.isDecember()){
            monthId.add("12");
        }

        return String.join(",", monthId);
    }

    private void setLastSearch(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String result = String.join(",", distinctLastSearchId);
            getSharedPreferences("PREF_APP_LAST_SEARCH_PACKAGE_TOUR", Context.MODE_PRIVATE).edit().putString("APP_LAST_SEARCH_PACKAGE_TOUR", result).apply();
        }
    }

    private void getLastSearch(){
        String getLastSearch = getSharedPreferences("PREF_APP_LAST_SEARCH_PACKAGE_TOUR", Context.MODE_PRIVATE).getString("APP_LAST_SEARCH_PACKAGE_TOUR",null);
        if (getLastSearch != null && !getLastSearch.equals("")){
            postRequestLastSearch(getLastSearch);
        }
    }

    private void postRequestLastSearch(String getLastSearch){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/LastSearch");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("category_id", "9");
        paramsPost.put("last_id", getLastSearch);
        paramsPost.put("latitude", "");
        paramsPost.put("longitude", "");
        httpCallPost.setParams(paramsPost);
        new LastSearchPackageTour(this).execute(httpCallPost);
    }
}