package com.ipanda.lanexangtourism.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Adapter.SettingsAdapterRecyclerView;
import com.ipanda.lanexangtourism.ClickListener.SettingsItemOnClickListener;
import com.ipanda.lanexangtourism.DialogFragment.SignOutFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.SettingsModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.DownloadCarRentalAsyncTask;
import com.ipanda.lanexangtourism.asynctask.DownloadItemsAsyncTask;
import com.ipanda.lanexangtourism.asynctask.DownloadPackageTourAsyncTask;
import com.ipanda.lanexangtourism.asynctask.DownloadProgramTourAsyncTask;
import com.ipanda.lanexangtourism.asynctask.DownloadTicketEventAsyncTask;
import com.ipanda.lanexangtourism.asynctask.UserAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.database.DatabaseSQL;
import com.ipanda.lanexangtourism.interface_callback.DownloadCarRentalCallBack;
import com.ipanda.lanexangtourism.interface_callback.DownloadItemsCallBack;
import com.ipanda.lanexangtourism.interface_callback.DownloadPackageTourCallBack;
import com.ipanda.lanexangtourism.interface_callback.DownloadProgramTourCallBack;
import com.ipanda.lanexangtourism.interface_callback.DownloadTicketEventCallBack;
import com.ipanda.lanexangtourism.interface_callback.UserCallBack;
import com.squareup.picasso.Picasso;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;


public class SettingsActivity extends AppCompatActivity implements View.OnClickListener, SettingsItemOnClickListener, UserCallBack
    , DownloadProgramTourCallBack, DownloadItemsCallBack, DownloadPackageTourCallBack, DownloadTicketEventCallBack, DownloadCarRentalCallBack {

    //variables
    private Toolbar mToolbar;

    private Button btnSignOut,btnSignUp;

    private Button button_download_data;

    private ImageView imgProfileSettings,imgTHB, imgUSD, imgLak, imgCny;

    private ExpandableLayout mShowContentLanguage, mShowContentCurrency, mShowContentMode, mShowContentBooking;

    private ImageView mImgUpDownLanguage, mImgUpDownCurrency, mImgUpDownMode, mImgUpDownBooking;

    private ImageView img_mode_online, img_mode_offline;

    private TextView txt_profile_full_name,txt_login_with;

    private RecyclerView mRecyclerViewLanguage, mRecyclerViewCurrency;

    private SettingsAdapterRecyclerView mAdapterLanguage, mAdapterCurrency;

    private ArrayList<SettingsModel> itemsLanguageArrayList, itemsCurrencyArrayList;

    private SettingsModel settingsModel;

    private ChangeLanguageLocale languageLocale;

    private ConstraintLayout btnLoginApp, btnTHB, btnUSD, btnLak, btnCny;

    private ConstraintLayout con_mode_online, con_mode_offline;

    private ConstraintLayout con_loader;

    private RelativeLayout btnLogOutApp;

    private UserModel userModels;

    private boolean isLanguageTH, isLanguageEN, isLanguageLO, isLanguageZH;

    private boolean isTHB = true, isUSD, isLAK, isCNY;

    private boolean isLogin;

    private boolean isModeOnline = true;

    private ProgressDialog alertDialog = null;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        //views
        mToolbar = findViewById(R.id.Settings_Toolbar);
        btnSignOut = findViewById(R.id.btn_log_off);
        btnSignUp = findViewById(R.id.btn_sign_up);
        con_loader = findViewById(R.id.con_loader);
        imgProfileSettings = findViewById(R.id.img_profile_settings);
        txt_profile_full_name = findViewById(R.id.txt_profile_full_name);
        txt_login_with = findViewById(R.id.txt_login_with);
        btnLoginApp = findViewById(R.id.btn_login_app);
        btnLogOutApp = findViewById(R.id.btn_logout_app);
        btnTHB = findViewById(R.id.con_thb);
        btnUSD = findViewById(R.id.con_usd);
        btnLak = findViewById(R.id.con_lak);
        btnCny = findViewById(R.id.con_cny);
        imgTHB = findViewById(R.id.img_thb);
        imgUSD = findViewById(R.id.img_usd);
        imgLak = findViewById(R.id.img_lak);
        imgCny = findViewById(R.id.img_cny);
        con_mode_online = findViewById(R.id.con_mode_online);
        con_mode_offline = findViewById(R.id.con_mode_offline);
        img_mode_online = findViewById(R.id.img_mode_online);
        img_mode_offline = findViewById(R.id.img_mode_offline);
        button_download_data = findViewById(R.id.button_download_data);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressedSettingsActivity();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setTitle(getResources().getString(R.string.txt_setting_toolbar));
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //setup on click
        btnSignOut.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        imgProfileSettings.setOnClickListener(this);
        btnTHB.setOnClickListener(this);
        btnUSD.setOnClickListener(this);
        btnLak.setOnClickListener(this);
        btnCny.setOnClickListener(this);
        con_mode_online.setOnClickListener(this);
        con_mode_offline.setOnClickListener(this);
        setonClickDownloadData();

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //check language
        checkSelectedLanguage();
        settingsModel = new SettingsModel();

        //check status login
        isLogin = getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).getBoolean("APP_LOGIN", false);
        if (isLogin){
            btnLogOutApp.setVisibility(View.VISIBLE);
            btnLoginApp.setVisibility(View.GONE);
        }else {
            btnLogOutApp.setVisibility(View.GONE);
            btnLoginApp.setVisibility(View.VISIBLE);
        }

        //check mode online and offline
        isModeOnline = getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        if (isModeOnline){
            img_mode_online.setVisibility(View.VISIBLE);
            img_mode_offline.setVisibility(View.GONE);
        }else {
            img_mode_online.setVisibility(View.GONE);
            img_mode_offline.setVisibility(View.VISIBLE);
        }

        //check user id
        getUserProfile();

        //check currency default
        checkCurrencyDefault();

        //initializing ArrayList
        setupDataLanguage();
        setupDataCurrency();

        //setup recyclerview
        setupRecyclerViewLanguage();
        setupRecyclerViewCurrency();
    }

    private void setonClickDownloadData() {
        button_download_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog();
            }
        });
    }

    private void setupDownloadContent(){
        int getVersion = getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
        if (getVersion != 0) {
            mDatabase = new DatabaseHelper(this, getVersion + 1);
        }else {
            mDatabase = new DatabaseHelper(this, getVersion + 1);
        }
        mManager = new DatabaseManager(this, mDatabase);

        String[] url_json = {
                getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ProgramTour/",
                getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items",
                getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Reservations/PackageTours",
                getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Reservations/EventTicket",
                getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Reservations/CarRent"
        };

        new DownloadProgramTourAsyncTask(this).execute(url_json[0]);
        new DownloadItemsAsyncTask(this).execute(url_json[1]);
        new DownloadPackageTourAsyncTask(this).execute(url_json[2]);
        new DownloadTicketEventAsyncTask(this).execute(url_json[3]);
        new DownloadCarRentalAsyncTask(this).execute(url_json[4]);

        getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).edit().putInt("PREF_APP_VERSION_DB", getVersion+1).apply();
    }

    private void getUserProfile() {
        String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        String url ="";
        if (isModeOnline) {
            if (userId != null) {
                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/user/" + userId;
                new UserAsyncTask(this).execute(url);
            }
        }else {
            btnLogOutApp.setVisibility(View.GONE);
            btnLoginApp.setVisibility(View.GONE);
        }

    }

    private void checkCurrencyDefault() {
        switch (getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB")) {
            case "THB":
                imgTHB.setVisibility(View.VISIBLE);
                break;
            case "USD":
                imgUSD.setVisibility(View.VISIBLE);
                break;
            case "LAK":
                imgLak.setVisibility(View.VISIBLE);
                break;
            case "CNY":
                imgCny.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void checkSelectedLanguage() {
        switch (languageLocale.getLanguage().toString()){
            case "th":
                isLanguageTH = true;
                isLanguageEN = false;
                isLanguageLO = false;
                isLanguageZH = false;
                break;
            case "en":
                isLanguageTH = false;
                isLanguageEN = true;
                isLanguageLO = false;
                isLanguageZH = false;
                break;
            case "lo":
                isLanguageTH = false;
                isLanguageEN = false;
                isLanguageLO = true;
                isLanguageZH = false;
                break;
            case "zh":
                isLanguageTH = false;
                isLanguageEN = false;
                isLanguageLO = false;
                isLanguageZH = true;
                break;
        }
    }

    private void setupDataLanguage() {
        itemsLanguageArrayList = new ArrayList<>();
        itemsLanguageArrayList.add(new SettingsModel(0,"ภาษาไทย",isLanguageTH));
        itemsLanguageArrayList.add(new SettingsModel(1,"English",isLanguageEN));
        itemsLanguageArrayList.add(new SettingsModel(2,"ລາວ",isLanguageLO));
        itemsLanguageArrayList.add(new SettingsModel(3,"中文",isLanguageZH));
    }

    private void setupDataCurrency() {
        itemsCurrencyArrayList = new ArrayList<>();
        itemsCurrencyArrayList.add(new SettingsModel(0,"THB",isTHB));
        itemsCurrencyArrayList.add(new SettingsModel(1,"USD",isUSD));
        itemsCurrencyArrayList.add(new SettingsModel(2,"LAK",isLAK));
        itemsCurrencyArrayList.add(new SettingsModel(3,"CNY",isCNY));
    }

    private void setupRecyclerViewLanguage() {
        mRecyclerViewLanguage = findViewById(R.id.recycler_setting_language);
        mAdapterLanguage = new SettingsAdapterRecyclerView(this,itemsLanguageArrayList,this);
        mRecyclerViewLanguage.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewLanguage.setAdapter(mAdapterLanguage);
    }

    private void setupRecyclerViewCurrency() {
        mRecyclerViewCurrency = findViewById(R.id.recycler_setting_currency);
        mAdapterCurrency = new SettingsAdapterRecyclerView(this,itemsCurrencyArrayList,this);
        mRecyclerViewCurrency.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewCurrency.setAdapter(mAdapterCurrency);
    }

    public void showContentLanguage(View view){
        //views
        mShowContentLanguage = findViewById(R.id.expandable_setting_language);
        mImgUpDownLanguage = findViewById(R.id.img_arrow_language);
        mShowContentLanguage.toggle();
        if (mShowContentLanguage.isExpanded()){
            mImgUpDownLanguage.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownLanguage.setImageResource(R.drawable.ic_arrow_right);
        }
    }

    public void showContentCurrency(View view){
        //views
        mShowContentCurrency = findViewById(R.id.expandable_setting_currency);
        mImgUpDownCurrency = findViewById(R.id.img_arrow_currency);
        mShowContentCurrency.toggle();
        if (mShowContentCurrency.isExpanded()){
            mImgUpDownCurrency.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownCurrency.setImageResource(R.drawable.ic_arrow_right);
        }
    }

    public void showContentMode(View view){
        //views
        mShowContentMode = findViewById(R.id.expandable_setting_mode);
        mImgUpDownMode = findViewById(R.id.img_arrow_mode);
        mShowContentMode.toggle();
        if (mShowContentMode.isExpanded()){
            mImgUpDownMode.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownMode.setImageResource(R.drawable.ic_arrow_right);
        }
    }

    public void showContentBooking(View view){
        //views
        mShowContentBooking = findViewById(R.id.expandable_setting_booking);
        mImgUpDownBooking = findViewById(R.id.img_arrow_booking);
        mShowContentBooking.toggle();
        if (mShowContentBooking.isExpanded()){
            mImgUpDownBooking.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownBooking.setImageResource(R.drawable.ic_arrow_right);
        }

        if (isModeOnline) {
            startActivity(new Intent(this, BookingActivity.class));
        }else {
            Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onBackPressedSettingsActivity(){
        startActivity(new Intent(this,MainActivity.class));
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,MainActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_log_off:
                SignOutFragment dialog = new SignOutFragment();
                dialog.show(getSupportFragmentManager(),"SignOutFragment");
                break;
            case R.id.btn_sign_up:
                startActivity(new Intent(SettingsActivity.this, LoginActivity.class));
                break;
            case R.id.img_profile_settings:
                Intent intent = new Intent(SettingsActivity.this,ProfileActivity.class);
                intent.putExtra("USER_MODEL",userModels);
                startActivity(intent);
                break;
            case R.id.con_thb:
                imgTHB.setVisibility(View.VISIBLE);
                imgUSD.setVisibility(View.GONE);
                imgLak.setVisibility(View.GONE);
                imgCny.setVisibility(View.GONE);
                getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).edit().putString("APP_CURRENCY", "THB").apply();
                break;
            case R.id.con_usd:
                imgTHB.setVisibility(View.GONE);
                imgUSD.setVisibility(View.VISIBLE);
                imgLak.setVisibility(View.GONE);
                imgCny.setVisibility(View.GONE);
                getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).edit().putString("APP_CURRENCY", "USD").apply();
                break;
            case R.id.con_lak:
                imgTHB.setVisibility(View.GONE);
                imgUSD.setVisibility(View.GONE);
                imgLak.setVisibility(View.VISIBLE);
                imgCny.setVisibility(View.GONE);
                getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).edit().putString("APP_CURRENCY", "LAK").apply();
                break;
            case R.id.con_cny:
                imgTHB.setVisibility(View.GONE);
                imgUSD.setVisibility(View.GONE);
                imgLak.setVisibility(View.GONE);
                imgCny.setVisibility(View.VISIBLE);
                getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).edit().putString("APP_CURRENCY", "CNY").apply();
                break;
            case R.id.con_mode_online:
                isModeOnline = true;
                getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).edit().putBoolean("APP_MODE_ONLINE", isModeOnline).apply();
                recreate();
                break;
            case R.id.con_mode_offline:
                isModeOnline = false;
                getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).edit().putBoolean("APP_MODE_ONLINE", isModeOnline).apply();
                recreate();
                break;
        }
    }

    @Override
    public void itemClicked(SettingsModel settingsModel, ImageView imgCheck) {
        switch (settingsModel.getTitle().toString()) {
            case "ภาษาไทย":
                languageLocale.setLocale("th");
                recreate();
                break;
            case "English":
                languageLocale.setLocale("en");
                recreate();
                break;
            case "ລາວ":
                languageLocale.setLocale("lo");
                recreate();
                break;
            case "中文":
                languageLocale.setLocale("zh");
                recreate();
                break;
            case "THB":
                isTHB = true;
                isUSD = false;
                isLAK = false;
                isCNY = false;
                break;
            case "USD":
                isTHB = false;
                isUSD = true;
                isLAK = false;
                isCNY = false;
                break;
            case "LAK":
                isTHB = false;
                isUSD = false;
                isLAK = true;
                isCNY = false;
                break;
            case "CNY":
                isTHB = false;
                isUSD = false;
                isLAK = false;
                isCNY = true;
                break;
        }
        if (settingsModel.isSelected()){
            settingsModel.setSelected(false);
            imgCheck.setVisibility(View.INVISIBLE);
        }else {
            settingsModel.setSelected(true);
            imgCheck.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPreCallService() {
        btnLoginApp.setVisibility(View.GONE);
        btnLogOutApp.setVisibility(View.GONE);
        setViewVisibility(R.id.con_loader, View.VISIBLE);
        setViewVisibility(R.id.multipleRippleLoader, View.VISIBLE);
    }

    @Override
    public void onCallService() {

    }


    @Override
    public void onRequestCompleteListener(UserModel userModels) {
        if (userModels != null){
            Log.e("check data",userModels+"");
            this.userModels = userModels;
            Picasso.get().load(userModels.getProfilePicUrl()).error(R.drawable.img_laceholder_error).placeholder(R.drawable.img_placeholder).into(imgProfileSettings);
            txt_profile_full_name.setText(userModels.getFirstName()+" "+userModels.getLastName());
            txt_login_with.setText(getResources().getString(R.string.text_login_via)+" "+userModels.getLoginWith());

            btnLogOutApp.setVisibility(View.VISIBLE);
            setViewVisibility(R.id.con_loader, View.GONE);
            setViewVisibility(R.id.multipleRippleLoader, View.GONE);

        }else {
            setViewVisibility(R.id.con_loader, View.VISIBLE);
            setViewVisibility(R.id.img_load_field_id, View.VISIBLE);
            setViewVisibility(R.id.txt_load_field_id, View.VISIBLE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Offline Data Loading");
        builder.setMessage("\n It may take a few minutes to download.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setupDownloadContent();
                showProgressDialog();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
    private void alertDialogSuccess(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Offline Data Loading");
        builder.setMessage("\n download success.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void showProgressDialog(){
        alertDialog = new ProgressDialog(this);
        alertDialog.setTitle("Don't close app");
        alertDialog.setMessage("Please wait...");
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    private void setViewVisibility(int id, int visibility) {
        View view = findViewById(id);
        if (view != null) {
            view.setVisibility(visibility);
        }
    }


    @Override
    public void onPreCallServiceDownload() {

    }

    @Override
    public void onCallServiceDownload() {

    }

    @Override
    public void onRequestCompleteListenerDownloadCarRental(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            mManager.addCarRental(itemsModelArrayList);
            dismissProgressDialog();
            alertDialogSuccess();
            Toast.makeText(this, "download success.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestCompleteListenerDownloadTicketEvent(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            mManager.addTicketEvent(itemsModelArrayList);
        }
    }

    @Override
    public void onRequestCompleteListenerDownloadPackageTour(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            mManager.addPackageTour(itemsModelArrayList);
        }
    }

    @Override
    public void onRequestCompleteListenerDownloadItems(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            mManager.addItems(itemsModelArrayList);
        }
    }

    @Override
    public void onRequestCompleteListenerDownloadProgramTour(ArrayList<ProgramTourModel> programTourModelArrayList) {
        if (programTourModelArrayList != null && programTourModelArrayList.size() != 0){
            mManager.addProgramTour(programTourModelArrayList);
        }
    }

    @Override
    public void onRequestFailedDownload(String result) {

    }
}
