package com.ipanda.lanexangtourism.Activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;

public class SplashActivity extends AppCompatActivity {

    //Variables
    private static int SPLASH_SCREEN = 4000;

    private static final int REQ_CODE = 123;

    private Animation topAnim, bottomAnim;

    private ImageView splashImage1,splashImage2,splashImage3,splashImage4;

    private ChangeLanguageLocale languageLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //Animation
        topAnim = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_animation);

        //Hooks
        splashImage1 = findViewById(R.id.splash_image_1);
        splashImage2 = findViewById(R.id.splash_image_2);
        splashImage3 = findViewById(R.id.splash_image_3);
        splashImage4 = findViewById(R.id.splash_image_4);

        //Ues
        splashImage1.setAnimation(topAnim);
        splashImage2.setAnimation(bottomAnim);
        splashImage3.setAnimation(bottomAnim);
        splashImage4.setAnimation(bottomAnim);

        boolean checkLogin = getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).getBoolean("APP_LOGIN", false);
        boolean checkLanguage = getSharedPreferences("PREF_APP_STATE_LANGUAGE", Context.MODE_PRIVATE).getBoolean("APP_STATE_LANGUAGE", false);

        //Wait 5 second
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkLogin){
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }else {
                    if (checkLanguage){
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    }else {
                        Intent intent = new Intent(SplashActivity.this, LanguageActivity.class);
                        startActivityForResult(intent, REQ_CODE);
                    }

                }
            }
        },SPLASH_SCREEN);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE && resultCode == RESULT_OK) {
            recreate();
        }
    }
}
