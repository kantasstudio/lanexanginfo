package com.ipanda.lanexangtourism.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.VideoContentAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.VideoContentModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.VideoContentAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.VideoContentCallBack;

import java.util.ArrayList;

public class VideoContentActivity extends AppCompatActivity implements VideoContentCallBack {

    //variables
    private static String TAG = VideoContentActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private String userId;

    private RecyclerView recycler_list_video_content;

    private VideoContentAdapter videoContentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_content);
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", "");

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //views
        mToolbar = findViewById(R.id.menu_toolbar);
        recycler_list_video_content = findViewById(R.id.recycler_list_video_content);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        String urlVideoContent = getString(R.string.app_api_ip) +"dasta_thailand/api/mobile/user/Adsense/getVideoContent";
        new VideoContentAsyncTask(this).execute(urlVideoContent);

    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteVideoContent(ArrayList<VideoContentModel> videoContentArrayList) {
        if (videoContentArrayList != null && videoContentArrayList.size() != 0){
            videoContentAdapter = new VideoContentAdapter(this, videoContentArrayList ,languageLocale.getLanguage());
            recycler_list_video_content.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            recycler_list_video_content.setAdapter(videoContentAdapter);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }
}