package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.AddPhotoClickListener;
import com.ipanda.lanexangtourism.Interface_click.DatesTripClickListener;
import com.ipanda.lanexangtourism.Interface_click.PlacesItemClickListener;
import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class AddDatesTripAdapter extends RecyclerView.Adapter<AddDatesTripAdapter.ItemViewHolder> {

    private Context context;
    private ArrayList<DatesTripModel> modelDatesTripArrayList;
    private ArrayList<DatesTripModel> returnDatesTopic;
    private String language;
    private DatesTripClickListener listener;
    private AddPhotoClickListener photoListener;
    private PlacesItemClickListener placesListener;
    private PlacesRecyclerViewAdapter mPlacesAdapter;


    public AddDatesTripAdapter(Context context, ArrayList<DatesTripModel> modelDatesTripArrayList, String language, DatesTripClickListener listener, PlacesItemClickListener placesListener, AddPhotoClickListener photoListener) {
        this.context = context;
        this.modelDatesTripArrayList = modelDatesTripArrayList;
        this.returnDatesTopic = new ArrayList<>(modelDatesTripArrayList);
        this.language = language;
        this.listener = listener;
        this.placesListener = placesListener;
        this.photoListener = photoListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_days_program_tour_view, parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final DatesTripModel tripModel = (DatesTripModel) modelDatesTripArrayList.get(position);
        final DatesTripModel trip = (DatesTripModel) returnDatesTopic.get(position);

        ((ItemViewHolder)holder).txt_dates_no.setText("วันที่"+(position+1)+": ");
        switch (language) {
            case "th":
                ((ItemViewHolder)holder).edt_dates_topic.setText(tripModel.getDatesTripTopicThai());
                break;
            case "en":
                ((ItemViewHolder)holder).edt_dates_topic.setText(tripModel.getDatesTripTopicEnglish());
                break;
            case "lo":
                ((ItemViewHolder)holder).edt_dates_topic.setText(tripModel.getDatesTripTopicLaos());
                break;
            case "zh":
                ((ItemViewHolder)holder).edt_dates_topic.setText(tripModel.getDatesTripTopicChinese());
                break;
        }

        ((ItemViewHolder)holder).edt_dates_topic.setFocusable(false);
        ((ItemViewHolder)holder).edt_dates_topic.setEnabled(false);
        ((ItemViewHolder)holder).img_edit_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ItemViewHolder)holder).edt_dates_topic.setFocusable(true);
                ((ItemViewHolder)holder).edt_dates_topic.setEnabled(true);
                ((ItemViewHolder)holder).edt_dates_topic.setCursorVisible(true);
                ((ItemViewHolder)holder).edt_dates_topic.setClickable(true);
                ((ItemViewHolder)holder).edt_dates_topic.setFocusableInTouchMode(true);
            }
        });

        ((ItemViewHolder)holder).edt_dates_topic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String convert  = charSequence.toString();
                switch (language){
                    case "th":
                        trip.setDatesTripTopicThai(convert);
                        break;
                    case "en":
                        trip.setDatesTripTopicEnglish(convert);
                        break;
                    case "lo":
                        trip.setDatesTripTopicLaos(convert);
                        break;
                    case "zh":
                        trip.setDatesTripTopicChinese(convert);
                        break;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ((ItemViewHolder)holder).img_edit_dates_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickEventMore(view, tripModel,position);
            }
        });

        ((ItemViewHolder)holder).btn_cv_selected_places.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placesListener.itemClickedPlaces(tripModel);
            }
        });


        if (tripModel.getTouristAttractionsModelArrayList() != null && tripModel.getTouristAttractionsModelArrayList().size() != 0) {

            mPlacesAdapter = new PlacesRecyclerViewAdapter(context, tripModel.getTouristAttractionsModelArrayList(), language, photoListener, tripModel);
            ((ItemViewHolder) holder).recycler_places_id.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
            ((ItemViewHolder) holder).recycler_places_id.setAdapter(mPlacesAdapter);


        }
/*        if (mPlacesAdapter != null) {
            if (mPlacesAdapter.returnTouristAttractionsArrayList() != null && mPlacesAdapter.returnTouristAttractionsArrayList().size() != 0) {
                returnDatesTopic.get(position).getTouristAttractionsModelArrayList().clear();
                returnDatesTopic.get(position).setTouristAttractionsModelArrayList(mPlacesAdapter.returnTouristAttractionsArrayList());
            }
        }*/
    }


    @Override
    public int getItemCount() {
        return modelDatesTripArrayList.size();
    }

    private void onClickEventMore(View v, DatesTripModel tripModel, int position) {
        PopupMenu menu = new PopupMenu(context,v);
        menu.getMenuInflater().inflate(R.menu.menu_more,menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_edit:
                        listener.onClickEventDatesTrip(tripModel, position,"edit");
                        break;
                    case R.id.action_delete:
                        listener.onClickEventDatesTrip(tripModel, position,"delete");
                        break;
                }

                return true;
            }
        });
        menu.show();
    }

    public ArrayList<DatesTripModel> returnDatesTopic(){
        return returnDatesTopic;
    }






    public class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView txt_dates_no;
        EditText edt_dates_topic;
        ImageView img_edit_topic;
        ImageView img_edit_dates_trip;
        RecyclerView recycler_places_id;
        CardView btn_cv_selected_places;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_dates_no = itemView.findViewById(R.id.txt_dates_no);
            edt_dates_topic = itemView.findViewById(R.id.edt_dates_topic);
            img_edit_topic = itemView.findViewById(R.id.img_edit_topic);
            img_edit_dates_trip = itemView.findViewById(R.id.img_edit_dates_trip);
            recycler_places_id = itemView.findViewById(R.id.recycler_places_id);
            btn_cv_selected_places = itemView.findViewById(R.id.btn_cv_selected_places);

        }

    }


}
