package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AddPhotoDatesTripAdapter extends RecyclerView.Adapter<AddPhotoDatesTripAdapter.ImagePlacesViewHolder> {

    private Context context;
    private ArrayList<PhotoTourist> arrayList;

    public AddPhotoDatesTripAdapter(Context context, ArrayList<PhotoTourist> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ImagePlacesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_view2, parent,false);
        return new ImagePlacesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagePlacesViewHolder holder, int position) {
        final PhotoTourist photo = (PhotoTourist) arrayList.get(position);

        if (photo.isPaths()) {
            Glide.with(context).load(photo.getPhotoTouristPaths()).apply(new RequestOptions()).into(holder.image);
        }else {
            String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
            Picasso.get().load(paths+photo.getPhotoTouristPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ImagePlacesViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        public ImagePlacesViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.img_item_photo_id);
        }
    }
}
