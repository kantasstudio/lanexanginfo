package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

public class AddPlacesAdapter extends RecyclerView.Adapter<AddPlacesAdapter.PlacesViewHolder>{

    private Context context;
    private ItemsModel arrayList;
    private String language;

    public AddPlacesAdapter(Context context, ItemsModel arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
    }

    @NonNull
    @Override
    public PlacesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_places_view, parent,false);
        return new PlacesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesViewHolder holder, int position) {
        final ItemsModel items = (ItemsModel) arrayList;

        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+items.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((PlacesViewHolder)holder).img_item_cover_id);

        switch (language){
            case "th":
                ((PlacesViewHolder)holder).txt_item_topic_id.setText(items.getTopicTH());
                break;
            case "en":
                ((PlacesViewHolder)holder).txt_item_topic_id.setText(items.getTopicEN());
                break;
            case "lo":
                ((PlacesViewHolder)holder).txt_item_topic_id.setText(items.getTopicLO());
                break;
            case "zh":
                ((PlacesViewHolder)holder).txt_item_topic_id.setText(items.getTopicZH());
                break;
        }


    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public ItemsModel getArrayList() {
        return arrayList;
    }

    public class PlacesViewHolder extends RecyclerView.ViewHolder{

        ImageView img_item_cover_id;
        TextView txt_item_topic_id;
        RecyclerView recycler_item_image_id;

        public PlacesViewHolder(@NonNull View itemView) {
            super(itemView);

            img_item_cover_id = itemView.findViewById(R.id.img_item_cover_id);
            txt_item_topic_id = itemView.findViewById(R.id.txt_item_topic_id);
            recycler_item_image_id = itemView.findViewById(R.id.recycler_item_image_id);

        }
    }


}
