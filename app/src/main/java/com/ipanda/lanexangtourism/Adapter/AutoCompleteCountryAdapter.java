package com.ipanda.lanexangtourism.Adapter;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ipanda.lanexangtourism.Model.CountryModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class AutoCompleteCountryAdapter extends ArrayAdapter<CountryModel> {

    private ArrayList<CountryModel> countryListFull;
    private ArrayList<CountryModel> countryListDefault;
    private String language;

    public AutoCompleteCountryAdapter(@NonNull Context context, @NonNull ArrayList<CountryModel> countryList, String language) {
        super(context, 0, countryList);
        this.language = language;
        countryListFull = new ArrayList<>(countryList);
        this.countryListDefault = countryList;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return countryFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CountryModel country = getItem(position);
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_layout_item_spinner, parent, false);
        }
        TextView txt_title_spinner = convertView.findViewById(R.id.txt_title_spinner);
        if (country != null) {

            switch (language){
                case "th":
                    txt_title_spinner.setText(country.getCountryTH());
                    break;
                case "en":
                    txt_title_spinner.setText(country.getCountryEN());
                    break;
                case "lo":
                    txt_title_spinner.setText(country.getCountryLO());
                    break;
                case "zh":
                    txt_title_spinner.setText(country.getCountryZH());
                    break;
            }

        }
        Toast.makeText(getContext(), getPosition(country)+"", Toast.LENGTH_SHORT).show();

        return convertView;
    }

    private Filter countryFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            ArrayList<CountryModel> suggestions = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(countryListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (CountryModel item : countryListFull) {
                    switch (language){
                        case "th":
                            if (item.getCountryTH().toLowerCase().contains(filterPattern)) {
                                suggestions.add(item);
                            }
                            break;
                        case "en":
                            if (item.getCountryEN().toLowerCase().contains(filterPattern)) {
                                suggestions.add(item);
                            }
                            break;
                        case "lo":
                            if (item.getCountryLO().toLowerCase().contains(filterPattern)) {
                                suggestions.add(item);
                            }
                            break;
                        case "zh":
                            if (item.getCountryZH().toLowerCase().contains(filterPattern)) {
                                suggestions.add(item);
                            }
                            break;
                    }

                }
            }

            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((ArrayList) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String country = "";

            switch (language){
                case "th":
                    country = ((CountryModel) resultValue).getCountryTH();
                    break;
                case "en":
                    country = ((CountryModel) resultValue).getCountryEN();
                    break;
                case "lo":
                    country = ((CountryModel) resultValue).getCountryLO();
                    break;
                case "zh":
                    country = ((CountryModel) resultValue).getCountryZH();
                    break;
            }
            return country;
        }
    };


}
