package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.BookingItemsClickListener;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class BookingItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<BookingModel> arrayList;
    private String language;
    private BookingItemsClickListener listener;
    private static int TYPE_PACKAGE = 1;
    private static int TYPE_TICKET = 2;
    private static int TYPE_CAR = 3;
    private static int TYPE_HOTEL = 4;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;


    public BookingItemsAdapter(Context context, ArrayList<BookingModel> arrayList, String language,BookingItemsClickListener listener, RateModel rateModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
        this.rateModel = rateModel;
        exchangeRate = new ExchangeRate();
    }

    @Override
    public int getItemViewType(int position) {
        if (arrayList.get(position).getItemsModel() != null) {
            if (arrayList.get(position).getItemsModel().getMenuItemModel().getMenuItemId() == 9) {
                return TYPE_PACKAGE;
            } else if (arrayList.get(position).getItemsModel().getMenuItemModel().getMenuItemId() == 10) {
                return TYPE_TICKET;
            } else if (arrayList.get(position).getItemsModel().getMenuItemModel().getMenuItemId() == 11) {
                return TYPE_CAR;
            } else {
                return TYPE_HOTEL;
            }
        }else {
            return position;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view;
        if (viewType == TYPE_PACKAGE) {
            view = LayoutInflater.from(context).inflate(R.layout.booking_package_tour, viewGroup, false);
            return new PackageViewHolder(view);
        }
        else if (viewType == TYPE_TICKET){
            view = LayoutInflater.from(context).inflate(R.layout.booking_ticket, viewGroup, false);
            return new TicketViewHolder(view);
        }
        else if (viewType == TYPE_CAR){
            view = LayoutInflater.from(context).inflate(R.layout.booking_car, viewGroup, false);
            return new CarViewHolder(view);
        }
        else{
            view = LayoutInflater.from(context).inflate(R.layout.booking_hotels, viewGroup, false);
            return new HotelViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final BookingModel bookings = (BookingModel) arrayList.get(position);
        if (getItemViewType(position) == TYPE_PACKAGE) {
            if (bookings.getItemsModel() != null) {
                ((PackageViewHolder) holder).setCallPackage(bookings, bookings.getItemsModel());
                ((PackageViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.clickedPackage(bookings);
                    }
                });
            }

        }
        else if (getItemViewType(position) == TYPE_TICKET){
            if (bookings.getItemsModel() != null) {
                ((TicketViewHolder) holder).setCallTicket(bookings, bookings.getItemsModel());
                ((TicketViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.clickedTicket(bookings);
                    }
                });
            }

        }
        else if (getItemViewType(position) == TYPE_CAR){
            if (bookings.getItemsModel() != null) {
                ((CarViewHolder) holder).setCallCar(bookings, bookings.getItemsModel());
                ((CarViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.clickedCar(bookings);
                    }
                });
            }
        }
        else {
            if (bookings.getItemsModel() != null) {
                ((HotelViewHolder) holder).setCallHotel(bookings, bookings.getItemsModel().getRoomModel());
                ((HotelViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.clickedHotel(bookings);
                    }
                });
            }

        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class PackageViewHolder extends RecyclerView.ViewHolder{

        private ImageView img_package_tour;
        private ImageView img_booking_share;
        private ImageView img_booking_status;
        private TextView txt_title_package_tour;
        private TextView txt_mark_location;
        private TextView txt_type_activity;
        private TextView txt_period;
        private TextView txt_booking_period;
        private TextView txt_company;
        private TextView txt_details;
        private TextView txt_booking_status;
        private TextView txt_period_title;

        public PackageViewHolder(@NonNull View itemView) {
            super(itemView);
            img_package_tour = itemView.findViewById(R.id.img_package_tour);
            img_booking_share = itemView.findViewById(R.id.img_booking_share);
            img_booking_status = itemView.findViewById(R.id.img_booking_status);
            txt_title_package_tour = itemView.findViewById(R.id.txt_title_package_tour);
            txt_mark_location = itemView.findViewById(R.id.txt_mark_location);
            txt_type_activity = itemView.findViewById(R.id.txt_type_activity);
            txt_period = itemView.findViewById(R.id.txt_period);
            txt_booking_period = itemView.findViewById(R.id.txt_booking_period);
            txt_company = itemView.findViewById(R.id.txt_company);
            txt_details = itemView.findViewById(R.id.txt_details);
            txt_booking_status = itemView.findViewById(R.id.txt_booking_status);
            txt_period_title = itemView.findViewById(R.id.txt_period);
        }

        private void setCallPackage(BookingModel booking,ItemsModel items) {
            String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
            Picasso.get().load(paths+items.getCoverItemsModelArrayList().get(0).getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_package_tour);
            switch (language){
                case "th":
                    txt_title_package_tour.setText(items.getTopicTH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusTH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                    break;
                case "en":
                    txt_title_package_tour.setText(items.getTopicEN());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusEN());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                    break;
                case "lo":
                    txt_title_package_tour.setText(items.getTopicLO());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusLO());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                    break;
                case "zh":
                    txt_title_package_tour.setText(items.getTopicZH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusZH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                    break;
            }

            txt_booking_period.setText(booking.getCheckIn() + " - " + booking.getCheckOut());

            if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 1){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_booked));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_booked));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 2){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_wait));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_wait));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 3){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_refuse));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_refuse));
            }else {

            }

            if (items.getBusinessModel() != null){
                switch (language){
                    case "th":
                        txt_company.setText(items.getBusinessModel().getNameTH());
                        break;
                    case "en":
                        txt_company.setText(items.getBusinessModel().getNameEN());
                        break;
                    case "lo":
                        txt_company.setText(items.getBusinessModel().getNameLO());
                        break;
                    case "zh":
                        txt_company.setText(items.getBusinessModel().getNameZH());
                        break;
                }
            }

            if (items.getProvincesModelArrayList() != null && items.getProvincesModelArrayList().size() != 0){
                String province = "";
                for (ProvincesModel pro : items.getProvincesModelArrayList())
                switch (language){
                    case "th":
                        province += pro.getProvincesTH()+" ";
                        break;
                    case "en":
                        province += pro.getProvincesEN()+" ";
                        break;
                    case "lo":
                        province += pro.getProvincesLO()+" ";
                        break;
                    case "zh":
                        province += pro.getProvincesZH()+" ";
                        break;
                }
                txt_mark_location.setText(province);
            }
            img_booking_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.clickedSharePackage(items);
                }
            });

            if (items.getTravelDetailsModelArrayList() != null && items.getTravelDetailsModelArrayList().size() != 0) {
                int day = items.getTravelDetailsModelArrayList().size();
                txt_period_title.setText(day+ " " +context.getResources().getString(R.string.text_day) + " "
                        + (day - 1) + " " + context.getResources().getString(R.string.text_night));
            }
        }

    }

    public class TicketViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_package_tour;
        private ImageView img_booking_share;
        private ImageView img_booking_status;
        private TextView txt_title_package_tour;
        private TextView txt_mark_location;
        private TextView txt_type_activity;
        private TextView txt_period;
        private TextView txt_booking_period;
        private TextView txt_company;
        private TextView txt_details;
        private TextView txt_booking_status;

        public TicketViewHolder(@NonNull View itemView) {
            super(itemView);
            img_package_tour = itemView.findViewById(R.id.img_package_tour);
            img_booking_share = itemView.findViewById(R.id.img_booking_share);
            img_booking_status = itemView.findViewById(R.id.img_booking_status);
            txt_title_package_tour = itemView.findViewById(R.id.txt_title_package_tour);
            txt_mark_location = itemView.findViewById(R.id.txt_mark_location);
            txt_type_activity = itemView.findViewById(R.id.txt_type_activity);
            txt_period = itemView.findViewById(R.id.txt_period);
            txt_booking_period = itemView.findViewById(R.id.txt_booking_period);
            txt_company = itemView.findViewById(R.id.txt_company);
            txt_details = itemView.findViewById(R.id.txt_details);
            txt_booking_status = itemView.findViewById(R.id.txt_booking_status);
        }

        private void setCallTicket(BookingModel booking,ItemsModel items) {
            String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
            Picasso.get().load(paths+items.getCoverItemsModelArrayList().get(0).getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_package_tour);

            switch (language){
                case "th":
                    txt_title_package_tour.setText(items.getTopicTH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusTH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                    break;
                case "en":
                    txt_title_package_tour.setText(items.getTopicEN());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusEN());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                    break;
                case "lo":
                    txt_title_package_tour.setText(items.getTopicLO());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusLO());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                    break;
                case "zh":
                    txt_title_package_tour.setText(items.getTopicZH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusZH());
                    txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                    break;
            }

            txt_booking_period.setText(booking.getCheckIn() + " - " + booking.getCheckOut());

            if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 1){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_booked));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_booked));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 2){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_wait));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_wait));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 3){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_refuse));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_refuse));
            }else {

            }

            if (items.getBusinessModel() != null){
                switch (language){
                    case "th":
                        txt_company.setText(items.getBusinessModel().getNameTH());
                        break;
                    case "en":
                        txt_company.setText(items.getBusinessModel().getNameEN());
                        break;
                    case "lo":
                        txt_company.setText(items.getBusinessModel().getNameLO());
                        break;
                    case "zh":
                        txt_company.setText(items.getBusinessModel().getNameZH());
                        break;
                }
            }

            if (items.getProvincesModel() != null){
                switch (language){
                    case "th":
                        txt_mark_location.setText(items.getProvincesModel().getProvincesTH());
                        break;
                    case "en":
                        txt_mark_location.setText(items.getProvincesModel().getProvincesEN());
                        break;
                    case "lo":
                        txt_mark_location.setText(items.getProvincesModel().getProvincesLO());
                        break;
                    case "zh":
                        txt_mark_location.setText(items.getProvincesModel().getProvincesZH());
                        break;
                }

            }

            img_booking_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.clickedShareTicket(items);
                }
            });

        }
    }

    public class CarViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_items;
        private ImageView img_booking_status;
        private TextView txt_booking_status;
        private TextView txt_name_car;
        private TextView txt_type_car;
        private TextView txt_transmission_car;
        private TextView txt_passenger;
        private TextView txt_price_car;
        private TextView txt_company;
        private TextView txt_exchange_rate_id;

        public CarViewHolder(@NonNull View itemView) {
            super(itemView);
            img_items = itemView.findViewById(R.id.img_items);
            img_booking_status = itemView.findViewById(R.id.img_booking_status);
            txt_booking_status = itemView.findViewById(R.id.txt_booking_status);
            txt_name_car = itemView.findViewById(R.id.txt_name_car);
            txt_type_car = itemView.findViewById(R.id.txt_type_car);
            txt_transmission_car = itemView.findViewById(R.id.txt_transmission_car);
            txt_passenger = itemView.findViewById(R.id.txt_passenger);
            txt_price_car = itemView.findViewById(R.id.txt_price_car);
            txt_company = itemView.findViewById(R.id.txt_company_name);
            txt_exchange_rate_id = itemView.findViewById(R.id.txt_exchange_rate_title_id);
        }

        private void setCallCar(BookingModel booking, ItemsModel items) {
            String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
            Picasso.get().load(paths+items.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_items);
            txt_passenger.setText(context.getResources().getString(R.string.text_passenger)+" : "+items.getCarModel().getSeats()+"");

            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
            String price  = null;


            if (rateModel != null) {
                String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
                switch (currency) {
                    case "THB":
                        price = decimalFormat.format(items.getCarModel().getPricePerDay());
                        txt_exchange_rate_id.setText(currency);
                        txt_price_car.setText(price);
                        break;
                    case "USD":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateUSD()));
                        txt_exchange_rate_id.setText(currency);
                        txt_price_car.setText(price);
                        break;
                    case "CNY":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateCNY()));
                        txt_exchange_rate_id.setText(currency);
                        txt_price_car.setText(price);
                        break;
                }
            }else {
                price = decimalFormat.format(items.getCarModel().getPricePerDay());
                txt_price_car.setText(price);
            }

            switch (language){
                case "th":
                    txt_name_car.setText(items.getTopicTH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusTH());
                    txt_type_car.setText(context.getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeTH());
                    txt_transmission_car.setText(context.getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemTH());
                    break;
                case "en":
                    txt_name_car.setText(items.getTopicTH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusEN());
                    txt_type_car.setText(context.getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeEN());
                    txt_transmission_car.setText(context.getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemEN());
                    break;
                case "lo":
                    txt_name_car.setText(items.getTopicTH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusLO());
                    txt_type_car.setText(context.getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeLO());
                    txt_transmission_car.setText(context.getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemLO());
                    break;
                case "zh":
                    txt_name_car.setText(items.getTopicTH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusZH());
                    txt_type_car.setText(context.getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeZH());
                    txt_transmission_car.setText(context.getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemZH());
                    break;
            }
            if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 1){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_booked));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_booked));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 2){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_wait));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_wait));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 3){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_refuse));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_refuse));
            }else {

            }

            if (items.getBusinessModel() != null){
                switch (language){
                    case "th":
                        txt_company.setText(items.getBusinessModel().getNameTH());
                        break;
                    case "en":
                        txt_company.setText(items.getBusinessModel().getNameEN());
                        break;
                    case "lo":
                        txt_company.setText(items.getBusinessModel().getNameLO());
                        break;
                    case "zh":
                        txt_company.setText(items.getBusinessModel().getNameZH());
                        break;
                }
            }

        }
    }

    public class HotelViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_ex_hotel_id;
        private ImageView img_booking_status;
        private TextView txt_booking_status;
        private TextView txt_ex_title_hotel_id;
        private TextView txt_ex_description_hotel_id;
        private TextView txt_ex_status_breakFast_id;
        private TextView txt_ex_price_hotel_id;
        private TextView txt_exchange_rate_id;

        public HotelViewHolder(@NonNull View itemView) {
            super(itemView);
            img_ex_hotel_id = itemView.findViewById(R.id.img_ex_hotel_id);
            img_booking_status = itemView.findViewById(R.id.img_booking_status);
            txt_booking_status = itemView.findViewById(R.id.txt_booking_status);
            txt_ex_title_hotel_id = itemView.findViewById(R.id.txt_ex_title_hotel_id);
            txt_ex_description_hotel_id = itemView.findViewById(R.id.txt_ex_description_hotel_id);
            txt_ex_status_breakFast_id = itemView.findViewById(R.id.txt_ex_status_breakFast_id);
            txt_ex_price_hotel_id = itemView.findViewById(R.id.txt_ex_price_hotel_id);
            txt_exchange_rate_id = itemView.findViewById(R.id.txt_exchange_rate_id);
        }

        private void setCallHotel(BookingModel booking, RoomModel rooms) {
            String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
            Picasso.get().load(paths+ rooms.getRoomPictureModelArrayList().get(0).getPicturePaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_ex_hotel_id);

            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
            String price  = null;

            if (rateModel != null) {
                String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
                switch (currency) {
                    case "THB":
                        price = decimalFormat.format(booking.getPaymentTransactionModel().getTotal());
                        txt_exchange_rate_id.setText(currency);
                        txt_ex_price_hotel_id.setText(price);
                        break;
                    case "USD":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(booking.getPaymentTransactionModel().getTotal(),rateModel.getRateUSD()));
                        txt_exchange_rate_id.setText(currency);
                        txt_ex_price_hotel_id.setText(price);
                        break;
                    case "CNY":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(booking.getPaymentTransactionModel().getTotal(),rateModel.getRateCNY()));
                        txt_exchange_rate_id.setText(currency);
                        txt_ex_price_hotel_id.setText(price);
                        break;
                }
            }else {
                price = decimalFormat.format(booking.getPaymentTransactionModel().getTotal());
                txt_ex_price_hotel_id.setText(price);
            }

            switch (language){
                case "th":
                    txt_ex_title_hotel_id.setText(rooms.getTopicTH());
                    txt_ex_description_hotel_id.setText(rooms.getDescriptionTH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusTH());
                    if (rooms.getBreakFast() == 0){
                        txt_ex_status_breakFast_id.setText("ไม่มีอาหารเช้า");
                    }else {
                        txt_ex_status_breakFast_id.setText("อาหารเช้า");
                    }
                    break;
                case "en":
                    txt_ex_title_hotel_id.setText(rooms.getTopicEN());
                    txt_ex_description_hotel_id.setText(rooms.getDescriptionEN());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusEN());
                    if (rooms.getBreakFast() == 0){
                        txt_ex_status_breakFast_id.setText("No breakfast");
                    }else {
                        txt_ex_status_breakFast_id.setText("Breakfast");
                    }
                    break;
                case "lo":
                    txt_ex_title_hotel_id.setText(rooms.getTopicLO());
                    txt_ex_description_hotel_id.setText(rooms.getDescriptionLO());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusLO());
                    if (rooms.getBreakFast() == 0){
                        txt_ex_status_breakFast_id.setText("ບໍ່ມີອາຫານເຊົ້າ");
                    }else {
                        txt_ex_status_breakFast_id.setText("ອາຫານເຊົ້າ");
                    }
                    break;
                case "zh":
                    txt_ex_title_hotel_id.setText(rooms.getTopicZH());
                    txt_ex_description_hotel_id.setText(rooms.getDescriptionZH());
                    txt_booking_status.setText(booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusZH());
                    if (rooms.getBreakFast() == 0){
                        txt_ex_status_breakFast_id.setText("没有早餐");
                    }else {
                        txt_ex_status_breakFast_id.setText("早餐");
                    }
                    break;
            }

            if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 1){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_booked));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_booked));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 2){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_wait));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_wait));
            }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 3){
                img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_refuse));
                txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_refuse));
            }else {

            }


        }
    }
}
