package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.ipanda.lanexangtourism.Interface_click.ScreenBookingClickListener;
import com.ipanda.lanexangtourism.Model.ScreenBookingModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BookingViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<ScreenBookingModel> mListScreen;
    private ScreenBookingClickListener listener;

    public BookingViewPagerAdapter(Context mContext, ArrayList<ScreenBookingModel> mListScreen, ScreenBookingClickListener listener) {
        this.mContext = mContext;
        this.mListScreen = mListScreen;
        this.listener = listener;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final ScreenBookingModel screen = (ScreenBookingModel) mListScreen.get(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = inflater.inflate(R.layout.layout_item_booking_home_view,null);

        ImageView imgSlide = layoutScreen.findViewById(R.id.view_booking_img);
        String paths = mContext.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/"+screen.getScreenImage();
        Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(imgSlide);

        layoutScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClickedScreenBooking(screen);
            }
        });

        container.addView(layoutScreen);
        return layoutScreen;
    }

    @Override
    public int getCount() {
        return mListScreen.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
