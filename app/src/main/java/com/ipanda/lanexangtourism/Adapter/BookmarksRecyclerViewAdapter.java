package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.BookmarksClickListener;
import com.ipanda.lanexangtourism.Model.BookMarksModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BookmarksRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<BookMarksModel> arrayList;
    private String language;
    private BookmarksClickListener listener;

    public BookmarksRecyclerViewAdapter(Context context, ArrayList<BookMarksModel> arrayList, String language,BookmarksClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_bookmarks_view, parent, false);
        return new BookMarksViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final BookMarksModel bookMarks = (BookMarksModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        ((BookMarksViewHolder)holder).txt_bookmark_count.setText(bookMarks.getCountUserBookMarks()+"");

        for (ItemsModel items : bookMarks.getItemsModelArrayList()) {
            Picasso.get().load(paths+items.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((BookMarksViewHolder)holder).img_cover_items);
            ((BookMarksViewHolder)holder).txt_items_open_close.setText(items.getTimeOpen()+"-"+items.getTimeClose());
            ((BookMarksViewHolder)holder).txt_items_rating.setText(items.getRatingStarModel().getAverageReview()+"");
            ((BookMarksViewHolder)holder).con_bookmark_interested.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.itemClickedInterested(bookMarks);
                }
            });
            ((BookMarksViewHolder)holder).img_items_bookmarks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.itemClickedBookmarks(bookMarks);
                }
            });
            ((BookMarksViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.itemClickedItems(items);
                }
            });
            ((BookMarksViewHolder)holder).txt_items_kg.setText(items.getDistance()+"");
            switch (language){
                case "th":
                    if (items.getTopicTH().length() > 20){
                        String text = items.getTopicTH().substring(0,20)+"...";
                        ((BookMarksViewHolder)holder).txt_items_topic.setText(text);
                    }else {
                        ((BookMarksViewHolder)holder).txt_items_topic.setText(items.getTopicTH());
                    }
                    ((BookMarksViewHolder)holder).txt_items_contact.setText(items.getContactTH());
                    ((BookMarksViewHolder)holder).txt_items_subcategory.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                    break;
                case "en":
                    if (items.getTopicEN().length() > 20){
                        String text = items.getTopicEN().substring(0,20)+"...";
                        ((BookMarksViewHolder)holder).txt_items_topic.setText(text);
                    }else {
                        ((BookMarksViewHolder)holder).txt_items_topic.setText(items.getTopicEN());
                    }
                    ((BookMarksViewHolder)holder).txt_items_contact.setText(items.getContactEN());
                    ((BookMarksViewHolder)holder).txt_items_subcategory.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                    break;
                case "lo":
                    if (items.getTopicLO().length() > 20){
                        String text = items.getTopicLO().substring(0,20)+"...";
                        ((BookMarksViewHolder)holder).txt_items_topic.setText(text);
                    }else {
                        ((BookMarksViewHolder)holder).txt_items_topic.setText(items.getTopicLO());
                    }
                    ((BookMarksViewHolder)holder).txt_items_contact.setText(items.getContactLO());
                    ((BookMarksViewHolder)holder).txt_items_subcategory.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                    break;
                case "zh":
                    if (items.getTopicZH().length() > 20){
                        String text = items.getTopicZH().substring(0,20)+"...";
                        ((BookMarksViewHolder)holder).txt_items_topic.setText(text);
                    }else {
                        ((BookMarksViewHolder)holder).txt_items_topic.setText(items.getTopicZH());
                    }
                    ((BookMarksViewHolder)holder).txt_items_contact.setText(items.getContactZH());
                    ((BookMarksViewHolder)holder).txt_items_subcategory.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                    break;
            }
            switch (items.getMenuItemModel().getMenuItemId()){
                case 2:
                    ((BookMarksViewHolder)holder).rl_bg_star.setBackground(context.getResources().getDrawable(R.drawable.shape_star_tour));
                    break;
                case 3:
                    ((BookMarksViewHolder)holder).rl_bg_star.setBackground(context.getResources().getDrawable(R.drawable.shape_star_restaurant));
                    break;
                case 4:
                    ((BookMarksViewHolder)holder).rl_bg_star.setBackground(context.getResources().getDrawable(R.drawable.shape_star_shopping));
                    break;
                case 5:
                    ((BookMarksViewHolder)holder).rl_bg_star.setBackground(context.getResources().getDrawable(R.drawable.shape_star_hotel));
                    break;
                case 6:
                    ((BookMarksViewHolder)holder).rl_bg_star.setBackground(context.getResources().getDrawable(R.drawable.shape_star_contact));
                    break;
            }


        }

        if (bookMarks.isBookmarksState()) {
            ((BookMarksViewHolder) holder).img_items_bookmarks.setImageResource(R.drawable.ic_bookmark_red);
        }else {
            ((BookMarksViewHolder) holder).img_items_bookmarks.setImageResource(R.drawable.ic_bookmark_inactive);
        }




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class BookMarksViewHolder extends RecyclerView.ViewHolder{
        private ConstraintLayout con_bookmark_interested;
        private ImageView img_cover_items, img_items_bookmarks;
        private TextView txt_items_topic;
        private TextView txt_bookmark_count, txt_items_contact, txt_status_open, txt_items_subcategory, txt_items_rating,txt_items_open_close,txt_items_kg;
        private RelativeLayout rl_bg_star;

        public BookMarksViewHolder(@NonNull View itemView) {
            super(itemView);
            con_bookmark_interested = itemView.findViewById(R.id.con_bookmark_interested);
            img_cover_items = itemView.findViewById(R.id.img_cover_items);
            img_items_bookmarks = itemView.findViewById(R.id.img_items_bookmarks);
            txt_items_topic = itemView.findViewById(R.id.txt_items_topic);
            txt_bookmark_count = itemView.findViewById(R.id.txt_bookmark_count);
            txt_items_contact = itemView.findViewById(R.id.txt_items_contact);
            txt_status_open = itemView.findViewById(R.id.txt_status_open);
            txt_items_subcategory = itemView.findViewById(R.id.txt_items_subcategory);
            txt_items_rating = itemView.findViewById(R.id.txt_items_rating);
            txt_items_open_close = itemView.findViewById(R.id.txt_items_open_close);
            txt_items_kg = itemView.findViewById(R.id.txt_items_kg);
            rl_bg_star = itemView.findViewById(R.id.rl_bg_star);


        }

    }
}
