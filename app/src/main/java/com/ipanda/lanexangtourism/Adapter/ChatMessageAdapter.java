package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.BookingItemsClickListener;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ChatMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<MessageModel> arrayList;
    private String language;
    private static int TYPE_SEND = 1;
    private static int TYPE_REPLY = 2;
    private int userId;
    private String chatBy;

    public ChatMessageAdapter(Context context, ArrayList<MessageModel> arrayList, String language, int userId, String chatBy) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.userId = userId;
        this.chatBy = chatBy;
    }

    @Override
    public int getItemViewType(int position) {
        if (arrayList.get(position).getUserModel().getUserId() ==  userId) {
            return TYPE_SEND;
        } else {
            return TYPE_REPLY;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view;
        if (viewType == TYPE_SEND) {
            view = LayoutInflater.from(context).inflate(R.layout.item_chat_send, viewGroup, false);
            return new SendMessageViewHolder(view);
        } else{
            view = LayoutInflater.from(context).inflate(R.layout.item_chat_reply, viewGroup, false);
            return new ReplyMessageViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final MessageModel message = (MessageModel) arrayList.get(position);
        if (getItemViewType(position) == TYPE_SEND) {
            ((SendMessageViewHolder)holder).setCallSendMessage(message);
            ((SendMessageViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }else {
            ((ReplyMessageViewHolder)holder).setCallReplyMessage(message);
            ((ReplyMessageViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class SendMessageViewHolder extends RecyclerView.ViewHolder{

        private ImageView image_profile;
        private TextView txt_name_profile;
        private TextView txt_chat_message;

        public SendMessageViewHolder(@NonNull View itemView) {
            super(itemView);
            image_profile = itemView.findViewById(R.id.image_profile);
            txt_name_profile = itemView.findViewById(R.id.txt_name_profile);
            txt_chat_message = itemView.findViewById(R.id.txt_chat_message);
        }

        private void setCallSendMessage(MessageModel message) {
            if (chatBy.equals("Person")) {
                if (message.getUserModel().getUserRoleModel().getUserRoleId() == 5) {
                    String pathsOnline = message.getUserModel().getProfilePicUrl();
                    Picasso.get().load(pathsOnline).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(image_profile);
                } else {
                    String paths = context.getString(R.string.app_api_ip) + "dasta_thailand/assets/img/uploadfile/" + message.getUserModel().getProfilePicUrl();
                    Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(image_profile);
                }
                txt_name_profile.setText(message.getUserModel().getFirstName() + " " + message.getUserModel().getLastName());
                txt_chat_message.setText(message.getMessage());
            }else {
                if (message.getUserModel().getUserRoleModel().getUserRoleId() == 5) {
                    String pathsOnline = message.getUserModel().getProfilePicUrl();
                    Picasso.get().load(pathsOnline).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(image_profile);
                } else {
                    String paths = context.getString(R.string.app_api_ip) + "dasta_thailand/assets/img/uploadfile/" + message.getUserModel().getProfilePicUrl();
                    Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(image_profile);
                }
                txt_name_profile.setText(message.getUserModel().getFirstName() + " " + message.getUserModel().getLastName());
                txt_chat_message.setText(message.getMessage());
            }
        }

    }



    public class ReplyMessageViewHolder extends RecyclerView.ViewHolder {

        private ImageView image_profile;
        private TextView txt_name_profile;
        private TextView txt_chat_message;

        public ReplyMessageViewHolder(@NonNull View itemView) {
            super(itemView);
            image_profile = itemView.findViewById(R.id.image_profile);
            txt_name_profile = itemView.findViewById(R.id.txt_name_profile);
            txt_chat_message = itemView.findViewById(R.id.txt_chat_message);
        }

        private void setCallReplyMessage(MessageModel message) {
            if (chatBy.equals("Person")) {
                if (message.getUserModel().getUserRoleModel().getUserRoleId() == 5) {
                    String pathsOnline = message.getUserModel().getProfilePicUrl();
                    Picasso.get().load(pathsOnline).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(image_profile);
                } else {
                    String paths = context.getString(R.string.app_api_ip) + "dasta_thailand/assets/img/uploadfile/" + message.getUserModel().getProfilePicUrl();
                    Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(image_profile);
                }
                txt_name_profile.setText(message.getUserModel().getFirstName() + " " + message.getUserModel().getLastName());
                txt_chat_message.setText(message.getMessage());
            }else {
                if (message.getUserModel().getUserRoleModel().getUserRoleId() == 5) {
                    String pathsOnline = message.getUserModel().getProfilePicUrl();
                    Picasso.get().load(pathsOnline).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(image_profile);
                } else {
                    String paths = context.getString(R.string.app_api_ip) + "dasta_thailand/assets/img/uploadfile/" + chatBy;
                    Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(image_profile);
                }
                txt_name_profile.setText(message.getUserModel().getFirstName() + " " + message.getUserModel().getLastName());
                txt_chat_message.setText(message.getMessage());
            }
        }
    }
}
