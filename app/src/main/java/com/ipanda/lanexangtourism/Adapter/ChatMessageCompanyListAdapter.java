package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ChatMessageClickListener;
import com.ipanda.lanexangtourism.Interface_click.ChatMessageCompanyClickListener;
import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ChatMessageCompanyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<MessageModel> arrayList;
    private String language;
    private String userId;
    private ChatMessageCompanyClickListener listener;

    public ChatMessageCompanyListAdapter(Context context, ArrayList<MessageModel> arrayList, String language, String userId, ChatMessageCompanyClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
        this.userId = userId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_notification_view, parent, false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final MessageModel message = (MessageModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        String pathsOnline = "";

        ((MessageViewHolder)holder).txt_chat_message.setText(message.getMessage());

        String[] arrOfStr = message.getCreatedAt().split(" ");

        ((MessageViewHolder)holder).txt_chat_times.setText(arrOfStr[1]);

/*        if (message.getUserModel().getUserId() != Integer.parseInt(userId)){
            ((MessageViewHolder)holder).txt_name_profile.setText(message.getUserModel().getFirstName()+" "+message.getUserModel().getLastName());
            pathsOnline = message.getUserModel().getProfilePicUrl();
        }

        if (message.getUserSend().getUserId() != Integer.parseInt(userId)){
            ((MessageViewHolder)holder).txt_name_profile.setText(message.getUserSend().getFirstName()+" "+message.getUserSend().getLastName());
            pathsOnline = message.getUserSend().getProfilePicUrl();
        }*/

        ((MessageViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickChatMessage(message,((MessageViewHolder)holder).notification);
            }
        });

        if (message.isRead()){
            ((MessageViewHolder)holder).notification.setVisibility(View.VISIBLE);
            if (message.getCountMessage() > 0){
                ((MessageViewHolder)holder).notification.setVisibility(View.VISIBLE);
                ((MessageViewHolder)holder).txt_count_message.setText(message.getCountMessage()+"");
            }else {
                ((MessageViewHolder)holder).notification.setVisibility(View.GONE);
            }
        }else {
            ((MessageViewHolder)holder).notification.setVisibility(View.GONE);
        }

        Picasso.get().load(paths+message.getItemsModel().getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((MessageViewHolder)holder).image_profile);
        String companyName = "";
        String topicName = "";
        String type = "";
        switch (language){
            case "th":
                companyName = message.getBusinessModel().getNameTH();
                topicName = message.getItemsModel().getTopicTH();
                type = message.getBusinessModel().getMenuItemModel().getMenuItemTH();
                break;
            case "en":
                companyName = message.getBusinessModel().getNameEN();
                topicName = message.getItemsModel().getTopicEN();
                type = message.getBusinessModel().getMenuItemModel().getMenuItemEN();
                break;
            case "lo":
                companyName = message.getBusinessModel().getNameLO();
                topicName = message.getItemsModel().getTopicLO();
                type = message.getBusinessModel().getMenuItemModel().getMenuItemLO();
                break;
            case "zh":
                companyName = message.getBusinessModel().getNameZH();
                topicName = message.getItemsModel().getTopicZH();
                type = message.getBusinessModel().getMenuItemModel().getMenuItemZH();
                break;
        }

        ((MessageViewHolder)holder).txt_topic_item.setText(topicName);
        ((MessageViewHolder)holder).txt_company_name.setText(companyName);
        ((MessageViewHolder)holder).txt_type_item.setText(type);



    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class MessageViewHolder extends RecyclerView.ViewHolder{

        private ImageView image_profile;
        private TextView txt_topic_item;
        private TextView txt_company_name;
        private TextView txt_type_item;
        private TextView txt_chat_message;
        private TextView txt_chat_times;
        private TextView txt_count_message;
        private RelativeLayout notification;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            image_profile = itemView.findViewById(R.id.image_profile);
            txt_topic_item = itemView.findViewById(R.id.txt_topic_item);
            txt_company_name = itemView.findViewById(R.id.txt_company_name);
            txt_type_item = itemView.findViewById(R.id.txt_type_item);
            txt_chat_message = itemView.findViewById(R.id.txt_chat_message);
            txt_chat_times = itemView.findViewById(R.id.txt_chat_times);
            txt_count_message = itemView.findViewById(R.id.txt_count_message);
            notification = itemView.findViewById(R.id.notification);
        }


    }
}
