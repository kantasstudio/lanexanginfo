package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ChatMessageClickListener;
import com.ipanda.lanexangtourism.Model.BookMarksModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ChatMessageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<MessageModel> arrayList;
    private String language;
    private String userId;
    private ChatMessageClickListener listener;

    public ChatMessageListAdapter(Context context, ArrayList<MessageModel> arrayList, String language, String userId, ChatMessageClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
        this.userId = userId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_message_view, parent, false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final MessageModel message = (MessageModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        String pathsOnline = "";

        ((MessageViewHolder)holder).txt_chat_message.setText(message.getMessage());

        String[] arrOfStr = message.getCreatedAt().split(" ");

        ((MessageViewHolder)holder).txt_chat_times.setText(arrOfStr[1]);

        if (message.getUserModel().getUserId() != Integer.parseInt(userId)){
            ((MessageViewHolder)holder).txt_name_profile.setText(message.getUserModel().getFirstName()+" "+message.getUserModel().getLastName());
            pathsOnline = message.getUserModel().getProfilePicUrl();
        }

        if (message.getUserSend().getUserId() != Integer.parseInt(userId)){
            ((MessageViewHolder)holder).txt_name_profile.setText(message.getUserSend().getFirstName()+" "+message.getUserSend().getLastName());
            pathsOnline = message.getUserSend().getProfilePicUrl();
        }

        Picasso.get().load(pathsOnline).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((MessageViewHolder)holder).image_profile);

        ((MessageViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickChatMessage(message,((MessageViewHolder)holder).notification);
            }
        });

        if (message.isRead()){
            ((MessageViewHolder)holder).notification.setVisibility(View.VISIBLE);
            if (message.getCountMessage() > 0){
                ((MessageViewHolder)holder).notification.setVisibility(View.VISIBLE);
                ((MessageViewHolder)holder).count_message.setText(message.getCountMessage()+"");
            }else {
                ((MessageViewHolder)holder).notification.setVisibility(View.GONE);
            }
        }else {
            ((MessageViewHolder)holder).notification.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class MessageViewHolder extends RecyclerView.ViewHolder{

        private ImageView image_profile;
        private TextView txt_name_profile;
        private TextView txt_chat_message;
        private TextView txt_chat_times;
        private TextView count_message;
        private RelativeLayout notification;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            image_profile = itemView.findViewById(R.id.image_profile);
            txt_name_profile = itemView.findViewById(R.id.txt_name_profile);
            txt_chat_message = itemView.findViewById(R.id.txt_chat_message);
            txt_chat_times = itemView.findViewById(R.id.txt_chat_times);
            count_message = itemView.findViewById(R.id.count_message);
            notification = itemView.findViewById(R.id.notification);
        }


    }
}
