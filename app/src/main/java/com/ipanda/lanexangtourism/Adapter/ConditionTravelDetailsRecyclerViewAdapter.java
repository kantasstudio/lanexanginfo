package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class ConditionTravelDetailsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<BookingConditionModel> arrayList;
    private String language;

    public ConditionTravelDetailsRecyclerViewAdapter(Context context, ArrayList<BookingConditionModel> arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_package_tour_terms_conditions_child, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final BookingConditionModel itemsModel = (BookingConditionModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_title_conditions.setText(itemsModel.getConditionTopicTH());
                ((ItemViewHolder)holder).txt_terms_conditions.setText(itemsModel.getConditionDetailTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_title_conditions.setText(itemsModel.getConditionTopicEN());
                ((ItemViewHolder)holder).txt_terms_conditions.setText(itemsModel.getConditionDetailEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_title_conditions.setText(itemsModel.getConditionTopicLO());
                ((ItemViewHolder)holder).txt_terms_conditions.setText(itemsModel.getConditionDetailLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_title_conditions.setText(itemsModel.getConditionTopicZH());
                ((ItemViewHolder)holder).txt_terms_conditions.setText(itemsModel.getConditionDetailZH());
                break;
        }

        ((ItemViewHolder)holder).con_terms_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ItemViewHolder)holder).expandable_terms_conditions.toggle();
                if (((ItemViewHolder)holder).expandable_terms_conditions.isExpanded()){
                    ((ItemViewHolder)holder).img_arrow_event.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    ((ItemViewHolder)holder).img_arrow_event.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private ImageView img_arrow_event;
        private ConstraintLayout con_terms_conditions;
        private TextView txt_title_conditions, txt_terms_conditions;
        private ExpandableLayout expandable_terms_conditions;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            img_arrow_event = itemView.findViewById(R.id.img_arrow_event);
            con_terms_conditions = itemView.findViewById(R.id.con_terms_conditions);
            txt_title_conditions = itemView.findViewById(R.id.txt_title_conditions);
            txt_terms_conditions = itemView.findViewById(R.id.txt_terms_conditions);
            expandable_terms_conditions = itemView.findViewById(R.id.expandable_terms_conditions);
        }

    }
}
