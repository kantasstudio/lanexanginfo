package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.CountryModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class CountySpinnerAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<CountryModel> arrayList;


    public CountySpinnerAdapter(Context context, ArrayList<CountryModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CountryModel countryModel = arrayList.get(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.custom_layout_item_spinner, parent, false);


        TextView title = convertView.findViewById(R.id.txt_title_spinner);

        ChangeLanguageLocale changeLanguageLocale = new ChangeLanguageLocale(context);

        switch (changeLanguageLocale.getLanguage()){
            case "th":
                title.setText(countryModel.getCountryTH());
                break;
            case "en":
                title.setText(countryModel.getCountryEN());
                break;
            case "lo":
                title.setText(countryModel.getCountryLO());
                break;
            case "zh":
                title.setText(countryModel.getCountryZH());
                break;
        }



        return convertView;
    }
}
