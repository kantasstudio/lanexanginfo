package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class DatesTripAdapterRecyclerView extends RecyclerView.Adapter<DatesTripAdapterRecyclerView.ItemViewHolder> {

    private Context context;
    private ArrayList<DatesTripModel> modelDatesTripArrayList;
    private FragmentManager supportFragmentManager;
    private String language;

    public DatesTripAdapterRecyclerView(Context context, ArrayList<DatesTripModel> modelDatesTripArrayList, FragmentManager supportFragmentManager, String language) {
        this.context = context;
        this.modelDatesTripArrayList = modelDatesTripArrayList;
        this.supportFragmentManager = supportFragmentManager;
        this.language = language;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_programtour_day_view, parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        final DatesTripModel tripModel = (DatesTripModel) modelDatesTripArrayList.get(position);

        switch (language) {
            case "th":
                ((ItemViewHolder)holder).txt_tour_title_day.setText(tripModel.getDatesTripTopicThai());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_tour_title_day.setText(tripModel.getDatesTripTopicEnglish());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_tour_title_day.setText(tripModel.getDatesTripTopicLaos());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_tour_title_day.setText(tripModel.getDatesTripTopicChinese());
                break;
        }

        DatesTripItemsAdapterRecyclerView adapterRecyclerView = new DatesTripItemsAdapterRecyclerView(context,tripModel.getTouristAttractionsModelArrayList(),supportFragmentManager,language);
        ((ItemViewHolder)holder).recycler_dates_items.setAdapter(adapterRecyclerView);

    }


    @Override
    public int getItemCount() {
        return modelDatesTripArrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView txt_tour_title_day;
        RecyclerView recycler_dates_items;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_tour_title_day = itemView.findViewById(R.id.txt_tour_title_day);
            recycler_dates_items = itemView.findViewById(R.id.recycler_dates_items);
            recycler_dates_items.setLayoutManager( new LinearLayoutManager(context));
        }
    }
}
