package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.DialogFragment.SlidingImageTourFragment;
import com.ipanda.lanexangtourism.Interface_click.PhotoTourClickListener;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.Model.TouristAttractionsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.items_view.ViewTourismActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DatesTripItemsAdapterRecyclerView extends RecyclerView.Adapter<DatesTripItemsAdapterRecyclerView.ItemViewHolder> implements PhotoTourClickListener  {

    private Context context;
    private ArrayList<TouristAttractionsModel> touristAttractionsModelArrayList;
    private FragmentManager supportFragmentManager;
    private String language;

    public DatesTripItemsAdapterRecyclerView(Context context, ArrayList<TouristAttractionsModel> touristAttractionsModelArrayList, FragmentManager supportFragmentManager, String language) {
        this.context = context;
        this.touristAttractionsModelArrayList = touristAttractionsModelArrayList;
        this.supportFragmentManager = supportFragmentManager;
        this.language = language;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_programtour_day_item_view, parent,false);
        return new ItemViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        final TouristAttractionsModel attractionsModel = (TouristAttractionsModel) touristAttractionsModelArrayList.get(position);

        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        if (attractionsModel.getItemsModel().getCoverItemsModel() != null) {
            Picasso.get().load(paths + attractionsModel.getItemsModel().getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder)
                    .error(R.drawable.img_laceholder_error).into(((ItemViewHolder) holder).img_item_cover);
        }
        switch (language) {
            case "th":
                ((ItemViewHolder)holder).txt_title_items.setText(attractionsModel.getItemsModel().getTopicTH());
                ((ItemViewHolder)holder).txt_items_contact.setText(attractionsModel.getItemsModel().getContactTH());
                ((ItemViewHolder)holder).txt_day_open_close.setText("ทุกวัน");
                break;
            case "en":
                ((ItemViewHolder)holder).txt_title_items.setText(attractionsModel.getItemsModel().getTopicEN());
                ((ItemViewHolder)holder).txt_items_contact.setText(attractionsModel.getItemsModel().getContactEN());
                ((ItemViewHolder)holder).txt_day_open_close.setText("Every day");
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_title_items.setText(attractionsModel.getItemsModel().getTopicLO());
                ((ItemViewHolder)holder).txt_items_contact.setText(attractionsModel.getItemsModel().getContactLO());
                ((ItemViewHolder)holder).txt_day_open_close.setText("ທຸກວັນ");
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_title_items.setText(attractionsModel.getItemsModel().getTopicZH());
                ((ItemViewHolder)holder).txt_items_contact.setText(attractionsModel.getItemsModel().getContactZH());
                ((ItemViewHolder)holder).txt_day_open_close.setText("每天");
                break;
        }

        ((ItemViewHolder)holder).txt_time_open_close.setText(attractionsModel.getItemsModel().getTimeOpen()+"-"+attractionsModel.getItemsModel().getTimeClose());
        ((ItemViewHolder)holder).txt_phone_items.setText(attractionsModel.getItemsModel().getPhone());

        PhotoTouristAdapterRecyclerView photoTouristAdapter = new PhotoTouristAdapterRecyclerView(context,attractionsModel.getPhotoTouristArrayList(),this::itemClicked,language);
        ((ItemViewHolder)holder).recycler_photo_tourist.setAdapter(photoTouristAdapter);

        ((ItemViewHolder)holder).con_items_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewTourismActivity.class);
                intent.putExtra("ITEMS_MODEL",attractionsModel.getItemsModel());
                intent.putExtra("CATEGORY",attractionsModel.getItemsModel().getCategoryModel().getCategoryId());
                context.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return touristAttractionsModelArrayList.size();
    }

    @Override
    public void itemClicked(int position, ArrayList<PhotoTourist> photoTouristArrayList) {
        Bundle bundle = new Bundle();
        DialogFragment fragment = SlidingImageTourFragment.newInstance();
        bundle.putParcelableArrayList("ItemsPhotoDetailModels",photoTouristArrayList);
        bundle.putInt("SelectedImage",position);
        bundle.putString("language",language);
        fragment.setArguments(bundle);
        fragment.show(supportFragmentManager,"DatesTripItemsAdapterRecyclerView");
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView txt_tour_title_day;
        ImageView img_item_cover;
        TextView txt_title_items, txt_items_contact, txt_day_open_close, txt_time_open_close, txt_phone_items;
        RecyclerView recycler_photo_tourist;
        ConstraintLayout con_items_view;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_tour_title_day = itemView.findViewById(R.id.txt_tour_title_day);
            img_item_cover = itemView.findViewById(R.id.img_item_cover);
            txt_title_items = itemView.findViewById(R.id.txt_title_items);
            txt_items_contact = itemView.findViewById(R.id.txt_items_contact);
            txt_day_open_close = itemView.findViewById(R.id.txt_day_open_close);
            txt_time_open_close = itemView.findViewById(R.id.txt_time_open_close);
            txt_phone_items = itemView.findViewById(R.id.txt_phone_items);
            con_items_view = itemView.findViewById(R.id.con_items_view);
            recycler_photo_tourist = itemView.findViewById(R.id.recycler_photo_tourist);
            recycler_photo_tourist.setLayoutManager( new LinearLayoutManager(context));
        }
    }
}
