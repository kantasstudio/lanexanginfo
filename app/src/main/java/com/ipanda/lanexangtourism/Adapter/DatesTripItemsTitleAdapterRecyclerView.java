package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.TouristAttractionsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class DatesTripItemsTitleAdapterRecyclerView extends RecyclerView.Adapter<DatesTripItemsTitleAdapterRecyclerView.ItemViewHolder> {

    private Context context;
    private ArrayList<TouristAttractionsModel> touristAttractionsModelArrayList;
    private String language;

    public DatesTripItemsTitleAdapterRecyclerView(Context context, ArrayList<TouristAttractionsModel> touristAttractionsModelArrayList, String language) {
        this.context = context;
        this.touristAttractionsModelArrayList = touristAttractionsModelArrayList;
        this.language = language;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_title_program_tour_view, parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        final TouristAttractionsModel attractionsModel = (TouristAttractionsModel) touristAttractionsModelArrayList.get(position);

        switch (language) {
            case "th":
                ((ItemViewHolder)holder).txt_items_title.setText(attractionsModel.getItemsModel().getTopicTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_items_title.setText(attractionsModel.getItemsModel().getTopicEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_items_title.setText(attractionsModel.getItemsModel().getTopicLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_items_title.setText(attractionsModel.getItemsModel().getTopicZH());
                break;
        }


    }


    @Override
    public int getItemCount() {
        return touristAttractionsModelArrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView txt_items_title;
        ImageView img_location_mark;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_items_title = itemView.findViewById(R.id.txt_items_title);
            img_location_mark = itemView.findViewById(R.id.img_location_mark);
        }
    }
}
