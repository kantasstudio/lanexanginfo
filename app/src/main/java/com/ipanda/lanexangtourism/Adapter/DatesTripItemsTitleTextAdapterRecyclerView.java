package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class DatesTripItemsTitleTextAdapterRecyclerView extends RecyclerView.Adapter<DatesTripItemsTitleTextAdapterRecyclerView.ItemViewHolder> {

    private Context context;
    private ArrayList<DatesTripModel> modelDatesTripArrayList;
    private String language;

    public DatesTripItemsTitleTextAdapterRecyclerView(Context context, ArrayList<DatesTripModel> modelDatesTripArrayList, String language) {
        this.context = context;
        this.modelDatesTripArrayList = modelDatesTripArrayList;
        this.language = language;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_title_dates_trip_view, parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        final DatesTripModel tripModel = (DatesTripModel) modelDatesTripArrayList.get(position);

        switch (language) {
            case "th":
                ((ItemViewHolder)holder).txt_items_topic.setText(tripModel.getDatesTripTopicThai()+" : ");
                break;
            case "en":
                ((ItemViewHolder)holder).txt_items_topic.setText(tripModel.getDatesTripTopicEnglish()+" : ");
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_items_topic.setText(tripModel.getDatesTripTopicLaos()+" : ");
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_items_topic.setText(tripModel.getDatesTripTopicChinese()+" : ");
                break;
        }

        DatesTripItemsTitleAdapterRecyclerView adapter = new DatesTripItemsTitleAdapterRecyclerView(context,tripModel.getTouristAttractionsModelArrayList(),language);
        ((ItemViewHolder)holder).recycler_title_items.setAdapter(adapter);

    }


    @Override
    public int getItemCount() {
        return modelDatesTripArrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView txt_items_topic;
        RecyclerView recycler_title_items;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_items_topic = itemView.findViewById(R.id.txt_items_topic);
            recycler_title_items = itemView.findViewById(R.id.recycler_title_items);
            recycler_title_items.setLayoutManager( new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        }
    }
}
