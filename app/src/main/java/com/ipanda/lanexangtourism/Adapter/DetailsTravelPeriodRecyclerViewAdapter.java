package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.BookingDetailsClickListener;
import com.ipanda.lanexangtourism.Model.TravelPeriodModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class DetailsTravelPeriodRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<TravelPeriodModel> arrayList;
    private String language;
    private BookingDetailsClickListener clickListener;
    public int mSelectedItem = -1;

    public DetailsTravelPeriodRecyclerViewAdapter(Context context, ArrayList<TravelPeriodModel> arrayList, String language,BookingDetailsClickListener clickListener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_booking_detail_time_schedule_child, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final TravelPeriodModel itemsModel = (TravelPeriodModel) arrayList.get(position);

        String[] subCheckStart = itemsModel.getPeriodStart().split("-");
        String[] subCheckEnd = itemsModel.getPeriodEnd().split("-");
        String scsDate = subCheckStart[2],scsMonth = subCheckStart[1],scsYear = subCheckStart[0];
        String sceDate = subCheckEnd[2],sceMonth = subCheckEnd[1],sceYear = subCheckEnd[0];
        String start = scsDate+"-"+scsMonth+"-"+scsYear, end = sceDate+"-"+sceMonth+"-"+sceYear;

        ((ItemViewHolder)holder).txt_round_trip.setText(start+" - "+end);
        ((ItemViewHolder)holder).txt_all_seats.setText(itemsModel.getAmount()+"");
        ((ItemViewHolder)holder).txt_space.setText(itemsModel.getAmount()+"");


        ((ItemViewHolder)holder).check_booking_detail.setChecked(position == mSelectedItem);
        ((ItemViewHolder)holder).check_booking_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedItem = position;
                clickListener.itemClicked(start,end,position);
                notifyDataSetChanged();
            }
        });



       

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private boolean bookingCheck = false;
        private CheckBox check_booking_detail, check_booking_detail_full;
        private TextView txt_round_trip, txt_all_seats, txt_space;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            check_booking_detail = itemView.findViewById(R.id.check_booking_detail);
            check_booking_detail_full = itemView.findViewById(R.id.check_booking_detail_full);
            txt_round_trip = itemView.findViewById(R.id.txt_round_trip);
            txt_all_seats = itemView.findViewById(R.id.txt_all_seats);
            txt_space = itemView.findViewById(R.id.txt_space);
        }

    }
}
