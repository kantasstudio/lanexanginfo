package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedDurationClickListener;
import com.ipanda.lanexangtourism.Model.DurationPackageTourModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class DurationPackageTourAdapter extends RecyclerView.Adapter<DurationPackageTourAdapter.FilterViewHolder>{

    private Context context;
    private ArrayList<DurationPackageTourModel> arrayList;
    private String language;
    private FilterSelectedDurationClickListener listener;


    public DurationPackageTourAdapter(Context context, ArrayList<DurationPackageTourModel> arrayList, String language, FilterSelectedDurationClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
    }

    @NonNull
    @Override
    public FilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_title_view,parent,false);
        return new FilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterViewHolder holder, int position) {
        final DurationPackageTourModel duration = (DurationPackageTourModel) arrayList.get(position);

        switch (language){
            case "th":
                holder.title.setText(duration.getTextThai());
                break;
            case "en":
                holder.title.setText(duration.getTextEnglish());
                break;
            case "lo":
                holder.title.setText(duration.getTextLaos());
                break;
            case "zh":
                holder.title.setText(duration.getTextChinese());
                break;
        }

        if(holder.isSelected){
            holder.img.setVisibility(View.VISIBLE);
        }else {
            holder.img.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.isSelected){
                    duration.setFilter(false);
                    holder.isSelected = false;
                    holder.img.setVisibility(View.GONE);
                }else {
                    holder.isSelected = true;
                    duration.setFilter(true);
                    holder.img.setVisibility(View.VISIBLE);
                }
                if (listener != null) {
                    listener.getFilterSelectedDuration(arrayList);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class FilterViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        ImageView img;
        boolean isSelected = false;
        public FilterViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.filter_title);
            img = itemView.findViewById(R.id.filter_image);
        }
    }
}
