package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.ClickListener.ItemImageClickListener;
import com.ipanda.lanexangtourism.Model.FacilitiesModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class FacilitiesRecyclerViewAdapter extends RecyclerView.Adapter<FacilitiesRecyclerViewAdapter.FacilitiesViewHolder>{

    private Context context;
    private ArrayList<FacilitiesModel> arrayList;
    private ItemImageClickListener listener;
    private String language;

    public FacilitiesRecyclerViewAdapter(Context context, ArrayList<FacilitiesModel> arrayList, ItemImageClickListener listener, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
    }

    @NonNull
    @Override
    public FacilitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_description_facilities_view, parent,false);
        return new FacilitiesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FacilitiesViewHolder holder, int position) {
        final FacilitiesModel itemsModel = (FacilitiesModel) arrayList.get(position);
        switch (language){
            case "th":
                ((FacilitiesViewHolder)holder).txt_facilities_id.setText(itemsModel.getFacilitiesTH());
                break;
            case "en":
                ((FacilitiesViewHolder)holder).txt_facilities_id.setText(itemsModel.getFacilitiesEN());
                break;
            case "lo":
                ((FacilitiesViewHolder)holder).txt_facilities_id.setText(itemsModel.getFacilitiesLO());
                break;
            case "zh":
                ((FacilitiesViewHolder)holder).txt_facilities_id.setText(itemsModel.getFacilitiesZH());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class FacilitiesViewHolder extends RecyclerView.ViewHolder{
        TextView txt_facilities_id;
        public FacilitiesViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_facilities_id = itemView.findViewById(R.id.txt_facilities_id);
        }
    }


}
