package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.FilterItemsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class FilterItemsAdapterRecyclerView extends RecyclerView.Adapter<FilterItemsAdapterRecyclerView.FilterItemsViewHolder>{

    private Context context;
    private ArrayList<FilterItemsModel> itemsModelArrayList;

    public FilterItemsAdapterRecyclerView(Context context, ArrayList<FilterItemsModel> itemsModelArrayList) {
        this.context = context;
        this.itemsModelArrayList = itemsModelArrayList;
    }

    @NonNull
    @Override
    public FilterItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_title_view,parent,false);
        return new FilterItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterItemsViewHolder holder, int position) {
        final FilterItemsModel itemsModel = (FilterItemsModel) itemsModelArrayList.get(position);
        holder.title.setText(itemsModel.getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, itemsModel.getTitle()+"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsModelArrayList.size();
    }


    public class FilterItemsViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        ImageView img;
        public FilterItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.filter_title);
            img = itemView.findViewById(R.id.filter_image);
        }
    }
}
