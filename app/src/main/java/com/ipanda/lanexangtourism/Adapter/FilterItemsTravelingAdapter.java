package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.FilterSelectedTravelingClickListener;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class FilterItemsTravelingAdapter extends RecyclerView.Adapter<FilterItemsTravelingAdapter.FilterViewHolder>{

    private Context context;
    private ArrayList<SubCategoryModel> arrayList;
    private String language;
    private FilterSelectedTravelingClickListener listener;

    public FilterItemsTravelingAdapter(Context context, ArrayList<SubCategoryModel> arrayList, String language,FilterSelectedTravelingClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
    }

    @NonNull
    @Override
    public FilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_title_view,parent,false);
        return new FilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterViewHolder holder, int position) {
        final SubCategoryModel subCategory = (SubCategoryModel) arrayList.get(position);

        switch (language){
            case "th":
                holder.title.setText(subCategory.getCategoryTH());
                break;
            case "en":
                holder.title.setText(subCategory.getCategoryEN());
                break;
            case "lo":
                holder.title.setText(subCategory.getCategoryLO());
                break;
            case "zh":
                holder.title.setText(subCategory.getCategoryZH());
                break;
        }

        if(holder.isSelected){
            holder.img.setVisibility(View.VISIBLE);
        }else {
            holder.img.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.isSelected){
                    subCategory.setFilter(false);
                    holder.isSelected = false;
                    holder.img.setVisibility(View.GONE);
                }else {
                    holder.isSelected = true;
                    subCategory.setFilter(true);
                    holder.img.setVisibility(View.VISIBLE);
                }
                if (listener != null){
                    listener.getFilterSelectedTraveling(arrayList);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class FilterViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        ImageView img;
        boolean isSelected = false;
        public FilterViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.filter_title);
            img = itemView.findViewById(R.id.filter_image);
        }
    }
}
