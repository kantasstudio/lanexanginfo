package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FilterTypeOfPackagesAdapter extends RecyclerView.Adapter<FilterTypeOfPackagesAdapter.FilterViewHolder>{

    private Context context;
    private ArrayList<SubCategoryModel> arrayList;
    private String language;
    private FilterSelectedClickListener listener;

    public FilterTypeOfPackagesAdapter(Context context, ArrayList<SubCategoryModel> arrayList, String language, FilterSelectedClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
    }

    @NonNull
    @Override
    public FilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_types_of_packages,parent,false);
        return new FilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterViewHolder holder, int position) {
        final SubCategoryModel subCategory = (SubCategoryModel) arrayList.get(position);

        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/"+subCategory.getCategoryPath();
        Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((FilterViewHolder)holder).imgTypeOfPackages);
        switch (language){
            case "th":
                holder.txtTypeOfPackages.setText(subCategory.getCategoryTH());
                break;
            case "en":
                holder.txtTypeOfPackages.setText(subCategory.getCategoryEN());
                break;
            case "lo":
                holder.txtTypeOfPackages.setText(subCategory.getCategoryLO());
                break;
            case "zh":
                holder.txtTypeOfPackages.setText(subCategory.getCategoryZH());
                break;
        }

        if(holder.isSelected){
//            holder.img.setVisibility(View.VISIBLE);
        }else {
//            holder.img.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.isSelected){
                    subCategory.setFilter(false);
                    holder.isSelected = false;
//                    holder.img.setVisibility(View.GONE);
                }else {
                    holder.isSelected = true;
                    subCategory.setFilter(true);
//                    holder.img.setVisibility(View.VISIBLE);
                }
                if (listener != null) {
                    listener.getFilterSelectedTourist(arrayList);
                }
            }
        });

        holder.checkTypeOfPackages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.checkTypeOfPackages.isChecked()){
                    subCategory.setFilter(true);
                }else {
                    subCategory.setFilter(false);
                }
                if (listener != null) {
                    listener.getFilterSelectedTourist(arrayList);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class FilterViewHolder extends RecyclerView.ViewHolder{
        TextView txtTypeOfPackages;
        ImageView imgTypeOfPackages;
        CardView cardTypeOfPackages;
        CheckBox checkTypeOfPackages;
        boolean isSelected = false;
        public FilterViewHolder(@NonNull View itemView) {
            super(itemView);

            cardTypeOfPackages = itemView.findViewById(R.id.card_type_of_packages);
            imgTypeOfPackages = itemView.findViewById(R.id.img_type_of_packages);
            txtTypeOfPackages = itemView.findViewById(R.id.txt_type_of_packages);
            checkTypeOfPackages = itemView.findViewById(R.id.check_type_of_packages);
        }
    }
}
