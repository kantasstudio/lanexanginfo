package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.FollowClickListener;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FollowRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<UserModel> arrayList;
    private FollowClickListener listener;

    public FollowRecyclerViewAdapter(Context context, ArrayList<UserModel> arrayList ,FollowClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_personnel_view, parent, false);
        return new FollowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final UserModel user = (UserModel) arrayList.get(position);
        Picasso.get().load(user.getProfilePicUrl()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((FollowViewHolder)holder).img_follow_profile);

        ((FollowViewHolder)holder).txt_follow_name.setText(user.getFirstName() +" "+user.getLastName());
        ((FollowViewHolder)holder).txt_follow_login_with.setText(user.getLoginWith());


        if (user.isFollowState()){
            ((FollowViewHolder)holder).txt_follow_title.setText("ติดตามอยู่");
            ((FollowViewHolder)holder).txt_follow_title.setTextColor(context.getResources().getColor(R.color.colorTextInputPersonnelInfo));
            ((FollowViewHolder)holder).rl_btn_follow.setBackground(context.getResources().getDrawable(R.drawable.shape_button_follow));
        }else {
            ((FollowViewHolder)holder).txt_follow_title.setText("ติดตาม");
            ((FollowViewHolder)holder).txt_follow_title.setTextColor(context.getResources().getColor(R.color.white));
            ((FollowViewHolder)holder).rl_btn_follow.setBackground(context.getResources().getDrawable(R.drawable.shape_button));
        }



        ((FollowViewHolder)holder).rl_btn_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickedFollow(((FollowViewHolder)holder).txt_follow_title,((FollowViewHolder)holder).rl_btn_follow,user);
            }
        });

        ((FollowViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickedProfile(user);
            }
        });

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class FollowViewHolder extends RecyclerView.ViewHolder{
        private ImageView img_follow_profile;
        private TextView txt_follow_name,txt_follow_login_with,txt_follow_title;
        private RelativeLayout rl_btn_follow;

        public FollowViewHolder(@NonNull View itemView) {
            super(itemView);
            img_follow_profile = itemView.findViewById(R.id.img_follow_profile);
            txt_follow_name = itemView.findViewById(R.id.txt_follow_name);
            txt_follow_login_with = itemView.findViewById(R.id.txt_follow_login_with);
            txt_follow_title = itemView.findViewById(R.id.txt_follow_title);
            rl_btn_follow = itemView.findViewById(R.id.rl_btn_follow);
        }

    }
}
