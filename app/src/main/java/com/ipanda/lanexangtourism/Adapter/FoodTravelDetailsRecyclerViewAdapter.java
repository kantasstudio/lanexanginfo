package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.FoodModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class FoodTravelDetailsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<FoodModel> arrayList;
    private String language;

    public FoodTravelDetailsRecyclerViewAdapter(Context context, ArrayList<FoodModel> arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_package_tour_food_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final FoodModel itemsModel = (FoodModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        ((ItemViewHolder)holder).txt_the_date.setText((position+1)+"");

        if (itemsModel.isFoodBreakFast()){
            ((ItemViewHolder)holder).img_morning.setVisibility(View.VISIBLE);
        }else {
            ((ItemViewHolder)holder).img_morning.setImageResource(R.drawable.ic_not_food);
        }

        if (itemsModel.isFoodLunch()){
            ((ItemViewHolder)holder).img_evening.setVisibility(View.VISIBLE);
        }else {
            ((ItemViewHolder)holder).img_morning.setImageResource(R.drawable.ic_not_food);
        }

        if (itemsModel.isFoodDinner()){
            ((ItemViewHolder)holder).img_afternoon.setVisibility(View.VISIBLE);
        }else {
            ((ItemViewHolder)holder).img_morning.setImageResource(R.drawable.ic_not_food);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_the_date;
        private ImageView img_morning, img_afternoon , img_evening;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_the_date = itemView.findViewById(R.id.txt_the_date);
            img_morning = itemView.findViewById(R.id.img_morning);
            img_afternoon = itemView.findViewById(R.id.img_morning);
            img_evening = itemView.findViewById(R.id.img_morning);
        }

    }
}
