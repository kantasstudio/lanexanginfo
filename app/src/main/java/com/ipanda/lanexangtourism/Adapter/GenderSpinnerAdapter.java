package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class GenderSpinnerAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> arrayList;

    public GenderSpinnerAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final String str = arrayList.get(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.custom_layout_item_spinner, parent, false);

        TextView title = convertView.findViewById(R.id.txt_title_spinner);
        ChangeLanguageLocale changeLanguageLocale = new ChangeLanguageLocale(context);

        title.setText(str);



        return convertView;
    }
}
