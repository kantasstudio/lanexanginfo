package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.ClickListener.ItemImageClickListener;
import com.ipanda.lanexangtourism.Model.DeliciousGuaranteeModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GuaranteeRecyclerViewAdapter extends RecyclerView.Adapter<GuaranteeRecyclerViewAdapter.GuaranteeViewHolder>{

    private Context context;
    private ArrayList<DeliciousGuaranteeModel> arrayList;
    private ItemImageClickListener listener;
    private String language;

    public GuaranteeRecyclerViewAdapter(Context context, ArrayList<DeliciousGuaranteeModel> arrayList, ItemImageClickListener listener, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
    }

    @NonNull
    @Override
    public GuaranteeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_description_guarantee_view, parent,false);
        return new GuaranteeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GuaranteeViewHolder holder, int position) {
        final DeliciousGuaranteeModel itemsModel = (DeliciousGuaranteeModel) arrayList.get(position);

        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/"+itemsModel.getGuaranteePaths();
        Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((GuaranteeViewHolder)holder).img_guarantee_id);
        switch (language){
            case "th":

                break;
            case "en":

                break;
            case "lo":

                break;
            case "zh":

                break;
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class GuaranteeViewHolder extends RecyclerView.ViewHolder{
        ImageView img_guarantee_id;
        public GuaranteeViewHolder(@NonNull View itemView) {
            super(itemView);
            img_guarantee_id = itemView.findViewById(R.id.img_guarantee_id);
        }
    }


}
