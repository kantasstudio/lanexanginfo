package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.HotelAccommodationModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class HotelTravelDetailsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<HotelAccommodationModel> arrayList;
    private String language;

    public HotelTravelDetailsRecyclerViewAdapter(Context context, ArrayList<HotelAccommodationModel> arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_package_tour_hotel_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final HotelAccommodationModel itemsModel = (HotelAccommodationModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        ((ItemViewHolder)holder).txt_the_date.setText((position+1)+"");
        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_accommodation.setText(itemsModel.getAccommodationTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_accommodation.setText(itemsModel.getAccommodationEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_accommodation.setText(itemsModel.getAccommodationLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_accommodation.setText(itemsModel.getAccommodationZH());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_the_date, txt_accommodation;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_the_date = itemView.findViewById(R.id.txt_the_date);
            txt_accommodation = itemView.findViewById(R.id.txt_accommodation);
        }

    }
}
