package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class ImageAddPhotoToItemsAdapter extends RecyclerView.Adapter<ImageAddPhotoToItemsAdapter.ImagePlacesViewHolder> {

    private Context context;
    private ArrayList<ItemsPhotoDetailModel> arrayList;
    private ArrayList<ItemsPhotoDetailModel> returnArrayList;
    private String language;


    public ImageAddPhotoToItemsAdapter(Context context, ArrayList<ItemsPhotoDetailModel> arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.returnArrayList = new ArrayList<>(arrayList);
        this.language = language;
    }

    @NonNull
    @Override
    public ImagePlacesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_view, parent,false);
        return new ImagePlacesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagePlacesViewHolder holder, int position) {
        final ItemsPhotoDetailModel photo = (ItemsPhotoDetailModel) arrayList.get(position);
        final ItemsPhotoDetailModel rePhoto = (ItemsPhotoDetailModel) returnArrayList.get(position);

        Glide.with(context).load(photo.getPhotoPaths()).apply(new RequestOptions()).into(holder.image);

        ((ImagePlacesViewHolder)holder).edtDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String convert  = charSequence.toString();

                switch (language){
                    case "th":
                        rePhoto.setPhotoTextTH(convert);
                        break;
                    case "en":
                        rePhoto.setPhotoTextEN(convert);
                        break;
                    case "lo":
                        rePhoto.setPhotoTextLO(convert);
                        break;
                    case "zh":
                        rePhoto.setPhotoTextZH(convert);
                        break;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });





    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public ArrayList<ItemsPhotoDetailModel> returnArrayList(){
        return returnArrayList;
    }


    public class ImagePlacesViewHolder extends RecyclerView.ViewHolder{
        ImageButton btnDelete;
        ImageView image;
        EditText edtDescription;
        public ImagePlacesViewHolder(@NonNull View itemView) {
            super(itemView);
            btnDelete = itemView.findViewById(R.id.imgBtn_delete_image);
            image = itemView.findViewById(R.id.img_image_places);
            edtDescription = itemView.findViewById(R.id.edt_description_image);
        }
    }

}
