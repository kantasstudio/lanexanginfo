package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ipanda.lanexangtourism.Interface_click.SetThisPhotoList;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class ImagePlacesRecyclerViewAdapter extends RecyclerView.Adapter<ImagePlacesRecyclerViewAdapter.ImagePlacesViewHolder> {

    private Context context;
    private ArrayList<PhotoTourist> arrayList;
    private ArrayList<PhotoTourist> returnArrayList;
    private String language;


    private SetThisPhotoList thisPhotoList;

    public ImagePlacesRecyclerViewAdapter(Context context, ArrayList<PhotoTourist> arrayList, String language, SetThisPhotoList thisPhotoList) {
        this.context = context;
        this.arrayList = arrayList;
        this.returnArrayList = new ArrayList<>(arrayList);
        this.language = language;
        this.thisPhotoList = thisPhotoList;
    }

    @NonNull
    @Override
    public ImagePlacesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_view, parent,false);
        return new ImagePlacesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagePlacesViewHolder holder, int position) {
        final PhotoTourist photo = (PhotoTourist) arrayList.get(position);
        final PhotoTourist rePhoto = (PhotoTourist) returnArrayList.get(position);

        Glide.with(context).load(photo.getPhotoTouristPaths()).apply(new RequestOptions()).into(holder.image);

        ((ImagePlacesViewHolder)holder).edtDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String convert  = charSequence.toString();

                switch (language){
                    case "th":
                        rePhoto.setPhotoTouristTH(convert);
                        break;
                    case "en":
                        rePhoto.setPhotoTouristEN(convert);
                        break;
                    case "lo":
                        rePhoto.setPhotoTouristLO(convert);
                        break;
                    case "zh":
                        rePhoto.setPhotoTouristZH(convert);
                        break;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });





    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public ArrayList<PhotoTourist> returnArrayList(){
        return returnArrayList;
    }


    public class ImagePlacesViewHolder extends RecyclerView.ViewHolder{
        ImageButton btnDelete;
        ImageView image;
        EditText edtDescription;
        public ImagePlacesViewHolder(@NonNull View itemView) {
            super(itemView);
            btnDelete = itemView.findViewById(R.id.imgBtn_delete_image);
            image = itemView.findViewById(R.id.img_image_places);
            edtDescription = itemView.findViewById(R.id.edt_description_image);
        }
    }

}
