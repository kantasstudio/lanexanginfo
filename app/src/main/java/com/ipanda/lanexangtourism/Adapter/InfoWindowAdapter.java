package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;

public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private View view;
    private ItemsModel items;

    public InfoWindowAdapter(Context context, View view, ItemsModel items) {
        this.context = context;
        this.view = view;
        this.items = items;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        TextView txt_google_title_id = view.findViewById(R.id.txt_google_title_id);
        txt_google_title_id.setText(marker.getTitle());

        return view;
    }
}
