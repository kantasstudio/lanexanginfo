package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.ClickListener.ItemImageClickListener;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class InterestingRecyclerViewAdapter extends RecyclerView.Adapter<InterestingRecyclerViewAdapter.InterestingViewHolder>{

    private Context context;
    private ArrayList<ItemsPhotoDetailModel> arrayList;
    private ItemImageClickListener listener;
    private String language;

    public InterestingRecyclerViewAdapter(Context context, ArrayList<ItemsPhotoDetailModel> arrayList, String language, ItemImageClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
    }

    @NonNull
    @Override
    public InterestingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_description_interesting_view, parent,false);
        return new InterestingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InterestingViewHolder holder, int position) {
        final ItemsPhotoDetailModel itemsModel = (ItemsPhotoDetailModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        Picasso.get().load(paths+itemsModel.getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((InterestingViewHolder)holder).img_interesting_id);

        switch (language){
            case "th":
                ((InterestingViewHolder)holder).txt_interesting_details_id.setText(itemsModel.getPhotoTextTH());
                break;
            case "en":
                ((InterestingViewHolder)holder).txt_interesting_details_id.setText(itemsModel.getPhotoTextEN());
                break;
            case "lo":
                ((InterestingViewHolder)holder).txt_interesting_details_id.setText(itemsModel.getPhotoTextLO());
                break;
            case "zh":
                ((InterestingViewHolder)holder).txt_interesting_details_id.setText(itemsModel.getPhotoTextZH());
                break;
        }

        ((InterestingViewHolder)holder).img_interesting_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClickedInteresting(position);
            }
        });

        ((InterestingViewHolder)holder).txt_interesting_details_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class InterestingViewHolder extends RecyclerView.ViewHolder{
        ImageView img_interesting_id;
        TextView txt_interesting_details_id;
        public InterestingViewHolder(@NonNull View itemView) {
            super(itemView);
            img_interesting_id = itemView.findViewById(R.id.img_interesting_id);
            txt_interesting_details_id = itemView.findViewById(R.id.txt_interesting_details_id);
        }
    }


}
