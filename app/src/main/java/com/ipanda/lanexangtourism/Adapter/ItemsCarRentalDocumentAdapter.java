package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class ItemsCarRentalDocumentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<BookingConditionModel> arrayList;
    private String language;

    public ItemsCarRentalDocumentAdapter(Context context, ArrayList<BookingConditionModel> arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.items_document_carrental, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final BookingConditionModel bookings = (BookingConditionModel) arrayList.get(position);

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_condition_document.setText(bookings.getConditionDetailTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_condition_document.setText(bookings.getConditionDetailEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_condition_document.setText(bookings.getConditionDetailLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_condition_document.setText(bookings.getConditionDetailZH());
                break;
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_condition_document;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_condition_document = itemView.findViewById(R.id.txt_condition_document);
        }

    }
}
