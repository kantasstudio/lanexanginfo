package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.ItemsCarRentClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ItemsCarRentalRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ItemsModel> arrayList;
    private String language;
    private ItemsCarRentClickListener clickListener;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;

    public ItemsCarRentalRecyclerViewAdapter(Context context, ArrayList<ItemsModel> arrayList, String language, ItemsCarRentClickListener clickListener, RateModel rateModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.clickListener = clickListener;
        this.rateModel = rateModel;
        exchangeRate = new ExchangeRate();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_car_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ItemsModel itemsModel = (ItemsModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        Picasso.get().load(paths+itemsModel.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).img_items);
        ((ItemViewHolder)holder).txt_passenger.setText(context.getResources().getString(R.string.text_passenger)+" : "+itemsModel.getCarModel().getSeats()+"");

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = null;

        if (rateModel != null) {
            String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(itemsModel.getCarModel().getPricePerDay());
                    ((ItemViewHolder) holder).txt_exchange_rate_id.setText(currency);
                    ((ItemViewHolder) holder).txt_price_car.setText(price);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getCarModel().getPricePerDay(),rateModel.getRateUSD()));
                    ((ItemViewHolder) holder).txt_exchange_rate_id.setText(currency);
                    ((ItemViewHolder) holder).txt_price_car.setText(price);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getCarModel().getPricePerDay(),rateModel.getRateCNY()));
                    ((ItemViewHolder) holder).txt_exchange_rate_id.setText(currency);
                    ((ItemViewHolder) holder).txt_price_car.setText(price);
                    break;
            }
        }else {
            price = decimalFormat.format(itemsModel.getCarModel().getPricePerDay());
            ((ItemViewHolder) holder).txt_price_car.setText(price);
        }

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_name_car.setText(itemsModel.getTopicTH());
                ((ItemViewHolder)holder).txt_type_car.setText(context.getResources().getString(R.string.text_type)+" : "+itemsModel.getCarModel().getTypeTH());
                ((ItemViewHolder)holder).txt_transmission_car.setText(context.getResources().getString(R.string.text_gear)+" : "+itemsModel.getCarModel().getGearSystemTH());

                break;
            case "en":
                ((ItemViewHolder)holder).txt_name_car.setText(itemsModel.getTopicEN());
                ((ItemViewHolder)holder).txt_type_car.setText(context.getResources().getString(R.string.text_type)+" : "+itemsModel.getCarModel().getTypeEN());
                ((ItemViewHolder)holder).txt_transmission_car.setText(context.getResources().getString(R.string.text_gear)+" : "+itemsModel.getCarModel().getGearSystemEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_name_car.setText(itemsModel.getTopicLO());
                ((ItemViewHolder)holder).txt_type_car.setText(context.getResources().getString(R.string.text_type)+" : "+itemsModel.getCarModel().getTypeLO());
                ((ItemViewHolder)holder).txt_transmission_car.setText(context.getResources().getString(R.string.text_gear)+" : "+itemsModel.getCarModel().getGearSystemLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_name_car.setText(itemsModel.getTopicZH());
                ((ItemViewHolder)holder).txt_type_car.setText(context.getResources().getString(R.string.text_type)+" : "+itemsModel.getCarModel().getTypeZH());
                ((ItemViewHolder)holder).txt_transmission_car.setText(context.getResources().getString(R.string.text_gear)+" : "+itemsModel.getCarModel().getGearSystemZH());
                break;
        }


        ((ItemViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.itemClickedItems(itemsModel);
            }
        });

        if (itemsModel.getBusinessModel() != null){
            switch (language){
                case "th":
                    ((ItemViewHolder)holder).txt_company_name.setText(itemsModel.getBusinessModel().getNameTH());
                    break;
                case "en":
                    ((ItemViewHolder)holder).txt_company_name.setText(itemsModel.getBusinessModel().getNameEN());
                    break;
                case "lo":
                    ((ItemViewHolder)holder).txt_company_name.setText(itemsModel.getBusinessModel().getNameLO());
                    break;
                case "zh":
                    ((ItemViewHolder)holder).txt_company_name.setText(itemsModel.getBusinessModel().getNameZH());
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private ImageView img_items;
        private TextView txt_name_car, txt_type_car, txt_transmission_car, txt_passenger, txt_price_car, txt_company_name;
        private TextView txt_exchange_rate_id;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            img_items = itemView.findViewById(R.id.img_items);
            txt_name_car = itemView.findViewById(R.id.txt_name_car);
            txt_type_car = itemView.findViewById(R.id.txt_type_car);
            txt_transmission_car = itemView.findViewById(R.id.txt_transmission_car);
            txt_passenger = itemView.findViewById(R.id.txt_passenger);
            txt_price_car = itemView.findViewById(R.id.txt_price_car);
            txt_company_name = itemView.findViewById(R.id.txt_company_name);
            txt_exchange_rate_id = itemView.findViewById(R.id.txt_exchange_rate_title_id);
        }

    }
}
