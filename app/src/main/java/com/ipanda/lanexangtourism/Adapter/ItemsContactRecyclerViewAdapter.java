package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ItemsClickListener;
import com.ipanda.lanexangtourism.Model.ItemsContactModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ItemsContactRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private Context context;
    private ArrayList<ItemsContactModel> arrayList;
    private ArrayList<ItemsContactModel> arrayListFull;
    private ItemsClickListener listener;
    private String language;
    private TextView countFilter;

    public ItemsContactRecyclerViewAdapter(Context context, ArrayList<ItemsContactModel> arrayList, ItemsClickListener listener, String language, TextView txt_count_filter) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
        this.arrayListFull = new ArrayList<>(arrayList);
        this.countFilter = txt_count_filter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_view2, parent, false);
        return new HotelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ItemsContactModel itemsModel = (ItemsContactModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+itemsModel.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((HotelViewHolder)holder).imgItemsCover);

        switch (language){
            case "th":
                if (itemsModel.getTopicTH().length() > 20){
                    String text = itemsModel.getTopicTH().substring(0,20)+"...";
                    ((HotelViewHolder)holder).txtItemsTopic.setText(text);
                }else {
                    ((HotelViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicTH());
                }
                ((HotelViewHolder)holder).txtItemsContact.setText(itemsModel.getContactTH());
                ((HotelViewHolder)holder).txtItemsSubcategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryTH());
                break;
            case "en":
                if (itemsModel.getTopicEN().length() > 20){
                    String text = itemsModel.getTopicEN().substring(0,20)+"...";
                    ((HotelViewHolder)holder).txtItemsTopic.setText(text);
                }else {
                    ((HotelViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicEN());
                }
                ((HotelViewHolder)holder).txtItemsContact.setText(itemsModel.getContactEN());
                ((HotelViewHolder)holder).txtItemsSubcategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryEN());
                break;
            case "lo":
                if (itemsModel.getTopicEN().length() > 20){
                    String text = itemsModel.getTopicEN().substring(0,20)+"...";
                    ((HotelViewHolder)holder).txtItemsTopic.setText(text);
                }else {
                    ((HotelViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicEN());
                }
                ((HotelViewHolder)holder).txtItemsContact.setText(itemsModel.getContactLO());
                ((HotelViewHolder)holder).txtItemsSubcategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryLO());
                break;
            case "zh":
                if (itemsModel.getTopicZH().length() > 20){
                    String text = itemsModel.getTopicZH().substring(0,20)+"...";
                    ((HotelViewHolder)holder).txtItemsTopic.setText(text);
                }else {
                    ((HotelViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicZH());
                }
                ((HotelViewHolder)holder).txtItemsContact.setText(itemsModel.getContactZH());
                ((HotelViewHolder)holder).txtItemsSubcategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryZH());
                break;
        }

        ((HotelViewHolder)holder).txtItemsOpenClose.setText(itemsModel.getTimeOpen()+"-"+itemsModel.getTimeClose());
        ((HotelViewHolder)holder).txtItemsRating.setText(itemsModel.getRatingStarModel().getAverageReview()+"");
        ((HotelViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_contact));
        ((HotelViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClicked(itemsModel);
            }
        });
        ((HotelViewHolder)holder).imgItemsBookMarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClicked(((HotelViewHolder)holder).imgItemsBookMarks,itemsModel);
            }
        });

        if (itemsModel.getBookMarksModel() != null) {
            if (itemsModel.getBookMarksModel().isBookmarksState()) {
                ((HotelViewHolder) holder).imgItemsBookMarks.setImageResource(R.drawable.ic_bookmark_red);
            } else {
                ((HotelViewHolder) holder).imgItemsBookMarks.setImageResource(R.drawable.ic_bookmark_inactive);
            }
        }

        if (itemsModel.getPeriodDayModel() != null && itemsModel.getPeriodTimeModel() != null) {
            if (itemsModel.getPeriodDayModel().getPeriodOpenId() == itemsModel.getPeriodDayModel().getPeriodCloseId()) {
                switch (language) {
                    case "th":
                        ((HotelViewHolder)holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenTH());
                        break;
                    case "en":
                        ((HotelViewHolder)holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenEN());
                        break;
                    case "lo":
                        ((HotelViewHolder)holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenLO());
                        break;
                    case "zh":
                        ((HotelViewHolder)holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenZH());
                        break;
                }
            } else {
                switch (language) {
                    case "th":
                        ((HotelViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenTH() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseTH());
                        break;
                    case "en":
                        ((HotelViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenEN() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseEN());
                        break;
                    case "lo":
                        ((HotelViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenLO() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseLO());
                        break;
                    case "zh":
                        ((HotelViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenZH() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseZH());
                        break;
                }
            }
        }

        try {
            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
            Date currentTime  = parser.parse(getCurrentTime());
            Date timeOpen = parser.parse(itemsModel.getTimeOpen());
            Date timeClosed = parser.parse(itemsModel.getTimeClose());

            int diffOpen = currentTime.compareTo(timeOpen);
            int diffClosed = currentTime.compareTo(timeClosed);

            if (timeOpen.equals(timeClosed)) {
                ((HotelViewHolder) holder).txtStatusOpen.setText(context.getString(R.string.text_open));
                ((HotelViewHolder) holder).txtStatusOpen.setTextColor(context.getResources().getColor(R.color.colorFontOpen));
            }else {
                if (diffOpen >= 0 && diffClosed <= 0) {
                    ((HotelViewHolder) holder).txtStatusOpen.setText(context.getString(R.string.text_open));
                    ((HotelViewHolder) holder).txtStatusOpen.setTextColor(context.getResources().getColor(R.color.colorFontOpen));
                } else if (diffOpen <= 0 && diffClosed >= 0) {
                    ((HotelViewHolder) holder).txtStatusOpen.setText(context.getString(R.string.text_closed));
                    ((HotelViewHolder) holder).txtStatusOpen.setTextColor(context.getResources().getColor(R.color.status_refuse));
                } else {
                    ((HotelViewHolder) holder).txtStatusOpen.setText(context.getString(R.string.text_closed));
                    ((HotelViewHolder) holder).txtStatusOpen.setTextColor(context.getResources().getColor(R.color.status_refuse));
                }
            }

        }catch (ParseException ex){
            Log.e("error diff", ex.toString());
        }

        ((HotelViewHolder)holder).txtItemsKG.setText(itemsModel.getDistance()+"");

    }

    @Override
    public int getItemCount() {
        int count = arrayList.size();
        countFilter.setText(count+"");
        return arrayList.size();
    }



    public class HotelViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgItemsCover, imgItemsBookMarks;
        private TextView txtItemsTopic;
        private TextView txtItemsContact, txtItemsSubcategory, txtItemsRating, txtItemsOpenClose, txtItemsKG;
        private TextView txtStatusOpen;
        private TextView txtOpenEveryday;
        private RelativeLayout rlBackGroundStar;

        public HotelViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItemsCover = itemView.findViewById(R.id.img_cover_items);
            imgItemsBookMarks = itemView.findViewById(R.id.img_items_bookmarks);
            txtItemsTopic = itemView.findViewById(R.id.txt_items_topic);
            txtItemsContact = itemView.findViewById(R.id.txt_items_contact);
            txtItemsSubcategory = itemView.findViewById(R.id.txt_items_subcategory);
            txtItemsRating = itemView.findViewById(R.id.txt_items_rating);
            txtItemsOpenClose = itemView.findViewById(R.id.txt_items_open_close);
            txtItemsKG = itemView.findViewById(R.id.txt_items_kg);
            rlBackGroundStar = itemView.findViewById(R.id.rl_bg_star);
            txtStatusOpen = itemView.findViewById(R.id.txt_status_open);
            txtOpenEveryday = itemView.findViewById(R.id.txt_open_everyday);
        }

    }

    public static String getCurrentTime() {
        String DATE_FORMAT_1 = "HH:mm";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_1);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    @Override
    public Filter getFilter() {
        return itemsFilter;
    }

    private Filter itemsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            ArrayList<ItemsContactModel> filteredList = new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0){
                filteredList.addAll(arrayListFull);
            }else {
                String filterPattern = charSequence.toString().toLowerCase().trim();
                for (ItemsContactModel item : arrayListFull) {

                    if (filterPattern.equals("open")){
                        if (item.isOpenOrClosed()){
                            filteredList.add(item);
                        }else {

                        }
                    }else {
                        if (item.isOpenOrClosed()){

                        }else {
                            filteredList.add(item);
                        }
                    }

                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return  results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            arrayList.clear();
            arrayList.addAll((ArrayList) filterResults.values);
            notifyDataSetChanged();
        }
    };
}
