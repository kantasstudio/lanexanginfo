package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ItemsSearchClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ItemsHomeSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ItemsModel> itemsModelArrayList;
    private String language;
    private ItemsSearchClickListener listener;


    public ItemsHomeSearchAdapter(Context context, ArrayList<ItemsModel> itemsModelArrayList,String language, ItemsSearchClickListener listener) {
        this.context = context;
        this.itemsModelArrayList = itemsModelArrayList;
        this.language = language;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_home_search_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ItemsModel itemsModel = (ItemsModel) itemsModelArrayList.get(position);

        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+itemsModel.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).imgItemsCover);

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicTH());
                ((ItemViewHolder)holder).txtItemsCategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicEN());
                ((ItemViewHolder)holder).txtItemsCategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicLO());
                ((ItemViewHolder)holder).txtItemsCategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicZH());
                ((ItemViewHolder)holder).txtItemsCategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryZH());
                break;
        }

        ((ItemViewHolder)holder).txtItemsRating.setText(itemsModel.getRatingStarModel().getAverageReview()+"");

        if (itemsModel.getMenuItemModel().getMenuItemId() == 2){
            ((ItemViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_tour));
        }else if (itemsModel.getMenuItemModel().getMenuItemId() == 3){
            ((ItemViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_restaurant));
        }else if (itemsModel.getMenuItemModel().getMenuItemId() == 4){
            ((ItemViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_shopping));
        }else if (itemsModel.getMenuItemModel().getMenuItemId() == 5){
            ((ItemViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_hotel));
        }else if (itemsModel.getMenuItemModel().getMenuItemId() == 6){
            ((ItemViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_contact));
        }else {

        }

        ((ItemViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickedItemSearch(itemsModel);
            }
        });

        ((ItemViewHolder)holder).txtItemsKG.setText(itemsModel.getDistance()+"");


    }

    @Override
    public int getItemCount() {
        return itemsModelArrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgItemsCover;
        private TextView txtItemsTopic;
        private TextView txtItemsCategory;
        private TextView txtItemsRating;
        private TextView txtItemsKG;
        private RelativeLayout rlBackGroundStar;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItemsCover = itemView.findViewById(R.id.img_item_search_id);
            txtItemsTopic = itemView.findViewById(R.id.txt_item_title_id);
            txtItemsCategory = itemView.findViewById(R.id.txt_item_category_id);
            txtItemsRating = itemView.findViewById(R.id.txt_items_rating);
            txtItemsKG = itemView.findViewById(R.id.txt_item_kg_id);
            rlBackGroundStar = itemView.findViewById(R.id.rl_bg_star);
        }

    }
}
