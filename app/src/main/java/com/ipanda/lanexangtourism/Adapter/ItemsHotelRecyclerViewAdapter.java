package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.ItemsClickListener;
import com.ipanda.lanexangtourism.Model.ItemsHotelModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ItemsHotelRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ItemsHotelModel> arrayList;
    private ItemsClickListener listener;
    private String language;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;

    public ItemsHotelRecyclerViewAdapter(Context context, ArrayList<ItemsHotelModel> arrayList, ItemsClickListener listener, String language, RateModel rateModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
        this.rateModel = rateModel;
        exchangeRate = new ExchangeRate();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_view3, parent, false);
        return new HotelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ItemsHotelModel itemsModel = (ItemsHotelModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+itemsModel.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((HotelViewHolder)holder).imgItemsCover);
        ((HotelViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicTH());
        ((HotelViewHolder)holder).txtItemsContact.setText(itemsModel.getContactTH());

        switch (language){
            case "th":
                if (itemsModel.getTopicTH().length() > 40){
                    String text = itemsModel.getTopicTH().substring(0,40)+"...";
                    ((HotelViewHolder)holder).txtItemsTopic.setText(text);
                }else {
                    ((HotelViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicTH());
                }
                ((HotelViewHolder)holder).txtItemsContact.setText(itemsModel.getContactTH());
                break;
            case "en":
                if (itemsModel.getTopicEN().length() > 40){
                    String text = itemsModel.getTopicEN().substring(0,40)+"...";
                    ((HotelViewHolder)holder).txtItemsTopic.setText(text);
                }else {
                    ((HotelViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicEN());
                }
                ((HotelViewHolder)holder).txtItemsContact.setText(itemsModel.getContactEN());
                break;
            case "lo":
                if (itemsModel.getTopicLO().length() > 40){
                    String text = itemsModel.getTopicLO().substring(0,40)+"...";
                    ((HotelViewHolder)holder).txtItemsTopic.setText(text);
                }else {
                    ((HotelViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicLO());
                }
                ((HotelViewHolder)holder).txtItemsContact.setText(itemsModel.getContactLO());
                break;
            case "zh":
                if (itemsModel.getTopicZH().length() > 40){
                    String text = itemsModel.getTopicZH().substring(0,40)+"...";
                    ((HotelViewHolder)holder).txtItemsTopic.setText(text);
                }else {
                    ((HotelViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicZH());
                }
                ((HotelViewHolder)holder).txtItemsContact.setText(itemsModel.getContactZH());
                break;
        }

        ((HotelViewHolder)holder).txtItemsRating.setText(itemsModel.getRatingStarModel().getAverageReview()+"");
        ((HotelViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_hotel));
        ((HotelViewHolder)holder).txtItemsReview.setText(itemsModel.getRatingStarModel().getCountReview()+"");
        ((HotelViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClicked(itemsModel);
            }
        });
        ((HotelViewHolder)holder).imgItemsBookMarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClicked(((HotelViewHolder)holder).imgItemsBookMarks,itemsModel);
            }
        });
        if (itemsModel.getBookMarksModel() != null) {
            if (itemsModel.getBookMarksModel().isBookmarksState()) {
                ((HotelViewHolder) holder).imgItemsBookMarks.setImageResource(R.drawable.ic_bookmark_red);
            } else {
                ((HotelViewHolder) holder).imgItemsBookMarks.setImageResource(R.drawable.ic_bookmark);
            }
        }

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String price  = null;

        if (rateModel != null) {
            String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(itemsModel.getPrice());
                    ((HotelViewHolder)holder).txtItemsCurrency.setText(currency);
                    ((HotelViewHolder)holder).txtItemsPrice.setText(price);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getPrice(),rateModel.getRateUSD()));
                    ((HotelViewHolder)holder).txtItemsCurrency.setText(currency);
                    ((HotelViewHolder)holder).txtItemsPrice.setText(price);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getPrice(),rateModel.getRateCNY()));
                    ((HotelViewHolder)holder).txtItemsCurrency.setText(currency);
                    ((HotelViewHolder)holder).txtItemsPrice.setText(price);
                    break;
            }
        }else {
            price = decimalFormat.format(itemsModel.getPrice());
            ((HotelViewHolder)holder).txtItemsPrice.setText(price);
        }

        ((HotelViewHolder)holder).txtItemsKG.setText(itemsModel.getDistance()+"");

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class HotelViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgItemsCover, imgItemsBookMarks;
        private TextView txtItemsTopic;
        private TextView txtItemsCurrency;
        private TextView txtItemsContact, txtItemsRating,txtItemsReview, txtItemsPrice, txtItemsKG;
        private RelativeLayout rlBackGroundStar;

        public HotelViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItemsCover = itemView.findViewById(R.id.img_cover_items);
            imgItemsBookMarks = itemView.findViewById(R.id.img_items_bookmarks);
            txtItemsTopic = itemView.findViewById(R.id.txt_items_topic);
            txtItemsContact = itemView.findViewById(R.id.txt_items_contact);
            txtItemsRating = itemView.findViewById(R.id.txt_items_rating);
            txtItemsReview = itemView.findViewById(R.id.txt_items_review);
            txtItemsCurrency = itemView.findViewById(R.id.txt_items_currency);
            txtItemsPrice = itemView.findViewById(R.id.txt_items_price);
            txtItemsKG = itemView.findViewById(R.id.txt_items_kg);
            rlBackGroundStar = itemView.findViewById(R.id.rl_bg_star);
        }

    }
}
