package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Interface_click.ItemsMapsClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class ItemsMapsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable{

    private Context context;
    private ArrayList<ItemsModel> arrayList;
    private ArrayList<ItemsModel> arrayListFull;
    private String language;
    private ItemsMapsClickListener listener;

    public ItemsMapsRecyclerViewAdapter(Context context, ArrayList<ItemsModel> arrayList, String language,ItemsMapsClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
        this.arrayListFull = new ArrayList<>(arrayList);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_hide, parent, false);
        return new ItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ItemsModel itemsModel = (ItemsModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+itemsModel.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemsViewHolder)holder).imgItemsCover);

        ((ItemsViewHolder)holder).txtItemsKG.setText(itemsModel.getDistance()+"");

        switch (language){
            case "th":
                ((ItemsViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicTH());
                ((ItemsViewHolder)holder).txtItemsContact.setText(itemsModel.getContactTH());
                ((ItemsViewHolder)holder).txtItemsSubcategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryTH());
                break;
            case "en":
                ((ItemsViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicEN());
                ((ItemsViewHolder)holder).txtItemsContact.setText(itemsModel.getContactEN());
                ((ItemsViewHolder)holder).txtItemsSubcategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryEN());
                break;
            case "lo":
                ((ItemsViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicLO());
                ((ItemsViewHolder)holder).txtItemsContact.setText(itemsModel.getContactLO());
                ((ItemsViewHolder)holder).txtItemsSubcategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryLO());
                break;
            case "zh":
                ((ItemsViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicZH());
                ((ItemsViewHolder)holder).txtItemsContact.setText(itemsModel.getContactZH());
                ((ItemsViewHolder)holder).txtItemsSubcategory.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryZH());
                break;
        }


        ((ItemsViewHolder)holder).txtItemsOpenClose.setText(itemsModel.getTimeOpen()+"-"+itemsModel.getTimeClose());

        switch (itemsModel.getMenuItemModel().getMenuItemId()){
            case 2:
                ((ItemsViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_tour));
                break;
            case 3:
                ((ItemsViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_restaurant));
                break;
            case 4:
                ((ItemsViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_shopping));
                break;
            case 5:
                ((ItemsViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_hotel));
                break;
            case 6:
                ((ItemsViewHolder)holder).rlBackGroundStar.setBackground(context.getResources().getDrawable(R.drawable.shape_star_contact));
                break;

        }

        ((ItemsViewHolder)holder).txtItemsRating.setText(itemsModel.getRatingStarModel().getAverageReview()+"");

        ((ItemsViewHolder)holder).btn_in_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickedItemsMove(itemsModel);
            }
        });

        ((ItemsViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.clickedItemsMaps(itemsModel);
            }
        });

        ((ItemsViewHolder)holder).imgItemsBookMarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ItemsViewHolder)holder).onClickBookmark(itemsModel);
            }
        });

        if (itemsModel.getBookMarksModel().isBookmarksState()){
            ((ItemsViewHolder)holder).imgItemsBookMarks.setImageResource(R.drawable.ic_bookmark_red);
        }else {
            ((ItemsViewHolder)holder).imgItemsBookMarks.setImageResource(R.drawable.ic_bookmark_dark);
        }

        if (itemsModel.getPeriodDayModel() != null && itemsModel.getPeriodTimeModel() != null) {
            if (itemsModel.getPeriodDayModel().getPeriodOpenId() == itemsModel.getPeriodDayModel().getPeriodCloseId()) {
                switch (language) {
                    case "th":
                        ((ItemsViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenTH());
                        break;
                    case "en":
                        ((ItemsViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenEN());
                        break;
                    case "lo":
                        ((ItemsViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenLO());
                        break;
                    case "zh":
                        ((ItemsViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenZH());
                        break;
                }
            } else {
                switch (language) {
                    case "th":
                        ((ItemsViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenTH() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseTH());
                        break;
                    case "en":
                        ((ItemsViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenEN() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseEN());
                        break;
                    case "lo":
                        ((ItemsViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenLO() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseLO());
                        break;
                    case "zh":
                        ((ItemsViewHolder) holder).txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenZH() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseZH());
                        break;
                }
            }
        }

        try {
            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
            Date currentTime  = parser.parse(getCurrentTime());
            Date timeOpen = parser.parse(itemsModel.getTimeOpen());
            Date timeClosed = parser.parse(itemsModel.getTimeClose());

            int diffOpen = currentTime.compareTo(timeOpen);
            int diffClosed = currentTime.compareTo(timeClosed);

            if (timeOpen.equals(timeClosed)) {
                ((ItemsViewHolder) holder).txtStatusOpen.setText(context.getString(R.string.text_open));
                ((ItemsViewHolder) holder).txtStatusOpen.setTextColor(context.getResources().getColor(R.color.colorFontOpen));
            }else {
                if (diffOpen >= 0 && diffClosed <= 0) {
                    ((ItemsViewHolder) holder).txtStatusOpen.setText(context.getString(R.string.text_open));
                    ((ItemsViewHolder) holder).txtStatusOpen.setTextColor(context.getResources().getColor(R.color.colorFontOpen));
                } else if (diffOpen <= 0 && diffClosed >= 0) {
                    ((ItemsViewHolder) holder).txtStatusOpen.setText(context.getString(R.string.text_closed));
                    ((ItemsViewHolder) holder).txtStatusOpen.setTextColor(context.getResources().getColor(R.color.status_refuse));
                } else {
                    ((ItemsViewHolder) holder).txtStatusOpen.setText(context.getString(R.string.text_closed));
                    ((ItemsViewHolder) holder).txtStatusOpen.setTextColor(context.getResources().getColor(R.color.status_refuse));
                }
            }

        }catch (ParseException ex){
            Log.e("error diff", ex.toString());
        }



    }

    @Override
    public int getItemCount() {
/*        if(arrayList != null){
            return arrayListFull.size();
        } else {
            return 0;
        }*/
        return arrayList.size();
    }



    public class ItemsViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgItemsCover, imgItemsBookMarks;
        private TextView txtItemsTopic,txtItemsContact, txtItemsSubcategory, txtItemsRating, txtItemsOpenClose, txtItemsKG;
        private TextView txtStatusOpen;
        private TextView txtOpenEveryday;
        private RelativeLayout rlBackGroundStar;
        private CardView btn_in_location;

        public ItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItemsCover = itemView.findViewById(R.id.img_cover_items);
            imgItemsBookMarks = itemView.findViewById(R.id.img_items_bookmarks);
            txtItemsTopic = itemView.findViewById(R.id.txt_items_topic);
            txtItemsContact = itemView.findViewById(R.id.txt_items_contact);
            txtItemsSubcategory = itemView.findViewById(R.id.txt_items_subcategory);
            txtItemsRating = itemView.findViewById(R.id.txt_items_rating);
            txtItemsOpenClose = itemView.findViewById(R.id.txt_items_open_close);
            txtItemsKG = itemView.findViewById(R.id.txt_items_kg);
            rlBackGroundStar = itemView.findViewById(R.id.rl_bg_star);
            txtStatusOpen = itemView.findViewById(R.id.txt_items_status);
            txtOpenEveryday = itemView.findViewById(R.id.txt_open_everyday);
            btn_in_location = itemView.findViewById(R.id.btn_in_location);
        }

        private void onClickBookmark(ItemsModel itemsModel){
            String userId = context.getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
            if (itemsModel != null && userId != null){
                HttpCall httpCallPost = new HttpCall();
                httpCallPost.setMethodType(HttpCall.POST);
                httpCallPost.setUrl(context.getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/BookMarks");
                HashMap<String,String> paramsPost = new HashMap<>();
                paramsPost.put("User_user_id",userId);
                paramsPost.put("Items_items_id", String.valueOf(itemsModel.getItemsId()));
                httpCallPost.setParams(paramsPost);
                new HttpPostRequestAsyncTask().execute(httpCallPost);
            }else {
                new AlertDialog.Builder(context)
                        .setTitle("กรุณาล็อกอินก่อน")
                        .setMessage("....")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(context, LoginActivity.class);
                                context.startActivity(intent);
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create().show();
            }

            if (itemsModel.getBookMarksModel().isBookmarksState()){
                itemsModel.getBookMarksModel().setBookmarksState(false);
                imgItemsBookMarks.setImageResource(R.drawable.ic_bookmark_inactive);
            }else {
                itemsModel.getBookMarksModel().setBookmarksState(true);
                imgItemsBookMarks.setImageResource(R.drawable.ic_bookmark_red);
            }
        }
    }

    private static String getCurrentTime() {
        String DATE_FORMAT_1 = "HH:mm";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_1);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    @Override
    public Filter getFilter() {
        return itemsFilter;
    }

    private Filter itemsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            ArrayList<ItemsModel> filteredList = new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0){
                filteredList.addAll(arrayListFull);
            }else {
                String filterPattern = charSequence.toString().toLowerCase().trim();
                for (ItemsModel item : arrayListFull) {

                    switch (language){
                        case "th":
                            if (item.getTopicTH().toLowerCase().contains(filterPattern)){
                                filteredList.add(item);
                            }
                            break;
                        case "en":
                            if (item.getTopicEN().toLowerCase().contains(filterPattern)){
                                filteredList.add(item);
                            }
                            break;
                        case "lo":
                            if (item.getTopicLO().toLowerCase().contains(filterPattern)){
                                filteredList.add(item);
                            }
                            break;
                        case "zh":
                            if (item.getTopicZH().toLowerCase().contains(filterPattern)){
                                filteredList.add(item);
                            }
                            break;
                    }

                    if (filterPattern.equals("open")){
                        if (item.isOpenOrClosed()){
                            filteredList.add(item);
                        }else {

                        }
                    }else {
                        if (item.isOpenOrClosed()){

                        }else {
                            filteredList.add(item);
                        }
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return  results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            arrayList.clear();
            arrayList.addAll((ArrayList) filterResults.values);
            notifyDataSetChanged();
        }
    };
}
