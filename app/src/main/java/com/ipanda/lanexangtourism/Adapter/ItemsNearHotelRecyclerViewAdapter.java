package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.ClickListener.ItemBookmarkClickListener;
import com.ipanda.lanexangtourism.Interface_click.ItemsClickListener;
import com.ipanda.lanexangtourism.Model.ItemsHotelModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ItemsNearHotelRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ItemsHotelModel> arrayList;
    private ItemsClickListener listener;
    private String language;
    private ItemBookmarkClickListener clickListener;

    public ItemsNearHotelRecyclerViewAdapter(Context context, ArrayList<ItemsHotelModel> arrayList, ItemsClickListener listener, String language, ItemBookmarkClickListener clickListener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_view, parent, false);
        return new ItemsNearViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ItemsHotelModel itemsModel = (ItemsHotelModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+itemsModel.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemsNearViewHolder)holder).img_near_items);

        if (itemsModel.getRatingStarModel() != null) {
            ((ItemsNearViewHolder) holder).txt_items_rating.setText(itemsModel.getRatingStarModel().getAverageReview() + "");
        }
        ((ItemsNearViewHolder)holder).rl_bg_star.setBackground(context.getResources().getDrawable(R.drawable.shape_star_hotel));
        switch (language){
            case "th":
                ((ItemsNearViewHolder)holder).txt_near_topic.setText(itemsModel.getTopicTH());
                ((ItemsNearViewHolder)holder).txt_near_category.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryTH());
                ((ItemsNearViewHolder)holder).txt_near_kg.setText(itemsModel.getDistance()+" "+context.getResources().getString(R.string.text_km));
                break;
            case "en":
                ((ItemsNearViewHolder)holder).txt_near_topic.setText(itemsModel.getTopicEN());
                ((ItemsNearViewHolder)holder).txt_near_category.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryEN());
                ((ItemsNearViewHolder)holder).txt_near_kg.setText(itemsModel.getDistance()+" "+context.getResources().getString(R.string.text_km));
                break;
            case "lo":
                ((ItemsNearViewHolder)holder).txt_near_topic.setText(itemsModel.getTopicLO());
                ((ItemsNearViewHolder)holder).txt_near_category.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryLO());
                ((ItemsNearViewHolder)holder).txt_near_kg.setText(itemsModel.getDistance()+" "+context.getResources().getString(R.string.text_km));
                break;
            case "zh":
                ((ItemsNearViewHolder)holder).txt_near_topic.setText(itemsModel.getTopicZH());
                ((ItemsNearViewHolder)holder).txt_near_category.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryZH());
                ((ItemsNearViewHolder)holder).txt_near_kg.setText(itemsModel.getDistance()+" "+context.getResources().getString(R.string.text_km));
                break;
        }
        ((ItemsNearViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClicked(itemsModel);
            }
        });

        ((ItemsNearViewHolder)holder).img_near_bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.itemClickedBookmark(((ItemsNearViewHolder)holder).img_near_bookmark,itemsModel);
            }
        });
        if (itemsModel.getBookMarksModel() != null) {
            if (itemsModel.getBookMarksModel().isBookmarksState()) {
                ((ItemsNearViewHolder) holder).img_near_bookmark.setImageResource(R.drawable.ic_bookmark_red);
            } else {
                ((ItemsNearViewHolder) holder).img_near_bookmark.setImageResource(R.drawable.ic_bookmark);
            }
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemsNearViewHolder extends RecyclerView.ViewHolder{
        private ImageView img_near_items, img_near_bookmark;
        private TextView txt_near_topic;
        private TextView txt_near_kg, txt_status_open, txt_near_category, txt_items_rating;
        private RelativeLayout rl_bg_star;

        public ItemsNearViewHolder(@NonNull View itemView) {
            super(itemView);
            img_near_items = itemView.findViewById(R.id.img_near_items);
            img_near_bookmark = itemView.findViewById(R.id.img_near_bookmark);
            txt_near_topic = itemView.findViewById(R.id.txt_near_topic);
            txt_near_kg = itemView.findViewById(R.id.txt_near_kg);
            txt_status_open = itemView.findViewById(R.id.txt_status_open);
            txt_near_category = itemView.findViewById(R.id.txt_near_category);
            txt_items_rating = itemView.findViewById(R.id.txt_items_rating);
            rl_bg_star = itemView.findViewById(R.id.rl_bg_star);
        }

    }
}
