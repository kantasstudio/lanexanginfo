package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.ItemsPackageTourClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ItemsPackageTourRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ItemsModel> arrayList;
    private String language;
    private ItemsPackageTourClickListener clickListener;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;

    public ItemsPackageTourRecyclerViewAdapter(Context context, ArrayList<ItemsModel> arrayList, String language,ItemsPackageTourClickListener clickListener, RateModel rateModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.clickListener = clickListener;
        this.rateModel = rateModel;
        exchangeRate = new ExchangeRate();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_package_tour, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ItemsModel itemsModel = (ItemsModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        Picasso.get().load(paths+itemsModel.getCoverItemsModelArrayList().get(0).getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).imgItemsCover);

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_title_package_tour.setText(itemsModel.getTopicTH());
                ((ItemViewHolder)holder).txt_type_activity.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_title_package_tour.setText(itemsModel.getTopicEN());
                ((ItemViewHolder)holder).txt_type_activity.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_title_package_tour.setText(itemsModel.getTopicLO());
                ((ItemViewHolder)holder).txt_type_activity.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_title_package_tour.setText(itemsModel.getTopicZH());
                ((ItemViewHolder)holder).txt_type_activity.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryZH());
                break;
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price  = "";
        for (int i = 0; i < itemsModel.getTravelPeriodModelArrayList().size() ; i++) {

            if (rateModel != null) {
                String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
                switch (currency) {
                    case "THB":
                        price = decimalFormat.format(itemsModel.getTravelPeriodModelArrayList().get(0).getAdultSpecialPrice());
                        ((ItemViewHolder) holder).txt_exchange_rate_id.setText(currency);
                        ((ItemViewHolder) holder).txt_price.setText(price);
                        break;
                    case "USD":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getTravelPeriodModelArrayList().get(0).getAdultSpecialPrice(),rateModel.getRateUSD()));
                        ((ItemViewHolder) holder).txt_exchange_rate_id.setText(currency);
                        ((ItemViewHolder) holder).txt_price.setText(price);
                        break;
                    case "CNY":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getTravelPeriodModelArrayList().get(0).getAdultSpecialPrice(),rateModel.getRateCNY()));
                        ((ItemViewHolder) holder).txt_exchange_rate_id.setText(currency);
                        ((ItemViewHolder) holder).txt_price.setText(price);
                        break;
                }
            }else {
                price = decimalFormat.format(itemsModel.getTravelPeriodModelArrayList().get(0).getAdultSpecialPrice());
                ((ItemViewHolder) holder).txt_price.setText(price);
            }
        }

        ((ItemViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.itemClickedItems(itemsModel);
            }
        });

        if (itemsModel.getBusinessModel() != null){
            switch (language){
                case "th":
                    ((ItemViewHolder)holder).txt_company.setText(itemsModel.getBusinessModel().getNameTH());
                    break;
                case "en":
                    ((ItemViewHolder)holder).txt_company.setText(itemsModel.getBusinessModel().getNameEN());
                    break;
                case "lo":
                    ((ItemViewHolder)holder).txt_company.setText(itemsModel.getBusinessModel().getNameLO());
                    break;
                case "zh":
                    ((ItemViewHolder)holder).txt_company.setText(itemsModel.getBusinessModel().getNameZH());
                    break;
            }
        }

        if (itemsModel.getProvincesModelArrayList() != null && itemsModel.getProvincesModelArrayList().size() != 0){
            String province = "";
            for (ProvincesModel pro : itemsModel.getProvincesModelArrayList())
                switch (language){
                    case "th":
                        province += pro.getProvincesTH()+" ";
                        break;
                    case "en":
                        province += pro.getProvincesEN()+" ";
                        break;
                    case "lo":
                        province += pro.getProvincesLO()+" ";
                        break;
                    case "zh":
                        province += pro.getProvincesZH()+" ";
                        break;
                }
            ((ItemViewHolder)holder).txt_mark_location.setText(province);
        }


        if (itemsModel.getTravelDetailsModelArrayList() != null && itemsModel.getTravelDetailsModelArrayList().size() != 0) {
            int day = itemsModel.getTravelDetailsModelArrayList().size();
            ((ItemViewHolder)holder).txt_period_title.setText(day+ " " +context.getResources().getString(R.string.text_day) + " "
                    + (day - 1) + " " + context.getResources().getString(R.string.text_night));
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgItemsCover;
        private TextView txt_title_package_tour, txt_price, txt_company, txt_details,txt_mark_location;
        private TextView txt_exchange_rate_id;
        private TextView txt_type_activity;
        private TextView txt_period_title;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItemsCover = itemView.findViewById(R.id.img_package_tour);
            txt_title_package_tour = itemView.findViewById(R.id.txt_title_package_tour);
            txt_price = itemView.findViewById(R.id.txt_price);
            txt_company = itemView.findViewById(R.id.txt_company);
            txt_details = itemView.findViewById(R.id.txt_details);
            txt_mark_location = itemView.findViewById(R.id.txt_mark_location);
            txt_exchange_rate_id = itemView.findViewById(R.id.txt_exchange_rate_id);
            txt_type_activity = itemView.findViewById(R.id.txt_type_activity);
            txt_period_title = itemView.findViewById(R.id.txt_period);
        }

    }
}
