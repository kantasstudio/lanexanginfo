package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class ItemsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ItemsModel> itemsModelArrayList;

    public ItemsRecyclerViewAdapter(Context context, ArrayList<ItemsModel> itemsModelArrayList) {
        this.context = context;
        this.itemsModelArrayList = itemsModelArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_view2, parent, false);
        return new TourismViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ItemsModel itemsModel = (ItemsModel) itemsModelArrayList.get(position);

        ((TourismViewHolder)holder).txtItemsTopic.setText(itemsModel.getTopicTH());

    }

    @Override
    public int getItemCount() {
        return itemsModelArrayList.size();
    }



    public class TourismViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgItemsCover, imgItemsBookMarks;
        private TextView txtItemsTopic,txtItemsContact, txtItemsSubcategory, txtItemsRating, txtItemsOpenClose, txtItemsKG;
        private RelativeLayout rlBackGroundStar;

        public TourismViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItemsCover = itemView.findViewById(R.id.img_cover_items);
            imgItemsBookMarks = itemView.findViewById(R.id.img_items_bookmarks);
            txtItemsTopic = itemView.findViewById(R.id.txt_items_topic);
            txtItemsContact = itemView.findViewById(R.id.txt_items_contact);
            txtItemsSubcategory = itemView.findViewById(R.id.txt_items_subcategory);
            txtItemsRating = itemView.findViewById(R.id.txt_items_rating);
            txtItemsOpenClose = itemView.findViewById(R.id.txt_items_open_close);
            txtItemsKG = itemView.findViewById(R.id.txt_items_kg);
            rlBackGroundStar = itemView.findViewById(R.id.rl_bg_star);
        }
    }

    public class HotelViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgItemsCover, imgItemsBookMarks;
        private TextView txtItemsTopic,txtItemsContact, txtItemsRating,txtItemsReview, txtItemsPrice, txtItemsKG;
        private RelativeLayout rlBackGroundStar;

        public HotelViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItemsCover = itemView.findViewById(R.id.img_cover_items);
            imgItemsBookMarks = itemView.findViewById(R.id.img_items_bookmarks);
            txtItemsTopic = itemView.findViewById(R.id.txt_items_topic);
            txtItemsContact = itemView.findViewById(R.id.txt_items_contact);
            txtItemsRating = itemView.findViewById(R.id.txt_items_rating);
            txtItemsReview = itemView.findViewById(R.id.txt_items_review);
            txtItemsPrice = itemView.findViewById(R.id.txt_items_price);
            txtItemsKG = itemView.findViewById(R.id.txt_items_kg);
            rlBackGroundStar = itemView.findViewById(R.id.rl_bg_star);
        }

    }
}
