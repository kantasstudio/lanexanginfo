package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MultipleImagePagerAdapter extends androidx.viewpager.widget.PagerAdapter {

    private Context mContext;
    private ArrayList<CoverItemsModel> arrayList;


    public MultipleImagePagerAdapter(Context mContext, ArrayList<CoverItemsModel> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        final CoverItemsModel itemModel = (CoverItemsModel) arrayList.get(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_cover_image_view,null);

        ImageView image = view.findViewById(R.id.img_cover_image_id);
        ImageView foreground = view.findViewById(R.id.img_foreground);
        foreground.setVisibility(View.GONE);

        String paths = mContext.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+itemModel.getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(image);


        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }


}
