package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class MultipleImageRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<CoverItemsModel> arrayList;


    public MultipleImageRecyclerViewAdapter(Context context, ArrayList<CoverItemsModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cover_image_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final CoverItemsModel itemsModel = (CoverItemsModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

//        Picasso.get().load(paths+itemsModel.getCoverItemsModelArrayList().get(0).getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).imgItemsCover);




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgItemsCover;
        private TextView txt_title_package_tour, txt_price, txt_company, txt_details;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItemsCover = itemView.findViewById(R.id.img_package_tour);
            txt_title_package_tour = itemView.findViewById(R.id.txt_title_package_tour);
            txt_price = itemView.findViewById(R.id.txt_price);
            txt_company = itemView.findViewById(R.id.txt_company);
            txt_details = itemView.findViewById(R.id.txt_details);
        }

    }
}
