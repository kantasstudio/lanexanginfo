package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.MyProgramTourClickListener;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyProgramTourRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ProgramTourModel> arrayList;
    private String language;
    private MyProgramTourClickListener listener;


    public MyProgramTourRecyclerViewAdapter(Context context, ArrayList<ProgramTourModel> arrayList, String language, MyProgramTourClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_programtour_view3, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ProgramTourModel items = (ProgramTourModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+items.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).img_item_program_tour);

        Picasso.get().load(items.getUserModel().getProfilePicUrl()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).img_profile);
        ((ItemViewHolder)holder).txt_profile_full_name.setText(items.getUserModel().getFirstName()+" "+items.getUserModel().getLastName());

        ((ItemViewHolder)holder).txt_create_date_time.setText(items.getCreatedDTTM());


        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_title_program_tour.setText(" : "+items.getProgramTourNameTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_title_program_tour.setText(" : "+items.getProgramTourNameEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_title_program_tour.setText(" : "+items.getProgramTourNameLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_title_program_tour.setText(" : "+items.getProgramTourNameZH());
                break;
        }


        ((ItemViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClicked(items);
            }
        });

        ((ItemViewHolder)holder).btn_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClockEventMore(view, items);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private void onClockEventMore(View v, ProgramTourModel programTour) {
        PopupMenu menu = new PopupMenu(context,v);
        menu.getMenuInflater().inflate(R.menu.menu_more,menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_edit:
                        listener.itemClickedMore(programTour, "edit");
                        break;
                    case R.id.action_delete:
                        listener.itemClickedMore(programTour, "delete");
                        break;
                }

                return true;
            }
        });
        menu.show();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private ImageView img_item_program_tour;
        private ImageView img_profile;
        private ImageView btn_more;
        private TextView txt_profile_full_name;
        private TextView txt_title_program_tour;
        private TextView txt_create_date_time;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            img_item_program_tour = itemView.findViewById(R.id.img_item_program_tour);
            img_profile = itemView.findViewById(R.id.img_profile);
            btn_more = itemView.findViewById(R.id.btn_more);
            txt_profile_full_name = itemView.findViewById(R.id.txt_profile_full_name);
            txt_title_program_tour = itemView.findViewById(R.id.txt_title_program_tour);
            txt_create_date_time = itemView.findViewById(R.id.txt_create_date_time);


        }

    }
}
