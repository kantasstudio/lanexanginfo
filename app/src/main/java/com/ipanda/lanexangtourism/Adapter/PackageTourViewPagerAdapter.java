package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.ipanda.lanexangtourism.Interface_click.ScreenPackageTourClickListener;
import com.ipanda.lanexangtourism.Model.ScreenPackageTourModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PackageTourViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<ScreenPackageTourModel> mListScreen;
    private ScreenPackageTourClickListener listener;


    public PackageTourViewPagerAdapter(Context mContext, ArrayList<ScreenPackageTourModel> mListScreen, ScreenPackageTourClickListener listener) {
        this.mContext = mContext;
        this.mListScreen = mListScreen;
        this.listener = listener;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        final ScreenPackageTourModel screen = (ScreenPackageTourModel) mListScreen.get(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = inflater.inflate(R.layout.layout_item_package_tour_view,null);

        ImageView imgSlide = layoutScreen.findViewById(R.id.package_tour_img);
        String paths = mContext.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/"+screen.getScreenImage();
        Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(imgSlide);

        layoutScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClickedScreenPackageTour(screen.getItemsModel());
            }
        });

        container.addView(layoutScreen);
        return layoutScreen;
    }

    @Override
    public int getCount() {
        return mListScreen.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }


}
