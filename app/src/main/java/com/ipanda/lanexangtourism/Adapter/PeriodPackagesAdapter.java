package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class PeriodPackagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<BookingModel> arrayList;
    private String language;
    private RateModel rateModel;

    public PeriodPackagesAdapter(Context context, ArrayList<BookingModel> arrayList, String language, RateModel rateModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.rateModel = rateModel;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_booking_details_number_of_travelers, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final BookingModel bookingModel = (BookingModel) arrayList.get(position);
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        ExchangeRate exchangeRate = new ExchangeRate();
            if (bookingModel.getSpecialAdult() != 0){
                String pricePerPerson = null, priceTotal = null;
                if (rateModel != null) {
                    String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
                    switch (currency) {
                        case "THB":
                            pricePerPerson = decimalFormat.format(bookingModel.getPriceSpecialAdult());
                            priceTotal = decimalFormat.format(bookingModel.getPriceSpecialAdult() * bookingModel.getSpecialAdult());
                            ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                            ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                            ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getSpecialAdult()+" "+context.getResources().getString(R.string.text_sir));
                            ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                            break;
                        case "USD":
                            pricePerPerson = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceSpecialAdult(),rateModel.getRateUSD()));
                            priceTotal = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceSpecialAdult() * bookingModel.getSpecialAdult(),rateModel.getRateUSD()));
                            ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                            ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                            ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getSpecialAdult()+" "+context.getResources().getString(R.string.text_sir));
                            ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                            break;
                        case "CNY":
                            pricePerPerson = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceSpecialAdult(),rateModel.getRateCNY()));
                            priceTotal = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceSpecialAdult() * bookingModel.getSpecialAdult(),rateModel.getRateCNY()));
                            ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                            ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                            ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getSpecialAdult()+" "+context.getResources().getString(R.string.text_sir));
                            ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                            break;
                    }
                }else {
                    pricePerPerson = decimalFormat.format(bookingModel.getPriceSpecialAdult());
                    priceTotal = decimalFormat.format(bookingModel.getPriceSpecialAdult() * bookingModel.getSpecialAdult());
                    ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                    ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                    ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getSpecialAdult()+" "+context.getResources().getString(R.string.text_sir));
                    ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                }
            }
            if (bookingModel.getGuestAdult() != 0){
                String pricePerPerson = null, priceTotal = null;
                if (rateModel != null) {
                    String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
                    switch (currency) {
                        case "THB":
                            pricePerPerson = decimalFormat.format(bookingModel.getPriceAdult());
                            priceTotal = decimalFormat.format(bookingModel.getPriceAdult() * bookingModel.getGuestAdult());
                            ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                            ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                            ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getGuestAdult()+" "+context.getResources().getString(R.string.text_sir));
                            ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                            break;
                        case "USD":
                            pricePerPerson = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceAdult(),rateModel.getRateUSD()));
                            priceTotal = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceAdult() * bookingModel.getGuestAdult(),rateModel.getRateUSD()));
                            ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                            ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                            ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getGuestAdult()+" "+context.getResources().getString(R.string.text_sir));
                            ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                            break;
                        case "CNY":
                            pricePerPerson = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceAdult(),rateModel.getRateCNY()));
                            priceTotal = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceAdult() * bookingModel.getGuestAdult(),rateModel.getRateCNY()));
                            ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                            ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                            ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getGuestAdult()+" "+context.getResources().getString(R.string.text_sir));
                            ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                            break;
                    }
                }else {
                    pricePerPerson = decimalFormat.format(bookingModel.getPriceAdult());
                    priceTotal = decimalFormat.format(bookingModel.getPriceAdult() * bookingModel.getGuestAdult());
                    ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                    ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                    ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getGuestAdult()+" "+context.getResources().getString(R.string.text_sir));
                    ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                }
            }
            if (bookingModel.getSpecialChild() != 0){
                String pricePerPerson = null, priceTotal = null;
                if (rateModel != null) {
                    String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
                    switch (currency) {
                        case "THB":
                            pricePerPerson = decimalFormat.format(bookingModel.getPriceSpecialChild());
                            priceTotal = decimalFormat.format(bookingModel.getPriceSpecialChild() * bookingModel.getSpecialChild());
                            ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                            ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                            ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getSpecialChild()+" "+context.getResources().getString(R.string.text_sir));
                            ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                            break;
                        case "USD":
                            pricePerPerson = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceSpecialChild(),rateModel.getRateUSD()));
                            priceTotal = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceSpecialChild() * bookingModel.getSpecialChild(),rateModel.getRateUSD()));
                            ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                            ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                            ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getSpecialChild()+" "+context.getResources().getString(R.string.text_sir));
                            ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                            break;
                        case "CNY":
                            pricePerPerson = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceSpecialChild(),rateModel.getRateCNY()));
                            priceTotal = decimalFormat.format(exchangeRate.getExchangeRateUSD(bookingModel.getPriceSpecialChild() * bookingModel.getSpecialChild(),rateModel.getRateCNY()));
                            ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                            ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                            ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getSpecialChild()+" "+context.getResources().getString(R.string.text_sir));
                            ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                            break;
                    }
                }else {
                    pricePerPerson = decimalFormat.format(bookingModel.getPriceSpecialChild());
                    priceTotal = decimalFormat.format(bookingModel.getPriceSpecialChild() * bookingModel.getSpecialChild());
                    ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                    ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                    ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getSpecialChild()+" "+context.getResources().getString(R.string.text_sir));
                    ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
                }

            }
            if (bookingModel.getGuestChild() != 0){
                String pricePerPerson = decimalFormat.format(bookingModel.getPriceChild());
                String priceTotal = decimalFormat.format(bookingModel.getPriceChild() * bookingModel.getGuestChild());
                ((ItemViewHolder)holder).txt_traveler.setText(bookingModel.getTraveler());
                ((ItemViewHolder)holder).txt_price_per_person.setText(pricePerPerson);
                ((ItemViewHolder)holder).txt_amount.setText(bookingModel.getGuestChild()+" "+context.getResources().getString(R.string.text_sir));
                ((ItemViewHolder)holder).txt_total_price.setText(priceTotal);
            }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_traveler, txt_price_per_person, txt_amount, txt_total_price;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_traveler = itemView.findViewById(R.id.txt_traveler);
            txt_price_per_person = itemView.findViewById(R.id.txt_price_per_person);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            txt_total_price = itemView.findViewById(R.id.txt_total_price);
        }

    }
}
