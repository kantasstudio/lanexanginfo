package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.PhotoTourClickListener;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PhotoTouristAdapterRecyclerView extends RecyclerView.Adapter<PhotoTouristAdapterRecyclerView.ItemViewHolder> {

    private Context context;
    private ArrayList<PhotoTourist> photoTouristArrayList;
    private PhotoTourClickListener clickListener;
    private String language;


    public PhotoTouristAdapterRecyclerView(Context context, ArrayList<PhotoTourist> photoTouristArrayList, PhotoTourClickListener clickListener, String language) {
        this.context = context;
        this.photoTouristArrayList = photoTouristArrayList;
        this.clickListener = clickListener;
        this.language = language;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_programtour_photo_view, parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final PhotoTourist photoTourist = (PhotoTourist) photoTouristArrayList.get(position);

        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";




        switch (language) {
            case "th":
                ((ItemViewHolder)holder).txt_photo_details.setText(photoTourist.getPhotoTouristTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_photo_details.setText(photoTourist.getPhotoTouristEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_photo_details.setText(photoTourist.getPhotoTouristLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_photo_details.setText(photoTourist.getPhotoTouristZH());
                break;
        }

        Picasso.get().load(paths + photoTourist.getPhotoTouristPaths()).placeholder(R.drawable.img_placeholder)
                .error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).img_photo);

        ((ItemViewHolder)holder).img_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.itemClicked(position,photoTouristArrayList);
            }
        });

    }

    @Override
    public int getItemCount() {
        return photoTouristArrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView txt_photo_details;
        ImageView img_photo;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_photo_details = itemView.findViewById(R.id.txt_photo_details);
            img_photo = itemView.findViewById(R.id.img_photo);

        }
    }
}
