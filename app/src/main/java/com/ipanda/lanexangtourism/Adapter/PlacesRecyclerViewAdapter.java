package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.AddPhotoClickListener;
import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.Model.TouristAttractionsModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PlacesRecyclerViewAdapter extends RecyclerView.Adapter<PlacesRecyclerViewAdapter.PlacesViewHolder>{

    private Context context;
    private ArrayList<TouristAttractionsModel> arrayList;
    private ArrayList<TouristAttractionsModel> returnTouristAttractionsArrayList;
    private String language;
    private AddPhotoClickListener listener;
    private AddPhotoDatesTripAdapter mPhotoAdapter;
    private DatesTripModel tripModel;

    public PlacesRecyclerViewAdapter(Context context, ArrayList<TouristAttractionsModel> arrayList,String language,AddPhotoClickListener listener, DatesTripModel tripModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.returnTouristAttractionsArrayList = new ArrayList<>(arrayList);
        this.language = language;
        this.listener = listener;
        this.tripModel = tripModel;
    }

    @NonNull
    @Override
    public PlacesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_places_list_view, parent,false);
        return new PlacesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesViewHolder holder, int position) {
        final TouristAttractionsModel attractions = (TouristAttractionsModel) arrayList.get(position);
        final TouristAttractionsModel returnTourist = (TouristAttractionsModel) returnTouristAttractionsArrayList.get(position);

        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+attractions.getItemsModel().getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((PlacesViewHolder)holder).imgItemsCover);

        switch (language) {
            case "th":
                ((PlacesViewHolder)holder).txtItemsTopic.setText(attractions.getItemsModel().getTopicTH());
                ((PlacesViewHolder)holder).txtItemsPlaces.setText(attractions.getTitleTH());
                ((PlacesViewHolder)holder).edt_item_input_places_id.setText(attractions.getDetailTH());
                ((PlacesViewHolder)holder).txtItemsAddress.setText(attractions.getItemsModel().getContactTH());
                break;
            case "en":
                ((PlacesViewHolder)holder).txtItemsTopic.setText(attractions.getItemsModel().getTopicEN());
                ((PlacesViewHolder)holder).txtItemsPlaces.setText(attractions.getTitleEN());
                ((PlacesViewHolder)holder).edt_item_input_places_id.setText(attractions.getDetailEN());
                ((PlacesViewHolder)holder).txtItemsAddress.setText(attractions.getItemsModel().getContactEN());
                break;
            case "lo":
                ((PlacesViewHolder)holder).txtItemsTopic.setText(attractions.getItemsModel().getTopicLO());
                ((PlacesViewHolder)holder).txtItemsPlaces.setText(attractions.getTitleLO());
                ((PlacesViewHolder)holder).edt_item_input_places_id.setText(attractions.getDetailLO());
                ((PlacesViewHolder)holder).txtItemsAddress.setText(attractions.getItemsModel().getContactLO());
                break;
            case "zh":
                ((PlacesViewHolder)holder).txtItemsTopic.setText(attractions.getItemsModel().getTopicZH());
                ((PlacesViewHolder)holder).txtItemsPlaces.setText(attractions.getTitleZH());
                ((PlacesViewHolder)holder).edt_item_input_places_id.setText(attractions.getDetailZH());
                ((PlacesViewHolder)holder).txtItemsAddress.setText(attractions.getItemsModel().getContactZH());
                break;
        }
        if (attractions.getItemsModel().getPeriodDayModel() != null && attractions.getItemsModel().getPeriodTimeModel() != null) {
            if (attractions.getItemsModel().getPeriodDayModel().getPeriodOpenId() == attractions.getItemsModel().getPeriodDayModel().getPeriodCloseId()) {
                switch (language) {
                    case "th":
                        ((PlacesViewHolder) holder).txtOpenEveryday.setText(attractions.getItemsModel().getPeriodDayModel().getPeriodOpenTH());
                        break;
                    case "en":
                        ((PlacesViewHolder) holder).txtOpenEveryday.setText(attractions.getItemsModel().getPeriodDayModel().getPeriodOpenEN());
                        break;
                    case "lo":
                        ((PlacesViewHolder) holder).txtOpenEveryday.setText(attractions.getItemsModel().getPeriodDayModel().getPeriodOpenLO());
                        break;
                    case "zh":
                        ((PlacesViewHolder) holder).txtOpenEveryday.setText(attractions.getItemsModel().getPeriodDayModel().getPeriodOpenZH());
                        break;
                }
            } else {
                switch (language) {
                    case "th":
                        ((PlacesViewHolder) holder).txtOpenEveryday.setText(attractions.getItemsModel().getPeriodDayModel().getPeriodOpenTH() + " - " + attractions.getItemsModel().getPeriodDayModel().getPeriodCloseTH());
                        break;
                    case "en":
                        ((PlacesViewHolder) holder).txtOpenEveryday.setText(attractions.getItemsModel().getPeriodDayModel().getPeriodOpenEN() + " - " + attractions.getItemsModel().getPeriodDayModel().getPeriodCloseEN());
                        break;
                    case "lo":
                        ((PlacesViewHolder) holder).txtOpenEveryday.setText(attractions.getItemsModel().getPeriodDayModel().getPeriodOpenLO() + " - " + attractions.getItemsModel().getPeriodDayModel().getPeriodCloseLO());
                        break;
                    case "zh":
                        ((PlacesViewHolder) holder).txtOpenEveryday.setText(attractions.getItemsModel().getPeriodDayModel().getPeriodOpenZH() + " - " + attractions.getItemsModel().getPeriodDayModel().getPeriodCloseZH());
                        break;
                }
            }

        }

        ((PlacesViewHolder)holder).txtTimeOpenClose.setText(attractions.getItemsModel().getTimeOpen()+"-"+attractions.getItemsModel().getTimeClose());
        ((PlacesViewHolder)holder).txtItemPhone.setText(attractions.getItemsModel().getPhone());

        ((PlacesViewHolder)holder).txtItemCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (attractions.getItemsModel().getItemsDetailModelArrayList() != null && attractions.getItemsModel().getItemsDetailModelArrayList().size() != 0) {
                    ((PlacesViewHolder)holder).edt_item_input_places_id.setText(attractions.getItemsModel().getItemsDetailModelArrayList().get(1).getDetailTH());
                }

            }
        });

        ((PlacesViewHolder)holder).btn_cv_selected_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickedPhoto(tripModel,attractions);
            }
        });

        ((PlacesViewHolder)holder).edt_item_input_places_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String convert  = charSequence.toString();

                switch (language){
                    case "th":
                        attractions.setDetailTH(convert);
                        break;
                    case "en":
                        attractions.setDetailEN(convert);
                        break;
                    case "lo":
                        attractions.setDetailLO(convert);
                        break;
                    case "zh":
                        attractions.setDetailZH(convert);
                        break;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        if (attractions.getPhotoTouristArrayList() != null && attractions.getPhotoTouristArrayList().size() != 0){
            mPhotoAdapter = new AddPhotoDatesTripAdapter(context, attractions.getPhotoTouristArrayList());
            ((PlacesViewHolder)holder).recycler_item_image_id.setLayoutManager(new LinearLayoutManager(context,RecyclerView.VERTICAL, false));
            ((PlacesViewHolder)holder).recycler_item_image_id.setAdapter(mPhotoAdapter);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class PlacesViewHolder extends RecyclerView.ViewHolder{

        TextView txtItemsPlaces;
        ImageView imgItemsCover;
        TextView txtItemsTopic;
        TextView txtItemsAddress;
        TextView txtOpenEveryday;
        TextView txtTimeOpenClose;
        TextView txtItemPhone;
        TextView txtItemCopy;
        CardView btn_cv_selected_photo;
        RecyclerView recycler_item_image_id;
        EditText edt_item_input_places_id;

        public PlacesViewHolder(@NonNull View itemView) {
            super(itemView);

            txtItemsPlaces = itemView.findViewById(R.id.txt_item_places_id);
            imgItemsCover = itemView.findViewById(R.id.img_item_cover_id);
            txtItemsTopic = itemView.findViewById(R.id.txt_item_topic_id);
            txtItemsAddress = itemView.findViewById(R.id.txt_item_address_id);
            txtOpenEveryday = itemView.findViewById(R.id.txtOpenEveryday);
            txtTimeOpenClose = itemView.findViewById(R.id.txt_item_open_close_id);
            txtItemPhone = itemView.findViewById(R.id.txt_item_phone_id);
            txtItemCopy = itemView.findViewById(R.id.txt_item_copy_id);
            btn_cv_selected_photo = itemView.findViewById(R.id.btn_cv_selected_photo);
            recycler_item_image_id = itemView.findViewById(R.id.recycler_item_image_id);
            edt_item_input_places_id = itemView.findViewById(R.id.edt_item_input_places_id);

        }


    }

    public ArrayList<TouristAttractionsModel> returnTouristAttractionsArrayList(){
        return returnTouristAttractionsArrayList;
    }


}
