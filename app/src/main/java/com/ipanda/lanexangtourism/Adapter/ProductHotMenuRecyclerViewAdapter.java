package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.ClickListener.ItemImageClickListener;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.ProductPhotoModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ProductHotMenuRecyclerViewAdapter extends RecyclerView.Adapter<ProductHotMenuRecyclerViewAdapter.ProductHotMenuViewHolder>{

    private Context context;
    private ArrayList<ProductModel> arrayList;
    private ItemImageClickListener listener;
    private String language;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;

    public ProductHotMenuRecyclerViewAdapter(Context context, ArrayList<ProductModel> arrayList, ItemImageClickListener listener, String language, RateModel rateModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
        this.rateModel = rateModel;
        exchangeRate = new ExchangeRate();
    }

    @NonNull
    @Override
    public ProductHotMenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_description_hotmenu_view, parent,false);
        return new ProductHotMenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHotMenuViewHolder holder, int position) {
        final ProductModel itemsModel = (ProductModel) arrayList.get(position);

        for (ProductPhotoModel photo: itemsModel.getProductPhotoModelArrayList()) {
            Picasso.get().load("http://203.154.71.91/dasta_thailand/assets/img/uploadfile/"+photo.getProductPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ProductHotMenuViewHolder)holder).img_hot_menu_id);
        }

        switch (language){
            case "th":
                ((ProductHotMenuViewHolder)holder).txt_hot_name_id.setText(itemsModel.getProductNamesTH());
                break;
            case "en":
                ((ProductHotMenuViewHolder)holder).txt_hot_name_id.setText(itemsModel.getProductNamesEN());
                break;
            case "lo":
                ((ProductHotMenuViewHolder)holder).txt_hot_name_id.setText(itemsModel.getProductNamesLO());
                break;
            case "zh":
                ((ProductHotMenuViewHolder)holder).txt_hot_name_id.setText(itemsModel.getProductNamesZH());
                break;
        }


        ((ProductHotMenuViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClickedHotMenu(position);
            }
        });

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String price = null;

        if (rateModel != null) {
            String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(itemsModel.getProductPrice());
                    ((ProductHotMenuViewHolder) holder).txt_hot_price_id.setText(currency+" "+price);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getProductPrice(),rateModel.getRateUSD()));
                    ((ProductHotMenuViewHolder) holder).txt_hot_price_id.setText(currency+" "+price);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getProductPrice(),rateModel.getRateCNY()));
                    ((ProductHotMenuViewHolder) holder).txt_hot_price_id.setText(currency+" "+price);
                    break;
            }
        }else {
            price = decimalFormat.format(itemsModel.getProductPrice());
            ((ProductHotMenuViewHolder) holder).txt_hot_price_id.setText(price);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ProductHotMenuViewHolder extends RecyclerView.ViewHolder{
        ImageView img_hot_menu_id;
        TextView txt_hot_name_id, txt_hot_price_id;
        public ProductHotMenuViewHolder(@NonNull View itemView) {
            super(itemView);
            img_hot_menu_id = itemView.findViewById(R.id.img_hot_menu_id);
            txt_hot_name_id = itemView.findViewById(R.id.txt_hot_name_id);
            txt_hot_price_id = itemView.findViewById(R.id.txt_hot_price_id);
        }
    }


}
