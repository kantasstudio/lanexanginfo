package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ProductModel> arrayList;
    private String language;
    private TextView txtTotals;
    private TextView txt_exchange_rate_detail;
    private double totals;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;

    public ProductListAdapter(Context context, ArrayList<ProductModel> arrayList, String language, TextView txtTotals, RateModel rateModel,TextView txt_exchange_rate_detail) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.txtTotals = txtTotals;
        this.rateModel = rateModel;
        this.txt_exchange_rate_detail = txt_exchange_rate_detail;
        exchangeRate = new ExchangeRate();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_product_detail_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ProductModel product = (ProductModel) arrayList.get(position);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_product_name_id.setText(product.getProductNamesTH());
                ((ItemViewHolder)holder).txt_product_item_id.setText(product.getProductOrderDetails().getQuantity()+" ชุด");
                break;
            case "en":
                ((ItemViewHolder)holder).txt_product_name_id.setText(product.getProductNamesEN());
                ((ItemViewHolder)holder).txt_product_item_id.setText(product.getProductOrderDetails().getQuantity()+" set");
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_product_name_id.setText(product.getProductNamesLO());
                ((ItemViewHolder)holder).txt_product_item_id.setText(product.getProductOrderDetails().getQuantity()+" ຕັ້ງ");
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_product_name_id.setText(product.getProductNamesZH());
                ((ItemViewHolder)holder).txt_product_item_id.setText(product.getProductOrderDetails().getQuantity()+" 组");
                break;
        }

        totals += product.getProductOrderDetails().getTotalPrice();

        String price  = null;
        String summary = null;



        String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");

        if (rateModel != null) {
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(product.getProductOrderDetails().getTotalPrice());
                    summary = decimalFormat.format(totals);
                    ((ItemViewHolder) holder).txt_product_price_id.setText(price);
                    ((ItemViewHolder) holder).txt_exchange_rate_id.setText(currency);
                    txtTotals.setText(summary);
                    txt_exchange_rate_detail.setText(currency);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(product.getProductOrderDetails().getTotalPrice(),rateModel.getRateUSD()));
                    summary = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                    ((ItemViewHolder) holder).txt_product_price_id.setText(price);
                    ((ItemViewHolder) holder).txt_exchange_rate_id.setText(currency);
                    txtTotals.setText(summary);
                    txt_exchange_rate_detail.setText(currency);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(product.getProductOrderDetails().getTotalPrice(),rateModel.getRateCNY()));
                    summary = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                    ((ItemViewHolder) holder).txt_product_price_id.setText(price);
                    ((ItemViewHolder) holder).txt_exchange_rate_id.setText(currency);
                    txtTotals.setText(summary);
                    txt_exchange_rate_detail.setText(currency);
                    break;
            }
        }else {
            price = decimalFormat.format(product.getProductOrderDetails().getTotalPrice());
            summary = decimalFormat.format(totals);
            ((ItemViewHolder) holder).txt_product_price_id.setText(price);
            txtTotals.setText(summary);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_product_name_id;
        private TextView txt_product_item_id;
        private TextView txt_product_price_id;
        private TextView txt_exchange_rate_id;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_product_name_id = itemView.findViewById(R.id.txt_product_name_id);
            txt_product_item_id = itemView.findViewById(R.id.txt_product_item_id);
            txt_product_price_id = itemView.findViewById(R.id.txt_product_price_id);
            txt_exchange_rate_id = itemView.findViewById(R.id.txt_exchange_rate_id);
        }

    }
}
