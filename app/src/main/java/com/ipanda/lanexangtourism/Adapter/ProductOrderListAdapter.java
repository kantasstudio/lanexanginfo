package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.OrderDetailsClickListener;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class ProductOrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<BookingModel> arrayList;
    private String language;
    private OrderDetailsClickListener listener;

    public ProductOrderListAdapter(Context context, ArrayList<BookingModel> arrayList, String language,OrderDetailsClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_product_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final BookingModel booking = (BookingModel) arrayList.get(position);

        int count =  booking.getProductModelArrayList().size();
        ((ItemViewHolder)holder).txt_product_item_id.setText(count+" "+ context.getResources().getString(R.string.text_list));

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_product_name_id.setText(booking.getItemsModel().getTopicTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_product_name_id.setText(booking.getItemsModel().getTopicEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_product_name_id.setText(booking.getItemsModel().getTopicLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_product_name_id.setText(booking.getItemsModel().getTopicZH());
                break;
        }

        if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 1){
            ((ItemViewHolder)holder).img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_booked));
            ((ItemViewHolder)holder).txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_booked));
        }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 2){
            ((ItemViewHolder)holder).img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_wait));
            ((ItemViewHolder)holder).txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_wait));
        }else if (booking.getPaymentTransactionModel().getPurchaseStatusModel().getStatusId() == 3){
            ((ItemViewHolder)holder).img_booking_status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_status_refuse));
            ((ItemViewHolder)holder).txt_booking_status.setTextColor(context.getResources().getColor(R.color.status_refuse));
        }else {

        }


        ((ItemViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickedOrder(booking);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private ImageView img_product_id;
        private ImageView img_booking_status;
        private TextView txt_product_name_id, txt_product_item_id, txt_booking_status;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            img_product_id = itemView.findViewById(R.id.img_product_id);
            img_booking_status = itemView.findViewById(R.id.img_booking_status);
            txt_product_name_id = itemView.findViewById(R.id.txt_product_name_id);
            txt_product_item_id = itemView.findViewById(R.id.txt_product_item_id);
            txt_booking_status = itemView.findViewById(R.id.txt_booking_status);
        }

    }
}
