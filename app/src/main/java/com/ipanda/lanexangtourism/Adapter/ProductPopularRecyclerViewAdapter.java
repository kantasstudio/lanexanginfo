package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.ClickListener.ItemImageClickListener;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.PurchaseOrder;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.ProductOrderDetails;
import com.ipanda.lanexangtourism.Model.ProductPhotoModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ProductPopularRecyclerViewAdapter extends RecyclerView.Adapter<ProductPopularRecyclerViewAdapter.ProductPopularViewHolder>{

    private Context context;
    private ArrayList<ProductModel> arrayList;
    private ItemImageClickListener listener;
    private String language;
    private TextView txt_popular_total_id;
    private double totals = 0;
    private PurchaseOrder purchaseOrder;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;
    private TextView txt_exchange_rate_id;

    public ProductPopularRecyclerViewAdapter(Context context, ArrayList<ProductModel> arrayList, ItemImageClickListener listener, String language, TextView txt_popular_total_id,PurchaseOrder purchaseOrder, RateModel rateModel, TextView txt_exchange_rate_id) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
        this.txt_popular_total_id = txt_popular_total_id;
        this.purchaseOrder = purchaseOrder;
        this.rateModel = rateModel;
        this.txt_exchange_rate_id = txt_exchange_rate_id;
        exchangeRate = new ExchangeRate();
    }

    @NonNull
    @Override
    public ProductPopularViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_description_popularproducts_view, parent,false);
        return new ProductPopularViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductPopularViewHolder holder, int position) {
        final ProductModel itemsModel = (ProductModel) arrayList.get(position);
        ProductOrderDetails orderDetails = new ProductOrderDetails();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String price = null;

        for (ProductPhotoModel photo: itemsModel.getProductPhotoModelArrayList()) {
            Picasso.get().load("http://203.154.71.91/dasta_thailand/assets/img/uploadfile/"+photo.getProductPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ProductPopularViewHolder)holder).img_popular_id);
        }
        ((ProductPopularViewHolder)holder).show_amount_product.setVisibility(View.GONE);
        String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");

        if (rateModel != null) {
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(itemsModel.getProductPrice());
                    ((ProductPopularViewHolder) holder).txt_popular_price_id.setText(currency+" "+price);
                    txt_exchange_rate_id.setText(currency);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getProductPrice(),rateModel.getRateUSD()));
                    ((ProductPopularViewHolder) holder).txt_popular_price_id.setText(currency+" "+price);
                    txt_exchange_rate_id.setText(currency);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getProductPrice(),rateModel.getRateCNY()));
                    ((ProductPopularViewHolder) holder).txt_popular_price_id.setText(currency+" "+price);
                    txt_exchange_rate_id.setText(currency);
                    break;
            }
        }else {
            price = decimalFormat.format(itemsModel.getProductPrice());
            ((ProductPopularViewHolder) holder).txt_popular_price_id.setText(price);
        }


        switch (language){
            case "th":
                ((ProductPopularViewHolder)holder).txt_popular_name_id.setText(itemsModel.getProductNamesTH());
                break;
            case "en":
                ((ProductPopularViewHolder)holder).txt_popular_name_id.setText(itemsModel.getProductNamesEN());
                break;
            case "lo":
                ((ProductPopularViewHolder)holder).txt_popular_name_id.setText(itemsModel.getProductNamesLO());
                break;
            case "zh":
                ((ProductPopularViewHolder)holder).txt_popular_name_id.setText(itemsModel.getProductNamesZH());
                break;
        }
        ((ProductPopularViewHolder)holder).img_popular_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClickedPopularProduct(position);
            }
        });

        ((ProductPopularViewHolder)holder).img_add_per_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProductPopularViewHolder)holder).order += 1;
                ((ProductPopularViewHolder)holder).txt_amount_products.setText(((ProductPopularViewHolder)holder).order+"");
                ((ProductPopularViewHolder)holder).show_amount_product.setVisibility(View.VISIBLE);
                totals += itemsModel.getProductPrice();
                String price = null;

                if (rateModel != null) {
                    switch (currency) {
                        case "THB":
                            price = decimalFormat.format(totals);
                            txt_popular_total_id.setText(currency+" "+price);
                            txt_exchange_rate_id.setText(currency);
                            break;
                        case "USD":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                            txt_popular_total_id.setText(currency+" "+price);
                            txt_exchange_rate_id.setText(currency);
                            break;
                        case "CNY":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateCNY()));
                            txt_popular_total_id.setText(currency+" "+price);
                            txt_exchange_rate_id.setText(currency);
                            break;
                    }
                }else {
                    price = decimalFormat.format(totals);
                    txt_popular_total_id.setText(price);
                }


                orderDetails.setQuantity(((ProductPopularViewHolder)holder).order);
                itemsModel.setProductOrderDetails(orderDetails);
                itemsModel.setSelected(true);

                purchaseOrder.getPopularOrderDetails(arrayList,itemsModel);
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ProductPopularViewHolder extends RecyclerView.ViewHolder{

        int order = 0;
        RelativeLayout show_amount_product;
        ImageView img_popular_id, img_add_per_order;
        TextView txt_amount_products, txt_popular_name_id, txt_popular_price_id;
        public ProductPopularViewHolder(@NonNull View itemView) {
            super(itemView);
            show_amount_product = itemView.findViewById(R.id.show_amount_product);
            img_popular_id = itemView.findViewById(R.id.img_popular_id);
            txt_amount_products = itemView.findViewById(R.id.txt_amount_products);
            txt_popular_name_id = itemView.findViewById(R.id.txt_popular_name_id);
            txt_popular_price_id = itemView.findViewById(R.id.txt_popular_price_id);
            img_add_per_order = itemView.findViewById(R.id.img_add_per_order);
        }
    }


}
