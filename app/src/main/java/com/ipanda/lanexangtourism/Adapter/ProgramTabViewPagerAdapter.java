package com.ipanda.lanexangtourism.Adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ipanda.lanexangtourism.Model.ProgramTourModel;

import java.util.ArrayList;

public class ProgramTabViewPagerAdapter extends FragmentPagerAdapter {

    //Variables
    private final ArrayList<Fragment> fragmentsList = new ArrayList<>();
    private final ArrayList<String> fragmentTitle = new ArrayList<>();

    public ProgramTabViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentsList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    public void addFragment(Fragment fragment,String title,ArrayList<ProgramTourModel> programTourModelArrayList){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("ProgramTourModel", programTourModelArrayList);
        fragment.setArguments(bundle);
        fragmentsList.add(fragment);
        fragmentTitle.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitle.get(position);
    }
}
