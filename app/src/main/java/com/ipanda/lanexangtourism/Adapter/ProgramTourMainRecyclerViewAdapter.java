package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ProgramTourClickListener;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProgramTourMainRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ProgramTourModel> arrayList;
    private String language;
    private ProgramTourClickListener listener;

    public ProgramTourMainRecyclerViewAdapter(Context context, ArrayList<ProgramTourModel> arrayList, String language,ProgramTourClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_programtour_view2, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ProgramTourModel items = (ProgramTourModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+items.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).img_item_program_tour);

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_title_program_tour.setText(items.getProgramTourNameTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_title_program_tour.setText(items.getProgramTourNameEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_title_program_tour.setText(items.getProgramTourNameLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_title_program_tour.setText(items.getProgramTourNameZH());
                break;
        }

        ((ItemViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClicked(items);
            }
        });


        Picasso.get().load(items.getUserModel().getProfilePicUrl()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).img_profile);
        ((ItemViewHolder)holder).txt_name_person.setText(items.getUserModel().getFirstName()+" "+items.getUserModel().getLastName());


        if (items.isOfficial()){
            ((ItemViewHolder)holder).rl_official.setVisibility(View.VISIBLE);
        }else {
            ((ItemViewHolder)holder).rl_official.setVisibility(View.GONE);
        }

        ((ItemViewHolder)holder).txt_time_stamp_program_tour.setText(items.getCreatedDTTM());

        ((ItemViewHolder)holder).txt_count_like.setText(context.getString(R.string.text_like)+" "+items.getLikesModel().getLikes());

        if (items.getLikesModel().isLikeState()){
            ((ItemViewHolder)holder).img_like.setImageResource(R.drawable.ic_like_red);
        }else {
            ((ItemViewHolder)holder).img_like.setImageResource(R.drawable.ic_like);
        }
        ((ItemViewHolder)holder).img_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickedLikeProgramTour(((ItemViewHolder)holder).img_like,((ItemViewHolder)holder).txt_count_like,items);
            }
        });

        if (items.getCountView() != null && !items.getCountView().equals("")){
            ((ItemViewHolder)holder).txt_count_bookmarks.setText(context.getString(R.string.text_read)+" "+items.getCountView());
        }else {
            ((ItemViewHolder)holder).txt_count_bookmarks.setText(context.getString(R.string.text_read)+" "+0);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private ImageView img_item_program_tour, img_profile, img_like, img_bookmarks;
        private TextView txt_title_program_tour,txt_time_stamp_program_tour,txt_name_person;
        private TextView txt_count_like, txt_count_bookmarks;
        private RelativeLayout rl_official;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            img_item_program_tour = itemView.findViewById(R.id.img_item_program_tour);
            img_profile = itemView.findViewById(R.id.img_profile);
            img_like = itemView.findViewById(R.id.img_like);
            img_bookmarks = itemView.findViewById(R.id.img_bookmarks);
            txt_title_program_tour = itemView.findViewById(R.id.txt_title_program_tour);
            txt_time_stamp_program_tour = itemView.findViewById(R.id.txt_time_stamp_program_tour);
            txt_name_person = itemView.findViewById(R.id.txt_name_person);
            txt_count_like = itemView.findViewById(R.id.txt_count_like);
            txt_count_bookmarks = itemView.findViewById(R.id.txt_count_bookmarks);
            rl_official = itemView.findViewById(R.id.rl_official);


        }

    }
}
