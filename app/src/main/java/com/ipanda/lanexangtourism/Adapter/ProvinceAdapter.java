package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ProvinceClickListener;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class ProvinceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ProvincesModel> arrayList;
    private String language;
    private ProvinceClickListener listener;

    public ProvinceAdapter(Context context, ArrayList<ProvincesModel> arrayList, String language,ProvinceClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_province_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ProvincesModel provinces = (ProvincesModel) arrayList.get(position);

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_province.setText(provinces.getProvincesTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_province.setText(provinces.getProvincesEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_province.setText(provinces.getProvincesLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_province.setText(provinces.getProvincesZH());
                break;
        }

        ((ItemViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickedProvince(provinces);
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private TextView txt_province;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_province = itemView.findViewById(R.id.txt_province);
        }

    }
}
