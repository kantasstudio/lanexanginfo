package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.ProductPhotoModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class PurchaseOrderImageAdapter extends RecyclerView.Adapter<PurchaseOrderImageAdapter.ProductPopularViewHolder>{

    private Context context;
    private ArrayList<ProductModel> arrayList;
    private String language;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;

    public PurchaseOrderImageAdapter(Context context, ArrayList<ProductModel> arrayList, String language,RateModel rateModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.rateModel = rateModel;
        exchangeRate = new ExchangeRate();
    }

    @NonNull
    @Override
    public ProductPopularViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_description_popularproducts_view, parent,false);
        return new ProductPopularViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductPopularViewHolder holder, int position) {
        final ProductModel product = (ProductModel) arrayList.get(position);

        for (ProductPhotoModel photo : product.getProductPhotoModelArrayList()) {
            Picasso.get().load("http://203.154.71.91/dasta_thailand/assets/img/uploadfile/" + photo.getProductPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ProductPopularViewHolder) holder).img_popular_id);
        }

        ((ProductPopularViewHolder) holder).show_amount_product.setVisibility(View.VISIBLE);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String price = null;

        String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
        if (rateModel != null) {
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(product.getProductPrice());
                    ((ProductPopularViewHolder) holder).txt_popular_price_id.setText(currency+" "+price);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(product.getProductPrice(),rateModel.getRateUSD()));
                    ((ProductPopularViewHolder) holder).txt_popular_price_id.setText(currency+" "+price);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(product.getProductPrice(),rateModel.getRateCNY()));
                    ((ProductPopularViewHolder) holder).txt_popular_price_id.setText(currency+" "+price);
                    break;
            }
        }else {
            price = decimalFormat.format(product.getProductPrice());
            ((ProductPopularViewHolder) holder).txt_popular_price_id.setText(price);
        }

        ((ProductPopularViewHolder) holder).txt_amount_products.setText(product.getProductOrderDetails().getQuantity() + "");

        switch (language) {
            case "th":
                ((ProductPopularViewHolder) holder).txt_popular_name_id.setText(product.getProductNamesTH());
                break;
            case "en":
                ((ProductPopularViewHolder) holder).txt_popular_name_id.setText(product.getProductNamesEN());
                break;
            case "lo":
                ((ProductPopularViewHolder) holder).txt_popular_name_id.setText(product.getProductNamesLO());
                break;
            case "zh":
                ((ProductPopularViewHolder) holder).txt_popular_name_id.setText(product.getProductNamesZH());
                break;
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ProductPopularViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout show_amount_product;
        ImageView img_popular_id, img_add_per_order;
        TextView txt_amount_products, txt_popular_name_id, txt_popular_price_id;
        public ProductPopularViewHolder(@NonNull View itemView) {
            super(itemView);
            show_amount_product = itemView.findViewById(R.id.show_amount_product);
            img_popular_id = itemView.findViewById(R.id.img_popular_id);
            txt_amount_products = itemView.findViewById(R.id.txt_amount_products);
            txt_popular_name_id = itemView.findViewById(R.id.txt_popular_name_id);
            txt_popular_price_id = itemView.findViewById(R.id.txt_popular_price_id);
            img_add_per_order = itemView.findViewById(R.id.img_add_per_order);
        }
    }


}
