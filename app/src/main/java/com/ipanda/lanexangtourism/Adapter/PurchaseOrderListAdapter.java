package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.interface_callback.ChangeOrderList;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class PurchaseOrderListAdapter extends RecyclerView.Adapter<PurchaseOrderListAdapter.ProductPopularViewHolder>{

    private Context context;
    private ArrayList<ProductModel> arrayList;
    private String language;
    private ChangeOrderList changeOrderList;
    private TextView txtOrderTotals;
    private double totals = 0;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;
    private TextView txt_exchange_rate;
    private String currency;

    public PurchaseOrderListAdapter(Context context, ArrayList<ProductModel> arrayList, String language, ChangeOrderList changeOrderList, TextView txt_order_totals, TextView txt_exchange_rate,RateModel rateModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.changeOrderList = changeOrderList;
        this.txtOrderTotals = txt_order_totals;
        this.txt_exchange_rate = txt_exchange_rate;
        this.rateModel = rateModel;
        exchangeRate = new ExchangeRate();

    }

    @NonNull
    @Override
    public ProductPopularViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_list_view, parent,false);
        return new ProductPopularViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductPopularViewHolder holder, int position) {
        final ProductModel product = (ProductModel) arrayList.get(position);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String price = null;

        ((ProductPopularViewHolder) holder).txt_amount_id.setText(product.getProductOrderDetails().getQuantity() + "");

        currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");

        if (rateModel != null) {
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(product.getProductPrice()*product.getProductOrderDetails().getQuantity());
                    ((ProductPopularViewHolder) holder).txt_price_id.setText(price);
                    ((ProductPopularViewHolder) holder).txt_exchange_rate_id.setText(currency);
                    txt_exchange_rate.setText(currency);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(product.getProductPrice()*product.getProductOrderDetails().getQuantity(),rateModel.getRateUSD()));
                    ((ProductPopularViewHolder) holder).txt_price_id.setText(price);
                    ((ProductPopularViewHolder) holder).txt_exchange_rate_id.setText(currency);
                    txt_exchange_rate.setText(currency);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(product.getProductPrice()*product.getProductOrderDetails().getQuantity(),rateModel.getRateCNY()));
                    ((ProductPopularViewHolder) holder).txt_price_id.setText(price);
                    ((ProductPopularViewHolder) holder).txt_exchange_rate_id.setText(currency);
                    txt_exchange_rate.setText(currency);
                    break;
            }
        }else {
            price = decimalFormat.format(product.getProductPrice()*product.getProductOrderDetails().getQuantity());
            ((ProductPopularViewHolder) holder).txt_price_id.setText(price);
        }


        ((ProductPopularViewHolder) holder).defaultValue = product.getProductOrderDetails().getQuantity();

        totals += product.getProductPrice()*product.getProductOrderDetails().getQuantity();

        switch (language) {
            case "th":
                ((ProductPopularViewHolder) holder).txt_product_id.setText(product.getProductNamesTH());
                break;
            case "en":
                ((ProductPopularViewHolder) holder).txt_product_id.setText(product.getProductNamesEN());
                break;
            case "lo":
                ((ProductPopularViewHolder) holder).txt_product_id.setText(product.getProductNamesLO());
                break;
            case "zh":
                ((ProductPopularViewHolder) holder).txt_product_id.setText(product.getProductNamesZH());
                break;
        }

        ((ProductPopularViewHolder) holder).btn_plus_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProductPopularViewHolder) holder).setPlus(1, product.getProductPrice());
                changeOrderList.changeOrderList(((ProductPopularViewHolder) holder).defaultValue,position);
            }
        });

        ((ProductPopularViewHolder) holder).btn_minus_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProductPopularViewHolder) holder).setMinus(1, product.getProductPrice());
                if (((ProductPopularViewHolder)holder).defaultValue == 0) {
//                    arrayList.remove(position);
//                    notifyDataSetChanged();
                    ((ProductPopularViewHolder)holder).isDeleted = true;
                    changeOrderList.changeDeletedOrderList(arrayList);
                }
                changeOrderList.changeOrderList(((ProductPopularViewHolder)holder).defaultValue,position);
            }
        });


        String priceTotals = null;
        if (rateModel != null) {
            switch (currency) {
                case "THB":
                    priceTotals = decimalFormat.format(totals);
                    txtOrderTotals.setText(priceTotals);
                    break;
                case "USD":
                    priceTotals = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                    txtOrderTotals.setText(priceTotals);
                    break;
                case "CNY":
                    priceTotals = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateCNY()));
                    txtOrderTotals.setText(priceTotals);
                    break;
            }
        }else {
            priceTotals = decimalFormat.format(totals);
            txtOrderTotals.setText(priceTotals);
        }



    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ProductPopularViewHolder extends RecyclerView.ViewHolder{
        int defaultValue = 0;
        double totals = 0;
        boolean isDeleted = false;
        ImageButton btn_minus_id;
        TextView txt_amount_id;
        ImageButton btn_plus_id;
        TextView txt_product_id;
        TextView txt_price_id;
        TextView txt_exchange_rate_id;

        public ProductPopularViewHolder(@NonNull View itemView) {
            super(itemView);
            btn_minus_id = itemView.findViewById(R.id.btn_minus_id);
            txt_amount_id = itemView.findViewById(R.id.txt_amount_id);
            btn_plus_id = itemView.findViewById(R.id.btn_plus_id);
            txt_product_id = itemView.findViewById(R.id.txt_product_id);
            txt_price_id = itemView.findViewById(R.id.txt_price_id);
            txt_exchange_rate_id = itemView.findViewById(R.id.txt_exchange_rate_id);
        }

        private void setPlus(int plus, double price) {
            defaultValue += plus;
            totals = (price * defaultValue);
            setValue();
        }

        private void setMinus(int minus, double price) {
            defaultValue -= minus;
            totals = (price * defaultValue);
            if (defaultValue <= 0){
                defaultValue = 0;
                totals = (price * defaultValue);
            }
            setValue();
        }

        private void setValue() {
            DecimalFormat decimalFormat = new DecimalFormat("#,###", new DecimalFormatSymbols());
            String priceTotals = null;

            txt_amount_id.setText(defaultValue+"");

            if (rateModel != null) {
                switch (currency) {
                    case "THB":
                        priceTotals = decimalFormat.format(totals);
                        txtOrderTotals.setText(priceTotals);
                        txt_price_id.setText(priceTotals);
                        break;
                    case "USD":
                        priceTotals = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                        txtOrderTotals.setText(priceTotals);
                        txt_price_id.setText(priceTotals);
                        break;
                    case "CNY":
                        priceTotals = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateCNY()));
                        txtOrderTotals.setText(priceTotals);
                        txt_price_id.setText(priceTotals);
                        break;
                }
            }else {
                priceTotals = decimalFormat.format(totals);
                txtOrderTotals.setText(priceTotals);
                txt_price_id.setText(priceTotals);
            }

        }
    }

    public String getTotalsOrderList(){
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String priceTotals = decimalFormat.format(totals);
        return priceTotals;
    }

    public ArrayList<ProductModel> productModelArrayList(){
        return arrayList;
    }


}
