package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ReviewClickListener;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ReviewsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<ReviewModel> arrayList;
    private String language;
    private ReviewClickListener listener;
    private boolean type;

    public ReviewsRecyclerViewAdapter(Context context, ArrayList<ReviewModel> arrayList, String language, ReviewClickListener listener,boolean type) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
        this.type = type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_review_view, parent, false);
        return new ReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ReviewModel reviewModel = (ReviewModel) arrayList.get(position);

        Picasso.get().load(reviewModel.getUserModel().getProfilePicUrl()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ReviewViewHolder)holder).img_person);
        ((ReviewViewHolder)holder).rating_person_id.setRating(reviewModel.getReviewRating());
        ((ReviewViewHolder)holder).txt_rating_date_time_id.setText(reviewModel.getReviewTimestamp());
        ((ReviewViewHolder)holder).txt_rating_review_id.setText(reviewModel.getReviewText()+"");

        String userId = context.getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        if (userId != null){
            if (String.valueOf(reviewModel.getUserModel().getUserId()).equals(userId)){
                ((ReviewViewHolder)holder).img_btn_more_rating_id.setVisibility(View.VISIBLE);
            }else {
                ((ReviewViewHolder)holder).img_btn_more_rating_id.setVisibility(View.GONE);
            }
        }
        ((ReviewViewHolder)holder).img_btn_more_rating_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSelectedMenu(view, reviewModel,position);
            }
        });

        if (type){
            ((ReviewViewHolder)holder).img_btn_more_rating_id.setVisibility(View.GONE);
            ((ReviewViewHolder)holder).txt_rating_name_items.setVisibility(View.VISIBLE);
        }else {
            ((ReviewViewHolder)holder).txt_rating_name_items.setVisibility(View.GONE);
        }
        String getItemName = "";
        switch (language){
            case "th":
                getItemName = "("+reviewModel.getItemsModel().getTopicTH()+")";
                break;
            case "en":
                getItemName = "("+reviewModel.getItemsModel().getTopicEN()+")";
                break;
            case "lo":
                getItemName = "("+reviewModel.getItemsModel().getTopicLO()+")";
                break;
            case "zh":
                getItemName = "("+reviewModel.getItemsModel().getTopicZH()+")";
                break;
        }

        ((ReviewViewHolder)holder).txt_name_person.setText(reviewModel.getUserModel().getFirstName()+ " " + reviewModel.getUserModel().getLastName()+ " " +getItemName);

        ((ReviewViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickedProfile(view,reviewModel);
            }
        });


    }

    private void onClickSelectedMenu(View view, final ReviewModel reviewModel,int position) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("REVIEW_MODEL",reviewModel);
        PopupMenu menu = new PopupMenu(context, view);
        menu.getMenuInflater().inflate(R.menu.menu_review,menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_review_edit:
                        listener.itemClicked(view,reviewModel);
                        break;
                    case R.id.action_review_delete:
                        listener.itemClickedDelete(view,reviewModel,position);
                        break;
                }

                return true;
            }
        });

        menu.show();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ReviewViewHolder extends RecyclerView.ViewHolder{
        private ImageView img_person;
        private ImageButton img_btn_more_rating_id;
        private TextView txt_name_person,txt_rating_date_time_id, txt_rating_review_id,txt_rating_name_items;
        private RatingBar rating_person_id;

        public ReviewViewHolder(@NonNull View itemView) {
            super(itemView);
            img_person = itemView.findViewById(R.id.img_person);
            img_btn_more_rating_id = itemView.findViewById(R.id.img_btn_more_rating_id);
            txt_name_person = itemView.findViewById(R.id.txt_name_person);
            txt_rating_date_time_id = itemView.findViewById(R.id.txt_rating_date_time_id);
            txt_rating_review_id = itemView.findViewById(R.id.txt_rating_review_id);
            rating_person_id = itemView.findViewById(R.id.rating_person_id);
            txt_rating_name_items = itemView.findViewById(R.id.txt_rating_name_items);
        }

    }
}
