package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.ClickListener.ItemImageClickListener;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.interface_callback.BookingRoomSelected;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class RoomRecyclerViewAdapter extends RecyclerView.Adapter<RoomRecyclerViewAdapter.RoomViewHolder>{

    private Context context;
    private ArrayList<RoomModel> arrayList;
    private ItemImageClickListener listener;
    private BookingRoomSelected roomSelected;
    private String language;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;

    public RoomRecyclerViewAdapter(Context context, ArrayList<RoomModel> arrayList, ItemImageClickListener listener, String language, BookingRoomSelected roomSelected,RateModel rateModel) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
        this.language = language;
        this.roomSelected = roomSelected;
        this.rateModel = rateModel;
        exchangeRate = new ExchangeRate();
    }

    @NonNull
    @Override
    public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_description_exhotel_view, parent,false);
        return new RoomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomViewHolder holder, int position) {
        final RoomModel itemsModel = (RoomModel) arrayList.get(position);

        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+ itemsModel.getRoomPictureModelArrayList().get(0).getPicturePaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((RoomViewHolder)holder).img_ex_hotel_id);

        switch (language){
            case "th":
                ((RoomViewHolder)holder).txt_ex_title_hotel_id.setText(itemsModel.getTopicTH());
                ((RoomViewHolder)holder).txt_ex_description_hotel_id.setText(itemsModel.getDescriptionTH());
                if (itemsModel.getBreakFast() == 0){
                    ((RoomViewHolder)holder).txt_ex_status_breakFast_id.setText("ไม่มีอาหารเช้า");
                }else {
                    ((RoomViewHolder)holder).txt_ex_status_breakFast_id.setText("อาหารเช้า");
                }
                break;
            case "en":
                ((RoomViewHolder)holder).txt_ex_title_hotel_id.setText(itemsModel.getTopicEN());
                ((RoomViewHolder)holder).txt_ex_description_hotel_id.setText(itemsModel.getDescriptionEN());
                if (itemsModel.getBreakFast() == 0){
                    ((RoomViewHolder)holder).txt_ex_status_breakFast_id.setText("No breakfast");
                }else {
                    ((RoomViewHolder)holder).txt_ex_status_breakFast_id.setText("Breakfast");
                }
                break;
            case "lo":
                ((RoomViewHolder)holder).txt_ex_title_hotel_id.setText(itemsModel.getTopicLO());
                ((RoomViewHolder)holder).txt_ex_description_hotel_id.setText(itemsModel.getDescriptionLO());
                if (itemsModel.getBreakFast() == 0){
                    ((RoomViewHolder)holder).txt_ex_status_breakFast_id.setText("ບໍ່ມີອາຫານເຊົ້າ");
                }else {
                    ((RoomViewHolder)holder).txt_ex_status_breakFast_id.setText("ອາຫານເຊົ້າ");
                }
                break;
            case "zh":
                ((RoomViewHolder)holder).txt_ex_title_hotel_id.setText(itemsModel.getTopicZH());
                ((RoomViewHolder)holder).txt_ex_description_hotel_id.setText(itemsModel.getDescriptionZH());
                if (itemsModel.getBreakFast() == 0){
                    ((RoomViewHolder)holder).txt_ex_status_breakFast_id.setText("没有早餐");
                }else {
                    ((RoomViewHolder)holder).txt_ex_status_breakFast_id.setText("早餐");
                }
                break;
        }


        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String price  = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        if (rateModel != null) {
            String currency = context.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(itemsModel.getPrice());
                    ((RoomViewHolder)holder).txt_exchange_rate_id.setText(currency);
                    ((RoomViewHolder)holder).txt_ex_price_hotel_id.setText(price);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getPrice(),rateModel.getRateUSD()));
                    ((RoomViewHolder)holder).txt_exchange_rate_id.setText(currency);
                    ((RoomViewHolder)holder).txt_ex_price_hotel_id.setText(price);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getPrice(),rateModel.getRateCNY()));
                    ((RoomViewHolder)holder).txt_exchange_rate_id.setText(currency);
                    ((RoomViewHolder)holder).txt_ex_price_hotel_id.setText(price);
                    break;
            }
        }else {
            price = decimalFormat.format(itemsModel.getPrice());
            ((RoomViewHolder)holder).txt_ex_price_hotel_id.setText(price);
        }






        ((RoomViewHolder)holder).img_ex_hotel_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClickedHotel(position);
            }
        });

        ((RoomViewHolder)holder).relativeLayout_booking_exHotel_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roomSelected.selectedRoom(itemsModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class RoomViewHolder extends RecyclerView.ViewHolder{
        ImageView img_ex_hotel_id;
        TextView txt_ex_title_hotel_id,txt_ex_description_hotel_id,txt_ex_status_breakFast_id,txt_ex_price_hotel_id;
        TextView txt_exchange_rate_id;
        RelativeLayout relativeLayout_booking_exHotel_id;
        public RoomViewHolder(@NonNull View itemView) {
            super(itemView);
            img_ex_hotel_id = itemView.findViewById(R.id.img_ex_hotel_id);
            txt_ex_title_hotel_id = itemView.findViewById(R.id.txt_ex_title_hotel_id);
            txt_ex_description_hotel_id = itemView.findViewById(R.id.txt_ex_description_hotel_id);
            txt_ex_status_breakFast_id = itemView.findViewById(R.id.txt_ex_status_breakFast_id);
            txt_ex_price_hotel_id = itemView.findViewById(R.id.txt_ex_price_hotel_id);
            relativeLayout_booking_exHotel_id = itemView.findViewById(R.id.relativeLayout_booking_exHotel_id);
            txt_exchange_rate_id = itemView.findViewById(R.id.txt_exchange_rate_id);
        }
    }


}
