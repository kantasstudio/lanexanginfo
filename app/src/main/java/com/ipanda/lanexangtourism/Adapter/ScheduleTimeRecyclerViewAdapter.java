package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ScheduleTimeClickListener;
import com.ipanda.lanexangtourism.Model.PeriodTimeModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class ScheduleTimeRecyclerViewAdapter extends RecyclerView.Adapter<ScheduleTimeRecyclerViewAdapter.ItemsViewHolder>{

    private Context context;
    private ArrayList<PeriodTimeModel> arrayList;
    private ScheduleTimeClickListener listener;
    private int mSelectedItem  = -1;

    public ScheduleTimeRecyclerViewAdapter(Context context, ArrayList<PeriodTimeModel> arrayList, ScheduleTimeClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.radio_choose_event_ticket, parent,false);
        return new ItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsViewHolder holder, int position) {
        final PeriodTimeModel itemsModel = (PeriodTimeModel) arrayList.get(position);

        ((ItemsViewHolder)holder).radioButton_schedule_time.setText(itemsModel.getPeriodOpen()+" - "+itemsModel.getPeriodClose());
        ((ItemsViewHolder)holder).radioButton_schedule_time.setChecked(mSelectedItem == position);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedItem = ((ItemsViewHolder)holder).getAdapterPosition();
                notifyDataSetChanged();
                listener.itemClicked(itemsModel.getPeriodOpen(),itemsModel.getPeriodClose());
            }
        };
        ((ItemsViewHolder)holder).radioButton_schedule_time.setOnClickListener(clickListener);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ItemsViewHolder extends RecyclerView.ViewHolder{
        ConstraintLayout con_schedule_time;
        RadioButton radioButton_schedule_time;
        public ItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            con_schedule_time = itemView.findViewById(R.id.con_schedule_time);
            radioButton_schedule_time = itemView.findViewById(R.id.radioButton_schedule_time);
        }
    }


}
