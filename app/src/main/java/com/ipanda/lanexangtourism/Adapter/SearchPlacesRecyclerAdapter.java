package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.SelectedPlacesClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.utils.BaseViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchPlacesRecyclerAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;
    private Context context;
    private ArrayList<ItemsModel> mItems;
    private SelectedPlacesClickListener mClickListener;
    private String language;

    public SearchPlacesRecyclerAdapter(Context context, ArrayList<ItemsModel> itemsModels, String language, SelectedPlacesClickListener mClickListener) {
        this.context = context;
        this.mItems = itemsModels;
        this.language = language;
        this.mClickListener = mClickListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_places_view, parent, false));
            case VIEW_TYPE_LOADING:
                return new ProgressHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == mItems.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public void addItems(ArrayList<ItemsModel> itemsModels) {
        mItems.addAll(itemsModels);
        notifyDataSetChanged();
    }

    public void addLoading() {
        isLoaderVisible = true;
        mItems.add(new ItemsModel());
        notifyItemInserted(mItems.size() - 1);
    }

    public void removeLoading() {
        isLoaderVisible = false;
        int position = mItems.size() - 1;
        ItemsModel item = getItem(position);
        if (item != null) {
            mItems.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    ItemsModel getItem(int position) {
        return mItems.get(position);
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.img_item_cover)
        ImageView imgCoverPlaces;
        @BindView(R.id.imgBtn_add_places)
        ImageButton imgBtnAddPlaces;
        @BindView(R.id.txt_item_topic)
        TextView txtTopic;
        @BindView(R.id.txt_item_contact)
        TextView txtContact;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);
            ItemsModel itemsModel = mItems.get(position);

            String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
//            itemsModel.getCoverItemsModel().getCoverPaths()
            Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(imgCoverPlaces);

            switch (language){
                case "th":
                    txtTopic.setText(itemsModel.getTopicTH());
                    txtContact.setText(itemsModel.getContactTH());
                    break;
                case "en":
                    txtTopic.setText(itemsModel.getTopicEN());
                    txtContact.setText(itemsModel.getContactEN());
                    break;
                case "lo":
                    txtTopic.setText(itemsModel.getTopicLO());
                    txtContact.setText(itemsModel.getContactLO());
                    break;
                case "zh":
                    txtTopic.setText(itemsModel.getTopicZH());
                    txtContact.setText(itemsModel.getContactZH());
                    break;
            }

        }
    }

    public class ProgressHolder extends BaseViewHolder {
        ProgressHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {
        }
    }
}
