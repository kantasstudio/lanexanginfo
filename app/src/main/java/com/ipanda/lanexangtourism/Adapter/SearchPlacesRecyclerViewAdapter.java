package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.SelectedPlacesClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SearchPlacesRecyclerViewAdapter extends RecyclerView.Adapter<SearchPlacesRecyclerViewAdapter.SearchPlacesViewHolder> implements Filterable {

    private Context context;
    private ArrayList<ItemsModel> itemsModelArrayList;
    private ArrayList<ItemsModel> arrayListFull;
    private SelectedPlacesClickListener mClickListener;
    private String language;

    public SearchPlacesRecyclerViewAdapter(Context context, ArrayList<ItemsModel> itemsModelArrayList,String language , SelectedPlacesClickListener mClickListener) {
        this.context = context;
        this.itemsModelArrayList = itemsModelArrayList;
        this.mClickListener = mClickListener;
        this.language = language;
        this.arrayListFull = new ArrayList<>(itemsModelArrayList);
    }

    @NonNull
    @Override
    public SearchPlacesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_places_view, parent,false);
        return new SearchPlacesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchPlacesViewHolder holder, int position) {
        final ItemsModel itemsModel = (ItemsModel) itemsModelArrayList.get(position);


        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+itemsModel.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((SearchPlacesViewHolder)holder).imgCoverPlaces);

        switch (language){
            case "th":
                ((SearchPlacesViewHolder)holder).txtTopic.setText(itemsModel.getTopicTH());
                ((SearchPlacesViewHolder)holder).txtContact.setText(itemsModel.getContactTH());
                break;
            case "en":
                ((SearchPlacesViewHolder)holder).txtTopic.setText(itemsModel.getTopicEN());
                ((SearchPlacesViewHolder)holder).txtContact.setText(itemsModel.getContactEN());
                break;
            case "lo":
                ((SearchPlacesViewHolder)holder).txtTopic.setText(itemsModel.getTopicLO());
                ((SearchPlacesViewHolder)holder).txtContact.setText(itemsModel.getContactLO());
                break;
            case "zh":
                ((SearchPlacesViewHolder)holder).txtTopic.setText(itemsModel.getTopicZH());
                ((SearchPlacesViewHolder)holder).txtContact.setText(itemsModel.getContactZH());
                break;
        }

        if (itemsModel.isSelectedItemToPlaces()){
            ((SearchPlacesViewHolder)holder).imgBtnAddPlaces.setImageResource(R.drawable.ic_delete_light);
        }else {
            ((SearchPlacesViewHolder)holder).imgBtnAddPlaces.setImageResource(R.drawable.ic_add_light);
        }

        ((SearchPlacesViewHolder)holder).imgBtnAddPlaces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.itemClickedPlaces(itemsModel,holder.imgBtnAddPlaces);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsModelArrayList.size();
    }


    public class SearchPlacesViewHolder extends RecyclerView.ViewHolder{
        ImageView imgCoverPlaces;
        ImageButton imgBtnAddPlaces;
        TextView txtTopic, txtContact;
        public SearchPlacesViewHolder(@NonNull View itemView) {
            super(itemView);
            imgCoverPlaces = itemView.findViewById(R.id.img_item_cover);
            imgBtnAddPlaces = itemView.findViewById(R.id.imgBtn_add_places);
            txtTopic = itemView.findViewById(R.id.txt_item_topic);
            txtContact = itemView.findViewById(R.id.txt_item_contact);
        }
    }

    @Override
    public Filter getFilter() {
        return itemsFilter;
    }

    private Filter itemsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            ArrayList<ItemsModel> filteredList = new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0){
                filteredList.addAll(arrayListFull);
            }else {
                String filterPattern = charSequence.toString().toLowerCase().trim();
                for (ItemsModel item : arrayListFull) {
                    switch (language){
                        case "th":
                            if (item.getTopicTH().toLowerCase().contains(filterPattern)){
                                filteredList.add(item);
                            }
                            break;
                        case "en":
                            if (item.getTopicEN().toLowerCase().contains(filterPattern)){
                                filteredList.add(item);
                            }
                            break;
                        case "lo":
                            if (item.getTopicLO().toLowerCase().contains(filterPattern)){
                                filteredList.add(item);
                            }
                            break;
                        case "zh":
                            if (item.getTopicZH().toLowerCase().contains(filterPattern)){
                                filteredList.add(item);
                            }
                            break;
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return  results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            itemsModelArrayList.clear();
            itemsModelArrayList.addAll((ArrayList) filterResults.values);
            notifyDataSetChanged();
        }
    };
}
