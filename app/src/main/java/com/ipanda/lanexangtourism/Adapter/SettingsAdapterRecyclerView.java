package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.ClickListener.SettingsItemOnClickListener;
import com.ipanda.lanexangtourism.Model.SettingsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class SettingsAdapterRecyclerView extends RecyclerView.Adapter<SettingsAdapterRecyclerView.FilterItemsViewHolder>{

    private Context context;
    private ArrayList<SettingsModel> itemsModelArrayList;
    private SettingsItemOnClickListener itemOnClickListener;

    public SettingsAdapterRecyclerView(Context context, ArrayList<SettingsModel> itemsModelArrayList,SettingsItemOnClickListener itemOnClickListener) {
        this.context = context;
        this.itemsModelArrayList = itemsModelArrayList;
        this.itemOnClickListener = itemOnClickListener;
    }

    @NonNull
    @Override
    public FilterItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_title_view,parent,false);
        return new FilterItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterItemsViewHolder holder, int position) {
        final SettingsModel itemsModel = (SettingsModel) itemsModelArrayList.get(position);
        holder.title.setText(itemsModel.getTitle());
        if (itemsModel.isSelected()){
            holder.img.setVisibility(View.VISIBLE);
        }else {
            holder.img.setVisibility(View.INVISIBLE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemOnClickListener.itemClicked(itemsModel,holder.img);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsModelArrayList.size();
    }


    public class FilterItemsViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        ImageView img;
        public FilterItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.filter_title);
            img = itemView.findViewById(R.id.filter_image);
        }
    }
}
