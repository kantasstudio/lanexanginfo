package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.ProductPhotoModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class SlidingHotMenuViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<ProductModel> list;
    private ChangeLanguageLocale changeLanguageLocale;
    private RateModel rateModel;
    private ExchangeRate exchangeRate;

    public SlidingHotMenuViewPagerAdapter(Context mContext, ArrayList<ProductModel> mListScreen, RateModel rateModel) {
        this.mContext = mContext;
        this.list = mListScreen;
        this.rateModel = rateModel;
        exchangeRate = new ExchangeRate();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        final ProductModel itemModel = (ProductModel) list.get(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = inflater.inflate(R.layout.custom_hot_menu_sliding_view,null);

        String paths = mContext.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        ImageView img_sliding_id = layoutScreen.findViewById(R.id.img_sliding_id);
        TextView txt_sliding_topic_id = layoutScreen.findViewById(R.id.txt_sliding_topic_id);
        TextView txt_sliding_price = layoutScreen.findViewById(R.id.txt_sliding_price);
        TextView txt_sliding_description = layoutScreen.findViewById(R.id.txt_sliding_description);

        for (ProductPhotoModel photo : itemModel.getProductPhotoModelArrayList()) {
            Picasso.get().load(paths+photo.getProductPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_sliding_id);
        }
        String currency = mContext.getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String price = null;

        changeLanguageLocale = new ChangeLanguageLocale(mContext);
        switch (changeLanguageLocale.getLanguage()){
            case "th":
                txt_sliding_topic_id.setText(itemModel.getProductNamesTH());
                txt_sliding_description.setText(itemModel.getDescriptionTH());

                if (rateModel != null) {
                    switch (currency) {
                        case "THB":
                            price = decimalFormat.format(itemModel.getProductPrice());
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                        case "USD":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemModel.getProductPrice(),rateModel.getRateUSD()));
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                        case "CNY":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemModel.getProductPrice(),rateModel.getRateCNY()));
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                    }
                }else {
                    price = decimalFormat.format(itemModel.getProductPrice());
                    txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                }


                break;
            case "en":
                txt_sliding_topic_id.setText(itemModel.getProductNamesEN());
                txt_sliding_description.setText(itemModel.getDescriptionEN());
                if (rateModel != null) {
                    switch (currency) {
                        case "THB":
                            price = decimalFormat.format(itemModel.getProductPrice());
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                        case "USD":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemModel.getProductPrice(),rateModel.getRateUSD()));
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                        case "CNY":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemModel.getProductPrice(),rateModel.getRateCNY()));
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                    }
                }else {
                    price = decimalFormat.format(itemModel.getProductPrice());
                    txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                }
                break;
            case "lo":
                txt_sliding_topic_id.setText(itemModel.getProductNamesLO());
                txt_sliding_description.setText(itemModel.getDescriptionLO());
                if (rateModel != null) {
                    switch (currency) {
                        case "THB":
                            price = decimalFormat.format(itemModel.getProductPrice());
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                        case "USD":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemModel.getProductPrice(),rateModel.getRateUSD()));
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                        case "CNY":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemModel.getProductPrice(),rateModel.getRateCNY()));
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                    }
                }else {
                    price = decimalFormat.format(itemModel.getProductPrice());
                    txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                }
                break;
            case "zh":
                txt_sliding_topic_id.setText(itemModel.getProductNamesZH());
                txt_sliding_description.setText(itemModel.getDescriptionZH());
                if (rateModel != null) {
                    switch (currency) {
                        case "THB":
                            price = decimalFormat.format(itemModel.getProductPrice());
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                        case "USD":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemModel.getProductPrice(),rateModel.getRateUSD()));
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                        case "CNY":
                            price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemModel.getProductPrice(),rateModel.getRateCNY()));
                            txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                            break;
                    }
                }else {
                    price = decimalFormat.format(itemModel.getProductPrice());
                    txt_sliding_price.setText(mContext.getString(R.string.text_selling_price)+currency+" "+price);
                }
                break;
        }

        container.addView(layoutScreen);
        return layoutScreen;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }


}
