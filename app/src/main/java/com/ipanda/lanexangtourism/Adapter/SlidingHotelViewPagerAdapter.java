package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.ipanda.lanexangtourism.Model.RoomPictureModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SlidingHotelViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<RoomPictureModel> list;


    public SlidingHotelViewPagerAdapter(Context mContext, ArrayList<RoomPictureModel> mListScreen) {
        this.mContext = mContext;
        this.list = mListScreen;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        final RoomPictureModel itemModel = (RoomPictureModel) list.get(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = inflater.inflate(R.layout.custom_hotel_sliding_view,null);

        String paths = mContext.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        ImageView img_sliding_id = layoutScreen.findViewById(R.id.img_sliding_id);
        Picasso.get().load(paths+itemModel.getPicturePaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_sliding_id);


        container.addView(layoutScreen);
        return layoutScreen;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }


}
