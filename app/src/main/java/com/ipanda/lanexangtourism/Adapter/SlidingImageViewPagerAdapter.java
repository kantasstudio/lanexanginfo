package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SlidingImageViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<ItemsPhotoDetailModel> list;
    private String language;

    public SlidingImageViewPagerAdapter(Context mContext, ArrayList<ItemsPhotoDetailModel> mListScreen, String language) {
        this.mContext = mContext;
        this.list = mListScreen;
        this.language = language;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        final ItemsPhotoDetailModel itemModel = (ItemsPhotoDetailModel) list.get(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = inflater.inflate(R.layout.custom_image_sliding_view,null);

        String paths = mContext.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        ImageView img_sliding_id = layoutScreen.findViewById(R.id.img_sliding_id);
        ImageView show_detail_text_id = layoutScreen.findViewById(R.id.show_detail_text_id);
        TextView txt_sliding_detail_id = layoutScreen.findViewById(R.id.txt_sliding_detail_id);
        TextView txt_sliding_detail_see_id = layoutScreen.findViewById(R.id.txt_sliding_detail_see_id);

        Picasso.get().load(paths+itemModel.getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_sliding_id);

        switch (language){
            case "th":
                txt_sliding_detail_id.setText(itemModel.getPhotoTextTH());
                txt_sliding_detail_see_id.setText(itemModel.getPhotoTextTH());
                break;
            case "en":
                txt_sliding_detail_id.setText(itemModel.getPhotoTextEN());
                txt_sliding_detail_see_id.setText(itemModel.getPhotoTextEN());
                break;
            case "lo":
                txt_sliding_detail_id.setText(itemModel.getPhotoTextLO());
                txt_sliding_detail_see_id.setText(itemModel.getPhotoTextLO());
                break;
            case "zh":
                txt_sliding_detail_id.setText(itemModel.getPhotoTextZH());
                txt_sliding_detail_see_id.setText(itemModel.getPhotoTextZH());
                break;
        }



        txt_sliding_detail_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_sliding_detail_id.setVisibility(View.GONE);
                show_detail_text_id.setVisibility(View.VISIBLE);
                txt_sliding_detail_see_id.setVisibility(View.VISIBLE);
            }
        });

        show_detail_text_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_sliding_detail_id.setVisibility(View.VISIBLE);
                show_detail_text_id.setVisibility(View.GONE);
                txt_sliding_detail_see_id.setVisibility(View.GONE);
            }
        });



        container.addView(layoutScreen);
        return layoutScreen;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }


}
