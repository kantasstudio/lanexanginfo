package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class SpinnerCategorySubAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflter;
    private String language;
    private ArrayList<SubCategoryModel> arrayList;

    public SpinnerCategorySubAdapter(Context context, ArrayList<SubCategoryModel> arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.custom_layout_item_spinner, null);
        final SubCategoryModel subCategory = (SubCategoryModel) arrayList.get(position);

        TextView names = (TextView) view.findViewById(R.id.txt_title_spinner);

        switch (language) {
            case "th":
                names.setText(subCategory.getCategoryTH());
                break;
            case "en":
                names.setText(subCategory.getCategoryEN());
                break;
            case "lo":
                names.setText(subCategory.getCategoryLO());
                break;
            case "zh":
                names.setText(subCategory.getCategoryZH());
                break;
        }

        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        return super.isEnabled(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);

        TextView tv = (TextView) view;
        if (position == 0) {
            tv.setTextColor(Color.GRAY);
        }else {

        }
        return view;
    }

}
