package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class SpinnerProvinceAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflter;
    private String language;
    private ArrayList<ProvincesModel> provincesArrayList;

    public SpinnerProvinceAdapter(Context context, ArrayList<ProvincesModel> provincesArrayList, String language) {
        this.context = context;
        this.provincesArrayList = provincesArrayList;
        this.language = language;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return provincesArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return provincesArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.custom_layout_item_spinner, null);
        final ProvincesModel provinces = (ProvincesModel) provincesArrayList.get(position);

        TextView names = (TextView) view.findViewById(R.id.txt_title_spinner);

        switch (language) {
            case "th":
                names.setText(provinces.getProvincesTH());
                break;
            case "en":
                names.setText(provinces.getProvincesEN());
                break;
            case "lo":
                names.setText(provinces.getProvincesLO());
                break;
            case "zh":
                names.setText(provinces.getProvincesZH());
                break;
        }

        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        return super.isEnabled(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);

        TextView tv = (TextView) view;
        if (position == 0) {
            tv.setTextColor(Color.GRAY);
        }else {

        }
        return view;
    }

}
