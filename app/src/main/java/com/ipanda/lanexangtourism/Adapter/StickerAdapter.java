package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.StickerTitleClickListener;
import com.ipanda.lanexangtourism.Model.StickerModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class StickerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<StickerModel> arrayList;
    private String language;
    private StickerTitleClickListener listener;

    public StickerAdapter(Context context, ArrayList<StickerModel> arrayList, String language, StickerTitleClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.title_sticker, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final StickerModel sticker = (StickerModel) arrayList.get(position);

        switch (language){
            case "th":
                ((ItemViewHolder)holder).titleSticker.setText(sticker.getStickerTH());
                break;
            case "en":
                ((ItemViewHolder)holder).titleSticker.setText(sticker.getStickerEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).titleSticker.setText(sticker.getStickerLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).titleSticker.setText(sticker.getStickerZH());
                break;
        }

        ((ItemViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickedSticker(sticker);
                for (int i = 0; i < arrayList.size() ; i++) {
                    if (arrayList.get(position) == arrayList.get(i)){
                        arrayList.get(i).setSelected(true);
                        ((ItemViewHolder) holder).titleSticker.setTextColor(context.getResources().getColor(R.color.white));
                        ((ItemViewHolder) holder).consTitleSticker.setBackground(context.getResources().getDrawable(R.drawable.shape_sticker_selected));
                    }else {
                        arrayList.get(i).setSelected(false);
                        ((ItemViewHolder) holder).titleSticker.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        ((ItemViewHolder) holder).consTitleSticker.setBackground(context.getResources().getDrawable(R.drawable.shape_sticker));
                    }
                }
                notifyDataSetChanged();
            }
        });

        if (sticker.isSelected()){
            ((ItemViewHolder) holder).titleSticker.setTextColor(context.getResources().getColor(R.color.white));
            ((ItemViewHolder) holder).consTitleSticker.setBackground(context.getResources().getDrawable(R.drawable.shape_sticker_selected));
        }else {
            ((ItemViewHolder) holder).titleSticker.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            ((ItemViewHolder) holder).consTitleSticker.setBackground(context.getResources().getDrawable(R.drawable.shape_sticker));
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private TextView titleSticker;
        private ConstraintLayout consTitleSticker;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            titleSticker = itemView.findViewById(R.id.txt_title_sticker);
            consTitleSticker = itemView.findViewById(R.id.cons_title_sticker);
        }

    }
}
