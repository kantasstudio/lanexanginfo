package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.StickerImageClickListener;
import com.ipanda.lanexangtourism.Model.StickerPathsModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class StickerImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<StickerPathsModel> arrayList;
    private String language;
    private StickerImageClickListener listener;

    public StickerImageAdapter(Context context, ArrayList<StickerPathsModel> arrayList, String language, StickerImageClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.image_sticker, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final StickerPathsModel sticker = (StickerPathsModel) arrayList.get(position);

        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile_Sticker/"+sticker.getStickerPaths();
        Picasso.get().load(paths).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((ItemViewHolder)holder).imgSticker);

/*        int resId = getResourceByFilename(context, sticker.getStickerPaths());
        Bitmap imageBitmap = BitmapFactory.decodeResource(context.getResources(), resId);
        ((ItemViewHolder)holder).imgSticker.setImageBitmap(imageBitmap);*/

        ((ItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickedStickerImage(sticker);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgSticker;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imgSticker = itemView.findViewById(R.id.img_sticker);
        }

    }

    public static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }
}
