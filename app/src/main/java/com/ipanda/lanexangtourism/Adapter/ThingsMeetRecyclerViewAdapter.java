package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ItemsImageClickListener;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ThingsMeetRecyclerViewAdapter extends RecyclerView.Adapter<ThingsMeetRecyclerViewAdapter.InterestingViewHolder>{

    private Context context;
    private ArrayList<ItemsPhotoDetailModel> arrayList;
    private ItemsImageClickListener listener;

    public ThingsMeetRecyclerViewAdapter(Context context, ArrayList<ItemsPhotoDetailModel> arrayList, ItemsImageClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public InterestingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_image_view, parent,false);
        return new InterestingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InterestingViewHolder holder, int position) {
        final ItemsPhotoDetailModel itemsModel = (ItemsPhotoDetailModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        Picasso.get().load(paths+itemsModel.getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((InterestingViewHolder)holder).img_view_id);

        String language = context.getSharedPreferences("PREF_APP_LOCALE", Context.MODE_PRIVATE).getString("APP_LOCALE","th");
        switch (language){
            case "th":
                ((InterestingViewHolder)holder).txt_view_id.setText(itemsModel.getPhotoTextTH());
                break;
            case "en":
                ((InterestingViewHolder)holder).txt_view_id.setText(itemsModel.getPhotoTextEN());
                break;
            case "lo":
                ((InterestingViewHolder)holder).txt_view_id.setText(itemsModel.getPhotoTextLO());
                break;
            case "zh":
                ((InterestingViewHolder)holder).txt_view_id.setText(itemsModel.getPhotoTextZH());
                break;
        }


        ((InterestingViewHolder)holder).img_view_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClicked(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class InterestingViewHolder extends RecyclerView.ViewHolder{
        ImageView img_view_id;
        TextView txt_view_id;
        public InterestingViewHolder(@NonNull View itemView) {
            super(itemView);
            img_view_id = itemView.findViewById(R.id.img_view_id);
            txt_view_id = itemView.findViewById(R.id.txt_view_id);
        }
    }


}
