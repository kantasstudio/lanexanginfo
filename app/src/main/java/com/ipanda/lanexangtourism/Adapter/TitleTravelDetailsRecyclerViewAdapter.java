package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Model.TravelDetailsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class TitleTravelDetailsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<TravelDetailsModel> arrayList;
    private String language;

    public TitleTravelDetailsRecyclerViewAdapter(Context context, ArrayList<TravelDetailsModel> arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_package_tour_detail_travel_view, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final TravelDetailsModel itemsModel = (TravelDetailsModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        switch (language){
            case "th":
                ((ItemViewHolder)holder).txt_the_date.setText(itemsModel.getDetailsDateTH());
                break;
            case "en":
                ((ItemViewHolder)holder).txt_the_date.setText(itemsModel.getDetailsDateEN());
                break;
            case "lo":
                ((ItemViewHolder)holder).txt_the_date.setText(itemsModel.getDetailsDateLO());
                break;
            case "zh":
                ((ItemViewHolder)holder).txt_the_date.setText(itemsModel.getDetailsDateZH());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_the_date, txt_the_location;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_the_date = itemView.findViewById(R.id.txt_the_date);
            txt_the_location = itemView.findViewById(R.id.txt_the_location);
        }

    }
}
