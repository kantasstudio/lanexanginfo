package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.ClickListener.ItemImageClickListener;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TravellingRecyclerViewAdapter extends RecyclerView.Adapter<TravellingRecyclerViewAdapter.TravellingViewHolder>{

    private Context context;
    private ArrayList<ItemsPhotoDetailModel> arrayList;
    private ItemImageClickListener listener;

    public TravellingRecyclerViewAdapter(Context context, ArrayList<ItemsPhotoDetailModel> arrayList, ItemImageClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public TravellingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_description_travelling_view, parent,false);
        return new TravellingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TravellingViewHolder holder, int position) {
        final ItemsPhotoDetailModel itemsModel = (ItemsPhotoDetailModel) arrayList.get(position);
        String paths = context.getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        Picasso.get().load(paths+itemsModel.getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(((TravellingViewHolder)holder).img_travelling_id);
        ((TravellingViewHolder)holder).txt_travelling_details_id.setText(itemsModel.getPhotoTextTH());
        ((TravellingViewHolder)holder).img_travelling_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.itemClickedTravelling(position);
            }
        });
        ((TravellingViewHolder)holder).txt_travelling_details_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class TravellingViewHolder extends RecyclerView.ViewHolder{
        ImageView img_travelling_id;
        TextView txt_travelling_details_id;
        public TravellingViewHolder(@NonNull View itemView) {
            super(itemView);
            img_travelling_id = itemView.findViewById(R.id.img_travelling_id);
            txt_travelling_details_id = itemView.findViewById(R.id.txt_travelling_details_id);
        }
    }


}
