package com.ipanda.lanexangtourism.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ipanda.lanexangtourism.Interface_click.ReviewClickListener;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.Model.VideoContentModel;
import com.ipanda.lanexangtourism.R;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class VideoContentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<VideoContentModel> arrayList;
    private String language;

    public VideoContentAdapter(Context context, ArrayList<VideoContentModel> arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.language = language;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_video_content_view, parent, false);
        return new VideoContentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final VideoContentModel videoContent = (VideoContentModel) arrayList.get(position);

        ((VideoContentViewHolder)holder).youtube_player_view.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                String videoId = videoContent.getVideoContentURL();
                youTubePlayer.loadVideo(videoId, 0);
                youTubePlayer.pause();
            }
        });

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class VideoContentViewHolder extends RecyclerView.ViewHolder{

        private YouTubePlayerView youtube_player_view;

        public VideoContentViewHolder(@NonNull View itemView) {
            super(itemView);

            youtube_player_view = itemView.findViewById(R.id.youtube_player_view);

        }

    }
}
