package com.ipanda.lanexangtourism.ClickListener;

import android.widget.ImageView;

import com.ipanda.lanexangtourism.Model.ItemsModel;

public interface ItemBookmarkClickListener {

    void itemClickedBookmark(ImageView img, ItemsModel items);

}
