package com.ipanda.lanexangtourism.ClickListener;

public interface ItemImageClickListener {
    void itemClickedInteresting(int position);
    void itemClickedTravelling(int position);
    void itemClickedHotel(int position);
    void itemClickedHotMenu(int position);
    void itemClickedPopularProduct(int position);
}
