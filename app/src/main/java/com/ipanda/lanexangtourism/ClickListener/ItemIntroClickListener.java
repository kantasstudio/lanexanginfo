package com.ipanda.lanexangtourism.ClickListener;

public interface ItemIntroClickListener {
    void itemClicked(int position);
}
