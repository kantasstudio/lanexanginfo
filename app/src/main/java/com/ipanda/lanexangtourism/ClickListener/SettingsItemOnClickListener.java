package com.ipanda.lanexangtourism.ClickListener;

import android.widget.ImageView;

import com.ipanda.lanexangtourism.Model.SettingsModel;

public interface SettingsItemOnClickListener {
    void itemClicked(SettingsModel settingsModel, ImageView imgCheck);
}
