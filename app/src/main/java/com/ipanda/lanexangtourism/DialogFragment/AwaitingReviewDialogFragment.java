package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.ipanda.lanexangtourism.Activity.MainActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;

public class AwaitingReviewDialogFragment extends DialogFragment implements View.OnClickListener {

    //variables
    private static String TAG = AwaitingReviewDialogFragment.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private ImageButton btnDismiss;

    public AwaitingReviewDialogFragment() {

    }


    public static AwaitingReviewDialogFragment newInstance() {
        AwaitingReviewDialogFragment fragment = new AwaitingReviewDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_awaiting_review_dialog, container, false);
        //views
        btnDismiss = view.findViewById(R.id.imgBtn_dismiss_awaiting_review);

        //set on click
        btnDismiss.setOnClickListener(this);


        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBtn_dismiss_awaiting_review:
                startActivity(new Intent(getContext(), MainActivity.class));
                dismiss();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
