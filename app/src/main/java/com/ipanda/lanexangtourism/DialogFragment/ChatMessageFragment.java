package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Activity.ChatMessageActivity;
import com.ipanda.lanexangtourism.Adapter.ChatMessageAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.GetChatRoomAsyncTask;
import com.ipanda.lanexangtourism.asynctask.MessageAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.GetChatRoomCallBack;
import com.ipanda.lanexangtourism.interface_callback.MessageCallBack;
import com.ipanda.lanexangtourism.interface_callback.MessageSendCallBack;
import com.ipanda.lanexangtourism.message.Config;
import com.ipanda.lanexangtourism.post_booking.HttpPostMessageRequest;

import java.util.ArrayList;
import java.util.HashMap;


public class ChatMessageFragment extends DialogFragment implements GetChatRoomCallBack , MessageCallBack , MessageSendCallBack {

    //Variables
    private static String TAG = ChatMessageFragment.class.getSimpleName();

    private Toolbar mToolbar;

    private TextView toolbar_title;

    private ChangeLanguageLocale languageLocale;

    private String userId;

    private String sendToUserId;

    private String title;

    private String chatRoomId;

    private String urlGetRoomId;

    private String urlChatMessages;

    private EditText edtMessage;

    private ImageView imgSendMessage;

    private RecyclerView recyclerMessage;

    private ChatMessageAdapter messageAdapter;

    private ArrayList<MessageModel> newMessageArrayList;

    private BroadcastReceiver broadcastReceiver;

    private String coverPathsCompany;

    public ChatMessageFragment() {
        // Required empty public constructor
    }

    public static ChatMessageFragment newInstance() {
        ChatMessageFragment fragment = new ChatMessageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {
            sendToUserId = getArguments().getString("Send_To_UserId");
            title = getArguments().getString("Send_To_Name");
            chatRoomId = getArguments().getString("Chat_Room_Id");
            urlGetRoomId = getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/getChatRoom?from_user_id="+userId+"&to_user_id="+sendToUserId;
            coverPathsCompany = getArguments().getString("Cover_Paths_Company");

            new GetChatRoomAsyncTask(this).execute(urlGetRoomId);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_message, container, false);

        //Views
        mToolbar = view.findViewById(R.id.message_toolbar);
        toolbar_title = view.findViewById(R.id.toolbar_title);
        edtMessage = view.findViewById(R.id.edt_message_id);
        recyclerMessage = view.findViewById(R.id.recycler_chat_message);
        imgSendMessage = view.findViewById(R.id.img_send_message_id);

        toolbar_title.setText(title);

        newMessageArrayList = new ArrayList<>();

        //setup Toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        if(((AppCompatActivity)getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle result = new Bundle();
                    result.putString("back", "back");
                    getParentFragmentManager().setFragmentResult("KEY_MESSAGE", result);
                    dismiss();

                }
            });
            ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        }

        // set event onClick
        imgSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupPostMessage();
            }
        });

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    GetChatMessage();
                }
            }
        };



        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerSendMessage(ArrayList<MessageModel> messageArrayList) {
        if (messageArrayList != null && messageArrayList.size() != 0) {
            GetChatMessage();
        }
    }

    @Override
    public void onRequestCompleteListenerChatRoom(String chatRoomId) {
        if (chatRoomId != null && !chatRoomId.equals("")){
            this.chatRoomId = chatRoomId;
            GetChatMessage();
        }
    }

    @Override
    public void onRequestCompleteListener(ArrayList<MessageModel> messageArrayList) {
        if (messageArrayList != null && messageArrayList.size() != 0) {
            this.newMessageArrayList.clear();
            this.newMessageArrayList = messageArrayList;
            setMessageToAdapter();
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private void GetChatMessage() {
        urlChatMessages = getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/getChatMessages/" + chatRoomId;
        new MessageAsyncTask(ChatMessageFragment.this).execute(urlChatMessages);
    }

    private void setMessageToAdapter(){
        recyclerMessage.setHasFixedSize(true);
        messageAdapter = new ChatMessageAdapter(getContext(), newMessageArrayList, languageLocale.getLanguage(), Integer.parseInt(userId),coverPathsCompany);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        recyclerMessage.setLayoutManager(linearLayoutManager);
        recyclerMessage.setItemAnimator(new DefaultItemAnimator());
        recyclerMessage.setAdapter(messageAdapter);
    }

    private void setupPostMessage(){
        String edtGetMessage = edtMessage.getText().toString();
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/sendingMessage");
        HashMap<String, String> paramsPost = new HashMap<>();
        paramsPost.put("to_user_id", String.valueOf(sendToUserId));
        paramsPost.put("from_user_id", userId);
        paramsPost.put("Items_items_id", "-1");
        paramsPost.put("message", edtGetMessage);
        httpCallPost.setParams(paramsPost);
        new HttpPostMessageRequest(this).execute(httpCallPost);
        edtMessage.setText("");
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

}