package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.archit.calendardaterangepicker.customviews.CalendarListener;
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ChooseDateHotelDialogFragment extends DialogFragment {

    //variable
    private static String TAG = ChooseDateHotelDialogFragment.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private ImageView img_dismiss_hotel;

    private TextView toolbar_title, toolbar_title_right;

    private TextView txt_check_in_day, txt_check_in_date, txt_check_in_month;

    private TextView txt_check_out_day, txt_check_out_date, txt_check_out_month;

    private String sDate = null, eDate = null ,showDate = null, showMonth = null;

    private boolean isStartDate = false, isEndDate = false;

    private DateRangeCalendarView cdrvCalendar;

    public ChooseDateHotelDialogFragment() {
        // Required empty public constructor
    }

    public static ChooseDateHotelDialogFragment newInstance() {
        ChooseDateHotelDialogFragment fragment = new ChooseDateHotelDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_date_hotel_dialog, container, false);

        // Views
        img_dismiss_hotel = view.findViewById(R.id.img_dismiss_hotel);
        toolbar_title = view.findViewById(R.id.toolbar_title);
        toolbar_title_right = view.findViewById(R.id.toolbar_title_right);
        txt_check_in_day = view.findViewById(R.id.txt_check_in_day);
        txt_check_in_date = view.findViewById(R.id.txt_check_in_date);
        txt_check_in_month = view.findViewById(R.id.txt_check_in_month);
        txt_check_out_day = view.findViewById(R.id.txt_check_out_day);
        txt_check_out_date = view.findViewById(R.id.txt_check_out_date);
        txt_check_out_month = view.findViewById(R.id.txt_check_out_month);
        cdrvCalendar = view.findViewById(R.id.cdrvCalendar);

        //setup calendar
        setUpCalendar();
        getCurrentDate();

        //set event onClick
        img_dismiss_hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        toolbar_title_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStartDate && isEndDate && sDate != null && eDate != null && showDate != null && showMonth != null){
                    Bundle result = new Bundle();
                    result.putString("CHOOSE_START_DATE", sDate);
                    result.putString("CHOOSE_END_DATE", eDate);
                    result.putString("CHOOSE_SHOW_DATE", showDate);
                    result.putString("CHOOSE_SHOW_MONTH", showMonth);
                    getParentFragmentManager().setFragmentResult("KEY_CHOOSE_DATE_HOTEL", result);
                    getParentFragmentManager().setFragmentResult("KEY_CHOOSE_DATE_HOTEL_MAIN", result);
                    getParentFragmentManager().setFragmentResult("Reload", result);
                    dismiss();
                }else {
                    Toast.makeText(getContext(), "Please select Date.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    private void setUpCalendar() {
        cdrvCalendar.setNavLeftImage(ContextCompat.getDrawable(getContext(),R.drawable.ic_left));
        cdrvCalendar.setNavRightImage(ContextCompat.getDrawable(getContext(),R.drawable.ic_right));
        Calendar current = Calendar.getInstance();
        cdrvCalendar.setCurrentMonth(current);
        cdrvCalendar.setSelectedDateRange(current, current);
        cdrvCalendar.setCalendarListener(new CalendarListener() {
            @Override
            public void onFirstDateSelected(@NotNull Calendar startDate) {
                isStartDate = false;
                isEndDate = false;
            }

            @Override
            public void onDateRangeSelected(@NotNull Calendar startDate, @NotNull Calendar endDate) {
                String[] subStart = startDate.getTime().toString().split(" ");
                String[] subEnd = endDate.getTime().toString().split(" ");
                isStartDate = true;
                isEndDate = true;
                int startDay = startDate.get(Calendar.DAY_OF_MONTH);
                int startMonth = (startDate.get(Calendar.MONTH)+1);
                int startYear = startDate.get(Calendar.YEAR);
                int endDay = endDate.get(Calendar.DAY_OF_MONTH);
                int endMonth = (endDate.get(Calendar.MONTH)+1);
                int endYear = endDate.get(Calendar.YEAR);
                txt_check_in_day.setText(subStart[2]);
                txt_check_in_date.setText(subStart[0]);
                txt_check_in_month.setText(subStart[1]);
                txt_check_out_day.setText(subEnd[2]);
                txt_check_out_date.setText(subEnd[0]);
                txt_check_out_month.setText(subEnd[1]);
                sDate = startDay+"-"+startMonth+"-"+startYear;
                eDate = endDay+"-"+endMonth+"-"+endYear;
                showDate = subStart[2]+"-"+subEnd[2]+" "+subStart[1];
                showMonth = subStart[2]+"-"+subStart[1]+"-"+startYear+" - "+subEnd[2]+"-"+subEnd[1]+"-"+endYear;
            }
        });
    }


    private void getCurrentDate(){
        String pattern = "EEEE dd MMM yyyy HH:mm";
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String mDate = simpleDateFormat.format(date);
        String[] subString = mDate.split(" ");
        String subDate = subString[0];
        String subDay = subString[1];
        String subMonth = subString[2];
        String subYear = subString[3];
        String subTime = subString[4];
        txt_check_in_day.setText(subDay);
        txt_check_in_date.setText(subDate);
        txt_check_in_month.setText(subMonth);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}