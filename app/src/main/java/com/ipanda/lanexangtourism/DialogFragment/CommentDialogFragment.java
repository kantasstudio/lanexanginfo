package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.items_view.ViewProgramTourActivity;

import java.util.HashMap;


public class CommentDialogFragment extends DialogFragment implements View.OnClickListener {

    //variables
    private static String TAG = CommentDialogFragment.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private ReviewModel reviewModel;

    private String userId;

    private String url = "",textComment = null;

    private ImageView img_user_profile;

    private EditText edt_comment;

    private RelativeLayout ry_send_comment;

    private ProgramTourModel tourModel;



    public CommentDialogFragment() {

    }

    public static CommentDialogFragment newInstance() {
        CommentDialogFragment fragment = new CommentDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {
            reviewModel = getArguments().getParcelable("REVIEW_MODEL");
            tourModel = getArguments().getParcelable("PROGRAM_TOUR_MODEL");
        }
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment_dialog, container, false);

        //views
        img_user_profile = view.findViewById(R.id.img_user_profile);
        edt_comment = view.findViewById(R.id.edt_comment);
        ry_send_comment = view.findViewById(R.id.ry_send_comment);

        if (reviewModel != null){
            edt_comment.setText(reviewModel.getReviewText());
        }

        //set event onclick
        ry_send_comment.setOnClickListener(this);


        return view;
    }

    private boolean getTextComment() {
        if (TextUtils.isEmpty(edt_comment.getText().toString())){
            Toast.makeText(getContext(), "Empty field not allowed!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void methodPostUpdate() {
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Review/update");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("User_user_id",userId);
        paramsPost.put("review_rating", String.valueOf(0));
        paramsPost.put("review_text",edt_comment.getText().toString());
        paramsPost.put("review_id", String.valueOf(reviewModel.getReviewId()));
        httpCallPost.setParams(paramsPost);
        if (getTextComment()) {
            new HttpPostRequestAsyncTask().execute(httpCallPost);
            int REQUEST_CODE = 1001;
            Intent intent = new Intent(getContext(), ViewProgramTourActivity.class);
            intent.putExtra("PROGRAM_TOUR",tourModel);
            getActivity().startActivityForResult(intent,REQUEST_CODE);
            getActivity().finish();
            dismiss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ry_send_comment:
                methodPostUpdate();
                dismiss();
                break;
        }
    }
}
