package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Activity.MainActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;

public class ConfirmOrdersDialogFragment extends DialogFragment implements View.OnClickListener {

    //variables
    private static String TAG = ConfirmOrdersDialogFragment.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private TextView txt_order_phone_id, txt_confirm_details, txt_confirm_title;

    private RelativeLayout rl_btn_confirm_order_product;

    private String title = null, details = null, phone = null;

    public ConfirmOrdersDialogFragment() {

    }


    public static ConfirmOrdersDialogFragment newInstance() {
        ConfirmOrdersDialogFragment fragment = new ConfirmOrdersDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {
            title = getArguments().getString("TITLE");
            details = getArguments().getString("DETAILS");
            phone = getArguments().getString("PHONE");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_confirm_orders_dialog, container, false);

        //views
        txt_confirm_title = view.findViewById(R.id.txt_confirm_title);
        txt_confirm_details = view.findViewById(R.id.txt_confirm_details);
        txt_order_phone_id = view.findViewById(R.id.txt_confirm_phone);
        rl_btn_confirm_order_product = view.findViewById(R.id.rl_btn_confirm_product);

        //set content
        if (title != null && details != null) {
            txt_confirm_title.setText(title);
            txt_confirm_details.setText(details);
            txt_order_phone_id.setText(phone);
        }

        //set on click views
        rl_btn_confirm_order_product.setOnClickListener(this);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rl_btn_confirm_product:
                dismiss();
                startActivity(new Intent(getContext(), MainActivity.class));
                break;
        }
    }
}
