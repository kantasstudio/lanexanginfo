package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.PersonalInfoActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;


public class CopyrightDialogFragment extends DialogFragment {

    //variables
    private static String TAG = CopyrightDialogFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private UserModel user = null;

    public CopyrightDialogFragment() {
        // Required empty public constructor
    }

    public static CopyrightDialogFragment newInstance() {
        CopyrightDialogFragment fragment = new CopyrightDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {
            user = getArguments().getParcelable("SEND_USER_MODEL");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_copyright_dialog, container, false);

        RadioButton btnAgree = (RadioButton) view.findViewById(R.id.radio_agree);
        RadioButton btnDisAgree = (RadioButton) view.findViewById(R.id.radio_disagree);

        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnAgree.isChecked()){
                    if (user != null){
                        Intent intent = new Intent(getContext(), PersonalInfoActivity.class);
                        intent.putExtra("SEND_USER_MODEL",user);
                        startActivity(intent);
                    }else {
                        Toast.makeText(getContext(), "Account Personal info not working !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnDisAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnDisAgree.isChecked()){
                    dismiss();
                }
            }
        });



        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}