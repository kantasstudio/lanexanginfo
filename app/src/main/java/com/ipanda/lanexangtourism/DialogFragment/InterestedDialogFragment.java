package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Adapter.FollowRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.FollowClickListener;
import com.ipanda.lanexangtourism.Model.BookMarksModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;

import java.util.HashMap;


public class InterestedDialogFragment extends DialogFragment implements FollowClickListener {

    //variables
    private static String TAG = InterestedDialogFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private RecyclerView recycler_interested;

    private FollowRecyclerViewAdapter followAdapter;

    private ImageView img_back_interested;

    private TextView txt_title_items,txt_address_items;

    private BookMarksModel bookMarksModel;

    public InterestedDialogFragment() {

    }


    public static InterestedDialogFragment newInstance() {
        InterestedDialogFragment fragment = new InterestedDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        if (getArguments() != null) {
            bookMarksModel = getArguments().getParcelable("BOOKMARKS_MODEL");
        }
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_interested_dialog, container, false);

        //views
        recycler_interested = view.findViewById(R.id.recycler_interested);
        img_back_interested = view.findViewById(R.id.img_back_interested);
        txt_title_items = view.findViewById(R.id.txt_title_items);
        txt_address_items = view.findViewById(R.id.txt_address_items);

        switch (languageLocale.getLanguage()){
            case "th":
                txt_title_items.setText(bookMarksModel.getItemsModelArrayList().get(0).getTopicTH());
                txt_address_items.setText(bookMarksModel.getItemsModelArrayList().get(0).getContactTH());
                break;
            case "en":
                txt_title_items.setText(bookMarksModel.getItemsModelArrayList().get(0).getTopicEN());
                txt_address_items.setText(bookMarksModel.getItemsModelArrayList().get(0).getContactTH());
                break;
            case "lo":
                txt_title_items.setText(bookMarksModel.getItemsModelArrayList().get(0).getTopicLO());
                txt_address_items.setText(bookMarksModel.getItemsModelArrayList().get(0).getContactTH());
                break;
            case "zh":
                txt_title_items.setText(bookMarksModel.getItemsModelArrayList().get(0).getTopicZH());
                txt_address_items.setText(bookMarksModel.getItemsModelArrayList().get(0).getContactTH());
                break;
        }

        if (bookMarksModel != null){
            followAdapter = new FollowRecyclerViewAdapter(getContext(), bookMarksModel.getUserModelArrayList(), this);
            recycler_interested.setLayoutManager(new LinearLayoutManager(getContext()));
            recycler_interested.setAdapter(followAdapter);
        }

        img_back_interested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void itemClickedProfile(UserModel user) {
        Bundle bundle = new Bundle();
        bundle.putString("USER_ID",String.valueOf(user.getUserId()));
        DialogFragment dialogFragment = ViewProfileDialogFragment.newInstance();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(getFragmentManager(),"InterestedDialogFragment");
    }

    @Override
    public void itemClickedFollow(TextView txt_follow_title, RelativeLayout rl_btn_follow, UserModel user) {
        String userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        if (user != null && userId != null) {
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Followers");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("followers_user_id", String.valueOf(user.getUserId()));
            httpCallPost.setParams(paramsPost);
            new HttpPostRequestAsyncTask().execute(httpCallPost);

            if (user.isFollowState()){
                user.setFollowState(false);
                txt_follow_title.setText("ติดตาม");
                txt_follow_title.setTextColor(getResources().getColor(R.color.white));
                rl_btn_follow.setBackground(getResources().getDrawable(R.drawable.shape_button));

            }else {
                user.setFollowState(true);
                txt_follow_title.setText("ติดตามอยู่");
                txt_follow_title.setTextColor(getResources().getColor(R.color.colorTextInputPersonnelInfo));
                rl_btn_follow.setBackground(getResources().getDrawable(R.drawable.shape_button_follow));
            }

        }
    }
}
