package com.ipanda.lanexangtourism.DialogFragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agrawalsuneet.loaderspack.loaders.MultipleRippleLoader;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.ipanda.lanexangtourism.Adapter.ItemsHomeSearchAdapter;
import com.ipanda.lanexangtourism.Adapter.ProvinceAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ItemsSearchClickListener;
import com.ipanda.lanexangtourism.Interface_click.ProvinceClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ItemsHomeSearchAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsHomeSearchPostRequest;
import com.ipanda.lanexangtourism.asynctask.ProvinceAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ItemsLastSearchHomeCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsHomeSearchCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsHomeSearchPostCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProvinceCallBack;
import com.ipanda.lanexangtourism.items_view.ViewTourismActivity;
import com.ipanda.lanexangtourism.post_search.LastSearchItems;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ItemsSearchHomeDialogFragment extends DialogFragment implements View.OnClickListener, ProvinceCallBack, ProvinceClickListener
        , ItemsHomeSearchCallBack, ItemsSearchClickListener, ItemsHomeSearchPostCallBack, ItemsLastSearchHomeCallBack {

    //variables
    private static final String TAG = ItemsSearchHomeDialogFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private View view;

    private int getCategory;

    private String titleMenu;

    private String userId, url;

    private RelativeLayout btnBackPressed;

    private ImageView btnSearch;

    private AutoCompleteTextView autoCompleteSearch;

    private RecyclerView recyclerProvince;

    private ProvinceAdapter provinceAdapter;

    private RecyclerView recyclerItems;

    private ItemsHomeSearchAdapter itemsAdapter;

    private MultipleRippleLoader mLoaderProvince, mLoaderItems;

    private LinearLayout showProvince, showItems;

    private ConstraintLayout conTitleSearch, conLastSearch, conProvince, conSearchMenu, conLocationIn;

    private ImageView imgItemSearch;

    private TextView txtItemMenu;

    private Location locationResult;

    private FusedLocationProviderClient fusedLocationClient;

    private RecyclerView recycler_last_search;

    private ItemsHomeSearchAdapter lastSearchItemsAdapter;

    private Collection<String> lastSearchId = new ArrayList<>();

    private List<String> distinctLastSearchId = new ArrayList<>();


    public ItemsSearchHomeDialogFragment() {
        // Required empty public constructor
    }


    public static ItemsSearchHomeDialogFragment newInstance() {
        ItemsSearchHomeDialogFragment fragment = new ItemsSearchHomeDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);


        if (getArguments() != null) {
            getCategory = getArguments().getInt("CATEGORY_ID", 0);
            titleMenu = getArguments().getString("TEXT_TITLE");
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener((Activity) getContext(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            locationResult = location;
                            getLastSearch();
                        }
                    }
                });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_items_search_dialog, container, false);

        //Views
        btnBackPressed = view.findViewById(R.id.btn_back);
        btnSearch = view.findViewById(R.id.btn_search);
        autoCompleteSearch = view.findViewById(R.id.autoCompleteTextSearch);
        recyclerProvince = view.findViewById(R.id.recycler_province);
        mLoaderProvince = view.findViewById(R.id.multipleRippleLoader_province);
        showProvince = view.findViewById(R.id.show_not_found_province);
        conTitleSearch = view.findViewById(R.id.con_search_title_id);
        conProvince = view.findViewById(R.id.con_home_province_id);
        conLastSearch = view.findViewById(R.id.con_home_last_search_id);
        conSearchMenu = view.findViewById(R.id.con_search_menu_id);
        conLocationIn = view.findViewById(R.id.con_location_in);

        recyclerItems = view.findViewById(R.id.recycler_home_search);
        mLoaderItems = view.findViewById(R.id.multipleRippleLoader_items);
        showItems = view.findViewById(R.id.show_not_found_items);

        imgItemSearch = view.findViewById(R.id.img_item_search_id);
        txtItemMenu = view.findViewById(R.id.txt_item_menu_id);



        recycler_last_search = view.findViewById(R.id.recycler_last_search);

        //set on click
        btnBackPressed.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        conSearchMenu.setOnClickListener(this);
        conLocationIn.setOnClickListener(this);

        url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/ProvinceGroup";
        if (url != null){
            new ProvinceAsyncTask(this).execute(url);
        }
        if (getCategory != 0){
            setupContentSearch(getCategory);
        }
        autoCompleteSearch.setOnEditorActionListener(editorActionListener);
        getLastSearch();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_back:
                dismiss();
                break;
            case R.id.btn_search:
                methodPostHomeSearch();
                break;
            case R.id.con_search_menu_id:
                setupSearchAllItems();
                break;
            case R.id.con_location_in:
                methodPostHomeSearchMyLocation();
                break;
        }
    }

    private void setupSearchAllItems() {
        if (userId != null) {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Search/SearchItemByMenuItemId/" + getCategory + "?user_id=" + userId
            +"&latitude="+locationResult.getLatitude()+"&longitude="+locationResult.getLongitude();
        }else {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Search/SearchItemByMenuItemId/" + getCategory + "?user_id="
            +"&latitude="+locationResult.getLatitude()+"&longitude="+locationResult.getLongitude();
        }
        new ItemsHomeSearchAsyncTask(this).execute(url);
        mLoaderItems.setVisibility(View.VISIBLE);
        showItems.setVisibility(View.GONE);
        recyclerItems.setVisibility(View.GONE);
    }

    private void methodPostHomeSearch(){
        String textSearch = autoCompleteSearch.getText().toString().replaceAll("", "");
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SearchItem");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("str_search",textSearch);
        paramsPost.put("user_id",userId);
        paramsPost.put("category_id", String.valueOf(getCategory));
        paramsPost.put("latitude", locationResult.getLatitude()+"");
        paramsPost.put("longitude", locationResult.getLongitude()+"");
        httpCallPost.setParams(paramsPost);
        new ItemsHomeSearchPostRequest(this).execute(httpCallPost);
        getLastSearch();
    }

    private void methodPostHomeSearchMyLocation(){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SearchItemNearMe");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("user_id",userId);
        paramsPost.put("category_id", String.valueOf(getCategory));
        paramsPost.put("latitude", locationResult.getLatitude()+"");
        paramsPost.put("longitude", locationResult.getLongitude()+"");
        httpCallPost.setParams(paramsPost);
        new ItemsHomeSearchPostRequest(this).execute(httpCallPost);
    }

    @Override
    public void onClickedProvince(ProvincesModel provinces) {
        if (userId != null) {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Search/SearchItemByProvince/"+provinces.getProvincesId()+"?user_id=" + userId +
                    "&latitude="+locationResult.getLatitude()+"&longitude="+locationResult.getLongitude()+"&category_id="+getCategory;
        }else {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Search/SearchItemByProvince/"+provinces.getProvincesId()+"?user_id=" +
                    "&latitude="+locationResult.getLatitude()+"&longitude="+locationResult.getLongitude()+"&category_id="+getCategory;
        }
        mLoaderProvince.setVisibility(View.GONE);
        recyclerProvince.setVisibility(View.GONE);
        new ItemsHomeSearchAsyncTask(this).execute(url);
        setLastSearch();
    }

    @Override
    public void onClickedItemSearch(ItemsModel items) {
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL", items);
        intent.putExtra("CATEGORY",items.getMenuItemModel().getMenuItemId());
        startActivity(intent);
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerLastSearch(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0) {
            lastSearchItemsAdapter = new ItemsHomeSearchAdapter(getContext(), itemsModelArrayList, languageLocale.getLanguage(),this);
            recycler_last_search.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recycler_last_search.setAdapter(lastSearchItemsAdapter);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0) {
            Log.e("check data", itemsModelArrayList + "");
            itemsAdapter = new ItemsHomeSearchAdapter(getContext(), itemsModelArrayList, languageLocale.getLanguage(),this);
            recyclerItems.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerItems.setAdapter(itemsAdapter);

            mLoaderItems.setVisibility(View.GONE);
            showItems.setVisibility(View.GONE);
            recyclerItems.setVisibility(View.VISIBLE);
            mLoaderProvince.setVisibility(View.GONE);
            recyclerProvince.setVisibility(View.VISIBLE);

            for (ItemsModel item : itemsModelArrayList) {
                lastSearchId.add(item.getItemsId()+"");
            }
            distinctLastSearchId = lastSearchId.stream().distinct().collect(Collectors.toList());
            setLastSearch();
            getLastSearch();

        }else {
            mLoaderItems.setVisibility(View.GONE);
            showItems.setVisibility(View.VISIBLE);
            recyclerItems.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestCompleteListenerProvinces(ArrayList<ProvincesModel> provincesList) {
        if (provincesList != null && provincesList.size() != 0) {
            Log.e("check data", provincesList + "");
            provinceAdapter = new ProvinceAdapter(getContext(), provincesList, languageLocale.getLanguage(),this);
            recyclerProvince.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerProvince.setAdapter(provinceAdapter);

        }else {
            mLoaderProvince.setVisibility(View.GONE);
            showProvince.setVisibility(View.VISIBLE);
            recyclerProvince.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private void setupContentSearch(int getCategory) {
        switch (getCategory){
            case 2:
                imgItemSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu2));
                txtItemMenu.setText(titleMenu);
                break;
            case 3:
                imgItemSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu3));
                txtItemMenu.setText(titleMenu);
                break;
            case 4:
                imgItemSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu4));
                txtItemMenu.setText(titleMenu);
                break;
            case 5:
                imgItemSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu5));
                txtItemMenu.setText(titleMenu);
                break;
            case 6:
                imgItemSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu6));
                txtItemMenu.setText(titleMenu);
                break;
        }

    }

    @Override
    public void onPreCallServicePost() {

    }

    @Override
    public void onCallServicePost() {

    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestCompleteListenerPost(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0) {
            Log.e("check data", itemsModelArrayList + "");
            Collections.sort(itemsModelArrayList,new DistanceSorter());
            itemsAdapter = new ItemsHomeSearchAdapter(getContext(), itemsModelArrayList, languageLocale.getLanguage(),this);
            recyclerItems.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerItems.setAdapter(itemsAdapter);

            mLoaderItems.setVisibility(View.GONE);
            showItems.setVisibility(View.GONE);
            recyclerItems.setVisibility(View.VISIBLE);

            for (ItemsModel item : itemsModelArrayList) {
                lastSearchId.add(item.getItemsId()+"");
            }
            distinctLastSearchId = lastSearchId.stream().distinct().collect(Collectors.toList());
            setLastSearch();
            getLastSearch();

        }else {
            mLoaderItems.setVisibility(View.GONE);
            showItems.setVisibility(View.VISIBLE);
            recyclerItems.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestFailedPost(String result) {

    }

    private TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

            switch (actionId){
                case EditorInfo.IME_ACTION_SEND:
                    methodPostHomeSearch();
                    break;
            }
            return false;
        }
    };

    public class DistanceSorter implements Comparator<ItemsModel> {
        public int compare(ItemsModel o1, ItemsModel o2) {
            return o1.getDistance() - o2.getDistance();
        }
    }

    private void setLastSearch(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String result = String.join(",", distinctLastSearchId);
            getContext().getSharedPreferences("PREF_APP_LAST_SEARCH_ITEMS"+getCategory, Context.MODE_PRIVATE).edit().putString("APP_LAST_SEARCH_ITEMS"+getCategory, result).apply();
        }
    }

    private void getLastSearch(){
        String getLastSearch = getContext().getSharedPreferences("PREF_APP_LAST_SEARCH_ITEMS"+getCategory, Context.MODE_PRIVATE).getString("APP_LAST_SEARCH_ITEMS"+getCategory,null);
        if (getLastSearch != null && !getLastSearch.equals("")){
            postRequestLastSearch(getLastSearch);
        }
    }

    private void postRequestLastSearch(String getLastSearch){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/LastSearch");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("category_id", getCategory+"");
        paramsPost.put("last_id", getLastSearch);
        if (locationResult != null) {
            paramsPost.put("latitude", String.valueOf(locationResult.getLatitude()));
            paramsPost.put("longitude", String.valueOf(locationResult.getLongitude()));
        }else {
            paramsPost.put("latitude", "");
            paramsPost.put("longitude", "");
        }
        httpCallPost.setParams(paramsPost);
        new LastSearchItems(this).execute(httpCallPost);
    }
}