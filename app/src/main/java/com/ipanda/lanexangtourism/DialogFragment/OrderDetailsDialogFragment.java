package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Adapter.PurchaseOrderImageAdapter;
import com.ipanda.lanexangtourism.Adapter.PurchaseOrderListAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.PaymentTransactionModel;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ChangeOrderList;
import com.ipanda.lanexangtourism.post_booking.HttpPostShopping;

import java.util.ArrayList;
import java.util.HashMap;


public class OrderDetailsDialogFragment extends DialogFragment implements View.OnClickListener , ChangeOrderList {

    //variables
    private static final String TAG = OrderDetailsDialogFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private Button btn_confirm_orders;

    private ImageView img_dismiss_orders;

    private RecyclerView recycler_orders_views;

    private RecyclerView recycler_orders_details;

    private TextView txt_order_totals, txt_scan_qr_code_id;

    private TextView txt_amount_products;

    private TextView txt_exchange_rate;

    private EditText edt_first_name_id, edt_last_name_id, edt_email_id, edt_phone_id, edt_weChat_id, edt_whatsApp_id, edt_discount_id;

    private ArrayList<ProductModel> productList = new ArrayList<>();

    private String userId;

    private PurchaseOrderImageAdapter orderImageAdapter;

    private PurchaseOrderListAdapter orderListAdapter;

    private BookingModel booking = new BookingModel();

    private PaymentTransactionModel payment = new PaymentTransactionModel();

    private ItemsModel items = new ItemsModel();

    private RateModel rateModel;

    public OrderDetailsDialogFragment() {
        // Required empty public constructor
    }

    public static OrderDetailsDialogFragment newInstance() {
        OrderDetailsDialogFragment fragment = new OrderDetailsDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);


        if (getArguments() != null) {
            rateModel = getArguments().getParcelable("EXCHANGE_RATE_MODEL");
            productList = getArguments().getParcelableArrayList("PRODUCT_LIST");
            items = getArguments().getParcelable("ITEMS");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_details_dialog, container, false);

        //views
        btn_confirm_orders = view.findViewById(R.id.btn_confirm_orders);
        img_dismiss_orders = view.findViewById(R.id.img_dismiss_orders);;
        recycler_orders_views = view.findViewById(R.id.recycler_orders_views);
        recycler_orders_details = view.findViewById(R.id.recycler_orders_details);
        txt_order_totals = view.findViewById(R.id.txt_order_totals);
        txt_scan_qr_code_id = view.findViewById(R.id.txt_scan_qr_code_id);
        edt_first_name_id = view.findViewById(R.id.edt_first_name_id);
        edt_last_name_id = view.findViewById(R.id.edt_last_name_id);
        edt_email_id = view.findViewById(R.id.edt_email_id);
        edt_phone_id = view.findViewById(R.id.edt_phone_id);
        edt_weChat_id = view.findViewById(R.id.edt_weChat_id);
        edt_whatsApp_id = view.findViewById(R.id.edt_whatsApp_id);
        edt_discount_id = view.findViewById(R.id.edt_discount_id);
        txt_exchange_rate = view.findViewById(R.id.txt_exchange_rate_id);

        //set on click views
        btn_confirm_orders.setOnClickListener(this);
        img_dismiss_orders.setOnClickListener(this);
        txt_scan_qr_code_id.setOnClickListener(this);

        //set recycler view
        if (productList.size() != 0) {
            setUpRecyclerView();
        }

        return view;
    }

    private void setUpRecyclerView() {

        orderImageAdapter = new PurchaseOrderImageAdapter(getContext(), productList, languageLocale.getLanguage(),rateModel);
        recycler_orders_views.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        recycler_orders_views.setAdapter(orderImageAdapter);


        orderListAdapter = new PurchaseOrderListAdapter(getContext(), productList, languageLocale.getLanguage(), this,txt_order_totals, txt_exchange_rate,rateModel);
        recycler_orders_details.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recycler_orders_details.setAdapter(orderListAdapter);

//        orderImageAdapter.notifyDataSetChanged();
//        orderListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_confirm_orders:
                if (userId != null) {
                    if (getTextBookingDetail()) {
                        setupHttpPost();
                    }
                }else {
                    startActivity(new Intent(getContext(), LoginActivity.class));
                }
                break;
            case R.id.img_dismiss_orders:
                dismiss();
                break;
            case R.id.txt_scan_qr_code_id:
                Bundle bundle = new Bundle();
                DialogFragment dialogFragment = ScanQRCodeDialogFragment.newInstance();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(getFragmentManager(), TAG);
                break;
        }
    }


    @Override
    public void changeOrderList(int defaultValue, int position) {
        try {
            productList.get(position).getProductOrderDetails().setQuantity(defaultValue);
            setUpRecyclerView();
        }catch (Exception e){
            Log.e("Error changeOrderList",e.getMessage().toString());
        }finally {

        }

    }

    @Override
    public void changeDeletedOrderList(ArrayList<ProductModel> productArrayList) {

//        orderImageAdapter = new PurchaseOrderImageAdapter(getContext(), productArrayList, language);
//        recycler_orders_views.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
//        recycler_orders_views.setAdapter(orderImageAdapter);
//
//        orderListAdapter = new PurchaseOrderListAdapter(getContext(), productArrayList, language, this);
//        recycler_orders_details.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
//        recycler_orders_details.setAdapter(orderListAdapter);

    }



    private boolean getTextBookingDetail() {
        if (TextUtils.isEmpty(edt_first_name_id.getText().toString())){
            Toast.makeText(getContext(), "Empty field not allowed!, Fist Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setFirstName(edt_first_name_id.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_last_name_id.getText().toString())){
            Toast.makeText(getContext(), "Empty field not allowed!, Last Name", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setLastName(edt_last_name_id.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_email_id.getText().toString())){
            Toast.makeText(getContext(), "Empty field not allowed!, Email", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setEmail(edt_email_id.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_phone_id.getText().toString())){
            Toast.makeText(getContext(), "Empty field not allowed!, phone", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setPhone(edt_phone_id.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_weChat_id.getText().toString())){
            Toast.makeText(getContext(), "Empty field not allowed!, WeChat", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setWeChatId(edt_weChat_id.getText().toString().replaceAll(" ", ""));
        }
        if (TextUtils.isEmpty(edt_whatsApp_id.getText().toString())){
            Toast.makeText(getContext(), "Empty field not allowed!, WhatsApp", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            payment.setWhatsAppId(edt_whatsApp_id.getText().toString().replaceAll(" ", ""));
        }
            payment.setDiscount(edt_discount_id.getText().toString().replaceAll(" ", ""));
        return true;
    }

    private void setupHttpPost() {
        ArrayList<ProductModel> list = orderListAdapter.productModelArrayList();
        String productId = "";
        String quantity = "";

        for(ProductModel p : list){
            if (p.isSelected()){
                if (list.size() > 1) {
                    productId += p.getProductId() + ",";
                    quantity += p.getProductOrderDetails().getQuantity() + ",";
                }else {
                    productId += p.getProductId();
                    quantity += p.getProductOrderDetails().getQuantity();
                }
            }

        }

        if (userId != null && items != null){
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Booking");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("Items_items_id", String.valueOf(items.getItemsId()));
            paramsPost.put("paymentTransaction_firstName", payment.getFirstName());
            paramsPost.put("paymentTransaction_lastName", payment.getLastName());
            paramsPost.put("paymentTransaction_email", payment.getEmail());
            paramsPost.put("paymentTransaction_phone", payment.getPhone());
            paramsPost.put("paymentTransaction_weChatId", payment.getWeChatId());
            paramsPost.put("paymentTransaction_whatsAppId", payment.getWhatsAppId());
            paramsPost.put("Product_product_id", productId);
            paramsPost.put("orderDetails_quantity", quantity);
            httpCallPost.setParams(paramsPost);

            new HttpPostShopping().execute(httpCallPost);
            Bundle bundle = new Bundle();
            DialogFragment dialogFragment = ConfirmOrdersDialogFragment.newInstance();
            bundle.putString("TITLE","กำลังทำการส่งข้อมูลเพื่อทำการสั่งสินค้า");
            bundle.putString("DETAILS","กรุณารอสักครู่ระบบกำส่งข้อมูลเบื่องต้นไปยังพนักงานของทางร้าน \nหากทางร้านค้าไม่ติดต่อกับสามารถ ติอต่อไปยังช่องทางโดยตรง");
            if (items.getBusinessModel() != null) {
                bundle.putString("PHONE", items.getBusinessModel().getPhone());
            }else {
                bundle.putString("PHONE", "-");
            }
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getFragmentManager(),TAG);
        }
    }
}
