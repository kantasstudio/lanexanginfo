package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.ipanda.lanexangtourism.Adapter.SearchPlacesRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.SelectedPlacesClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class PlacesAddFragment extends DialogFragment implements View.OnClickListener, SelectedPlacesClickListener {

    //variables
    private static String TAG = PlacesAddFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private View view;

    private ImageButton btnDismiss;

    private EditText edtSearchForPlaces;

    private Spinner spinnerProvince, spinnerTypePlaces;

    private ConstraintLayout showSearchSelectedPlaces;

    private CardView showBtnAddNewPlaces, showBtnAddPlaces;

    private RecyclerView mRecyclerPlaces;
    private SearchPlacesRecyclerViewAdapter mSearchPlacesAdapter;
    private ArrayList<ItemsModel> itemsModelArrayList;

    public PlacesAddFragment() {

    }

    public static PlacesAddFragment newInstance() {
        PlacesAddFragment fragment = new PlacesAddFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();


        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_places_add, container, false);

        onCreateView();


        return view;
    }

    public void onCreateView() {
        //views
        btnDismiss = view.findViewById(R.id.imgBtn_dismiss);
        edtSearchForPlaces = view.findViewById(R.id.edt_search_for_places);
        mRecyclerPlaces = view.findViewById(R.id.recyclerView_search_places);
        spinnerProvince = view.findViewById(R.id.spinner_province);
        spinnerTypePlaces = view.findViewById(R.id.spinner_type_places);
        showSearchSelectedPlaces = view.findViewById(R.id.showSearch_selected_places);
        showBtnAddNewPlaces = view.findViewById(R.id.showBtn_add_new_places);
        showBtnAddPlaces = view.findViewById(R.id.showBtn_add_places);

        //set on click
        btnDismiss.setOnClickListener(this);
        showBtnAddNewPlaces.setOnClickListener(this);
        showBtnAddPlaces.setOnClickListener(this);



    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBtn_dismiss:
                dismiss();
                break;
            case R.id.showBtn_add_new_places:
                break;
            case R.id.showBtn_add_places:
                onClickAddPlaces();
                break;
        }
    }

    private void onClickAddPlaces() {
        int REQUEST_CODE = 1001;
        Intent intent = new Intent();
        intent.putExtra("result","result");
        DialogFragment fragment = (PlacesAddFragment) getTargetFragment();
        fragment.onActivityResult(REQUEST_CODE, Activity.RESULT_OK, intent);
        dismiss();
    }

    @Override
    public void itemClickedPlaces(ItemsModel itemsModel, ImageButton imageButton) {
        if (itemsModel.isSelectedItemToPlaces()){
            showSearchSelectedPlaces.setVisibility(View.VISIBLE);
            showBtnAddPlaces.setVisibility(View.GONE);
            showBtnAddNewPlaces.setVisibility(View.VISIBLE);
            itemsModel.setSelectedItemToPlaces(false);
            imageButton.setImageResource(R.drawable.ic_add_light);
        }else {
            showBtnAddPlaces.setVisibility(View.VISIBLE);
            showSearchSelectedPlaces.setVisibility(View.GONE);
            showBtnAddNewPlaces.setVisibility(View.VISIBLE);
            itemsModel.setSelectedItemToPlaces(true);
            imageButton.setImageResource(R.drawable.ic_delete_light);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
