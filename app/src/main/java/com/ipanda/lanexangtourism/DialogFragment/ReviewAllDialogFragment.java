package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Adapter.ReviewsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ReviewClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ReviewsAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ReviewsCallBack;
import com.ipanda.lanexangtourism.items_view.ViewTourismActivity;

import java.util.ArrayList;
import java.util.HashMap;


public class ReviewAllDialogFragment extends DialogFragment implements ReviewsCallBack , ReviewClickListener {

    //variable
    private ImageView img_back_review_all;

    private RecyclerView recycler_reviews;

    private ReviewsRecyclerViewAdapter reviewsAdapter;

    private ItemsModel itemsModel;

    private ChangeLanguageLocale languageLocale;

    private ArrayList<ReviewModel> reviewArrayList;

    public ReviewAllDialogFragment() {
        // Required empty public constructor
    }


    public static ReviewAllDialogFragment newInstance() {
        ReviewAllDialogFragment fragment = new ReviewAllDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        if (getArguments() != null) {
            itemsModel = getArguments().getParcelable("ITEMS_MODEL");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review_all_dialog, container, false);

        img_back_review_all = view.findViewById(R.id.img_back_review_all);
        recycler_reviews = view.findViewById(R.id.recycler_reviews);

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        img_back_review_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSendResItemId();
                dismiss();
            }
        });

        String urlRating = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Review?items_id="+itemsModel.getItemsId();
        new ReviewsAsyncTask(this).execute(urlRating);

        return view;
    }

    private void onSendResItemId() {
        int REQUEST_CODE = 1001;
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",itemsModel);
        intent.putExtra("CATEGORY_ID",itemsModel.getMenuItemModel().getMenuItemId());
        getActivity().startActivityForResult(intent,REQUEST_CODE);
        getActivity().finish();
        dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteReviews(ArrayList<ReviewModel> arrayList) {
        if (arrayList != null){
            Log.e("Log data",arrayList+"");
            this.reviewArrayList = arrayList;
            reviewsAdapter = new ReviewsRecyclerViewAdapter(getContext(), reviewArrayList, languageLocale.getLanguage(),this,false);
            recycler_reviews.setLayoutManager(new LinearLayoutManager(getContext()));
            recycler_reviews.setAdapter(reviewsAdapter);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClicked(View view, ReviewModel reviewModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("REVIEW_MODEL",reviewModel);
        DialogFragment fragment = ReviewDialogFragment.newInstance();
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(),"ReviewAllDialogFragment");
    }

    @Override
    public void itemClickedDelete(View view, ReviewModel reviewModel, int position) {
        String userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Review/delete");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("User_user_id",userId);
        paramsPost.put("review_id", String.valueOf(reviewModel.getReviewId()));
        httpCallPost.setParams(paramsPost);
        new AlertDialog.Builder(getContext())
                .setTitle("คุณต้องการลบข้อมูลหรือไม่")
                .setMessage("....")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new HttpPostRequestAsyncTask().execute(httpCallPost);
                        reviewArrayList.remove(position);
                        reviewsAdapter.notifyItemRemoved(position);
                        reviewsAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .create().show();
    }

    @Override
    public void itemClickedProfile(View view, ReviewModel reviewModel) {
        Bundle bundle = new Bundle();
        bundle.putString("USER_ID",String.valueOf(reviewModel.getUserModel().getUserId()));
        DialogFragment dialogFragment = ViewProfileDialogFragment.newInstance();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(getFragmentManager(),"ViewTourismActivity");
    }
}
