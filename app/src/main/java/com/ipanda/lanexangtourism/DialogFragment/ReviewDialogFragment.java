package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.items_view.ViewTourismActivity;

import java.util.HashMap;


public class ReviewDialogFragment extends DialogFragment {

    //variables
    private static String TAG = ReviewDialogFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private RelativeLayout btn_confirm_review_items;

    private RatingBar ratingBar;

    private EditText edt_review_id;

    private int mStar;

    private String mReview;

    private String userId;

    private ItemsModel itemsModel;

    private ReviewModel reviewModel;

    public ReviewDialogFragment() {
        // Required empty public constructor
    }

    public static ReviewDialogFragment newInstance() {
        ReviewDialogFragment fragment = new ReviewDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();


        if (getArguments() != null) {
            itemsModel = getArguments().getParcelable("ITEMS_MODEL");
            reviewModel = getArguments().getParcelable("REVIEW_MODEL");
        }
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review_dialog, container, false);

        //views
        btn_confirm_review_items = view.findViewById(R.id.btn_confirm_review_items);
        ratingBar = view.findViewById(R.id.ratingBar);
        edt_review_id = view.findViewById(R.id.edt_review_id);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                mStar = (int) ratingBar.getRating();
            }
        });

        btn_confirm_review_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId != null && itemsModel != null){
                    methodPost();
                    onSendResItemId();
                }else if (userId != null && reviewModel != null){
                    methodPostUpdate();
                    onSendResItemId();
                }

            }
        });

        return view;
    }

    private void onSendResItemId() {
        int REQUEST_CODE = 1001;
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",itemsModel);
        intent.putExtra("CATEGORY_ID",itemsModel.getMenuItemModel().getMenuItemId());
        getActivity().startActivityForResult(intent,REQUEST_CODE);
        getActivity().finish();
        dismiss();
    }

    private void methodPostUpdate() {
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Review/update");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("User_user_id",userId);
        paramsPost.put("review_rating", String.valueOf(mStar));
        paramsPost.put("review_text",edt_review_id.getText().toString());
        paramsPost.put("review_id", String.valueOf(reviewModel.getReviewId()));
        httpCallPost.setParams(paramsPost);
        new HttpPostRequestAsyncTask().execute(httpCallPost);
    }

    private void methodPost(){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Review");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("User_user_id",userId);
        paramsPost.put("review_rating", String.valueOf(mStar));
        paramsPost.put("review_text",edt_review_id.getText().toString());
        paramsPost.put("Items_items_id", String.valueOf(itemsModel.getItemsId()));
        httpCallPost.setParams(paramsPost);
        new HttpPostRequestAsyncTask().execute(httpCallPost);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
