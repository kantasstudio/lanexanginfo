package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Activity.ChooseDateCarRentalActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;


public class SearchCarRentalDialogFragment extends DialogFragment implements View.OnClickListener {

    //variables

    private static String TAG = SearchCarRentalDialogFragment.class.getSimpleName();

    private ImageView btn_back;

    private ConstraintLayout con_selected_calendar;

    private ChangeLanguageLocale languageLocale;

    public SearchCarRentalDialogFragment() {
        // Required empty public constructor
    }


    public static SearchCarRentalDialogFragment newInstance() {
        SearchCarRentalDialogFragment fragment = new SearchCarRentalDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();


        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_car_rental_dialog, container, false);

        //views
        btn_back = view.findViewById(R.id.btn_back);
        con_selected_calendar = view.findViewById(R.id.con_selected_calendar);

        //set event onclick
        btn_back.setOnClickListener(this);
        con_selected_calendar.setOnClickListener(this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_back:
                dismiss();
                break;
            case R.id.con_selected_calendar:
                startActivity(new Intent(getContext(), ChooseDateCarRentalActivity.class));
                break;
        }

    }
}
