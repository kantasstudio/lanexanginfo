package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Activity.ResultEventTicketActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;


public class SearchEventTicketDialogFragment extends DialogFragment implements View.OnClickListener {

    //variables
    private ChangeLanguageLocale languageLocale;

    private ImageView btn_back,btn_search;

    private ImageView img_last_search;

    private TextView txt_last_search;

    private AutoCompleteTextView autoCompleteTextSearch;

    private ConstraintLayout con_search_all_package_tour;


    public SearchEventTicketDialogFragment() {
        // Required empty public constructor
    }


    public static SearchEventTicketDialogFragment newInstance() {
        SearchEventTicketDialogFragment fragment = new SearchEventTicketDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_event_ticket_dialog, container, false);

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        //views
        btn_back = view.findViewById(R.id.btn_back);
        btn_search = view.findViewById(R.id.btn_search);
        autoCompleteTextSearch = view.findViewById(R.id.autoCompleteTextSearch);
        img_last_search = view.findViewById(R.id.img_last_search);
        txt_last_search = view.findViewById(R.id.txt_last_search);
        con_search_all_package_tour = view.findViewById(R.id.con_search_all_package_tour);


        //set onclick event
        btn_back.setOnClickListener(this);
        btn_search.setOnClickListener(this);
        con_search_all_package_tour.setOnClickListener(this);

        //set img
        img_last_search.setImageResource(R.drawable.ic_style);

        //set text
        txt_last_search.setText(getResources().getString(R.string.text_event_ticket_all));




        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_back:
                dismiss();
                break;
            case R.id.btn_search:
                startActivity(new Intent(getContext(), ResultEventTicketActivity.class));
                break;
            case R.id.con_search_all_package_tour:
                break;

        }
    }
}
