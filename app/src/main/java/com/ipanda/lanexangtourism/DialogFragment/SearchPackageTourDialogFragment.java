package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.ResultPackageTourActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.R;


public class SearchPackageTourDialogFragment extends DialogFragment implements View.OnClickListener {


    //variables
    private ChangeLanguageLocale languageLocale;

    private ConstraintLayout con_calendar_view;

    private ConstraintLayout con_filter_calendar_view;

    private ConstraintLayout con_last_search_booking_view;

    private ConstraintLayout con_search_all_package_tour;

    private boolean isFilterCalendar = false;

    private ImageView btn_back, btn_search;

    public SearchPackageTourDialogFragment() {
        // Required empty public constructor
    }


    public static SearchPackageTourDialogFragment newInstance() {
        SearchPackageTourDialogFragment fragment = new SearchPackageTourDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_package_tour_dialog, container, false);

        //views
        con_calendar_view = view.findViewById(R.id.con_calendar_view);
        con_filter_calendar_view = view.findViewById(R.id.con_filter_calendar_view);
        con_last_search_booking_view = view.findViewById(R.id.con_last_search_booking_view);
        con_search_all_package_tour = view.findViewById(R.id.con_search_all_package_tour);
        btn_back = view.findViewById(R.id.btn_back);
        btn_search = view.findViewById(R.id.btn_search);

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();



        //set on click event
        con_filter_calendar_view.setOnClickListener(this);
        con_search_all_package_tour.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_search.setOnClickListener(this);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.con_filter_calendar_view:
                if (isFilterCalendar){
                    con_calendar_view.setVisibility(View.GONE);
                    con_last_search_booking_view.setVisibility(View.VISIBLE);
                    isFilterCalendar = false;
                }else {
                    con_calendar_view.setVisibility(View.VISIBLE);
                    con_last_search_booking_view.setVisibility(View.GONE);
                    isFilterCalendar = true;
                }
                break;
            case R.id.con_search_all_package_tour:
                startActivity(new Intent(getContext(), ResultPackageTourActivity.class));
                break;
            case R.id.btn_back:
                dismiss();
                break;
            case R.id.btn_search:
                Toast.makeText(getContext(), "Search..", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(), ResultPackageTourActivity.class));
                break;
        }
    }
}
