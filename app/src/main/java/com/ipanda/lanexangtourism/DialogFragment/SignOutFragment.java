package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.LineLogin;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.post_booking.HttpPostTokenChat;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.api.LineApiClientBuilder;

import java.util.HashMap;


public class SignOutFragment extends DialogFragment implements View.OnClickListener {

    //variables
    private static String TAG = SignOutFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private Button btnSignOut, btnCancel;

    private GoogleSignInClient googleSignInClient;

    private static LineApiClient lineApiClient;


    public SignOutFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();


        //Login Google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getResources().getString(R.string.google_login_app_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(getContext(), gso);

        //Login Line
        LineApiClientBuilder apiClientBuilder = new LineApiClientBuilder(getContext(), LineLogin.CHANNEL_ID);
        lineApiClient = apiClientBuilder.build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_out, container, false);

        //views
        btnSignOut = view.findViewById(R.id.btn_sign_out);
        btnCancel = view.findViewById(R.id.btn_cancel);

        //setup on click
        btnSignOut.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_sign_out:
                getActivity().finish();
                checkEventSignOut();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }

    private void checkEventSignOut() {
        String loginWith = getContext().getSharedPreferences("PREF_APP_UserLoginType", Context.MODE_PRIVATE).getString("APP_UserLoginType", "");
        switch (loginWith){
            case "facebook":
                Toast.makeText(getContext(), "Sign out with "+loginWith+"", Toast.LENGTH_SHORT).show();
                FacebookSdk.sdkInitialize(getContext());
                LoginManager.getInstance().logOut();
                getContext().getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", false).apply();
                getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).edit().putString("APP_USER_ID", null).apply();
                break;
            case "google":
                googleSignInClient.signOut()
                        .addOnCompleteListener((Activity) getContext(), new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(getContext(), "Sign out with "+loginWith+"", Toast.LENGTH_SHORT).show();
                                getContext().getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", false).apply();
                                getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).edit().putString("APP_USER_ID", null).apply();
                            }
                            });
                break;
            case "line":
                new LogoutTask().execute();
                Toast.makeText(getContext(), "Sign out with "+loginWith+"", Toast.LENGTH_SHORT).show();
                break;
        }

        setupHttpPost();

    }

    private void setupHttpPost() {
        String userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        if (userId != null) {
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("user_id", userId);
            paramsPost.put("currentToken", "");
            httpCallPost.setParams(paramsPost);
            new HttpPostTokenChat().execute(httpCallPost);
        }else {

        }
    }

    public class LogoutTask extends AsyncTask<Void, Void, LineApiResponse> {
        final static String TAG = "LogoutTask";

        @Override
        protected LineApiResponse doInBackground(Void... voids) {
            return lineApiClient.logout();
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(LineApiResponse lineApiResponse) {
            super.onPostExecute(lineApiResponse);
            if(lineApiResponse.isSuccess()){

                getContext().getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", false).apply();
                getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).edit().putString("APP_USER_ID", null).apply();
            } else {
                Toast.makeText(getContext(), "Logout Failed", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Logout Failed: " + lineApiResponse.getErrorData().toString());
            }
        }

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
