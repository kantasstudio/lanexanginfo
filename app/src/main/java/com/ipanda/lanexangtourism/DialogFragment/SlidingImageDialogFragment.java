package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ipanda.lanexangtourism.Adapter.SlidingImageViewPagerAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;


public class SlidingImageDialogFragment extends DialogFragment implements View.OnClickListener {

    //variables
    private ViewPager image_viewpager;

    private TabLayout tab_indicator_count;

    private SlidingImageViewPagerAdapter pagerAdapter;

    private ArrayList<ItemsPhotoDetailModel> itemsPhotoDetailModelArrayList;

    private ImageView img_dismiss_sliding;

    private TextView txt_count_sliding;

    private int count = 0, size = 0;

    private ChangeLanguageLocale languageLocale;



    public SlidingImageDialogFragment() {

    }


    public static SlidingImageDialogFragment newInstance() {
        SlidingImageDialogFragment fragment = new SlidingImageDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        if (getArguments() != null) {
            itemsPhotoDetailModelArrayList = getArguments().getParcelableArrayList("ItemsPhotoDetailModels");
            count = getArguments().getInt("SelectedImage",0);
        }
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sliding_image_dialog, container, false);

        //views
        image_viewpager = view.findViewById(R.id.image_viewpager);
        tab_indicator_count = view.findViewById(R.id.tab_indicator_count);
        img_dismiss_sliding = view.findViewById(R.id.img_dismiss_sliding);
        txt_count_sliding = view.findViewById(R.id.txt_count_sliding);

        //set on clicked
        img_dismiss_sliding.setOnClickListener(this);

        //setup viewpager
        pagerAdapter = new SlidingImageViewPagerAdapter(getContext(),itemsPhotoDetailModelArrayList,languageLocale.getLanguage());
        image_viewpager.setAdapter(pagerAdapter);

        //setup tabLayout with viewpager
        tab_indicator_count.setupWithViewPager(image_viewpager);
        //get value position tab
        getTabIndicatorPosition();
        size = itemsPhotoDetailModelArrayList.size();
        txt_count_sliding.setText((count+1)+"/"+size);
        image_viewpager.setCurrentItem(count);

        return view;

    }

    private void getTabIndicatorPosition() {
        tab_indicator_count.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
            @Override
            public void onTabSelected(TabLayout.Tab tab){
                count = tab.getPosition();
                txt_count_sliding.setText((count+1)+"/"+size);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_dismiss_sliding:
                dismiss();
                break;
        }
    }
}
