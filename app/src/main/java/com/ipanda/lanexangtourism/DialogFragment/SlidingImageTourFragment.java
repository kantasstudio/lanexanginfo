package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ipanda.lanexangtourism.Adapter.SlidingImageTourViewPagerAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;


public class SlidingImageTourFragment extends DialogFragment implements View.OnClickListener{

    //variables
    private ViewPager image_viewpager;

    private TabLayout tab_indicator_count;

    private SlidingImageTourViewPagerAdapter pagerAdapter;

    private ArrayList<PhotoTourist> photoTouristArrayList;

    private ImageView img_dismiss_sliding;

    private TextView txt_count_sliding;

    private int count = 0, size = 0;

    private ChangeLanguageLocale languageLocale;


    public SlidingImageTourFragment() {

    }


    public static SlidingImageTourFragment newInstance() {
        SlidingImageTourFragment fragment = new SlidingImageTourFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        if (getArguments() != null) {
            photoTouristArrayList = getArguments().getParcelableArrayList("ItemsPhotoDetailModels");
            count = getArguments().getInt("SelectedImage",0);
        }
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sliding_image_tour, container, false);

        //views
        image_viewpager = view.findViewById(R.id.image_viewpager);
        tab_indicator_count = view.findViewById(R.id.tab_indicator_count);
        img_dismiss_sliding = view.findViewById(R.id.img_dismiss_sliding);
        txt_count_sliding = view.findViewById(R.id.txt_count_sliding);

        //set on clicked
        img_dismiss_sliding.setOnClickListener(this);

        //setup viewpager
        pagerAdapter = new SlidingImageTourViewPagerAdapter(getContext(),photoTouristArrayList,languageLocale.getLanguage());
        image_viewpager.setAdapter(pagerAdapter);

        //setup tabLayout with viewpager
        tab_indicator_count.setupWithViewPager(image_viewpager);
        //get value position tab
        getTabIndicatorPosition();
        size = photoTouristArrayList.size();
        txt_count_sliding.setText((count+1)+"/"+size);
        image_viewpager.setCurrentItem(count);

        return view;
    }

    private void getTabIndicatorPosition() {
        tab_indicator_count.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
            @Override
            public void onTabSelected(TabLayout.Tab tab){
                count = tab.getPosition();
                txt_count_sliding.setText((count+1)+"/"+size);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_dismiss_sliding:
                dismiss();
                break;
        }
    }
}
