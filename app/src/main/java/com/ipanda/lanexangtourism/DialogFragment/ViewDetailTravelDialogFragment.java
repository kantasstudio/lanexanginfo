package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.BookingDetailsActivity;
import com.ipanda.lanexangtourism.Activity.EventTicketDetailsActivity;
import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Adapter.DetailsTravelDetailsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.TravelDetailsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;


public class ViewDetailTravelDialogFragment extends DialogFragment implements View.OnClickListener {

    //variables
    private ChangeLanguageLocale languageLocale;

    private ImageView img_back;

    private String language;

    private ArrayList<TravelDetailsModel> arrayList;

    private RecyclerView mRecyclerDetailTravel;

    private DetailsTravelDetailsRecyclerViewAdapter mAdapterTravelDetails;

    private ItemsModel itemsModel;

    private RateModel rateModel;

    private Button btn_confirm_booking;

    private String type;

    private String userId;

    private Boolean isModeOnline;

    public ViewDetailTravelDialogFragment() {

    }

    public static ViewDetailTravelDialogFragment newInstance() {
        ViewDetailTravelDialogFragment fragment = new ViewDetailTravelDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        if (getArguments() != null) {
            type = getArguments().getString("TYPE_BOOKING");
            arrayList = getArguments().getParcelableArrayList("TRAVEL_DETAILS");
            itemsModel = getArguments().getParcelable("ITEMS_MODEL");
            rateModel = getArguments().getParcelable("EXCHANGE_RATE_MODEL");
        }

        languageLocale = new ChangeLanguageLocale(getContext());
        language = languageLocale.getLanguage();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_view_detail_travel_dialog, container, false);

        //views
        img_back = view.findViewById(R.id.img_back);
        mRecyclerDetailTravel = view.findViewById(R.id.recycler_detail_travel);
        btn_confirm_booking = view.findViewById(R.id.btn_confirm_booking);

        //set on click event
        img_back.setOnClickListener(this);

        btn_confirm_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isModeOnline) {
                    if (type.equals("PackageTour")) {
                        Intent intent = new Intent(getContext(), BookingDetailsActivity.class);
                        intent.putExtra("ITEMS_MODEL", itemsModel);
                        intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                        if (userId != null) {
                            startActivity(intent);
                        } else {
                            startActivity(new Intent(getContext(), LoginActivity.class));
                        }
                    } else {
                        Intent intent = new Intent(getContext(), EventTicketDetailsActivity.class);
                        intent.putExtra("ITEMS_MODEL", itemsModel);
                        intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                        if (userId != null) {
                            startActivity(intent);
                        } else {
                            startActivity(new Intent(getContext(), LoginActivity.class));
                        }
                    }
                }else {
                    Toast.makeText(getContext(), "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mAdapterTravelDetails = new DetailsTravelDetailsRecyclerViewAdapter(getContext(),arrayList, language);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerDetailTravel.setLayoutManager(layoutManager);
        mRecyclerDetailTravel.setAdapter(mAdapterTravelDetails);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:
                dismiss();
                break;
        }
    }
}
