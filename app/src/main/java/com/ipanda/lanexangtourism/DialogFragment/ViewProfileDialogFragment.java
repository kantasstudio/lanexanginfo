package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Activity.BookmarksActivity;
import com.ipanda.lanexangtourism.Activity.ChatMessageActivity;
import com.ipanda.lanexangtourism.Activity.FollowActivity;
import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Activity.OrderDetailsActivity;
import com.ipanda.lanexangtourism.Activity.PersonnelProgramActivity;
import com.ipanda.lanexangtourism.Activity.PersonnelReviewActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.asynctask.UserAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.UserCallBack;
import com.squareup.picasso.Picasso;

import java.util.HashMap;


public class ViewProfileDialogFragment extends DialogFragment implements UserCallBack, View.OnClickListener  {

    //Variables
    private static String TAG = ViewProfileDialogFragment.class.getSimpleName();

    private Toolbar mToolbar;

    private ImageView toolbar_back_id,img_profile;

    private TextView toolbar_title_id;

    private TextView txt_profile_full_name,txt_login_with;

    private TextView txt_count_following, txt_count_followers, txt_count_recordedList, txt_count_review, txt_count_programTour;

    private RelativeLayout btnFollowing, btnFollowers, btnBookmarks, btnReview, btnProgramTour;

    private CardView cv_btn_follow;

    private CardView cv_btn_message;

    private Button btnListMessage,btn_follow_id;

    private UserModel userModel;

    private String userId;

    private String userIdFollow;

    private ChangeLanguageLocale languageLocale;

    public ViewProfileDialogFragment() {

    }


    public static ViewProfileDialogFragment newInstance() {
        ViewProfileDialogFragment fragment = new ViewProfileDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {
            userIdFollow = getArguments().getString("USER_ID");
            if (userId != null){
                if (userId.equals(userIdFollow)){
                    String url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/User/"+userIdFollow;
                    new UserAsyncTask(this).execute(url);
                }else {
                    String url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/User/"+userId+"?follow="+userIdFollow;
                    new UserAsyncTask(this).execute(url);
                }

            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_view_profile_dialog, container, false);

        //views
        mToolbar = view.findViewById(R.id.Toolbar);
        toolbar_back_id = view.findViewById(R.id.toolbar_back_id);
        toolbar_title_id = view.findViewById(R.id.toolbar_title_id);
        img_profile = view.findViewById(R.id.img_profile);
        btnFollowing = view.findViewById(R.id.btn_following);
        btnFollowers = view.findViewById(R.id.btn_followers);
        btnBookmarks = view.findViewById(R.id.btn_bookmarks);
        btnReview = view.findViewById(R.id.btn_review);
        cv_btn_follow = view.findViewById(R.id.cv_btn_follow);
        btnProgramTour = view.findViewById(R.id.btn_program_tour);
        btnListMessage = view.findViewById(R.id.btn_list_message);
        btn_follow_id = view.findViewById(R.id.btn_follow_id);
        txt_profile_full_name = view.findViewById(R.id.txt_profile_full_name);
        txt_login_with = view.findViewById(R.id.txt_login_with);
        txt_count_following = view.findViewById(R.id.txt_count_following);
        txt_count_followers = view.findViewById(R.id.txt_count_followers);
        txt_count_recordedList = view.findViewById(R.id.txt_count_recordedList);
        txt_count_review = view.findViewById(R.id.txt_count_review);
        txt_count_programTour = view.findViewById(R.id.txt_count_programTour);
        cv_btn_message = view.findViewById(R.id.cv_btn_message);

        //setup Toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);


        //set on click button
        toolbar_back_id.setOnClickListener(this);
        btnFollowing.setOnClickListener(this);
        btnFollowers.setOnClickListener(this);
        btnBookmarks.setOnClickListener(this);
        btnReview.setOnClickListener(this);
        btnProgramTour.setOnClickListener(this);
        btnListMessage.setOnClickListener(this);
        btn_follow_id.setOnClickListener(this);



        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(UserModel userModels) {
        if (userModels != null){
            this.userModel = userModels;
            Log.e("check data",userModels+"");
            Picasso.get().load(userModel.getProfilePicUrl()).error(R.drawable.img_laceholder_error).placeholder(R.drawable.img_placeholder).into(img_profile);
            txt_profile_full_name.setText(userModel.getFirstName()+" "+userModel.getLastName());
            toolbar_title_id.setText(userModel.getFirstName()+" "+userModel.getLastName());
            txt_login_with.setText("เข้าระบบผ่าน "+userModel.getLoginWith());
            txt_count_followers.setText(userModels.getCountFollowers()+"");
            txt_count_following.setText(userModels.getCountFollowing()+"");
            txt_count_review.setText(userModels.getCountReview()+"");
            txt_count_recordedList.setText(userModels.getCountRecordedList()+"");
            txt_count_programTour.setText(userModels.getCountProgramTour()+"");

            if (userModels.getUserId() == Integer.parseInt(userId)){
                cv_btn_message.setVisibility(View.GONE);
            }

            if (userId.equals(userIdFollow)){
                cv_btn_follow.setVisibility(View.GONE);
            }else {
                cv_btn_follow.setVisibility(View.VISIBLE);
            }

            if (userModel.isFollowState()){
                btn_follow_id.setText("ติดตามอยู่");
                btn_follow_id.setTextColor(getResources().getColor(R.color.colorTextInputPersonnelInfo));
                cv_btn_follow.setBackground(getResources().getDrawable(R.drawable.shape_button_follow));
            }else {
                btn_follow_id.setText("ติดตาม");
                btn_follow_id.setTextColor(getResources().getColor(R.color.white));
                cv_btn_follow.setBackground(getResources().getDrawable(R.drawable.shape_button));
            }

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_id:
                dismiss();
                break;
            case R.id.btn_following:
                Intent following = new Intent(getContext(), FollowActivity.class);
                following.putExtra("TEXT_FOLLOWER",getString(R.string.text_following));
                following.putExtra("USER_ID",String.valueOf(userModel.getUserId()));
                following.putExtra("TYPE","following");
                startActivity(following);
                break;
            case R.id.btn_followers:
                Intent followers = new Intent(getContext(), FollowActivity.class);
                followers.putExtra("TEXT_FOLLOWER",getString(R.string.text_followers));
                followers.putExtra("USER_ID",String.valueOf(userModel.getUserId()));
                followers.putExtra("TYPE","follower");
                startActivity(followers);
                break;
            case R.id.btn_bookmarks:
                if (userIdFollow != null){
                    Intent bookmarks = new Intent(getContext(), BookmarksActivity.class);
                    bookmarks.putExtra("USER_ID",userIdFollow);
                    startActivity(bookmarks);
                }
                break;
            case R.id.btn_review:
                if (userIdFollow != null){
                    Intent review = new Intent(getContext(), PersonnelReviewActivity.class);
                    review.putExtra("USER_ID",userIdFollow);
                    startActivity(review);
                }
                break;
            case R.id.btn_program_tour:
                startActivity(new Intent(getContext(), PersonnelProgramActivity.class));
                break;
            case R.id.btn_list_message:
                Intent chatMessage = new Intent(getContext(), ChatMessageActivity.class);
                chatMessage.putExtra("Send_To_Name", userModel.getFirstName()+" "+userModel.getLastName());
                chatMessage.putExtra("Send_To_UserId",String.valueOf(userModel.getUserId()));
                chatMessage.putExtra("Send_Dialog",true);
                startActivity(chatMessage);
                break;
            case R.id.btn_follow_id:
                onClickFollow();
                break;
        }
    }

    private void onClickFollow() {

        if (userId != null && userIdFollow != null) {
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Followers");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("followers_user_id", userIdFollow);
            httpCallPost.setParams(paramsPost);
            new HttpPostRequestAsyncTask().execute(httpCallPost);

            if (userModel.isFollowState()){
                userModel.setFollowState(false);
                btn_follow_id.setText("ติดตาม");
                btn_follow_id.setTextColor(getResources().getColor(R.color.white));
                cv_btn_follow.setBackground(getResources().getDrawable(R.drawable.shape_button));

            }else {
                userModel.setFollowState(true);
                btn_follow_id.setText("ติดตามอยู่");
                btn_follow_id.setTextColor(getResources().getColor(R.color.colorTextInputPersonnelInfo));
                cv_btn_follow.setBackground(getResources().getDrawable(R.drawable.shape_button_follow));
            }

        }
    }

    // Reload current fragment
    private void changeFragment() {
        try {
            FragmentManager manager = getFragmentManager();
            ViewProfileDialogFragment dialog = new ViewProfileDialogFragment();
            dialog.show(manager,"FamilyFragment");
        } catch (Exception exception) {
            Log.i("ChangFragment",exception.toString());
        }
    }//End Reload current fragment
}
