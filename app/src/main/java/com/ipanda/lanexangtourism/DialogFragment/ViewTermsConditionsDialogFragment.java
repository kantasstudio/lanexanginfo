package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.BookingDetailsActivity;
import com.ipanda.lanexangtourism.Activity.EventTicketDetailsActivity;
import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Adapter.ConditionTravelDetailsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.TravelPeriodModel;
import com.ipanda.lanexangtourism.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;


public class ViewTermsConditionsDialogFragment extends DialogFragment implements View.OnClickListener {

    //variables
    private ChangeLanguageLocale languageLocale;

    private ImageView img_back;

    private ArrayList<BookingConditionModel> arrayList = new ArrayList<>();

    private RecyclerView mRecyclerConditions;

    private ConditionTravelDetailsRecyclerViewAdapter mAdapterCondition;

    private ItemsModel itemsModel;

    private RateModel rateModel;

    private Button btn_confirm_booking;

    private String userId;

    private String type;

    private TextView txt_currency;

    private TextView txt_price_booking;

    private TextView toolbar_title;

    private Boolean isModeOnline;

    public ViewTermsConditionsDialogFragment() {
        // Required empty public constructor
    }


    public static ViewTermsConditionsDialogFragment newInstance() {
        ViewTermsConditionsDialogFragment fragment = new ViewTermsConditionsDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        if (getArguments() != null) {
            type = getArguments().getString("TYPE_BOOKING");
            arrayList = getArguments().getParcelableArrayList("BOOKING_CONDITION");
            itemsModel = getArguments().getParcelable("ITEMS_MODEL");
            rateModel = getArguments().getParcelable("EXCHANGE_RATE_MODEL");
        }

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLanguage();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_terms_conditions_dialog, container, false);

        //views
        img_back = view.findViewById(R.id.img_back);
        mRecyclerConditions = view.findViewById(R.id.recycler_terms_conditions);
        btn_confirm_booking = view.findViewById(R.id.btn_confirm_booking);
        txt_currency = view.findViewById(R.id.txt_currency);
        txt_price_booking = view.findViewById(R.id.txt_price_booking);
        toolbar_title = view.findViewById(R.id.toolbar_title);


        btn_confirm_booking.setText(getResources().getString(R.string.text_make_booking));
        toolbar_title.setText(getResources().getString(R.string.text_terms_Conditions));

        //set on click event
        img_back.setOnClickListener(this);
        btn_confirm_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isModeOnline) {
                    if (type.equals("PackageTour")) {
                        Intent intent = new Intent(getContext(), BookingDetailsActivity.class);
                        intent.putExtra("ITEMS_MODEL", itemsModel);
                        intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                        if (userId != null) {
                            startActivity(intent);
                        } else {
                            startActivity(new Intent(getContext(), LoginActivity.class));
                        }
                    } else {
                        Intent intent = new Intent(getContext(), EventTicketDetailsActivity.class);
                        intent.putExtra("ITEMS_MODEL", itemsModel);
                        intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                        if (userId != null) {
                            startActivity(intent);
                        } else {
                            startActivity(new Intent(getContext(), LoginActivity.class));
                        }
                    }
                }else {
                    Toast.makeText(getContext(), "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mAdapterCondition = new ConditionTravelDetailsRecyclerViewAdapter(getContext(),arrayList, languageLocale.getLanguage());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerConditions.setLayoutManager(layoutManager);
        mRecyclerConditions.setAdapter(mAdapterCondition);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        if (type.equals("PackageTour")){

            int totals = 0;
            if (itemsModel.getTravelPeriodModelArrayList() != null && itemsModel.getTravelPeriodModelArrayList().size() != 0) {
                for (TravelPeriodModel tp : itemsModel.getTravelPeriodModelArrayList()) {
                    if (tp.getAdultPrice() < tp.getAdultSpecialPrice()) {
                        totals = tp.getAdultPrice();
                    } else {
                        totals = tp.getAdultSpecialPrice();
                    }
                }
            }

            if (rateModel != null) {
                String currency = getContext().getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
                switch (currency) {
                    case "THB":
                        price = decimalFormat.format(totals);
                        txt_price_booking.setText(price);
                        txt_currency.setText("THB");
                        break;
                    case "USD":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                        txt_price_booking.setText(price);
                        txt_currency.setText("USD");
                        break;
                    case "CNY":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateCNY()));
                        txt_price_booking.setText(price);
                        txt_currency.setText("CNY");
                        break;
                }
            }else {
                price = decimalFormat.format(totals);
                txt_price_booking.setText(price);
                txt_currency.setText("THB");
            }
        }else {
            if (rateModel != null) {
                String currency = getContext().getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
                switch (currency) {
                    case "THB":
                        price = decimalFormat.format(itemsModel.getPriceGuestAdult());
                        txt_price_booking.setText(price);
                        txt_currency.setText("THB");
                        break;
                    case "USD":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getPriceGuestAdult(),rateModel.getRateUSD()));
                        txt_price_booking.setText(price);
                        txt_currency.setText("USD");
                        break;
                    case "CNY":
                        price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getPriceGuestAdult(),rateModel.getRateCNY()));
                        txt_price_booking.setText(price);
                        txt_currency.setText("CNY");
                        break;
                }
            }else {
                price = decimalFormat.format(itemsModel.getPriceGuestAdult());
                txt_price_booking.setText(price);
                txt_currency.setText("THB");
            }
        }


        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:
                dismiss();
                break;
        }
    }
}
