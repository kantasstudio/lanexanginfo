package com.ipanda.lanexangtourism.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Activity.BookingDetailsActivity;
import com.ipanda.lanexangtourism.Activity.EventTicketDetailsActivity;
import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Adapter.ThingsMeetRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.ItemsImageClickListener;
import com.ipanda.lanexangtourism.Model.ItemsDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;


public class ViewThingsMeetDialogFragment extends DialogFragment implements View.OnClickListener , ItemsImageClickListener {

    //variables
    private ChangeLanguageLocale languageLocale;

    private ImageView img_back;

    private TextView txt_things_meet;

    private RecyclerView mRecycler;

    private ThingsMeetRecyclerViewAdapter mAdapter;

    private ArrayList<ItemsDetailModel> arrayList;

    private ItemsModel itemsModel;

    private RateModel rateModel;

    private Button btn_confirm_booking;

    private String userId;

    private TextView txt_currency;

    private TextView txt_price_booking;

    private Boolean isModeOnline;

    public ViewThingsMeetDialogFragment() {
        // Required empty public constructor
    }


    public static ViewThingsMeetDialogFragment newInstance() {
        ViewThingsMeetDialogFragment fragment = new ViewThingsMeetDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        if (getArguments() != null) {
            arrayList = getArguments().getParcelableArrayList("DETAIL_THING");
            itemsModel = getArguments().getParcelable("ITEMS_MODEL");
            rateModel = getArguments().getParcelable("EXCHANGE_RATE_MODEL");
        }
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_things_meet_dialog, container, false);

        //views
        img_back = view.findViewById(R.id.img_back);
        txt_things_meet = view.findViewById(R.id.txt_things_meet);
        mRecycler = view.findViewById(R.id.recycler_things_meet);
        btn_confirm_booking = view.findViewById(R.id.btn_confirm_booking);
        txt_currency = view.findViewById(R.id.txt_currency);
        txt_price_booking = view.findViewById(R.id.txt_price_booking);

        //set on click event
        img_back.setOnClickListener(this);
        btn_confirm_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EventTicketDetailsActivity.class);
                intent.putExtra("ITEMS_MODEL", itemsModel);
                intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
                if (isModeOnline) {
                    if (userId != null) {
                        startActivity(intent);
                    } else {
                        startActivity(new Intent(getContext(), LoginActivity.class));
                    }
                }
            }
        });


        mAdapter = new ThingsMeetRecyclerViewAdapter(getContext(),arrayList.get(0).getItemsPhotoDetailModels(),this);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setAdapter(mAdapter);

        switch (languageLocale.getLanguage()){
            case "th":
                txt_things_meet.setText(arrayList.get(0).getDetailTH());
                break;
            case "en":
                txt_things_meet.setText(arrayList.get(0).getDetailEN());
                break;
            case "lo":
                txt_things_meet.setText(arrayList.get(0).getDetailLO());
                break;
            case "zh":
                txt_things_meet.setText(arrayList.get(0).getDetailZH());
                break;
        }

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        if (rateModel != null) {
            String currency = getContext().getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(itemsModel.getPriceGuestAdult());
                    txt_price_booking.setText(price);
                    txt_currency.setText("THB");
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getPriceGuestAdult(),rateModel.getRateUSD()));
                    txt_price_booking.setText(price);
                    txt_currency.setText("USD");
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(itemsModel.getPriceGuestAdult(),rateModel.getRateCNY()));

                    txt_price_booking.setText(price);
                    txt_currency.setText("CNY");
                    break;
            }
        }else {
            price = decimalFormat.format(itemsModel.getPriceGuestAdult());
            txt_price_booking.setText(price);
            txt_currency.setText("THB");
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:
                dismiss();
                break;
        }
    }

    @Override
    public void itemClicked(int position) {
        Bundle bundle = new Bundle();
        DialogFragment fragment = SlidingImageDialogFragment.newInstance();
        bundle.putParcelableArrayList("ItemsPhotoDetailModels",arrayList.get(0).getItemsPhotoDetailModels());
        bundle.putInt("SelectedImage",position);
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(),"ViewTourismActivity");
    }
}
