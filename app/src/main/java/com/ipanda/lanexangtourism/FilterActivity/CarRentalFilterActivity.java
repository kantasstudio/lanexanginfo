package com.ipanda.lanexangtourism.FilterActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.slider.RangeSlider;
import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapter;
import com.ipanda.lanexangtourism.Adapter.FilterTypeOfPackagesAdapter;
import com.ipanda.lanexangtourism.Adapter.SeatsAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.Interface_click.SeatsFilterClickListener;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.RangeModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.SeatsModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.FilterItemsCategoryAsyncTask;
import com.ipanda.lanexangtourism.asynctask.FilterTypeOfPackagesAsyncTask;
import com.ipanda.lanexangtourism.asynctask.PriceRangeAsyncTask;
import com.ipanda.lanexangtourism.asynctask.SeatsAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.FilterItemsCallBack;
import com.ipanda.lanexangtourism.interface_callback.FilterTypeOfPackagesCallBack;
import com.ipanda.lanexangtourism.interface_callback.PriceRangeCallBack;
import com.ipanda.lanexangtourism.interface_callback.SeatsCallBack;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

public class CarRentalFilterActivity extends AppCompatActivity implements FilterItemsCallBack, FilterSelectedClickListener, PriceRangeCallBack,
        ExchangeRateCallBack , SeatsCallBack, SeatsFilterClickListener {

    //Variables
    private Toolbar mToolbar;

    private TextView toolbar_title, toolbar_title_right;

    private TextView txtMax, txtMin;

    private ChangeLanguageLocale languageLocale;

    private RangeSlider rangeSlider;

    private RateModel rateModel;

    private RecyclerView recycler_filter_type_car;

    private FilterItemsAdapter filterItemsAdapter;

    private RecyclerView recycler_filter_seat;

    private SeatsAdapter seatsAdapter;

    private Button btn_confirm;

    private ArrayList<SubCategoryModel> subCategoryArrayList;

    private ArrayList<SeatsModel> seatsArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_rental_filter);

        //views
        mToolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title_right = findViewById(R.id.toolbar_title_right);
        txtMin = findViewById(R.id.txt_min);
        txtMax = findViewById(R.id.txt_max);
        rangeSlider = findViewById(R.id.range_slider);
        recycler_filter_type_car = findViewById(R.id.recycler_filter_type_car);
        recycler_filter_seat = findViewById(R.id.recycler_filter_seat);
        btn_confirm = findViewById(R.id.btn_confirm);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 40, 65, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //setup event onClick
        setupOnClick();

        String urlRate = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ExchangeRate";
        new ExchangeRateAsyncTask(this).execute(urlRate);

        String url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SubCategory/11";
        new FilterItemsCategoryAsyncTask(this).execute(url);

        String urlPriceRange = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Fillter/price_range/11";
        new PriceRangeAsyncTask(this).execute(urlPriceRange);

        String urlSeats = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Fillter/number_of_seats";
        new SeatsAsyncTask(this).execute(urlSeats);

    }

    private void setupOnClick() {
        TextView txt_type_car = findViewById(R.id.txt_type_car);
        ImageView img_arrow_type_car = findViewById(R.id.img_arrow_type_car);
        ExpandableLayout expandable_type_car = findViewById(R.id.expandable_type_car);

        TextView open_start_price = findViewById(R.id.open_start_price);
        ImageView img_arrow_start_price = findViewById(R.id.img_arrow_start_price);
        ExpandableLayout expandable_open_start_price = findViewById(R.id.expandable_open_start_price);

        TextView open_seat = findViewById(R.id.open_seat);
        ImageView img_arrow_seat = findViewById(R.id.img_arrow_seat);
        ExpandableLayout expandable_period_seat = findViewById(R.id.expandable_period_seat);

        TextView open_company = findViewById(R.id.open_company);
        ImageView img_arrow_company = findViewById(R.id.img_arrow_company);
        ExpandableLayout expandable_company = findViewById(R.id.expandable_company);

        txt_type_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandable_type_car.toggle();
                if (expandable_type_car.isExpanded()){
                    img_arrow_type_car.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    img_arrow_type_car.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        open_start_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandable_open_start_price.toggle();
                if (expandable_open_start_price.isExpanded()){
                    img_arrow_start_price.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    img_arrow_start_price.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        open_seat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandable_period_seat.toggle();
                if (expandable_period_seat.isExpanded()){
                    img_arrow_seat.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    img_arrow_seat.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        open_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandable_company.toggle();
                if (expandable_company.isExpanded()){
                    img_arrow_company.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    img_arrow_company.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        toolbar_title_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recreate();
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("getMaxMin",txtMin.getText().toString().replaceAll(",","")+","+txtMax.getText().toString().replaceAll(",",""));
                returnIntent.putParcelableArrayListExtra("sub_category_list",subCategoryArrayList);
                returnIntent.putParcelableArrayListExtra("seats_list",seatsArrayList);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<SeatsModel> seatsArrayList) {
        if (seatsArrayList != null && seatsArrayList.size() != 0){
            seatsAdapter = new SeatsAdapter(this, seatsArrayList, languageLocale.getLanguage(),this);
            recycler_filter_seat.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            recycler_filter_seat.setAdapter(seatsAdapter);
        }
    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }

    @Override
    public void onRequestCompleteListener(CategoryModel categoryModel) {
        if (categoryModel != null){

            if (categoryModel.getSubCategoryModelArrayList() != null && categoryModel.getSubCategoryModelArrayList().size() != 0) {
                filterItemsAdapter = new FilterItemsAdapter(this, categoryModel.getSubCategoryModelArrayList(), languageLocale.getLanguage(),this);
                recycler_filter_type_car.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
                recycler_filter_type_car.setAdapter(filterItemsAdapter);
            }

        }
    }

    @Override
    public void onRequestCompleteListener(RangeModel rangeModel) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        ExchangeRate exchangeRate = new ExchangeRate();
        String max = null;
        String min = null;
        Float getMax = null;
        Float getMin = null;


        String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
        if (rangeModel != null) {
            switch (currency) {
                case "THB":
                    max = decimalFormat.format(rangeModel.getMaximum());
                    min = decimalFormat.format(rangeModel.getMinimum());
                    getMax = rangeModel.getMaximum();
                    getMin = rangeModel.getMinimum();
                    break;
                case "USD":
                    max = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateUSD()));
                    min = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateUSD()));
                    getMax = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateUSD());
                    getMin = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateUSD());
                    break;
                case "CNY":
                    max = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateCNY()));
                    min = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateCNY()));
                    getMax = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateCNY());
                    getMin = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateCNY());
                    break;
            }
        }else {
            max = decimalFormat.format(rangeModel.getMaximum());
            min = decimalFormat.format(rangeModel.getMinimum());
            getMax = rangeModel.getMaximum();
            getMin = rangeModel.getMinimum();
        }

        txtMin.setText(min);
        txtMax.setText(max);
        List<Float> values = new ArrayList<>();
        values.add(getMin);
        values.add(getMax);
        rangeSlider.setValueFrom(getMin);
        rangeSlider.setValueTo(getMax);
        rangeSlider.setValues(values);

        rangeSlider.addOnSliderTouchListener(new RangeSlider.OnSliderTouchListener() {
            @Override
            public void onStartTrackingTouch(@NonNull RangeSlider slider) {
                Log.d("onStartTrackingTouch", slider.getValues().toString());
            }

            @Override
            public void onStopTrackingTouch(@NonNull RangeSlider slider) {
                float getMin = slider.getValues().get(0);
                float getMax = slider.getValues().get(1);
                txtMin.setText(decimalFormat.format(getMin));
                txtMax.setText(decimalFormat.format(getMax));
                Log.d("onStartTrackingTouch", slider.getValues().toString());
            }
        });

    }

    @Override
    public void onRequestFailed(String result) {

    }


    @Override
    public void getFilterSelectedTourist(ArrayList<SubCategoryModel> subCategoryArrayList) {
        if (subCategoryArrayList != null && subCategoryArrayList.size() != 0){
            this.subCategoryArrayList = subCategoryArrayList;
        }
    }

    @Override
    public void getFilterSelectedSeats(ArrayList<SeatsModel> seatsArrayList) {
        if (seatsArrayList != null && seatsArrayList.size() != 0){
            this.seatsArrayList = seatsArrayList;
        }
    }
}
