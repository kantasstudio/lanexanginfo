package com.ipanda.lanexangtourism.FilterActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapterRecyclerView;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.FilterItemsModel;
import com.ipanda.lanexangtourism.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class ContactFilterActivity extends AppCompatActivity {

    //variables
    private static String TAG = ContactFilterActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ExpandableLayout mShowContentTypeContact, mShowContentTravel;
    
    private ImageView mImgUpDownTypeContact, mImgUpDownTravel;

    private RecyclerView mRecyclerViewTypeContact, mRecyclerViewTravel;

    private FilterItemsAdapterRecyclerView mAdapterTypeContact, mAdapterTravel;

    private ArrayList<FilterItemsModel> itemsTypeContactArrayList, itemsTravelArrayList;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_filter);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //init set data
        setDataTypeContact();
        setDataTravel();

        //setup recyclerview
        setupRecyclerViewTypeContact();
        setupRecyclerViewTravel();

    }

    private void setDataTypeContact() {
        itemsTypeContactArrayList = new ArrayList<>();
        itemsTypeContactArrayList.add(new FilterItemsModel(0,"แจ้งเหตุด่วน เหตุร้าย",false));
        itemsTypeContactArrayList.add(new FilterItemsModel(1,"โรงพยาบาล",false));
        itemsTypeContactArrayList.add(new FilterItemsModel(2,"ศูนย์ข้อมูลนักท่องเที่ยว",false));
    }

    private void setDataTravel() {
        itemsTravelArrayList = new ArrayList<>();
        itemsTravelArrayList.add(new FilterItemsModel(0,"การเดินทางด้วยเที่่ยวบิน",false));
        itemsTravelArrayList.add(new FilterItemsModel(1,"การเดินทางด้วยรถประจำทาง",false));
        itemsTravelArrayList.add(new FilterItemsModel(2,"การเดินทางด้วยรถไฟ",false));
        itemsTravelArrayList.add(new FilterItemsModel(3,"การเดินทางด้วยเรือ",false));
    }

    private void setupRecyclerViewTypeContact() {
        mRecyclerViewTypeContact = findViewById(R.id.recycler_filter_type_contact);
        mAdapterTypeContact = new FilterItemsAdapterRecyclerView(this,itemsTypeContactArrayList);
        mRecyclerViewTypeContact.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewTypeContact.setAdapter(mAdapterTypeContact);
    }

    private void setupRecyclerViewTravel() {
        mRecyclerViewTravel = findViewById(R.id.recycler_filter_travel);
        mAdapterTravel = new FilterItemsAdapterRecyclerView(this,itemsTravelArrayList);
        mRecyclerViewTravel.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewTravel.setAdapter(mAdapterTravel);
    }

    public void showContentOpenTypeContact(View view){
        //views
        mShowContentTypeContact = findViewById(R.id.expandable_type_contact);
        mImgUpDownTypeContact = findViewById(R.id.img_arrow_type_contact);
        mShowContentTypeContact.toggle();
        if (mShowContentTypeContact.isExpanded()){
            mImgUpDownTypeContact.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownTypeContact.setImageResource(R.drawable.ic_arrow_right);
        }
    }

    public void showContentOpenTravel(View view){
        //views
        mShowContentTravel = findViewById(R.id.expandable_travel);
        mImgUpDownTravel = findViewById(R.id.img_arrow_travel);
        mShowContentTravel.toggle();
        if (mShowContentTravel.isExpanded()){
            mImgUpDownTravel.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownTravel.setImageResource(R.drawable.ic_arrow_right);
        }
    }

}
