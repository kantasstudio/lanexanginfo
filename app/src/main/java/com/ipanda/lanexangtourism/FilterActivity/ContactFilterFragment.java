package com.ipanda.lanexangtourism.FilterActivity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapter;
import com.ipanda.lanexangtourism.Adapter.FilterItemsTravelingAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedTravelingClickListener;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.FilterItemsCategoryAsyncTask;
import com.ipanda.lanexangtourism.asynctask.FilterItemsTravelingAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.FilterItemsCallBack;
import com.ipanda.lanexangtourism.interface_callback.FilterTravelingCallBack;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class ContactFilterFragment extends DialogFragment implements FilterItemsCallBack, FilterTravelingCallBack , FilterSelectedClickListener, FilterSelectedTravelingClickListener {

    //variables
    private static final String TAG = ContactFilterFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private int category;

    private String userId, url;

    private ImageView imgBack;

    private Button btnConfirm;

    private ConstraintLayout conTypeContactFilter;

    private ImageView imgArrowTypeContact;

    private ExpandableLayout expandableTypeContact;

    private RecyclerView recyclerFilterTypeContact;

    private FilterItemsAdapter filterItemsAdapter;

    private ConstraintLayout conTravelingFilter;

    private ImageView imgArrowTravel;

    private ExpandableLayout expandableTravel;

    private RecyclerView recyclerFilterTravel;

    private FilterItemsTravelingAdapter travelingAdapter;

    private Bundle result = new Bundle();

    private ArrayList<SubCategoryModel> subCategoryArrayList;

    private ArrayList<SubCategoryModel> travelingArrayList;


    public ContactFilterFragment() {
        // Required empty public constructor
    }


    public static ContactFilterFragment newInstance() {
        ContactFilterFragment fragment = new ContactFilterFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        if (getArguments() != null) {
            category = getArguments().getInt("CATEGORY_ID",0);
            url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SubCategory/"+category;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_filter, container, false);

        //views
        imgBack = view.findViewById(R.id.img_back);
        btnConfirm = view.findViewById(R.id.btn_confirm);
        conTypeContactFilter = view.findViewById(R.id.con_type_contact_filter);
        imgArrowTypeContact = view.findViewById(R.id.img_arrow_type_contact);
        expandableTypeContact = view.findViewById(R.id.expandable_type_contact);
        recyclerFilterTypeContact = view.findViewById(R.id.recycler_filter_type_contact);

        conTravelingFilter = view.findViewById(R.id.con_traveling_filter);
        imgArrowTravel = view.findViewById(R.id.img_arrow_travel);
        expandableTravel = view.findViewById(R.id.expandable_travel);
        recyclerFilterTravel = view.findViewById(R.id.recycler_filter_travel);


        String urlTraveling = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SubCategory/7";

        //set AsyncTack
        if (category != 0) {
            new FilterItemsCategoryAsyncTask(this).execute(url);
            new FilterItemsTravelingAsyncTask(this).execute(urlTraveling);
        }

        //setup event onClicked
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        conTypeContactFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableTypeContact.toggle();
                if (expandableTypeContact.isExpanded()){
                    imgArrowTypeContact.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    imgArrowTypeContact.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        conTravelingFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableTravel.toggle();
                if (expandableTravel.isExpanded()){
                    imgArrowTravel.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    imgArrowTravel.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.putParcelableArrayList("sub_category_list",subCategoryArrayList);
                result.putParcelableArrayList("traveling_list",travelingArrayList);
                getParentFragmentManager().setFragmentResult("key_menu_six", result);
                dismiss();
            }
        });


        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerTraveling(CategoryModel categoryModel) {
        if (categoryModel != null){
            Log.e("check data", categoryModel+"");

            travelingAdapter = new FilterItemsTravelingAdapter(getContext(), categoryModel.getSubCategoryModelArrayList(), languageLocale.getLanguage(), this::getFilterSelectedTraveling);
            recyclerFilterTravel.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerFilterTravel.setAdapter(travelingAdapter);

        }
    }

    @Override
    public void onRequestCompleteListener(CategoryModel categoryModel) {
        if (categoryModel != null){
            Log.e("check data", categoryModel+"");

            filterItemsAdapter = new FilterItemsAdapter(getContext(), categoryModel.getSubCategoryModelArrayList(), languageLocale.getLanguage(), this::getFilterSelectedTourist);
            recyclerFilterTypeContact.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerFilterTypeContact.setAdapter(filterItemsAdapter);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void getFilterSelectedTourist(ArrayList<SubCategoryModel> subCategoryArrayList) {
        this.subCategoryArrayList = subCategoryArrayList;
    }

    @Override
    public void getFilterSelectedTraveling(ArrayList<SubCategoryModel> travelingArrayList) {
        this.travelingArrayList = travelingArrayList;
    }
}