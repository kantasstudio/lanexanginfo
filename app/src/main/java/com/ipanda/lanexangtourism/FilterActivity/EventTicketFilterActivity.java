package com.ipanda.lanexangtourism.FilterActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.slider.RangeSlider;
import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Adapter.FilterTypeOfPackagesAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.RangeModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.FilterTypeOfPackagesAsyncTask;
import com.ipanda.lanexangtourism.asynctask.PriceRangeAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.FilterTypeOfPackagesCallBack;
import com.ipanda.lanexangtourism.interface_callback.PriceRangeCallBack;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

public class EventTicketFilterActivity extends AppCompatActivity implements FilterTypeOfPackagesCallBack, FilterSelectedClickListener ,PriceRangeCallBack, ExchangeRateCallBack {

    //Variables
    private static String TAG = EventTicketFilterActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private Toolbar mToolbar;

    private TextView toolbar_title, toolbar_title_right;

    private TextView txtMax, txtMin;

    private RangeSlider rangeSlider;

    private RateModel rateModel;

    private RecyclerView recycler_filter_company_package_tour;

    private RecyclerView recycler_filter_type_ticket;

    private FilterTypeOfPackagesAdapter filterTypeOfPackagesAdapter;

    private Button btn_confirm;

    private ArrayList<SubCategoryModel> subCategoryArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_ticket_filter);

        //views
        mToolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title_right = findViewById(R.id.toolbar_title_right);
        txtMin = findViewById(R.id.txt_min);
        txtMax = findViewById(R.id.txt_max);
        recycler_filter_type_ticket = findViewById(R.id.recycler_filter_type_ticket);
        recycler_filter_company_package_tour = findViewById(R.id.recycler_filter_company_package_tour);
        rangeSlider = findViewById(R.id.range_slider);
        btn_confirm = findViewById(R.id.btn_confirm);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //setup event onClick
        setupOnClick();

        String urlRate = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ExchangeRate";
        new ExchangeRateAsyncTask(this).execute(urlRate);

        String url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SubCategory/10";
        new FilterTypeOfPackagesAsyncTask(this).execute(url);

        String urlPriceRange = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Fillter/price_range/10";
        new PriceRangeAsyncTask(this).execute(urlPriceRange);

    }

    private void setupOnClick() {
        TextView open_start_price = findViewById(R.id.open_start_price);
        ImageView img_arrow_start_price = findViewById(R.id.img_arrow_start_price);
        ExpandableLayout expandable_open_start_price = findViewById(R.id.expandable_open_start_price);

        TextView open_company = findViewById(R.id.open_company);
        ImageView img_arrow_company_package_tour = findViewById(R.id.img_arrow_company_package_tour);
        ExpandableLayout expandable_company_package_tour = findViewById(R.id.expandable_company_package_tour);

        TextView open_type_ticket = findViewById(R.id.open_type_ticket);
        ImageView img_arrow_type_ticker = findViewById(R.id.img_arrow_type_ticker);
        ExpandableLayout expandable_type_ticket = findViewById(R.id.expandable_type_ticket);

        open_start_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandable_open_start_price.toggle();
                if (expandable_open_start_price.isExpanded()){
                    img_arrow_start_price.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    img_arrow_start_price.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        open_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandable_company_package_tour.toggle();
                if (expandable_company_package_tour.isExpanded()){
                    img_arrow_company_package_tour.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    img_arrow_company_package_tour.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        open_type_ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandable_type_ticket.toggle();
                if (expandable_type_ticket.isExpanded()){
                    img_arrow_type_ticker.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    img_arrow_type_ticker.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        toolbar_title_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recreate();
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("getMaxMin",txtMin.getText().toString().replaceAll(",","")+","+txtMax.getText().toString().replaceAll(",",""));
                returnIntent.putParcelableArrayListExtra("sub_category_list",subCategoryArrayList);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }

    @Override
    public void onRequestCompleteListener(CategoryModel categoryModel) {
        if (categoryModel != null){

            if (categoryModel.getSubCategoryModelArrayList() != null && categoryModel.getSubCategoryModelArrayList().size() != 0) {
                filterTypeOfPackagesAdapter = new FilterTypeOfPackagesAdapter(this, categoryModel.getSubCategoryModelArrayList(), languageLocale.getLanguage(), this);
                recycler_filter_type_ticket.setLayoutManager(new GridLayoutManager(this, 2));
                recycler_filter_type_ticket.setAdapter(filterTypeOfPackagesAdapter);
            }

        }
    }

    @Override
    public void onRequestCompleteListener(RangeModel rangeModel) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        ExchangeRate exchangeRate = new ExchangeRate();
        String max = null;
        String min = null;
        Float getMax = null;
        Float getMin = null;


        String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
        if (rangeModel != null) {
            switch (currency) {
                case "THB":
                    max = decimalFormat.format(rangeModel.getMaximum());
                    min = decimalFormat.format(rangeModel.getMinimum());
                    getMax = rangeModel.getMaximum();
                    getMin = rangeModel.getMinimum();
                    break;
                case "USD":
                    max = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateUSD()));
                    min = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateUSD()));
                    getMax = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateUSD());
                    getMin = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateUSD());
                    break;
                case "CNY":
                    max = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateCNY()));
                    min = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateCNY()));
                    getMax = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateCNY());
                    getMin = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateCNY());
                    break;
            }
        }else {
            max = decimalFormat.format(rangeModel.getMaximum());
            min = decimalFormat.format(rangeModel.getMinimum());
            getMax = rangeModel.getMaximum();
            getMin = rangeModel.getMinimum();
        }

        txtMin.setText(min);
        txtMax.setText(max);
        List<Float> values = new ArrayList<>();
        values.add(getMin);
        values.add(getMax);
        rangeSlider.setValueFrom(getMin);
        rangeSlider.setValueTo(getMax);
        rangeSlider.setValues(values);

        rangeSlider.addOnSliderTouchListener(new RangeSlider.OnSliderTouchListener() {
            @Override
            public void onStartTrackingTouch(@NonNull RangeSlider slider) {
                Log.d("onStartTrackingTouch", slider.getValues().toString());
            }

            @Override
            public void onStopTrackingTouch(@NonNull RangeSlider slider) {
                float getMin = slider.getValues().get(0);
                float getMax = slider.getValues().get(1);
                txtMin.setText(decimalFormat.format(getMin));
                txtMax.setText(decimalFormat.format(getMax));
                Log.d("onStartTrackingTouch", slider.getValues().toString());
            }
        });

    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void getFilterSelectedTourist(ArrayList<SubCategoryModel> subCategoryArrayList) {
        if (subCategoryArrayList != null && subCategoryArrayList.size() != 0){
            this.subCategoryArrayList = subCategoryArrayList;
        }
    }
}
