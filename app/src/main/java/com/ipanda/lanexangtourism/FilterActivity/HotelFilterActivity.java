package com.ipanda.lanexangtourism.FilterActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapterRecyclerView;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.FilterItemsModel;
import com.ipanda.lanexangtourism.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;


public class HotelFilterActivity extends AppCompatActivity {

    //variables
    private static String TAG = HotelFilterActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ExpandableLayout mShowContentStartPrice, mShowContentRateHotel,mShowContentTypeHotel , mShowContentFacilities;

    private ImageView mImgUpDownStartPrice, mImgUpDownRateHotel, mImgUpDownTypeHotel, mImgUpDownFacilities;

    private RecyclerView mRecyclerViewTypeHotel, mRecyclerViewFacilities;

    private FilterItemsAdapterRecyclerView mAdapterTypeHotel, mAdapterFacilities;

    private ArrayList<FilterItemsModel> itemsModelArrayList, itemsFacilitiesArrayList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_filter);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //init set data
        setDataTypeHotel();
        setDataFacilities();

        //setup recyclerview
        setupRecyclerViewTypeHotel();
        setupRecyclerViewFacilities();

    }

    private void setDataFacilities() {
        itemsFacilitiesArrayList = new ArrayList<>();
        itemsFacilitiesArrayList.add(new FilterItemsModel(0,"Wi-Fi ฟรี",false));
        itemsFacilitiesArrayList.add(new FilterItemsModel(1,"ที่จอดรถ",false));
        itemsFacilitiesArrayList.add(new FilterItemsModel(2,"ฟิตเนส",false));
        itemsFacilitiesArrayList.add(new FilterItemsModel(3,"สัตว์เลี้ยงเข้าได้",false));
        itemsFacilitiesArrayList.add(new FilterItemsModel(4,"ลิฟ",false));
        itemsFacilitiesArrayList.add(new FilterItemsModel(5,"สระว่ายน้ำ",false));
        itemsFacilitiesArrayList.add(new FilterItemsModel(6,"สปา",false));
        itemsFacilitiesArrayList.add(new FilterItemsModel(7,"แผนกต้อนรับ 24 ชม.",false));
        itemsFacilitiesArrayList.add(new FilterItemsModel(8,"ร้านอาหาร",false));
        itemsFacilitiesArrayList.add(new FilterItemsModel(9,"บริการรับ-ส่ง",false));
    }


    private void setDataTypeHotel() {
        itemsModelArrayList = new ArrayList<>();
        itemsModelArrayList.add(new FilterItemsModel(0,"อื่นๆ",false));
        itemsModelArrayList.add(new FilterItemsModel(1,"โรงแรมขนาดใหญ่",false));
        itemsModelArrayList.add(new FilterItemsModel(2,"โรงแรมขนาดกลาง",false));
        itemsModelArrayList.add(new FilterItemsModel(3,"โรงแรมราคาประหยัด",false));
        itemsModelArrayList.add(new FilterItemsModel(4,"โรงแรมเยาวชน",false));
        itemsModelArrayList.add(new FilterItemsModel(5,"อพาร์ตเมนต์",false));
        itemsModelArrayList.add(new FilterItemsModel(6,"โฮมสเตย์",false));
        itemsModelArrayList.add(new FilterItemsModel(7,"รีสอร์ต",false));
        itemsModelArrayList.add(new FilterItemsModel(8,"โฮสเทล",false));
        itemsModelArrayList.add(new FilterItemsModel(9,"แคมป์",false));
        itemsModelArrayList.add(new FilterItemsModel(10,"วิลล่า",false));
    }

    private void setupRecyclerViewTypeHotel() {
        mRecyclerViewTypeHotel = findViewById(R.id.recycler_filter_type_hotel);
        mAdapterTypeHotel = new FilterItemsAdapterRecyclerView(this,itemsModelArrayList);
        mRecyclerViewTypeHotel.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewTypeHotel.setAdapter(mAdapterTypeHotel);
    }

    private void setupRecyclerViewFacilities() {
        mRecyclerViewFacilities = findViewById(R.id.recycler_filter_facilities);
        mAdapterFacilities = new FilterItemsAdapterRecyclerView(this,itemsFacilitiesArrayList);
        mRecyclerViewFacilities.setLayoutManager(new GridLayoutManager(this,1));
        mRecyclerViewFacilities.setAdapter(mAdapterFacilities);
    }


    public void showContentOpenStartPrice(View view){
        //views
        mShowContentStartPrice = findViewById(R.id.expandable_open_start_price);
        mImgUpDownStartPrice = findViewById(R.id.img_arrow_start_price);
        mShowContentStartPrice.toggle();
        if (mShowContentStartPrice.isExpanded()){
            mImgUpDownStartPrice.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownStartPrice.setImageResource(R.drawable.ic_arrow_right);
        }

    }

    public void showContentOpenRateHotel(View view){
        //views
        mShowContentRateHotel = findViewById(R.id.expandable_rate_hotel);
        mImgUpDownRateHotel = findViewById(R.id.img_arrow_rate_hotel);
        mShowContentRateHotel.toggle();
        if (mShowContentRateHotel.isExpanded()){
            mImgUpDownRateHotel.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownRateHotel.setImageResource(R.drawable.ic_arrow_right);
        }

    }

    public void showContentOpenTypeHotel(View view){
        mShowContentTypeHotel = findViewById(R.id.expandable_type_hotel);
        mImgUpDownTypeHotel = findViewById(R.id.img_arrow_type_hotel);
        mShowContentTypeHotel.toggle();
        if (mShowContentTypeHotel.isExpanded()){
            mImgUpDownTypeHotel.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownTypeHotel.setImageResource(R.drawable.ic_arrow_right);
        }
    }

    public void showContentOpenFacilities(View view){
        mShowContentFacilities = findViewById(R.id.expandable_facilities);
        mImgUpDownFacilities = findViewById(R.id.img_arrow_facilities);
        mShowContentFacilities.toggle();
        if (mShowContentFacilities.isExpanded()){
            mImgUpDownFacilities.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownFacilities.setImageResource(R.drawable.ic_arrow_right);
        }
    }

}
