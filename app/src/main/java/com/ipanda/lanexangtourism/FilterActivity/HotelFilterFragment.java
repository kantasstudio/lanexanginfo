package com.ipanda.lanexangtourism.FilterActivity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.material.slider.RangeSlider;
import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapter;
import com.ipanda.lanexangtourism.Adapter.FilterItemsFacilitiesAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedFacilitiesClickListener;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.FacilitiesModel;
import com.ipanda.lanexangtourism.Model.RangeModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.FilterItemsCategoryAsyncTask;
import com.ipanda.lanexangtourism.asynctask.FilterItemsFacilitiesAsyncTask;
import com.ipanda.lanexangtourism.asynctask.FilterTypeOfPackagesAsyncTask;
import com.ipanda.lanexangtourism.asynctask.PriceRangeAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.FilterFacilitiesCallBack;
import com.ipanda.lanexangtourism.interface_callback.FilterItemsCallBack;
import com.ipanda.lanexangtourism.interface_callback.PriceRangeCallBack;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;


public class HotelFilterFragment extends DialogFragment implements FilterItemsCallBack, FilterFacilitiesCallBack, FilterSelectedClickListener, FilterSelectedFacilitiesClickListener
, PriceRangeCallBack , ExchangeRateCallBack {

    //variables
    private static final String TAG = HotelFilterFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private int category;

    private String userId, url;

    private ImageView imgBack;

    private Button btnConfirm;

    private ConstraintLayout conStartPriceFilter;

    private ImageView imgArrowStartPrice;

    private ExpandableLayout expandableStartPrice;

    private TextView txtMax, txtMin;

    private SeekBar seekBar;

    private ConstraintLayout conLevelFilter;

    private ImageView imgArrowRateHotel;

    private ExpandableLayout expandableRateHotel;

    private LinearLayout linStarOne, linStarTwo, linStarThree, linStarFour, linStarFive;

    private boolean isStarOne, isStarTwo, isStarThree, isStarFour, isStarFive;

    private ConstraintLayout conTypeHotelFilter;

    private ImageView imgArrowTypeHotel;

    private ExpandableLayout expandableTypeHotel;

    private RecyclerView recyclerFilterTypeHotel;

    private FilterItemsAdapter filterItemsAdapter;

    private ConstraintLayout conFacilityFilter;

    private ImageView imgArrowFacilities;

    private ExpandableLayout expandableFacilities;

    private RecyclerView recyclerFilterFacilities;

    private FilterItemsFacilitiesAdapter facilitiesAdapter;

    private Bundle result = new Bundle();

    private ArrayList<SubCategoryModel> subCategoryArrayList;

    private ArrayList<FacilitiesModel> facilitiesArrayList;

    private RangeSlider rangeSlider;

    private RateModel rateModel;

    private Float getMin = null;

    private Float getMax = null;

    public HotelFilterFragment() {
        // Required empty public constructor
    }


    public static HotelFilterFragment newInstance() {
        HotelFilterFragment fragment = new HotelFilterFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        if (getArguments() != null) {
            category = getArguments().getInt("CATEGORY_ID",0);
            url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SubCategory/"+category;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hotel_filter, container, false);

        //views
        imgBack = view.findViewById(R.id.img_back);
        btnConfirm = view.findViewById(R.id.btn_confirm);

        conStartPriceFilter = view.findViewById(R.id.con_start_price_filter);
        imgArrowStartPrice = view.findViewById(R.id.img_arrow_start_price);
        expandableStartPrice = view.findViewById(R.id.expandable_open_start_price);
        txtMax = view.findViewById(R.id.txt_max);
        txtMin = view.findViewById(R.id.txt_min);
        seekBar = view.findViewById(R.id.seekBar);
        rangeSlider = view.findViewById(R.id.range_slider);

        conLevelFilter = view.findViewById(R.id.con_level_filter);
        imgArrowRateHotel = view.findViewById(R.id.img_arrow_rate_hotel);
        expandableRateHotel = view.findViewById(R.id.expandable_rate_hotel);
        linStarOne = view.findViewById(R.id.linStarOne);
        linStarTwo = view.findViewById(R.id.linStarTwo);
        linStarThree = view.findViewById(R.id.linStarThree);
        linStarFour = view.findViewById(R.id.linStarFour);
        linStarFive = view.findViewById(R.id.linStarFive);

        conTypeHotelFilter = view.findViewById(R.id.con_type_hotel_filter);
        imgArrowTypeHotel = view.findViewById(R.id.img_arrow_type_hotel);
        expandableTypeHotel = view.findViewById(R.id.expandable_type_hotel);
        recyclerFilterTypeHotel = view.findViewById(R.id.recycler_filter_type_hotel);

        conFacilityFilter = view.findViewById(R.id.con_facility_filter);
        imgArrowFacilities = view.findViewById(R.id.img_arrow_facilities);
        expandableFacilities = view.findViewById(R.id.expandable_facilities);
        recyclerFilterFacilities = view.findViewById(R.id.recycler_filter_facilities);

        //set AsyncTack
        if (category != 0) {
            new FilterItemsCategoryAsyncTask(this).execute(url);
        }

        String urlRate = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ExchangeRate";
        new ExchangeRateAsyncTask(this).execute(urlRate);

        String urlFacilities = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/Facilities";
        new FilterItemsFacilitiesAsyncTask(this).execute(urlFacilities);

        String urlPriceRange = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Fillter/price_range/5";
        new PriceRangeAsyncTask(this).execute(urlPriceRange);

        //setup event onClicked
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        conStartPriceFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableStartPrice.toggle();
                if (expandableStartPrice.isExpanded()){
                    imgArrowStartPrice.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    imgArrowStartPrice.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        conLevelFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableRateHotel.toggle();
                if (expandableRateHotel.isExpanded()){
                    imgArrowRateHotel.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    imgArrowRateHotel.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        conTypeHotelFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableTypeHotel.toggle();
                if (expandableTypeHotel.isExpanded()){
                    imgArrowTypeHotel.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    imgArrowTypeHotel.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        conFacilityFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableFacilities.toggle();
                if (expandableFacilities.isExpanded()){
                    imgArrowFacilities.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    imgArrowFacilities.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        linStarOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStarOne){
                    isStarOne = false;
                    linStarOne.setBackground(getResources().getDrawable(R.drawable.shape_hotel_selected));
                }else {
                    isStarOne = true;
                    linStarOne.setBackground(getResources().getDrawable(R.drawable.shape_filter_hotel_selected));
                }
            }
        });

        linStarTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStarTwo){
                    isStarTwo = false;
                    linStarTwo.setBackground(getResources().getDrawable(R.drawable.shape_hotel_selected));
                }else {
                    isStarTwo = true;
                    linStarTwo.setBackground(getResources().getDrawable(R.drawable.shape_filter_hotel_selected));
                }
            }
        });

        linStarThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStarThree){
                    isStarThree = false;
                    linStarThree.setBackground(getResources().getDrawable(R.drawable.shape_hotel_selected));
                }else {
                    isStarThree = true;
                    linStarThree.setBackground(getResources().getDrawable(R.drawable.shape_filter_hotel_selected));
                }
            }
        });

        linStarFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStarFour){
                    isStarFour = false;
                    linStarFour.setBackground(getResources().getDrawable(R.drawable.shape_hotel_selected));
                }else {
                    isStarFour = true;
                    linStarFour.setBackground(getResources().getDrawable(R.drawable.shape_filter_hotel_selected));
                }
            }
        });

        linStarFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStarFive){
                    isStarFive = false;
                    linStarFive.setBackground(getResources().getDrawable(R.drawable.shape_hotel_selected));
                }else {
                    isStarFive = true;
                    linStarFive.setBackground(getResources().getDrawable(R.drawable.shape_filter_hotel_selected));
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.putBoolean("is_one",isStarOne);
                result.putBoolean("is_two",isStarTwo);
                result.putBoolean("is_three",isStarThree);
                result.putBoolean("is_four",isStarFour);
                result.putBoolean("is_five",isStarFive);
                result.putParcelableArrayList("sub_category_list",subCategoryArrayList);
                result.putParcelableArrayList("facilities_list",facilitiesArrayList);
                String max = txtMax.getText().toString().replaceAll(",","");
                String min = txtMin.getText().toString().replaceAll(",","");
                result.putString("getMax",max);
                result.putString("getMin",min);
                getParentFragmentManager().setFragmentResult("key_menu_five", result);
                dismiss();
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }

    @Override
    public void onRequestCompleteListener(RangeModel rangeModel) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        ExchangeRate exchangeRate = new ExchangeRate();
        String max = null;
        String min = null;
        Float getMax = null;
        Float getMin = null;
        if (rangeModel != null){

            String currency = getContext().getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            if (rangeModel != null) {
                if (currency != null) {
                    switch (currency) {
                        case "THB":
                            max = decimalFormat.format(rangeModel.getMaximum());
                            min = decimalFormat.format(rangeModel.getMinimum());
                            getMax = rangeModel.getMaximum();
                            getMin = rangeModel.getMinimum();
                            break;
                        case "USD":
                            max = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateUSD()));
                            min = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateUSD()));
                            getMax = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateUSD());
                            getMin = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateUSD());
                            break;
                        case "CNY":
                            max = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateCNY()));
                            min = decimalFormat.format(exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateCNY()));
                            getMax = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMaximum(), rateModel.getRateCNY());
                            getMin = (float) exchangeRate.getExchangeRateUSD(rangeModel.getMinimum(), rateModel.getRateCNY());
                            break;
                    }
                }
            }else {
                max = decimalFormat.format(rangeModel.getMaximum());
                min = decimalFormat.format(rangeModel.getMinimum());
            }

            txtMin.setText(min);
            txtMax.setText(max);
            List<Float> values = new ArrayList<>();
            values.add(getMin);
            values.add(getMax);
            rangeSlider.setValueFrom(getMin);
            rangeSlider.setValueTo(getMax);
            rangeSlider.setValues(values);

            rangeSlider.addOnSliderTouchListener(new RangeSlider.OnSliderTouchListener() {
                @Override
                public void onStartTrackingTouch(@NonNull RangeSlider slider) {
                    Log.d("onStartTrackingTouch", slider.getValues().toString());
                }

                @Override
                public void onStopTrackingTouch(@NonNull RangeSlider slider) {
                    float getMin = slider.getValues().get(0);
                    float getMax = slider.getValues().get(1);
                    txtMin.setText(decimalFormat.format(getMin));
                    txtMax.setText(decimalFormat.format(getMax));
                    Log.d("onStartTrackingTouch", slider.getValues().toString());
                }
            });
        }
    }

    @Override
    public void onRequestCompleteListener(CategoryModel categoryModel) {
        if (categoryModel != null){
            Log.e("check data", categoryModel+"");

            filterItemsAdapter = new FilterItemsAdapter(getContext(), categoryModel.getSubCategoryModelArrayList(), languageLocale.getLanguage(), this::getFilterSelectedTourist);
            recyclerFilterTypeHotel.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerFilterTypeHotel.setAdapter(filterItemsAdapter);

        }
    }

    @Override
    public void onRequestCompleteListenerFacilities(ArrayList<FacilitiesModel> facilitiesArrayList) {
        if (facilitiesArrayList != null && facilitiesArrayList.size() != 0) {
            Log.e("check data", facilitiesArrayList + "");

            facilitiesAdapter = new FilterItemsFacilitiesAdapter(getContext(), facilitiesArrayList, languageLocale.getLanguage(), this::getFilterSelectedFacilities);
            recyclerFilterFacilities.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerFilterFacilities.setAdapter(facilitiesAdapter);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void getFilterSelectedTourist(ArrayList<SubCategoryModel> subCategoryArrayList) {
        this.subCategoryArrayList = subCategoryArrayList;
    }

    @Override
    public void getFilterSelectedFacilities(ArrayList<FacilitiesModel> facilitiesArrayList) {
        this.facilitiesArrayList = facilitiesArrayList;
    }
}