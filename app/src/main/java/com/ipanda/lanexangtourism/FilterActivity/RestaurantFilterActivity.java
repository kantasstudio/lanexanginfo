package com.ipanda.lanexangtourism.FilterActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapterRecyclerView;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.FilterItemsModel;
import com.ipanda.lanexangtourism.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class RestaurantFilterActivity extends AppCompatActivity {

    //variables
    private static String TAG = RestaurantFilterActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ExpandableLayout mShowContentTypeRestaurant, mShowContentGuarantee;

    private ImageView mImgUpDownTypeRestaurant, mImgUpDownGuarantee;

    private RecyclerView mRecyclerViewTypeRestaurant, mRecyclerViewGuarantee;

    private FilterItemsAdapterRecyclerView mAdapterTypeRestaurant, mAdapterGuarantee;

    private ArrayList<FilterItemsModel> itemsTypeRestaurantArrayList, itemsGuaranteeArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_filter);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //init set data
        setDataTypeRestaurant();
        setDataGuarantee();

        //setup recyclerview
        setupRecyclerViewTypeRestaurant();
        setupRecyclerViewGuarantee();

    }


    private void setDataTypeRestaurant() {
        itemsTypeRestaurantArrayList = new ArrayList<>();
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(0,"อาหารจานเดียว/ตามสั่ง",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(1,"Buffet",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(2,"ร้านอาหารริมน้ำ",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(3,"ของหวาน",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(4,"ร้านกินเส้นต่างๆ",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(5,"อาหารอีสาน",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(6,"อาหารญี่ปุ่น",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(7,"ร้านอาหารเกาหลี",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(8,"ร้านอาหารปิ้งย่าง",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(9,"อาหารทะเล",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(10,"กาแฟคาเฟ่",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(11,"สเต๊ก",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(12,"บาร์และผับ",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(13,"ร้านแนวฟิวชั่น",false));
        itemsTypeRestaurantArrayList.add(new FilterItemsModel(14,"ร้านบรรยากาศชิลๆ",false));
    }

    private void setDataGuarantee() {
        itemsGuaranteeArrayList = new ArrayList<>();
        itemsGuaranteeArrayList.add(new FilterItemsModel(0,"ได้รับการรับรอง ipanda",false));
        itemsGuaranteeArrayList.add(new FilterItemsModel(1,"Clean Food Good Taste",false));
        itemsGuaranteeArrayList.add(new FilterItemsModel(2,"เซลล์ชวนชิม",false));
    }

    private void setupRecyclerViewTypeRestaurant() {
        mRecyclerViewTypeRestaurant = findViewById(R.id.recycler_filter_type_restaurant);
        mAdapterTypeRestaurant = new FilterItemsAdapterRecyclerView(this,itemsTypeRestaurantArrayList);
        mRecyclerViewTypeRestaurant.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewTypeRestaurant.setAdapter(mAdapterTypeRestaurant);
    }

    private void setupRecyclerViewGuarantee() {
        mRecyclerViewGuarantee = findViewById(R.id.recycler_filter_guarantee);
        mAdapterGuarantee = new FilterItemsAdapterRecyclerView(this,itemsGuaranteeArrayList);
        mRecyclerViewGuarantee.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewGuarantee.setAdapter(mAdapterGuarantee);
    }

    public void showContentOpenTypeRestaurant(View view){
        //views
        mShowContentTypeRestaurant = findViewById(R.id.expandable_type_restaurant);
        mImgUpDownTypeRestaurant = findViewById(R.id.img_arrow_type_restaurant);
        mShowContentTypeRestaurant.toggle();
        if (mShowContentTypeRestaurant.isExpanded()){
            mImgUpDownTypeRestaurant.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownTypeRestaurant.setImageResource(R.drawable.ic_arrow_right);
        }
    }

    public void showContentOpenGuarantee(View view){
        //views
        mShowContentGuarantee = findViewById(R.id.expandable_guarantee);
        mImgUpDownGuarantee = findViewById(R.id.img_arrow_guarantee);
        mShowContentGuarantee.toggle();
        if (mShowContentGuarantee.isExpanded()){
            mImgUpDownGuarantee.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownGuarantee.setImageResource(R.drawable.ic_arrow_right);
        }
    }
}
