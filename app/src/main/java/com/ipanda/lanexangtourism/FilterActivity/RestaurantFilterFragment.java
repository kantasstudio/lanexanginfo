package com.ipanda.lanexangtourism.FilterActivity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapter;
import com.ipanda.lanexangtourism.Adapter.FilterItemsDeliciousAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedDeliciousClickListener;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.DeliciousGuaranteeModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.FilterItemsCategoryAsyncTask;
import com.ipanda.lanexangtourism.asynctask.FilterItemsDeliciousAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.FilterDeliciousCallBack;
import com.ipanda.lanexangtourism.interface_callback.FilterItemsCallBack;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;


public class RestaurantFilterFragment extends DialogFragment implements FilterItemsCallBack , FilterDeliciousCallBack , FilterSelectedClickListener, FilterSelectedDeliciousClickListener {

    //variables
    private static final String TAG = RestaurantFilterFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private int category;

    private String userId, url;

    private ImageView imgBack;

    private Button btnConfirm;

    private ConstraintLayout conRestaurantFilter;

    private ImageView imgArrowRestaurant;

    private ExpandableLayout expandableRestaurant;

    private RecyclerView recyclerRestaurant;

    private FilterItemsAdapter filterItemsAdapter;

    private ConstraintLayout conDeliciousGuaranteedFilter;

    private ImageView imgArrowGuarantee;

    private ExpandableLayout expandableGuarantee;

    private RecyclerView recyclerGuarantee;

    private FilterItemsDeliciousAdapter filterDeliciousAdapter;

    private Bundle result = new Bundle();

    private ArrayList<SubCategoryModel> subCategoryArrayList;

    private ArrayList<DeliciousGuaranteeModel> deliciousArrayList;

    public RestaurantFilterFragment() {
        // Required empty public constructor
    }

    public static RestaurantFilterFragment newInstance() {
        RestaurantFilterFragment fragment = new RestaurantFilterFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        if (getArguments() != null) {
            category = getArguments().getInt("CATEGORY_ID",0);
            url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SubCategory/"+category;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant_filter, container, false);

        //views
        imgBack = view.findViewById(R.id.img_back);
        btnConfirm = view.findViewById(R.id.btn_confirm);
        conRestaurantFilter = view.findViewById(R.id.con_restaurant_filter);
        imgArrowRestaurant = view.findViewById(R.id.img_arrow_type_restaurant);
        expandableRestaurant = view.findViewById(R.id.expandable_type_restaurant);
        recyclerRestaurant = view.findViewById(R.id.recycler_filter_type_restaurant);
        conDeliciousGuaranteedFilter = view.findViewById(R.id.con_delicious_guaranteed_filter);
        imgArrowGuarantee = view.findViewById(R.id.img_arrow_guarantee);
        expandableGuarantee = view.findViewById(R.id.expandable_guarantee);
        recyclerGuarantee = view.findViewById(R.id.recycler_filter_guarantee);

        //setup event onClicked
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        conRestaurantFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableRestaurant.toggle();
                if (expandableRestaurant.isExpanded()){
                    imgArrowRestaurant.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    imgArrowRestaurant.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        conDeliciousGuaranteedFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableGuarantee.toggle();
                if (expandableGuarantee.isExpanded()){
                    imgArrowGuarantee.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    imgArrowGuarantee.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.putParcelableArrayList("sub_category_list",subCategoryArrayList);
                result.putParcelableArrayList("delicious_list",deliciousArrayList);
                getParentFragmentManager().setFragmentResult("key_menu_three", result);
                dismiss();
            }
        });

        //set AsyncTack
        if (category != 0) {
            new FilterItemsCategoryAsyncTask(this).execute(url);
        }

        String urlDelicious = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/DeliciousGuarantee";
        new FilterItemsDeliciousAsyncTask(this).execute(urlDelicious);

        return view;
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(CategoryModel categoryModel) {
        if (categoryModel != null){
            Log.e("check data", categoryModel+"");

            filterItemsAdapter = new FilterItemsAdapter(getContext(), categoryModel.getSubCategoryModelArrayList(), languageLocale.getLanguage(),this::getFilterSelectedTourist);
            recyclerRestaurant.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerRestaurant.setAdapter(filterItemsAdapter);

        }
    }

    @Override
    public void onRequestCompleteListenerDelicious(ArrayList<DeliciousGuaranteeModel> guaranteeArrayList) {
        if (guaranteeArrayList != null && guaranteeArrayList.size() != 0) {
            Log.e("check data", guaranteeArrayList + "");

            filterDeliciousAdapter = new FilterItemsDeliciousAdapter(getContext(), guaranteeArrayList, languageLocale.getLanguage(), this::getFilterSelectedDelicious);
            recyclerGuarantee.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerGuarantee.setAdapter(filterDeliciousAdapter);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void getFilterSelectedTourist(ArrayList<SubCategoryModel> subCategoryArrayList) {
        this.subCategoryArrayList = subCategoryArrayList;
    }

    @Override
    public void getFilterSelectedDelicious(ArrayList<DeliciousGuaranteeModel> deliciousArrayList) {
        this.deliciousArrayList = deliciousArrayList;
    }
}