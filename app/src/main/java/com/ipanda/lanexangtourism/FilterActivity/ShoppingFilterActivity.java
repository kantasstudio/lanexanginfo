package com.ipanda.lanexangtourism.FilterActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapterRecyclerView;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.FilterItemsModel;
import com.ipanda.lanexangtourism.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class ShoppingFilterActivity extends AppCompatActivity {

    //variables
    private static String TAG = ShoppingFilterActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ExpandableLayout mShowContentShopping;

    private ImageView mImgUpDownShopping;

    private RecyclerView mRecyclerViewShopping;

    private FilterItemsAdapterRecyclerView mAdapterShopping;

    private ArrayList<FilterItemsModel> itemsShoppingArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_filter);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        //init set data
        setDataShopping();

        //setup recyclerview
        setupRecyclerViewShopping();

    }

    private void setDataShopping() {
        itemsShoppingArrayList = new ArrayList<>();
        itemsShoppingArrayList.add(new FilterItemsModel(0,"เกี่ยวกับอาหารและของกิน",false));
        itemsShoppingArrayList.add(new FilterItemsModel(1,"นักสะสมของแปลก",false));
        itemsShoppingArrayList.add(new FilterItemsModel(2,"ศิลปะ",false));
        itemsShoppingArrayList.add(new FilterItemsModel(3,"เสื้อผ้า",false));
    }

    private void setupRecyclerViewShopping() {
        mRecyclerViewShopping = findViewById(R.id.recycler_filter_shopping);
        mAdapterShopping = new FilterItemsAdapterRecyclerView(this,itemsShoppingArrayList);
        mRecyclerViewShopping.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewShopping.setAdapter(mAdapterShopping);
    }

    public void showContentOpenShopping(View view){
        //views
        mShowContentShopping = findViewById(R.id.expandable_shopping);
        mImgUpDownShopping = findViewById(R.id.img_arrow_shopping);
        mShowContentShopping.toggle();
        if (mShowContentShopping.isExpanded()){
            mImgUpDownShopping.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownShopping.setImageResource(R.drawable.ic_arrow_right);
        }
    }
}
