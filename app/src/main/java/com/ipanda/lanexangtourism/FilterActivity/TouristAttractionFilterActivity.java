package com.ipanda.lanexangtourism.FilterActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapterRecyclerView;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.FilterItemsModel;
import com.ipanda.lanexangtourism.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class TouristAttractionFilterActivity extends AppCompatActivity {

    //variables
    private static String TAG = TouristAttractionFilterActivity.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ExpandableLayout mShowContentTouristAttraction;

    private ImageView mImgUpDownTouristAttraction;

    private RecyclerView mRecyclerViewTouristAttraction;

    private FilterItemsAdapterRecyclerView mAdapterTouristAttraction;

    private ArrayList<FilterItemsModel> itemsTouristAttractionArrayList;

    private ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tourist_attraction_filter);

        //get language
        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();
        
        //views
        img_back = findViewById(R.id.img_back);

        //init set data
        setDataTouristAttraction();

        //setup recyclerview
        setupRecyclerViewTouristAttraction();

        //setup event onclick
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }

    private void setDataTouristAttraction() {
        itemsTouristAttractionArrayList = new ArrayList<>();
        itemsTouristAttractionArrayList.add(new FilterItemsModel(0,"ท่องเที่ยวตามธรรมชาติ",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(1,"พิพิธภัณฑ์",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(2,"ประวัติศาสตร์และวัฒนธรรม",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(3,"โบราณสถาน",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(4,"วัดและวัง",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(5,"อุทยานแห่งชาติ",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(6,"ภูเขาและทุ่งหญ้า",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(7,"สวนพฤกษศาสตร์",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(8,"แม่น้ำอ่างขื่อน",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(9,"หมู่บ้านและชุมชน",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(10,"สวนสนุก",false));
        itemsTouristAttractionArrayList.add(new FilterItemsModel(11,"ห้างสรรพสินค้า",false));
    }

    private void setupRecyclerViewTouristAttraction() {
        mRecyclerViewTouristAttraction = findViewById(R.id.recycler_filter_tourist_attraction);
        mAdapterTouristAttraction = new FilterItemsAdapterRecyclerView(this,itemsTouristAttractionArrayList);
        mRecyclerViewTouristAttraction.setLayoutManager(new GridLayoutManager(this, 1));
        mRecyclerViewTouristAttraction.setAdapter(mAdapterTouristAttraction);
    }

    public void showContentOpenTouristAttraction(View view){
        //views
        mShowContentTouristAttraction = findViewById(R.id.expandable_tourist_attraction);
        mImgUpDownTouristAttraction = findViewById(R.id.img_arrow_tourist_attraction);
        mShowContentTouristAttraction.toggle();
        if (mShowContentTouristAttraction.isExpanded()){
            mImgUpDownTouristAttraction.setImageResource(R.drawable.ic_arrow_up);
        }else {
            mImgUpDownTouristAttraction.setImageResource(R.drawable.ic_arrow_right);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
