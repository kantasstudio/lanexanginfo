package com.ipanda.lanexangtourism.FilterActivity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Adapter.FilterItemsAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.FilterItemsCategoryAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.FilterItemsCallBack;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class TouristAttractionFilterFragment extends DialogFragment implements FilterItemsCallBack, FilterSelectedClickListener {

    //variables
    private static final String TAG = TouristAttractionFilterFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private int category;

    private String userId, url;

    private ImageView img_back;

    private Button btnConfirm;

    private ConstraintLayout mTouristAttraction;

    private ExpandableLayout mShowContentTouristAttraction;

    private ImageView mImgUpDownTouristAttraction;

    private RecyclerView recyclerFilterTouristAttraction;

    private FilterItemsAdapter filterItemsAdapter;

    private Bundle result = new Bundle();

    private ArrayList<SubCategoryModel> subCategoryArrayList;

    public TouristAttractionFilterFragment() {
        // Required empty public constructor
    }


    public static TouristAttractionFilterFragment newInstance() {
        TouristAttractionFilterFragment fragment = new TouristAttractionFilterFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        if (getArguments() != null) {
            category = getArguments().getInt("CATEGORY_ID",0);
            url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SubCategory/"+category;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tourist_attraction_filter, container, false);

        //views
        img_back = view.findViewById(R.id.img_back);
        mTouristAttraction = view.findViewById(R.id.con_tourist_attraction);
        mShowContentTouristAttraction = view.findViewById(R.id.expandable_tourist_attraction);
        mImgUpDownTouristAttraction = view.findViewById(R.id.img_arrow_tourist_attraction);
        recyclerFilterTouristAttraction = view.findViewById(R.id.recycler_filter_tourist_attraction);
        btnConfirm = view.findViewById(R.id.btn_confirm);

        //setup event onClicked
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        mTouristAttraction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mShowContentTouristAttraction.toggle();
                if (mShowContentTouristAttraction.isExpanded()){
                    mImgUpDownTouristAttraction.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    mImgUpDownTouristAttraction.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.putParcelableArrayList("sub_category_list",subCategoryArrayList);
                getParentFragmentManager().setFragmentResult("key_menu_two", result);
                dismiss();
            }
        });

        //set AsyncTack
        if (category != 0) {
            new FilterItemsCategoryAsyncTask(this).execute(url);
        }

        return view;
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(CategoryModel categoryModel) {
        if (categoryModel != null){
            Log.e("check data", categoryModel+"");

            filterItemsAdapter = new FilterItemsAdapter(getContext(), categoryModel.getSubCategoryModelArrayList(), languageLocale.getLanguage(),this::getFilterSelectedTourist);
            recyclerFilterTouristAttraction.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerFilterTouristAttraction.setAdapter(filterItemsAdapter);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void getFilterSelectedTourist(ArrayList<SubCategoryModel> subCategoryArrayList) {
        this.subCategoryArrayList = subCategoryArrayList;
    }
}