package com.ipanda.lanexangtourism.Helper;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

public class ChangeLanguageLocale  {

    private Context context;

    public ChangeLanguageLocale(Context context) {
        this.context = context;
    }


    public void setLocale(String lang){
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        context.getSharedPreferences("PREF_APP_LOCALE", Context.MODE_PRIVATE).edit().putString("APP_LOCALE", lang).apply();
    }

    public void getLoadLocale(){
        String language = context.getSharedPreferences("PREF_APP_LOCALE", Context.MODE_PRIVATE).getString("APP_LOCALE","th");
        setLocale(language);
    }

    public String getLanguage(){
        String language = context.getSharedPreferences("PREF_APP_LOCALE", Context.MODE_PRIVATE).getString("APP_LOCALE","th");
        return language ;
    }

}
