package com.ipanda.lanexangtourism.Helper;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ExchangeRate {

    private DecimalFormatSymbols symbols = new DecimalFormatSymbols();
    private DecimalFormat decimalFormat = new DecimalFormat("#.###", symbols);

    public double getExchangeRateUSD(double rateTHB, double rateUSD){
        double totals = 0;
        double usd = Double.parseDouble(decimalFormat.format(rateUSD));
        totals = usd * rateTHB;
        return totals;
    }

    public double getExchangeRateCNY(double rateTHB, double rateCNY){
        double totals = 0;
        double cny = Double.parseDouble(decimalFormat.format(rateCNY));
        totals = cny * rateTHB;
        return totals;
    }

}
