package com.ipanda.lanexangtourism.Helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.ipanda.lanexangtourism.MenuFragment.BookingFragment;
import com.ipanda.lanexangtourism.MenuFragment.ContactFragment;
import com.ipanda.lanexangtourism.MenuFragment.HomeFragment;
import com.ipanda.lanexangtourism.MenuFragment.HotelFragment;
import com.ipanda.lanexangtourism.MenuFragment.ProgramTourFragment;
import com.ipanda.lanexangtourism.MenuFragment.RestaurantFragment;
import com.ipanda.lanexangtourism.MenuFragment.ShoppingFragment;
import com.ipanda.lanexangtourism.MenuFragment.TourismLocationFragment;
import com.ipanda.lanexangtourism.R;

public class LoadMenuFragmentHelper  {

    private Context context;
    private View view;
    private FragmentTransaction fragmentTransaction;
    private DrawerLayout mDrawerLayout;
    public static int navItemIndex = 0;
    private Bundle bundle = new Bundle();

    public LoadMenuFragmentHelper(Context context, View view, FragmentTransaction fragmentTransaction, DrawerLayout mDrawerLayout) {
        this.context = context;
        this.view = view;
        this.fragmentTransaction = fragmentTransaction;
        this.mDrawerLayout = mDrawerLayout;

    }

    //Menu Drawer
    public void drawerMenu() {
        ImageView menuHome = view.findViewById(R.id.menu_home);
        ConstraintLayout menuProgramTour = view.findViewById(R.id.menu_program_tour);
        ConstraintLayout menuTourismLocation = view.findViewById(R.id.menu_tourism_location);
        ConstraintLayout menuRestaurant = view.findViewById(R.id.menu_restaurant);
        ConstraintLayout menuShopping = view.findViewById(R.id.menu_shopping);
        ConstraintLayout menuHotel = view.findViewById(R.id.menu_hotel);
        ConstraintLayout menuContact = view.findViewById(R.id.menu_contact);
        ConstraintLayout menuBooking = view.findViewById(R.id.menu_booking);
        ImageView btnBurgerClose = view.findViewById(R.id.btn_burger_close);

        menuHome.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.START);
                navItemIndex = 0;
                loadFragment();

            }
        });
        menuProgramTour.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.START);
                navItemIndex = 1;
                loadFragment();
            }
        });
        menuTourismLocation.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.START);
                navItemIndex = 2;
                loadFragment();
            }
        });
        menuRestaurant.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.START);
                navItemIndex = 3;
                loadFragment();
            }
        });
        menuShopping.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.START);
                navItemIndex = 4;
                loadFragment();
            }
        });
        menuHotel.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.START);
                navItemIndex = 5;
                loadFragment();
            }
        });
        menuContact.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.START);
                navItemIndex = 6;
                loadFragment();
            }
        });
        menuBooking.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.START);
                navItemIndex = 7;
                loadFragment();
            }
        });

        btnBurgerClose.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.START);
            }
        });
    }

    public void drawerMenuRight() {
        ImageView menuHome = view.findViewById(R.id.menu_home);
        ConstraintLayout menuProgramTour = view.findViewById(R.id.menu_program_tour);
        ConstraintLayout menuTourismLocation = view.findViewById(R.id.menu_tourism_location);
        ConstraintLayout menuRestaurant = view.findViewById(R.id.menu_restaurant);
        ConstraintLayout menuShopping = view.findViewById(R.id.menu_shopping);
        ConstraintLayout menuHotel = view.findViewById(R.id.menu_hotel);
        ConstraintLayout menuContact = view.findViewById(R.id.menu_contact);
        ConstraintLayout menuBooking = view.findViewById(R.id.menu_booking);
        ImageView btnBurgerClose = view.findViewById(R.id.btn_burger_close);

        menuHome.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                navItemIndex = 0;
                loadFragment();

            }
        });
        menuProgramTour.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                navItemIndex = 1;
                loadFragment();
            }
        });
        menuTourismLocation.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                navItemIndex = 2;
                loadFragment();
            }
        });
        menuRestaurant.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                navItemIndex = 3;
                loadFragment();
            }
        });
        menuShopping.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                navItemIndex = 4;
                loadFragment();
            }
        });
        menuHotel.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                navItemIndex = 5;
                loadFragment();
            }
        });
        menuContact.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                navItemIndex = 6;
                loadFragment();
            }
        });
        menuBooking.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
                navItemIndex = 7;
                loadFragment();
            }
        });

        btnBurgerClose.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
            }
        });
    }

    private void loadFragment() {
        Fragment fragment = getHomeFragment();
        fragment.setArguments(bundle);
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.FrameLayout_Menu, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void selectedLoadFragment(int nav){
        navItemIndex = nav;
        loadFragment();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                ProgramTourFragment tourFragment = new ProgramTourFragment();
                return tourFragment;
            case 2:
                TourismLocationFragment locationFragment = new TourismLocationFragment();
                bundle.putString("TEXT_TITLE",context.getResources().getString(R.string.txt_tourism_location));
                bundle.putInt("CATEGORY_ID",2);
                return locationFragment;
            case 3:
                RestaurantFragment restaurantFragment = new RestaurantFragment();
                bundle.putString("TEXT_TITLE",context.getResources().getString(R.string.txt_restaurant));
                bundle.putInt("CATEGORY_ID",3);
                return restaurantFragment;
            case 4:
                ShoppingFragment shoppingFragment = new ShoppingFragment();
                bundle.putString("TEXT_TITLE",context.getResources().getString(R.string.txt_shopping));
                bundle.putInt("CATEGORY_ID",4);
                return shoppingFragment;
            case 5:
                HotelFragment hotelFragment = new HotelFragment();
                bundle.putString("TEXT_TITLE",context.getResources().getString(R.string.txt_hotel));
                bundle.putInt("CATEGORY_ID",5);
                return hotelFragment;
            case 6:
                ContactFragment contactFragment = new ContactFragment();
                bundle.putString("TEXT_TITLE",context.getResources().getString(R.string.txt_contact));
                bundle.putInt("CATEGORY_ID",6);
                return contactFragment;
            case 7:
                BookingFragment bookingFragment = new BookingFragment();
                return bookingFragment;
            default:
                return new HomeFragment();
        }

    }//Ene menu drawer

}
