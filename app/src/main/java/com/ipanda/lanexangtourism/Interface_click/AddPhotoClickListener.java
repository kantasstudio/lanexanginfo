package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.Model.TouristAttractionsModel;

public interface AddPhotoClickListener {

        void itemClickedPhoto(DatesTripModel date, TouristAttractionsModel tourist);

}
