package com.ipanda.lanexangtourism.Interface_click;

public interface BookingDetailsClickListener {

    void itemClicked(String periodStart,String periodEnd,int position);

}
