package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;

public interface BookingItemsClickListener {

    void clickedPackage(BookingModel booking);
    void clickedTicket(BookingModel booking);
    void clickedCar(BookingModel booking);
    void clickedHotel(BookingModel booking);
    void clickedSharePackage(ItemsModel items);
    void clickedShareTicket(ItemsModel items);

}
