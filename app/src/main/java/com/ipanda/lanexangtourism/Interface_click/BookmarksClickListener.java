package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.BookMarksModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;

public interface BookmarksClickListener {

    void itemClickedInterested(BookMarksModel bookMarksModel);
    void itemClickedItems(ItemsModel itemsModel);
    void itemClickedBookmarks(BookMarksModel bookMarksModel);

}
