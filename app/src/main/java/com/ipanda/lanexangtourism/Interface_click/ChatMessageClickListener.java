package com.ipanda.lanexangtourism.Interface_click;


import android.widget.RelativeLayout;

import com.ipanda.lanexangtourism.Model.MessageModel;

public interface ChatMessageClickListener {

    void onClickChatMessage(MessageModel message, RelativeLayout notification);

}
