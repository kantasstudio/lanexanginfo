package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.DatesTripModel;

public interface DatesTripClickListener {

    void onClickEventDatesTrip(DatesTripModel datesTrip,int position, String event);

}
