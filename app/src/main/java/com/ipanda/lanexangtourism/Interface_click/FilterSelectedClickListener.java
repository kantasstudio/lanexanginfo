package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.SubCategoryModel;

import java.util.ArrayList;

public interface FilterSelectedClickListener {

    void getFilterSelectedTourist(ArrayList<SubCategoryModel> subCategoryArrayList);

}
