package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.DeliciousGuaranteeModel;

import java.util.ArrayList;

public interface FilterSelectedDeliciousClickListener {

    void getFilterSelectedDelicious(ArrayList<DeliciousGuaranteeModel> deliciousArrayList);

}
