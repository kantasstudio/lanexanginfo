package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.DurationPackageTourModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;

import java.util.ArrayList;

public interface FilterSelectedDurationClickListener {

    void getFilterSelectedDuration(ArrayList<DurationPackageTourModel> durationArrayList);

}
