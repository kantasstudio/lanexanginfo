package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.FacilitiesModel;

import java.util.ArrayList;

public interface FilterSelectedFacilitiesClickListener {

    void getFilterSelectedFacilities(ArrayList<FacilitiesModel> facilitiesArrayList);

}
