package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.SubCategoryModel;

import java.util.ArrayList;

public interface FilterSelectedTravelingClickListener {

    void getFilterSelectedTraveling(ArrayList<SubCategoryModel> travelingArrayList);

}
