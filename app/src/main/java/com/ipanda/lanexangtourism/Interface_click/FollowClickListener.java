package com.ipanda.lanexangtourism.Interface_click;

import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Model.UserModel;

public interface FollowClickListener {

    void itemClickedProfile(UserModel user);
    void itemClickedFollow(TextView txt_follow_title, RelativeLayout rl_btn_follow, UserModel user);

}
