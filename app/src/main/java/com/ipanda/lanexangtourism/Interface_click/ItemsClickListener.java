package com.ipanda.lanexangtourism.Interface_click;

import android.widget.ImageView;

import com.ipanda.lanexangtourism.Model.ItemsContactModel;
import com.ipanda.lanexangtourism.Model.ItemsHotelModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsRestaurantModel;
import com.ipanda.lanexangtourism.Model.ItemsShoppingModel;
import com.ipanda.lanexangtourism.Model.ItemsTourismModel;

public interface ItemsClickListener {
    void itemClicked(ItemsTourismModel items);
    void itemClicked(ItemsRestaurantModel items);
    void itemClicked(ItemsShoppingModel items);
    void itemClicked(ItemsHotelModel items);
    void itemClicked(ItemsContactModel items);
    void itemClicked(ImageView bookmarks, ItemsModel itemsModel);
}
