package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.ItemsModel;

public interface ItemsMapsClickListener {

    void clickedItemsMaps(ItemsModel items);

    void clickedItemsMove(ItemsModel items);

}
