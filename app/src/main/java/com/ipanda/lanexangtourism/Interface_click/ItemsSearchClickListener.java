package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.ItemsModel;

public interface ItemsSearchClickListener {

    void onClickedItemSearch(ItemsModel items);
}
