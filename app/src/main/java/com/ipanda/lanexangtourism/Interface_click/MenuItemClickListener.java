package com.ipanda.lanexangtourism.Interface_click;

import android.view.View;

public interface MenuItemClickListener {
    void menuItemClicked(View view);
}
