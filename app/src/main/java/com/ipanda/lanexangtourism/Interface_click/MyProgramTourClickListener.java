package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.ProgramTourModel;

public interface MyProgramTourClickListener {

    void itemClicked(ProgramTourModel tourModel);
    void itemClickedMore(ProgramTourModel tourModel, String event);

}
