package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.BookingModel;

public interface OrderDetailsClickListener {

    void itemClickedOrder(BookingModel booking);

}
