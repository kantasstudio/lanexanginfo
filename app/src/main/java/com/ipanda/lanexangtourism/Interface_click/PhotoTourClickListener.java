package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.PhotoTourist;

import java.util.ArrayList;

public interface PhotoTourClickListener {
    void itemClicked (int position, ArrayList<PhotoTourist> photoTouristArrayList);
}
