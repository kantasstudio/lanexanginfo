package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.DatesTripModel;

public interface PlacesItemClickListener {

    void itemClickedPlaces(DatesTripModel trip);

}
