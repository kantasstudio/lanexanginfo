package com.ipanda.lanexangtourism.Interface_click;

import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.Model.ProgramTourModel;

public interface ProgramTourClickListener {

    void itemClicked(ProgramTourModel tourModel);
    void itemClickedLikeProgramTour(ImageView img_like, TextView txt_count_like, ProgramTourModel tourModel);

}
