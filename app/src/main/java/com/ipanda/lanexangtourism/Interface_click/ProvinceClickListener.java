package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.ProvincesModel;

public interface ProvinceClickListener {

    void onClickedProvince(ProvincesModel provinces);

}
