package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.ProductModel;

import java.util.ArrayList;

public interface PurchaseOrder {

    void getPopularOrderDetails(ArrayList<ProductModel> list, ProductModel product);
}
