package com.ipanda.lanexangtourism.Interface_click;

import android.view.View;

import com.ipanda.lanexangtourism.Model.ReviewModel;

public interface ReviewClickListener {
    void itemClicked(View view, ReviewModel reviewModel);
    void itemClickedDelete(View view, ReviewModel reviewModel,int position);
    void itemClickedProfile(View view, ReviewModel reviewModel);
}
