package com.ipanda.lanexangtourism.Interface_click;

public interface ScheduleTimeClickListener {

    void itemClicked(String timeStart, String timeEnd);

}
