package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.ScreenBookingModel;

public interface ScreenBookingClickListener {

    void itemClickedScreenBooking(ScreenBookingModel screenBooking);

}
