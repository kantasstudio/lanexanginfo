package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.ProgramTourModel;

public interface ScreenOfficialClickListener {

    void itemClickedScreenOfficial(ProgramTourModel programTourModel);

}
