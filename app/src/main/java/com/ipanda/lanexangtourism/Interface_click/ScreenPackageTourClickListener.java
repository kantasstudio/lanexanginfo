package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.ItemsModel;

public interface ScreenPackageTourClickListener {

    void itemClickedScreenPackageTour(ItemsModel itemsModel);

}
