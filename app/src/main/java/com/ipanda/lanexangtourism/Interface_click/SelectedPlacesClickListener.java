package com.ipanda.lanexangtourism.Interface_click;

import android.widget.ImageButton;

import com.ipanda.lanexangtourism.Model.ItemsModel;

public interface SelectedPlacesClickListener {
    void itemClickedPlaces(ItemsModel itemsModel, ImageButton imageButton);
}
