package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.PhotoTourist;

import java.util.ArrayList;

public interface SetThisPhotoList {

    void setThisPhotoList(ArrayList<PhotoTourist> photoArrayList);

}
