package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.ProvincesModel;

public interface SpinnerProvincesClickListener {

    void onClickedSpinnerProvinces(ProvincesModel provinces);

}
