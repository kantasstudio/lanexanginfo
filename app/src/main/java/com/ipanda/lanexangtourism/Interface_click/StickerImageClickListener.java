package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.StickerPathsModel;

public interface StickerImageClickListener {

    void onClickedStickerImage(StickerPathsModel sticker);

}
