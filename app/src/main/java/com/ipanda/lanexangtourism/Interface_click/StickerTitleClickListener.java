package com.ipanda.lanexangtourism.Interface_click;

import com.ipanda.lanexangtourism.Model.StickerModel;

public interface StickerTitleClickListener {

    void onClickedSticker(StickerModel sticker);

}
