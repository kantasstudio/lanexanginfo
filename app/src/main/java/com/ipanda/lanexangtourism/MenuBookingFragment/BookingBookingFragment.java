package com.ipanda.lanexangtourism.MenuBookingFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.ipanda.lanexangtourism.Activity.DetailsCarActivity;
import com.ipanda.lanexangtourism.Activity.DetailsHotelActivity;
import com.ipanda.lanexangtourism.Activity.DetailsPackageActivity;
import com.ipanda.lanexangtourism.Activity.DetailsTicketActivity;
import com.ipanda.lanexangtourism.Adapter.BookingItemsAdapter;
import com.ipanda.lanexangtourism.Adapter.FilterItemsMyBookingAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.BookingItemsClickListener;
import com.ipanda.lanexangtourism.Interface_click.FilterSelectedClickListener;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.BookingItemsAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.BookingItemsCallBack;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;


public class BookingBookingFragment extends Fragment implements BookingItemsCallBack, BookingItemsClickListener , ExchangeRateCallBack, FilterSelectedClickListener {

    //Variables
    private static final String TAG = BookingBookingFragment.class.getSimpleName();

    private View view;

    private ChangeLanguageLocale languageLocale;

    private String userId;

    private CardView btnSortByGroup;

    private RecyclerView mRecyclerBookingItems;

    private BookingItemsAdapter mAdapter;

    private RateModel rateModel;

    private FilterItemsMyBookingAdapter mAdapterMyBooking;

    private ArrayList<SubCategoryModel> itemsMyBookingArrayList;

    private boolean isSortByGroup = false;

    private ArrayList<BookingModel> bookingList;

    private CircularSticksLoader loader_items_id;

    private ImageView img_load_field_id;

    private TextView txt_load_field_id;

    private Boolean isModeOnline;

    public BookingBookingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_booking_boooking, container, false);

        //views
        mRecyclerBookingItems = view.findViewById(R.id.recycler_booking_items);
        btnSortByGroup = view.findViewById(R.id.txt_sort_by_group);
        loader_items_id = view.findViewById(R.id.loader_items_id);
        img_load_field_id = view.findViewById(R.id.img_load_field_id);
        txt_load_field_id = view.findViewById(R.id.txt_load_field_id);

        if (isModeOnline) {
            if (userId != null) {
                String urlRate = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ExchangeRate";
                new ExchangeRateAsyncTask(this).execute(urlRate);

                String url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Booking?user_id=" + userId;
                new BookingItemsAsyncTask(this).execute(url);
            }
        }else {

        }

        itemsMyBookingArrayList = new ArrayList<>();
        itemsMyBookingArrayList.add(new SubCategoryModel(9,getString(R.string.text_tour_package),false));
        itemsMyBookingArrayList.add(new SubCategoryModel(10,getString(R.string.text_event_ticket),false));
        itemsMyBookingArrayList.add(new SubCategoryModel(11,getString(R.string.text_car_rental),false));
        itemsMyBookingArrayList.add(new SubCategoryModel(5,getString(R.string.txt_hotel),false));

        //setup on Click
        btnSortByGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupPopupFilter();
                if (isSortByGroup){
                    isSortByGroup = false;
                    setViewVisibility(R.id.scroll_view_id, View.GONE);
                }else {
                    isSortByGroup = true;
                    setViewVisibility(R.id.scroll_view_id, View.VISIBLE);
                }

            }
        });

        return view;
    }

    private void setupPopupFilter() {

        //views
        ConstraintLayout consMyBooking = view.findViewById(R.id.cons_my_booking);
        ExpandableLayout mShowContentMyBooking = view.findViewById(R.id.expandable_my_booking);
        ImageView mImgUpDownMyBooking = view.findViewById(R.id.img_arrow_my_booking);
        RecyclerView mRecyclerViewMyBooking = view.findViewById(R.id.recycler_filter_my_booking);

        consMyBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mShowContentMyBooking.toggle();
                if (mShowContentMyBooking.isExpanded()){
                    mImgUpDownMyBooking.setImageResource(R.drawable.ic_arrow_up);
                }else {
                    mImgUpDownMyBooking.setImageResource(R.drawable.ic_arrow_right);
                }
            }
        });

        mAdapterMyBooking = new FilterItemsMyBookingAdapter(getContext(),itemsMyBookingArrayList, languageLocale.getLanguage(), this);
        mRecyclerViewMyBooking.setLayoutManager(new GridLayoutManager(getContext(), 1));
        mRecyclerViewMyBooking.setAdapter(mAdapterMyBooking);

    }

    @Override
    public void onPreCallService() {
        mRecyclerBookingItems.setVisibility(View.GONE);
        loader_items_id.setVisibility(View.VISIBLE);
        img_load_field_id.setVisibility(View.GONE);
        txt_load_field_id.setVisibility(View.GONE);
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }

    @Override
    public void onRequestCompleteListener(ArrayList<BookingModel> bookingArrayList) {
        if (bookingArrayList != null && bookingArrayList.size() != 0){
            Log.e("check data",bookingArrayList+"");
            this.bookingList = bookingArrayList;
            setupBooking(bookingList);

            mRecyclerBookingItems.setVisibility(View.VISIBLE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.GONE);
            txt_load_field_id.setVisibility(View.GONE);
            btnSortByGroup.setVisibility(View.VISIBLE);
        }else {
            btnSortByGroup.setVisibility(View.GONE);
            mRecyclerBookingItems.setVisibility(View.GONE);
            loader_items_id.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.VISIBLE);
            txt_load_field_id.setVisibility(View.VISIBLE);
        }
    }

    private void setupBooking(ArrayList<BookingModel> list) {
        if (list != null && list.size() != 0){
            mAdapter = new BookingItemsAdapter(getContext(), list, languageLocale.getLanguage(), this, rateModel);
            mRecyclerBookingItems.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            mRecyclerBookingItems.setAdapter(mAdapter);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void clickedPackage(BookingModel booking) {
        Intent intent = new Intent(getContext(), DetailsPackageActivity.class);
        intent.putExtra("BOOKING", booking);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void clickedTicket(BookingModel booking) {
        Intent intent = new Intent(getContext(), DetailsTicketActivity.class);
        intent.putExtra("BOOKING", booking);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void clickedCar(BookingModel booking) {
        Intent intent = new Intent(getContext(), DetailsCarActivity.class);
        intent.putExtra("BOOKING", booking);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void clickedHotel(BookingModel booking) {
        Intent intent = new Intent(getContext(), DetailsHotelActivity.class);
        intent.putExtra("BOOKING", booking);
        intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
        startActivity(intent);
    }

    @Override
    public void clickedSharePackage(ItemsModel items) {
        shareItems(items);
    }

    @Override
    public void clickedShareTicket(ItemsModel items) {
        shareItems(items);
    }

    private void shareItems(ItemsModel items) {
        String ShareSub = "";
        switch (languageLocale.getLanguage()){
            case "th":
                ShareSub = " "+items.getTopicTH()+"\n";
                break;
            case "en":
                ShareSub = " "+items.getTopicEN()+"\n";
                break;
            case "lo":
                ShareSub = " "+items.getTopicLO()+"\n";
                break;
            case "zh":
                ShareSub = " "+items.getTopicZH()+"\n";
                break;
        }
        String uri = "http://maps.google.com/maps?saddr=" +items.getLatitude()+","+items.getLongitude()+"&iwloc=A";
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, ShareSub);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, uri );
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    private void setViewVisibility(int id, int visibility) {
        view = view.findViewById(id);
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    @Override
    public void getFilterSelectedTourist(ArrayList<SubCategoryModel> subCategoryArrayList) {
        ArrayList<BookingModel> newBookingList = new ArrayList<>();
        for (SubCategoryModel sub : subCategoryArrayList) {
            if (sub.isFilter()){
                for (BookingModel book : bookingList){
                    if (book.getItemsModel().getMenuItemModel().getMenuItemId() == sub.getCategoryId()){
                        newBookingList.add(book);
                    }
                }
            }
        }
        setupBooking(newBookingList);
    }

}
