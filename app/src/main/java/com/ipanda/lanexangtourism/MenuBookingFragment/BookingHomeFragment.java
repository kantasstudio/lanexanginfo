package com.ipanda.lanexangtourism.MenuBookingFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ipanda.lanexangtourism.Activity.BookingCarRentalActivity;
import com.ipanda.lanexangtourism.Activity.BookingPackageTourActivity;
import com.ipanda.lanexangtourism.Activity.BookingTicketEventActivity;
import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Adapter.BookingViewPagerAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ScreenBookingClickListener;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.ScreenBookingModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ScreenBookingAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.ScreenBookingCallBack;
import com.ipanda.lanexangtourism.items_view.ViewEventTicketActivity;
import com.ipanda.lanexangtourism.items_view.ViewPackageTourActivity;

import java.util.ArrayList;

public class BookingHomeFragment extends Fragment implements View.OnClickListener , ScreenBookingCallBack , ScreenBookingClickListener
    , ExchangeRateCallBack {

    //Variables
    private static String TAG = BookingHomeFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private View view;

    private ViewPager screenPager;

    private TabLayout tabIndicator;

    private ArrayList<ScreenBookingModel> listScreen;

    private BookingViewPagerAdapter mAdapter;

    private ImageView imgTourPackage, imgEventTicket, imgCarRental;

    private TextView txtTourPackage, txtEventTicket, txtCarRental;

    private RateModel rateModel;

    private Boolean isModeOnline;

    public BookingHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        if (getArguments() != null) {

        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_booking_home, container, false);

        //views
        screenPager = view.findViewById(R.id.ViewPager_Booking_Home);
        tabIndicator = view.findViewById(R.id.view_booking_tab_indicator);
        imgTourPackage = view.findViewById(R.id.img_tour_package);
        imgEventTicket = view.findViewById(R.id.img_event_ticket);
        imgCarRental = view.findViewById(R.id.img_car_rental);
        txtTourPackage = view.findViewById(R.id.txt_tour_package);
        txtEventTicket = view.findViewById(R.id.txt_event_ticket);
        txtCarRental = view.findViewById(R.id.txt_car_rental);

        //set on click event
        imgTourPackage.setOnClickListener(this);
        imgEventTicket.setOnClickListener(this);
        imgCarRental.setOnClickListener(this);
        txtTourPackage.setOnClickListener(this);
        txtEventTicket.setOnClickListener(this);
        txtCarRental.setOnClickListener(this);

        if (isModeOnline) {
            String urlScreenBooking = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Adsense/getReservationsAds";
            new ScreenBookingAsyncTask(this).execute(urlScreenBooking);

            String urlRate = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ExchangeRate";
            new ExchangeRateAsyncTask(this).execute(urlRate);
        }else {

        }

        return view;
    }


    private void intentActivityPackageTour(){
        startActivity(new Intent(getContext(), BookingPackageTourActivity.class));
    }

    private void intentActivityTicketEvent(){
        startActivity(new Intent(getContext(), BookingTicketEventActivity.class));
    }

    private void intentActivityCarRental(){
        startActivity(new Intent(getContext(), BookingCarRentalActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_tour_package:
                intentActivityPackageTour();
                break;
            case R.id.img_event_ticket:
                intentActivityTicketEvent();
                break;
            case R.id.img_car_rental:
                intentActivityCarRental();
                break;
            case R.id.txt_tour_package:
                intentActivityPackageTour();
                break;
            case R.id.txt_event_ticket:
                intentActivityTicketEvent();
                break;
            case R.id.txt_car_rental:
                intentActivityCarRental();
                break;
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }

    @Override
    public void onRequestCompleteListenerScreenBooking(ArrayList<ScreenBookingModel> screenBookingArrayList) {
        if (screenBookingArrayList != null && screenBookingArrayList.size() != 0){
            //setup viewpager
            mAdapter = new BookingViewPagerAdapter(getContext(),screenBookingArrayList, this);
            screenPager.setAdapter(mAdapter);

            //setup tabLayout with viewpager
            tabIndicator.setupWithViewPager(screenPager);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClickedScreenBooking(ScreenBookingModel screenBooking) {
        if (screenBooking != null){
            switch (screenBooking.getMenuItemModel().getMenuItemId()){
                case 9:
                    Intent intentPackageTour = new Intent(getContext(), ViewPackageTourActivity.class);
                    intentPackageTour.putExtra("ITEMS_MODEL", screenBooking.getItemsModel());
                    intentPackageTour.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                    startActivity(intentPackageTour);
                    break;
                case 10:
                    Intent intentEventTicket = new Intent(getContext(), ViewEventTicketActivity.class);
                    intentEventTicket.putExtra("ITEMS_MODEL", screenBooking.getItemsModel());
                    intentEventTicket.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                    startActivity(intentEventTicket);
                    break;
                case 11:
                    intentActivityCarRental();
                    break;
            }
        }
    }
}
