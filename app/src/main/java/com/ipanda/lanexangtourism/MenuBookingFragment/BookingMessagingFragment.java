package com.ipanda.lanexangtourism.MenuBookingFragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.ChatMessageActivity;
import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Adapter.ChatMessageCompanyListAdapter;
import com.ipanda.lanexangtourism.DialogFragment.ChatMessageFragment;
import com.ipanda.lanexangtourism.FilterActivity.TouristAttractionFilterFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ChatMessageCompanyClickListener;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.MessageCompanyListAsyncTask;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.MessageCompanyListCallBack;
import com.ipanda.lanexangtourism.message.Config;
import com.ipanda.lanexangtourism.post_booking.HttpPostReadMessages;

import java.util.ArrayList;
import java.util.HashMap;


public class BookingMessagingFragment extends Fragment implements MessageCompanyListCallBack , ChatMessageCompanyClickListener {

    //Variables
    private static String TAG = BookingMessagingFragment.class.getSimpleName();

    private static final int REQUEST_GET_MESSAGE = 1001;

    private ChangeLanguageLocale languageLocale;

    private Toolbar mToolbar;

    private String userId;

    private RecyclerView recycler_list_message_company;

    private ChatMessageCompanyListAdapter chatMessageCompanyListAdapter;

    private BroadcastReceiver broadcastReceiver;

    private Boolean isModeOnline;

    public BookingMessagingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        if (getArguments() != null) {

        }

        if (isModeOnline) {
            getMessageList();
        }else {

        }

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.PUSH_LIST)) {
                    getMessageList();
                }
            }
        };

        //fragment get result
        getParentFragmentManager().setFragmentResultListener("KEY_MESSAGE", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle bundle) {
                getMessageList();
                String test = bundle.getString("back");
                Toast.makeText(getContext(), test+"", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booking_messaging, container, false);

        //Views
        recycler_list_message_company = view.findViewById(R.id.recycler_list_message_company);


        return view;
    }

    private void getMessageList() {
        if (userId != null) {
            String url = getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/getListMessage/" + userId + "?type=2";
            new MessageCompanyListAsyncTask(this).execute(url);
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<MessageModel> messageArrayList) {
        if (messageArrayList != null && messageArrayList.size() != 0){

            chatMessageCompanyListAdapter = new ChatMessageCompanyListAdapter(getContext(), messageArrayList, languageLocale.getLanguage(), userId, this);
            recycler_list_message_company.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recycler_list_message_company.setAdapter(chatMessageCompanyListAdapter);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onClickChatMessage(MessageModel message, RelativeLayout notification) {
        String name = "";
        String id = "";
        if (message.getUserModel().getUserId() != Integer.parseInt(userId)){
//            name = message.getUserModel().getFirstName()+" "+message.getUserModel().getLastName();
            id = String.valueOf(message.getUserModel().getUserId());
        }

        if (message.getUserSend().getUserId() != Integer.parseInt(userId)){
//            name = message.getUserSend().getFirstName()+" "+message.getUserSend().getLastName();
            id = String.valueOf(message.getUserSend().getUserId());
        }

        switch (languageLocale.getLanguage()){
            case "th":
                name = message.getBusinessModel().getNameTH()+"\n"+message.getBusinessModel().getMenuItemModel().getMenuItemTH();
                break;
            case "en":
                name = message.getBusinessModel().getNameEN()+"\n"+message.getBusinessModel().getMenuItemModel().getMenuItemEN();
                break;
            case "lo":
                name = message.getBusinessModel().getNameLO()+"\n"+message.getBusinessModel().getMenuItemModel().getMenuItemLO();
                break;
            case "zh":
                name = message.getBusinessModel().getNameZH()+"\n"+message.getBusinessModel().getMenuItemModel().getMenuItemZH();
                break;
        }

        Bundle bundle = new Bundle();
        DialogFragment dialogFragment = ChatMessageFragment.newInstance();
        bundle.putString("Send_To_Name", name);
        bundle.putString("Send_To_UserId", id);
        bundle.putString("Chat_Room_Id", message.getRoomId()+"");
        bundle.putString("Cover_Paths_Company",message.getItemsModel().getCoverItemsModel().getCoverPaths());
        dialogFragment.setArguments(bundle);
        dialogFragment.show(getFragmentManager(), TAG);
        message.setRead(false);
        notification.setVisibility(View.GONE);
        updateReadMessages(message.getRoomId()+"");

    }

    private void updateReadMessages(String roomId){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/chat/Chat/updateReadMessages");
        HashMap<String, String> paramsPost = new HashMap<>();
        paramsPost.put("chatroom_id", roomId);
        paramsPost.put("user_id", userId);
        httpCallPost.setParams(paramsPost);
        new HttpPostReadMessages().execute(httpCallPost);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver,
                new IntentFilter(Config.PUSH_LIST));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_GET_MESSAGE:
                if (data != null) {
                    String test = data.getStringExtra("BackPressed");
                    getMessageList();
                }
                break;
        }
    }
}
