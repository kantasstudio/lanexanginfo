package com.ipanda.lanexangtourism.MenuFragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Activity.SettingsActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.LoadMenuFragmentHelper;
import com.ipanda.lanexangtourism.MenuBookingFragment.BookingBookingFragment;
import com.ipanda.lanexangtourism.MenuBookingFragment.BookingHomeFragment;
import com.ipanda.lanexangtourism.MenuBookingFragment.BookingMessagingFragment;
import com.ipanda.lanexangtourism.R;


public class BookingFragment extends Fragment implements  BottomNavigationView.OnNavigationItemSelectedListener{

    //Variables
    private static String TAG = BookingFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private Toolbar mToolbar;

    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle toggle;

    private BottomNavigationView bottomNavigationView;

    private View view;

    private TextView txtTitleToolbar;

    public static int navItemIndex = 0;

    public BookingFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_booking, container, false);

        //views
        mDrawerLayout = view.findViewById(R.id.drawer_layout);
        mToolbar = view.findViewById(R.id.menu_toolbar_settings);
        bottomNavigationView = view.findViewById(R.id.bottom_navigation_booking);
        txtTitleToolbar = view.findViewById(R.id.toolbar_title);

        //setup Toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);


        //setup ActionBarDrawerToggle
        toggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        if(((AppCompatActivity)getActivity()).getSupportActionBar() != null) {
            Drawable drawable = getResources().getDrawable(R.drawable.ic_menu_burger_dark);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 70, 60, true));
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        }

        //set text
        txtTitleToolbar.setText(getString(R.string.text_booking_various));

        //set on click drawer menu
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        LoadMenuFragmentHelper helper = new LoadMenuFragmentHelper(getContext(),view,fragmentTransaction,mDrawerLayout);
        helper.drawerMenu();


        //setup navigation
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        //default fragment
        loadFragment(new BookingHomeFragment());

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_setting_dark, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                Toast.makeText(getContext(), "Settings", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(), SettingsActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()){
            case R.id.bottomNavigationHomeId:
                fragment = new BookingHomeFragment();
                txtTitleToolbar.setText(getString(R.string.text_booking_various));
                break;
            case R.id.bottomNavigationBookingId:
                fragment = new BookingBookingFragment();
                txtTitleToolbar.setText(getString(R.string.text_my_booking));
                break;
            case R.id.bottomNavigationMessagingId:
                fragment = new BookingMessagingFragment();
                txtTitleToolbar.setText(getString(R.string.text_notifications));
                break;
        }
        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null){
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.FrameLayout_Booking, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}
