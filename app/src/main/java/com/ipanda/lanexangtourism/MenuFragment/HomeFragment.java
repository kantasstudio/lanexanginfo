package com.ipanda.lanexangtourism.MenuFragment;

import android.Manifest;
import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.agrawalsuneet.loaderspack.loaders.MultipleRippleLoader;
import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.interfaces.ItemClickListener;
import com.denzcoskun.imageslider.models.SlideModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.ipanda.lanexangtourism.Activity.BookingPackageTourActivity;
import com.ipanda.lanexangtourism.Activity.HomeSearchActivity;
import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Activity.ProgramTourCreateActivity;
import com.ipanda.lanexangtourism.Activity.SettingsActivity;
import com.ipanda.lanexangtourism.Activity.VideoContentActivity;
import com.ipanda.lanexangtourism.Adapter.ItemsNearHotelRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ItemsNearRestaurantRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ItemsNearTourismRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.PackageTourViewPagerAdapter;
import com.ipanda.lanexangtourism.Adapter.VideoContentAdapter;
import com.ipanda.lanexangtourism.ClickListener.ItemBookmarkClickListener;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.LoadMenuFragmentHelper;
import com.ipanda.lanexangtourism.Interface_click.ItemsClickListener;
import com.ipanda.lanexangtourism.Interface_click.ScreenPackageTourClickListener;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsContactModel;
import com.ipanda.lanexangtourism.Model.ItemsHotelModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsRestaurantModel;
import com.ipanda.lanexangtourism.Model.ItemsShoppingModel;
import com.ipanda.lanexangtourism.Model.ItemsTourismModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.ScreenNewsReleaseModel;
import com.ipanda.lanexangtourism.Model.ScreenPackageTourModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.VideoContentModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsHotelAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsRestaurantAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsTourismAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ScreenNewsReleaseAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ScreenPackageTourAsyncTask;
import com.ipanda.lanexangtourism.asynctask.VideoContentAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsHotelCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsRestaurantCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsTourismCallBack;
import com.ipanda.lanexangtourism.interface_callback.ScreenNewsReleaseCallBack;
import com.ipanda.lanexangtourism.interface_callback.ScreenPackageTourCallBack;
import com.ipanda.lanexangtourism.interface_callback.VideoContentCallBack;
import com.ipanda.lanexangtourism.items_view.ViewPackageTourActivity;
import com.ipanda.lanexangtourism.items_view.ViewTourismActivity;
import com.ipanda.lanexangtourism.post_search.HomeHotelPostRequest;
import com.ipanda.lanexangtourism.post_search.HomeRestaurantPostRequest;
import com.ipanda.lanexangtourism.post_search.HomeTourismPostRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;


public class HomeFragment extends Fragment implements View.OnClickListener, ItemsTourismCallBack, ItemsRestaurantCallBack,
        ItemsHotelCallBack, ItemsClickListener, ItemBookmarkClickListener , ScreenNewsReleaseCallBack , ScreenPackageTourCallBack ,
        VideoContentCallBack, ExchangeRateCallBack, ScreenPackageTourClickListener {


    //Variables
    private static String TAG = HomeFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private Toolbar mToolbar;

    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle toggle;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    private AppBarLayout appBarLayout;

    private ViewPager screenPager;

    private TabLayout tabIndicator;

    private ArrayList<ScreenPackageTourModel> listScreen;

    private PackageTourViewPagerAdapter mAdapter;

    private ItemsNearTourismRecyclerViewAdapter mTourismAdapter;

    private ItemsNearRestaurantRecyclerViewAdapter mRestaurantAdapter;

    private ItemsNearHotelRecyclerViewAdapter mHotelAdapter;

    private RecyclerView recycler_near_tourist, recycler_near_hotels, recycler_near_restaurants;

    private RecyclerView recycler_video_content;

    private CardView mShowSearch, btnSearch;

    private LayoutTransition transition;

    private ImageView btnBurgerClose, btnShowBurgerOpen, btnShowSttings;

    private LoadMenuFragmentHelper loadMenuFragmentHelper;

    public static int navItemIndex = 0;

    private String urlTourism = "", urlRestaurant = "", urlHotel = "";

    private View view;

    private LinearLayout btnTourismLocation, btnRestaurant, btnShopping, btnHotel, btnContact, btnBooking;

    private TextView txt_all_tourist, txt_all_hotels, txt_all_restaurants, txt_all_package_tour, txt_all_video_content;

    private MultipleRippleLoader multipleRippleLoader_near_restaurants, multipleRippleLoader_near_hotels, multipleRippleLoader_near_tourist;

    private LinearLayout show_not_found_near_restaurants, show_not_found_near_hotels, show_not_found_near_tourist;

    private String userId;

    private String language;

    private CardView btnSearchIn;

    private Button btn_create_program_tour;

    private ImageView img_location_mark_id;

    private ImageSlider imageSlider;

    //Location
    private static final int PERMISSION_ID = 1;
    private FusedLocationProviderClient client;
    private Location locationResult;


    private VideoContentAdapter videoContentAdapter;

    private RateModel rateModel;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private ArrayList<ItemsRestaurantModel> newRestaurantOffline = new ArrayList<>();

    private ArrayList<ItemsHotelModel> newHotelOffline = new ArrayList<>();

    private ArrayList<ItemsTourismModel> newTourismOffline = new ArrayList<>();

    private int getVersion;

    public HomeFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);

        if (isModeOnline){
            String urlRate = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ExchangeRate";
            new ExchangeRateAsyncTask(this).execute(urlRate);
        }else {
            getVersion = getContext().getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
            if (getVersion != 0) {
                mDatabase = new DatabaseHelper(getContext(), getVersion);
                mManager = new DatabaseManager(getContext(), mDatabase);
            }else {
                alertDialog();
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        //views
        collapsingToolbarLayout = view.findViewById(R.id.CollapsingToolbarLayout_main);
        appBarLayout = view.findViewById(R.id.AppBarLayout_main);
        mDrawerLayout = view.findViewById(R.id.drawer_layout);
        mToolbar = view.findViewById(R.id.toolbar);
        btnSearch = view.findViewById(R.id.btn_search_home);
        recycler_near_tourist = view.findViewById(R.id.recycler_near_tourist);
        recycler_near_hotels = view.findViewById(R.id.recycler_near_hotels);
        recycler_near_restaurants = view.findViewById(R.id.recycler_near_restaurants);
        recycler_video_content = view.findViewById(R.id.recycler_video_content);
        multipleRippleLoader_near_restaurants = view.findViewById(R.id.multipleRippleLoader_near_restaurants);
        multipleRippleLoader_near_hotels = view.findViewById(R.id.multipleRippleLoader_near_hotels);
        multipleRippleLoader_near_tourist = view.findViewById(R.id.multipleRippleLoader_near_tourist);
        show_not_found_near_restaurants = view.findViewById(R.id.show_not_found_near_restaurants);
        show_not_found_near_hotels = view.findViewById(R.id.show_not_found_near_hotels);
        show_not_found_near_tourist = view.findViewById(R.id.show_not_found_near_tourist);
        btn_create_program_tour = view.findViewById(R.id.btn_create_program_tour);
        img_location_mark_id = view.findViewById(R.id.img_location_mark_id);
        imageSlider = view.findViewById(R.id.image_slider);
        txt_all_package_tour = view.findViewById(R.id.txt_all_package_tour);
        txt_all_video_content = view.findViewById(R.id.txt_all_video_content);


        //setup Toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);

        screenPager = view.findViewById(R.id.ViewPager_Package_Tour);
        tabIndicator = view.findViewById(R.id.package_tour_tab_indicator);

        mShowSearch = view.findViewById(R.id.showSearch);
        btnShowBurgerOpen = view.findViewById(R.id.btn_show_burger);
        btnShowSttings = view.findViewById(R.id.btn_show_settings);
        btnBurgerClose = view.findViewById(R.id.btn_burger_close);
        btnSearchIn = view.findViewById(R.id.cv_btn_search);

        //views button layout
        btnTourismLocation = view.findViewById(R.id.selected_tourism_location);
        btnRestaurant = view.findViewById(R.id.selected_restaurant);
        btnShopping = view.findViewById(R.id.selected_shopping);
        btnHotel = view.findViewById(R.id.selected_hotel);
        btnContact = view.findViewById(R.id.selected_contact);
        btnBooking = view.findViewById(R.id.selected_booking);
        txt_all_tourist = view.findViewById(R.id.txt_all_tourist);
        txt_all_hotels = view.findViewById(R.id.txt_all_hotels);
        txt_all_restaurants = view.findViewById(R.id.txt_all_restaurants);

        //set on click all items
        setOnclickAllItems();

        //set on click button
        btnShowBurgerOpen.setOnClickListener(this);
        btnShowSttings.setOnClickListener(this);
        btnBurgerClose.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnSearchIn.setOnClickListener(this);

        btnTourismLocation.setOnClickListener(this);
        btnRestaurant.setOnClickListener(this);
        btnShopping.setOnClickListener(this);
        btnHotel.setOnClickListener(this);
        btnContact.setOnClickListener(this);
        btnBooking.setOnClickListener(this);

        txt_all_package_tour.setOnClickListener(this);
        txt_all_video_content.setOnClickListener(this);

        //setup ActionBarDrawerToggle
        toggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        if(((AppCompatActivity)getActivity()).getSupportActionBar() != null) {
            Drawable drawable = getResources().getDrawable(R.drawable.ic_menu_burger);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 70, 60, true));
//            newDrawable.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        }

        client = LocationServices.getFusedLocationProviderClient(getActivity());
        getPermissionLocation();

        if (isModeOnline){

            img_location_mark_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (locationResult != null) {
                        methodPostHomeSearchMyLocationRestaurant();
                        methodPostHomeSearchMyLocationHotel();
                        methodPostHomeSearchMyLocationTourism();
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.permission_required) + "", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btn_create_program_tour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getContext(), ProgramTourCreateActivity.class));
                }
            });

            if (userId != null){
                urlRestaurant = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/3?user_id=" + userId;
                urlHotel = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/5?user_id=" + userId;
                urlTourism = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/2?user_id=" + userId;
            }else {
                urlRestaurant = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/3?user_id=";
                urlHotel = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/5?user_id=";
                urlTourism = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/2?user_id=";
            }

            getAsyncTask();

            String urlScreenNewsRelease = getString(R.string.app_api_ip) +"dasta_thailand/api/mobile/user/Adsense/2";
            new ScreenNewsReleaseAsyncTask(this).execute(urlScreenNewsRelease);

            String urlScreenPackageTour = getString(R.string.app_api_ip) +"dasta_thailand/api/mobile/user/Adsense/4";
            new ScreenPackageTourAsyncTask(this).execute(urlScreenPackageTour);

            String urlVideoContent = getString(R.string.app_api_ip) +"dasta_thailand/api/mobile/user/Adsense/getVideoContent";
            new VideoContentAsyncTask(this).execute(urlVideoContent);

        }else {
            if (getVersion != 0) {
                newTourismOffline = mManager.getItemsNearTourist();
                newRestaurantOffline = mManager.getItemsNearRestaurants();
                newHotelOffline = mManager.getItemsNearHotels();
                setItemsOfflineMode();
            }
        }


        //set changed collapsing
        setChangedCollapsing();

        //setup transition
        transition = new LayoutTransition();

        //set on click drawer menu
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        loadMenuFragmentHelper = new LoadMenuFragmentHelper(getContext(),view,fragmentTransaction,mDrawerLayout);
        loadMenuFragmentHelper.drawerMenu();


        return view;
    }

    private void setItemsOfflineMode() {

        if (newTourismOffline != null && newTourismOffline.size() != 0) {
            mTourismAdapter = new ItemsNearTourismRecyclerViewAdapter(getContext(), newTourismOffline, this, languageLocale.getLanguage(), this);
            recycler_near_tourist.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            recycler_near_tourist.setAdapter(mTourismAdapter);
            recycler_near_tourist.setVisibility(View.VISIBLE);
        }

        if (newRestaurantOffline != null && newRestaurantOffline.size() != 0) {
            mRestaurantAdapter = new ItemsNearRestaurantRecyclerViewAdapter(getContext(), newRestaurantOffline, this, languageLocale.getLanguage(), this);
            recycler_near_restaurants.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            recycler_near_restaurants.setAdapter(mRestaurantAdapter);
            recycler_near_restaurants.setVisibility(View.VISIBLE);
        }

        if (newHotelOffline != null && newHotelOffline.size() != 0) {
            mHotelAdapter = new ItemsNearHotelRecyclerViewAdapter(getContext(), newHotelOffline, this, languageLocale.getLanguage(), this);
            recycler_near_hotels.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            recycler_near_hotels.setAdapter(mHotelAdapter);
            recycler_near_hotels.setVisibility(View.VISIBLE);
        }

        ConstraintLayout con_package_tour_view = view.findViewById(R.id.con_package_tour_view);
        con_package_tour_view.setVisibility(View.GONE);

        ConstraintLayout con_list_video_view = view.findViewById(R.id.con_list_video_view);
        con_list_video_view.setVisibility(View.GONE);


        try {
            ImageView imgModeOffline = view.findViewById(R.id.img_mode_offline);
            imgModeOffline.setImageResource(R.drawable.offline_mode);
            imgModeOffline.setVisibility(View.VISIBLE);
        }catch (Exception e){
            Log.e(TAG, e.getMessage().toString());
        }



    }

    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {
        LocationManager locationManager = (LocationManager) getActivity()
                .getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            client.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location != null){
                        locationResult = location;
                        if (userId != null){
                            urlRestaurant = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/3?user_id=" + userId + "&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                            urlHotel = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/5?user_id=" + userId + "&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                            urlTourism = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/2?user_id=" + userId + "&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                        }else {
                            urlRestaurant = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/3?user_id=&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                            urlHotel = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/5?user_id=&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                            urlTourism = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/2?user_id=&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                        }
                        getAsyncTask();
                    }else {
                        LocationRequest locationRequest = new LocationRequest()
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setInterval(10000)
                                .setFastestInterval(1000)
                                .setNumUpdates(1);
                        LocationCallback locationCallback = new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                super.onLocationResult(locationResult);
                                Location location1 = locationResult.getLastLocation();
                                Toast.makeText(getContext(), location1.getLatitude()+"  "+location1.getLongitude(), Toast.LENGTH_SHORT).show();
                            }
                        };
                        client.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper());
                    }
                }
            });
        }else {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }


    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        }
    }



    private void methodPostHomeSearchMyLocationRestaurant(){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SearchItemNearMe");
        HashMap<String,String> paramsPost = new HashMap<>();
        if (userId != null){
            paramsPost.put("user_id",userId);
        }else {
            paramsPost.put("user_id","");
        }
        paramsPost.put("category_id", "3");
        paramsPost.put("latitude", locationResult.getLatitude()+"");
        paramsPost.put("longitude", locationResult.getLongitude()+"");
        httpCallPost.setParams(paramsPost);
        new HomeRestaurantPostRequest(this).execute(httpCallPost);
    }

    private void methodPostHomeSearchMyLocationHotel(){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SearchItemNearMe");
        HashMap<String,String> paramsPost = new HashMap<>();
        if (userId != null){
            paramsPost.put("user_id",userId);
        }else {
            paramsPost.put("user_id","");
        }
        paramsPost.put("category_id", "5");
        paramsPost.put("latitude", locationResult.getLatitude()+"");
        paramsPost.put("longitude", locationResult.getLongitude()+"");
        httpCallPost.setParams(paramsPost);
        new HomeHotelPostRequest(this).execute(httpCallPost);
    }

    private void methodPostHomeSearchMyLocationTourism(){
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Search/SearchItemNearMe");
        HashMap<String,String> paramsPost = new HashMap<>();
        if (userId != null){
            paramsPost.put("user_id",userId);
        }else {
            paramsPost.put("user_id","");
        }
        paramsPost.put("category_id", "2");
        paramsPost.put("latitude", locationResult.getLatitude()+"");
        paramsPost.put("longitude", locationResult.getLongitude()+"");
        httpCallPost.setParams(paramsPost);
        new HomeTourismPostRequest(this).execute(httpCallPost);
    }


    private void getAsyncTask() {
        if (isModeOnline){
            new ItemsRestaurantAsyncTask(this).execute(urlRestaurant);
            new ItemsHotelAsyncTask(this).execute(urlHotel);
            new ItemsTourismAsyncTask(this).execute(urlTourism);
        }else {

        }

    }

    private void setOnclickAllItems() {
        txt_all_hotels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMenuFragmentHelper.selectedLoadFragment(5);
            }
        });

        txt_all_restaurants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMenuFragmentHelper.selectedLoadFragment(3);
            }
        });

        txt_all_tourist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMenuFragmentHelper.selectedLoadFragment(2);
            }
        });
    }


    private void setChangedCollapsing() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    mShowSearch.setVisibility(View.GONE);
                    mShowSearch.setLayoutTransition(transition);
                }
                if (scrollRange + verticalOffset == 0) {
                    mShowSearch.setVisibility(View.VISIBLE);
                    mShowSearch.setLayoutTransition(transition);
                    isShow = true;
                } else if(isShow) {
                    mShowSearch.setVisibility(View.GONE);
                    mShowSearch.setLayoutTransition(transition);
                    isShow = false;
                }
            }

        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_setting, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                Toast.makeText(getContext(), "Settings", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(),SettingsActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_burger_close:
                mDrawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.btn_show_burger:
                mDrawerLayout.openDrawer(Gravity.START);
                break;
            case R.id.btn_show_settings:
                startActivity(new Intent(getContext(), SettingsActivity.class));
                break;
            case R.id.btn_search_home:
                if (isModeOnline) {
                    startActivity(new Intent(getContext(), HomeSearchActivity.class));
                }else {

                }
                break;
            case R.id.selected_tourism_location:
                loadMenuFragmentHelper.selectedLoadFragment(2);
                break;
            case R.id.selected_restaurant:
                loadMenuFragmentHelper.selectedLoadFragment(3);
                break;
            case R.id.selected_shopping:
                loadMenuFragmentHelper.selectedLoadFragment(4);
                break;
            case R.id.selected_hotel:
                loadMenuFragmentHelper.selectedLoadFragment(5);
                break;
            case R.id.selected_contact:
                loadMenuFragmentHelper.selectedLoadFragment(6);
                break;
            case R.id.selected_booking:
                loadMenuFragmentHelper.selectedLoadFragment(7);
                break;
            case R.id.cv_btn_search:
                if (isModeOnline) {
                    startActivity(new Intent(getContext(), HomeSearchActivity.class));
                }else {

                }
                break;
            case R.id.txt_all_package_tour:
                startActivity(new Intent(getContext(), BookingPackageTourActivity.class));
                break;
            case R.id.txt_all_video_content:
                startActivity(new Intent(getContext(), VideoContentActivity.class));
                break;
        }
    }

    @Override
    public void onPreCallService() {
        if (multipleRippleLoader_near_restaurants != null) {
            multipleRippleLoader_near_restaurants.setVisibility(View.VISIBLE);
            show_not_found_near_restaurants.setVisibility(View.GONE);
            recycler_near_restaurants.setVisibility(View.GONE);
        }
        if (multipleRippleLoader_near_tourist != null) {
            multipleRippleLoader_near_tourist.setVisibility(View.VISIBLE);
            show_not_found_near_tourist.setVisibility(View.GONE);
            recycler_near_tourist.setVisibility(View.GONE);
        }
        if (multipleRippleLoader_near_hotels != null) {
            multipleRippleLoader_near_hotels.setVisibility(View.VISIBLE);
            show_not_found_near_hotels.setVisibility(View.GONE);
            recycler_near_hotels.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }

    @Override
    public void onRequestCompleteVideoContent(ArrayList<VideoContentModel> videoContentArrayList) {
        if (videoContentArrayList != null && videoContentArrayList.size() != 0){
            videoContentAdapter = new VideoContentAdapter(getContext(), videoContentArrayList,languageLocale.getLanguage());
            recycler_video_content.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
            recycler_video_content.setAdapter(videoContentAdapter);
        }
    }

    @Override
    public void onRequestCompleteListenerPackageTour(ArrayList<ScreenPackageTourModel> screenPackageTourArrayList) {
        if (screenPackageTourArrayList != null && screenPackageTourArrayList.size() != 0){
            //setup viewpager
            mAdapter = new PackageTourViewPagerAdapter(getContext(),screenPackageTourArrayList, this);
            screenPager.setAdapter(mAdapter);

            //setup tabLayout with viewpager
            tabIndicator.setupWithViewPager(screenPager);


        }
    }

    @Override
    public void itemClickedScreenPackageTour(ItemsModel itemsModel) {
        if (itemsModel != null) {
            Intent intent = new Intent(getContext(), ViewPackageTourActivity.class);
            intent.putExtra("ITEMS_MODEL", itemsModel);
            intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestCompleteListenerNewsRelease(ArrayList<ScreenNewsReleaseModel> releaseArrayList) {
        if (releaseArrayList != null && releaseArrayList.size() != 0){
            List<SlideModel> slideList = new ArrayList<>();
            try {
                for (ScreenNewsReleaseModel screen : releaseArrayList){
                    String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/"+screen.getScreenImage();
                    slideList.add(new SlideModel(paths,"",ScaleTypes.CENTER_CROP));
                }
            }catch (Exception e){
                Log.e(TAG, e.getMessage().toString());
            }


            //set image slide
            imageSlider.setImageList(slideList, ScaleTypes.CENTER_CROP);

            imageSlider.setItemClickListener(new ItemClickListener() {
                @Override
                public void onItemSelected(int i) {
                    if (releaseArrayList != null && releaseArrayList.size() != 0) {
                        for (int j = 0; j < slideList.size(); j++) {
                            if (i == j) {
                                openWebPage(releaseArrayList.get(j).getLinkURL());
                            }
                        }
                    }
                }
            });
        }
    }


    @Override
    public void onRequestCompleteRestaurant(ArrayList<ItemsRestaurantModel> restaurantArrayList) {
        if (restaurantArrayList != null && restaurantArrayList.size() != 0){
            Collections.sort(restaurantArrayList,new DistanceSorterRestaurant());
            mRestaurantAdapter = new ItemsNearRestaurantRecyclerViewAdapter(getContext(),restaurantArrayList,this,languageLocale.getLanguage(),this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            recycler_near_restaurants.setLayoutManager(layoutManager);
            recycler_near_restaurants.setAdapter(mRestaurantAdapter);

            multipleRippleLoader_near_restaurants.setVisibility(View.GONE);
            show_not_found_near_restaurants.setVisibility(View.GONE);
            recycler_near_restaurants.setVisibility(View.VISIBLE);
        }else {
            multipleRippleLoader_near_restaurants.setVisibility(View.GONE);
            show_not_found_near_restaurants.setVisibility(View.VISIBLE);
            recycler_near_restaurants.setVisibility(View.GONE);
        }
    }






    public class DistanceSorterRestaurant implements Comparator<ItemsRestaurantModel> {
        public int compare(ItemsRestaurantModel o1, ItemsRestaurantModel o2) {
            return o1.getDistance() - o2.getDistance();
        }
    }

    @Override
    public void onRequestCompleteHotel(ArrayList<ItemsHotelModel> hotelArrayList) {
        if (hotelArrayList != null && hotelArrayList.size() != 0){
            Collections.sort(hotelArrayList,new DistanceSorterHotel());
            mHotelAdapter = new ItemsNearHotelRecyclerViewAdapter(getContext(),hotelArrayList,this,languageLocale.getLanguage(),this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            recycler_near_hotels.setLayoutManager(layoutManager);
            recycler_near_hotels.setAdapter(mHotelAdapter);

            multipleRippleLoader_near_hotels.setVisibility(View.GONE);
            show_not_found_near_hotels.setVisibility(View.GONE);
            recycler_near_hotels.setVisibility(View.VISIBLE);

        }else {
            multipleRippleLoader_near_hotels.setVisibility(View.GONE);
            show_not_found_near_hotels.setVisibility(View.VISIBLE);
            recycler_near_hotels.setVisibility(View.GONE);

        }
    }

    public class DistanceSorterHotel implements Comparator<ItemsHotelModel> {
        public int compare(ItemsHotelModel o1, ItemsHotelModel o2) {
            return o1.getDistance() - o2.getDistance();
        }
    }

    @Override
    public void onRequestCompleteTourism(ArrayList<ItemsTourismModel> tourismArrayList) {
        if (tourismArrayList != null && tourismArrayList.size() != 0){
            Collections.sort(tourismArrayList,new DistanceSorterTourism());
            mTourismAdapter = new ItemsNearTourismRecyclerViewAdapter(getContext(),tourismArrayList,this, languageLocale.getLanguage(),this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            recycler_near_tourist.setLayoutManager(layoutManager);
            recycler_near_tourist.setAdapter(mTourismAdapter);

            multipleRippleLoader_near_tourist.setVisibility(View.GONE);
            show_not_found_near_tourist.setVisibility(View.GONE);
            recycler_near_tourist.setVisibility(View.VISIBLE);
        }else {
            multipleRippleLoader_near_tourist.setVisibility(View.GONE);
            show_not_found_near_tourist.setVisibility(View.VISIBLE);
            recycler_near_tourist.setVisibility(View.GONE);
        }
    }

    public class DistanceSorterTourism implements Comparator<ItemsTourismModel> {
        public int compare(ItemsTourismModel o1, ItemsTourismModel o2) {
            return o1.getDistance() - o2.getDistance();
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClicked(ItemsTourismModel items) {
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",items);
        intent.putExtra("CATEGORY",2);
        startActivity(intent);
    }

    @Override
    public void itemClicked(ItemsRestaurantModel items) {
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",items);
        intent.putExtra("CATEGORY",3);
        startActivity(intent);
    }

    @Override
    public void itemClicked(ItemsShoppingModel items) {

    }

    @Override
    public void itemClicked(ItemsHotelModel items) {
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",items);
        intent.putExtra("CATEGORY",5);
        startActivity(intent);
    }

    @Override
    public void itemClicked(ItemsContactModel items) {

    }

    @Override
    public void itemClicked(ImageView bookmarks, ItemsModel itemsModel) {

    }


    @Override
    public void itemClickedBookmark(ImageView bookmarks, ItemsModel itemsModel) {
        if (isModeOnline) {
            if (itemsModel != null && userId != null) {
                HttpCall httpCallPost = new HttpCall();
                httpCallPost.setMethodType(HttpCall.POST);
                httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/BookMarks");
                HashMap<String, String> paramsPost = new HashMap<>();
                paramsPost.put("User_user_id", userId);
                paramsPost.put("Items_items_id", String.valueOf(itemsModel.getItemsId()));
                httpCallPost.setParams(paramsPost);
                new HttpPostRequestAsyncTask().execute(httpCallPost);
            } else {
                new AlertDialog.Builder(getContext())
                        .setTitle("กรุณาล็อกอินก่อน")
                        .setMessage("....")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(getContext(), LoginActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create().show();
            }
            if (itemsModel.getBookMarksModel() != null) {
                if (itemsModel.getBookMarksModel().isBookmarksState()) {
                    itemsModel.getBookMarksModel().setBookmarksState(false);
                    bookmarks.setImageResource(R.drawable.ic_bookmark);
                } else {
                    itemsModel.getBookMarksModel().setBookmarksState(true);
                    bookmarks.setImageResource(R.drawable.ic_bookmark_red);
                }
            }
        }else {
            Toast.makeText(getContext(), "Offline Mode.", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isLocationEnabled(){
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean checkPermissions(){
        if (ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    private void getPermissionLocation(){
        if (ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            getCurrentLocation();
        }else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_ID);
        }
    }

    private void requestLocationPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION)
                && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION)){
            new AlertDialog.Builder(getContext())
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed because of this and that")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_ID);

                        }
                    })
                    .create().show();
        }else {
            ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_ID);
        }
    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getContext(), "Permission GRANTED", Toast.LENGTH_SHORT).show();
                getCurrentLocation();
            }else {
                Toast.makeText(getContext(), "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}


