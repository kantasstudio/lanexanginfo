package com.ipanda.lanexangtourism.MenuFragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.agrawalsuneet.loaderspack.loaders.MultipleRippleLoader;
import com.google.android.material.tabs.TabLayout;
import com.ipanda.lanexangtourism.Activity.ItemsSearchActivity;
import com.ipanda.lanexangtourism.Activity.ProgramTourActivity;
import com.ipanda.lanexangtourism.Activity.ProgramTourCreateActivity;
import com.ipanda.lanexangtourism.Activity.SettingsActivity;
import com.ipanda.lanexangtourism.Adapter.OfficialViewPagerAdapter;
import com.ipanda.lanexangtourism.Adapter.ProgramTourMainRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.LoadMenuFragmentHelper;
import com.ipanda.lanexangtourism.Interface_click.ProgramTourClickListener;
import com.ipanda.lanexangtourism.Interface_click.ScreenOfficialClickListener;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.ScreenOfficialModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ProgramTourMainAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ScreenOfficialAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProgramTourMainCallBack;
import com.ipanda.lanexangtourism.interface_callback.ScreenOfficialCallBack;
import com.ipanda.lanexangtourism.items_view.ViewPackageTourActivity;
import com.ipanda.lanexangtourism.items_view.ViewProgramTourActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class ProgramTourFragment extends Fragment implements View.OnClickListener , ProgramTourMainCallBack , ProgramTourClickListener ,
        ScreenOfficialCallBack , ScreenOfficialClickListener , ExchangeRateCallBack {

    //Variables
    private static final String TAG = ProgramTourFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private Toolbar mToolbar;

    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle toggle;

    private View view;

    private ViewPager screenPager;

    private TabLayout tabIndicator;

    private OfficialViewPagerAdapter mAdapter;

    public static int navItemIndex = 0;

    private RecyclerView recycler_program_tour_main;

    private ConstraintLayout btnSearchProgramTour;

    private ConstraintLayout con_list_official_view;

    private Button btn_create_program_tour;

    private TextView txtTitleToolbar;

    private TextView txt_program_tour_all;

    private TextView txt_all_official;

    private MultipleRippleLoader multipleRippleLoader;

    private LinearLayout show_not_found;

    private String userId;

    private RateModel rateModel;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    private ArrayList<ProgramTourModel> newProgramTourOffline = new ArrayList<>();

    public ProgramTourFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_program_tour, container, false);

        //views
        mToolbar = view.findViewById(R.id.menu_toolbar_settings);
        mDrawerLayout = view.findViewById(R.id.drawer_layout);
        screenPager = view.findViewById(R.id.ViewPager_Official);
        tabIndicator = view.findViewById(R.id.official_tab_indicator);
        btnSearchProgramTour = view.findViewById(R.id.btn_search_program_tour);
        txtTitleToolbar = view.findViewById(R.id.toolbar_title);
        txt_program_tour_all = view.findViewById(R.id.txt_program_tour_all);
        recycler_program_tour_main = view.findViewById(R.id.recycler_program_tour_main);
        btn_create_program_tour = view.findViewById(R.id.btn_create_program_tour);
        multipleRippleLoader = view.findViewById(R.id.multipleRippleLoader);
        show_not_found = view.findViewById(R.id.show_not_found);
        txt_all_official = view.findViewById(R.id.txt_all_official);
        con_list_official_view = view.findViewById(R.id.con_list_official_view);

        //setup Toolbar
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        //set title toolbar
        txtTitleToolbar.setText(getString(R.string.txt_program_tour));

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        //setup ActionBarDrawerToggle
        toggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            Drawable drawable = getResources().getDrawable(R.drawable.ic_menu_burger_dark);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 70, 60, true));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        }

        //set on click drawer menu
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        LoadMenuFragmentHelper helper = new LoadMenuFragmentHelper(getContext(), view, fragmentTransaction, mDrawerLayout);
        helper.drawerMenu();

        //set button on click search program tour
        btnSearchProgramTour.setOnClickListener(this);
        txt_program_tour_all.setOnClickListener(this);
        btn_create_program_tour.setOnClickListener(this);
        txt_all_official.setOnClickListener(this);

        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        String url = "";
        if (userId != null) {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ProgramTour?user_id="+userId;
        } else {
            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ProgramTour/";
        }

        String urlScreenOfficial = getString(R.string.app_api_ip) +"dasta_thailand/api/mobile/user/Adsense/1";
        String urlRate = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ExchangeRate";

        if (isModeOnline){
            new ExchangeRateAsyncTask(this).execute(urlRate);
            new ProgramTourMainAsyncTask(this).execute(url);
            new ScreenOfficialAsyncTask(this).execute(urlScreenOfficial);
        }else {
            getVersion = getContext().getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
            if (getVersion != 0) {
                mDatabase = new DatabaseHelper(getContext(), getVersion);
                mManager = new DatabaseManager(getContext(), mDatabase);
                setItemsOfflineMode();
            }else {
                alertDialog();
            }
        }



        return view;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_setting_dark, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Toast.makeText(getContext(), "Settings", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(), SettingsActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_search_program_tour:
                if (isModeOnline) {
                    startActivity(new Intent(getContext(), ItemsSearchActivity.class));
                }else {
                    Toast.makeText(getContext(), "Offline Mode. ", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.txt_all_official:
                if (isModeOnline) {
                    startActivity(new Intent(getContext(), ItemsSearchActivity.class));
                }else {
                    Toast.makeText(getContext(), "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.txt_program_tour_all:
                startActivity(new Intent(getContext(), ProgramTourActivity.class));
                break;
            case R.id.btn_create_program_tour:
                if (isModeOnline) {
                    startActivity(new Intent(getContext(), ProgramTourCreateActivity.class));
                }else {
                    Toast.makeText(getContext(), "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void setItemsOfflineMode() {
        newProgramTourOffline = mManager.getListProgramTours();
        if (newProgramTourOffline != null && newProgramTourOffline.size() != 0){
            ProgramTourMainRecyclerViewAdapter adapter = new ProgramTourMainRecyclerViewAdapter(getContext(), newProgramTourOffline, languageLocale.getLanguage(), this);
            recycler_program_tour_main.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            recycler_program_tour_main.setAdapter(adapter);
            recycler_program_tour_main.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onPreCallService() {
        multipleRippleLoader.setVisibility(View.VISIBLE);
        show_not_found.setVisibility(View.GONE);
        recycler_program_tour_main.setVisibility(View.GONE);
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }

    @Override
    public void onRequestCompleteListenerScreenOfficial(ArrayList<ScreenOfficialModel> screenOfficialArrayList) {
        if (screenOfficialArrayList != null && screenOfficialArrayList.size() != 0){
            //setup viewpager
            mAdapter = new OfficialViewPagerAdapter(getContext(), screenOfficialArrayList, this);
            screenPager.setAdapter(mAdapter);

            //setup tabLayout with viewpager
            tabIndicator.setupWithViewPager(screenPager);
        }
    }

    @Override
    public void onRequestCompleteListener(ArrayList<ProgramTourModel> programTourModelArrayList) {
        if (programTourModelArrayList != null && programTourModelArrayList.size() != 0) {
            Log.e("check data", programTourModelArrayList + "");

            ProgramTourMainRecyclerViewAdapter adapter = new ProgramTourMainRecyclerViewAdapter(getContext(), programTourModelArrayList, languageLocale.getLanguage(), this);
            recycler_program_tour_main.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            recycler_program_tour_main.setAdapter(adapter);

            multipleRippleLoader.setVisibility(View.GONE);
            show_not_found.setVisibility(View.GONE);
            recycler_program_tour_main.setVisibility(View.VISIBLE);

        } else {
            multipleRippleLoader.setVisibility(View.GONE);
            show_not_found.setVisibility(View.VISIBLE);
            recycler_program_tour_main.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClicked(ProgramTourModel tourModel) {
        Intent intent = new Intent(getContext(), ViewProgramTourActivity.class);
        intent.putExtra("PROGRAM_TOUR", tourModel);
        intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
        startActivity(intent);
    }

    @Override
    public void itemClickedLikeProgramTour(ImageView img_like, TextView txt_count_like, ProgramTourModel tourModel) {
        if (isModeOnline) {
            String userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Likes");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("ProgramTour_programTour_id", String.valueOf(tourModel.getProgramTourId()));
            httpCallPost.setParams(paramsPost);
            if (userId != null) {
                new HttpPostRequestAsyncTask().execute(httpCallPost);
                loadFragment();
            } else {

            }
        }else {
            Toast.makeText(getContext(), "Offline Mode.", Toast.LENGTH_SHORT).show();
        }
    }

    // Reload current fragment
    private boolean loadFragment(){
        Fragment fragment = new ProgramTourFragment();
        if (fragment != null){
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.FrameLayout_Menu, fragment)
                    .commit();
            return true;
        }
        return false;
    }//End Reload current fragment


    @Override
    public void itemClickedScreenOfficial(ProgramTourModel programTourModel) {
        if (isModeOnline) {
            if (programTourModel != null) {
                Intent intent = new Intent(getContext(), ViewProgramTourActivity.class);
                intent.putExtra("PROGRAM_TOUR", programTourModel);
                intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                startActivity(intent);
            }
        }
    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
