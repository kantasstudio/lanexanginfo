package com.ipanda.lanexangtourism.MenuFragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.NavigationFragment.ListViewNavigationFragment;
import com.ipanda.lanexangtourism.NavigationFragment.MapsViewNavigationFragment;
import com.ipanda.lanexangtourism.R;


public class ShoppingFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    //Variables
    private static final String TAG = ShoppingFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private View view;

    private BottomNavigationView bottomNavigationView;

    private Bundle bundle;

    public ShoppingFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {

        }
        bundle = new Bundle();
        bundle.putString("TEXT_TITLE",getResources().getString(R.string.txt_shopping));
        bundle.putInt("CATEGORY_ID",4);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_shopping, container, false);

        //views
        bottomNavigationView = view.findViewById(R.id.bottom_navigation);

        //setup navigation
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        //default fragment
        loadFragment(new ListViewNavigationFragment());

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()){
            case R.id.bottomNavigationListViewId:
                fragment = new ListViewNavigationFragment();
                break;
            case R.id.bottomNavigationMapsViewId:
                fragment = new MapsViewNavigationFragment();
                break;
        }
        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment){
        fragment.setArguments(bundle);
        if (fragment != null){
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.FrameLayout_Navigation, fragment)
                    .commit();
            return true;
        }
        return false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}
