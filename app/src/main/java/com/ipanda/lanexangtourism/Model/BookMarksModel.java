package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class BookMarksModel implements Parcelable {

    private int bookMarkId;
    private String bookMarkTimestamp;
    private int countUserBookMarks;
    private boolean bookmarksState;
    private int distance;
    private int distanceMetr;
    private ArrayList<UserModel> userModelArrayList;
    private ArrayList<ItemsModel> itemsModelArrayList;


    public BookMarksModel() {

    }

    public int getBookMarkId() {
        return bookMarkId;
    }

    public void setBookMarkId(int bookMarkId) {
        this.bookMarkId = bookMarkId;
    }

    public String getBookMarkTimestamp() {
        return bookMarkTimestamp;
    }

    public void setBookMarkTimestamp(String bookMarkTimestamp) {
        this.bookMarkTimestamp = bookMarkTimestamp;
    }

    public int getCountUserBookMarks() {
        return countUserBookMarks;
    }

    public void setCountUserBookMarks(int countUserBookMarks) {
        this.countUserBookMarks = countUserBookMarks;
    }

    public boolean isBookmarksState() {
        return bookmarksState;
    }

    public void setBookmarksState(boolean bookmarksState) {
        this.bookmarksState = bookmarksState;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDistanceMetr() {
        return distanceMetr;
    }

    public void setDistanceMetr(int distanceMetr) {
        this.distanceMetr = distanceMetr;
    }

    public ArrayList<UserModel> getUserModelArrayList() {
        return userModelArrayList;
    }

    public void setUserModelArrayList(ArrayList<UserModel> userModelArrayList) {
        this.userModelArrayList = userModelArrayList;
    }

    public ArrayList<ItemsModel> getItemsModelArrayList() {
        return itemsModelArrayList;
    }

    public void setItemsModelArrayList(ArrayList<ItemsModel> itemsModelArrayList) {
        this.itemsModelArrayList = itemsModelArrayList;
    }

    protected BookMarksModel(Parcel in) {
        bookMarkId = in.readInt();
        bookMarkTimestamp = in.readString();
        countUserBookMarks = in.readInt();
        bookmarksState = in.readByte() != 0;
        distance = in.readInt();
        distanceMetr = in.readInt();
        userModelArrayList = in.createTypedArrayList(UserModel.CREATOR);
        itemsModelArrayList = in.createTypedArrayList(ItemsModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(bookMarkId);
        dest.writeString(bookMarkTimestamp);
        dest.writeInt(countUserBookMarks);
        dest.writeByte((byte) (bookmarksState ? 1 : 0));
        dest.writeInt(distance);
        dest.writeInt(distanceMetr);
        dest.writeTypedList(userModelArrayList);
        dest.writeTypedList(itemsModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookMarksModel> CREATOR = new Creator<BookMarksModel>() {
        @Override
        public BookMarksModel createFromParcel(Parcel in) {
            return new BookMarksModel(in);
        }

        @Override
        public BookMarksModel[] newArray(int size) {
            return new BookMarksModel[size];
        }
    };
}
