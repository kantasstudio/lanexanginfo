package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class BookingConditionModel implements Parcelable {

    private int conditionId;
    private int conditionCategory_id;
    private String conditionTopicTH;
    private String conditionTopicEN;
    private String conditionTopicLO;
    private String conditionTopicZH;
    private String conditionDetailTH;
    private String conditionDetailEN;
    private String conditionDetailLO;
    private String conditionDetailZH;


    public BookingConditionModel() {

    }

    public int getConditionId() {
        return conditionId;
    }

    public void setConditionId(int conditionId) {
        this.conditionId = conditionId;
    }

    public int getConditionCategory_id() {
        return conditionCategory_id;
    }

    public void setConditionCategory_id(int conditionCategory_id) {
        this.conditionCategory_id = conditionCategory_id;
    }

    public String getConditionTopicTH() {
        return conditionTopicTH;
    }

    public void setConditionTopicTH(String conditionTopicTH) {
        this.conditionTopicTH = conditionTopicTH;
    }

    public String getConditionTopicEN() {
        return conditionTopicEN;
    }

    public void setConditionTopicEN(String conditionTopicEN) {
        this.conditionTopicEN = conditionTopicEN;
    }

    public String getConditionTopicLO() {
        return conditionTopicLO;
    }

    public void setConditionTopicLO(String conditionTopicLO) {
        this.conditionTopicLO = conditionTopicLO;
    }

    public String getConditionTopicZH() {
        return conditionTopicZH;
    }

    public void setConditionTopicZH(String conditionTopicZH) {
        this.conditionTopicZH = conditionTopicZH;
    }

    public String getConditionDetailTH() {
        return conditionDetailTH;
    }

    public void setConditionDetailTH(String conditionDetailTH) {
        this.conditionDetailTH = conditionDetailTH;
    }

    public String getConditionDetailEN() {
        return conditionDetailEN;
    }

    public void setConditionDetailEN(String conditionDetailEN) {
        this.conditionDetailEN = conditionDetailEN;
    }

    public String getConditionDetailLO() {
        return conditionDetailLO;
    }

    public void setConditionDetailLO(String conditionDetailLO) {
        this.conditionDetailLO = conditionDetailLO;
    }

    public String getConditionDetailZH() {
        return conditionDetailZH;
    }

    public void setConditionDetailZH(String conditionDetailZH) {
        this.conditionDetailZH = conditionDetailZH;
    }

    protected BookingConditionModel(Parcel in) {
        conditionId = in.readInt();
        conditionCategory_id = in.readInt();
        conditionTopicTH = in.readString();
        conditionTopicEN = in.readString();
        conditionTopicLO = in.readString();
        conditionTopicZH = in.readString();
        conditionDetailTH = in.readString();
        conditionDetailEN = in.readString();
        conditionDetailLO = in.readString();
        conditionDetailZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(conditionId);
        dest.writeInt(conditionCategory_id);
        dest.writeString(conditionTopicTH);
        dest.writeString(conditionTopicEN);
        dest.writeString(conditionTopicLO);
        dest.writeString(conditionTopicZH);
        dest.writeString(conditionDetailTH);
        dest.writeString(conditionDetailEN);
        dest.writeString(conditionDetailLO);
        dest.writeString(conditionDetailZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingConditionModel> CREATOR = new Creator<BookingConditionModel>() {
        @Override
        public BookingConditionModel createFromParcel(Parcel in) {
            return new BookingConditionModel(in);
        }

        @Override
        public BookingConditionModel[] newArray(int size) {
            return new BookingConditionModel[size];
        }
    };
}
