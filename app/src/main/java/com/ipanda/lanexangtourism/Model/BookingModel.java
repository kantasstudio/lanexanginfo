package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class BookingModel implements Parcelable {

    private int bookingId;
    private String checkIn;
    private String checkOut;
    private String checkInTime;
    private String checkOutTime;
    private String traveler;
    private int position;
    private int guestAdult;
    private int guestChild;
    private int specialAdult;
    private int specialChild;
    private int priceAdult;
    private int priceChild;
    private int priceSpecialAdult;
    private int priceSpecialChild;
    private int duration;
    private int scheduleSeat;
    private String scheduleCheckIn;
    private String scheduleCheckOut;
    private int scheduleGuestAdult;
    private int scheduleGuestAdultSingle;
    private int scheduleGuestChild;
    private int scheduleGuestChildBed;
    private PaymentTransactionModel paymentTransactionModel;
    private ItemsModel itemsModel;
    private RoomModel roomModel;
    private UserModel userModel;
    private ArrayList<ProductModel> productModelArrayList;

    public BookingModel() {

    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getTraveler() {
        return traveler;
    }

    public void setTraveler(String traveler) {
        this.traveler = traveler;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getGuestAdult() {
        return guestAdult;
    }

    public void setGuestAdult(int guestAdult) {
        this.guestAdult = guestAdult;
    }

    public int getGuestChild() {
        return guestChild;
    }

    public void setGuestChild(int guestChild) {
        this.guestChild = guestChild;
    }

    public int getSpecialAdult() {
        return specialAdult;
    }

    public void setSpecialAdult(int specialAdult) {
        this.specialAdult = specialAdult;
    }

    public int getSpecialChild() {
        return specialChild;
    }

    public void setSpecialChild(int specialChild) {
        this.specialChild = specialChild;
    }

    public int getPriceAdult() {
        return priceAdult;
    }

    public void setPriceAdult(int priceAdult) {
        this.priceAdult = priceAdult;
    }

    public int getPriceChild() {
        return priceChild;
    }

    public void setPriceChild(int priceChild) {
        this.priceChild = priceChild;
    }

    public int getPriceSpecialAdult() {
        return priceSpecialAdult;
    }

    public void setPriceSpecialAdult(int priceSpecialAdult) {
        this.priceSpecialAdult = priceSpecialAdult;
    }

    public int getPriceSpecialChild() {
        return priceSpecialChild;
    }

    public void setPriceSpecialChild(int priceSpecialChild) {
        this.priceSpecialChild = priceSpecialChild;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getScheduleSeat() {
        return scheduleSeat;
    }

    public void setScheduleSeat(int scheduleSeat) {
        this.scheduleSeat = scheduleSeat;
    }

    public String getScheduleCheckIn() {
        return scheduleCheckIn;
    }

    public void setScheduleCheckIn(String scheduleCheckIn) {
        this.scheduleCheckIn = scheduleCheckIn;
    }

    public String getScheduleCheckOut() {
        return scheduleCheckOut;
    }

    public void setScheduleCheckOut(String scheduleCheckOut) {
        this.scheduleCheckOut = scheduleCheckOut;
    }

    public int getScheduleGuestAdult() {
        return scheduleGuestAdult;
    }

    public void setScheduleGuestAdult(int scheduleGuestAdult) {
        this.scheduleGuestAdult = scheduleGuestAdult;
    }

    public int getScheduleGuestAdultSingle() {
        return scheduleGuestAdultSingle;
    }

    public void setScheduleGuestAdultSingle(int scheduleGuestAdultSingle) {
        this.scheduleGuestAdultSingle = scheduleGuestAdultSingle;
    }

    public int getScheduleGuestChild() {
        return scheduleGuestChild;
    }

    public void setScheduleGuestChild(int scheduleGuestChild) {
        this.scheduleGuestChild = scheduleGuestChild;
    }

    public int getScheduleGuestChildBed() {
        return scheduleGuestChildBed;
    }

    public void setScheduleGuestChildBed(int scheduleGuestChildBed) {
        this.scheduleGuestChildBed = scheduleGuestChildBed;
    }

    public PaymentTransactionModel getPaymentTransactionModel() {
        return paymentTransactionModel;
    }

    public void setPaymentTransactionModel(PaymentTransactionModel paymentTransactionModel) {
        this.paymentTransactionModel = paymentTransactionModel;
    }

    public ItemsModel getItemsModel() {
        return itemsModel;
    }

    public void setItemsModel(ItemsModel itemsModel) {
        this.itemsModel = itemsModel;
    }

    public RoomModel getRoomModel() {
        return roomModel;
    }

    public void setRoomModel(RoomModel roomModel) {
        this.roomModel = roomModel;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public ArrayList<ProductModel> getProductModelArrayList() {
        return productModelArrayList;
    }

    public void setProductModelArrayList(ArrayList<ProductModel> productModelArrayList) {
        this.productModelArrayList = productModelArrayList;
    }

    protected BookingModel(Parcel in) {
        bookingId = in.readInt();
        checkIn = in.readString();
        checkOut = in.readString();
        checkInTime = in.readString();
        checkOutTime = in.readString();
        traveler = in.readString();
        position = in.readInt();
        guestAdult = in.readInt();
        guestChild = in.readInt();
        specialAdult = in.readInt();
        specialChild = in.readInt();
        priceAdult = in.readInt();
        priceChild = in.readInt();
        priceSpecialAdult = in.readInt();
        priceSpecialChild = in.readInt();
        duration = in.readInt();
        scheduleSeat = in.readInt();
        scheduleCheckIn = in.readString();
        scheduleCheckOut = in.readString();
        scheduleGuestAdult = in.readInt();
        scheduleGuestAdultSingle = in.readInt();
        scheduleGuestChild = in.readInt();
        scheduleGuestChildBed = in.readInt();
        paymentTransactionModel = in.readParcelable(PaymentTransactionModel.class.getClassLoader());
        itemsModel = in.readParcelable(ItemsModel.class.getClassLoader());
        roomModel = in.readParcelable(RoomModel.class.getClassLoader());
        userModel = in.readParcelable(UserModel.class.getClassLoader());
        productModelArrayList = in.createTypedArrayList(ProductModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(bookingId);
        dest.writeString(checkIn);
        dest.writeString(checkOut);
        dest.writeString(checkInTime);
        dest.writeString(checkOutTime);
        dest.writeString(traveler);
        dest.writeInt(position);
        dest.writeInt(guestAdult);
        dest.writeInt(guestChild);
        dest.writeInt(specialAdult);
        dest.writeInt(specialChild);
        dest.writeInt(priceAdult);
        dest.writeInt(priceChild);
        dest.writeInt(priceSpecialAdult);
        dest.writeInt(priceSpecialChild);
        dest.writeInt(duration);
        dest.writeInt(scheduleSeat);
        dest.writeString(scheduleCheckIn);
        dest.writeString(scheduleCheckOut);
        dest.writeInt(scheduleGuestAdult);
        dest.writeInt(scheduleGuestAdultSingle);
        dest.writeInt(scheduleGuestChild);
        dest.writeInt(scheduleGuestChildBed);
        dest.writeParcelable(paymentTransactionModel, flags);
        dest.writeParcelable(itemsModel, flags);
        dest.writeParcelable(roomModel, flags);
        dest.writeParcelable(userModel, flags);
        dest.writeTypedList(productModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingModel> CREATOR = new Creator<BookingModel>() {
        @Override
        public BookingModel createFromParcel(Parcel in) {
            return new BookingModel(in);
        }

        @Override
        public BookingModel[] newArray(int size) {
            return new BookingModel[size];
        }
    };
}
