package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class BusinessModel implements Parcelable {

    private int businessId;
    private String nameTH;
    private String nameEN;
    private String nameLO;
    private String nameZH;
    private float latitude;
    private float longitude;
    private String presentAddress;
    private String licensePaths;
    private String operatorNumber;
    private String phone;
    private String web;
    private String facebook;
    private String line;
    private MenuItemModel menuItemModel;
    private UserModel userModel;

    public BusinessModel() {

    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public String getNameTH() {
        return nameTH;
    }

    public void setNameTH(String nameTH) {
        this.nameTH = nameTH;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameLO() {
        return nameLO;
    }

    public void setNameLO(String nameLO) {
        this.nameLO = nameLO;
    }

    public String getNameZH() {
        return nameZH;
    }

    public void setNameZH(String nameZH) {
        this.nameZH = nameZH;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getLicensePaths() {
        return licensePaths;
    }

    public void setLicensePaths(String licensePaths) {
        this.licensePaths = licensePaths;
    }

    public String getOperatorNumber() {
        return operatorNumber;
    }

    public void setOperatorNumber(String operatorNumber) {
        this.operatorNumber = operatorNumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public MenuItemModel getMenuItemModel() {
        return menuItemModel;
    }

    public void setMenuItemModel(MenuItemModel menuItemModel) {
        this.menuItemModel = menuItemModel;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    protected BusinessModel(Parcel in) {
        businessId = in.readInt();
        nameTH = in.readString();
        nameEN = in.readString();
        nameLO = in.readString();
        nameZH = in.readString();
        latitude = in.readFloat();
        longitude = in.readFloat();
        presentAddress = in.readString();
        licensePaths = in.readString();
        operatorNumber = in.readString();
        phone = in.readString();
        web = in.readString();
        facebook = in.readString();
        line = in.readString();
        menuItemModel = in.readParcelable(MenuItemModel.class.getClassLoader());
        userModel = in.readParcelable(UserModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(businessId);
        dest.writeString(nameTH);
        dest.writeString(nameEN);
        dest.writeString(nameLO);
        dest.writeString(nameZH);
        dest.writeFloat(latitude);
        dest.writeFloat(longitude);
        dest.writeString(presentAddress);
        dest.writeString(licensePaths);
        dest.writeString(operatorNumber);
        dest.writeString(phone);
        dest.writeString(web);
        dest.writeString(facebook);
        dest.writeString(line);
        dest.writeParcelable(menuItemModel, flags);
        dest.writeParcelable(userModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BusinessModel> CREATOR = new Creator<BusinessModel>() {
        @Override
        public BusinessModel createFromParcel(Parcel in) {
            return new BusinessModel(in);
        }

        @Override
        public BusinessModel[] newArray(int size) {
            return new BusinessModel[size];
        }
    };
}
