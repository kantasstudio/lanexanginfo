package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class CarModel implements Parcelable {

    private int rentalId;
    private int seats;
    private int pricePerDay;
    private int pickUpPrice;
    private int returnPrice;
    private int depositPrice;
    private int overcharge;
    private String typeTH;
    private String typeEN;
    private String typeLO;
    private String typeZH;
    private String gearSystemTH;
    private String gearSystemEN;
    private String gearSystemLO;
    private String gearSystemZH;
    private boolean isBasicInsurance;
    private String basicInsuranceTH;
    private String basicInsuranceEN;
    private String basicInsuranceLO;
    private String basicInsuranceZH;


    public CarModel() {

    }

    public int getRentalId() {
        return rentalId;
    }

    public void setRentalId(int rentalId) {
        this.rentalId = rentalId;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(int pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public int getPickUpPrice() {
        return pickUpPrice;
    }

    public void setPickUpPrice(int pickUpPrice) {
        this.pickUpPrice = pickUpPrice;
    }

    public int getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(int returnPrice) {
        this.returnPrice = returnPrice;
    }

    public int getDepositPrice() {
        return depositPrice;
    }

    public void setDepositPrice(int depositPrice) {
        this.depositPrice = depositPrice;
    }

    public int getOvercharge() {
        return overcharge;
    }

    public void setOvercharge(int overcharge) {
        this.overcharge = overcharge;
    }

    public String getTypeTH() {
        return typeTH;
    }

    public void setTypeTH(String typeTH) {
        this.typeTH = typeTH;
    }

    public String getTypeEN() {
        return typeEN;
    }

    public void setTypeEN(String typeEN) {
        this.typeEN = typeEN;
    }

    public String getTypeLO() {
        return typeLO;
    }

    public void setTypeLO(String typeLO) {
        this.typeLO = typeLO;
    }

    public String getTypeZH() {
        return typeZH;
    }

    public void setTypeZH(String typeZH) {
        this.typeZH = typeZH;
    }

    public String getGearSystemTH() {
        return gearSystemTH;
    }

    public void setGearSystemTH(String gearSystemTH) {
        this.gearSystemTH = gearSystemTH;
    }

    public String getGearSystemEN() {
        return gearSystemEN;
    }

    public void setGearSystemEN(String gearSystemEN) {
        this.gearSystemEN = gearSystemEN;
    }

    public String getGearSystemLO() {
        return gearSystemLO;
    }

    public void setGearSystemLO(String gearSystemLO) {
        this.gearSystemLO = gearSystemLO;
    }

    public String getGearSystemZH() {
        return gearSystemZH;
    }

    public void setGearSystemZH(String gearSystemZH) {
        this.gearSystemZH = gearSystemZH;
    }

    public boolean isBasicInsurance() {
        return isBasicInsurance;
    }

    public void setBasicInsurance(boolean basicInsurance) {
        isBasicInsurance = basicInsurance;
    }

    public String getBasicInsuranceTH() {
        return basicInsuranceTH;
    }

    public void setBasicInsuranceTH(String basicInsuranceTH) {
        this.basicInsuranceTH = basicInsuranceTH;
    }

    public String getBasicInsuranceEN() {
        return basicInsuranceEN;
    }

    public void setBasicInsuranceEN(String basicInsuranceEN) {
        this.basicInsuranceEN = basicInsuranceEN;
    }

    public String getBasicInsuranceLO() {
        return basicInsuranceLO;
    }

    public void setBasicInsuranceLO(String basicInsuranceLO) {
        this.basicInsuranceLO = basicInsuranceLO;
    }

    public String getBasicInsuranceZH() {
        return basicInsuranceZH;
    }

    public void setBasicInsuranceZH(String basicInsuranceZH) {
        this.basicInsuranceZH = basicInsuranceZH;
    }

    protected CarModel(Parcel in) {
        rentalId = in.readInt();
        seats = in.readInt();
        pricePerDay = in.readInt();
        pickUpPrice = in.readInt();
        returnPrice = in.readInt();
        depositPrice = in.readInt();
        overcharge = in.readInt();
        typeTH = in.readString();
        typeEN = in.readString();
        typeLO = in.readString();
        typeZH = in.readString();
        gearSystemTH = in.readString();
        gearSystemEN = in.readString();
        gearSystemLO = in.readString();
        gearSystemZH = in.readString();
        isBasicInsurance = in.readByte() != 0;
        basicInsuranceTH = in.readString();
        basicInsuranceEN = in.readString();
        basicInsuranceLO = in.readString();
        basicInsuranceZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(rentalId);
        dest.writeInt(seats);
        dest.writeInt(pricePerDay);
        dest.writeInt(pickUpPrice);
        dest.writeInt(returnPrice);
        dest.writeInt(depositPrice);
        dest.writeInt(overcharge);
        dest.writeString(typeTH);
        dest.writeString(typeEN);
        dest.writeString(typeLO);
        dest.writeString(typeZH);
        dest.writeString(gearSystemTH);
        dest.writeString(gearSystemEN);
        dest.writeString(gearSystemLO);
        dest.writeString(gearSystemZH);
        dest.writeByte((byte) (isBasicInsurance ? 1 : 0));
        dest.writeString(basicInsuranceTH);
        dest.writeString(basicInsuranceEN);
        dest.writeString(basicInsuranceLO);
        dest.writeString(basicInsuranceZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CarModel> CREATOR = new Creator<CarModel>() {
        @Override
        public CarModel createFromParcel(Parcel in) {
            return new CarModel(in);
        }

        @Override
        public CarModel[] newArray(int size) {
            return new CarModel[size];
        }
    };

    public double getTotals(int start, int end){
        double totals = 0;
        int count = 0;
        count = (end-start)+1;
        if (count == 0){
            count+=1;
        }
        totals = pricePerDay * count;
        totals += returnPrice+pickUpPrice;
        return totals;
    }


}
