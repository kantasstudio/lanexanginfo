package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class CategoryModel implements Parcelable {

    private int categoryId;
    private String categoryTH;
    private String categoryEN;
    private String categoryLO;
    private String categoryZH;
    private SubCategoryModel subCategoryModel;
    private ArrayList<SubCategoryModel> subCategoryModelArrayList;
    private MenuItemModel menuItemModel;

    public CategoryModel() {

    }


    public CategoryModel(int categoryId) {
        this.categoryId = categoryId;
    }

    public CategoryModel(String categoryTH, String categoryEN, String categoryLO, String categoryZH) {
        this.categoryTH = categoryTH;
        this.categoryEN = categoryEN;
        this.categoryLO = categoryLO;
        this.categoryZH = categoryZH;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTH() {
        return categoryTH;
    }

    public void setCategoryTH(String categoryTH) {
        this.categoryTH = categoryTH;
    }

    public String getCategoryEN() {
        return categoryEN;
    }

    public void setCategoryEN(String categoryEN) {
        this.categoryEN = categoryEN;
    }

    public String getCategoryLO() {
        return categoryLO;
    }

    public void setCategoryLO(String categoryLO) {
        this.categoryLO = categoryLO;
    }

    public String getCategoryZH() {
        return categoryZH;
    }

    public void setCategoryZH(String categoryZH) {
        this.categoryZH = categoryZH;
    }

    public SubCategoryModel getSubCategoryModel() {
        return subCategoryModel;
    }

    public void setSubCategoryModel(SubCategoryModel subCategoryModel) {
        this.subCategoryModel = subCategoryModel;
    }

    public ArrayList<SubCategoryModel> getSubCategoryModelArrayList() {
        return subCategoryModelArrayList;
    }

    public void setSubCategoryModelArrayList(ArrayList<SubCategoryModel> subCategoryModelArrayList) {
        this.subCategoryModelArrayList = subCategoryModelArrayList;
    }

    public MenuItemModel getMenuItemModel() {
        return menuItemModel;
    }

    public void setMenuItemModel(MenuItemModel menuItemModel) {
        this.menuItemModel = menuItemModel;
    }

    protected CategoryModel(Parcel in) {
        categoryId = in.readInt();
        categoryTH = in.readString();
        categoryEN = in.readString();
        categoryLO = in.readString();
        categoryZH = in.readString();
        subCategoryModel = in.readParcelable(SubCategoryModel.class.getClassLoader());
        subCategoryModelArrayList = in.createTypedArrayList(SubCategoryModel.CREATOR);
        menuItemModel = in.readParcelable(MenuItemModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(categoryId);
        dest.writeString(categoryTH);
        dest.writeString(categoryEN);
        dest.writeString(categoryLO);
        dest.writeString(categoryZH);
        dest.writeParcelable(subCategoryModel, flags);
        dest.writeTypedList(subCategoryModelArrayList);
        dest.writeParcelable(menuItemModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CategoryModel> CREATOR = new Creator<CategoryModel>() {
        @Override
        public CategoryModel createFromParcel(Parcel in) {
            return new CategoryModel(in);
        }

        @Override
        public CategoryModel[] newArray(int size) {
            return new CategoryModel[size];
        }
    };
}
