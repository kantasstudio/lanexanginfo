package com.ipanda.lanexangtourism.Model;


import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;

import java.util.ArrayList;

public class CountryModel implements Parcelable {

    private int countryId;
    private String countryCode;
    private String countryInitials;
    private String countryTH;
    private String countryEN;
    private String countryLO;
    private String countryZH;
    private ArrayList<ProvincesModel> provincesModelArrayList;
    private Context context;
    public CountryModel() {

    }

    public CountryModel(Context context) {
        this.context = context;

    }


    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryInitials() {
        return countryInitials;
    }

    public void setCountryInitials(String countryInitials) {
        this.countryInitials = countryInitials;
    }

    public String getCountryTH() {
        return countryTH;
    }

    public void setCountryTH(String countryTH) {
        this.countryTH = countryTH;
    }

    public String getCountryEN() {
        return countryEN;
    }

    public void setCountryEN(String countryEN) {
        this.countryEN = countryEN;
    }

    public String getCountryLO() {
        return countryLO;
    }

    public void setCountryLO(String countryLO) {
        this.countryLO = countryLO;
    }

    public String getCountryZH() {
        return countryZH;
    }

    public void setCountryZH(String countryZH) {
        this.countryZH = countryZH;
    }

    public ArrayList<ProvincesModel> getProvincesModelArrayList() {
        return provincesModelArrayList;
    }

    public void setProvincesModelArrayList(ArrayList<ProvincesModel> provincesModelArrayList) {
        this.provincesModelArrayList = provincesModelArrayList;
    }


    @NonNull
    @Override
    public String toString() {
        ChangeLanguageLocale languageLocale = new ChangeLanguageLocale(context);
        String country = "";
        switch (languageLocale.getLanguage()){
            case "th":
                country = getCountryTH();
                break;
            case "en":
                country = getCountryEN();
                break;
            case "lo":
                country = getCountryLO();
                break;
            case "zh":
                country = getCountryZH();
                break;
        }
        return country;
    }

    protected CountryModel(Parcel in) {
        countryId = in.readInt();
        countryCode = in.readString();
        countryInitials = in.readString();
        countryTH = in.readString();
        countryEN = in.readString();
        countryLO = in.readString();
        countryZH = in.readString();
        provincesModelArrayList = in.createTypedArrayList(ProvincesModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(countryId);
        dest.writeString(countryCode);
        dest.writeString(countryInitials);
        dest.writeString(countryTH);
        dest.writeString(countryEN);
        dest.writeString(countryLO);
        dest.writeString(countryZH);
        dest.writeTypedList(provincesModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CountryModel> CREATOR = new Creator<CountryModel>() {
        @Override
        public CountryModel createFromParcel(Parcel in) {
            return new CountryModel(in);
        }

        @Override
        public CountryModel[] newArray(int size) {
            return new CountryModel[size];
        }
    };


}
