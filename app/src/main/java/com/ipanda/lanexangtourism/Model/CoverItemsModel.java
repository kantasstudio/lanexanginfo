package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class CoverItemsModel implements Parcelable {

    private int coverId;
    private String coverPaths;
    private String coverURL;
    private TypeCover typeCover;
    private TempImageCoverModel tempImageCoverModel;

    public CoverItemsModel() {

    }

    public CoverItemsModel(String coverPaths) {
        this.coverPaths = coverPaths;
    }

    public int getCoverId() {
        return coverId;
    }

    public void setCoverId(int coverId) {
        this.coverId = coverId;
    }

    public String getCoverPaths() {
        return coverPaths;
    }

    public void setCoverPaths(String coverPaths) {
        this.coverPaths = coverPaths;
    }

    public String getCoverURL() {
        return coverURL;
    }

    public void setCoverURL(String coverURL) {
        this.coverURL = coverURL;
    }

    public TypeCover getTypeCover() {
        return typeCover;
    }

    public void setTypeCover(TypeCover typeCover) {
        this.typeCover = typeCover;
    }

    public TempImageCoverModel getTempImageCoverModel() {
        return tempImageCoverModel;
    }

    public void setTempImageCoverModel(TempImageCoverModel tempImageCoverModel) {
        this.tempImageCoverModel = tempImageCoverModel;
    }

    protected CoverItemsModel(Parcel in) {
        coverId = in.readInt();
        coverPaths = in.readString();
        coverURL = in.readString();
        typeCover = in.readParcelable(TypeCover.class.getClassLoader());
        tempImageCoverModel = in.readParcelable(TempImageCoverModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(coverId);
        dest.writeString(coverPaths);
        dest.writeString(coverURL);
        dest.writeParcelable(typeCover, flags);
        dest.writeParcelable(tempImageCoverModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CoverItemsModel> CREATOR = new Creator<CoverItemsModel>() {
        @Override
        public CoverItemsModel createFromParcel(Parcel in) {
            return new CoverItemsModel(in);
        }

        @Override
        public CoverItemsModel[] newArray(int size) {
            return new CoverItemsModel[size];
        }
    };
}
