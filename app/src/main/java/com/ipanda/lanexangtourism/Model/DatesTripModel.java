package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DatesTripModel implements Parcelable {

    private int datesTripId;
    private String datesTripTopicThai;
    private String datesTripTopicEnglish;
    private String datesTripTopicLaos;
    private String datesTripTopicChinese;
    private String datesTripState;
    private ArrayList<TouristAttractionsModel> touristAttractionsModelArrayList;
    private ArrayList<PhotoTourist> photoTouristArrayList;

    public DatesTripModel() {

    }

    public int getDatesTripId() {
        return datesTripId;
    }

    public void setDatesTripId(int datesTripId) {
        this.datesTripId = datesTripId;
    }

    public String getDatesTripTopicThai() {
        return datesTripTopicThai;
    }

    public void setDatesTripTopicThai(String datesTripTopicThai) {
        this.datesTripTopicThai = datesTripTopicThai;
    }

    public String getDatesTripTopicEnglish() {
        return datesTripTopicEnglish;
    }

    public void setDatesTripTopicEnglish(String datesTripTopicEnglish) {
        this.datesTripTopicEnglish = datesTripTopicEnglish;
    }

    public String getDatesTripTopicLaos() {
        return datesTripTopicLaos;
    }

    public void setDatesTripTopicLaos(String datesTripTopicLaos) {
        this.datesTripTopicLaos = datesTripTopicLaos;
    }

    public String getDatesTripTopicChinese() {
        return datesTripTopicChinese;
    }

    public void setDatesTripTopicChinese(String datesTripTopicChinese) {
        this.datesTripTopicChinese = datesTripTopicChinese;
    }

    public String getDatesTripState() {
        return datesTripState;
    }

    public void setDatesTripState(String datesTripState) {
        this.datesTripState = datesTripState;
    }

    public ArrayList<TouristAttractionsModel> getTouristAttractionsModelArrayList() {
        return touristAttractionsModelArrayList;
    }

    public void setTouristAttractionsModelArrayList(ArrayList<TouristAttractionsModel> touristAttractionsModelArrayList) {
        this.touristAttractionsModelArrayList = touristAttractionsModelArrayList;
    }

    public ArrayList<PhotoTourist> getPhotoTouristArrayList() {
        return photoTouristArrayList;
    }

    public void setPhotoTouristArrayList(ArrayList<PhotoTourist> photoTouristArrayList) {
        this.photoTouristArrayList = photoTouristArrayList;
    }

    protected DatesTripModel(Parcel in) {
        datesTripId = in.readInt();
        datesTripTopicThai = in.readString();
        datesTripTopicEnglish = in.readString();
        datesTripTopicLaos = in.readString();
        datesTripTopicChinese = in.readString();
        datesTripState = in.readString();
        touristAttractionsModelArrayList = in.createTypedArrayList(TouristAttractionsModel.CREATOR);
        photoTouristArrayList = in.createTypedArrayList(PhotoTourist.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(datesTripId);
        dest.writeString(datesTripTopicThai);
        dest.writeString(datesTripTopicEnglish);
        dest.writeString(datesTripTopicLaos);
        dest.writeString(datesTripTopicChinese);
        dest.writeString(datesTripState);
        dest.writeTypedList(touristAttractionsModelArrayList);
        dest.writeTypedList(photoTouristArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DatesTripModel> CREATOR = new Creator<DatesTripModel>() {
        @Override
        public DatesTripModel createFromParcel(Parcel in) {
            return new DatesTripModel(in);
        }

        @Override
        public DatesTripModel[] newArray(int size) {
            return new DatesTripModel[size];
        }
    };
}
