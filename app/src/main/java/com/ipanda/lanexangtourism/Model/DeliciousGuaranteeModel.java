package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class DeliciousGuaranteeModel implements Parcelable {

    private int guaranteeId;
    private String guaranteeTH;
    private String guaranteeEN;
    private String guaranteeLO;
    private String guaranteeZH;
    private String guaranteePaths;
    private boolean isFilter;

    public DeliciousGuaranteeModel() {

    }

    public int getGuaranteeId() {
        return guaranteeId;
    }

    public void setGuaranteeId(int guaranteeId) {
        this.guaranteeId = guaranteeId;
    }

    public String getGuaranteeTH() {
        return guaranteeTH;
    }

    public void setGuaranteeTH(String guaranteeTH) {
        this.guaranteeTH = guaranteeTH;
    }

    public String getGuaranteeEN() {
        return guaranteeEN;
    }

    public void setGuaranteeEN(String guaranteeEN) {
        this.guaranteeEN = guaranteeEN;
    }

    public String getGuaranteeLO() {
        return guaranteeLO;
    }

    public void setGuaranteeLO(String guaranteeLO) {
        this.guaranteeLO = guaranteeLO;
    }

    public String getGuaranteeZH() {
        return guaranteeZH;
    }

    public void setGuaranteeZH(String guaranteeZH) {
        this.guaranteeZH = guaranteeZH;
    }

    public String getGuaranteePaths() {
        return guaranteePaths;
    }

    public void setGuaranteePaths(String guaranteePaths) {
        this.guaranteePaths = guaranteePaths;
    }

    public boolean isFilter() {
        return isFilter;
    }

    public void setFilter(boolean filter) {
        isFilter = filter;
    }

    protected DeliciousGuaranteeModel(Parcel in) {
        guaranteeId = in.readInt();
        guaranteeTH = in.readString();
        guaranteeEN = in.readString();
        guaranteeLO = in.readString();
        guaranteeZH = in.readString();
        guaranteePaths = in.readString();
        isFilter = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(guaranteeId);
        dest.writeString(guaranteeTH);
        dest.writeString(guaranteeEN);
        dest.writeString(guaranteeLO);
        dest.writeString(guaranteeZH);
        dest.writeString(guaranteePaths);
        dest.writeByte((byte) (isFilter ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DeliciousGuaranteeModel> CREATOR = new Creator<DeliciousGuaranteeModel>() {
        @Override
        public DeliciousGuaranteeModel createFromParcel(Parcel in) {
            return new DeliciousGuaranteeModel(in);
        }

        @Override
        public DeliciousGuaranteeModel[] newArray(int size) {
            return new DeliciousGuaranteeModel[size];
        }
    };
}
