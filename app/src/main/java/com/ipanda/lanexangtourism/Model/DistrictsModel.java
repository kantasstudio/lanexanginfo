package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DistrictsModel implements Parcelable {

    private int districtsId;
    private int districtsCode;
    private String districtsTH;
    private String districtsEN;
    private String districtsLO;
    private String districtsZH;
    private SubDistrictsModel subDistrictsModel;
    private ArrayList<SubDistrictsModel> subDistrictsModelArrayList;


    public DistrictsModel() {

    }

    public DistrictsModel(int districtsId) {
        this.districtsId = districtsId;
    }

    public DistrictsModel(String districtsTH, String districtsEN, String districtsLO, String districtsZH) {
        this.districtsTH = districtsTH;
        this.districtsEN = districtsEN;
        this.districtsLO = districtsLO;
        this.districtsZH = districtsZH;
    }

    public int getDistrictsId() {
        return districtsId;
    }

    public void setDistrictsId(int districtsId) {
        this.districtsId = districtsId;
    }

    public int getDistrictsCode() {
        return districtsCode;
    }

    public void setDistrictsCode(int districtsCode) {
        this.districtsCode = districtsCode;
    }

    public String getDistrictsTH() {
        return districtsTH;
    }

    public void setDistrictsTH(String districtsTH) {
        this.districtsTH = districtsTH;
    }

    public String getDistrictsEN() {
        return districtsEN;
    }

    public void setDistrictsEN(String districtsEN) {
        this.districtsEN = districtsEN;
    }

    public String getDistrictsLO() {
        return districtsLO;
    }

    public void setDistrictsLO(String districtsLO) {
        this.districtsLO = districtsLO;
    }

    public String getDistrictsZH() {
        return districtsZH;
    }

    public void setDistrictsZH(String districtsZH) {
        this.districtsZH = districtsZH;
    }

    public SubDistrictsModel getSubDistrictsModel() {
        return subDistrictsModel;
    }

    public void setSubDistrictsModel(SubDistrictsModel subDistrictsModel) {
        this.subDistrictsModel = subDistrictsModel;
    }

    public ArrayList<SubDistrictsModel> getSubDistrictsModelArrayList() {
        return subDistrictsModelArrayList;
    }

    public void setSubDistrictsModelArrayList(ArrayList<SubDistrictsModel> subDistrictsModelArrayList) {
        this.subDistrictsModelArrayList = subDistrictsModelArrayList;
    }

    protected DistrictsModel(Parcel in) {
        districtsId = in.readInt();
        districtsCode = in.readInt();
        districtsTH = in.readString();
        districtsEN = in.readString();
        districtsLO = in.readString();
        districtsZH = in.readString();
        subDistrictsModel = in.readParcelable(SubDistrictsModel.class.getClassLoader());
        subDistrictsModelArrayList = in.createTypedArrayList(SubDistrictsModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(districtsId);
        dest.writeInt(districtsCode);
        dest.writeString(districtsTH);
        dest.writeString(districtsEN);
        dest.writeString(districtsLO);
        dest.writeString(districtsZH);
        dest.writeParcelable(subDistrictsModel, flags);
        dest.writeTypedList(subDistrictsModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DistrictsModel> CREATOR = new Creator<DistrictsModel>() {
        @Override
        public DistrictsModel createFromParcel(Parcel in) {
            return new DistrictsModel(in);
        }

        @Override
        public DistrictsModel[] newArray(int size) {
            return new DistrictsModel[size];
        }
    };
}
