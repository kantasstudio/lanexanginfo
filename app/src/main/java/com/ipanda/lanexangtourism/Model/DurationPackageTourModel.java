package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class DurationPackageTourModel implements Parcelable {

    private String textThai;
    private String textEnglish;
    private String textLaos;
    private String textChinese;
    private String textValue;
    private boolean isFilter;

    public DurationPackageTourModel() {

    }

    public String getTextThai() {
        return textThai;
    }

    public void setTextThai(String textThai) {
        this.textThai = textThai;
    }

    public String getTextEnglish() {
        return textEnglish;
    }

    public void setTextEnglish(String textEnglish) {
        this.textEnglish = textEnglish;
    }

    public String getTextLaos() {
        return textLaos;
    }

    public void setTextLaos(String textLaos) {
        this.textLaos = textLaos;
    }

    public String getTextChinese() {
        return textChinese;
    }

    public void setTextChinese(String textChinese) {
        this.textChinese = textChinese;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public boolean isFilter() {
        return isFilter;
    }

    public void setFilter(boolean filter) {
        isFilter = filter;
    }

    protected DurationPackageTourModel(Parcel in) {
        textThai = in.readString();
        textEnglish = in.readString();
        textLaos = in.readString();
        textChinese = in.readString();
        textValue = in.readString();
        isFilter = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(textThai);
        dest.writeString(textEnglish);
        dest.writeString(textLaos);
        dest.writeString(textChinese);
        dest.writeString(textValue);
        dest.writeByte((byte) (isFilter ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DurationPackageTourModel> CREATOR = new Creator<DurationPackageTourModel>() {
        @Override
        public DurationPackageTourModel createFromParcel(Parcel in) {
            return new DurationPackageTourModel(in);
        }

        @Override
        public DurationPackageTourModel[] newArray(int size) {
            return new DurationPackageTourModel[size];
        }
    };
}
