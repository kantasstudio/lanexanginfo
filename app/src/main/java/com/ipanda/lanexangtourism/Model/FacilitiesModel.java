package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class FacilitiesModel implements Parcelable {

    private int facilitiesId;
    private String facilitiesTH;
    private String facilitiesEN;
    private String facilitiesLO;
    private String facilitiesZH;
    private boolean isFilter;

    public FacilitiesModel() {

    }

    public int getFacilitiesId() {
        return facilitiesId;
    }

    public void setFacilitiesId(int facilitiesId) {
        this.facilitiesId = facilitiesId;
    }

    public String getFacilitiesTH() {
        return facilitiesTH;
    }

    public void setFacilitiesTH(String facilitiesTH) {
        this.facilitiesTH = facilitiesTH;
    }

    public String getFacilitiesEN() {
        return facilitiesEN;
    }

    public void setFacilitiesEN(String facilitiesEN) {
        this.facilitiesEN = facilitiesEN;
    }

    public String getFacilitiesLO() {
        return facilitiesLO;
    }

    public void setFacilitiesLO(String facilitiesLO) {
        this.facilitiesLO = facilitiesLO;
    }

    public String getFacilitiesZH() {
        return facilitiesZH;
    }

    public void setFacilitiesZH(String facilitiesZH) {
        this.facilitiesZH = facilitiesZH;
    }

    public boolean isFilter() {
        return isFilter;
    }

    public void setFilter(boolean filter) {
        isFilter = filter;
    }

    protected FacilitiesModel(Parcel in) {
        facilitiesId = in.readInt();
        facilitiesTH = in.readString();
        facilitiesEN = in.readString();
        facilitiesLO = in.readString();
        facilitiesZH = in.readString();
        isFilter = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(facilitiesId);
        dest.writeString(facilitiesTH);
        dest.writeString(facilitiesEN);
        dest.writeString(facilitiesLO);
        dest.writeString(facilitiesZH);
        dest.writeByte((byte) (isFilter ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FacilitiesModel> CREATOR = new Creator<FacilitiesModel>() {
        @Override
        public FacilitiesModel createFromParcel(Parcel in) {
            return new FacilitiesModel(in);
        }

        @Override
        public FacilitiesModel[] newArray(int size) {
            return new FacilitiesModel[size];
        }
    };
}
