package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class FollowersModel implements Parcelable {

    private int followersId;
    private String timeStamp;
    private ArrayList<UserModel> followersUserId;
    private ArrayList<UserModel> userId;

    public FollowersModel() {

    }

    public int getFollowersId() {
        return followersId;
    }

    public void setFollowersId(int followersId) {
        this.followersId = followersId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public ArrayList<UserModel> getFollowersUserId() {
        return followersUserId;
    }

    public void setFollowersUserId(ArrayList<UserModel> followersUserId) {
        this.followersUserId = followersUserId;
    }

    public ArrayList<UserModel> getUserId() {
        return userId;
    }

    public void setUserId(ArrayList<UserModel> userId) {
        this.userId = userId;
    }

    protected FollowersModel(Parcel in) {
        followersId = in.readInt();
        timeStamp = in.readString();
        followersUserId = in.createTypedArrayList(UserModel.CREATOR);
        userId = in.createTypedArrayList(UserModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(followersId);
        dest.writeString(timeStamp);
        dest.writeTypedList(followersUserId);
        dest.writeTypedList(userId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FollowersModel> CREATOR = new Creator<FollowersModel>() {
        @Override
        public FollowersModel createFromParcel(Parcel in) {
            return new FollowersModel(in);
        }

        @Override
        public FollowersModel[] newArray(int size) {
            return new FollowersModel[size];
        }
    };
}
