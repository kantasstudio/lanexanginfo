package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class FoodModel implements Parcelable {

    private int foodId;
    private int foodDateNo;
    private boolean foodBreakFast;
    private boolean foodLunch;
    private boolean foodDinner;

    public FoodModel() {

    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public int getFoodDateNo() {
        return foodDateNo;
    }

    public void setFoodDateNo(int foodDateNo) {
        this.foodDateNo = foodDateNo;
    }

    public boolean isFoodBreakFast() {
        return foodBreakFast;
    }

    public void setFoodBreakFast(boolean foodBreakFast) {
        this.foodBreakFast = foodBreakFast;
    }

    public boolean isFoodLunch() {
        return foodLunch;
    }

    public void setFoodLunch(boolean foodLunch) {
        this.foodLunch = foodLunch;
    }

    public boolean isFoodDinner() {
        return foodDinner;
    }

    public void setFoodDinner(boolean foodDinner) {
        this.foodDinner = foodDinner;
    }

    protected FoodModel(Parcel in) {
        foodId = in.readInt();
        foodDateNo = in.readInt();
        foodBreakFast = in.readByte() != 0;
        foodLunch = in.readByte() != 0;
        foodDinner = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(foodId);
        dest.writeInt(foodDateNo);
        dest.writeByte((byte) (foodBreakFast ? 1 : 0));
        dest.writeByte((byte) (foodLunch ? 1 : 0));
        dest.writeByte((byte) (foodDinner ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FoodModel> CREATOR = new Creator<FoodModel>() {
        @Override
        public FoodModel createFromParcel(Parcel in) {
            return new FoodModel(in);
        }

        @Override
        public FoodModel[] newArray(int size) {
            return new FoodModel[size];
        }
    };
}
