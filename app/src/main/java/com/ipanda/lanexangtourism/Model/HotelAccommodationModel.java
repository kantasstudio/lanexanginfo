package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class HotelAccommodationModel implements Parcelable {

    private int accommodationId;
    private String accommodationDate;
    private String accommodationTH;
    private String accommodationEN;
    private String accommodationLO;
    private String accommodationZH;


    public HotelAccommodationModel() {

    }

    public int getAccommodationId() {
        return accommodationId;
    }

    public void setAccommodationId(int accommodationId) {
        this.accommodationId = accommodationId;
    }

    public String getAccommodationDate() {
        return accommodationDate;
    }

    public void setAccommodationDate(String accommodationDate) {
        this.accommodationDate = accommodationDate;
    }

    public String getAccommodationTH() {
        return accommodationTH;
    }

    public void setAccommodationTH(String accommodationTH) {
        this.accommodationTH = accommodationTH;
    }

    public String getAccommodationEN() {
        return accommodationEN;
    }

    public void setAccommodationEN(String accommodationEN) {
        this.accommodationEN = accommodationEN;
    }

    public String getAccommodationLO() {
        return accommodationLO;
    }

    public void setAccommodationLO(String accommodationLO) {
        this.accommodationLO = accommodationLO;
    }

    public String getAccommodationZH() {
        return accommodationZH;
    }

    public void setAccommodationZH(String accommodationZH) {
        this.accommodationZH = accommodationZH;
    }

    protected HotelAccommodationModel(Parcel in) {
        accommodationId = in.readInt();
        accommodationDate = in.readString();
        accommodationTH = in.readString();
        accommodationEN = in.readString();
        accommodationLO = in.readString();
        accommodationZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(accommodationId);
        dest.writeString(accommodationDate);
        dest.writeString(accommodationTH);
        dest.writeString(accommodationEN);
        dest.writeString(accommodationLO);
        dest.writeString(accommodationZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HotelAccommodationModel> CREATOR = new Creator<HotelAccommodationModel>() {
        @Override
        public HotelAccommodationModel createFromParcel(Parcel in) {
            return new HotelAccommodationModel(in);
        }

        @Override
        public HotelAccommodationModel[] newArray(int size) {
            return new HotelAccommodationModel[size];
        }
    };
}
