package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ItemsDetailModel implements Parcelable {

    private int detailId;
    private String detailTH;
    private String detailEN;
    private String detailLO;
    private String detailZH;
    private ItemsTopicDetailModel itemsTopicDetailModel;
    private ArrayList<ItemsPhotoDetailModel> itemsPhotoDetailModels;

    public ItemsDetailModel() {

    }

    public int getDetailId() {
        return detailId;
    }

    public void setDetailId(int detailId) {
        this.detailId = detailId;
    }

    public String getDetailTH() {
        return detailTH;
    }

    public void setDetailTH(String detailTH) {
        this.detailTH = detailTH;
    }

    public String getDetailEN() {
        return detailEN;
    }

    public void setDetailEN(String detailEN) {
        this.detailEN = detailEN;
    }

    public String getDetailLO() {
        return detailLO;
    }

    public void setDetailLO(String detailLO) {
        this.detailLO = detailLO;
    }

    public String getDetailZH() {
        return detailZH;
    }

    public void setDetailZH(String detailZH) {
        this.detailZH = detailZH;
    }

    public ItemsTopicDetailModel getItemsTopicDetailModel() {
        return itemsTopicDetailModel;
    }

    public void setItemsTopicDetailModel(ItemsTopicDetailModel itemsTopicDetailModel) {
        this.itemsTopicDetailModel = itemsTopicDetailModel;
    }

    public ArrayList<ItemsPhotoDetailModel> getItemsPhotoDetailModels() {
        return itemsPhotoDetailModels;
    }

    public void setItemsPhotoDetailModels(ArrayList<ItemsPhotoDetailModel> itemsPhotoDetailModels) {
        this.itemsPhotoDetailModels = itemsPhotoDetailModels;
    }

    protected ItemsDetailModel(Parcel in) {
        detailId = in.readInt();
        detailTH = in.readString();
        detailEN = in.readString();
        detailLO = in.readString();
        detailZH = in.readString();
        itemsTopicDetailModel = in.readParcelable(ItemsTopicDetailModel.class.getClassLoader());
        itemsPhotoDetailModels = in.createTypedArrayList(ItemsPhotoDetailModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(detailId);
        dest.writeString(detailTH);
        dest.writeString(detailEN);
        dest.writeString(detailLO);
        dest.writeString(detailZH);
        dest.writeParcelable(itemsTopicDetailModel, flags);
        dest.writeTypedList(itemsPhotoDetailModels);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemsDetailModel> CREATOR = new Creator<ItemsDetailModel>() {
        @Override
        public ItemsDetailModel createFromParcel(Parcel in) {
            return new ItemsDetailModel(in);
        }

        @Override
        public ItemsDetailModel[] newArray(int size) {
            return new ItemsDetailModel[size];
        }
    };
}
