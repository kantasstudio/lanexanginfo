package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ItemsModel implements Parcelable {

    private int itemsId;
    private String topicTH;
    private String topicEN;
    private String topicLO;
    private String topicZH;
    private String dayOpenTH;
    private String dayOpenEN;
    private String dayOpenLO;
    private String dayOpenZH;
    private String dayCloseTH;
    private String dayCloseEN;
    private String dayCloseLO;
    private String dayCloseZH;
    private String timeOpen;
    private String timeClose;
    private String latitude;
    private String longitude;
    private String contactTH;
    private String contactEN;
    private String contactLO;
    private String contactZH;
    private String phone;
    private String email;
    private String line;
    private String facebookPage;
    private String arURL;
    private Double price;
    private int isActive;
    private int isPublish;
    private String dateDurationTH;
    private String dateDurationEN;
    private String dateDurationLO;
    private String dateDurationZH;
    private String highlightsTH;
    private String highlightsEN;
    private String highlightsLO;
    private String highlightsZH;
    private String locationInformationTH;
    private String locationInformationEN;
    private String locationInformationLO;
    private String locationInformationZH;
    private String typeOfCarsTH;
    private String typeOfCarsEN;
    private String typeOfCarsLO;
    private String typeOfCarsZH;
    private int countView;
    private String createDatetime;
    private String updateDatetime;
    private boolean isSelectedItemToPlaces = false;
    private int distance;
    private int distance_metr;
    private int priceGuestAdult;
    private int priceGuestChild;
    private boolean isOpenOrClosed;
    private BookMarksModel bookMarksModel;
    private MenuItemModel menuItemModel;
    private CategoryModel categoryModel;
    private SubMenuItemModel subMenuItemModel;
    private CoverItemsModel coverItemsModel;
    private CarModel carModel;
    private RatingStarModel ratingStarModel;
    private ProvincesModel provincesModel;
    private ItemsDetailModel itemsDetailModel;
    private RoomModel roomModel;
    private BusinessModel businessModel;
    private PeriodTimeModel periodTimeModel;
    private PeriodDayModel periodDayModel;
    private ArrayList<ProvincesModel> provincesModelArrayList;
    private ArrayList<PeriodTimeModel> periodTimeModelArrayList;
    private ArrayList<FoodModel> foodModelArrayList;
    private ArrayList<FacilitiesModel> facilitiesModelArrayList;
    private ArrayList<ItemsDetailModel> itemsDetailModelArrayList;
    private ArrayList<TravelDetailsModel> travelDetailsModelArrayList;
    private ArrayList<TravelPeriodModel> travelPeriodModelArrayList;
    private ArrayList<ProductModel> productModelArrayList;
    private ArrayList<RoomModel> roomModelArrayList;
    private ArrayList<SubCategoryModel> subCategoryModelArrayList;
    private ArrayList<DeliciousGuaranteeModel> deliciousGuaranteeModelArrayList;
    private ArrayList<HotelAccommodationModel> hotelAccommodationModelArrayList;
    private ArrayList<BookingConditionModel> bookingConditionModelArrayList;
    private ArrayList<ReviewModel> reviewModelArrayList;
    private ArrayList<CoverItemsModel> coverItemsModelArrayList;


    public ItemsModel() {
    }

    public int getItemsId() {
        return itemsId;
    }

    public void setItemsId(int itemsId) {
        this.itemsId = itemsId;
    }

    public String getTopicTH() {
        return topicTH;
    }

    public void setTopicTH(String topicTH) {
        this.topicTH = topicTH;
    }

    public String getTopicEN() {
        return topicEN;
    }

    public void setTopicEN(String topicEN) {
        this.topicEN = topicEN;
    }

    public String getTopicLO() {
        return topicLO;
    }

    public void setTopicLO(String topicLO) {
        this.topicLO = topicLO;
    }

    public String getTopicZH() {
        return topicZH;
    }

    public void setTopicZH(String topicZH) {
        this.topicZH = topicZH;
    }

    public String getDayOpenTH() {
        return dayOpenTH;
    }

    public void setDayOpenTH(String dayOpenTH) {
        this.dayOpenTH = dayOpenTH;
    }

    public String getDayOpenEN() {
        return dayOpenEN;
    }

    public void setDayOpenEN(String dayOpenEN) {
        this.dayOpenEN = dayOpenEN;
    }

    public String getDayOpenLO() {
        return dayOpenLO;
    }

    public void setDayOpenLO(String dayOpenLO) {
        this.dayOpenLO = dayOpenLO;
    }

    public String getDayOpenZH() {
        return dayOpenZH;
    }

    public void setDayOpenZH(String dayOpenZH) {
        this.dayOpenZH = dayOpenZH;
    }

    public String getDayCloseTH() {
        return dayCloseTH;
    }

    public void setDayCloseTH(String dayCloseTH) {
        this.dayCloseTH = dayCloseTH;
    }

    public String getDayCloseEN() {
        return dayCloseEN;
    }

    public void setDayCloseEN(String dayCloseEN) {
        this.dayCloseEN = dayCloseEN;
    }

    public String getDayCloseLO() {
        return dayCloseLO;
    }

    public void setDayCloseLO(String dayCloseLO) {
        this.dayCloseLO = dayCloseLO;
    }

    public String getDayCloseZH() {
        return dayCloseZH;
    }

    public void setDayCloseZH(String dayCloseZH) {
        this.dayCloseZH = dayCloseZH;
    }

    public String getTimeOpen() {
        return timeOpen;
    }

    public void setTimeOpen(String timeOpen) {
        this.timeOpen = timeOpen;
    }

    public String getTimeClose() {
        return timeClose;
    }

    public void setTimeClose(String timeClose) {
        this.timeClose = timeClose;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getContactTH() {
        return contactTH;
    }

    public void setContactTH(String contactTH) {
        this.contactTH = contactTH;
    }

    public String getContactEN() {
        return contactEN;
    }

    public void setContactEN(String contactEN) {
        this.contactEN = contactEN;
    }

    public String getContactLO() {
        return contactLO;
    }

    public void setContactLO(String contactLO) {
        this.contactLO = contactLO;
    }

    public String getContactZH() {
        return contactZH;
    }

    public void setContactZH(String contactZH) {
        this.contactZH = contactZH;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getFacebookPage() {
        return facebookPage;
    }

    public void setFacebookPage(String facebookPage) {
        this.facebookPage = facebookPage;
    }

    public String getArURL() {
        return arURL;
    }

    public void setArURL(String arURL) {
        this.arURL = arURL;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(int isPublish) {
        this.isPublish = isPublish;
    }

    public String getDateDurationTH() {
        return dateDurationTH;
    }

    public void setDateDurationTH(String dateDurationTH) {
        this.dateDurationTH = dateDurationTH;
    }

    public String getDateDurationEN() {
        return dateDurationEN;
    }

    public void setDateDurationEN(String dateDurationEN) {
        this.dateDurationEN = dateDurationEN;
    }

    public String getDateDurationLO() {
        return dateDurationLO;
    }

    public void setDateDurationLO(String dateDurationLO) {
        this.dateDurationLO = dateDurationLO;
    }

    public String getDateDurationZH() {
        return dateDurationZH;
    }

    public void setDateDurationZH(String dateDurationZH) {
        this.dateDurationZH = dateDurationZH;
    }

    public String getHighlightsTH() {
        return highlightsTH;
    }

    public void setHighlightsTH(String highlightsTH) {
        this.highlightsTH = highlightsTH;
    }

    public String getHighlightsEN() {
        return highlightsEN;
    }

    public void setHighlightsEN(String highlightsEN) {
        this.highlightsEN = highlightsEN;
    }

    public String getHighlightsLO() {
        return highlightsLO;
    }

    public void setHighlightsLO(String highlightsLO) {
        this.highlightsLO = highlightsLO;
    }

    public String getHighlightsZH() {
        return highlightsZH;
    }

    public void setHighlightsZH(String highlightsZH) {
        this.highlightsZH = highlightsZH;
    }

    public String getLocationInformationTH() {
        return locationInformationTH;
    }

    public void setLocationInformationTH(String locationInformationTH) {
        this.locationInformationTH = locationInformationTH;
    }

    public String getLocationInformationEN() {
        return locationInformationEN;
    }

    public void setLocationInformationEN(String locationInformationEN) {
        this.locationInformationEN = locationInformationEN;
    }

    public String getLocationInformationLO() {
        return locationInformationLO;
    }

    public void setLocationInformationLO(String locationInformationLO) {
        this.locationInformationLO = locationInformationLO;
    }

    public String getLocationInformationZH() {
        return locationInformationZH;
    }

    public void setLocationInformationZH(String locationInformationZH) {
        this.locationInformationZH = locationInformationZH;
    }

    public String getTypeOfCarsTH() {
        return typeOfCarsTH;
    }

    public void setTypeOfCarsTH(String typeOfCarsTH) {
        this.typeOfCarsTH = typeOfCarsTH;
    }

    public String getTypeOfCarsEN() {
        return typeOfCarsEN;
    }

    public void setTypeOfCarsEN(String typeOfCarsEN) {
        this.typeOfCarsEN = typeOfCarsEN;
    }

    public String getTypeOfCarsLO() {
        return typeOfCarsLO;
    }

    public void setTypeOfCarsLO(String typeOfCarsLO) {
        this.typeOfCarsLO = typeOfCarsLO;
    }

    public String getTypeOfCarsZH() {
        return typeOfCarsZH;
    }

    public void setTypeOfCarsZH(String typeOfCarsZH) {
        this.typeOfCarsZH = typeOfCarsZH;
    }

    public int getCountView() {
        return countView;
    }

    public void setCountView(int countView) {
        this.countView = countView;
    }

    public String getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(String updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public boolean isSelectedItemToPlaces() {
        return isSelectedItemToPlaces;
    }

    public void setSelectedItemToPlaces(boolean selectedItemToPlaces) {
        isSelectedItemToPlaces = selectedItemToPlaces;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDistance_metr() {
        return distance_metr;
    }

    public void setDistance_metr(int distance_metr) {
        this.distance_metr = distance_metr;
    }

    public int getPriceGuestAdult() {
        return priceGuestAdult;
    }

    public void setPriceGuestAdult(int priceGuestAdult) {
        this.priceGuestAdult = priceGuestAdult;
    }

    public int getPriceGuestChild() {
        return priceGuestChild;
    }

    public void setPriceGuestChild(int priceGuestChild) {
        this.priceGuestChild = priceGuestChild;
    }

    public boolean isOpenOrClosed() {
        return isOpenOrClosed;
    }

    public void setOpenOrClosed(boolean openOrClosed) {
        isOpenOrClosed = openOrClosed;
    }

    public BookMarksModel getBookMarksModel() {
        return bookMarksModel;
    }

    public void setBookMarksModel(BookMarksModel bookMarksModel) {
        this.bookMarksModel = bookMarksModel;
    }

    public MenuItemModel getMenuItemModel() {
        return menuItemModel;
    }

    public void setMenuItemModel(MenuItemModel menuItemModel) {
        this.menuItemModel = menuItemModel;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public SubMenuItemModel getSubMenuItemModel() {
        return subMenuItemModel;
    }

    public void setSubMenuItemModel(SubMenuItemModel subMenuItemModel) {
        this.subMenuItemModel = subMenuItemModel;
    }

    public CoverItemsModel getCoverItemsModel() {
        return coverItemsModel;
    }

    public void setCoverItemsModel(CoverItemsModel coverItemsModel) {
        this.coverItemsModel = coverItemsModel;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public RatingStarModel getRatingStarModel() {
        return ratingStarModel;
    }

    public void setRatingStarModel(RatingStarModel ratingStarModel) {
        this.ratingStarModel = ratingStarModel;
    }

    public ProvincesModel getProvincesModel() {
        return provincesModel;
    }

    public void setProvincesModel(ProvincesModel provincesModel) {
        this.provincesModel = provincesModel;
    }

    public ItemsDetailModel getItemsDetailModel() {
        return itemsDetailModel;
    }

    public void setItemsDetailModel(ItemsDetailModel itemsDetailModel) {
        this.itemsDetailModel = itemsDetailModel;
    }

    public RoomModel getRoomModel() {
        return roomModel;
    }

    public void setRoomModel(RoomModel roomModel) {
        this.roomModel = roomModel;
    }

    public BusinessModel getBusinessModel() {
        return businessModel;
    }

    public void setBusinessModel(BusinessModel businessModel) {
        this.businessModel = businessModel;
    }

    public PeriodTimeModel getPeriodTimeModel() {
        return periodTimeModel;
    }

    public void setPeriodTimeModel(PeriodTimeModel periodTimeModel) {
        this.periodTimeModel = periodTimeModel;
    }

    public PeriodDayModel getPeriodDayModel() {
        return periodDayModel;
    }

    public void setPeriodDayModel(PeriodDayModel periodDayModel) {
        this.periodDayModel = periodDayModel;
    }

    public ArrayList<ProvincesModel> getProvincesModelArrayList() {
        return provincesModelArrayList;
    }

    public void setProvincesModelArrayList(ArrayList<ProvincesModel> provincesModelArrayList) {
        this.provincesModelArrayList = provincesModelArrayList;
    }

    public ArrayList<PeriodTimeModel> getPeriodTimeModelArrayList() {
        return periodTimeModelArrayList;
    }

    public void setPeriodTimeModelArrayList(ArrayList<PeriodTimeModel> periodTimeModelArrayList) {
        this.periodTimeModelArrayList = periodTimeModelArrayList;
    }

    public ArrayList<FoodModel> getFoodModelArrayList() {
        return foodModelArrayList;
    }

    public void setFoodModelArrayList(ArrayList<FoodModel> foodModelArrayList) {
        this.foodModelArrayList = foodModelArrayList;
    }

    public ArrayList<FacilitiesModel> getFacilitiesModelArrayList() {
        return facilitiesModelArrayList;
    }

    public void setFacilitiesModelArrayList(ArrayList<FacilitiesModel> facilitiesModelArrayList) {
        this.facilitiesModelArrayList = facilitiesModelArrayList;
    }

    public ArrayList<ItemsDetailModel> getItemsDetailModelArrayList() {
        return itemsDetailModelArrayList;
    }

    public void setItemsDetailModelArrayList(ArrayList<ItemsDetailModel> itemsDetailModelArrayList) {
        this.itemsDetailModelArrayList = itemsDetailModelArrayList;
    }

    public ArrayList<TravelDetailsModel> getTravelDetailsModelArrayList() {
        return travelDetailsModelArrayList;
    }

    public void setTravelDetailsModelArrayList(ArrayList<TravelDetailsModel> travelDetailsModelArrayList) {
        this.travelDetailsModelArrayList = travelDetailsModelArrayList;
    }

    public ArrayList<TravelPeriodModel> getTravelPeriodModelArrayList() {
        return travelPeriodModelArrayList;
    }

    public void setTravelPeriodModelArrayList(ArrayList<TravelPeriodModel> travelPeriodModelArrayList) {
        this.travelPeriodModelArrayList = travelPeriodModelArrayList;
    }

    public ArrayList<ProductModel> getProductModelArrayList() {
        return productModelArrayList;
    }

    public void setProductModelArrayList(ArrayList<ProductModel> productModelArrayList) {
        this.productModelArrayList = productModelArrayList;
    }

    public ArrayList<RoomModel> getRoomModelArrayList() {
        return roomModelArrayList;
    }

    public void setRoomModelArrayList(ArrayList<RoomModel> roomModelArrayList) {
        this.roomModelArrayList = roomModelArrayList;
    }

    public ArrayList<SubCategoryModel> getSubCategoryModelArrayList() {
        return subCategoryModelArrayList;
    }

    public void setSubCategoryModelArrayList(ArrayList<SubCategoryModel> subCategoryModelArrayList) {
        this.subCategoryModelArrayList = subCategoryModelArrayList;
    }

    public ArrayList<DeliciousGuaranteeModel> getDeliciousGuaranteeModelArrayList() {
        return deliciousGuaranteeModelArrayList;
    }

    public void setDeliciousGuaranteeModelArrayList(ArrayList<DeliciousGuaranteeModel> deliciousGuaranteeModelArrayList) {
        this.deliciousGuaranteeModelArrayList = deliciousGuaranteeModelArrayList;
    }

    public ArrayList<HotelAccommodationModel> getHotelAccommodationModelArrayList() {
        return hotelAccommodationModelArrayList;
    }

    public void setHotelAccommodationModelArrayList(ArrayList<HotelAccommodationModel> hotelAccommodationModelArrayList) {
        this.hotelAccommodationModelArrayList = hotelAccommodationModelArrayList;
    }

    public ArrayList<BookingConditionModel> getBookingConditionModelArrayList() {
        return bookingConditionModelArrayList;
    }

    public void setBookingConditionModelArrayList(ArrayList<BookingConditionModel> bookingConditionModelArrayList) {
        this.bookingConditionModelArrayList = bookingConditionModelArrayList;
    }

    public ArrayList<ReviewModel> getReviewModelArrayList() {
        return reviewModelArrayList;
    }

    public void setReviewModelArrayList(ArrayList<ReviewModel> reviewModelArrayList) {
        this.reviewModelArrayList = reviewModelArrayList;
    }

    public ArrayList<CoverItemsModel> getCoverItemsModelArrayList() {
        return coverItemsModelArrayList;
    }

    public void setCoverItemsModelArrayList(ArrayList<CoverItemsModel> coverItemsModelArrayList) {
        this.coverItemsModelArrayList = coverItemsModelArrayList;
    }

    protected ItemsModel(Parcel in) {
        itemsId = in.readInt();
        topicTH = in.readString();
        topicEN = in.readString();
        topicLO = in.readString();
        topicZH = in.readString();
        dayOpenTH = in.readString();
        dayOpenEN = in.readString();
        dayOpenLO = in.readString();
        dayOpenZH = in.readString();
        dayCloseTH = in.readString();
        dayCloseEN = in.readString();
        dayCloseLO = in.readString();
        dayCloseZH = in.readString();
        timeOpen = in.readString();
        timeClose = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        contactTH = in.readString();
        contactEN = in.readString();
        contactLO = in.readString();
        contactZH = in.readString();
        phone = in.readString();
        email = in.readString();
        line = in.readString();
        facebookPage = in.readString();
        arURL = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readDouble();
        }
        isActive = in.readInt();
        isPublish = in.readInt();
        dateDurationTH = in.readString();
        dateDurationEN = in.readString();
        dateDurationLO = in.readString();
        dateDurationZH = in.readString();
        highlightsTH = in.readString();
        highlightsEN = in.readString();
        highlightsLO = in.readString();
        highlightsZH = in.readString();
        locationInformationTH = in.readString();
        locationInformationEN = in.readString();
        locationInformationLO = in.readString();
        locationInformationZH = in.readString();
        typeOfCarsTH = in.readString();
        typeOfCarsEN = in.readString();
        typeOfCarsLO = in.readString();
        typeOfCarsZH = in.readString();
        countView = in.readInt();
        createDatetime = in.readString();
        updateDatetime = in.readString();
        isSelectedItemToPlaces = in.readByte() != 0;
        distance = in.readInt();
        distance_metr = in.readInt();
        priceGuestAdult = in.readInt();
        priceGuestChild = in.readInt();
        isOpenOrClosed = in.readByte() != 0;
        bookMarksModel = in.readParcelable(BookMarksModel.class.getClassLoader());
        menuItemModel = in.readParcelable(MenuItemModel.class.getClassLoader());
        categoryModel = in.readParcelable(CategoryModel.class.getClassLoader());
        subMenuItemModel = in.readParcelable(SubMenuItemModel.class.getClassLoader());
        coverItemsModel = in.readParcelable(CoverItemsModel.class.getClassLoader());
        carModel = in.readParcelable(CarModel.class.getClassLoader());
        ratingStarModel = in.readParcelable(RatingStarModel.class.getClassLoader());
        provincesModel = in.readParcelable(ProvincesModel.class.getClassLoader());
        itemsDetailModel = in.readParcelable(ItemsDetailModel.class.getClassLoader());
        roomModel = in.readParcelable(RoomModel.class.getClassLoader());
        businessModel = in.readParcelable(BusinessModel.class.getClassLoader());
        periodTimeModel = in.readParcelable(PeriodTimeModel.class.getClassLoader());
        periodDayModel = in.readParcelable(PeriodDayModel.class.getClassLoader());
        provincesModelArrayList = in.createTypedArrayList(ProvincesModel.CREATOR);
        periodTimeModelArrayList = in.createTypedArrayList(PeriodTimeModel.CREATOR);
        foodModelArrayList = in.createTypedArrayList(FoodModel.CREATOR);
        facilitiesModelArrayList = in.createTypedArrayList(FacilitiesModel.CREATOR);
        itemsDetailModelArrayList = in.createTypedArrayList(ItemsDetailModel.CREATOR);
        travelDetailsModelArrayList = in.createTypedArrayList(TravelDetailsModel.CREATOR);
        travelPeriodModelArrayList = in.createTypedArrayList(TravelPeriodModel.CREATOR);
        productModelArrayList = in.createTypedArrayList(ProductModel.CREATOR);
        roomModelArrayList = in.createTypedArrayList(RoomModel.CREATOR);
        subCategoryModelArrayList = in.createTypedArrayList(SubCategoryModel.CREATOR);
        deliciousGuaranteeModelArrayList = in.createTypedArrayList(DeliciousGuaranteeModel.CREATOR);
        hotelAccommodationModelArrayList = in.createTypedArrayList(HotelAccommodationModel.CREATOR);
        bookingConditionModelArrayList = in.createTypedArrayList(BookingConditionModel.CREATOR);
        reviewModelArrayList = in.createTypedArrayList(ReviewModel.CREATOR);
        coverItemsModelArrayList = in.createTypedArrayList(CoverItemsModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(itemsId);
        dest.writeString(topicTH);
        dest.writeString(topicEN);
        dest.writeString(topicLO);
        dest.writeString(topicZH);
        dest.writeString(dayOpenTH);
        dest.writeString(dayOpenEN);
        dest.writeString(dayOpenLO);
        dest.writeString(dayOpenZH);
        dest.writeString(dayCloseTH);
        dest.writeString(dayCloseEN);
        dest.writeString(dayCloseLO);
        dest.writeString(dayCloseZH);
        dest.writeString(timeOpen);
        dest.writeString(timeClose);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(contactTH);
        dest.writeString(contactEN);
        dest.writeString(contactLO);
        dest.writeString(contactZH);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(line);
        dest.writeString(facebookPage);
        dest.writeString(arURL);
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(price);
        }
        dest.writeInt(isActive);
        dest.writeInt(isPublish);
        dest.writeString(dateDurationTH);
        dest.writeString(dateDurationEN);
        dest.writeString(dateDurationLO);
        dest.writeString(dateDurationZH);
        dest.writeString(highlightsTH);
        dest.writeString(highlightsEN);
        dest.writeString(highlightsLO);
        dest.writeString(highlightsZH);
        dest.writeString(locationInformationTH);
        dest.writeString(locationInformationEN);
        dest.writeString(locationInformationLO);
        dest.writeString(locationInformationZH);
        dest.writeString(typeOfCarsTH);
        dest.writeString(typeOfCarsEN);
        dest.writeString(typeOfCarsLO);
        dest.writeString(typeOfCarsZH);
        dest.writeInt(countView);
        dest.writeString(createDatetime);
        dest.writeString(updateDatetime);
        dest.writeByte((byte) (isSelectedItemToPlaces ? 1 : 0));
        dest.writeInt(distance);
        dest.writeInt(distance_metr);
        dest.writeInt(priceGuestAdult);
        dest.writeInt(priceGuestChild);
        dest.writeByte((byte) (isOpenOrClosed ? 1 : 0));
        dest.writeParcelable(bookMarksModel, flags);
        dest.writeParcelable(menuItemModel, flags);
        dest.writeParcelable(categoryModel, flags);
        dest.writeParcelable(subMenuItemModel, flags);
        dest.writeParcelable(coverItemsModel, flags);
        dest.writeParcelable(carModel, flags);
        dest.writeParcelable(ratingStarModel, flags);
        dest.writeParcelable(provincesModel, flags);
        dest.writeParcelable(itemsDetailModel, flags);
        dest.writeParcelable(roomModel, flags);
        dest.writeParcelable(businessModel, flags);
        dest.writeParcelable(periodTimeModel, flags);
        dest.writeParcelable(periodDayModel, flags);
        dest.writeTypedList(provincesModelArrayList);
        dest.writeTypedList(periodTimeModelArrayList);
        dest.writeTypedList(foodModelArrayList);
        dest.writeTypedList(facilitiesModelArrayList);
        dest.writeTypedList(itemsDetailModelArrayList);
        dest.writeTypedList(travelDetailsModelArrayList);
        dest.writeTypedList(travelPeriodModelArrayList);
        dest.writeTypedList(productModelArrayList);
        dest.writeTypedList(roomModelArrayList);
        dest.writeTypedList(subCategoryModelArrayList);
        dest.writeTypedList(deliciousGuaranteeModelArrayList);
        dest.writeTypedList(hotelAccommodationModelArrayList);
        dest.writeTypedList(bookingConditionModelArrayList);
        dest.writeTypedList(reviewModelArrayList);
        dest.writeTypedList(coverItemsModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemsModel> CREATOR = new Creator<ItemsModel>() {
        @Override
        public ItemsModel createFromParcel(Parcel in) {
            return new ItemsModel(in);
        }

        @Override
        public ItemsModel[] newArray(int size) {
            return new ItemsModel[size];
        }
    };
}
