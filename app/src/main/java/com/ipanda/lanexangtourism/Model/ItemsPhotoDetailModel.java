package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemsPhotoDetailModel implements Parcelable {

    private int photoId;
    private String photoPaths;
    private String photoTextTH;
    private String photoTextEN;
    private String photoTextLO;
    private String photoTextZH;

    public ItemsPhotoDetailModel() {

    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public String getPhotoPaths() {
        return photoPaths;
    }

    public void setPhotoPaths(String photoPaths) {
        this.photoPaths = photoPaths;
    }

    public String getPhotoTextTH() {
        return photoTextTH;
    }

    public void setPhotoTextTH(String photoTextTH) {
        this.photoTextTH = photoTextTH;
    }

    public String getPhotoTextEN() {
        return photoTextEN;
    }

    public void setPhotoTextEN(String photoTextEN) {
        this.photoTextEN = photoTextEN;
    }

    public String getPhotoTextLO() {
        return photoTextLO;
    }

    public void setPhotoTextLO(String photoTextLO) {
        this.photoTextLO = photoTextLO;
    }

    public String getPhotoTextZH() {
        return photoTextZH;
    }

    public void setPhotoTextZH(String photoTextZH) {
        this.photoTextZH = photoTextZH;
    }

    protected ItemsPhotoDetailModel(Parcel in) {
        photoId = in.readInt();
        photoPaths = in.readString();
        photoTextTH = in.readString();
        photoTextEN = in.readString();
        photoTextLO = in.readString();
        photoTextZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(photoId);
        dest.writeString(photoPaths);
        dest.writeString(photoTextTH);
        dest.writeString(photoTextEN);
        dest.writeString(photoTextLO);
        dest.writeString(photoTextZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemsPhotoDetailModel> CREATOR = new Creator<ItemsPhotoDetailModel>() {
        @Override
        public ItemsPhotoDetailModel createFromParcel(Parcel in) {
            return new ItemsPhotoDetailModel(in);
        }

        @Override
        public ItemsPhotoDetailModel[] newArray(int size) {
            return new ItemsPhotoDetailModel[size];
        }
    };
}
