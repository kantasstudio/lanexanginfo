package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemsTopicDetailModel implements Parcelable {

    private int topicDetailId;
    private String topicDetailTH;
    private String topicDetailEN;
    private String topicDetailLO;
    private String topicDetailZH;

    public ItemsTopicDetailModel() {

    }

    public int getTopicDetailId() {
        return topicDetailId;
    }

    public void setTopicDetailId(int topicDetailId) {
        this.topicDetailId = topicDetailId;
    }

    public String getTopicDetailTH() {
        return topicDetailTH;
    }

    public void setTopicDetailTH(String topicDetailTH) {
        this.topicDetailTH = topicDetailTH;
    }

    public String getTopicDetailEN() {
        return topicDetailEN;
    }

    public void setTopicDetailEN(String topicDetailEN) {
        this.topicDetailEN = topicDetailEN;
    }

    public String getTopicDetailLO() {
        return topicDetailLO;
    }

    public void setTopicDetailLO(String topicDetailLO) {
        this.topicDetailLO = topicDetailLO;
    }

    public String getTopicDetailZH() {
        return topicDetailZH;
    }

    public void setTopicDetailZH(String topicDetailZH) {
        this.topicDetailZH = topicDetailZH;
    }

    protected ItemsTopicDetailModel(Parcel in) {
        topicDetailId = in.readInt();
        topicDetailTH = in.readString();
        topicDetailEN = in.readString();
        topicDetailLO = in.readString();
        topicDetailZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(topicDetailId);
        dest.writeString(topicDetailTH);
        dest.writeString(topicDetailEN);
        dest.writeString(topicDetailLO);
        dest.writeString(topicDetailZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemsTopicDetailModel> CREATOR = new Creator<ItemsTopicDetailModel>() {
        @Override
        public ItemsTopicDetailModel createFromParcel(Parcel in) {
            return new ItemsTopicDetailModel(in);
        }

        @Override
        public ItemsTopicDetailModel[] newArray(int size) {
            return new ItemsTopicDetailModel[size];
        }
    };
}
