package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class LikesModel implements Parcelable {

    private int likesId;
    private String likesTimestamp;
    private int likes;
    private boolean likeState;
    private int totalLikes;
    private ArrayList<UserModel> userModelArrayList;
    private ArrayList<ItemsModel> itemsModelArrayList;

    public LikesModel() {
        this.totalLikes = 0;
    }

    public void getPositiveLike(){
        totalLikes = (likes+1);
    }

    public void getDeleteLike(){
        totalLikes = (likes-1);
    }

    public int getLikesId() {
        return likesId;
    }

    public void setLikesId(int likesId) {
        this.likesId = likesId;
    }

    public String getLikesTimestamp() {
        return likesTimestamp;
    }

    public void setLikesTimestamp(String likesTimestamp) {
        this.likesTimestamp = likesTimestamp;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLikeState() {
        return likeState;
    }

    public void setLikeState(boolean likeState) {
        this.likeState = likeState;
    }

    public int getTotalLikes() {
        return totalLikes;
    }

    public ArrayList<UserModel> getUserModelArrayList() {
        return userModelArrayList;
    }

    public void setUserModelArrayList(ArrayList<UserModel> userModelArrayList) {
        this.userModelArrayList = userModelArrayList;
    }

    public ArrayList<ItemsModel> getItemsModelArrayList() {
        return itemsModelArrayList;
    }

    public void setItemsModelArrayList(ArrayList<ItemsModel> itemsModelArrayList) {
        this.itemsModelArrayList = itemsModelArrayList;
    }

    protected LikesModel(Parcel in) {
        likesId = in.readInt();
        likesTimestamp = in.readString();
        likes = in.readInt();
        likeState = in.readByte() != 0;
        totalLikes = in.readInt();
        userModelArrayList = in.createTypedArrayList(UserModel.CREATOR);
        itemsModelArrayList = in.createTypedArrayList(ItemsModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(likesId);
        dest.writeString(likesTimestamp);
        dest.writeInt(likes);
        dest.writeByte((byte) (likeState ? 1 : 0));
        dest.writeInt(totalLikes);
        dest.writeTypedList(userModelArrayList);
        dest.writeTypedList(itemsModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LikesModel> CREATOR = new Creator<LikesModel>() {
        @Override
        public LikesModel createFromParcel(Parcel in) {
            return new LikesModel(in);
        }

        @Override
        public LikesModel[] newArray(int size) {
            return new LikesModel[size];
        }
    };





}
