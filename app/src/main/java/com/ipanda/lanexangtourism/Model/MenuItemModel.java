package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class MenuItemModel implements Parcelable {

    private int menuItemId;
    private String menuItemTH;
    private String menuItemEN;
    private String menuItemLO;
    private String menuItemZH;

    public MenuItemModel() {

    }

    public MenuItemModel(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public String getMenuItemTH() {
        return menuItemTH;
    }

    public void setMenuItemTH(String menuItemTH) {
        this.menuItemTH = menuItemTH;
    }

    public String getMenuItemEN() {
        return menuItemEN;
    }

    public void setMenuItemEN(String menuItemEN) {
        this.menuItemEN = menuItemEN;
    }

    public String getMenuItemLO() {
        return menuItemLO;
    }

    public void setMenuItemLO(String menuItemLO) {
        this.menuItemLO = menuItemLO;
    }

    public String getMenuItemZH() {
        return menuItemZH;
    }

    public void setMenuItemZH(String menuItemZH) {
        this.menuItemZH = menuItemZH;
    }

    protected MenuItemModel(Parcel in) {
        menuItemId = in.readInt();
        menuItemTH = in.readString();
        menuItemEN = in.readString();
        menuItemLO = in.readString();
        menuItemZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(menuItemId);
        dest.writeString(menuItemTH);
        dest.writeString(menuItemEN);
        dest.writeString(menuItemLO);
        dest.writeString(menuItemZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MenuItemModel> CREATOR = new Creator<MenuItemModel>() {
        @Override
        public MenuItemModel createFromParcel(Parcel in) {
            return new MenuItemModel(in);
        }

        @Override
        public MenuItemModel[] newArray(int size) {
            return new MenuItemModel[size];
        }
    };
}
