package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class MessageModel implements Parcelable {

    private int messageId;
    private int roomId;
    private String message;
    private String createdAt;
    private boolean isRead;
    private int countMessage;
    private String gcmId;
    private UserModel userModel;
    private UserModel userSend;
    private ItemsModel itemsModel;
    private BusinessModel businessModel;

    public MessageModel() {

    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public int getCountMessage() {
        return countMessage;
    }

    public void setCountMessage(int countMessage) {
        this.countMessage = countMessage;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public UserModel getUserSend() {
        return userSend;
    }

    public void setUserSend(UserModel userSend) {
        this.userSend = userSend;
    }

    public ItemsModel getItemsModel() {
        return itemsModel;
    }

    public void setItemsModel(ItemsModel itemsModel) {
        this.itemsModel = itemsModel;
    }

    public BusinessModel getBusinessModel() {
        return businessModel;
    }

    public void setBusinessModel(BusinessModel businessModel) {
        this.businessModel = businessModel;
    }

    protected MessageModel(Parcel in) {
        messageId = in.readInt();
        roomId = in.readInt();
        message = in.readString();
        createdAt = in.readString();
        isRead = in.readByte() != 0;
        countMessage = in.readInt();
        gcmId = in.readString();
        userModel = in.readParcelable(UserModel.class.getClassLoader());
        userSend = in.readParcelable(UserModel.class.getClassLoader());
        itemsModel = in.readParcelable(ItemsModel.class.getClassLoader());
        businessModel = in.readParcelable(BusinessModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(messageId);
        dest.writeInt(roomId);
        dest.writeString(message);
        dest.writeString(createdAt);
        dest.writeByte((byte) (isRead ? 1 : 0));
        dest.writeInt(countMessage);
        dest.writeString(gcmId);
        dest.writeParcelable(userModel, flags);
        dest.writeParcelable(userSend, flags);
        dest.writeParcelable(itemsModel, flags);
        dest.writeParcelable(businessModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MessageModel> CREATOR = new Creator<MessageModel>() {
        @Override
        public MessageModel createFromParcel(Parcel in) {
            return new MessageModel(in);
        }

        @Override
        public MessageModel[] newArray(int size) {
            return new MessageModel[size];
        }
    };
}
