package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class MonthModel implements Parcelable {

    private boolean january;
    private boolean february;
    private boolean march;
    private boolean april;
    private boolean may;
    private boolean june;
    private boolean july;
    private boolean august;
    private boolean september;
    private boolean october;
    private boolean november;
    private boolean december;

    public MonthModel() {
    }

    public boolean isJanuary() {
        return january;
    }

    public void setJanuary(boolean january) {
        this.january = january;
    }

    public boolean isFebruary() {
        return february;
    }

    public void setFebruary(boolean february) {
        this.february = february;
    }

    public boolean isMarch() {
        return march;
    }

    public void setMarch(boolean march) {
        this.march = march;
    }

    public boolean isApril() {
        return april;
    }

    public void setApril(boolean april) {
        this.april = april;
    }

    public boolean isMay() {
        return may;
    }

    public void setMay(boolean may) {
        this.may = may;
    }

    public boolean isJune() {
        return june;
    }

    public void setJune(boolean june) {
        this.june = june;
    }

    public boolean isJuly() {
        return july;
    }

    public void setJuly(boolean july) {
        this.july = july;
    }

    public boolean isAugust() {
        return august;
    }

    public void setAugust(boolean august) {
        this.august = august;
    }

    public boolean isSeptember() {
        return september;
    }

    public void setSeptember(boolean september) {
        this.september = september;
    }

    public boolean isOctober() {
        return october;
    }

    public void setOctober(boolean october) {
        this.october = october;
    }

    public boolean isNovember() {
        return november;
    }

    public void setNovember(boolean november) {
        this.november = november;
    }

    public boolean isDecember() {
        return december;
    }

    public void setDecember(boolean december) {
        this.december = december;
    }

    protected MonthModel(Parcel in) {
        january = in.readByte() != 0;
        february = in.readByte() != 0;
        march = in.readByte() != 0;
        april = in.readByte() != 0;
        may = in.readByte() != 0;
        june = in.readByte() != 0;
        july = in.readByte() != 0;
        august = in.readByte() != 0;
        september = in.readByte() != 0;
        october = in.readByte() != 0;
        november = in.readByte() != 0;
        december = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (january ? 1 : 0));
        dest.writeByte((byte) (february ? 1 : 0));
        dest.writeByte((byte) (march ? 1 : 0));
        dest.writeByte((byte) (april ? 1 : 0));
        dest.writeByte((byte) (may ? 1 : 0));
        dest.writeByte((byte) (june ? 1 : 0));
        dest.writeByte((byte) (july ? 1 : 0));
        dest.writeByte((byte) (august ? 1 : 0));
        dest.writeByte((byte) (september ? 1 : 0));
        dest.writeByte((byte) (october ? 1 : 0));
        dest.writeByte((byte) (november ? 1 : 0));
        dest.writeByte((byte) (december ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MonthModel> CREATOR = new Creator<MonthModel>() {
        @Override
        public MonthModel createFromParcel(Parcel in) {
            return new MonthModel(in);
        }

        @Override
        public MonthModel[] newArray(int size) {
            return new MonthModel[size];
        }
    };
}
