package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class PartnerModel implements Parcelable {

    private int partnerId;
    private String partnerTH;
    private String partnerEN;
    private String partnerLO;
    private String partnerZH;
    private String loginFormsApp;
    private UserModel userModel;

    public PartnerModel() {

    }

    public int getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerTH() {
        return partnerTH;
    }

    public void setPartnerTH(String partnerTH) {
        this.partnerTH = partnerTH;
    }

    public String getPartnerEN() {
        return partnerEN;
    }

    public void setPartnerEN(String partnerEN) {
        this.partnerEN = partnerEN;
    }

    public String getPartnerLO() {
        return partnerLO;
    }

    public void setPartnerLO(String partnerLO) {
        this.partnerLO = partnerLO;
    }

    public String getPartnerZH() {
        return partnerZH;
    }

    public void setPartnerZH(String partnerZH) {
        this.partnerZH = partnerZH;
    }

    public String getLoginFormsApp() {
        return loginFormsApp;
    }

    public void setLoginFormsApp(String loginFormsApp) {
        this.loginFormsApp = loginFormsApp;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    protected PartnerModel(Parcel in) {
        partnerId = in.readInt();
        partnerTH = in.readString();
        partnerEN = in.readString();
        partnerLO = in.readString();
        partnerZH = in.readString();
        loginFormsApp = in.readString();
        userModel = in.readParcelable(UserModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(partnerId);
        dest.writeString(partnerTH);
        dest.writeString(partnerEN);
        dest.writeString(partnerLO);
        dest.writeString(partnerZH);
        dest.writeString(loginFormsApp);
        dest.writeParcelable(userModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PartnerModel> CREATOR = new Creator<PartnerModel>() {
        @Override
        public PartnerModel createFromParcel(Parcel in) {
            return new PartnerModel(in);
        }

        @Override
        public PartnerModel[] newArray(int size) {
            return new PartnerModel[size];
        }
    };
}
