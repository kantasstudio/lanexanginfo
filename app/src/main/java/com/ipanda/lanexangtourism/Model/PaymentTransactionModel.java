package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class PaymentTransactionModel implements Parcelable {

    private int paymentId;
    private String timestamp;
    private Double total;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String weChatId;
    private String whatsAppId;
    private String discount;
    private PurchaseStatusModel purchaseStatusModel;

    public PaymentTransactionModel() {

    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWeChatId() {
        return weChatId;
    }

    public void setWeChatId(String weChatId) {
        this.weChatId = weChatId;
    }

    public String getWhatsAppId() {
        return whatsAppId;
    }

    public void setWhatsAppId(String whatsAppId) {
        this.whatsAppId = whatsAppId;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public PurchaseStatusModel getPurchaseStatusModel() {
        return purchaseStatusModel;
    }

    public void setPurchaseStatusModel(PurchaseStatusModel purchaseStatusModel) {
        this.purchaseStatusModel = purchaseStatusModel;
    }

    protected PaymentTransactionModel(Parcel in) {
        paymentId = in.readInt();
        timestamp = in.readString();
        if (in.readByte() == 0) {
            total = null;
        } else {
            total = in.readDouble();
        }
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        phone = in.readString();
        weChatId = in.readString();
        whatsAppId = in.readString();
        discount = in.readString();
        purchaseStatusModel = in.readParcelable(PurchaseStatusModel.class.getClassLoader());
    }

    public static final Creator<PaymentTransactionModel> CREATOR = new Creator<PaymentTransactionModel>() {
        @Override
        public PaymentTransactionModel createFromParcel(Parcel in) {
            return new PaymentTransactionModel(in);
        }

        @Override
        public PaymentTransactionModel[] newArray(int size) {
            return new PaymentTransactionModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(paymentId);
        parcel.writeString(timestamp);
        if (total == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(total);
        }
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(email);
        parcel.writeString(phone);
        parcel.writeString(weChatId);
        parcel.writeString(whatsAppId);
        parcel.writeString(discount);
        parcel.writeParcelable(purchaseStatusModel, i);
    }
}
