package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class PeriodDayModel implements Parcelable {

    private int periodDayId;
    private int periodOpenId;
    private int periodCloseId;
    private String periodOpenTH;
    private String periodOpenEN;
    private String periodOpenLO;
    private String periodOpenZH;
    private String periodCloseTH;
    private String periodCloseEN;
    private String periodCloseLO;
    private String periodCloseZH;

    public PeriodDayModel() {

    }

    public int getPeriodDayId() {
        return periodDayId;
    }

    public void setPeriodDayId(int periodDayId) {
        this.periodDayId = periodDayId;
    }

    public int getPeriodOpenId() {
        return periodOpenId;
    }

    public void setPeriodOpenId(int periodOpenId) {
        this.periodOpenId = periodOpenId;
    }

    public int getPeriodCloseId() {
        return periodCloseId;
    }

    public void setPeriodCloseId(int periodCloseId) {
        this.periodCloseId = periodCloseId;
    }

    public String getPeriodOpenTH() {
        return periodOpenTH;
    }

    public void setPeriodOpenTH(String periodOpenTH) {
        this.periodOpenTH = periodOpenTH;
    }

    public String getPeriodOpenEN() {
        return periodOpenEN;
    }

    public void setPeriodOpenEN(String periodOpenEN) {
        this.periodOpenEN = periodOpenEN;
    }

    public String getPeriodOpenLO() {
        return periodOpenLO;
    }

    public void setPeriodOpenLO(String periodOpenLO) {
        this.periodOpenLO = periodOpenLO;
    }

    public String getPeriodOpenZH() {
        return periodOpenZH;
    }

    public void setPeriodOpenZH(String periodOpenZH) {
        this.periodOpenZH = periodOpenZH;
    }

    public String getPeriodCloseTH() {
        return periodCloseTH;
    }

    public void setPeriodCloseTH(String periodCloseTH) {
        this.periodCloseTH = periodCloseTH;
    }

    public String getPeriodCloseEN() {
        return periodCloseEN;
    }

    public void setPeriodCloseEN(String periodCloseEN) {
        this.periodCloseEN = periodCloseEN;
    }

    public String getPeriodCloseLO() {
        return periodCloseLO;
    }

    public void setPeriodCloseLO(String periodCloseLO) {
        this.periodCloseLO = periodCloseLO;
    }

    public String getPeriodCloseZH() {
        return periodCloseZH;
    }

    public void setPeriodCloseZH(String periodCloseZH) {
        this.periodCloseZH = periodCloseZH;
    }

    protected PeriodDayModel(Parcel in) {
        periodDayId = in.readInt();
        periodOpenId = in.readInt();
        periodCloseId = in.readInt();
        periodOpenTH = in.readString();
        periodOpenEN = in.readString();
        periodOpenLO = in.readString();
        periodOpenZH = in.readString();
        periodCloseTH = in.readString();
        periodCloseEN = in.readString();
        periodCloseLO = in.readString();
        periodCloseZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(periodDayId);
        dest.writeInt(periodOpenId);
        dest.writeInt(periodCloseId);
        dest.writeString(periodOpenTH);
        dest.writeString(periodOpenEN);
        dest.writeString(periodOpenLO);
        dest.writeString(periodOpenZH);
        dest.writeString(periodCloseTH);
        dest.writeString(periodCloseEN);
        dest.writeString(periodCloseLO);
        dest.writeString(periodCloseZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PeriodDayModel> CREATOR = new Creator<PeriodDayModel>() {
        @Override
        public PeriodDayModel createFromParcel(Parcel in) {
            return new PeriodDayModel(in);
        }

        @Override
        public PeriodDayModel[] newArray(int size) {
            return new PeriodDayModel[size];
        }
    };
}
