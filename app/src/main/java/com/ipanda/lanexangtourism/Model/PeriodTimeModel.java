package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class PeriodTimeModel implements Parcelable {

    private int periodId;
    private String periodOpen;
    private String periodClose;
    private PeriodDayModel periodDayModel;

    public PeriodTimeModel() {

    }

    public int getPeriodId() {
        return periodId;
    }

    public void setPeriodId(int periodId) {
        this.periodId = periodId;
    }

    public String getPeriodOpen() {
        return periodOpen;
    }

    public void setPeriodOpen(String periodOpen) {
        this.periodOpen = periodOpen;
    }

    public String getPeriodClose() {
        return periodClose;
    }

    public void setPeriodClose(String periodClose) {
        this.periodClose = periodClose;
    }

    public PeriodDayModel getPeriodDayModel() {
        return periodDayModel;
    }

    public void setPeriodDayModel(PeriodDayModel periodDayModel) {
        this.periodDayModel = periodDayModel;
    }

    protected PeriodTimeModel(Parcel in) {
        periodId = in.readInt();
        periodOpen = in.readString();
        periodClose = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(periodId);
        dest.writeString(periodOpen);
        dest.writeString(periodClose);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PeriodTimeModel> CREATOR = new Creator<PeriodTimeModel>() {
        @Override
        public PeriodTimeModel createFromParcel(Parcel in) {
            return new PeriodTimeModel(in);
        }

        @Override
        public PeriodTimeModel[] newArray(int size) {
            return new PeriodTimeModel[size];
        }
    };
}
