package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class PermissionProvinceGroupModel implements Parcelable {

    private int ppgId;
    private String ppgTH;
    private String ppgEN;
    private String ppgLO;
    private String ppgZH;

    public PermissionProvinceGroupModel() {

    }

    public int getPpgId() {
        return ppgId;
    }

    public void setPpgId(int ppgId) {
        this.ppgId = ppgId;
    }

    public String getPpgTH() {
        return ppgTH;
    }

    public void setPpgTH(String ppgTH) {
        this.ppgTH = ppgTH;
    }

    public String getPpgEN() {
        return ppgEN;
    }

    public void setPpgEN(String ppgEN) {
        this.ppgEN = ppgEN;
    }

    public String getPpgLO() {
        return ppgLO;
    }

    public void setPpgLO(String ppgLO) {
        this.ppgLO = ppgLO;
    }

    public String getPpgZH() {
        return ppgZH;
    }

    public void setPpgZH(String ppgZH) {
        this.ppgZH = ppgZH;
    }

    protected PermissionProvinceGroupModel(Parcel in) {
        ppgId = in.readInt();
        ppgTH = in.readString();
        ppgEN = in.readString();
        ppgLO = in.readString();
        ppgZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ppgId);
        dest.writeString(ppgTH);
        dest.writeString(ppgEN);
        dest.writeString(ppgLO);
        dest.writeString(ppgZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PermissionProvinceGroupModel> CREATOR = new Creator<PermissionProvinceGroupModel>() {
        @Override
        public PermissionProvinceGroupModel createFromParcel(Parcel in) {
            return new PermissionProvinceGroupModel(in);
        }

        @Override
        public PermissionProvinceGroupModel[] newArray(int size) {
            return new PermissionProvinceGroupModel[size];
        }
    };
}
