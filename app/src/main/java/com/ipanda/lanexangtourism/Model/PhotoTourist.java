package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class PhotoTourist implements Parcelable {

    private int photoTouristId;
    private String photoTouristPaths;
    private String photoTouristTH;
    private String photoTouristEN;
    private String photoTouristLO;
    private String photoTouristZH;
    private boolean isPaths;
    private String photoState;

    public PhotoTourist() {

    }


    public int getPhotoTouristId() {
        return photoTouristId;
    }

    public void setPhotoTouristId(int photoTouristId) {
        this.photoTouristId = photoTouristId;
    }

    public String getPhotoTouristPaths() {
        return photoTouristPaths;
    }

    public void setPhotoTouristPaths(String photoTouristPaths) {
        this.photoTouristPaths = photoTouristPaths;
    }

    public String getPhotoTouristTH() {
        return photoTouristTH;
    }

    public void setPhotoTouristTH(String photoTouristTH) {
        this.photoTouristTH = photoTouristTH;
    }

    public String getPhotoTouristEN() {
        return photoTouristEN;
    }

    public void setPhotoTouristEN(String photoTouristEN) {
        this.photoTouristEN = photoTouristEN;
    }

    public String getPhotoTouristLO() {
        return photoTouristLO;
    }

    public void setPhotoTouristLO(String photoTouristLO) {
        this.photoTouristLO = photoTouristLO;
    }

    public String getPhotoTouristZH() {
        return photoTouristZH;
    }

    public void setPhotoTouristZH(String photoTouristZH) {
        this.photoTouristZH = photoTouristZH;
    }

    public boolean isPaths() {
        return isPaths;
    }

    public void setPaths(boolean paths) {
        isPaths = paths;
    }

    public String getPhotoState() {
        return photoState;
    }

    public void setPhotoState(String photoState) {
        this.photoState = photoState;
    }

    protected PhotoTourist(Parcel in) {
        photoTouristId = in.readInt();
        photoTouristPaths = in.readString();
        photoTouristTH = in.readString();
        photoTouristEN = in.readString();
        photoTouristLO = in.readString();
        photoTouristZH = in.readString();
        isPaths = in.readByte() != 0;
        photoState = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(photoTouristId);
        dest.writeString(photoTouristPaths);
        dest.writeString(photoTouristTH);
        dest.writeString(photoTouristEN);
        dest.writeString(photoTouristLO);
        dest.writeString(photoTouristZH);
        dest.writeByte((byte) (isPaths ? 1 : 0));
        dest.writeString(photoState);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PhotoTourist> CREATOR = new Creator<PhotoTourist>() {
        @Override
        public PhotoTourist createFromParcel(Parcel in) {
            return new PhotoTourist(in);
        }

        @Override
        public PhotoTourist[] newArray(int size) {
            return new PhotoTourist[size];
        }
    };
}
