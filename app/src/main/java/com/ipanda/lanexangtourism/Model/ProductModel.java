package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ProductModel implements Parcelable {

    private int productId;
    private String productNamesTH;
    private String productNamesEN;
    private String productNamesLO;
    private String productNamesZH;
    private double productPrice;
    private String descriptionTH;
    private String descriptionEN;
    private String descriptionLO;
    private String descriptionZH;
    private boolean isSelected;
    private ProductOrderDetails productOrderDetails;
    private UserModel userModel;
    private ArrayList<ProductPhotoModel> productPhotoModelArrayList;

    public ProductModel() {

    }

    protected ProductModel(Parcel in) {
        productId = in.readInt();
        productNamesTH = in.readString();
        productNamesEN = in.readString();
        productNamesLO = in.readString();
        productNamesZH = in.readString();
        productPrice = in.readDouble();
        descriptionTH = in.readString();
        descriptionEN = in.readString();
        descriptionLO = in.readString();
        descriptionZH = in.readString();
        isSelected = in.readByte() != 0;
        productOrderDetails = in.readParcelable(ProductOrderDetails.class.getClassLoader());
        userModel = in.readParcelable(UserModel.class.getClassLoader());
        productPhotoModelArrayList = in.createTypedArrayList(ProductPhotoModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(productId);
        dest.writeString(productNamesTH);
        dest.writeString(productNamesEN);
        dest.writeString(productNamesLO);
        dest.writeString(productNamesZH);
        dest.writeDouble(productPrice);
        dest.writeString(descriptionTH);
        dest.writeString(descriptionEN);
        dest.writeString(descriptionLO);
        dest.writeString(descriptionZH);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeParcelable(productOrderDetails, flags);
        dest.writeParcelable(userModel, flags);
        dest.writeTypedList(productPhotoModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductModel> CREATOR = new Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel in) {
            return new ProductModel(in);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductNamesTH() {
        return productNamesTH;
    }

    public void setProductNamesTH(String productNamesTH) {
        this.productNamesTH = productNamesTH;
    }

    public String getProductNamesEN() {
        return productNamesEN;
    }

    public void setProductNamesEN(String productNamesEN) {
        this.productNamesEN = productNamesEN;
    }

    public String getProductNamesLO() {
        return productNamesLO;
    }

    public void setProductNamesLO(String productNamesLO) {
        this.productNamesLO = productNamesLO;
    }

    public String getProductNamesZH() {
        return productNamesZH;
    }

    public void setProductNamesZH(String productNamesZH) {
        this.productNamesZH = productNamesZH;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getDescriptionTH() {
        return descriptionTH;
    }

    public void setDescriptionTH(String descriptionTH) {
        this.descriptionTH = descriptionTH;
    }

    public String getDescriptionEN() {
        return descriptionEN;
    }

    public void setDescriptionEN(String descriptionEN) {
        this.descriptionEN = descriptionEN;
    }

    public String getDescriptionLO() {
        return descriptionLO;
    }

    public void setDescriptionLO(String descriptionLO) {
        this.descriptionLO = descriptionLO;
    }

    public String getDescriptionZH() {
        return descriptionZH;
    }

    public void setDescriptionZH(String descriptionZH) {
        this.descriptionZH = descriptionZH;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ProductOrderDetails getProductOrderDetails() {
        return productOrderDetails;
    }

    public void setProductOrderDetails(ProductOrderDetails productOrderDetails) {
        this.productOrderDetails = productOrderDetails;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public ArrayList<ProductPhotoModel> getProductPhotoModelArrayList() {
        return productPhotoModelArrayList;
    }

    public void setProductPhotoModelArrayList(ArrayList<ProductPhotoModel> productPhotoModelArrayList) {
        this.productPhotoModelArrayList = productPhotoModelArrayList;
    }
}
