package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductOrderDetails implements Parcelable {

    private int oderId;
    private String oderTimestamp;
    private int quantity;
    private double totalPrice;
    private double price;


    public ProductOrderDetails() {

    }

    public int getOderId() {
        return oderId;
    }

    public void setOderId(int oderId) {
        this.oderId = oderId;
    }

    public String getOderTimestamp() {
        return oderTimestamp;
    }

    public void setOderTimestamp(String oderTimestamp) {
        this.oderTimestamp = oderTimestamp;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    protected ProductOrderDetails(Parcel in) {
        oderId = in.readInt();
        oderTimestamp = in.readString();
        quantity = in.readInt();
        totalPrice = in.readDouble();
        price = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(oderId);
        dest.writeString(oderTimestamp);
        dest.writeInt(quantity);
        dest.writeDouble(totalPrice);
        dest.writeDouble(price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductOrderDetails> CREATOR = new Creator<ProductOrderDetails>() {
        @Override
        public ProductOrderDetails createFromParcel(Parcel in) {
            return new ProductOrderDetails(in);
        }

        @Override
        public ProductOrderDetails[] newArray(int size) {
            return new ProductOrderDetails[size];
        }
    };
}
