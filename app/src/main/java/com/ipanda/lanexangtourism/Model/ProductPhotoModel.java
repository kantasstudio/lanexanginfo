package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductPhotoModel implements Parcelable {

    private int productPhotoId;
    private String productPhotoPaths;

    public ProductPhotoModel() {
    }

    public int getProductPhotoId() {
        return productPhotoId;
    }

    public void setProductPhotoId(int productPhotoId) {
        this.productPhotoId = productPhotoId;
    }

    public String getProductPhotoPaths() {
        return productPhotoPaths;
    }

    public void setProductPhotoPaths(String productPhotoPaths) {
        this.productPhotoPaths = productPhotoPaths;
    }

    protected ProductPhotoModel(Parcel in) {
        productPhotoId = in.readInt();
        productPhotoPaths = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(productPhotoId);
        dest.writeString(productPhotoPaths);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductPhotoModel> CREATOR = new Creator<ProductPhotoModel>() {
        @Override
        public ProductPhotoModel createFromParcel(Parcel in) {
            return new ProductPhotoModel(in);
        }

        @Override
        public ProductPhotoModel[] newArray(int size) {
            return new ProductPhotoModel[size];
        }
    };
}
