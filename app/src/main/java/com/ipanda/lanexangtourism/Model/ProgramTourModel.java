package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ProgramTourModel implements Parcelable {

    private int programTourId;
    private String programTourNameTH;
    private String programTourNameEN;
    private String programTourNameLO;
    private String programTourNameZH;
    private String interestingThingsTH;
    private String interestingThingsEN;
    private String interestingThingsLO;
    private String interestingThingsZH;
    private String createdDTTM;
    private String updatedDTTM;
    private String countView;
    private String pathCovers;
    private String coverItem_state;
    private int isPublishStatus;
    private int isActive;
    private boolean isOfficial;
    private String isSaveDraft;
    private LikesModel likesModel;
    private MenuItemModel menuItemModel;
    private CoverItemsModel coverItemsModel;
    private UserModel userModel;
    private CategoryModel categoryModel;
    private SubCategoryModel subCategoryModel;
    private PublishStatusModel publishStatusModel;
    private ArrayList<DatesTripModel> datesTripModelArrayList;

    public ProgramTourModel() {

    }

    public int getProgramTourId() {
        return programTourId;
    }

    public void setProgramTourId(int programTourId) {
        this.programTourId = programTourId;
    }

    public String getProgramTourNameTH() {
        return programTourNameTH;
    }

    public void setProgramTourNameTH(String programTourNameTH) {
        this.programTourNameTH = programTourNameTH;
    }

    public String getProgramTourNameEN() {
        return programTourNameEN;
    }

    public void setProgramTourNameEN(String programTourNameEN) {
        this.programTourNameEN = programTourNameEN;
    }

    public String getProgramTourNameLO() {
        return programTourNameLO;
    }

    public void setProgramTourNameLO(String programTourNameLO) {
        this.programTourNameLO = programTourNameLO;
    }

    public String getProgramTourNameZH() {
        return programTourNameZH;
    }

    public void setProgramTourNameZH(String programTourNameZH) {
        this.programTourNameZH = programTourNameZH;
    }

    public String getInterestingThingsTH() {
        return interestingThingsTH;
    }

    public void setInterestingThingsTH(String interestingThingsTH) {
        this.interestingThingsTH = interestingThingsTH;
    }

    public String getInterestingThingsEN() {
        return interestingThingsEN;
    }

    public void setInterestingThingsEN(String interestingThingsEN) {
        this.interestingThingsEN = interestingThingsEN;
    }

    public String getInterestingThingsLO() {
        return interestingThingsLO;
    }

    public void setInterestingThingsLO(String interestingThingsLO) {
        this.interestingThingsLO = interestingThingsLO;
    }

    public String getInterestingThingsZH() {
        return interestingThingsZH;
    }

    public void setInterestingThingsZH(String interestingThingsZH) {
        this.interestingThingsZH = interestingThingsZH;
    }

    public String getCreatedDTTM() {
        return createdDTTM;
    }

    public void setCreatedDTTM(String createdDTTM) {
        this.createdDTTM = createdDTTM;
    }

    public String getUpdatedDTTM() {
        return updatedDTTM;
    }

    public void setUpdatedDTTM(String updatedDTTM) {
        this.updatedDTTM = updatedDTTM;
    }

    public String getCountView() {
        return countView;
    }

    public void setCountView(String countView) {
        this.countView = countView;
    }

    public String getPathCovers() {
        return pathCovers;
    }

    public void setPathCovers(String pathCovers) {
        this.pathCovers = pathCovers;
    }

    public String getCoverItem_state() {
        return coverItem_state;
    }

    public void setCoverItem_state(String coverItem_state) {
        this.coverItem_state = coverItem_state;
    }

    public int getIsPublishStatus() {
        return isPublishStatus;
    }

    public void setIsPublishStatus(int isPublishStatus) {
        this.isPublishStatus = isPublishStatus;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public boolean isOfficial() {
        return isOfficial;
    }

    public void setOfficial(boolean official) {
        isOfficial = official;
    }

    public String getIsSaveDraft() {
        return isSaveDraft;
    }

    public void setIsSaveDraft(String isSaveDraft) {
        this.isSaveDraft = isSaveDraft;
    }

    public LikesModel getLikesModel() {
        return likesModel;
    }

    public void setLikesModel(LikesModel likesModel) {
        this.likesModel = likesModel;
    }

    public MenuItemModel getMenuItemModel() {
        return menuItemModel;
    }

    public void setMenuItemModel(MenuItemModel menuItemModel) {
        this.menuItemModel = menuItemModel;
    }

    public CoverItemsModel getCoverItemsModel() {
        return coverItemsModel;
    }

    public void setCoverItemsModel(CoverItemsModel coverItemsModel) {
        this.coverItemsModel = coverItemsModel;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public SubCategoryModel getSubCategoryModel() {
        return subCategoryModel;
    }

    public void setSubCategoryModel(SubCategoryModel subCategoryModel) {
        this.subCategoryModel = subCategoryModel;
    }

    public PublishStatusModel getPublishStatusModel() {
        return publishStatusModel;
    }

    public void setPublishStatusModel(PublishStatusModel publishStatusModel) {
        this.publishStatusModel = publishStatusModel;
    }

    public ArrayList<DatesTripModel> getDatesTripModelArrayList() {
        return datesTripModelArrayList;
    }

    public void setDatesTripModelArrayList(ArrayList<DatesTripModel> datesTripModelArrayList) {
        this.datesTripModelArrayList = datesTripModelArrayList;
    }

    protected ProgramTourModel(Parcel in) {
        programTourId = in.readInt();
        programTourNameTH = in.readString();
        programTourNameEN = in.readString();
        programTourNameLO = in.readString();
        programTourNameZH = in.readString();
        interestingThingsTH = in.readString();
        interestingThingsEN = in.readString();
        interestingThingsLO = in.readString();
        interestingThingsZH = in.readString();
        createdDTTM = in.readString();
        updatedDTTM = in.readString();
        countView = in.readString();
        pathCovers = in.readString();
        coverItem_state = in.readString();
        isPublishStatus = in.readInt();
        isActive = in.readInt();
        isOfficial = in.readByte() != 0;
        isSaveDraft = in.readString();
        likesModel = in.readParcelable(LikesModel.class.getClassLoader());
        menuItemModel = in.readParcelable(MenuItemModel.class.getClassLoader());
        coverItemsModel = in.readParcelable(CoverItemsModel.class.getClassLoader());
        userModel = in.readParcelable(UserModel.class.getClassLoader());
        categoryModel = in.readParcelable(CategoryModel.class.getClassLoader());
        subCategoryModel = in.readParcelable(SubCategoryModel.class.getClassLoader());
        publishStatusModel = in.readParcelable(PublishStatusModel.class.getClassLoader());
        datesTripModelArrayList = in.createTypedArrayList(DatesTripModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(programTourId);
        dest.writeString(programTourNameTH);
        dest.writeString(programTourNameEN);
        dest.writeString(programTourNameLO);
        dest.writeString(programTourNameZH);
        dest.writeString(interestingThingsTH);
        dest.writeString(interestingThingsEN);
        dest.writeString(interestingThingsLO);
        dest.writeString(interestingThingsZH);
        dest.writeString(createdDTTM);
        dest.writeString(updatedDTTM);
        dest.writeString(countView);
        dest.writeString(pathCovers);
        dest.writeString(coverItem_state);
        dest.writeInt(isPublishStatus);
        dest.writeInt(isActive);
        dest.writeByte((byte) (isOfficial ? 1 : 0));
        dest.writeString(isSaveDraft);
        dest.writeParcelable(likesModel, flags);
        dest.writeParcelable(menuItemModel, flags);
        dest.writeParcelable(coverItemsModel, flags);
        dest.writeParcelable(userModel, flags);
        dest.writeParcelable(categoryModel, flags);
        dest.writeParcelable(subCategoryModel, flags);
        dest.writeParcelable(publishStatusModel, flags);
        dest.writeTypedList(datesTripModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProgramTourModel> CREATOR = new Creator<ProgramTourModel>() {
        @Override
        public ProgramTourModel createFromParcel(Parcel in) {
            return new ProgramTourModel(in);
        }

        @Override
        public ProgramTourModel[] newArray(int size) {
            return new ProgramTourModel[size];
        }
    };
}
