package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProvinceGroupModel implements Parcelable {

    private int pgId;
    private String pgTH;
    private String pgEN;
    private String pgZH;
    private String pgLO;
    private int isActive;
    private int isDelete;
    private String createDatetime;
    private String updateDatetime;

    public ProvinceGroupModel() {

    }

    public int getPgId() {
        return pgId;
    }

    public void setPgId(int pgId) {
        this.pgId = pgId;
    }

    public String getPgTH() {
        return pgTH;
    }

    public void setPgTH(String pgTH) {
        this.pgTH = pgTH;
    }

    public String getPgEN() {
        return pgEN;
    }

    public void setPgEN(String pgEN) {
        this.pgEN = pgEN;
    }

    public String getPgZH() {
        return pgZH;
    }

    public void setPgZH(String pgZH) {
        this.pgZH = pgZH;
    }

    public String getPgLO() {
        return pgLO;
    }

    public void setPgLO(String pgLO) {
        this.pgLO = pgLO;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(String updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    protected ProvinceGroupModel(Parcel in) {
        pgId = in.readInt();
        pgTH = in.readString();
        pgEN = in.readString();
        pgZH = in.readString();
        pgLO = in.readString();
        isActive = in.readInt();
        isDelete = in.readInt();
        createDatetime = in.readString();
        updateDatetime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(pgId);
        dest.writeString(pgTH);
        dest.writeString(pgEN);
        dest.writeString(pgZH);
        dest.writeString(pgLO);
        dest.writeInt(isActive);
        dest.writeInt(isDelete);
        dest.writeString(createDatetime);
        dest.writeString(updateDatetime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProvinceGroupModel> CREATOR = new Creator<ProvinceGroupModel>() {
        @Override
        public ProvinceGroupModel createFromParcel(Parcel in) {
            return new ProvinceGroupModel(in);
        }

        @Override
        public ProvinceGroupModel[] newArray(int size) {
            return new ProvinceGroupModel[size];
        }
    };
}
