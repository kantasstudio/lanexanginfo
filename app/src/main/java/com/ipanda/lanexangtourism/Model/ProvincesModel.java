package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ProvincesModel implements Parcelable {

    private int provincesId;
    private int provincesCode;
    private String provincesTH;
    private String provincesEN;
    private String provincesLO;
    private String provincesZH;
    private DistrictsModel districtsModel;
    ArrayList<DistrictsModel> districtsModelArrayList;

    public ProvincesModel() {

    }

    public ProvincesModel(int provincesId) {
        this.provincesId = provincesId;
    }

    public ProvincesModel(String provincesTH, String provincesEN, String provincesLO, String provincesZH) {
        this.provincesTH = provincesTH;
        this.provincesEN = provincesEN;
        this.provincesLO = provincesLO;
        this.provincesZH = provincesZH;
    }

    public int getProvincesId() {
        return provincesId;
    }

    public void setProvincesId(int provincesId) {
        this.provincesId = provincesId;
    }

    public int getProvincesCode() {
        return provincesCode;
    }

    public void setProvincesCode(int provincesCode) {
        this.provincesCode = provincesCode;
    }

    public String getProvincesTH() {
        return provincesTH;
    }

    public void setProvincesTH(String provincesTH) {
        this.provincesTH = provincesTH;
    }

    public String getProvincesEN() {
        return provincesEN;
    }

    public void setProvincesEN(String provincesEN) {
        this.provincesEN = provincesEN;
    }

    public String getProvincesLO() {
        return provincesLO;
    }

    public void setProvincesLO(String provincesLO) {
        this.provincesLO = provincesLO;
    }

    public String getProvincesZH() {
        return provincesZH;
    }

    public void setProvincesZH(String provincesZH) {
        this.provincesZH = provincesZH;
    }

    public DistrictsModel getDistrictsModel() {
        return districtsModel;
    }

    public void setDistrictsModel(DistrictsModel districtsModel) {
        this.districtsModel = districtsModel;
    }

    public ArrayList<DistrictsModel> getDistrictsModelArrayList() {
        return districtsModelArrayList;
    }

    public void setDistrictsModelArrayList(ArrayList<DistrictsModel> districtsModelArrayList) {
        this.districtsModelArrayList = districtsModelArrayList;
    }

    protected ProvincesModel(Parcel in) {
        provincesId = in.readInt();
        provincesCode = in.readInt();
        provincesTH = in.readString();
        provincesEN = in.readString();
        provincesLO = in.readString();
        provincesZH = in.readString();
        districtsModel = in.readParcelable(DistrictsModel.class.getClassLoader());
        districtsModelArrayList = in.createTypedArrayList(DistrictsModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(provincesId);
        dest.writeInt(provincesCode);
        dest.writeString(provincesTH);
        dest.writeString(provincesEN);
        dest.writeString(provincesLO);
        dest.writeString(provincesZH);
        dest.writeParcelable(districtsModel, flags);
        dest.writeTypedList(districtsModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProvincesModel> CREATOR = new Creator<ProvincesModel>() {
        @Override
        public ProvincesModel createFromParcel(Parcel in) {
            return new ProvincesModel(in);
        }

        @Override
        public ProvincesModel[] newArray(int size) {
            return new ProvincesModel[size];
        }
    };
}
