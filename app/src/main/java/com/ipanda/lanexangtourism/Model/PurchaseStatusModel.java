package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class PurchaseStatusModel implements Parcelable {

    private int statusId;
    private String statusTH;
    private String statusEN;
    private String statusLO;
    private String statusZH;

    public PurchaseStatusModel() {

    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatusTH() {
        return statusTH;
    }

    public void setStatusTH(String statusTH) {
        this.statusTH = statusTH;
    }

    public String getStatusEN() {
        return statusEN;
    }

    public void setStatusEN(String statusEN) {
        this.statusEN = statusEN;
    }

    public String getStatusLO() {
        return statusLO;
    }

    public void setStatusLO(String statusLO) {
        this.statusLO = statusLO;
    }

    public String getStatusZH() {
        return statusZH;
    }

    public void setStatusZH(String statusZH) {
        this.statusZH = statusZH;
    }

    protected PurchaseStatusModel(Parcel in) {
        statusId = in.readInt();
        statusTH = in.readString();
        statusEN = in.readString();
        statusLO = in.readString();
        statusZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusId);
        dest.writeString(statusTH);
        dest.writeString(statusEN);
        dest.writeString(statusLO);
        dest.writeString(statusZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PurchaseStatusModel> CREATOR = new Creator<PurchaseStatusModel>() {
        @Override
        public PurchaseStatusModel createFromParcel(Parcel in) {
            return new PurchaseStatusModel(in);
        }

        @Override
        public PurchaseStatusModel[] newArray(int size) {
            return new PurchaseStatusModel[size];
        }
    };
}
