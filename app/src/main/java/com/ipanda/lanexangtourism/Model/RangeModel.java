package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class RangeModel implements Parcelable {

    private float minimum;
    private float maximum;

    public RangeModel() {

    }

    public float getMinimum() {
        return minimum;
    }

    public void setMinimum(float minimum) {
        this.minimum = minimum;
    }

    public float getMaximum() {
        return maximum;
    }

    public void setMaximum(float maximum) {
        this.maximum = maximum;
    }

    protected RangeModel(Parcel in) {
        minimum = in.readFloat();
        maximum = in.readFloat();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(minimum);
        dest.writeFloat(maximum);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RangeModel> CREATOR = new Creator<RangeModel>() {
        @Override
        public RangeModel createFromParcel(Parcel in) {
            return new RangeModel(in);
        }

        @Override
        public RangeModel[] newArray(int size) {
            return new RangeModel[size];
        }
    };
}
