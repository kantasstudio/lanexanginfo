package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class RateModel implements Parcelable {

    private double rateUSD;

    private double rateCNY;

    public RateModel() {

    }

    protected RateModel(Parcel in) {
        rateUSD = in.readDouble();
        rateCNY = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(rateUSD);
        dest.writeDouble(rateCNY);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RateModel> CREATOR = new Creator<RateModel>() {
        @Override
        public RateModel createFromParcel(Parcel in) {
            return new RateModel(in);
        }

        @Override
        public RateModel[] newArray(int size) {
            return new RateModel[size];
        }
    };

    public double getRateUSD() {
        return rateUSD;
    }

    public void setRateUSD(double rateUSD) {
        this.rateUSD = rateUSD;
    }

    public double getRateCNY() {
        return rateCNY;
    }

    public void setRateCNY(double rateCNY) {
        this.rateCNY = rateCNY;
    }
}
