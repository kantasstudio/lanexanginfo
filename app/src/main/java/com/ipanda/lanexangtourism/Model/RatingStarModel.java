package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class RatingStarModel implements Parcelable {

    private int averageReview;
    private int countReview;
    private int oneStar;
    private int twoStar;
    private int threeStar;
    private int fourStar;
    private int fiveStar;
    private ArrayList<ReviewModel> reviewModelArrayList;

    public RatingStarModel() {

    }

    public int getAverageReview() {
        return averageReview;
    }

    public void setAverageReview(int averageReview) {
        this.averageReview = averageReview;
    }

    public int getCountReview() {
        return countReview;
    }

    public void setCountReview(int countReview) {
        this.countReview = countReview;
    }

    public int getOneStar() {
        return oneStar;
    }

    public void setOneStar(int oneStar) {
        this.oneStar = oneStar;
    }

    public int getTwoStar() {
        return twoStar;
    }

    public void setTwoStar(int twoStar) {
        this.twoStar = twoStar;
    }

    public int getThreeStar() {
        return threeStar;
    }

    public void setThreeStar(int threeStar) {
        this.threeStar = threeStar;
    }

    public int getFourStar() {
        return fourStar;
    }

    public void setFourStar(int fourStar) {
        this.fourStar = fourStar;
    }

    public int getFiveStar() {
        return fiveStar;
    }

    public void setFiveStar(int fiveStar) {
        this.fiveStar = fiveStar;
    }

    public ArrayList<ReviewModel> getReviewModelArrayList() {
        return reviewModelArrayList;
    }

    public void setReviewModelArrayList(ArrayList<ReviewModel> reviewModelArrayList) {
        this.reviewModelArrayList = reviewModelArrayList;
    }

    protected RatingStarModel(Parcel in) {
        averageReview = in.readInt();
        countReview = in.readInt();
        oneStar = in.readInt();
        twoStar = in.readInt();
        threeStar = in.readInt();
        fourStar = in.readInt();
        fiveStar = in.readInt();
        reviewModelArrayList = in.createTypedArrayList(ReviewModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(averageReview);
        dest.writeInt(countReview);
        dest.writeInt(oneStar);
        dest.writeInt(twoStar);
        dest.writeInt(threeStar);
        dest.writeInt(fourStar);
        dest.writeInt(fiveStar);
        dest.writeTypedList(reviewModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RatingStarModel> CREATOR = new Creator<RatingStarModel>() {
        @Override
        public RatingStarModel createFromParcel(Parcel in) {
            return new RatingStarModel(in);
        }

        @Override
        public RatingStarModel[] newArray(int size) {
            return new RatingStarModel[size];
        }
    };
}
