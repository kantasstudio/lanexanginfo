package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ReviewModel implements Parcelable {

    private int reviewId;
    private String reviewTimestamp;
    private String reviewText;
    private int reviewRating;
    private ItemsModel itemsModel;
    private UserModel userModel;
    private ProgramTourModel programTourModel;

    public ReviewModel() {

    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public String getReviewTimestamp() {
        return reviewTimestamp;
    }

    public void setReviewTimestamp(String reviewTimestamp) {
        this.reviewTimestamp = reviewTimestamp;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public int getReviewRating() {
        return reviewRating;
    }

    public void setReviewRating(int reviewRating) {
        this.reviewRating = reviewRating;
    }

    public ItemsModel getItemsModel() {
        return itemsModel;
    }

    public void setItemsModel(ItemsModel itemsModel) {
        this.itemsModel = itemsModel;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public ProgramTourModel getProgramTourModel() {
        return programTourModel;
    }

    public void setProgramTourModel(ProgramTourModel programTourModel) {
        this.programTourModel = programTourModel;
    }

    protected ReviewModel(Parcel in) {
        reviewId = in.readInt();
        reviewTimestamp = in.readString();
        reviewText = in.readString();
        reviewRating = in.readInt();
        itemsModel = in.readParcelable(ItemsModel.class.getClassLoader());
        userModel = in.readParcelable(UserModel.class.getClassLoader());
        programTourModel = in.readParcelable(ProgramTourModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(reviewId);
        dest.writeString(reviewTimestamp);
        dest.writeString(reviewText);
        dest.writeInt(reviewRating);
        dest.writeParcelable(itemsModel, flags);
        dest.writeParcelable(userModel, flags);
        dest.writeParcelable(programTourModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReviewModel> CREATOR = new Creator<ReviewModel>() {
        @Override
        public ReviewModel createFromParcel(Parcel in) {
            return new ReviewModel(in);
        }

        @Override
        public ReviewModel[] newArray(int size) {
            return new ReviewModel[size];
        }
    };
}
