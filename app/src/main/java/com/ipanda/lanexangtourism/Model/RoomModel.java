package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class RoomModel implements Parcelable {

    private int roomId;
    private String topicTH;
    private String topicEN;
    private String topicLO;
    private String topicZH;
    private String descriptionTH;
    private String descriptionEN;
    private String descriptionLO;
    private String descriptionZH;
    private String detailsTH;
    private String detailsEN;
    private String detailsLO;
    private String detailsZH;
    private Double price;
    private int breakFast;
    private ArrayList<RoomPictureModel> roomPictureModelArrayList;

    public RoomModel() {

    }

    protected RoomModel(Parcel in) {
        roomId = in.readInt();
        topicTH = in.readString();
        topicEN = in.readString();
        topicLO = in.readString();
        topicZH = in.readString();
        descriptionTH = in.readString();
        descriptionEN = in.readString();
        descriptionLO = in.readString();
        descriptionZH = in.readString();
        detailsTH = in.readString();
        detailsEN = in.readString();
        detailsLO = in.readString();
        detailsZH = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readDouble();
        }
        breakFast = in.readInt();
        roomPictureModelArrayList = in.createTypedArrayList(RoomPictureModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(roomId);
        dest.writeString(topicTH);
        dest.writeString(topicEN);
        dest.writeString(topicLO);
        dest.writeString(topicZH);
        dest.writeString(descriptionTH);
        dest.writeString(descriptionEN);
        dest.writeString(descriptionLO);
        dest.writeString(descriptionZH);
        dest.writeString(detailsTH);
        dest.writeString(detailsEN);
        dest.writeString(detailsLO);
        dest.writeString(detailsZH);
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(price);
        }
        dest.writeInt(breakFast);
        dest.writeTypedList(roomPictureModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RoomModel> CREATOR = new Creator<RoomModel>() {
        @Override
        public RoomModel createFromParcel(Parcel in) {
            return new RoomModel(in);
        }

        @Override
        public RoomModel[] newArray(int size) {
            return new RoomModel[size];
        }
    };

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getTopicTH() {
        return topicTH;
    }

    public void setTopicTH(String topicTH) {
        this.topicTH = topicTH;
    }

    public String getTopicEN() {
        return topicEN;
    }

    public void setTopicEN(String topicEN) {
        this.topicEN = topicEN;
    }

    public String getTopicLO() {
        return topicLO;
    }

    public void setTopicLO(String topicLO) {
        this.topicLO = topicLO;
    }

    public String getTopicZH() {
        return topicZH;
    }

    public void setTopicZH(String topicZH) {
        this.topicZH = topicZH;
    }

    public String getDescriptionTH() {
        return descriptionTH;
    }

    public void setDescriptionTH(String descriptionTH) {
        this.descriptionTH = descriptionTH;
    }

    public String getDescriptionEN() {
        return descriptionEN;
    }

    public void setDescriptionEN(String descriptionEN) {
        this.descriptionEN = descriptionEN;
    }

    public String getDescriptionLO() {
        return descriptionLO;
    }

    public void setDescriptionLO(String descriptionLO) {
        this.descriptionLO = descriptionLO;
    }

    public String getDescriptionZH() {
        return descriptionZH;
    }

    public void setDescriptionZH(String descriptionZH) {
        this.descriptionZH = descriptionZH;
    }

    public String getDetailsTH() {
        return detailsTH;
    }

    public void setDetailsTH(String detailsTH) {
        this.detailsTH = detailsTH;
    }

    public String getDetailsEN() {
        return detailsEN;
    }

    public void setDetailsEN(String detailsEN) {
        this.detailsEN = detailsEN;
    }

    public String getDetailsLO() {
        return detailsLO;
    }

    public void setDetailsLO(String detailsLO) {
        this.detailsLO = detailsLO;
    }

    public String getDetailsZH() {
        return detailsZH;
    }

    public void setDetailsZH(String detailsZH) {
        this.detailsZH = detailsZH;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getBreakFast() {
        return breakFast;
    }

    public void setBreakFast(int breakFast) {
        this.breakFast = breakFast;
    }

    public ArrayList<RoomPictureModel> getRoomPictureModelArrayList() {
        return roomPictureModelArrayList;
    }

    public void setRoomPictureModelArrayList(ArrayList<RoomPictureModel> roomPictureModelArrayList) {
        this.roomPictureModelArrayList = roomPictureModelArrayList;
    }
}
