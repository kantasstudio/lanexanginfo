package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class RoomPictureModel implements Parcelable {

    private int pictureId;
    private String picturePaths;


    public RoomPictureModel() {

    }

    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public String getPicturePaths() {
        return picturePaths;
    }

    public void setPicturePaths(String picturePaths) {
        this.picturePaths = picturePaths;
    }

    protected RoomPictureModel(Parcel in) {
        pictureId = in.readInt();
        picturePaths = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(pictureId);
        dest.writeString(picturePaths);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RoomPictureModel> CREATOR = new Creator<RoomPictureModel>() {
        @Override
        public RoomPictureModel createFromParcel(Parcel in) {
            return new RoomPictureModel(in);
        }

        @Override
        public RoomPictureModel[] newArray(int size) {
            return new RoomPictureModel[size];
        }
    };
}
