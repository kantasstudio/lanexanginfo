package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ScreenBookingModel implements Parcelable {

    private int id;
    private String screenImage;
    private MenuItemModel menuItemModel;
    private ItemsModel itemsModel;
    private ProgramTourModel programTourModel;
    private BusinessModel businessModel;

    public ScreenBookingModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScreenImage() {
        return screenImage;
    }

    public void setScreenImage(String screenImage) {
        this.screenImage = screenImage;
    }

    public MenuItemModel getMenuItemModel() {
        return menuItemModel;
    }

    public void setMenuItemModel(MenuItemModel menuItemModel) {
        this.menuItemModel = menuItemModel;
    }

    public ItemsModel getItemsModel() {
        return itemsModel;
    }

    public void setItemsModel(ItemsModel itemsModel) {
        this.itemsModel = itemsModel;
    }

    public ProgramTourModel getProgramTourModel() {
        return programTourModel;
    }

    public void setProgramTourModel(ProgramTourModel programTourModel) {
        this.programTourModel = programTourModel;
    }

    public BusinessModel getBusinessModel() {
        return businessModel;
    }

    public void setBusinessModel(BusinessModel businessModel) {
        this.businessModel = businessModel;
    }

    protected ScreenBookingModel(Parcel in) {
        id = in.readInt();
        screenImage = in.readString();
        menuItemModel = in.readParcelable(MenuItemModel.class.getClassLoader());
        itemsModel = in.readParcelable(ItemsModel.class.getClassLoader());
        programTourModel = in.readParcelable(ProgramTourModel.class.getClassLoader());
        businessModel = in.readParcelable(BusinessModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(screenImage);
        dest.writeParcelable(menuItemModel, flags);
        dest.writeParcelable(itemsModel, flags);
        dest.writeParcelable(programTourModel, flags);
        dest.writeParcelable(businessModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ScreenBookingModel> CREATOR = new Creator<ScreenBookingModel>() {
        @Override
        public ScreenBookingModel createFromParcel(Parcel in) {
            return new ScreenBookingModel(in);
        }

        @Override
        public ScreenBookingModel[] newArray(int size) {
            return new ScreenBookingModel[size];
        }
    };
}
