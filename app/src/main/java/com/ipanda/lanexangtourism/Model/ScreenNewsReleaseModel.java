package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ScreenNewsReleaseModel implements Parcelable {

    private int id;
    private String screenImage;
    private String linkURL;

    public ScreenNewsReleaseModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScreenImage() {
        return screenImage;
    }

    public void setScreenImage(String screenImage) {
        this.screenImage = screenImage;
    }

    public String getLinkURL() {
        return linkURL;
    }

    public void setLinkURL(String linkURL) {
        this.linkURL = linkURL;
    }

    protected ScreenNewsReleaseModel(Parcel in) {
        id = in.readInt();
        screenImage = in.readString();
        linkURL = in.readString();
    }

    public static final Creator<ScreenNewsReleaseModel> CREATOR = new Creator<ScreenNewsReleaseModel>() {
        @Override
        public ScreenNewsReleaseModel createFromParcel(Parcel in) {
            return new ScreenNewsReleaseModel(in);
        }

        @Override
        public ScreenNewsReleaseModel[] newArray(int size) {
            return new ScreenNewsReleaseModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(screenImage);
        dest.writeString(linkURL);
    }
}
