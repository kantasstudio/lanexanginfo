package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ScreenOfficialModel implements Parcelable {

    private int id;
    private String screenImage;
    private ProgramTourModel programTourModel;

    public ScreenOfficialModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScreenImage() {
        return screenImage;
    }

    public void setScreenImage(String screenImage) {
        this.screenImage = screenImage;
    }

    public ProgramTourModel getProgramTourModel() {
        return programTourModel;
    }

    public void setProgramTourModel(ProgramTourModel programTourModel) {
        this.programTourModel = programTourModel;
    }

    protected ScreenOfficialModel(Parcel in) {
        id = in.readInt();
        screenImage = in.readString();
        programTourModel = in.readParcelable(ProgramTourModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(screenImage);
        dest.writeParcelable(programTourModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ScreenOfficialModel> CREATOR = new Creator<ScreenOfficialModel>() {
        @Override
        public ScreenOfficialModel createFromParcel(Parcel in) {
            return new ScreenOfficialModel(in);
        }

        @Override
        public ScreenOfficialModel[] newArray(int size) {
            return new ScreenOfficialModel[size];
        }
    };
}
