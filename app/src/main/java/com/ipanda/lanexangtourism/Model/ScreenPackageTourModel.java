package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ScreenPackageTourModel implements Parcelable {

    private int id;
    private String screenImage;
    private ItemsModel itemsModel;

    public ScreenPackageTourModel() {

    }

    protected ScreenPackageTourModel(Parcel in) {
        id = in.readInt();
        screenImage = in.readString();
        itemsModel = in.readParcelable(ItemsModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(screenImage);
        dest.writeParcelable(itemsModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ScreenPackageTourModel> CREATOR = new Creator<ScreenPackageTourModel>() {
        @Override
        public ScreenPackageTourModel createFromParcel(Parcel in) {
            return new ScreenPackageTourModel(in);
        }

        @Override
        public ScreenPackageTourModel[] newArray(int size) {
            return new ScreenPackageTourModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScreenImage() {
        return screenImage;
    }

    public void setScreenImage(String screenImage) {
        this.screenImage = screenImage;
    }

    public ItemsModel getItemsModel() {
        return itemsModel;
    }

    public void setItemsModel(ItemsModel itemsModel) {
        this.itemsModel = itemsModel;
    }
}
