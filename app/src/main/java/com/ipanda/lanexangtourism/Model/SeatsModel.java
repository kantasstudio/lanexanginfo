package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class SeatsModel implements Parcelable {

    private String carSeats;
    private boolean isFilter;

    public SeatsModel() {

    }

    public String getCarSeats() {
        return carSeats;
    }

    public void setCarSeats(String carSeats) {
        this.carSeats = carSeats;
    }

    public boolean isFilter() {
        return isFilter;
    }

    public void setFilter(boolean filter) {
        isFilter = filter;
    }

    protected SeatsModel(Parcel in) {
        carSeats = in.readString();
        isFilter = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(carSeats);
        dest.writeByte((byte) (isFilter ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SeatsModel> CREATOR = new Creator<SeatsModel>() {
        @Override
        public SeatsModel createFromParcel(Parcel in) {
            return new SeatsModel(in);
        }

        @Override
        public SeatsModel[] newArray(int size) {
            return new SeatsModel[size];
        }
    };
}
