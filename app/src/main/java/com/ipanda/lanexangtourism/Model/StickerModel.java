package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class StickerModel implements Parcelable {

    private int stickerId;
    private String stickerTH;
    private String stickerEN;
    private String stickerLO;
    private String stickerZH;
    private String stickerIcon;
    private boolean isSelected;
    private ArrayList<StickerPathsModel> stickerPathsModelArrayList;

    public StickerModel() {

    }

    public StickerModel(String stickerTH) {
        this.stickerTH = stickerTH;
    }

    protected StickerModel(Parcel in) {
        stickerId = in.readInt();
        stickerTH = in.readString();
        stickerEN = in.readString();
        stickerLO = in.readString();
        stickerZH = in.readString();
        stickerIcon = in.readString();
        isSelected = in.readByte() != 0;
        stickerPathsModelArrayList = in.createTypedArrayList(StickerPathsModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(stickerId);
        dest.writeString(stickerTH);
        dest.writeString(stickerEN);
        dest.writeString(stickerLO);
        dest.writeString(stickerZH);
        dest.writeString(stickerIcon);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeTypedList(stickerPathsModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StickerModel> CREATOR = new Creator<StickerModel>() {
        @Override
        public StickerModel createFromParcel(Parcel in) {
            return new StickerModel(in);
        }

        @Override
        public StickerModel[] newArray(int size) {
            return new StickerModel[size];
        }
    };

    public int getStickerId() {
        return stickerId;
    }

    public void setStickerId(int stickerId) {
        this.stickerId = stickerId;
    }

    public String getStickerTH() {
        return stickerTH;
    }

    public void setStickerTH(String stickerTH) {
        this.stickerTH = stickerTH;
    }

    public String getStickerEN() {
        return stickerEN;
    }

    public void setStickerEN(String stickerEN) {
        this.stickerEN = stickerEN;
    }

    public String getStickerLO() {
        return stickerLO;
    }

    public void setStickerLO(String stickerLO) {
        this.stickerLO = stickerLO;
    }

    public String getStickerZH() {
        return stickerZH;
    }

    public void setStickerZH(String stickerZH) {
        this.stickerZH = stickerZH;
    }

    public String getStickerIcon() {
        return stickerIcon;
    }

    public void setStickerIcon(String stickerIcon) {
        this.stickerIcon = stickerIcon;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ArrayList<StickerPathsModel> getStickerPathsModelArrayList() {
        return stickerPathsModelArrayList;
    }

    public void setStickerPathsModelArrayList(ArrayList<StickerPathsModel> stickerPathsModelArrayList) {
        this.stickerPathsModelArrayList = stickerPathsModelArrayList;
    }
}
