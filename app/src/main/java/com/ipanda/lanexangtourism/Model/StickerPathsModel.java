package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class StickerPathsModel implements Parcelable {

    private int stickerPathsId;
    private String stickerPaths;

    public StickerPathsModel() {
    }

    public StickerPathsModel(String stickerPaths) {
        this.stickerPaths = stickerPaths;
    }

    public int getStickerPathsId() {
        return stickerPathsId;
    }

    public void setStickerPathsId(int stickerPathsId) {
        this.stickerPathsId = stickerPathsId;
    }

    public String getStickerPaths() {
        return stickerPaths;
    }

    public void setStickerPaths(String stickerPaths) {
        this.stickerPaths = stickerPaths;
    }

    protected StickerPathsModel(Parcel in) {
        stickerPathsId = in.readInt();
        stickerPaths = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(stickerPathsId);
        dest.writeString(stickerPaths);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StickerPathsModel> CREATOR = new Creator<StickerPathsModel>() {
        @Override
        public StickerPathsModel createFromParcel(Parcel in) {
            return new StickerPathsModel(in);
        }

        @Override
        public StickerPathsModel[] newArray(int size) {
            return new StickerPathsModel[size];
        }
    };
}
