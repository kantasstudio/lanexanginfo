package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubCategoryModel implements Parcelable {

    private int categoryId;
    private String categoryTitle;
    private String categoryTH;
    private String categoryEN;
    private String categoryLO;
    private String categoryZH;
    private String categoryPath;
    private boolean isFilter;

    public SubCategoryModel() {

    }

    public SubCategoryModel(int categoryId) {
        this.categoryId = categoryId;
    }

    public SubCategoryModel(String categoryTH, String categoryEN, String categoryLO, String categoryZH) {
        this.categoryTH = categoryTH;
        this.categoryEN = categoryEN;
        this.categoryLO = categoryLO;
        this.categoryZH = categoryZH;
    }

    public SubCategoryModel(int categoryId, String categoryTitle, boolean isFilter) {
        this.categoryId = categoryId;
        this.categoryTitle = categoryTitle;
        this.isFilter = isFilter;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryTH() {
        return categoryTH;
    }

    public void setCategoryTH(String categoryTH) {
        this.categoryTH = categoryTH;
    }

    public String getCategoryEN() {
        return categoryEN;
    }

    public void setCategoryEN(String categoryEN) {
        this.categoryEN = categoryEN;
    }

    public String getCategoryLO() {
        return categoryLO;
    }

    public void setCategoryLO(String categoryLO) {
        this.categoryLO = categoryLO;
    }

    public String getCategoryZH() {
        return categoryZH;
    }

    public void setCategoryZH(String categoryZH) {
        this.categoryZH = categoryZH;
    }

    public String getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
    }

    public boolean isFilter() {
        return isFilter;
    }

    public void setFilter(boolean filter) {
        isFilter = filter;
    }

    protected SubCategoryModel(Parcel in) {
        categoryId = in.readInt();
        categoryTitle = in.readString();
        categoryTH = in.readString();
        categoryEN = in.readString();
        categoryLO = in.readString();
        categoryZH = in.readString();
        categoryPath = in.readString();
        isFilter = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(categoryId);
        dest.writeString(categoryTitle);
        dest.writeString(categoryTH);
        dest.writeString(categoryEN);
        dest.writeString(categoryLO);
        dest.writeString(categoryZH);
        dest.writeString(categoryPath);
        dest.writeByte((byte) (isFilter ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubCategoryModel> CREATOR = new Creator<SubCategoryModel>() {
        @Override
        public SubCategoryModel createFromParcel(Parcel in) {
            return new SubCategoryModel(in);
        }

        @Override
        public SubCategoryModel[] newArray(int size) {
            return new SubCategoryModel[size];
        }
    };
}
