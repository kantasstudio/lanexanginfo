package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubDistrictsModel implements Parcelable {

    private int subDistrictsId;
    private int subDistrictsCode;
    private String subDistrictsTH;
    private String subDistrictsEN;
    private String subDistrictsLO;
    private String subDistrictsZH;

    public SubDistrictsModel() {

    }

    public SubDistrictsModel(int subDistrictsId) {
        this.subDistrictsId = subDistrictsId;
    }

    public SubDistrictsModel(String subDistrictsTH, String subDistrictsEN, String subDistrictsLO, String subDistrictsZH) {
        this.subDistrictsTH = subDistrictsTH;
        this.subDistrictsEN = subDistrictsEN;
        this.subDistrictsLO = subDistrictsLO;
        this.subDistrictsZH = subDistrictsZH;
    }

    public int getSubDistrictsId() {
        return subDistrictsId;
    }

    public void setSubDistrictsId(int subDistrictsId) {
        this.subDistrictsId = subDistrictsId;
    }

    public int getSubDistrictsCode() {
        return subDistrictsCode;
    }

    public void setSubDistrictsCode(int subDistrictsCode) {
        this.subDistrictsCode = subDistrictsCode;
    }

    public String getSubDistrictsTH() {
        return subDistrictsTH;
    }

    public void setSubDistrictsTH(String subDistrictsTH) {
        this.subDistrictsTH = subDistrictsTH;
    }

    public String getSubDistrictsEN() {
        return subDistrictsEN;
    }

    public void setSubDistrictsEN(String subDistrictsEN) {
        this.subDistrictsEN = subDistrictsEN;
    }

    public String getSubDistrictsLO() {
        return subDistrictsLO;
    }

    public void setSubDistrictsLO(String subDistrictsLO) {
        this.subDistrictsLO = subDistrictsLO;
    }

    public String getSubDistrictsZH() {
        return subDistrictsZH;
    }

    public void setSubDistrictsZH(String subDistrictsZH) {
        this.subDistrictsZH = subDistrictsZH;
    }

    protected SubDistrictsModel(Parcel in) {
        subDistrictsId = in.readInt();
        subDistrictsCode = in.readInt();
        subDistrictsTH = in.readString();
        subDistrictsEN = in.readString();
        subDistrictsLO = in.readString();
        subDistrictsZH = in.readString();
    }

    public static final Creator<SubDistrictsModel> CREATOR = new Creator<SubDistrictsModel>() {
        @Override
        public SubDistrictsModel createFromParcel(Parcel in) {
            return new SubDistrictsModel(in);
        }

        @Override
        public SubDistrictsModel[] newArray(int size) {
            return new SubDistrictsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(subDistrictsId);
        dest.writeInt(subDistrictsCode);
        dest.writeString(subDistrictsTH);
        dest.writeString(subDistrictsEN);
        dest.writeString(subDistrictsLO);
        dest.writeString(subDistrictsZH);
    }
}
