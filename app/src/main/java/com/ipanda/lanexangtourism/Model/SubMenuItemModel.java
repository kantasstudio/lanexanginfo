package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubMenuItemModel implements Parcelable {

    private int subMenuItemId;
    private String subMenuItemTH;
    private String subMenuItemEN;
    private String subMenuItemLO;
    private String subMenuItemZH;

    public SubMenuItemModel() {

    }

    public int getSubMenuItemId() {
        return subMenuItemId;
    }

    public void setSubMenuItemId(int subMenuItemId) {
        this.subMenuItemId = subMenuItemId;
    }

    public String getSubMenuItemTH() {
        return subMenuItemTH;
    }

    public void setSubMenuItemTH(String subMenuItemTH) {
        this.subMenuItemTH = subMenuItemTH;
    }

    public String getSubMenuItemEN() {
        return subMenuItemEN;
    }

    public void setSubMenuItemEN(String subMenuItemEN) {
        this.subMenuItemEN = subMenuItemEN;
    }

    public String getSubMenuItemLO() {
        return subMenuItemLO;
    }

    public void setSubMenuItemLO(String subMenuItemLO) {
        this.subMenuItemLO = subMenuItemLO;
    }

    public String getSubMenuItemZH() {
        return subMenuItemZH;
    }

    public void setSubMenuItemZH(String subMenuItemZH) {
        this.subMenuItemZH = subMenuItemZH;
    }

    protected SubMenuItemModel(Parcel in) {
        subMenuItemId = in.readInt();
        subMenuItemTH = in.readString();
        subMenuItemEN = in.readString();
        subMenuItemLO = in.readString();
        subMenuItemZH = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(subMenuItemId);
        dest.writeString(subMenuItemTH);
        dest.writeString(subMenuItemEN);
        dest.writeString(subMenuItemLO);
        dest.writeString(subMenuItemZH);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubMenuItemModel> CREATOR = new Creator<SubMenuItemModel>() {
        @Override
        public SubMenuItemModel createFromParcel(Parcel in) {
            return new SubMenuItemModel(in);
        }

        @Override
        public SubMenuItemModel[] newArray(int size) {
            return new SubMenuItemModel[size];
        }
    };
}
