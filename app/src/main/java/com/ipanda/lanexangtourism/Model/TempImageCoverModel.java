package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class TempImageCoverModel implements Parcelable {

    private int tempId;
    private String tempPaths;
    private String tempCropData;
    private String tempCropBoxData;
    private String tempConVasData;

    public TempImageCoverModel() {

    }

    public int getTempId() {
        return tempId;
    }

    public void setTempId(int tempId) {
        this.tempId = tempId;
    }

    public String getTempPaths() {
        return tempPaths;
    }

    public void setTempPaths(String tempPaths) {
        this.tempPaths = tempPaths;
    }

    public String getTempCropData() {
        return tempCropData;
    }

    public void setTempCropData(String tempCropData) {
        this.tempCropData = tempCropData;
    }

    public String getTempCropBoxData() {
        return tempCropBoxData;
    }

    public void setTempCropBoxData(String tempCropBoxData) {
        this.tempCropBoxData = tempCropBoxData;
    }

    public String getTempConVasData() {
        return tempConVasData;
    }

    public void setTempConVasData(String tempConVasData) {
        this.tempConVasData = tempConVasData;
    }

    protected TempImageCoverModel(Parcel in) {
        tempId = in.readInt();
        tempPaths = in.readString();
        tempCropData = in.readString();
        tempCropBoxData = in.readString();
        tempConVasData = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(tempId);
        dest.writeString(tempPaths);
        dest.writeString(tempCropData);
        dest.writeString(tempCropBoxData);
        dest.writeString(tempConVasData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TempImageCoverModel> CREATOR = new Creator<TempImageCoverModel>() {
        @Override
        public TempImageCoverModel createFromParcel(Parcel in) {
            return new TempImageCoverModel(in);
        }

        @Override
        public TempImageCoverModel[] newArray(int size) {
            return new TempImageCoverModel[size];
        }
    };
}
