package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class TouristAttractionsModel implements Parcelable {

    private int attractionsId;
    private String titleTH;
    private String titleEN;
    private String titleLO;
    private String titleZH;
    private String AttractionsTopicTH;
    private String AttractionsTopicEN;
    private String AttractionsTopicLO;
    private String AttractionsTopicZH;
    private String detailTH;
    private String detailEN;
    private String detailLO;
    private String detailZH;
    private ItemsModel itemsModel;
    private ArrayList<PhotoTourist> photoTouristArrayList;
    private String state;

    public TouristAttractionsModel() {

    }

    public int getAttractionsId() {
        return attractionsId;
    }

    public void setAttractionsId(int attractionsId) {
        this.attractionsId = attractionsId;
    }

    public String getTitleTH() {
        return titleTH;
    }

    public void setTitleTH(String titleTH) {
        this.titleTH = titleTH;
    }

    public String getTitleEN() {
        return titleEN;
    }

    public void setTitleEN(String titleEN) {
        this.titleEN = titleEN;
    }

    public String getTitleLO() {
        return titleLO;
    }

    public void setTitleLO(String titleLO) {
        this.titleLO = titleLO;
    }

    public String getTitleZH() {
        return titleZH;
    }

    public void setTitleZH(String titleZH) {
        this.titleZH = titleZH;
    }

    public String getAttractionsTopicTH() {
        return AttractionsTopicTH;
    }

    public void setAttractionsTopicTH(String attractionsTopicTH) {
        AttractionsTopicTH = attractionsTopicTH;
    }

    public String getAttractionsTopicEN() {
        return AttractionsTopicEN;
    }

    public void setAttractionsTopicEN(String attractionsTopicEN) {
        AttractionsTopicEN = attractionsTopicEN;
    }

    public String getAttractionsTopicLO() {
        return AttractionsTopicLO;
    }

    public void setAttractionsTopicLO(String attractionsTopicLO) {
        AttractionsTopicLO = attractionsTopicLO;
    }

    public String getAttractionsTopicZH() {
        return AttractionsTopicZH;
    }

    public void setAttractionsTopicZH(String attractionsTopicZH) {
        AttractionsTopicZH = attractionsTopicZH;
    }

    public String getDetailTH() {
        return detailTH;
    }

    public void setDetailTH(String detailTH) {
        this.detailTH = detailTH;
    }

    public String getDetailEN() {
        return detailEN;
    }

    public void setDetailEN(String detailEN) {
        this.detailEN = detailEN;
    }

    public String getDetailLO() {
        return detailLO;
    }

    public void setDetailLO(String detailLO) {
        this.detailLO = detailLO;
    }

    public String getDetailZH() {
        return detailZH;
    }

    public void setDetailZH(String detailZH) {
        this.detailZH = detailZH;
    }

    public ItemsModel getItemsModel() {
        return itemsModel;
    }

    public void setItemsModel(ItemsModel itemsModel) {
        this.itemsModel = itemsModel;
    }

    public ArrayList<PhotoTourist> getPhotoTouristArrayList() {
        return photoTouristArrayList;
    }

    public void setPhotoTouristArrayList(ArrayList<PhotoTourist> photoTouristArrayList) {
        this.photoTouristArrayList = photoTouristArrayList;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    protected TouristAttractionsModel(Parcel in) {
        attractionsId = in.readInt();
        titleTH = in.readString();
        titleEN = in.readString();
        titleLO = in.readString();
        titleZH = in.readString();
        AttractionsTopicTH = in.readString();
        AttractionsTopicEN = in.readString();
        AttractionsTopicLO = in.readString();
        AttractionsTopicZH = in.readString();
        detailTH = in.readString();
        detailEN = in.readString();
        detailLO = in.readString();
        detailZH = in.readString();
        itemsModel = in.readParcelable(ItemsModel.class.getClassLoader());
        photoTouristArrayList = in.createTypedArrayList(PhotoTourist.CREATOR);
        state = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(attractionsId);
        dest.writeString(titleTH);
        dest.writeString(titleEN);
        dest.writeString(titleLO);
        dest.writeString(titleZH);
        dest.writeString(AttractionsTopicTH);
        dest.writeString(AttractionsTopicEN);
        dest.writeString(AttractionsTopicLO);
        dest.writeString(AttractionsTopicZH);
        dest.writeString(detailTH);
        dest.writeString(detailEN);
        dest.writeString(detailLO);
        dest.writeString(detailZH);
        dest.writeParcelable(itemsModel, flags);
        dest.writeTypedList(photoTouristArrayList);
        dest.writeString(state);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TouristAttractionsModel> CREATOR = new Creator<TouristAttractionsModel>() {
        @Override
        public TouristAttractionsModel createFromParcel(Parcel in) {
            return new TouristAttractionsModel(in);
        }

        @Override
        public TouristAttractionsModel[] newArray(int size) {
            return new TouristAttractionsModel[size];
        }
    };
}
