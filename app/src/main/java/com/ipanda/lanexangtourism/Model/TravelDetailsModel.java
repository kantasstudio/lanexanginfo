package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class TravelDetailsModel implements Parcelable {

    private int detailsId;
    private String detailsDateTH;
    private String detailsDateEN;
    private String detailsDateLO;
    private String detailsDateZH;
    private String detailsTextTH;
    private String detailsTextEN;
    private String detailsTextLO;
    private String detailsTextZH;
    private ArrayList<TravelPeriodModel> travelPeriodModelArrayList;

    public TravelDetailsModel() {

    }

    public int getDetailsId() {
        return detailsId;
    }

    public void setDetailsId(int detailsId) {
        this.detailsId = detailsId;
    }

    public String getDetailsDateTH() {
        return detailsDateTH;
    }

    public void setDetailsDateTH(String detailsDateTH) {
        this.detailsDateTH = detailsDateTH;
    }

    public String getDetailsDateEN() {
        return detailsDateEN;
    }

    public void setDetailsDateEN(String detailsDateEN) {
        this.detailsDateEN = detailsDateEN;
    }

    public String getDetailsDateLO() {
        return detailsDateLO;
    }

    public void setDetailsDateLO(String detailsDateLO) {
        this.detailsDateLO = detailsDateLO;
    }

    public String getDetailsDateZH() {
        return detailsDateZH;
    }

    public void setDetailsDateZH(String detailsDateZH) {
        this.detailsDateZH = detailsDateZH;
    }

    public String getDetailsTextTH() {
        return detailsTextTH;
    }

    public void setDetailsTextTH(String detailsTextTH) {
        this.detailsTextTH = detailsTextTH;
    }

    public String getDetailsTextEN() {
        return detailsTextEN;
    }

    public void setDetailsTextEN(String detailsTextEN) {
        this.detailsTextEN = detailsTextEN;
    }

    public String getDetailsTextLO() {
        return detailsTextLO;
    }

    public void setDetailsTextLO(String detailsTextLO) {
        this.detailsTextLO = detailsTextLO;
    }

    public String getDetailsTextZH() {
        return detailsTextZH;
    }

    public void setDetailsTextZH(String detailsTextZH) {
        this.detailsTextZH = detailsTextZH;
    }

    public ArrayList<TravelPeriodModel> getTravelPeriodModelArrayList() {
        return travelPeriodModelArrayList;
    }

    public void setTravelPeriodModelArrayList(ArrayList<TravelPeriodModel> travelPeriodModelArrayList) {
        this.travelPeriodModelArrayList = travelPeriodModelArrayList;
    }

    protected TravelDetailsModel(Parcel in) {
        detailsId = in.readInt();
        detailsDateTH = in.readString();
        detailsDateEN = in.readString();
        detailsDateLO = in.readString();
        detailsDateZH = in.readString();
        detailsTextTH = in.readString();
        detailsTextEN = in.readString();
        detailsTextLO = in.readString();
        detailsTextZH = in.readString();
        travelPeriodModelArrayList = in.createTypedArrayList(TravelPeriodModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(detailsId);
        dest.writeString(detailsDateTH);
        dest.writeString(detailsDateEN);
        dest.writeString(detailsDateLO);
        dest.writeString(detailsDateZH);
        dest.writeString(detailsTextTH);
        dest.writeString(detailsTextEN);
        dest.writeString(detailsTextLO);
        dest.writeString(detailsTextZH);
        dest.writeTypedList(travelPeriodModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TravelDetailsModel> CREATOR = new Creator<TravelDetailsModel>() {
        @Override
        public TravelDetailsModel createFromParcel(Parcel in) {
            return new TravelDetailsModel(in);
        }

        @Override
        public TravelDetailsModel[] newArray(int size) {
            return new TravelDetailsModel[size];
        }
    };
}
