package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class TravelPeriodModel implements Parcelable {

    private int periodId;
    private int amount;
    private String periodStart;
    private String periodEnd;
    private int adultPrice;
    private int adultSpecialPrice;
    private int childPrice;
    private int childSpecialPrice;
    private String periodStatus;
    private boolean isChecked = false;

    public TravelPeriodModel() {

    }

    public int getPeriodId() {
        return periodId;
    }

    public void setPeriodId(int periodId) {
        this.periodId = periodId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(String periodStart) {
        this.periodStart = periodStart;
    }

    public String getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(String periodEnd) {
        this.periodEnd = periodEnd;
    }

    public int getAdultPrice() {
        return adultPrice;
    }

    public void setAdultPrice(int adultPrice) {
        this.adultPrice = adultPrice;
    }

    public int getAdultSpecialPrice() {
        return adultSpecialPrice;
    }

    public void setAdultSpecialPrice(int adultSpecialPrice) {
        this.adultSpecialPrice = adultSpecialPrice;
    }

    public int getChildPrice() {
        return childPrice;
    }

    public void setChildPrice(int childPrice) {
        this.childPrice = childPrice;
    }

    public int getChildSpecialPrice() {
        return childSpecialPrice;
    }

    public void setChildSpecialPrice(int childSpecialPrice) {
        this.childSpecialPrice = childSpecialPrice;
    }

    public String getPeriodStatus() {
        return periodStatus;
    }

    public void setPeriodStatus(String periodStatus) {
        this.periodStatus = periodStatus;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    protected TravelPeriodModel(Parcel in) {
        periodId = in.readInt();
        amount = in.readInt();
        periodStart = in.readString();
        periodEnd = in.readString();
        adultPrice = in.readInt();
        adultSpecialPrice = in.readInt();
        childPrice = in.readInt();
        childSpecialPrice = in.readInt();
        periodStatus = in.readString();
        isChecked = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(periodId);
        dest.writeInt(amount);
        dest.writeString(periodStart);
        dest.writeString(periodEnd);
        dest.writeInt(adultPrice);
        dest.writeInt(adultSpecialPrice);
        dest.writeInt(childPrice);
        dest.writeInt(childSpecialPrice);
        dest.writeString(periodStatus);
        dest.writeByte((byte) (isChecked ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TravelPeriodModel> CREATOR = new Creator<TravelPeriodModel>() {
        @Override
        public TravelPeriodModel createFromParcel(Parcel in) {
            return new TravelPeriodModel(in);
        }

        @Override
        public TravelPeriodModel[] newArray(int size) {
            return new TravelPeriodModel[size];
        }
    };
}
