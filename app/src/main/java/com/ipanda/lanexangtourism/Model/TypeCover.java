package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class TypeCover implements Parcelable {

    private int typeCoverId;
    private String typeCover;

    public TypeCover() {

    }

    protected TypeCover(Parcel in) {
        typeCoverId = in.readInt();
        typeCover = in.readString();
    }

    public static final Creator<TypeCover> CREATOR = new Creator<TypeCover>() {
        @Override
        public TypeCover createFromParcel(Parcel in) {
            return new TypeCover(in);
        }

        @Override
        public TypeCover[] newArray(int size) {
            return new TypeCover[size];
        }
    };

    public int getTypeCoverId() {
        return typeCoverId;
    }

    public void setTypeCoverId(int typeCoverId) {
        this.typeCoverId = typeCoverId;
    }

    public String getTypeCover() {
        return typeCover;
    }

    public void setTypeCover(String typeCover) {
        this.typeCover = typeCover;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typeCoverId);
        dest.writeString(typeCover);
    }
}
