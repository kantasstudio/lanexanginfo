package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class UserModel implements Parcelable {

    private int userId;
    private String identification;
    private String token;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String age;
    private String gender;
    private String presentAddress;
    private String loginWith;
    private String profilePicUrl;
    private String picState;
    private int isActive;
    private int isDelete;
    private String createdDatetime;
    private String updatedDatetime;
    private int countFollowers;
    private int countFollowing;
    private int countReview;
    private int countRecordedList;
    private int countProgramTour;
    private boolean followState;
    private UserRoleModel userRoleModel;
    private CountryModel countryModel;
    private ArrayList<ProductOrderDetails> productOrderDetailsArrayList;

    public UserModel() {

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getLoginWith() {
        return loginWith;
    }

    public void setLoginWith(String loginWith) {
        this.loginWith = loginWith;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getPicState() {
        return picState;
    }

    public void setPicState(String picState) {
        this.picState = picState;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(String createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getUpdatedDatetime() {
        return updatedDatetime;
    }

    public void setUpdatedDatetime(String updatedDatetime) {
        this.updatedDatetime = updatedDatetime;
    }

    public int getCountFollowers() {
        return countFollowers;
    }

    public void setCountFollowers(int countFollowers) {
        this.countFollowers = countFollowers;
    }

    public int getCountFollowing() {
        return countFollowing;
    }

    public void setCountFollowing(int countFollowing) {
        this.countFollowing = countFollowing;
    }

    public int getCountReview() {
        return countReview;
    }

    public void setCountReview(int countReview) {
        this.countReview = countReview;
    }

    public int getCountRecordedList() {
        return countRecordedList;
    }

    public void setCountRecordedList(int countRecordedList) {
        this.countRecordedList = countRecordedList;
    }

    public int getCountProgramTour() {
        return countProgramTour;
    }

    public void setCountProgramTour(int countProgramTour) {
        this.countProgramTour = countProgramTour;
    }

    public boolean isFollowState() {
        return followState;
    }

    public void setFollowState(boolean followState) {
        this.followState = followState;
    }

    public UserRoleModel getUserRoleModel() {
        return userRoleModel;
    }

    public void setUserRoleModel(UserRoleModel userRoleModel) {
        this.userRoleModel = userRoleModel;
    }

    public CountryModel getCountryModel() {
        return countryModel;
    }

    public void setCountryModel(CountryModel countryModel) {
        this.countryModel = countryModel;
    }

    public ArrayList<ProductOrderDetails> getProductOrderDetailsArrayList() {
        return productOrderDetailsArrayList;
    }

    public void setProductOrderDetailsArrayList(ArrayList<ProductOrderDetails> productOrderDetailsArrayList) {
        this.productOrderDetailsArrayList = productOrderDetailsArrayList;
    }

    protected UserModel(Parcel in) {
        userId = in.readInt();
        identification = in.readString();
        token = in.readString();
        email = in.readString();
        password = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        age = in.readString();
        gender = in.readString();
        presentAddress = in.readString();
        loginWith = in.readString();
        profilePicUrl = in.readString();
        picState = in.readString();
        isActive = in.readInt();
        isDelete = in.readInt();
        createdDatetime = in.readString();
        updatedDatetime = in.readString();
        countFollowers = in.readInt();
        countFollowing = in.readInt();
        countReview = in.readInt();
        countRecordedList = in.readInt();
        countProgramTour = in.readInt();
        followState = in.readByte() != 0;
        userRoleModel = in.readParcelable(UserRoleModel.class.getClassLoader());
        countryModel = in.readParcelable(CountryModel.class.getClassLoader());
        productOrderDetailsArrayList = in.createTypedArrayList(ProductOrderDetails.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userId);
        dest.writeString(identification);
        dest.writeString(token);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(age);
        dest.writeString(gender);
        dest.writeString(presentAddress);
        dest.writeString(loginWith);
        dest.writeString(profilePicUrl);
        dest.writeString(picState);
        dest.writeInt(isActive);
        dest.writeInt(isDelete);
        dest.writeString(createdDatetime);
        dest.writeString(updatedDatetime);
        dest.writeInt(countFollowers);
        dest.writeInt(countFollowing);
        dest.writeInt(countReview);
        dest.writeInt(countRecordedList);
        dest.writeInt(countProgramTour);
        dest.writeByte((byte) (followState ? 1 : 0));
        dest.writeParcelable(userRoleModel, flags);
        dest.writeParcelable(countryModel, flags);
        dest.writeTypedList(productOrderDetailsArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
