package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserRoleModel implements Parcelable {

    private int userRoleId;
    private String userRoleTH;
    private String userRoleEN;
    private String userRoleLO;
    private String userRoleZH;

    public UserRoleModel() {

    }

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getUserRoleTH() {
        return userRoleTH;
    }

    public void setUserRoleTH(String userRoleTH) {
        this.userRoleTH = userRoleTH;
    }

    public String getUserRoleEN() {
        return userRoleEN;
    }

    public void setUserRoleEN(String userRoleEN) {
        this.userRoleEN = userRoleEN;
    }

    public String getUserRoleLO() {
        return userRoleLO;
    }

    public void setUserRoleLO(String userRoleLO) {
        this.userRoleLO = userRoleLO;
    }

    public String getUserRoleZH() {
        return userRoleZH;
    }

    public void setUserRoleZH(String userRoleZH) {
        this.userRoleZH = userRoleZH;
    }

    protected UserRoleModel(Parcel in) {
        userRoleId = in.readInt();
        userRoleTH = in.readString();
        userRoleEN = in.readString();
        userRoleLO = in.readString();
        userRoleZH = in.readString();
    }

    public static final Creator<UserRoleModel> CREATOR = new Creator<UserRoleModel>() {
        @Override
        public UserRoleModel createFromParcel(Parcel in) {
            return new UserRoleModel(in);
        }

        @Override
        public UserRoleModel[] newArray(int size) {
            return new UserRoleModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userRoleId);
        dest.writeString(userRoleTH);
        dest.writeString(userRoleEN);
        dest.writeString(userRoleLO);
        dest.writeString(userRoleZH);
    }
}
