package com.ipanda.lanexangtourism.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class VideoContentModel implements Parcelable {

    private int videoContentId;
    private String videoContentURL;
    private String videoContentTH;
    private String videoContentEN;
    private String videoContentLO;
    private String videoContentZH;
    private UserModel userModel;

    public VideoContentModel() {
    }

    public VideoContentModel(String videoContentURL) {
        this.videoContentURL = videoContentURL;
    }

    public int getVideoContentId() {
        return videoContentId;
    }

    public void setVideoContentId(int videoContentId) {
        this.videoContentId = videoContentId;
    }

    public String getVideoContentURL() {
        return videoContentURL;
    }

    public void setVideoContentURL(String videoContentURL) {
        this.videoContentURL = videoContentURL;
    }

    public String getVideoContentTH() {
        return videoContentTH;
    }

    public void setVideoContentTH(String videoContentTH) {
        this.videoContentTH = videoContentTH;
    }

    public String getVideoContentEN() {
        return videoContentEN;
    }

    public void setVideoContentEN(String videoContentEN) {
        this.videoContentEN = videoContentEN;
    }

    public String getVideoContentLO() {
        return videoContentLO;
    }

    public void setVideoContentLO(String videoContentLO) {
        this.videoContentLO = videoContentLO;
    }

    public String getVideoContentZH() {
        return videoContentZH;
    }

    public void setVideoContentZH(String videoContentZH) {
        this.videoContentZH = videoContentZH;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    protected VideoContentModel(Parcel in) {
        videoContentId = in.readInt();
        videoContentURL = in.readString();
        videoContentTH = in.readString();
        videoContentEN = in.readString();
        videoContentLO = in.readString();
        videoContentZH = in.readString();
        userModel = in.readParcelable(UserModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(videoContentId);
        dest.writeString(videoContentURL);
        dest.writeString(videoContentTH);
        dest.writeString(videoContentEN);
        dest.writeString(videoContentLO);
        dest.writeString(videoContentZH);
        dest.writeParcelable(userModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoContentModel> CREATOR = new Creator<VideoContentModel>() {
        @Override
        public VideoContentModel createFromParcel(Parcel in) {
            return new VideoContentModel(in);
        }

        @Override
        public VideoContentModel[] newArray(int size) {
            return new VideoContentModel[size];
        }
    };
}
