package com.ipanda.lanexangtourism.NavigationFragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Activity.ProductOrderListActivity;
import com.ipanda.lanexangtourism.Adapter.ItemsContactRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ItemsHotelRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ItemsRestaurantRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ItemsShoppingRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ItemsTourismRecyclerViewAdapter;
import com.ipanda.lanexangtourism.DialogFragment.ChooseDateHotelDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.ItemsSearchHomeDialogFragment;
import com.ipanda.lanexangtourism.FilterActivity.ContactFilterFragment;
import com.ipanda.lanexangtourism.FilterActivity.HotelFilterFragment;
import com.ipanda.lanexangtourism.FilterActivity.RestaurantFilterFragment;
import com.ipanda.lanexangtourism.FilterActivity.ShoppingFilterFragment;
import com.ipanda.lanexangtourism.FilterActivity.TouristAttractionFilterFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.LoadMenuFragmentHelper;
import com.ipanda.lanexangtourism.Interface_click.ItemsClickListener;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.DeliciousGuaranteeModel;
import com.ipanda.lanexangtourism.Model.FacilitiesModel;
import com.ipanda.lanexangtourism.Model.ItemsContactModel;
import com.ipanda.lanexangtourism.Model.ItemsHotelModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsRestaurantModel;
import com.ipanda.lanexangtourism.Model.ItemsShoppingModel;
import com.ipanda.lanexangtourism.Model.ItemsTourismModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.BookingOrderDetailsAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsContactAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsHotelAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsRestaurantAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsShoppingAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsTourismAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.BookingOrderDetailsCallBack;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsContactCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsHotelCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsRestaurantCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsShoppingCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsTourismCallBack;
import com.ipanda.lanexangtourism.items_view.ViewTourismActivity;
import com.ipanda.lanexangtourism.post_search.HotelPostRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class ListViewNavigationFragment extends Fragment implements View.OnClickListener, ItemsTourismCallBack, ItemsRestaurantCallBack,
        ItemsShoppingCallBack, ItemsHotelCallBack, ItemsContactCallBack, ItemsClickListener, BookingOrderDetailsCallBack, ExchangeRateCallBack {

    //Variables
    private static final String TAG = ListViewNavigationFragment.class.getSimpleName();

    private Toolbar mToolbar;

    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle toggle;

    private View view;

    private ConstraintLayout btnSearch;

    private TextView txtTitleFilter, txtTitleSearchFilter, txt_count_filter;

    private RecyclerView mRecyclerView;

    private ItemsTourismRecyclerViewAdapter mTourismAdapter;
    private ItemsRestaurantRecyclerViewAdapter mRestaurantAdapter;
    private ItemsShoppingRecyclerViewAdapter mShoppingAdapter;
    private ItemsHotelRecyclerViewAdapter mHotelAdapter;
    private ItemsContactRecyclerViewAdapter mContactAdapter;

    private String mTitleFilter;

    private int getCategory;

    private String userId;

    private String url = "", urlOrder = "";

    private ChangeLanguageLocale languageLocale;

    private CircularSticksLoader sticksLoader;

    private ImageView img_out_of_area;

    private TextView txt_out_of_area;

    private String sDate = null, eDate = null, showDate = null, showMonth = null;

    private BookingModel bookingModel;

    private Bundle bundle;

    //Menu Filter
    private ArrayList<SubCategoryModel> subCategoryArrayList;

    private ArrayList<DeliciousGuaranteeModel> deliciousArrayList;

    private ArrayList<SubCategoryModel> travelingArrayList;

    private ArrayList<FacilitiesModel> facilitiesArrayList;

    private boolean isStarOne, isStarTwo, isStarThree, isStarFour, isStarFive;

    private SearchView searchTourist;

    private CheckBox checkFilterOpenClosed;

    private String filterOpenClosed = "";

    private RateModel rateModel;

    private FusedLocationProviderClient fusedLocationProviderClient;

    private List<Address> addresses;

//    private Location getLocation;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    private ArrayList<ItemsTourismModel> newTourismOffline = new ArrayList<>();

    private ArrayList<ItemsRestaurantModel> newRestaurantOffline = new ArrayList<>();

    private ArrayList<ItemsShoppingModel> newShoppingOffline = new ArrayList<>();

    private ArrayList<ItemsHotelModel> newHotelOffline = new ArrayList<>();

    private ArrayList<ItemsContactModel> newContactOffline = new ArrayList<>();


    public ListViewNavigationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        bundle = new Bundle();
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", "");
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        getVersion = getContext().getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);

        if (getArguments() != null) {
            bookingModel = new BookingModel();
            mTitleFilter = getArguments().getString("TEXT_TITLE");
            getCategory = getArguments().getInt("CATEGORY_ID");
            bookingModel = getArguments().getParcelable("BOOKING");
        }

        if (isModeOnline){
            String urlRate = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/ExchangeRate";
            new ExchangeRateAsyncTask(this).execute(urlRate);
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        }else {

        }

        //fragment get result
        getParentFragmentManager().setFragmentResultListener("KEY_CHOOSE_DATE_HOTEL", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle bundle) {
                bookingModel = new BookingModel();
                sDate = bundle.getString("CHOOSE_START_DATE");
                eDate = bundle.getString("CHOOSE_END_DATE");
                showDate = bundle.getString("CHOOSE_SHOW_DATE");
                showMonth = bundle.getString("CHOOSE_SHOW_MONTH");
                if (sDate != null && eDate != null && showMonth != null) {
                    bookingModel.setCheckIn(sDate);
                    bookingModel.setCheckOut(eDate);
                    bookingModel.setScheduleCheckIn(showMonth);
                }
            }
        });

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_view_navigation, container, false);
        //views
        mToolbar = view.findViewById(R.id.toolbar_search);
        mDrawerLayout = view.findViewById(R.id.drawer_layout);
        btnSearch = view.findViewById(R.id.btn_search_filter);
        txtTitleSearchFilter = view.findViewById(R.id.edt_title_search_filter);
        txtTitleFilter = view.findViewById(R.id.txt_title_filter);
        mRecyclerView = view.findViewById(R.id.recycler_items_view);
        txt_count_filter = view.findViewById(R.id.txt_count_filter);
        sticksLoader = view.findViewById(R.id.loader_navigation);
        img_out_of_area = view.findViewById(R.id.img_out_of_area);
        txt_out_of_area = view.findViewById(R.id.txt_out_of_area);
        checkFilterOpenClosed = view.findViewById(R.id.check_filter_open_closed_id);
        searchTourist = (SearchView) view.findViewById(R.id.search_open_closed_id);

        //setup Toolbar
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);

        //setup ActionBarDrawerToggle
        toggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            Drawable drawable = getResources().getDrawable(R.drawable.ic_menu_burger_dark);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 70, 60, true));
            newDrawable.setColorFilter(getResources().getColor(R.color.colorDrawerMenu), PorterDuff.Mode.SRC_ATOP);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        }

        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (isModeOnline) {
            if (userId != null) {
                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/" + getCategory + "?user_id=" + userId + "&latitude=&longitude=";
                urlOrder = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/OrderDetails?user_id=" + userId;
            } else {
                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/" + getCategory + "?user_id=" + "&latitude=&longitude=";
                urlOrder = null;
            }
            addresses = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                getLocation();
            } else {
                loadAsyncTask();
                ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
            }
        }else {
            if (getVersion != 0) {
                mDatabase = new DatabaseHelper(getContext(), getVersion);
                mManager = new DatabaseManager(getContext(), mDatabase);
                setItemsOfflineMode();
            }else {
                alertDialog();
            }
        }

        //set on click drawer menu
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        LoadMenuFragmentHelper helper = new LoadMenuFragmentHelper(getContext(), view, fragmentTransaction, mDrawerLayout);
        helper.drawerMenu();

        //set on click search
        btnSearch.setOnClickListener(this);

        //set text
        txtTitleSearchFilter.setText(getString(R.string.text_search) + "" + mTitleFilter);
        txtTitleFilter.setText(mTitleFilter);

        getParentFragmentManager().setFragmentResultListener("key_menu_two", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String key, @NonNull Bundle result) {
                subCategoryArrayList = result.getParcelableArrayList("sub_category_list");
                new ItemsTourismAsyncTask(ListViewNavigationFragment.this).execute(url);
            }
        });

        getParentFragmentManager().setFragmentResultListener("key_menu_three", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String key, @NonNull Bundle result) {
                subCategoryArrayList = result.getParcelableArrayList("sub_category_list");
                deliciousArrayList = result.getParcelableArrayList("delicious_list");
                new ItemsRestaurantAsyncTask(ListViewNavigationFragment.this).execute(url);
            }
        });

        getParentFragmentManager().setFragmentResultListener("key_menu_four", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String key, @NonNull Bundle result) {
                subCategoryArrayList = result.getParcelableArrayList("sub_category_list");
                new ItemsShoppingAsyncTask(ListViewNavigationFragment.this).execute(url);
            }
        });

        getParentFragmentManager().setFragmentResultListener("key_menu_five", this, new FragmentResultListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onFragmentResult(@NonNull String key, @NonNull Bundle result) {
                isStarOne = result.getBoolean("is_one");
                isStarTwo = result.getBoolean("is_two");
                isStarThree = result.getBoolean("is_three");
                isStarFour = result.getBoolean("is_four");
                isStarFive = result.getBoolean("is_five");
                subCategoryArrayList = result.getParcelableArrayList("sub_category_list");
                facilitiesArrayList = result.getParcelableArrayList("facilities_list");
                String getMax = result.getString("getMax");
                String getMin = result.getString("getMin");
//                new ItemsHotelAsyncTask(ListViewNavigationFragment.this).execute(url);

                HttpCall httpCallPost = new HttpCall();
                httpCallPost.setMethodType(HttpCall.POST);
                httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Fillter/");
                HashMap<String, String> paramsPost = new HashMap<>();
                paramsPost.put("category_id", "5");
                paramsPost.put("subcategory_id", getSubCategory());
                paramsPost.put("str_search", "");
                paramsPost.put("accommodation_level", getStar());
                paramsPost.put("facilities_id", getFacilities());
                paramsPost.put("price_range", getMin + "," + getMax);
                paramsPost.put("latitude", "");
                paramsPost.put("longitude", "");
                httpCallPost.setParams(paramsPost);
                new HotelPostRequest(ListViewNavigationFragment.this).execute(httpCallPost);
            }
        });

        getParentFragmentManager().setFragmentResultListener("key_menu_six", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String key, @NonNull Bundle result) {
                subCategoryArrayList = result.getParcelableArrayList("sub_category_list");
                travelingArrayList = result.getParcelableArrayList("traveling_list");
                new ItemsContactAsyncTask(ListViewNavigationFragment.this).execute(url);
            }
        });

        RelativeLayout filter_title = null;
        switch (getCategory) {
            case 2:
                filter_title = view.findViewById(R.id.relativeLayout_title_filter);
                TextView btnFilterTourismLocation = view.findViewById(R.id.filter_view_id);
                filter_title.setVisibility(View.VISIBLE);
                btnFilterTourismLocation.setText(getResources().getString(R.string.text_attraction_type));
                btnFilterTourismLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DialogFragment dialogFragment = TouristAttractionFilterFragment.newInstance();
                        bundle.putString("TEXT_TITLE", getResources().getString(R.string.text_attraction_type));
                        bundle.putInt("CATEGORY_ID", 2);
                        if (isModeOnline) {
                            dialogFragment.setArguments(bundle);
                            dialogFragment.show(getFragmentManager(), TAG);
                        }else {

                        }
                    }
                });


                checkFilterOpenClosed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkFilterOpenClosed.isChecked()) {
                            filterOpenClosed = "open";
                            checkFilterOpenClosed.setChecked(true);
                            searchTourist.setQuery(filterOpenClosed, true);
                            checkFilterOpenClosed.setTextColor(getResources().getColor(R.color.white));
                        } else {
                            filterOpenClosed = "";
                            checkFilterOpenClosed.setChecked(false);
                            searchTourist.setQuery(filterOpenClosed, true);
                            checkFilterOpenClosed.setTextColor(getResources().getColor(R.color.colorTextFilter));
                        }

                    }
                });
                break;
            case 3:
                filter_title = view.findViewById(R.id.relativeLayout_title_filter);
                TextView btnFilterTypeRestaurant = view.findViewById(R.id.filter_view_id);
                filter_title.setVisibility(View.VISIBLE);
                btnFilterTypeRestaurant.setText(getResources().getString(R.string.text_type_restaurants));
                btnFilterTypeRestaurant.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DialogFragment dialogFragment = RestaurantFilterFragment.newInstance();
                        bundle.putString("TEXT_TITLE", getResources().getString(R.string.text_type_restaurants));
                        bundle.putInt("CATEGORY_ID", 3);
                        if (isModeOnline) {
                            dialogFragment.setArguments(bundle);
                            dialogFragment.show(getFragmentManager(), TAG);
                        }else {

                        }
                    }
                });


                checkFilterOpenClosed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkFilterOpenClosed.isChecked()) {
                            filterOpenClosed = "open";
                            checkFilterOpenClosed.setChecked(true);
                            searchTourist.setQuery(filterOpenClosed, true);
                            checkFilterOpenClosed.setTextColor(getResources().getColor(R.color.white));
                        } else {
                            filterOpenClosed = "";
                            checkFilterOpenClosed.setChecked(false);
                            searchTourist.setQuery(filterOpenClosed, true);
                            checkFilterOpenClosed.setTextColor(getResources().getColor(R.color.colorTextFilter));
                        }

                    }
                });

                break;
            case 4:
                filter_title = view.findViewById(R.id.relativeLayout_title_filter);
                TextView btnFilterShopping = view.findViewById(R.id.filter_view_id);
                ConstraintLayout conOrdersDetail = view.findViewById(R.id.con_orders_detail_id);

                filter_title.setVisibility(View.VISIBLE);
                conOrdersDetail.setVisibility(View.VISIBLE);
                btnFilterShopping.setText(getResources().getString(R.string.text_type_of_souvenirs));
                btnFilterShopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DialogFragment dialogFragment = ShoppingFilterFragment.newInstance();
                        bundle.putString("TEXT_TITLE", getResources().getString(R.string.text_type_of_souvenirs));
                        bundle.putInt("CATEGORY_ID", 4);
                        if (isModeOnline) {
                            dialogFragment.setArguments(bundle);
                            dialogFragment.show(getFragmentManager(), TAG);
                        }else {

                        }
                    }
                });

                if (isModeOnline) {
                    if (urlOrder != null) {
                        new BookingOrderDetailsAsyncTask(this).execute(urlOrder);
                    }
                }else {

                }

                checkFilterOpenClosed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkFilterOpenClosed.isChecked()) {
                            filterOpenClosed = "open";
                            checkFilterOpenClosed.setChecked(true);
                            searchTourist.setQuery(filterOpenClosed, true);
                            checkFilterOpenClosed.setTextColor(getResources().getColor(R.color.white));
                        } else {
                            filterOpenClosed = "";
                            checkFilterOpenClosed.setChecked(false);
                            searchTourist.setQuery(filterOpenClosed, true);
                            checkFilterOpenClosed.setTextColor(getResources().getColor(R.color.colorTextFilter));
                        }

                    }
                });

                conOrdersDetail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });

                break;
            case 5:
                filter_title = view.findViewById(R.id.relativeLayout_title_filter_hotel);
                TextView btnFilterHotel = view.findViewById(R.id.filter_hotel_view_id);
                filter_title.setVisibility(View.VISIBLE);
                btnFilterHotel.setText(getResources().getString(R.string.text_accommodation_filter));
                btnFilterHotel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DialogFragment dialogFragment = HotelFilterFragment.newInstance();
                        bundle.putString("TEXT_TITLE", getResources().getString(R.string.text_accommodation_filter));
                        bundle.putInt("CATEGORY_ID", 5);
                        if (isModeOnline) {
                            dialogFragment.setArguments(bundle);
                            dialogFragment.show(getFragmentManager(), TAG);
                        }else {

                        }
                    }
                });

                CardView btnFilterSoft = view.findViewById(R.id.filter_view_date_id);
                btnFilterSoft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DialogFragment dialogFragment = ChooseDateHotelDialogFragment.newInstance();
                        dialogFragment.setArguments(bundle);
                        dialogFragment.show(getFragmentManager(), TAG);
                    }
                });


                break;
            case 6:
                filter_title = view.findViewById(R.id.relativeLayout_title_filter);
                TextView btnFilterContact = view.findViewById(R.id.filter_view_id);
                filter_title.setVisibility(View.VISIBLE);
                btnFilterContact.setText(getResources().getString(R.string.text_important_location));
                btnFilterContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DialogFragment dialogFragment = ContactFilterFragment.newInstance();
                        bundle.putString("TEXT_TITLE", getResources().getString(R.string.text_important_location));
                        bundle.putInt("CATEGORY_ID", 6);
                        if (isModeOnline) {
                            dialogFragment.setArguments(bundle);
                            dialogFragment.show(getFragmentManager(), TAG);
                        }else {

                        }
                    }
                });


                checkFilterOpenClosed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkFilterOpenClosed.isChecked()) {
                            filterOpenClosed = "open";
                            checkFilterOpenClosed.setChecked(true);
                            searchTourist.setQuery(filterOpenClosed, true);
                            checkFilterOpenClosed.setTextColor(getResources().getColor(R.color.white));
                        } else {
                            filterOpenClosed = "";
                            checkFilterOpenClosed.setChecked(false);
                            searchTourist.setQuery(filterOpenClosed, true);
                            checkFilterOpenClosed.setTextColor(getResources().getColor(R.color.colorTextFilter));
                        }

                    }
                });

                break;
        }

        //fragment get result
        getParentFragmentManager().setFragmentResultListener("KEY_CHOOSE_DATE_HOTEL_MAIN", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle bundle) {
                sDate = bundle.getString("CHOOSE_START_DATE");
                eDate = bundle.getString("CHOOSE_END_DATE");
                showDate = bundle.getString("CHOOSE_SHOW_DATE");
                showMonth = bundle.getString("CHOOSE_SHOW_MONTH");
                TextView txtDate = view.findViewById(R.id.filter_date_id);
                txtDate.setText(showDate);
                bookingModel.setCheckIn(sDate);
                bookingModel.setCheckOut(eDate);
                bookingModel.setScheduleCheckIn(showMonth);

            }
        });

        return view;
    }

    private void setItemsOfflineMode() {
        int count = 0;
        switch (getCategory){
            case 2:
                newTourismOffline = mManager.getItemsListTourist();
                if (newTourismOffline != null && newTourismOffline.size() != 0) {
                    count = newTourismOffline.size();
                    mTourismAdapter = new ItemsTourismRecyclerViewAdapter(getContext(), newTourismOffline, this, languageLocale.getLanguage(), txt_count_filter);
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    mRecyclerView.setAdapter(mTourismAdapter);
                    txt_count_filter.setText(count + "");
                    mRecyclerView.setVisibility(View.VISIBLE);
                }
                break;
            case 3:
                newRestaurantOffline = mManager.getItemsListRestaurants();
                if (newRestaurantOffline != null && newRestaurantOffline.size() != 0) {
                    count = newRestaurantOffline.size();
                    mRestaurantAdapter = new ItemsRestaurantRecyclerViewAdapter(getContext(), newRestaurantOffline, this, languageLocale.getLanguage(), txt_count_filter);
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    mRecyclerView.setAdapter(mRestaurantAdapter);
                    txt_count_filter.setText(count + "");
                    mRecyclerView.setVisibility(View.VISIBLE);
                }
                break;
            case 4:
                newShoppingOffline = mManager.getItemsListShopping();
                if (newShoppingOffline != null && newShoppingOffline.size() != 0) {
                    count = newShoppingOffline.size();
                    mShoppingAdapter = new ItemsShoppingRecyclerViewAdapter(getContext(), newShoppingOffline, this, languageLocale.getLanguage(), txt_count_filter);
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    mRecyclerView.setAdapter(mShoppingAdapter);
                    txt_count_filter.setText(count + "");
                    mRecyclerView.setVisibility(View.VISIBLE);
                }
                break;
            case 5:
                newHotelOffline = mManager.getItemsListHotels();
                if (newHotelOffline != null && newHotelOffline.size() != 0) {
                    count = newHotelOffline.size();
                    mHotelAdapter = new ItemsHotelRecyclerViewAdapter(getContext(), newHotelOffline, this, languageLocale.getLanguage(), rateModel);
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    mRecyclerView.setAdapter(mHotelAdapter);
                    txt_count_filter.setText(count + "");
                    mRecyclerView.setVisibility(View.VISIBLE);
                }
                break;
            case 6:
                newContactOffline = mManager.getItemsListContact();
                if (newContactOffline != null && newContactOffline.size() != 0) {
                    count = newContactOffline.size();
                    mContactAdapter = new ItemsContactRecyclerViewAdapter(getContext(), newContactOffline, this, languageLocale.getLanguage(), txt_count_filter);
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    mRecyclerView.setAdapter(mContactAdapter);
                    txt_count_filter.setText(count + "");
                    mRecyclerView.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Location location = task.getResult();
                if (location != null) {
                    try {
                        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                        if (addresses != null && addresses.size() != 0) {
                            url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/" + getCategory + "?user_id=" + userId + "&latitude=" + addresses.get(0).getLatitude() + "&longitude=" + addresses.get(0).getLongitude();
                            loadAsyncTask();
                        }else {
                            loadAsyncTask();
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void loadAsyncTask(){
        if (isModeOnline) {
            switch (getCategory) {
                case 2:
                    new ItemsTourismAsyncTask(ListViewNavigationFragment.this).execute(url);
                    break;
                case 3:
                    new ItemsRestaurantAsyncTask(ListViewNavigationFragment.this).execute(url);
                    break;
                case 4:
                    new ItemsShoppingAsyncTask(ListViewNavigationFragment.this).execute(url);
                    break;
                case 5:
                    new ItemsHotelAsyncTask(ListViewNavigationFragment.this).execute(url);
                    break;
                case 6:
                    new ItemsContactAsyncTask(ListViewNavigationFragment.this).execute(url);
                    break;
            }
        }else {

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_search_filter:
                if (isModeOnline) {
                    DialogFragment dialogFragment = ItemsSearchHomeDialogFragment.newInstance();
                    bundle.putInt("CATEGORY_ID", getCategory);
                    bundle.putString("TEXT_TITLE", mTitleFilter);
                    bundle.putParcelableArrayList("ADDRESSES", (ArrayList<? extends Parcelable>) addresses);
                    dialogFragment.setArguments(bundle);
                    dialogFragment.show(getFragmentManager(), TAG);
                }else {

                }
                break;
        }
    }

    @Override
    public void onPreCallService() {
        if (sticksLoader != null) {
            sticksLoader.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            img_out_of_area.setVisibility(View.GONE);
            txt_out_of_area.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }


    @Override
    public void onRequestCompleteListenerOrderDetails(ArrayList<BookingModel> bookingArrayList) {
        if (bookingArrayList != null && bookingArrayList.size() != 0) {
            TextView txtAmount = view.findViewById(R.id.txt_amount_id);
            int amount = bookingArrayList.size();
            txtAmount.setText(amount+"");

            ImageView btnOrderList = view.findViewById(R.id.img_order_list_id);
            btnOrderList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), ProductOrderListActivity.class);
                    intent.putExtra("Booking_Order", bookingArrayList);
                    startActivity(intent);
                }
            });

        }else {

        }
    }

    @Override
    public void onRequestCompleteTourism(ArrayList<ItemsTourismModel> tourismArrayList) {
        ArrayList<ItemsTourismModel> newTourism = new ArrayList<>();
        if (tourismArrayList != null && tourismArrayList.size() != 0){
            if (subCategoryArrayList != null && subCategoryArrayList.size() != 0){
                for (SubCategoryModel sub : subCategoryArrayList){
                    if (sub.isFilter()){
                        for (ItemsTourismModel item : tourismArrayList) {
                            if (item.getSubCategoryModelArrayList().get(0).getCategoryId() == sub.getCategoryId()){
                                newTourism.add(item);
                            }
                        }
                    }
                }

            }else {
                newTourism = tourismArrayList;
            }

            mTourismAdapter = new ItemsTourismRecyclerViewAdapter(getContext(),newTourism,this, languageLocale.getLanguage(),txt_count_filter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(mTourismAdapter);

            int count = newTourism.size();
            txt_count_filter.setText(count+"");

            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            img_out_of_area.setVisibility(View.GONE);
            txt_out_of_area.setVisibility(View.GONE);
        }else {
            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            img_out_of_area.setVisibility(View.VISIBLE);
            txt_out_of_area.setVisibility(View.VISIBLE);
        }

        searchTourist.setIconified(false);
        searchTourist.setIconifiedByDefault(false);
        searchTourist.setSubmitButtonEnabled(true);
        searchTourist.setBackgroundColor(Color.TRANSPARENT);
        searchTourist.clearFocus();
        searchTourist.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchTourist.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mTourismAdapter != null) {
                    mTourismAdapter.getFilter().filter(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mTourismAdapter != null) {
                    mTourismAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });

    }

    @Override
    public void onRequestCompleteRestaurant(ArrayList<ItemsRestaurantModel> restaurantArrayList) {
        ArrayList<ItemsRestaurantModel> newRestaurant = new ArrayList<>();
        if (restaurantArrayList != null && restaurantArrayList.size() != 0){
            if (subCategoryArrayList != null && subCategoryArrayList.size() != 0 && deliciousArrayList != null && deliciousArrayList.size() != 0) {
                for (SubCategoryModel sub : subCategoryArrayList) {
                    for (DeliciousGuaranteeModel del : deliciousArrayList) {
                        if (sub.isFilter() && del.isFilter()) {
                            for (ItemsRestaurantModel item : restaurantArrayList) {
                                if (item.getDeliciousGuaranteeModelArrayList() != null && item.getDeliciousGuaranteeModelArrayList().size() != 0) {
                                    for (DeliciousGuaranteeModel gu : item.getDeliciousGuaranteeModelArrayList()) {
                                        if (item.getSubCategoryModelArrayList().get(0).getCategoryId() == sub.getCategoryId() && gu.getGuaranteeId() == del.getGuaranteeId()) {
                                            newRestaurant.add(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (subCategoryArrayList != null && subCategoryArrayList.size() != 0){
                for (SubCategoryModel sub : subCategoryArrayList){
                    if (sub.isFilter()){
                        for (ItemsRestaurantModel item : restaurantArrayList) {
                            if (item.getSubCategoryModelArrayList().get(0).getCategoryId() == sub.getCategoryId()){
                                newRestaurant.add(item);
                            }
                        }
                    }
                }
            }
            else if (deliciousArrayList != null && deliciousArrayList.size() != 0){
                for (DeliciousGuaranteeModel del : deliciousArrayList){
                    if (del.isFilter()){
                        for (ItemsRestaurantModel item : restaurantArrayList) {
                            if (item.getDeliciousGuaranteeModelArrayList() != null && item.getDeliciousGuaranteeModelArrayList().size() != 0) {
                                for (DeliciousGuaranteeModel gu : item.getDeliciousGuaranteeModelArrayList()) {
                                    if (gu.getGuaranteeId() == del.getGuaranteeId()) {
                                        newRestaurant.add(item);
                                    }
                                }
                            }
                        }
                    }
                }
            }else {
                newRestaurant = restaurantArrayList;
            }

            mRestaurantAdapter = new ItemsRestaurantRecyclerViewAdapter(getContext(),newRestaurant,this,languageLocale.getLanguage(),txt_count_filter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(mRestaurantAdapter);

            int count = newRestaurant.size();
            txt_count_filter.setText(count+"");

            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            img_out_of_area.setVisibility(View.GONE);
            txt_out_of_area.setVisibility(View.GONE);
        }else {
            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            img_out_of_area.setVisibility(View.VISIBLE);
            txt_out_of_area.setVisibility(View.VISIBLE);
        }

        searchTourist.setIconified(false);
        searchTourist.setIconifiedByDefault(false);
        searchTourist.setSubmitButtonEnabled(true);
        searchTourist.setBackgroundColor(Color.TRANSPARENT);
        searchTourist.clearFocus();
        searchTourist.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchTourist.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mRestaurantAdapter != null) {
                    mRestaurantAdapter.getFilter().filter(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mRestaurantAdapter != null) {
                    mRestaurantAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });
    }

    @Override
    public void onRequestCompleteShopping(ArrayList<ItemsShoppingModel> shoppingArrayList) {
        ArrayList<ItemsShoppingModel> newShopping = new ArrayList<>();
        if (shoppingArrayList != null && shoppingArrayList.size() != 0){
            if (subCategoryArrayList != null && subCategoryArrayList.size() != 0){
                for (SubCategoryModel sub : subCategoryArrayList){
                    if (sub.isFilter()){
                        for (ItemsShoppingModel item : shoppingArrayList) {
                            if (item.getSubCategoryModelArrayList().get(0).getCategoryId() == sub.getCategoryId()){
                                newShopping.add(item);
                            }
                        }
                    }
                }

            }else {
                newShopping = shoppingArrayList;
            }

            mShoppingAdapter = new ItemsShoppingRecyclerViewAdapter(getContext(),newShopping,this,languageLocale.getLanguage(),txt_count_filter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(mShoppingAdapter);

            int count = newShopping.size();
            txt_count_filter.setText(count+"");

            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            img_out_of_area.setVisibility(View.GONE);
            txt_out_of_area.setVisibility(View.GONE);
        }else {
            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            img_out_of_area.setVisibility(View.VISIBLE);
            txt_out_of_area.setVisibility(View.VISIBLE);
        }

        searchTourist.setIconified(false);
        searchTourist.setIconifiedByDefault(false);
        searchTourist.setSubmitButtonEnabled(true);
        searchTourist.setBackgroundColor(Color.TRANSPARENT);
        searchTourist.clearFocus();
        searchTourist.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchTourist.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mShoppingAdapter != null) {
                    mShoppingAdapter.getFilter().filter(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mShoppingAdapter != null) {
                    mShoppingAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });
    }

    @Override
    public void onRequestCompleteHotel(ArrayList<ItemsHotelModel> hotelArrayList) {
        if (hotelArrayList != null && hotelArrayList.size() != 0){

            mHotelAdapter = new ItemsHotelRecyclerViewAdapter(getContext(),hotelArrayList,this,languageLocale.getLanguage(),rateModel);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(mHotelAdapter);
            int size = hotelArrayList.size();
            txt_count_filter.setText(size+"");

            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            img_out_of_area.setVisibility(View.GONE);
            txt_out_of_area.setVisibility(View.GONE);
        }else {
            txt_count_filter.setText("0");
            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            img_out_of_area.setVisibility(View.VISIBLE);
            txt_out_of_area.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestCompleteContact(ArrayList<ItemsContactModel> contactArrayList) {
        ArrayList<ItemsContactModel> newContact = new ArrayList<>();
        if (contactArrayList != null && contactArrayList.size() != 0){
            if (subCategoryArrayList != null && subCategoryArrayList.size() != 0 && travelingArrayList != null && travelingArrayList.size() != 0) {
                for (SubCategoryModel sub : subCategoryArrayList){
                    if (sub.isFilter()){
                        for (ItemsContactModel item : contactArrayList) {
                            if (item.getSubCategoryModelArrayList().get(0).getCategoryId() == sub.getCategoryId()){
                                newContact.add(item);
                            }
                        }
                    }
                }
                for (SubCategoryModel tra : travelingArrayList){
                    if (tra.isFilter()){
                        for (ItemsContactModel item : contactArrayList) {
                            if (item.getSubCategoryModelArrayList().get(0).getCategoryId() == tra.getCategoryId()){
                                newContact.add(item);
                            }
                        }
                    }
                }
            }else if (subCategoryArrayList != null && subCategoryArrayList.size() != 0){
                for (SubCategoryModel sub : subCategoryArrayList){
                    if (sub.isFilter()){
                        for (ItemsContactModel item : contactArrayList) {
                            if (item.getSubCategoryModelArrayList().get(0).getCategoryId() == sub.getCategoryId()){
                                newContact.add(item);
                            }
                        }
                    }
                }
            }else if (travelingArrayList != null && travelingArrayList.size() != 0){
                for (SubCategoryModel tra : travelingArrayList){
                    if (tra.isFilter()){
                        for (ItemsContactModel item : contactArrayList) {
                            if (item.getSubCategoryModelArrayList().get(0).getCategoryId() == tra.getCategoryId()){
                                newContact.add(item);
                            }
                        }
                    }
                }
            }else {
                newContact = contactArrayList;
            }

            mContactAdapter = new ItemsContactRecyclerViewAdapter(getContext(),newContact,this,languageLocale.getLanguage(),txt_count_filter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(mContactAdapter);

            int count = newContact.size();
            txt_count_filter.setText(count+"");

            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            img_out_of_area.setVisibility(View.GONE);
            txt_out_of_area.setVisibility(View.GONE);
        }else {
            sticksLoader.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            img_out_of_area.setVisibility(View.VISIBLE);
            txt_out_of_area.setVisibility(View.VISIBLE);
        }

        searchTourist.setIconified(false);
        searchTourist.setIconifiedByDefault(false);
        searchTourist.setSubmitButtonEnabled(true);
        searchTourist.setBackgroundColor(Color.TRANSPARENT);
        searchTourist.clearFocus();
        searchTourist.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchTourist.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mContactAdapter != null) {
                    mContactAdapter.getFilter().filter(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mContactAdapter != null) {
                    mContactAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });
    }

    @Override
    public void onRequestFailed(String result) {

    }


    @Override
    public void itemClicked(ItemsTourismModel items) {
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",items);
        intent.putExtra("CATEGORY",2);
        startActivity(intent);
    }

    @Override
    public void itemClicked(ItemsRestaurantModel items) {
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",items);
        intent.putExtra("CATEGORY",3);
        startActivity(intent);
    }

    @Override
    public void itemClicked(ItemsShoppingModel items) {
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",items);
        intent.putExtra("CATEGORY",4);
        startActivity(intent);
    }

    @Override
    public void itemClicked(ItemsHotelModel items) {
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",items);
        intent.putExtra("CATEGORY",5);
        intent.putExtra("BOOKING", bookingModel);
        startActivity(intent);
    }

    @Override
    public void itemClicked(ItemsContactModel items) {
        Intent intent = new Intent(getContext(), ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL",items);
        intent.putExtra("CATEGORY",6);
        startActivity(intent);
    }

    @Override
    public void itemClicked(ImageView bookmarks, ItemsModel itemsModel) {
        String userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        if (itemsModel != null && userId != null){
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/BookMarks");
            HashMap<String,String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id",userId);
            paramsPost.put("Items_items_id", String.valueOf(itemsModel.getItemsId()));
            httpCallPost.setParams(paramsPost);
            new HttpPostRequestAsyncTask().execute(httpCallPost);
        }else {
            new AlertDialog.Builder(getContext())
                    .setTitle("กรุณาล็อกอินก่อน")
                    .setMessage("....")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(getContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .create().show();
        }

        if (itemsModel.getBookMarksModel().isBookmarksState()){
            itemsModel.getBookMarksModel().setBookmarksState(false);
            bookmarks.setImageResource(R.drawable.ic_bookmark_inactive);
        }else {
            itemsModel.getBookMarksModel().setBookmarksState(true);
            bookmarks.setImageResource(R.drawable.ic_bookmark_red);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getFacilities() {
        List<String> facilitiesId = new ArrayList<>();
        if (facilitiesArrayList != null && facilitiesArrayList.size() != 0){
            for (FacilitiesModel fac : facilitiesArrayList) {
                if (fac.isFilter()){
                    facilitiesId.add(fac.getFacilitiesId()+"");
                }
            }
        }

        return String.join(",", facilitiesId);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getStar() {
        List<String> starId = new ArrayList<>();
        if (isStarOne){
            starId.add("1");
        }
        if (isStarTwo){
            starId.add("2");
        }
        if (isStarThree){
            starId.add("3");
        }
        if (isStarFour){
            starId.add("4");
        }
        if (isStarFive){
            starId.add("5");
        }
        return String.join(",", starId);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getSubCategory(){
        List<String> subId = new ArrayList<>();
        if (subCategoryArrayList != null && subCategoryArrayList.size() != 0) {
            for (SubCategoryModel sub : subCategoryArrayList) {
                if (sub.isFilter()) {
                    subId.add(sub.getCategoryId() + "");
                }
            }
        }
        return String.join(",", subId);
    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

}
