package com.ipanda.lanexangtourism.NavigationFragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ipanda.lanexangtourism.Adapter.ItemsMapsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.LoadMenuFragmentHelper;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ItemsAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.ItemsCallBack;
import com.ipanda.lanexangtourism.utils.PermissionUtils;
import com.ipanda.lanexangtourism.utils.SetMarkerGoogleUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MapsViewNavigationFragment extends Fragment implements View.OnClickListener, ItemsCallBack, OnMapReadyCallback, BottomNavigationView.OnNavigationItemSelectedListener {

    //Variables
    private static final String TAG = MapsViewNavigationFragment.class.getSimpleName();
    private static final int REQUEST_LOCATION = 101;
    private static final int REQUEST_FINE_LOCATION = 101;

    private ChangeLanguageLocale languageLocale;

    private Toolbar mToolbar;

    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle toggle;

    private GoogleMap googleMap;

    private MapView mapView_mark_icon;

    private Marker marker;

    private View view;

    private ArrayList<ItemsModel> itemsList;

    private ArrayList<ItemsModel> newItemsFilter;

    private ItemsMapsRecyclerViewAdapter mapsAdapter;

    private RecyclerView recycler_maps_items_sliding;

    private ArrayList<ItemsModel> itemsModelArrayList;

    private ImageView btnBack, btnOpenBurger, btnCloseBurger, btn_my_location, img_cover_items, img_items_bookmarks;

    private TextView txt_items_topic, txt_items_contact, txt_items_status, txt_items_subcategory, txt_items_rating, txt_items_kg, txt_items_open_close;

    private ImageView btnSearchFilterIcon;

    private ImageView actionBackMaps;

    private SearchView edt_title_search_filter;

    private CheckBox check_map_hotel, check_map_restaurant, check_map_tourism, check_map_shopping, check_map_contact;

    private CheckBox check_open_close;

    private RelativeLayout rl_bg_star;

    private int getCategory;

    private String mTitleFilter;

    private String urlGetItemsAll = "";

    private FusedLocationProviderClient mFusedLocationClient;

    private SetMarkerGoogleUtils setMarkerGoogleUtils;

    private boolean statusShowInfoWindow = true;

    private ProgressDialog alertDialog = null;

    private BottomNavigationView bottomNavigationView;

    private Bundle bundle;

    //Location
    private static final int PERMISSION_ID = 1;
    private FusedLocationProviderClient client;
    private Location locationResult;

    private Boolean isModeOnline;


    public MapsViewNavigationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        isModeOnline = getContext().getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        if (getArguments() != null) {
            mTitleFilter = getArguments().getString("TEXT_TITLE");
            getCategory = getArguments().getInt("CATEGORY_ID");
            bundle = new Bundle();
            bundle.putString("TEXT_TITLE",mTitleFilter);
            bundle.putInt("CATEGORY_ID",getCategory);
        }

        client = LocationServices.getFusedLocationProviderClient(getActivity());
        getPermissionLocation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_maps_view_navigation, container, false);

        //views
        mToolbar = view.findViewById(R.id.toolbar_search);
        mDrawerLayout = view.findViewById(R.id.drawer_layout);
        mapView_mark_icon = view.findViewById(R.id.mapView_mark_icon);
        btnOpenBurger = view.findViewById(R.id.action_drawer_id);
        btnCloseBurger = view.findViewById(R.id.btn_burger_close);
        edt_title_search_filter = view.findViewById(R.id.edt_title_search_filter);
        recycler_maps_items_sliding = view.findViewById(R.id.recycler_maps_items_sliding);
        btn_my_location = view.findViewById(R.id.btn_my_location);
        check_map_hotel = view.findViewById(R.id.check_map_hotel);
        check_map_restaurant = view.findViewById(R.id.check_map_restaurant);
        check_map_tourism = view.findViewById(R.id.check_map_tourism);
        check_map_shopping = view.findViewById(R.id.check_map_shopping);
        check_map_contact = view.findViewById(R.id.check_map_contact);
        check_open_close = view.findViewById(R.id.check_open_close);
        actionBackMaps = view.findViewById(R.id.action_back_maps_id);
//        btnSearchFilterIcon = view.findViewById(R.id.img_search_maps_id);

        setViewItemContent();


        //setup Toolbar
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);

        //setup ActionBarDrawerToggle
        toggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //set event drawer
        setOnEventDrawer();

        //set on click drawer menu
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        LoadMenuFragmentHelper helper = new LoadMenuFragmentHelper(getContext(), view, fragmentTransaction, mDrawerLayout);
        helper.drawerMenuRight();

        btn_my_location.setOnClickListener(this);

        //new array list
        itemsList = new ArrayList<>();
        newItemsFilter = new ArrayList<>();
        itemsModelArrayList = new ArrayList<>();

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        String userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        if (isModeOnline) {
            if (userId != null) {
                urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=" + userId;

            } else {
                urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=";
            }
        }else {

        }

        String countString = getString(R.string.text_search) + "" + mTitleFilter + "" + getString(R.string.text_in_the_map);
        String textSearch = ""+getString(R.string.text_search) ;


        edt_title_search_filter.setQuery("",false);
        edt_title_search_filter.setQueryHint(textSearch);
        edt_title_search_filter.setIconified(false);
        edt_title_search_filter.setIconifiedByDefault(false);
        edt_title_search_filter.setSubmitButtonEnabled(true);
        edt_title_search_filter.setBackgroundColor(Color.TRANSPARENT);
        edt_title_search_filter.clearFocus();


        mapView_mark_icon.onCreate(savedInstanceState);

        //views
        bottomNavigationView = view.findViewById(R.id.bottom_navigation);

        //setup navigation
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        actionBackMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //default fragment
                loadFragment(new ListViewNavigationFragment());
            }
        });


        return view;

    }

    private void getAsyncTask() {
        if (isModeOnline) {
            new ItemsAsyncTask(this).execute(urlGetItemsAll);
        }else {

        }
    }

    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {
        LocationManager locationManager = (LocationManager) getActivity()
                .getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            client.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location != null){
                        locationResult = location;
                        String userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
                        if (userId != null) {
                            urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=" + userId + "&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();

                        }else {
                            urlGetItemsAll = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?user_id=" + "&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                        }
                        getAsyncTask();
                    }else {
                        LocationRequest locationRequest = new LocationRequest()
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setInterval(10000)
                                .setFastestInterval(1000)
                                .setNumUpdates(1);
                        LocationCallback locationCallback = new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                super.onLocationResult(locationResult);
                                Location location1 = locationResult.getLastLocation();
                                Toast.makeText(getContext(), location1.getLatitude()+"  "+location1.getLongitude(), Toast.LENGTH_SHORT).show();
                            }
                        };
                        client.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper());
                        getAsyncTask();
                    }
                }
            });
        }else {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    private void setViewItemContent() {
        img_cover_items = view.findViewById(R.id.img_cover_items);
        txt_items_topic = view.findViewById(R.id.txt_items_topic);
        txt_items_contact = view.findViewById(R.id.txt_items_contact);
        txt_items_status = view.findViewById(R.id.txt_items_status);
        txt_items_subcategory = view.findViewById(R.id.txt_items_subcategory);
        rl_bg_star = view.findViewById(R.id.rl_bg_star);
        txt_items_rating = view.findViewById(R.id.txt_items_rating);
        txt_items_kg = view.findViewById(R.id.txt_items_kg);
        img_items_bookmarks = view.findViewById(R.id.img_items_bookmarks);
        txt_items_open_close = view.findViewById(R.id.txt_items_open_close);
    }


    private void checkMapDefaultMenu() {
        switch (getCategory) {
            case 2:
                check_map_tourism.setChecked(true);
                break;
            case 3:
                check_map_restaurant.setChecked(true);
                break;
            case 4:
                check_map_shopping.setChecked(true);
                break;
            case 5:
                check_map_hotel.setChecked(true);
                break;
            case 6:
                check_map_contact.setChecked(true);
                break;
        }
    }

    private void checkMapIsChecked() {
        check_map_tourism.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_tourism.isChecked()) {
                    setShowIconMarkerMaps(2);
                } else {
                    setHideIconMarkerMaps(2);
                }
            }
        });

        check_map_restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_restaurant.isChecked()) {
                    setShowIconMarkerMaps(3);
                } else {
                    setHideIconMarkerMaps(3);
                }
            }
        });

        check_map_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_shopping.isChecked()) {
                    setShowIconMarkerMaps(4);
                } else {
                    setHideIconMarkerMaps(4);
                }
            }
        });

        check_map_hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_hotel.isChecked()) {
                    setShowIconMarkerMaps(5);
                } else {
                    setHideIconMarkerMaps(5);
                }
            }
        });

        check_map_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check_map_contact.isChecked()) {
                    setShowIconMarkerMaps(6);
                } else {
                    setHideIconMarkerMaps(6);
                }
            }
        });


    }

    private void setOnEventDrawer() {
        //set on click open burger
        btnOpenBurger.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.END);
            }
        });
        //set on click close burger
        btnCloseBurger.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_my_location:
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                if (isModeOnline) {
                    googleMap.setMyLocationEnabled(false);
                    setMyLocation();
                }else {

                }
                break;
            case R.id.btn_in_location:
                if (isModeOnline) {
                    setMyLocation();
                }else {

                }
                break;

        }
    }

    private void setMyLocation() {
        Toast.makeText(getContext(), "Wait 3 second...", Toast.LENGTH_SHORT).show();
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );


    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();

            double latitude = mLastLocation.getLatitude();
            double longitude =  mLastLocation.getLongitude();
            LatLng latLng = new LatLng(latitude,longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10), 2000, null);
            int resId = getResourceByFilename(getContext(), "ic_here");
            Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), resId);
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 145, 160, false);
            MarkerOptions options = new MarkerOptions()
                    .title(null)
                    .position(latLng)
                    .zIndex(1.0f)
                    .icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap))
                    .draggable(false);
            marker = googleMap.addMarker(options);
        }
    };

    private void getPermissionLocation(){
        if (ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            getCurrentLocation();
        }else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_ID);
        }
    }

    private void requestLocationPermission(){
        new AlertDialog.Builder(getContext())
                .setTitle("Permission needed")
                .setMessage("Google Maps does not have permission to access your location")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loadFragment(new ListViewNavigationFragment());
                    }
                })
                .create().show();
    }

    @Override
    public void onPreCallService() {
        showProgressDialog();
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList) {
        if (itemsModelArrayList != null && itemsModelArrayList.size() != 0){
            Log.d("", itemsModelArrayList+"");
            this.itemsList = itemsModelArrayList;

            mapView_mark_icon.getMapAsync(this::onMapReady);

            //check map default menu
            checkMapDefaultMenu();
            //get check maps
            checkMapIsChecked();

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.clear();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        setMarkerGoogleUtils = new SetMarkerGoogleUtils(getContext(),googleMap,marker,itemsList,languageLocale.getLanguage(),recycler_maps_items_sliding ,check_open_close,edt_title_search_filter,btnSearchFilterIcon);
        setMarkerGoogleUtils.setShowIconMarkerMaps();
        setMarkerGoogleUtils.setShowIconMarkerMaps(String.valueOf(getCategory));
        dismissProgressDialog();
    }

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(getContext());
        alertDialog.setMessage("Loading screen for Google Maps ...");
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();

        }
    }

    public void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("\t message.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void setShowIconMarkerMaps(int category){
        setMarkerGoogleUtils.setShowIconMarkerMaps(String.valueOf(category));
    }

    private void setHideIconMarkerMaps(int category){
        setMarkerGoogleUtils.setHideIconMarkerMaps(String.valueOf(category));
    }

    private void setContentItemsView(Marker marker) {
        String title = marker.getTitle();
        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";

        for (ItemsModel item : itemsList) {
            if (item.getTopicTH().equals(title)) {
                Picasso.get().load(paths+item.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_cover_items);
                txt_items_topic.setText(item.getTopicTH());
                txt_items_contact.setText(item.getContactTH());
                txt_items_subcategory.setText(item.getSubCategoryModelArrayList().get(0).getCategoryTH());
                txt_items_open_close.setText(item.getTimeOpen()+"-"+item.getTimeClose());
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getContext(), "Permission GRANTED", Toast.LENGTH_SHORT).show();
                getCurrentLocation();
            }else {
                requestLocationPermission();
                Toast.makeText(getContext(), "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }
    // Reload current fragment
    public void reloadCurrentFragment() {
        try {
            MapsViewNavigationFragment currentFragment = (MapsViewNavigationFragment) getFragmentManager().findFragmentById(getId());
            FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
            mFragmentTransaction.replace(getId(), currentFragment, TAG);
            mFragmentTransaction.addToBackStack(TAG);
            mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            mFragmentTransaction.detach(currentFragment);
            mFragmentTransaction.attach(currentFragment);
            mFragmentTransaction.commit();
        } catch (Exception exception) {
            Log.i("",exception.toString());
        }
    }//End Reload current fragment

    @Override
    public void onResume() {
        super.onResume();
        mapView_mark_icon.onResume();
    }
    @Override
    public void onStart() {
        super.onStart();
        mapView_mark_icon.onStart();
    }
    @Override
    public void onStop() {
        super.onStop();
        mapView_mark_icon.onStop();
    }
    @Override
    public void onPause() {
        mapView_mark_icon.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        mapView_mark_icon.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView_mark_icon.onLowMemory();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()){
            case R.id.bottomNavigationListViewId:
                fragment = new ListViewNavigationFragment();
                break;
            case R.id.bottomNavigationMapsViewId:
                fragment = new MapsViewNavigationFragment();
                break;
        }
        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment){
        fragment.setArguments(bundle);
        if (fragment != null){
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.FrameLayout_Navigation, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}
