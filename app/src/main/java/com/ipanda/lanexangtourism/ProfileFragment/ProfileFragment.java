package com.ipanda.lanexangtourism.ProfileFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.ipanda.lanexangtourism.Activity.BookmarksActivity;
import com.ipanda.lanexangtourism.Activity.EditProfileActivity;
import com.ipanda.lanexangtourism.Activity.FollowActivity;
import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Activity.PersonnelProgramActivity;
import com.ipanda.lanexangtourism.Activity.PersonnelReviewActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.MenuFragment.ContactFragment;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.UnReadMessageAsyncTask;
import com.ipanda.lanexangtourism.asynctask.UserAsyncTask;
import com.ipanda.lanexangtourism.interface_callback.UnReadMessagesCallBack;
import com.ipanda.lanexangtourism.interface_callback.UserCallBack;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends Fragment implements View.OnClickListener , UserCallBack , UnReadMessagesCallBack {

    //variables
    private static final String TAG = ProfileFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ImageView img_profile;

    private ImageView img_edit_profile;

    private TextView btnEditProfile,txt_profile_full_name,txt_login_with;

    private TextView txt_count_following, txt_count_followers, txt_count_recordedList, txt_count_review, txt_count_programTour;

    private RelativeLayout btnFollowing, btnFollowers, btnBookmarks, btnReview, btnProgramTour;

    private Button btnListMessage;

    private UserModel userModel;

    String userId = null;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        //views
        btnEditProfile = view.findViewById(R.id.btn_edit_profile);
        img_edit_profile = view.findViewById(R.id.img_edit_profile);
        btnFollowing = view.findViewById(R.id.btn_following);
        btnFollowers = view.findViewById(R.id.btn_followers);
        btnBookmarks = view.findViewById(R.id.btn_bookmarks);
        btnReview = view.findViewById(R.id.btn_review);
        btnProgramTour = view.findViewById(R.id.btn_program_tour);
        btnListMessage = view.findViewById(R.id.btn_list_message);
        img_profile = view.findViewById(R.id.img_profile);
        txt_profile_full_name = view.findViewById(R.id.txt_profile_full_name);
        txt_login_with = view.findViewById(R.id.txt_login_with);
        txt_count_following = view.findViewById(R.id.txt_count_following);
        txt_count_followers = view.findViewById(R.id.txt_count_followers);
        txt_count_recordedList = view.findViewById(R.id.txt_count_recordedList);
        txt_count_review = view.findViewById(R.id.txt_count_review);
        txt_count_programTour = view.findViewById(R.id.txt_count_programTour);

        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        if (getArguments() != null){

        }else {
            String url = "";
            if (userId != null){
                url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/User/"+userId;
                new UserAsyncTask(this).execute(url);
            }
        }

        if (userId != null){
            String urlCountMessage = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/chat/Chat/getUnReadMessages/"+userId;
            new UnReadMessageAsyncTask(this).execute(urlCountMessage);
        }




        //setup on click edit profile
        btnEditProfile.setOnClickListener(this);

        //set on click button
        btnFollowing.setOnClickListener(this);
        btnFollowers.setOnClickListener(this);
        btnBookmarks.setOnClickListener(this);
        btnReview.setOnClickListener(this);
        btnProgramTour.setOnClickListener(this);
        btnListMessage.setOnClickListener(this);
        img_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                intent.putExtra("USER_MODEL",userModel);
                startActivity(intent);
            }
        });



        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_edit_profile:
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                intent.putExtra("USER_MODEL",userModel);
                startActivity(intent);
                break;
            case R.id.btn_following:
                Intent following = new Intent(getContext(), FollowActivity.class);
                following.putExtra("TEXT_FOLLOWER",getString(R.string.text_following));
                following.putExtra("USER_ID",String.valueOf(userModel.getUserId()));
                following.putExtra("TYPE","following");
                startActivity(following);
                break;
            case R.id.btn_followers:
                Intent followers = new Intent(getContext(), FollowActivity.class);
                followers.putExtra("TEXT_FOLLOWER",getString(R.string.text_followers));
                followers.putExtra("USER_ID",String.valueOf(userModel.getUserId()));
                followers.putExtra("TYPE","follower");
                startActivity(followers);
                break;
            case R.id.btn_bookmarks:
                if (userId != null){
                    Intent bookmarks = new Intent(getContext(), BookmarksActivity.class);
                    bookmarks.putExtra("USER_ID",userId);
                    startActivity(bookmarks);
                }
                break;
            case R.id.btn_review:
                if (userId != null){
                    Intent review = new Intent(getContext(), PersonnelReviewActivity.class);
                    review.putExtra("USER_ID",userId);
                    startActivity(review);
                }
                break;
            case R.id.btn_program_tour:
                startActivity(new Intent(getContext(), PersonnelProgramActivity.class));
                break;
            case R.id.btn_list_message:
                if (userId != null) {
                    startActivity(new Intent(getContext(), ListMessageActivity.class));
                }
                break;
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerUnReadMessages(String count) {
        if (count != null){
            btnListMessage.setText(getResources().getString(R.string.text_message)+" ("+count+") ");
        }
    }

    @Override
    public void onRequestCompleteListener(UserModel userModels) {
        if (userModels != null){
            Log.e("check data",userModels+"");
            this.userModel = userModels;
            Picasso.get().load(userModel.getProfilePicUrl()).error(R.drawable.img_laceholder_error).placeholder(R.drawable.img_placeholder).into(img_profile);
            txt_profile_full_name.setText(userModel.getFirstName()+" "+userModel.getLastName());
            txt_login_with.setText(getResources().getString(R.string.text_login_via)+" "+userModel.getLoginWith());
            txt_count_followers.setText(userModels.getCountFollowers()+"");
            txt_count_following.setText(userModels.getCountFollowing()+"");
            txt_count_review.setText(userModels.getCountReview()+"");
            txt_count_recordedList.setText(userModels.getCountRecordedList()+"");
            txt_count_programTour.setText(userModels.getCountProgramTour()+"");
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }
}
