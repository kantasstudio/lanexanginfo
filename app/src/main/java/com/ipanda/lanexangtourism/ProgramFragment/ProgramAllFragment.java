package com.ipanda.lanexangtourism.ProgramFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.ProgramTourEditMainActivity;
import com.ipanda.lanexangtourism.Adapter.MyProgramTourRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.MyProgramTourClickListener;
import com.ipanda.lanexangtourism.MenuFragment.ContactFragment;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.NavigationFragment.MapsViewNavigationFragment;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.MyProgramTourAsyncTask;
import com.ipanda.lanexangtourism.get_delete.HttpGetDeleteNoRequest;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.MyProgramTourCallBack;

import java.util.ArrayList;
import java.util.HashMap;


public class ProgramAllFragment extends Fragment implements View.OnClickListener ,MyProgramTourClickListener , MyProgramTourCallBack{

    //Variables
    private static final String TAG = ProgramAllFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private View view;

    private String userId;

    private RecyclerView recyclerMyProgramTour;

    private MyProgramTourRecyclerViewAdapter recyclerMyProgramTourAdapter;

    private ArrayList<ProgramTourModel> programTourModelArrayList;

    private String urlDelete = "";


    public ProgramAllFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        if (userId != null) {
            String url = getString(R.string.app_api_ip)+ "dasta_thailand/api/mobile/user/ProgramTour/getEditProgramTour?user_id="+userId;
            new MyProgramTourAsyncTask(this).execute(url);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userId = getContext().getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {

        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_program_all, container, false);
        //views
        recyclerMyProgramTour = view.findViewById(R.id.recycler_my_program_tour);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void itemClicked(ProgramTourModel tourModel) {

    }

    @Override
    public void itemClickedMore(ProgramTourModel tourModel, String event) {
        Intent intent = new Intent(getContext(), ProgramTourEditMainActivity.class);
        switch (event){
            case "edit":
                intent.putExtra("PROGRAM_TOUR", tourModel);
                startActivity(intent);
                break;
            case "delete":
                urlDelete = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ProgramTour/deleteProgramTour?programTour_id="+tourModel.getProgramTourId();
                alertDialogDelete();
                break;
        }
    }

    private void alertDialogDelete(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage("Are you sure, You wanted to make decision");
        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Toast.makeText(getContext(),"You clicked yes button",Toast.LENGTH_LONG).show();
                        new HttpGetDeleteNoRequest().execute(urlDelete);
                        getActivity().recreate();
                    }
                });

        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ProgramTourModel> programTourModelArrayList) {
        if (programTourModelArrayList != null && programTourModelArrayList.size() != 0){
            recyclerMyProgramTourAdapter = new MyProgramTourRecyclerViewAdapter(getContext(), programTourModelArrayList, languageLocale.getLanguage(), this);
            recyclerMyProgramTour.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            recyclerMyProgramTour.setAdapter(recyclerMyProgramTourAdapter);
        }else {

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }


}
