package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.BusinessModel;
import com.ipanda.lanexangtourism.Model.CarModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.DistrictsModel;
import com.ipanda.lanexangtourism.Model.FoodModel;
import com.ipanda.lanexangtourism.Model.HotelAccommodationModel;
import com.ipanda.lanexangtourism.Model.ItemsDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsTopicDetailModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.PaymentTransactionModel;
import com.ipanda.lanexangtourism.Model.PeriodDayModel;
import com.ipanda.lanexangtourism.Model.PeriodTimeModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.PurchaseStatusModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.Model.RoomPictureModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.SubDistrictsModel;
import com.ipanda.lanexangtourism.Model.TravelDetailsModel;
import com.ipanda.lanexangtourism.Model.TravelPeriodModel;
import com.ipanda.lanexangtourism.interface_callback.BookingItemsDetailsCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class BookingItemsDetailsAsyncTask extends AsyncTask<String,Integer,String> {

    private BookingModel arrayList;
    private BookingItemsDetailsCallBack callBackService;

    public BookingItemsDetailsAsyncTask(BookingItemsDetailsCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public BookingModel  onParserContentToModel(String dataJSon) {
        BookingModel list = new BookingModel();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonBooking = object.optJSONArray("Booking");

            for (int i = 0; i < jsonBooking.length() ; i++) {
                JSONObject jsBooking = jsonBooking.getJSONObject(i);
                BookingModel booking = new BookingModel();
                PaymentTransactionModel payment = new PaymentTransactionModel();
                PurchaseStatusModel purchaseStatus = new PurchaseStatusModel();

                JSONArray jsonBookingData = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("BookingData");
                JSONArray jsonItems = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData");

                if (jsBooking != null){
                    payment.setPaymentId(jsBooking.getInt("paymentTransaction_id"));
                    payment.setTimestamp(jsBooking.getString("paymentTransaction_timestamp"));
                    payment.setTotal(jsBooking.getDouble("PaymentTransaction_totalPrice"));
                    payment.setFirstName(jsBooking.getString("paymentTransaction_firstName"));
                    payment.setLastName(jsBooking.getString("paymentTransaction_lastName"));
                    payment.setEmail(jsBooking.getString("paymentTransaction_email"));
                    payment.setPhone(jsBooking.getString("paymentTransaction_phone"));
                    payment.setWeChatId(jsBooking.getString("paymentTransaction_weChatId"));
                    payment.setWhatsAppId(jsBooking.getString("paymentTransaction_whatsAppId"));
//                    payment.setDiscount(jsBooking.getString(""));
                    purchaseStatus.setStatusId(jsBooking.getInt("purchaseStatus_id"));
                    purchaseStatus.setStatusTH(jsBooking.getString("purchaseStatus_statusThai"));
                    purchaseStatus.setStatusEN(jsBooking.getString("purchaserStatus_statusEnglish"));
                    purchaseStatus.setStatusZH(jsBooking.getString("purchaseStatus_statusChinese"));
                    purchaseStatus.setStatusLO(jsBooking.getString("purchaseStatus_statusLaos"));
                    payment.setPurchaseStatusModel(purchaseStatus);
                    booking.setPaymentTransactionModel(payment);

                    for (int j = 0; j < jsonBookingData.length() ; j++) {
                        JSONObject jsBookingData = jsonBookingData.getJSONObject(j);

                        if (jsBookingData != null){
                            booking.setBookingId(jsBookingData.getInt("booking_id"));
                            booking.setCheckIn(jsBookingData.getString("booking_checkIn"));
                            booking.setCheckOut(jsBookingData.getString("booking_checkOut"));
                            booking.setCheckInTime(jsBookingData.getString("booking_checkInTime"));
                            booking.setCheckOutTime(jsBookingData.getString("booking_checkOutTime"));
                            booking.setGuestAdult(jsBookingData.getInt("booking_guestAdultSingle"));
                            booking.setSpecialAdult(jsBookingData.getInt("booking_guestAdult"));
                            booking.setGuestChild(jsBookingData.getInt("booking_guestChildBed"));
                            booking.setSpecialChild(jsBookingData.getInt("booking_guestChild"));
                            booking.setDuration(jsBookingData.getInt("booking_duration"));
//                            booking.setScheduleSeat(jsBooking.getInt("booking_seat"));
                        }

                    }

                    for (int j = 0; j < jsonItems.length() ; j++) {
                        JSONObject jsItems = jsonItems.getJSONObject(j);
                        ItemsModel items = new ItemsModel();
                        MenuItemModel menuItem = new MenuItemModel();
                        CoverItemsModel cover = new CoverItemsModel();
                        RoomModel roomModel = new RoomModel();
                        ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                        SubCategoryModel subCategory = new SubCategoryModel();

                        if (jsItems != null) {
                            if (jsItems.getInt("MenuItem_menuItem_id") == 5){
                                roomModel.setRoomId(jsItems.getInt("room_id"));
                                roomModel.setTopicTH(jsItems.getString("room_topicThai"));
                                roomModel.setTopicEN(jsItems.getString("room_topicEnglish"));
                                roomModel.setTopicLO(jsItems.getString("room_topicLaos"));
                                roomModel.setTopicZH(jsItems.getString("room_topicChinese"));
                                roomModel.setDescriptionTH(jsItems.getString("room_descriptionThai"));
                                roomModel.setDescriptionEN(jsItems.getString("room_descriptionEnglish"));
                                roomModel.setDescriptionLO(jsItems.getString("room_descriptionLaos"));
                                roomModel.setDescriptionZH(jsItems.getString("room_descriptionChinese"));
                                roomModel.setDetailsTH(jsItems.getString("room_detailsThai"));
                                roomModel.setDetailsEN(jsItems.getString("room_detailsThai"));
                                roomModel.setDetailsLO(jsItems.getString("room_detailsLaos"));
                                roomModel.setDetailsZH(jsItems.getString("room_detailsChinese"));
                                roomModel.setPrice(jsItems.getDouble("room_price"));
                                roomModel.setBreakFast(jsItems.getInt("room_breakfast"));
                                items.setRoomModel(roomModel);
                                menuItem.setMenuItemId(jsItems.getInt("MenuItem_menuItem_id"));
                                items.setMenuItemModel(menuItem);
                                items.setItemsId(jsItems.getInt("Items_items_id"));

                                ArrayList<RoomPictureModel> roomPictureModelArrayList = new ArrayList<>();
                                try {
                                    JSONArray jsonPhotoRoom = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("PhotoRoom");
                                    for (int k = 0; k < jsonPhotoRoom.length(); k++) {
                                        JSONObject jsPhotoRoom = jsonPhotoRoom.getJSONObject(k);
                                        RoomPictureModel pictureModel = new RoomPictureModel();
                                        if (jsPhotoRoom != null){
                                            pictureModel.setPictureId(jsPhotoRoom.getInt("pictureRoom_id"));
                                            pictureModel.setPicturePaths(jsPhotoRoom.getString("pictureRoom_paths"));
                                            roomPictureModelArrayList.add(pictureModel);
                                        }
                                        roomModel.setRoomPictureModelArrayList(roomPictureModelArrayList);
                                    }

                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonBusiness = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("Business");
                                    for (int k = 0; k < jsonBusiness.length() ; k++) {
                                        JSONObject jsBusiness = jsonBusiness.getJSONObject(k);
                                        if (jsBusiness != null) {
                                            BusinessModel business = new BusinessModel();
                                            business.setBusinessId(jsBusiness.getInt("business_id"));
                                            business.setNameTH(jsBusiness.getString("business_nameThai"));
                                            business.setNameEN(jsBusiness.getString("business_nameEnglish"));
                                            business.setNameLO(jsBusiness.getString("business_nameLaos"));
                                            business.setNameZH(jsBusiness.getString("business_nameChinese"));
                                            business.setLatitude((float) jsBusiness.getDouble("business_latitude"));
                                            business.setLongitude((float) jsBusiness.getDouble("business_longitude"));
                                            business.setPresentAddress(jsBusiness.getString("business_presentAddress"));
                                            business.setLicensePaths(jsBusiness.getString("business_license_paths"));
                                            business.setOperatorNumber(jsBusiness.getString("business_operator_number"));
                                            business.setPhone(jsBusiness.getString("business_phone"));
                                            business.setWeb(jsBusiness.getString("business_www"));
                                            business.setFacebook(jsBusiness.getString("business_facebook"));
                                            business.setLine(jsBusiness.getString("business_line"));
                                            items.setBusinessModel(business);
                                        }
                                    }
                                }catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                            else if (jsItems.getInt("MenuItem_menuItem_id") == 9){
                                items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                                items.setTopicTH(jsItems.getString("itmes_topicThai"));
                                items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                                items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                                items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                                items.setLatitude(jsItems.getString("items_latitude"));
                                items.setLongitude(jsItems.getString("items_longitude"));
                                items.setContactTH(jsItems.getString("items_contactThai"));
                                items.setContactEN(jsItems.getString("items_contactEnglish"));
                                items.setContactLO(jsItems.getString("items_contactLaos"));
                                items.setContactZH(jsItems.getString("items_contactChinese"));
                                items.setPhone(jsItems.getString("items_phone"));
                                items.setEmail(jsItems.getString("items_email"));
                                items.setLine(jsItems.getString("items_line"));
                                items.setFacebookPage(jsItems.getString("items_facebookPage"));
                                items.setArURL(jsItems.getString("item_ar_url"));
                                items.setIsActive(jsItems.getInt("items_isActive"));
                                items.setIsPublish(jsItems.getInt("items_isPublish"));
                                items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                                items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                                items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                                items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                                items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                                items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                                items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                                items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
                                menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                                menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                                menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                                menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                                menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                                items.setMenuItemModel(menuItem);
                                subCategory.setCategoryId(Integer.parseInt(jsItems.getString("subcategory_id")));
                                subCategory.setCategoryTH(jsItems.getString("subcategory_thai"));
                                subCategory.setCategoryEN(jsItems.getString("subcategory_english"));
                                subCategory.setCategoryLO(jsItems.getString("subcategory_laos"));
                                subCategory.setCategoryZH(jsItems.getString("subcategory_chinese"));
                                subCategoryArrayList.add(subCategory);
                                items.setSubCategoryModelArrayList(subCategoryArrayList);

                                try {
                                    JSONArray jsonCover = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("CoverItems");
                                    ArrayList<CoverItemsModel> coverItemsModelsArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonCover.length(); k++) {
                                        JSONObject jsCover = jsonCover.getJSONObject(k);
                                        CoverItemsModel coverItemsModels = new CoverItemsModel();

                                        if (jsCover != null) {
                                            coverItemsModels.setCoverId(jsCover.getInt("coverItem_id"));
                                            coverItemsModels.setCoverPaths(jsCover.getString("coverItem_paths"));
                                            if (jsCover.getString("coverItem_url") != null && jsCover.getString("coverItem_url") != "" && !jsCover.getString("coverItem_url").equals("")) {
                                                coverItemsModels.setCoverURL(jsCover.getString("coverItem_url"));
                                            } else {
                                                coverItemsModels.setCoverURL(null);
                                            }
                                            coverItemsModelsArrayList.add(coverItemsModels);
                                        }
                                    }
                                    items.setCoverItemsModelArrayList(coverItemsModelsArrayList);
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonDetails = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("TravelDetails");
                                    ArrayList<TravelDetailsModel> travelDetailsModelArrayList = new ArrayList<>();
                                    ArrayList<HotelAccommodationModel> hotelAccommodationModelsArrayList = new ArrayList<>();
                                    ArrayList<FoodModel> foodModelArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonDetails.length() ; k++) {
                                        JSONObject jsDetails = jsonDetails.getJSONObject(k);
                                        TravelDetailsModel travelDetailsModel = new TravelDetailsModel();
                                        HotelAccommodationModel hotelModel = new HotelAccommodationModel();
                                        FoodModel foodModel = new FoodModel();

                                        if (jsDetails != null){
                                            travelDetailsModel.setDetailsId(jsDetails.getInt("travelDetails_id"));
                                            travelDetailsModel.setDetailsDateTH(jsDetails.getString("travelDetails_dateThai"));
                                            travelDetailsModel.setDetailsDateEN(jsDetails.getString("travelDetails_dateEnglish"));
                                            travelDetailsModel.setDetailsDateLO(jsDetails.getString("travelDetails_dateLaos"));
                                            travelDetailsModel.setDetailsDateZH(jsDetails.getString("travelDetails_dateChinese"));
                                            travelDetailsModel.setDetailsTextTH(jsDetails.getString("travelDetails_textThai"));
                                            travelDetailsModel.setDetailsTextEN(jsDetails.getString("travelDetails_textEnglish"));
                                            travelDetailsModel.setDetailsTextLO(jsDetails.getString("travelDetails_textLaos"));
                                            travelDetailsModel.setDetailsTextZH(jsDetails.getString("travelDetails_textChinese"));

                                            hotelModel.setAccommodationTH(jsDetails.getString("travelDetails_hotel_nameThai"));
                                            hotelModel.setAccommodationEN(jsDetails.getString("travelDetails_hotel_nameEnglish"));
                                            hotelModel.setAccommodationLO(jsDetails.getString("travelDetails_hotel_nameLaos"));
                                            hotelModel.setAccommodationZH(jsDetails.getString("travelDetails_hotel_nameChinese"));

                                            if (jsDetails.getString("travelDetails_food_breakfast").equalsIgnoreCase("1")){
                                                foodModel.setFoodBreakFast(true);
                                            }else {
                                                foodModel.setFoodBreakFast(false);
                                            }

                                            if (jsDetails.getString("travelDetails_food_lunch").equalsIgnoreCase("1")){
                                                foodModel.setFoodLunch(true);
                                            }else {
                                                foodModel.setFoodLunch(false);
                                            }

                                            if (jsDetails.getString("travelDetails_food_dinner").equalsIgnoreCase("1")){
                                                foodModel.setFoodDinner(true);
                                            }else {
                                                foodModel.setFoodDinner(false);
                                            }

                                            travelDetailsModelArrayList.add(travelDetailsModel);
                                            hotelAccommodationModelsArrayList.add(hotelModel);
                                            foodModelArrayList.add(foodModel);
                                        }

                                    }
                                    items.setTravelDetailsModelArrayList(travelDetailsModelArrayList);
                                    items.setHotelAccommodationModelArrayList(hotelAccommodationModelsArrayList);
                                    items.setFoodModelArrayList(foodModelArrayList);
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonPeriod = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("TravelPeriod");
                                    ArrayList<TravelPeriodModel> travelPeriodModelArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonPeriod.length() ; k++) {
                                        JSONObject jsPeriod = jsonPeriod.getJSONObject(k);
                                        TravelPeriodModel periodModel = new TravelPeriodModel();

                                        if (jsPeriod != null){
                                            periodModel.setPeriodId(jsPeriod.getInt("travelPeriod_id"));
                                            periodModel.setAmount(jsPeriod.getInt("travelPeriod_amount"));
                                            periodModel.setPeriodStart(jsPeriod.getString("travelPeriod_time_period_start"));
                                            periodModel.setPeriodEnd(jsPeriod.getString("travelPeriod_time_period_end"));
                                            periodModel.setAdultPrice(jsPeriod.getInt("travelPeriod_adult_price"));
                                            periodModel.setAdultSpecialPrice(jsPeriod.getInt("travelPeriod_adult_special_price"));
                                            periodModel.setChildPrice(jsPeriod.getInt("travelPeriod_child_price"));
                                            periodModel.setChildSpecialPrice(jsPeriod.getInt("travelPeriod_child_special_price"));

                                            travelPeriodModelArrayList.add(periodModel);
                                        }
                                    }
                                    items.setTravelPeriodModelArrayList(travelPeriodModelArrayList);
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonCondition = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("BookingCondition");
                                    ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonCondition.length() ; k++) {
                                        JSONObject jsCondition = jsonCondition.getJSONObject(k);
                                        BookingConditionModel conditionModel = new BookingConditionModel();

                                        if (jsCondition != null){
                                            conditionModel.setConditionTopicTH(jsCondition.getString("bookingConditionCategory_topicThai"));
                                            conditionModel.setConditionTopicEN(jsCondition.getString("bookingConditionCategory_topicEnglish"));
                                            conditionModel.setConditionTopicLO(jsCondition.getString("bookingConditionCategory_topicChinese"));
                                            conditionModel.setConditionTopicZH(jsCondition.getString("bookingConditionCategory_topicLaos"));
                                            conditionModel.setConditionDetailTH(jsCondition.getString("bookingConditioncol_detailThai"));
                                            conditionModel.setConditionDetailEN(jsCondition.getString("bookingConditioncol_detailEnglish"));
                                            conditionModel.setConditionDetailLO(jsCondition.getString("bookingConditioncol_detailChinese"));
                                            conditionModel.setConditionDetailZH(jsCondition.getString("bookingConditioncol_detailLaos"));

                                            bookingConditionModelArrayList.add(conditionModel);
                                        }
                                    }
                                    items.setBookingConditionModelArrayList(bookingConditionModelArrayList);
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonBusiness = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("Business");
                                    for (int k = 0; k < jsonBusiness.length() ; k++) {
                                        JSONObject jsBusiness = jsonBusiness.getJSONObject(k);
                                        if (jsBusiness != null) {
                                            BusinessModel business = new BusinessModel();
                                            business.setBusinessId(jsBusiness.getInt("business_id"));
                                            business.setNameTH(jsBusiness.getString("business_nameThai"));
                                            business.setNameEN(jsBusiness.getString("business_nameEnglish"));
                                            business.setNameLO(jsBusiness.getString("business_nameLaos"));
                                            business.setNameZH(jsBusiness.getString("business_nameChinese"));
                                            business.setLatitude((float) jsBusiness.getDouble("business_latitude"));
                                            business.setLongitude((float) jsBusiness.getDouble("business_longitude"));
                                            business.setPresentAddress(jsBusiness.getString("business_presentAddress"));
                                            business.setLicensePaths(jsBusiness.getString("business_license_paths"));
                                            business.setOperatorNumber(jsBusiness.getString("business_operator_number"));
                                            business.setPhone(jsBusiness.getString("business_phone"));
                                            business.setWeb(jsBusiness.getString("business_www"));
                                            business.setFacebook(jsBusiness.getString("business_facebook"));
                                            business.setLine(jsBusiness.getString("business_line"));
                                            items.setBusinessModel(business);
                                        }
                                    }
                                }catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonScopeArea = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("ScopeArea");
                                    ArrayList<ProvincesModel> provincesModelArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonScopeArea.length() ; k++) {
                                        JSONObject jsScopeArea = jsonScopeArea.getJSONObject(k);
                                        ProvincesModel pro = new ProvincesModel();
                                        if (jsScopeArea != null) {
                                            pro.setProvincesId(jsScopeArea.getInt("provinces_id"));
                                            pro.setProvincesCode(jsScopeArea.getInt("provinces_code"));
                                            pro.setProvincesTH(jsScopeArea.getString("provinces_thai"));
                                            pro.setProvincesEN(jsScopeArea.getString("provinces_english"));
                                            pro.setProvincesLO(jsScopeArea.getString("provinces_laos"));
                                            pro.setProvincesZH(jsScopeArea.getString("provinces_chinese"));
                                            provincesModelArrayList.add(pro);
                                        }
                                    }
                                    items.setProvincesModelArrayList(provincesModelArrayList);
                                }catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                            else if (jsItems.getInt("MenuItem_menuItem_id") == 10){
                                items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                                items.setTopicTH(jsItems.getString("itmes_topicThai"));
                                items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                                items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                                items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                                items.setLatitude(jsItems.getString("items_latitude"));
                                items.setLongitude(jsItems.getString("items_longitude"));
                                items.setContactTH(jsItems.getString("items_contactThai"));
                                items.setContactEN(jsItems.getString("items_contactEnglish"));
                                items.setContactLO(jsItems.getString("items_contactLaos"));
                                items.setContactZH(jsItems.getString("items_contactChinese"));
                                items.setPhone(jsItems.getString("items_phone"));
                                items.setEmail(jsItems.getString("items_email"));
                                items.setLine(jsItems.getString("items_line"));
                                items.setFacebookPage(jsItems.getString("items_facebookPage"));
                                items.setArURL(jsItems.getString("item_ar_url"));
                                items.setIsActive(jsItems.getInt("items_isActive"));
                                items.setIsPublish(jsItems.getInt("items_isPublish"));
                                items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                                items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                                items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                                items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                                items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                                items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                                items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                                items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
                                items.setPriceGuestAdult(Integer.parseInt(jsItems.getString("items_priceguestAdult")));
                                items.setPriceGuestChild(Integer.parseInt(jsItems.getString("items_priceguestChild")));
                                menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                                menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                                menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                                menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                                menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                                items.setMenuItemModel(menuItem);
                                subCategory.setCategoryId(Integer.parseInt(jsItems.getString("subcategory_id")));
                                subCategory.setCategoryTH(jsItems.getString("subcategory_thai"));
                                subCategory.setCategoryEN(jsItems.getString("subcategory_english"));
                                subCategory.setCategoryLO(jsItems.getString("subcategory_laos"));
                                subCategory.setCategoryZH(jsItems.getString("subcategory_chinese"));
                                subCategoryArrayList.add(subCategory);
                                items.setSubCategoryModelArrayList(subCategoryArrayList);

                                ProvincesModel provinces = new ProvincesModel();
                                provinces.setProvincesId(jsItems.getInt("provinces_id"));
                                provinces.setProvincesCode(jsItems.getInt("provinces_code"));
                                provinces.setProvincesTH(jsItems.getString("provinces_thai"));
                                provinces.setProvincesEN(jsItems.getString("provinces_english"));
                                provinces.setProvincesLO(jsItems.getString("provinces_laos"));
                                provinces.setProvincesZH(jsItems.getString("provinces_chinese"));
                                DistrictsModel districts = new DistrictsModel();
                                districts.setDistrictsId(jsItems.getInt("districts_id"));
                                districts.setDistrictsCode(jsItems.getInt("districts_code"));
                                districts.setDistrictsTH(jsItems.getString("districts_thai"));
                                districts.setDistrictsEN(jsItems.getString("districts_english"));
                                districts.setDistrictsLO(jsItems.getString("districts_laos"));
                                districts.setDistrictsZH(jsItems.getString("districts_chinese"));
                                provinces.setDistrictsModel(districts);
                                SubDistrictsModel subDistricts = new SubDistrictsModel();
                                subDistricts.setSubDistrictsId(jsItems.getInt("subdistricts_id"));
                                subDistricts.setSubDistrictsCode(jsItems.getInt("subdistricts_zip_code"));
                                subDistricts.setSubDistrictsTH(jsItems.getString("subdistricts_thai"));
                                subDistricts.setSubDistrictsEN(jsItems.getString("subdistricts_english"));
                                subDistricts.setSubDistrictsLO(jsItems.getString("subdistricts_laos"));
                                subDistricts.setSubDistrictsZH(jsItems.getString("subdistricts_chinese"));
                                districts.setSubDistrictsModel(subDistricts);
                                items.setProvincesModel(provinces);

                                try {
                                    JSONArray jsonCover = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("CoverItems");
                                    ArrayList<CoverItemsModel> coverItemsModelsArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonCover.length(); k++) {
                                        JSONObject jsCover = jsonCover.getJSONObject(k);
                                        CoverItemsModel coverItemsModels = new CoverItemsModel();

                                        if (jsCover != null) {
                                            coverItemsModels.setCoverId(jsCover.getInt("coverItem_id"));
                                            coverItemsModels.setCoverPaths(jsCover.getString("coverItem_paths"));
                                            if (jsCover.getString("coverItem_url") != null && jsCover.getString("coverItem_url") != "" && !jsCover.getString("coverItem_url").equals("")) {
                                                coverItemsModels.setCoverURL(jsCover.getString("coverItem_url"));
                                            } else {
                                                coverItemsModels.setCoverURL(null);
                                            }
                                            coverItemsModelsArrayList.add(coverItemsModels);
                                        }
                                    }
                                    items.setCoverItemsModelArrayList(coverItemsModelsArrayList);
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonDetails = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("Detail");
                                    ArrayList<ItemsDetailModel> itemsDetailModelArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonDetails.length(); k++) {
                                        JSONObject jsDetail = jsonDetails.getJSONObject(k);
                                        ItemsDetailModel itemsDetail = new ItemsDetailModel();
                                        ItemsTopicDetailModel topicDetail = new ItemsTopicDetailModel();
                                        JSONArray jsonPhoto = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("Detail").getJSONObject(k).getJSONArray("Photo");

                                        if (jsDetail != null){
                                            itemsDetail.setDetailId(jsDetail.getInt("detail_id"));
                                            itemsDetail.setDetailTH(jsDetail.getString("detail_textThai"));
                                            itemsDetail.setDetailEN(jsDetail.getString("detail_textEnglish"));
                                            itemsDetail.setDetailLO(jsDetail.getString("detail_textLaos"));
                                            itemsDetail.setDetailZH(jsDetail.getString("detail_textChinese"));
                                            topicDetail.setTopicDetailId(jsDetail.getInt("topicdetail_id"));
                                            topicDetail.setTopicDetailTH(jsDetail.getString("topicdetail_Thai"));
                                            topicDetail.setTopicDetailEN(jsDetail.getString("topicdetail_English"));
                                            topicDetail.setTopicDetailLO(jsDetail.getString("topicdetail_Laos"));
                                            topicDetail.setTopicDetailZH(jsDetail.getString("topicdetail_Chinese"));
                                            itemsDetail.setItemsTopicDetailModel(topicDetail);
                                            itemsDetailModelArrayList.add(itemsDetail);

                                            ArrayList<ItemsPhotoDetailModel> photoDetailModelArrayList = new ArrayList<>();
                                            for (int l = 0; l < jsonPhoto.length(); l++){
                                                JSONObject jsPhoto = jsonPhoto.getJSONObject(l);
                                                ItemsPhotoDetailModel photoDetail = new ItemsPhotoDetailModel();

                                                if (jsPhoto != null){
                                                    photoDetail.setPhotoId(jsPhoto.getInt("photo_id"));
                                                    photoDetail.setPhotoPaths(jsPhoto.getString("photo_paths"));
                                                    photoDetail.setPhotoTextTH(jsPhoto.getString("photo_textThai"));
                                                    photoDetail.setPhotoTextEN(jsPhoto.getString("photo_textEnglish"));
                                                    photoDetail.setPhotoTextLO(jsPhoto.getString("photo_textLaos"));
                                                    photoDetail.setPhotoTextZH(jsPhoto.getString("photo_textChinese"));
                                                    photoDetailModelArrayList.add(photoDetail);
                                                }

                                            }
                                            itemsDetail.setItemsPhotoDetailModels(photoDetailModelArrayList);

                                        }
                                        items.setItemsDetailModelArrayList(itemsDetailModelArrayList);
                                    }
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }


                                try {
                                    JSONArray jsonPeriod = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("TimePeriod");
                                    ArrayList<PeriodTimeModel> periodTimeModelArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonPeriod.length() ; k++) {
                                        JSONObject jsPeriod = jsonPeriod.getJSONObject(k);
                                        PeriodTimeModel periodTime = new PeriodTimeModel();
                                        PeriodDayModel periodDay = new PeriodDayModel();

                                        if (jsPeriod != null){
                                            periodTime.setPeriodId(jsPeriod.getInt("itemsHasDayOfWeek_id"));
                                            periodTime.setPeriodOpen(jsPeriod.getString("items_timeOpen"));
                                            periodTime.setPeriodClose(jsPeriod.getString("items_timeClose"));

                                            JSONArray jsonOpen = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("TimePeriod").getJSONObject(k).getJSONArray("item_dayOpen_data");
                                            JSONArray jsonClose = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("TimePeriod").getJSONObject(k).getJSONArray("item_dayClose_data");

                                            for (int l = 0; l < jsonOpen.length() ; l++) {
                                                JSONObject jsOpen = jsonOpen.getJSONObject(l);
                                                if (jsOpen != null){
                                                    periodDay.setPeriodOpenTH(jsOpen.getString("dayofweek_thai"));
                                                    periodDay.setPeriodOpenEN(jsOpen.getString("dayofweek_English"));
                                                    periodDay.setPeriodOpenLO(jsOpen.getString("dayofweek_Chinese"));
                                                    periodDay.setPeriodOpenZH(jsOpen.getString("dayofweek_Laos"));
                                                }
                                            }
                                            for (int l = 0; l < jsonClose.length() ; l++) {
                                                JSONObject jsClose = jsonClose.getJSONObject(l);
                                                if (jsClose != null){
                                                    periodDay.setPeriodOpenTH(jsClose.getString("dayofweek_thai"));
                                                    periodDay.setPeriodOpenEN(jsClose.getString("dayofweek_English"));
                                                    periodDay.setPeriodOpenLO(jsClose.getString("dayofweek_Chinese"));
                                                    periodDay.setPeriodOpenZH(jsClose.getString("dayofweek_Laos"));
                                                }
                                            }
                                            periodTime.setPeriodDayModel(periodDay);
                                            periodTimeModelArrayList.add(periodTime);
                                        }
                                        items.setPeriodTimeModelArrayList(periodTimeModelArrayList);
                                        items.setPeriodDayModel(periodDay);
                                        items.setPeriodTimeModel(periodTime);
                                    }
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonCondition = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("BookingCondition");
                                    ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonCondition.length() ; k++) {
                                        JSONObject jsCondition = jsonCondition.getJSONObject(k);
                                        BookingConditionModel conditionModel = new BookingConditionModel();

                                        if (jsCondition != null){
                                            conditionModel.setConditionTopicTH(jsCondition.getString("bookingConditionCategory_topicThai"));
                                            conditionModel.setConditionTopicEN(jsCondition.getString("bookingConditionCategory_topicEnglish"));
                                            conditionModel.setConditionTopicLO(jsCondition.getString("bookingConditionCategory_topicChinese"));
                                            conditionModel.setConditionTopicZH(jsCondition.getString("bookingConditionCategory_topicLaos"));
                                            conditionModel.setConditionDetailTH(jsCondition.getString("bookingConditioncol_detailThai"));
                                            conditionModel.setConditionDetailEN(jsCondition.getString("bookingConditioncol_detailEnglish"));
                                            conditionModel.setConditionDetailLO(jsCondition.getString("bookingConditioncol_detailChinese"));
                                            conditionModel.setConditionDetailZH(jsCondition.getString("bookingConditioncol_detailLaos"));

                                            bookingConditionModelArrayList.add(conditionModel);
                                        }
                                    }
                                    items.setBookingConditionModelArrayList(bookingConditionModelArrayList);
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonBusiness = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("Business");
                                    for (int k = 0; k < jsonBusiness.length() ; k++) {
                                        JSONObject jsBusiness = jsonBusiness.getJSONObject(k);
                                        if (jsBusiness != null) {
                                            BusinessModel business = new BusinessModel();
                                            business.setBusinessId(jsBusiness.getInt("business_id"));
                                            business.setNameTH(jsBusiness.getString("business_nameThai"));
                                            business.setNameEN(jsBusiness.getString("business_nameEnglish"));
                                            business.setNameLO(jsBusiness.getString("business_nameLaos"));
                                            business.setNameZH(jsBusiness.getString("business_nameChinese"));
                                            business.setLatitude((float) jsBusiness.getDouble("business_latitude"));
                                            business.setLongitude((float) jsBusiness.getDouble("business_longitude"));
                                            business.setPresentAddress(jsBusiness.getString("business_presentAddress"));
                                            business.setLicensePaths(jsBusiness.getString("business_license_paths"));
                                            business.setOperatorNumber(jsBusiness.getString("business_operator_number"));
                                            business.setPhone(jsBusiness.getString("business_phone"));
                                            business.setWeb(jsBusiness.getString("business_www"));
                                            business.setFacebook(jsBusiness.getString("business_facebook"));
                                            business.setLine(jsBusiness.getString("business_line"));
                                            items.setBusinessModel(business);
                                        }
                                    }
                                }catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                            else if (jsItems.getInt("MenuItem_menuItem_id") == 11){
                                items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                                items.setTopicTH(jsItems.getString("itmes_topicThai"));
                                items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                                items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                                items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                                items.setLatitude(jsItems.getString("items_latitude"));
                                items.setLongitude(jsItems.getString("items_longitude"));
                                items.setContactTH(jsItems.getString("items_contactThai"));
                                items.setContactEN(jsItems.getString("items_contactEnglish"));
                                items.setContactLO(jsItems.getString("items_contactLaos"));
                                items.setContactZH(jsItems.getString("items_contactChinese"));
                                items.setPhone(jsItems.getString("items_phone"));
                                items.setEmail(jsItems.getString("items_email"));
                                items.setLine(jsItems.getString("items_line"));
                                items.setFacebookPage(jsItems.getString("items_facebookPage"));
                                items.setArURL(jsItems.getString("item_ar_url"));
                                items.setIsActive(jsItems.getInt("items_isActive"));
                                items.setIsPublish(jsItems.getInt("items_isPublish"));
                                items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                                items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                                items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                                items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                                items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                                items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                                items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                                items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
                                items.setPriceGuestAdult(Integer.parseInt(jsItems.getString("items_priceguestAdult")));
                                items.setPriceGuestChild(Integer.parseInt(jsItems.getString("items_priceguestChild")));
                                menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                                menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                                menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                                menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                                menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                                items.setMenuItemModel(menuItem);

                                CoverItemsModel coverItemsModels = new CoverItemsModel();
                                coverItemsModels.setCoverId(jsItems.getInt("coverItem_id"));
                                coverItemsModels.setCoverPaths(jsItems.getString("coverItem_paths"));
                                if (jsItems.getString("coverItem_url") != null && jsItems.getString("coverItem_url") != "" && !jsItems.getString("coverItem_url").equals("")) {
                                    coverItemsModels.setCoverURL(jsItems.getString("coverItem_url"));
                                } else {
                                    coverItemsModels.setCoverURL(null);
                                }
                                items.setCoverItemsModel(coverItemsModels);

                                CarModel carModel = new CarModel();
                                carModel.setSeats(jsItems.getInt("items_carSeats"));
                                carModel.setPricePerDay(jsItems.getInt("items_PricePerDay"));
                                carModel.setReturnPrice(jsItems.getInt("items_CarDeliveryPrice"));
                                carModel.setPickUpPrice(jsItems.getInt("items_PickUpPrice"));
                                carModel.setDepositPrice(jsItems.getInt("items_DepositPrice"));
                                if (jsItems.getString("items_isBasicInsurance").equalsIgnoreCase("1")){
                                    carModel.setBasicInsurance(true);
                                }else {
                                    carModel.setBasicInsurance(false);
                                }
                                carModel.setBasicInsuranceTH(jsItems.getString("items_BissicInsurance_detailThai"));
                                carModel.setBasicInsuranceEN(jsItems.getString("items_BissicInsurance_detailEnglish"));
                                carModel.setBasicInsuranceLO(jsItems.getString("items_BissicInsurance_detailLaos"));
                                carModel.setBasicInsuranceZH(jsItems.getString("items_BissicInsurance_detailChinese"));
                                carModel.setTypeTH(jsItems.getString("typesofCars_Thai"));
                                carModel.setTypeEN(jsItems.getString("typesofCars_English"));
                                carModel.setTypeLO(jsItems.getString("typesofCars_Laos"));
                                carModel.setTypeZH(jsItems.getString("typesofCars_Chinese"));
                                carModel.setGearSystemTH(jsItems.getString("gearSystem_Thai"));
                                carModel.setGearSystemEN(jsItems.getString("gearSystem_English"));
                                carModel.setGearSystemLO(jsItems.getString("gearSystem_Laos"));
                                carModel.setGearSystemZH(jsItems.getString("gearSystem_Chinese"));
                                items.setCarModel(carModel);

                                try {
                                    JSONArray jsonCondition = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("BookingCondition");
                                    ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                                    for (int k = 0; k < jsonCondition.length() ; k++) {
                                        JSONObject jsCondition = jsonCondition.getJSONObject(k);
                                        BookingConditionModel conditionModel = new BookingConditionModel();

                                        if (jsCondition != null){
                                            conditionModel.setConditionId(jsCondition.getInt("bookingConditionCategory_id"));
                                            conditionModel.setConditionTopicTH(jsCondition.getString("bookingConditionCategory_topicThai"));
                                            conditionModel.setConditionTopicEN(jsCondition.getString("bookingConditionCategory_topicEnglish"));
                                            conditionModel.setConditionTopicLO(jsCondition.getString("bookingConditionCategory_topicLaos"));
                                            conditionModel.setConditionTopicZH(jsCondition.getString("bookingConditionCategory_topicChinese"));
                                            conditionModel.setConditionDetailTH(jsCondition.getString("bookingConditioncol_detailThai"));
                                            conditionModel.setConditionDetailEN(jsCondition.getString("bookingConditioncol_detailEnglish"));
                                            conditionModel.setConditionDetailLO(jsCondition.getString("bookingConditioncol_detailLaos"));
                                            conditionModel.setConditionDetailZH(jsCondition.getString("bookingConditioncol_detailChinese"));

                                            bookingConditionModelArrayList.add(conditionModel);
                                        }
                                    }
                                    items.setBookingConditionModelArrayList(bookingConditionModelArrayList);
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                                try {
                                    JSONArray jsonBusiness = object.optJSONArray("Booking").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("Business");
                                    for (int k = 0; k < jsonBusiness.length() ; k++) {
                                        JSONObject jsBusiness = jsonBusiness.getJSONObject(k);
                                        if (jsBusiness != null) {
                                            BusinessModel business = new BusinessModel();
                                            business.setBusinessId(jsBusiness.getInt("business_id"));
                                            business.setNameTH(jsBusiness.getString("business_nameThai"));
                                            business.setNameEN(jsBusiness.getString("business_nameEnglish"));
                                            business.setNameLO(jsBusiness.getString("business_nameLaos"));
                                            business.setNameZH(jsBusiness.getString("business_nameChinese"));
                                            business.setLatitude((float) jsBusiness.getDouble("business_latitude"));
                                            business.setLongitude((float) jsBusiness.getDouble("business_longitude"));
                                            business.setPresentAddress(jsBusiness.getString("business_presentAddress"));
                                            business.setLicensePaths(jsBusiness.getString("business_license_paths"));
                                            business.setOperatorNumber(jsBusiness.getString("business_operator_number"));
                                            business.setPhone(jsBusiness.getString("business_phone"));
                                            business.setWeb(jsBusiness.getString("business_www"));
                                            business.setFacebook(jsBusiness.getString("business_facebook"));
                                            business.setLine(jsBusiness.getString("business_line"));
                                            items.setBusinessModel(business);
                                        }
                                    }
                                }catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                            else {
                                Log.e("Err ","Not Condition BookingItemsAsyncTask()");
                            }

                        }
                        booking.setItemsModel(items);
                    }

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
