package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.PaymentTransactionModel;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.ProductOrderDetails;
import com.ipanda.lanexangtourism.Model.PurchaseStatusModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.interface_callback.BookingOrderDetailsCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class BookingOrderDetailsAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<BookingModel> arrayList;
    private BookingOrderDetailsCallBack callBackService;

    public BookingOrderDetailsAsyncTask(BookingOrderDetailsCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerOrderDetails(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<BookingModel>  onParserContentToModel(String dataJSon) {
        ArrayList<BookingModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonBooking = object.optJSONArray("OrderDetails");

            for (int i = 0; i < jsonBooking.length() ; i++) {
                JSONObject jsBooking = jsonBooking.getJSONObject(i);
                BookingModel booking = new BookingModel();
                PaymentTransactionModel payment = new PaymentTransactionModel();
                PurchaseStatusModel purchaseStatus = new PurchaseStatusModel();

                JSONArray jsonOrder = object.optJSONArray("OrderDetails").getJSONObject(i).getJSONArray("OrderData");
                JSONArray jsonItems = object.optJSONArray("OrderDetails").getJSONObject(i).getJSONArray("ItemData");

                if (jsBooking != null){
                    payment.setPaymentId(jsBooking.getInt("paymentTransaction_id"));
                    payment.setTimestamp(jsBooking.getString("paymentTransaction_timestamp"));
                    payment.setTotal(jsBooking.getDouble("PaymentTransaction_totalPrice"));
                    payment.setFirstName(jsBooking.getString("paymentTransaction_firstName"));
                    payment.setLastName(jsBooking.getString("paymentTransaction_lastName"));
                    payment.setEmail(jsBooking.getString("paymentTransaction_email"));
                    payment.setPhone(jsBooking.getString("paymentTransaction_phone"));
                    payment.setWeChatId(jsBooking.getString("paymentTransaction_weChatId"));
                    payment.setWhatsAppId(jsBooking.getString("paymentTransaction_whatsAppId"));
//                    payment.setDiscount(jsBooking.getString(""));
                    purchaseStatus.setStatusId(jsBooking.getInt("purchaseStatus_id"));
                    purchaseStatus.setStatusTH(jsBooking.getString("purchaseStatus_statusThai"));
                    purchaseStatus.setStatusEN(jsBooking.getString("purchaserStatus_statusEnglish"));
                    purchaseStatus.setStatusZH(jsBooking.getString("purchaseStatus_statusChinese"));
                    purchaseStatus.setStatusLO(jsBooking.getString("purchaseStatus_statusLaos"));
                    payment.setPurchaseStatusModel(purchaseStatus);
                    booking.setPaymentTransactionModel(payment);


                    for (int j = 0; j < jsonItems.length() ; j++) {
                        JSONObject jsItems = jsonItems.getJSONObject(j);
                        ItemsModel items = new ItemsModel();
                        MenuItemModel menuItem = new MenuItemModel();
                        CoverItemsModel cover = new CoverItemsModel();
                        RoomModel roomModel = new RoomModel();
                        ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                        SubCategoryModel subCategory = new SubCategoryModel();

                        if (jsItems != null) {
                            items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                            items.setTopicTH(jsItems.getString("itmes_topicThai"));
                            items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                            items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                            items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                            items.setLatitude(jsItems.getString("items_latitude"));
                            items.setLongitude(jsItems.getString("items_longitude"));
                            items.setContactTH(jsItems.getString("items_contactThai"));
                            items.setContactEN(jsItems.getString("items_contactEnglish"));
                            items.setContactLO(jsItems.getString("items_contactLaos"));
                            items.setContactZH(jsItems.getString("items_contactChinese"));
                            items.setPhone(jsItems.getString("items_phone"));
                            items.setEmail(jsItems.getString("items_email"));
                            items.setLine(jsItems.getString("items_line"));
                            items.setFacebookPage(jsItems.getString("items_facebookPage"));
                            items.setArURL(jsItems.getString("item_ar_url"));
                            items.setIsActive(jsItems.getInt("items_isActive"));
                            items.setIsPublish(jsItems.getInt("items_isPublish"));
                            items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                            items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                            items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                            items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                            items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                            items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                            items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                            items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
                            items.setCreateDatetime(jsItems.getString("items_createdDTTM"));
                            items.setUpdateDatetime(jsItems.getString("items_updatedDTTM"));
                            items.setTimeOpen(jsItems.getString("items_timeOpen"));
                            items.setTimeClose(jsItems.getString("items_timeClose"));
                            menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                            menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                            menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                            menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                            menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                            items.setMenuItemModel(menuItem);
                            subCategory.setCategoryId(Integer.parseInt(jsItems.getString("subcategory_id")));
                            subCategory.setCategoryTH(jsItems.getString("subcategory_thai"));
                            subCategory.setCategoryEN(jsItems.getString("subcategory_english"));
                            subCategory.setCategoryLO(jsItems.getString("subcategory_laos"));
                            subCategory.setCategoryZH(jsItems.getString("subcategory_chinese"));
                            subCategoryArrayList.add(subCategory);
                            items.setSubCategoryModelArrayList(subCategoryArrayList);
                            cover.setCoverId(jsItems.getInt("coverItem_id"));
                            cover.setCoverPaths(jsItems.getString("coverItem_paths"));
                            if (jsItems.getString("coverItem_url") != null && jsItems.getString("coverItem_url") != "" && !jsItems.getString("coverItem_url").equals("")) {
                                cover.setCoverURL(jsItems.getString("coverItem_url"));
                            } else {
                                cover.setCoverURL(null);
                            }
                            items.setCoverItemsModel(cover);
                        }

                        booking.setItemsModel(items);
                    }

                    ArrayList<ProductModel> productModelArrayList = new ArrayList<>();

                    for (int j = 0; j < jsonOrder.length() ; j++) {
                        JSONObject jsOrder = jsonOrder.getJSONObject(j);
                        ProductModel product = new ProductModel();
                        ProductOrderDetails orderDetails = new ProductOrderDetails();

                        if (jsOrder != null){
                            orderDetails.setOderId(jsOrder.getInt("orderDetails_id"));
                            orderDetails.setOderTimestamp(jsOrder.getString("orderDetails_timestamp"));
                            orderDetails.setQuantity(jsOrder.getInt("orderDetails_quantity"));
                            orderDetails.setPrice(jsOrder.getDouble("orderDetails_Price"));
                            orderDetails.setTotalPrice(jsOrder.getDouble("orderDetails_totalPrice"));

                            JSONArray jsonProduct = object.optJSONArray("OrderDetails").getJSONObject(i).getJSONArray("OrderData").getJSONObject(j).getJSONArray("Product");
                            for (int k = 0; k < jsonProduct.length() ; k++) {
                                JSONObject jsProduct = jsonProduct.getJSONObject(k);

                                if (jsProduct != null){
                                    product.setProductId(jsProduct.getInt("product_id"));
                                    product.setProductNamesTH(jsProduct.getString("product_namesThai"));
                                    product.setProductNamesEN(jsProduct.getString("product_namesEnglish"));
                                    product.setProductNamesLO(jsProduct.getString("product_namesLaos"));
                                    product.setProductNamesZH(jsProduct.getString("product_namesChinese"));
                                    product.setProductPrice(jsProduct.getDouble("product_price"));
                                    product.setProductOrderDetails(orderDetails);
                                    productModelArrayList.add(product);
                                }
                            }
                        }

                    }
                    booking.setProductModelArrayList(productModelArrayList);

                }

                list.add(booking);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
