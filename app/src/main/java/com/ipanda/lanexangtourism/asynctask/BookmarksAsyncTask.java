package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BookMarksModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.RatingStarModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.interface_callback.BookmarksCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class BookmarksAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<BookMarksModel> arrayList;
    private BookmarksCallBack callBackService;

    public BookmarksAsyncTask(BookmarksCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<BookMarksModel>  onParserContentToModel(String dataJSon) {
        ArrayList<BookMarksModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonBookmarks = object.optJSONArray("BookMarks");

            for (int i = 0; i < jsonBookmarks.length() ; i++) {
                JSONObject jsBookmarks = jsonBookmarks.getJSONObject(i);
                BookMarksModel bookmarks = new BookMarksModel();

                if (jsBookmarks != null) {
                    bookmarks.setBookMarkId(jsBookmarks.getInt("bookmarks_id"));
                    bookmarks.setBookMarkTimestamp(jsBookmarks.getString("bookmarks_timestamp"));
                    bookmarks.setCountUserBookMarks(jsBookmarks.getInt("CountUser_BookMarks"));

                    JSONArray jsonUser = object.optJSONArray("BookMarks").getJSONObject(i).getJSONArray("User_BookMarks");
                    ArrayList<UserModel> userModelArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonUser.length() ; j++) {
                        JSONObject jsUser = jsonUser.getJSONObject(j);
                        UserModel user = new UserModel();

                        if (jsUser != null) {
                            user.setUserId(jsUser.getInt("user_id"));
                            user.setIdentification(jsUser.getString("user_identification"));
                            user.setToken(jsUser.getString("user_token"));
                            user.setEmail(jsUser.getString("user_email"));
                            user.setFirstName(jsUser.getString("user_firstName"));
                            user.setLastName(jsUser.getString("user_lastName"));
                            user.setAge(jsUser.getString("user_age"));
                            user.setGender(jsUser.getString("user_gender"));
                            user.setPresentAddress(jsUser.getString("user_presentAddress"));
                            user.setLoginWith(jsUser.getString("user_loginWith"));
                            user.setProfilePicUrl(jsUser.getString("user_profile_pic_url"));
                            user.setFollowState(jsUser.getBoolean("follow_state"));
                            userModelArrayList.add(user);
                        }
                    }
                    bookmarks.setUserModelArrayList(userModelArrayList);

                    JSONArray jsonItems = object.optJSONArray("BookMarks").getJSONObject(i).getJSONArray("ItemData");
                    ArrayList<ItemsModel> itemsModelArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonItems.length(); j++) {
                        JSONObject jsItems = jsonItems.getJSONObject(j);
                        ItemsModel items = new ItemsModel();
                        ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                        MenuItemModel menuItem = new MenuItemModel();
                        SubCategoryModel subCategory = new SubCategoryModel();
                        CoverItemsModel cover = new CoverItemsModel();

                        JSONArray jsonRating = object.optJSONArray("BookMarks").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("Review");

                        if (jsItems != null) {
                            items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                            items.setTopicTH(jsItems.getString("itmes_topicThai"));
                            items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                            items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                            items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                            items.setLatitude(jsItems.getString("items_latitude"));
                            items.setLongitude(jsItems.getString("items_longitude"));
                            items.setContactTH(jsItems.getString("items_contactThai"));
                            items.setContactEN(jsItems.getString("items_contactEnglish"));
                            items.setContactLO(jsItems.getString("items_contactLaos"));
                            items.setContactZH(jsItems.getString("items_contactChinese"));
                            items.setPhone(jsItems.getString("items_phone"));
                            items.setEmail(jsItems.getString("items_email"));
                            items.setLine(jsItems.getString("items_line"));
                            items.setFacebookPage(jsItems.getString("items_facebookPage"));
                            items.setArURL(jsItems.getString("item_ar_url"));
//                            items.setPrice(jsItems.getDouble("items_price"));
                            items.setIsActive(jsItems.getInt("items_isActive"));
                            items.setIsPublish(jsItems.getInt("items_isPublish"));
                            items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                            items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                            items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                            items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                            items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                            items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                            items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                            items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
                            items.setTimeOpen(jsItems.getString("items_timeOpen"));
                            items.setTimeClose(jsItems.getString("items_timeClose"));
                            menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                            menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                            menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                            menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                            menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                            items.setMenuItemModel(menuItem);
                            subCategory.setCategoryId(Integer.parseInt(jsItems.getString("subcategory_id")));
                            subCategory.setCategoryTH(jsItems.getString("subcategory_thai"));
                            subCategory.setCategoryEN(jsItems.getString("subcategory_english"));
                            subCategory.setCategoryLO(jsItems.getString("subcategory_laos"));
                            subCategory.setCategoryZH(jsItems.getString("subcategory_chinese"));
                            subCategoryArrayList.add(subCategory);
                            items.setSubCategoryModelArrayList(subCategoryArrayList);
                            cover.setCoverId(jsItems.getInt("coverItem_id"));
                            cover.setCoverPaths(jsItems.getString("coverItem_paths"));
                            bookmarks.setBookmarksState(jsItems.getBoolean("BookMarks_state"));
                            items.setDistance(jsItems.getInt("Distance"));
                            items.setDistance_metr(jsItems.getInt("Distance_metr"));
                            bookmarks.setDistance(jsItems.getInt("Distance"));
                            bookmarks.setDistanceMetr(jsItems.getInt("Distance_metr"));
                            if (jsItems.getString("coverItem_url") != null && jsItems.getString("coverItem_url") != "" && !jsItems.getString("coverItem_url").equals("")) {
                                cover.setCoverURL(jsItems.getString("coverItem_url"));
                            } else {
                                cover.setCoverURL(null);
                            }
                            items.setCoverItemsModel(cover);
                            itemsModelArrayList.add(items);

                            RatingStarModel ratingStarModel = new RatingStarModel();
                            for (int k = 0; k < jsonRating.length() ; k++) {
                                JSONObject jsRating = jsonRating.getJSONObject(k);
                                JSONArray jsonReview = object.optJSONArray("BookMarks").getJSONObject(i).getJSONArray("ItemData").getJSONObject(j).getJSONArray("Review").getJSONObject(k).getJSONArray("Reviews");

                                if (jsRating != null){
                                    ratingStarModel.setAverageReview(jsRating.getInt("AverageReview"));
                                    ratingStarModel.setCountReview(jsRating.getInt("CountReview"));
                                    ratingStarModel.setOneStar(jsRating.getInt("OneStar"));
                                    ratingStarModel.setTwoStar(jsRating.getInt("TwoStar"));
                                    ratingStarModel.setThreeStar(jsRating.getInt("ThreeStar"));
                                    ratingStarModel.setFourStar(jsRating.getInt("FourStar"));
                                    ratingStarModel.setFiveStar(jsRating.getInt("FiveStar"));

                                    ArrayList<ReviewModel> reviewModelArrayList = new ArrayList<>();
                                    for (int l = 0; l < jsonReview.length() ; l++) {
                                        JSONObject jsReview = jsonReview.getJSONObject(l);
                                        ReviewModel reviewModel = new ReviewModel();
                                        UserModel userModel = new UserModel();


                                        if (jsReview != null){
                                            reviewModel.setReviewId(jsReview.getInt("review_id"));
                                            reviewModel.setReviewTimestamp(jsReview.getString("review_timestamp"));
                                            reviewModel.setReviewRating(jsReview.getInt("review_rating"));
                                            reviewModel.setReviewText(jsReview.getString("review_text"));
                                            userModel.setUserId(jsReview.getInt("User_user_id"));
                                            userModel.setFirstName(jsReview.getString("user_firstName"));
                                            userModel.setLastName(jsReview.getString("user_lastName"));
                                            userModel.setProfilePicUrl(jsReview.getString("user_profile_pic_url"));
                                            ItemsModel it = new ItemsModel();
                                            it.setItemsId(jsReview.getInt("Items_items_id"));
                                            it.setTopicTH(jsReview.getString("itmes_topicThai"));
                                            it.setTopicEN(jsReview.getString("itmes_topicEnglish"));
                                            it.setTopicLO(jsReview.getString("itmes_topicLaos"));
                                            it.setTopicZH(jsReview.getString("itmes_topicChinese"));
                                            reviewModel.setItemsModel(it);
                                            reviewModel.setUserModel(userModel);
                                        }
                                        reviewModelArrayList.add(reviewModel);
                                    }
                                    ratingStarModel.setReviewModelArrayList(reviewModelArrayList);
                                }
                                items.setRatingStarModel(ratingStarModel);
                            }
                        }
                    }
                    bookmarks.setItemsModelArrayList(itemsModelArrayList);
                }
                list.add(bookmarks);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
