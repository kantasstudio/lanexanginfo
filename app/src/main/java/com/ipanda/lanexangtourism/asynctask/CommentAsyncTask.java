package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.interface_callback.CommentCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class CommentAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<ReviewModel> arrayList;
    private CommentCallBack callBackService;

    public CommentAsyncTask(CommentCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallServiceComment();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallServiceComment();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteComment(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ReviewModel>  onParserContentToModel(String dataJSon) {
        ArrayList<ReviewModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonRating = object.optJSONArray("Review");

            for (int j = 0; j < jsonRating.length() ; j++) {
                JSONObject jsRating = jsonRating.getJSONObject(j);

                JSONArray jsonReview = object.optJSONArray("Review").getJSONObject(j).getJSONArray("Reviews");
                if (jsRating != null){
                    for (int k = 0; k < jsonReview.length() ; k++) {
                        JSONObject jsReview = jsonReview.getJSONObject(k);
                        ReviewModel reviewModel = new ReviewModel();
                        UserModel userModel = new UserModel();
                        ItemsModel itemsModel = new ItemsModel();
                        ProgramTourModel tourModel = new ProgramTourModel();

                        if (jsReview != null){
                            reviewModel.setReviewId(jsReview.getInt("review_id"));
                            reviewModel.setReviewTimestamp(jsReview.getString("review_timestamp"));
                            reviewModel.setReviewRating(jsReview.getInt("review_rating"));
                            reviewModel.setReviewText(jsReview.getString("review_text"));
                            userModel.setUserId(jsReview.getInt("User_user_id"));
                            userModel.setFirstName(jsReview.getString("user_firstName"));
                            userModel.setLastName(jsReview.getString("user_lastName"));
                            userModel.setProfilePicUrl(jsReview.getString("user_profile_pic_url"));
                            tourModel.setProgramTourId(jsReview.getInt("ProgramTour_programTour_id"));
                            tourModel.setProgramTourNameTH(jsReview.getString("programTour_nameThai"));
                            tourModel.setProgramTourNameEN(jsReview.getString("programTour_nameEnglish"));
                            tourModel.setProgramTourNameLO(jsReview.getString("programTour_nameChinese"));
                            tourModel.setProgramTourNameZH(jsReview.getString("programTour_nameLaos"));
                            reviewModel.setProgramTourModel(tourModel);
                            reviewModel.setItemsModel(itemsModel);
                            reviewModel.setUserModel(userModel);
                            list.add(reviewModel);
                        }

                    }

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
