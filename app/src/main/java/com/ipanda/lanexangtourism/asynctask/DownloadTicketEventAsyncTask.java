package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.Model.BusinessModel;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.DistrictsModel;
import com.ipanda.lanexangtourism.Model.ItemsDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsTopicDetailModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.PeriodDayModel;
import com.ipanda.lanexangtourism.Model.PeriodTimeModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.SubDistrictsModel;
import com.ipanda.lanexangtourism.interface_callback.DownloadTicketEventCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsTicketEventTourCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class DownloadTicketEventAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<ItemsModel> arrayList;
    private DownloadTicketEventCallBack callBackService;

    public DownloadTicketEventAsyncTask(DownloadTicketEventCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallServiceDownload();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallServiceDownload();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerDownloadTicketEvent(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ItemsModel>  onParserContentToModel(String dataJSon) {
        ArrayList<ItemsModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonItems = object.optJSONArray("Items");

            for (int i = 0; i < jsonItems.length(); i++) {
                JSONObject jsItems = jsonItems.getJSONObject(i);
                ItemsModel items = new ItemsModel();
                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                MenuItemModel menuItem = new MenuItemModel();
                CategoryModel categoryModel= new CategoryModel();
                SubCategoryModel subCategory = new SubCategoryModel();


                JSONArray jsonCover = object.optJSONArray("Items").getJSONObject(i).getJSONArray("CoverItems");
                JSONArray jsonDetails = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Detail");
                JSONArray jsonPeriod = object.optJSONArray("Items").getJSONObject(i).getJSONArray("TimePeriod");
                JSONArray jsonCondition = object.optJSONArray("Items").getJSONObject(i).getJSONArray("BookingCondition");

                if (jsItems != null) {
                    items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                    items.setTopicTH(jsItems.getString("itmes_topicThai"));
                    items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                    items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                    items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                    items.setLatitude(jsItems.getString("items_latitude"));
                    items.setLongitude(jsItems.getString("items_longitude"));
                    items.setContactTH(jsItems.getString("items_contactThai"));
                    items.setContactEN(jsItems.getString("items_contactEnglish"));
                    items.setContactLO(jsItems.getString("items_contactLaos"));
                    items.setContactZH(jsItems.getString("items_contactChinese"));
                    items.setPhone(jsItems.getString("items_phone"));
                    items.setEmail(jsItems.getString("items_email"));
                    items.setLine(jsItems.getString("items_line"));
                    items.setFacebookPage(jsItems.getString("items_facebookPage"));
                    items.setArURL(jsItems.getString("item_ar_url"));
                    items.setIsActive(jsItems.getInt("items_isActive"));
                    items.setIsPublish(jsItems.getInt("items_isPublish"));
                    items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                    items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                    items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                    items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                    items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                    items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                    items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                    items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
                    items.setPriceGuestAdult(Integer.parseInt(jsItems.getString("items_priceguestAdult")));
                    items.setPriceGuestChild(Integer.parseInt(jsItems.getString("items_priceguestChild")));
                    menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                    menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                    menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                    menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                    menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                    items.setMenuItemModel(menuItem);
                    categoryModel.setCategoryId(jsItems.getInt("category_id"));
                    categoryModel.setCategoryTH(jsItems.getString("category_thai"));
                    categoryModel.setCategoryEN(jsItems.getString("category_english"));
                    categoryModel.setCategoryLO(jsItems.getString("category_laos"));
                    categoryModel.setCategoryZH(jsItems.getString("category_chinese"));
                    items.setCategoryModel(categoryModel);
                    subCategory.setCategoryId(Integer.parseInt(jsItems.getString("subcategory_id")));
                    subCategory.setCategoryTH(jsItems.getString("subcategory_thai"));
                    subCategory.setCategoryEN(jsItems.getString("subcategory_english"));
                    subCategory.setCategoryLO(jsItems.getString("subcategory_laos"));
                    subCategory.setCategoryZH(jsItems.getString("subcategory_chinese"));
                    subCategoryArrayList.add(subCategory);
                    items.setSubCategoryModelArrayList(subCategoryArrayList);
                    ProvincesModel provinces = new ProvincesModel();
                    provinces.setProvincesId(jsItems.getInt("provinces_id"));
                    provinces.setProvincesCode(jsItems.getInt("provinces_code"));
                    provinces.setProvincesTH(jsItems.getString("provinces_thai"));
                    provinces.setProvincesEN(jsItems.getString("provinces_english"));
                    provinces.setProvincesLO(jsItems.getString("provinces_laos"));
                    provinces.setProvincesZH(jsItems.getString("provinces_chinese"));
                    DistrictsModel districts = new DistrictsModel();
                    districts.setDistrictsId(jsItems.getInt("districts_id"));
                    districts.setDistrictsCode(jsItems.getInt("districts_code"));
                    districts.setDistrictsTH(jsItems.getString("districts_thai"));
                    districts.setDistrictsEN(jsItems.getString("districts_english"));
                    districts.setDistrictsLO(jsItems.getString("districts_laos"));
                    districts.setDistrictsZH(jsItems.getString("districts_chinese"));
                    provinces.setDistrictsModel(districts);
                    SubDistrictsModel subDistricts = new SubDistrictsModel();
                    subDistricts.setSubDistrictsId(jsItems.getInt("subdistricts_id"));
                    subDistricts.setSubDistrictsCode(jsItems.getInt("subdistricts_zip_code"));
                    subDistricts.setSubDistrictsTH(jsItems.getString("subdistricts_thai"));
                    subDistricts.setSubDistrictsEN(jsItems.getString("subdistricts_english"));
                    subDistricts.setSubDistrictsLO(jsItems.getString("subdistricts_laos"));
                    subDistricts.setSubDistrictsZH(jsItems.getString("subdistricts_chinese"));
                    districts.setSubDistrictsModel(subDistricts);
                    items.setProvincesModel(provinces);

                    ArrayList<CoverItemsModel> coverItemsModelsArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonCover.length(); j++) {
                        JSONObject jsCover = jsonCover.getJSONObject(j);
                        CoverItemsModel coverItemsModels = new CoverItemsModel();

                        if (jsCover != null) {
                            coverItemsModels.setCoverId(jsCover.getInt("coverItem_id"));
                            coverItemsModels.setCoverPaths(jsCover.getString("coverItem_paths"));
                            if (jsCover.getString("coverItem_url") != null && jsCover.getString("coverItem_url") != "" && !jsCover.getString("coverItem_url").equals("")) {
                                coverItemsModels.setCoverURL(jsCover.getString("coverItem_url"));
                            } else {
                                coverItemsModels.setCoverURL(null);
                            }
                            coverItemsModelsArrayList.add(coverItemsModels);
                        }
                    }
                    items.setCoverItemsModelArrayList(coverItemsModelsArrayList);

                    ArrayList<ItemsDetailModel> itemsDetailModelArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonDetails.length(); j++) {
                        JSONObject jsDetail = jsonDetails.getJSONObject(j);
                        ItemsDetailModel itemsDetail = new ItemsDetailModel();
                        ItemsTopicDetailModel topicDetail = new ItemsTopicDetailModel();
                        JSONArray jsonPhoto = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Detail").getJSONObject(j).getJSONArray("Photo");

                        if (jsDetail != null){
                            itemsDetail.setDetailId(jsDetail.getInt("detail_id"));
                            itemsDetail.setDetailTH(jsDetail.getString("detail_textThai"));
                            itemsDetail.setDetailEN(jsDetail.getString("detail_textEnglish"));
                            itemsDetail.setDetailLO(jsDetail.getString("detail_textLaos"));
                            itemsDetail.setDetailZH(jsDetail.getString("detail_textChinese"));
                            topicDetail.setTopicDetailId(jsDetail.getInt("topicdetail_id"));
                            topicDetail.setTopicDetailTH(jsDetail.getString("topicdetail_Thai"));
                            topicDetail.setTopicDetailEN(jsDetail.getString("topicdetail_English"));
                            topicDetail.setTopicDetailLO(jsDetail.getString("topicdetail_Laos"));
                            topicDetail.setTopicDetailZH(jsDetail.getString("topicdetail_Chinese"));
                            itemsDetail.setItemsTopicDetailModel(topicDetail);
                            itemsDetailModelArrayList.add(itemsDetail);

                            ArrayList<ItemsPhotoDetailModel> photoDetailModelArrayList = new ArrayList<>();
                            for (int k = 0; k < jsonPhoto.length(); k++){
                                JSONObject jsPhoto = jsonPhoto.getJSONObject(k);
                                ItemsPhotoDetailModel photoDetail = new ItemsPhotoDetailModel();

                                if (jsPhoto != null){
                                    photoDetail.setPhotoId(jsPhoto.getInt("photo_id"));
                                    photoDetail.setPhotoPaths(jsPhoto.getString("photo_paths"));
                                    photoDetail.setPhotoTextTH(jsPhoto.getString("photo_textThai"));
                                    photoDetail.setPhotoTextEN(jsPhoto.getString("photo_textEnglish"));
                                    photoDetail.setPhotoTextLO(jsPhoto.getString("photo_textLaos"));
                                    photoDetail.setPhotoTextZH(jsPhoto.getString("photo_textChinese"));
                                    photoDetailModelArrayList.add(photoDetail);
                                }

                            }
                            itemsDetail.setItemsPhotoDetailModels(photoDetailModelArrayList);

                        }
                        items.setItemsDetailModelArrayList(itemsDetailModelArrayList);
                    }

                    ArrayList<PeriodTimeModel> periodTimeModelArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonPeriod.length() ; j++) {
                        JSONObject jsPeriod = jsonPeriod.getJSONObject(j);
                        PeriodTimeModel periodTime = new PeriodTimeModel();
                        PeriodDayModel periodDay = new PeriodDayModel();

                        if (jsPeriod != null){
                            periodTime.setPeriodId(jsPeriod.getInt("itemsHasDayOfWeek_id"));
                            periodTime.setPeriodOpen(jsPeriod.getString("items_timeOpen"));
                            periodTime.setPeriodClose(jsPeriod.getString("items_timeClose"));
                            items.setTimeOpen(jsPeriod.getString("items_timeOpen"));
                            items.setTimeClose(jsPeriod.getString("items_timeClose"));

                            JSONArray jsonOpen = object.optJSONArray("Items").getJSONObject(i).getJSONArray("TimePeriod").getJSONObject(j).getJSONArray("item_dayOpen_data");
                            JSONArray jsonClose = object.optJSONArray("Items").getJSONObject(i).getJSONArray("TimePeriod").getJSONObject(j).getJSONArray("item_dayClose_data");

                            for (int k = 0; k < jsonOpen.length() ; k++) {
                                JSONObject jsOpen = jsonOpen.getJSONObject(k);
                                if (jsOpen != null){
                                    periodDay.setPeriodOpenTH(jsOpen.getString("dayofweek_thai"));
                                    periodDay.setPeriodOpenEN(jsOpen.getString("dayofweek_English"));
                                    periodDay.setPeriodOpenLO(jsOpen.getString("dayofweek_Chinese"));
                                    periodDay.setPeriodOpenZH(jsOpen.getString("dayofweek_Laos"));
                                }
                            }
                            for (int k = 0; k < jsonClose.length() ; k++) {
                                JSONObject jsClose = jsonClose.getJSONObject(k);
                                if (jsClose != null){
                                    periodDay.setPeriodOpenTH(jsClose.getString("dayofweek_thai"));
                                    periodDay.setPeriodOpenEN(jsClose.getString("dayofweek_English"));
                                    periodDay.setPeriodOpenLO(jsClose.getString("dayofweek_Chinese"));
                                    periodDay.setPeriodOpenZH(jsClose.getString("dayofweek_Laos"));
                                }
                            }
                            periodTime.setPeriodDayModel(periodDay);
                            periodTimeModelArrayList.add(periodTime);
                        }
                        items.setPeriodTimeModelArrayList(periodTimeModelArrayList);
                        items.setPeriodDayModel(periodDay);
                        items.setPeriodTimeModel(periodTime);
                    }

                    ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonCondition.length() ; j++) {
                        JSONObject jsCondition = jsonCondition.getJSONObject(j);
                        BookingConditionModel conditionModel = new BookingConditionModel();

                        if (jsCondition != null){
                            conditionModel.setConditionId(jsCondition.getInt("bookingCondition_id"));
                            conditionModel.setConditionTopicTH(jsCondition.getString("bookingConditionCategory_topicThai"));
                            conditionModel.setConditionTopicEN(jsCondition.getString("bookingConditionCategory_topicEnglish"));
                            conditionModel.setConditionTopicLO(jsCondition.getString("bookingConditionCategory_topicChinese"));
                            conditionModel.setConditionTopicZH(jsCondition.getString("bookingConditionCategory_topicLaos"));
                            conditionModel.setConditionDetailTH(jsCondition.getString("bookingConditioncol_detailThai"));
                            conditionModel.setConditionDetailEN(jsCondition.getString("bookingConditioncol_detailEnglish"));
                            conditionModel.setConditionDetailLO(jsCondition.getString("bookingConditioncol_detailChinese"));
                            conditionModel.setConditionDetailZH(jsCondition.getString("bookingConditioncol_detailLaos"));

                            bookingConditionModelArrayList.add(conditionModel);
                        }
                    }
                    items.setBookingConditionModelArrayList(bookingConditionModelArrayList);

                    JSONArray jsonBusiness = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Business");
                    for (int j = 0; j < jsonBusiness.length() ; j++) {
                        JSONObject jsBusiness = jsonBusiness.getJSONObject(j);
                        if (jsBusiness != null) {
                            BusinessModel business = new BusinessModel();
                            business.setBusinessId(jsBusiness.getInt("business_id"));
                            business.setNameTH(jsBusiness.getString("business_nameThai"));
                            business.setNameEN(jsBusiness.getString("business_nameEnglish"));
                            business.setNameLO(jsBusiness.getString("business_nameLaos"));
                            business.setNameZH(jsBusiness.getString("business_nameChinese"));
                            business.setLatitude((float) jsBusiness.getDouble("business_latitude"));
                            business.setLongitude((float) jsBusiness.getDouble("business_longitude"));
                            business.setPresentAddress(jsBusiness.getString("business_presentAddress"));
                            business.setLicensePaths(jsBusiness.getString("business_license_paths"));
                            business.setOperatorNumber(jsBusiness.getString("business_operator_number"));
                            business.setPhone(jsBusiness.getString("business_phone"));
                            business.setWeb(jsBusiness.getString("business_www"));
                            business.setFacebook(jsBusiness.getString("business_facebook"));
                            business.setLine(jsBusiness.getString("business_line"));
                            items.setBusinessModel(business);
                        }
                    }


                    list.add(items);


                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
