package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.interface_callback.FilterTravelingCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class FilterItemsTravelingAsyncTask extends AsyncTask<String,Integer,String> {

    private CategoryModel categoryModel;
    private FilterTravelingCallBack callBackService;

    public FilterItemsTravelingAsyncTask(FilterTravelingCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            categoryModel = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerTraveling(categoryModel);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public CategoryModel  onParserContentToModel(String dataJSon) {
        CategoryModel category = new CategoryModel();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonCategory = object.optJSONArray("SubCategory");

            ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
            for (int i = 0; i < jsonCategory.length(); i++) {
                JSONObject jsCategory = jsonCategory.getJSONObject(i);
                SubCategoryModel subCategory = new SubCategoryModel();
                MenuItemModel menu = new MenuItemModel();

                if (jsCategory != null) {
                    category.setCategoryId(jsCategory.getInt("category_id"));
                    category.setCategoryTH(jsCategory.getString("category_thai"));
                    category.setCategoryEN(jsCategory.getString("category_english"));
                    category.setCategoryLO(jsCategory.getString("category_laos"));
                    category.setCategoryZH(jsCategory.getString("category_chinese"));
                    menu.setMenuItemId(jsCategory.getInt("MenuItem_menuItem_id"));
                    category.setMenuItemModel(menu);
                    subCategory.setCategoryId(jsCategory.getInt("subcategory_id"));
                    subCategory.setCategoryTH(jsCategory.getString("subcategory_thai"));
                    subCategory.setCategoryEN(jsCategory.getString("subcategory_english"));
                    subCategory.setCategoryLO(jsCategory.getString("subcategory_laos"));
                    subCategory.setCategoryZH(jsCategory.getString("subcategory_chinese"));
                    subCategoryArrayList.add(subCategory);
                }

            }

            category.setSubCategoryModelArrayList(subCategoryArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return category;
    }

}
