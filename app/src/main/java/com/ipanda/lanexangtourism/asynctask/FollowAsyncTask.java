package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.interface_callback.FollowCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class FollowAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<UserModel> userModelArrayList;
    private FollowCallBack callBackService;

    public FollowAsyncTask(FollowCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            userModelArrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(userModelArrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<UserModel> onParserContentToModel(String dataJSon) {
        ArrayList<UserModel> list = new ArrayList<>();
        try {

            JSONArray array = new JSONArray(dataJSon);
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                UserModel user = new UserModel();
                if (jsonObject != null){
                    user.setUserId(jsonObject.getInt("user_id"));
                    user.setIdentification(jsonObject.getString("user_identification"));
                    user.setToken(jsonObject.getString("user_token"));
                    user.setEmail(jsonObject.getString("user_email"));
                    user.setFirstName(jsonObject.getString("user_firstName"));
                    user.setLastName(jsonObject.getString("user_lastName"));
                    user.setAge(jsonObject.getString("user_age"));
                    user.setGender(jsonObject.getString("user_gender"));
                    user.setPresentAddress(jsonObject.getString("user_presentAddress"));
                    user.setLoginWith(jsonObject.getString("user_loginWith"));
                    user.setProfilePicUrl(jsonObject.getString("user_profile_pic_url"));
                    user.setFollowState(jsonObject.getBoolean("follow_state"));
                    list.add(user);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
