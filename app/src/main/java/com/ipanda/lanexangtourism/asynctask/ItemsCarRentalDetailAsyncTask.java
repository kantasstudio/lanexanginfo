package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.Model.BusinessModel;
import com.ipanda.lanexangtourism.Model.CarModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.interface_callback.ItemsCarRentDetailCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ItemsCarRentalDetailAsyncTask extends AsyncTask<String,Integer,String> {

    private ItemsModel arrayList;
    private ItemsCarRentDetailCallBack callBackService;

    public ItemsCarRentalDetailAsyncTask(ItemsCarRentDetailCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ItemsModel  onParserContentToModel(String dataJSon) {

        ItemsModel items = new ItemsModel();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonItems = object.optJSONArray("Items");

            for (int i = 0; i < jsonItems.length(); i++) {
                JSONObject jsItems = jsonItems.getJSONObject(i);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                MenuItemModel menuItem = new MenuItemModel();
                SubCategoryModel subCategory = new SubCategoryModel();


                if (jsItems != null) {
                    items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                    items.setTopicTH(jsItems.getString("itmes_topicThai"));
                    items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                    items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                    items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                    items.setLatitude(jsItems.getString("items_latitude"));
                    items.setLongitude(jsItems.getString("items_longitude"));
                    items.setContactTH(jsItems.getString("items_contactThai"));
                    items.setContactEN(jsItems.getString("items_contactEnglish"));
                    items.setContactLO(jsItems.getString("items_contactLaos"));
                    items.setContactZH(jsItems.getString("items_contactChinese"));
                    items.setPhone(jsItems.getString("items_phone"));
                    items.setEmail(jsItems.getString("items_email"));
                    items.setLine(jsItems.getString("items_line"));
                    items.setFacebookPage(jsItems.getString("items_facebookPage"));
                    items.setArURL(jsItems.getString("item_ar_url"));
//                    items.setPrice(jsItems.getDouble("items_price"));
                    items.setIsActive(jsItems.getInt("items_isActive"));
                    items.setIsPublish(jsItems.getInt("items_isPublish"));
                    items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                    items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                    items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                    items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                    items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                    items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                    items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                    items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
//                    items.setTimeOpen(jsItems.getString("items_timeOpen"));
//                    items.setTimeClose(jsItems.getString("items_timeClose"));
                    items.setPriceGuestAdult(Integer.parseInt(jsItems.getString("items_priceguestAdult")));
                    items.setPriceGuestChild(Integer.parseInt(jsItems.getString("items_priceguestChild")));
                    menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                    menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                    menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                    menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                    menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                    items.setMenuItemModel(menuItem);

                    CoverItemsModel coverItemsModels = new CoverItemsModel();
                    coverItemsModels.setCoverId(jsItems.getInt("coverItem_id"));
                    coverItemsModels.setCoverPaths(jsItems.getString("coverItem_paths"));
                    if (jsItems.getString("coverItem_url") != null && jsItems.getString("coverItem_url") != "" && !jsItems.getString("coverItem_url").equals("")) {
                        coverItemsModels.setCoverURL(jsItems.getString("coverItem_url"));
                    } else {
                        coverItemsModels.setCoverURL(null);
                    }
                    items.setCoverItemsModel(coverItemsModels);

                    CarModel carModel = new CarModel();
                    carModel.setSeats(jsItems.getInt("items_carSeats"));
                    carModel.setPricePerDay(jsItems.getInt("items_PricePerDay"));
                    carModel.setReturnPrice(jsItems.getInt("items_CarDeliveryPrice"));
                    carModel.setPickUpPrice(jsItems.getInt("items_PickUpPrice"));
                    carModel.setDepositPrice(jsItems.getInt("items_DepositPrice"));
                    if (jsItems.getString("items_isBasicInsurance").equalsIgnoreCase("1")){
                        carModel.setBasicInsurance(true);
                    }else {
                        carModel.setBasicInsurance(false);
                    }
                    carModel.setBasicInsuranceTH(jsItems.getString("items_BissicInsurance_detailThai"));
                    carModel.setBasicInsuranceEN(jsItems.getString("items_BissicInsurance_detailEnglish"));
                    carModel.setBasicInsuranceLO(jsItems.getString("items_BissicInsurance_detailLaos"));
                    carModel.setBasicInsuranceZH(jsItems.getString("items_BissicInsurance_detailChinese"));
                    carModel.setTypeTH(jsItems.getString("typesofCars_Thai"));
                    carModel.setTypeEN(jsItems.getString("typesofCars_English"));
                    carModel.setTypeLO(jsItems.getString("typesofCars_Laos"));
                    carModel.setTypeZH(jsItems.getString("typesofCars_Chinese"));
                    carModel.setGearSystemTH(jsItems.getString("gearSystem_Thai"));
                    carModel.setGearSystemEN(jsItems.getString("gearSystem_English"));
                    carModel.setGearSystemLO(jsItems.getString("gearSystem_Laos"));
                    carModel.setGearSystemZH(jsItems.getString("gearSystem_Chinese"));
                    items.setCarModel(carModel);


                    JSONArray jsonCondition = object.optJSONArray("Items").getJSONObject(i).getJSONArray("BookingCondition");
                    ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonCondition.length() ; j++) {
                        JSONObject jsCondition = jsonCondition.getJSONObject(j);
                        BookingConditionModel conditionModel = new BookingConditionModel();

                        if (jsCondition != null){
                            conditionModel.setConditionId(jsCondition.getInt("bookingConditionCategory_id"));
                            conditionModel.setConditionTopicTH(jsCondition.getString("bookingConditionCategory_topicThai"));
                            conditionModel.setConditionTopicEN(jsCondition.getString("bookingConditionCategory_topicEnglish"));
                            conditionModel.setConditionTopicLO(jsCondition.getString("bookingConditionCategory_topicLaos"));
                            conditionModel.setConditionTopicZH(jsCondition.getString("bookingConditionCategory_topicChinese"));
                            conditionModel.setConditionDetailTH(jsCondition.getString("bookingConditioncol_detailThai"));
                            conditionModel.setConditionDetailEN(jsCondition.getString("bookingConditioncol_detailEnglish"));
                            conditionModel.setConditionDetailLO(jsCondition.getString("bookingConditioncol_detailLaos"));
                            conditionModel.setConditionDetailZH(jsCondition.getString("bookingConditioncol_detailChinese"));

                            bookingConditionModelArrayList.add(conditionModel);
                        }
                    }

                    JSONArray jsonBusiness = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Business");
                    for (int j = 0; j < jsonBusiness.length() ; j++) {
                        JSONObject jsBusiness = jsonBusiness.getJSONObject(j);
                        if (jsBusiness != null) {
                            BusinessModel business = new BusinessModel();
                            business.setBusinessId(jsBusiness.getInt("business_id"));
                            business.setNameTH(jsBusiness.getString("business_nameThai"));
                            business.setNameEN(jsBusiness.getString("business_nameEnglish"));
                            business.setNameLO(jsBusiness.getString("business_nameLaos"));
                            business.setNameZH(jsBusiness.getString("business_nameChinese"));
                            business.setLatitude((float) jsBusiness.getDouble("business_latitude"));
                            business.setLongitude((float) jsBusiness.getDouble("business_longitude"));
                            business.setPresentAddress(jsBusiness.getString("business_presentAddress"));
                            business.setLicensePaths(jsBusiness.getString("business_license_paths"));
                            business.setOperatorNumber(jsBusiness.getString("business_operator_number"));
                            business.setPhone(jsBusiness.getString("business_phone"));
                            business.setWeb(jsBusiness.getString("business_www"));
                            business.setFacebook(jsBusiness.getString("business_facebook"));
                            business.setLine(jsBusiness.getString("business_line"));
                            items.setBusinessModel(business);
                        }
                    }


                    items.setBookingConditionModelArrayList(bookingConditionModelArrayList);

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return items;
    }

}
