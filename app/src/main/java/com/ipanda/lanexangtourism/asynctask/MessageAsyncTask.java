package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.LikesModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.TouristAttractionsModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.Model.UserRoleModel;
import com.ipanda.lanexangtourism.interface_callback.MessageCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProgramTourCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MessageAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<MessageModel> arrayList;
    private MessageCallBack callBackService;

    public MessageAsyncTask(MessageCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<MessageModel>  onParserContentToModel(String dataJSon) {
        ArrayList<MessageModel> list = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonMessages = object.optJSONArray("ChatMessages");

            if (jsonMessages != null && jsonMessages.length() != 0) {

                for (int i = 0; i < jsonMessages.length() ; i++) {
                    JSONObject jsMessages = jsonMessages.getJSONObject(i);
                    MessageModel message = new MessageModel();
                    UserModel user = new UserModel();
                    UserRoleModel roleModel = new UserRoleModel();

                    message.setMessageId(jsMessages.getInt("messages_id"));
                    message.setMessage(jsMessages.getString("message"));
                    message.setCreatedAt(jsMessages.getString("message_created_at"));
                    message.setGcmId(jsMessages.getString("gcm_registration_id"));
                    if (jsMessages.getString("messages_read").equals("1")) {
                        message.setRead(true);
                    }else {
                        message.setRead(false);
                    }
                    message.setRoomId(jsMessages.getInt("ChatRooms_chatroom_id"));

                    user.setUserId(jsMessages.getInt("user_id"));
                    user.setFirstName(jsMessages.getString("user_firstName"));
                    user.setLastName(jsMessages.getString("user_lastName"));
                    roleModel.setUserRoleId(jsMessages.getInt("UserRole_userRole_id"));
                    user.setUserRoleModel(roleModel);
                    if (!jsMessages.getString("user_profile_pic_url").equals("") && jsMessages.getString("user_profile_pic_url") != null){
                        user.setProfilePicUrl(jsMessages.getString("user_profile_pic_url"));
                    }else {
                        user.setProfilePicUrl(null);
                    }

                    message.setUserModel(user);
                    list.add(message);

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
