package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BusinessModel;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.Model.UserRoleModel;
import com.ipanda.lanexangtourism.interface_callback.MessageCompanyListCallBack;
import com.ipanda.lanexangtourism.interface_callback.MessageListCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MessageCompanyListAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<MessageModel> arrayList;
    private MessageCompanyListCallBack callBackService;

    public MessageCompanyListAsyncTask(MessageCompanyListCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<MessageModel>  onParserContentToModel(String dataJSon) {
        ArrayList<MessageModel> list = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonMessages = object.optJSONArray("ChatRoomData");

            if (jsonMessages != null && jsonMessages.length() != 0) {

                for (int i = 0; i < jsonMessages.length() ; i++) {
                    JSONObject jsMessages = jsonMessages.getJSONObject(i);
                    MessageModel message = new MessageModel();

                    message.setMessageId(jsMessages.getInt("messages_id"));
                    message.setMessage(jsMessages.getString("message"));
                    message.setCreatedAt(jsMessages.getString("message_created_at"));
                    message.setCountMessage(jsMessages.getInt("count_read"));
                    if (jsMessages.getString("messages_read").equals("0")) {
                        message.setRead(true);
                    }else {
                        message.setRead(false);
                    }
                    message.setRoomId(jsMessages.getInt("ChatRooms_chatroom_id"));

                    JSONArray jsonUserSend = object.optJSONArray("ChatRoomData").getJSONObject(i).getJSONArray("UserSend");
                    if (jsonUserSend != null) {
                        for (int j = 0; j < jsonUserSend.length(); j++) {
                            JSONObject jsSend = jsonUserSend.getJSONObject(j);
                            UserModel user = new UserModel();
                            user.setUserId(jsSend.getInt("user_id"));
                            user.setFirstName(jsSend.getString("user_firstName"));
                            user.setLastName(jsSend.getString("user_lastName"));
                            if (!jsSend.getString("user_profile_pic_url").equals("") && jsSend.getString("user_profile_pic_url") != null){
                                user.setProfilePicUrl(jsSend.getString("user_profile_pic_url"));
                            }else {
                                user.setProfilePicUrl(null);
                            }
                            message.setUserModel(user);
                        }
                    }

                    JSONArray jsonUserReceived = object.optJSONArray("ChatRoomData").getJSONObject(i).getJSONArray("UserReceiv");
                    if (jsonUserReceived != null) {
                        for (int j = 0; j < jsonUserReceived.length(); j++) {
                            JSONObject jsReceived = jsonUserReceived.getJSONObject(j);
                            UserModel user = new UserModel();
                            user.setUserId(jsReceived.getInt("user_id"));
                            user.setFirstName(jsReceived.getString("user_firstName"));
                            user.setLastName(jsReceived.getString("user_lastName"));
                            if (!jsReceived.getString("user_profile_pic_url").equals("") && jsReceived.getString("user_profile_pic_url") != null){
                                user.setProfilePicUrl(jsReceived.getString("user_profile_pic_url"));
                            }else {
                                user.setProfilePicUrl(null);
                            }
                            message.setUserSend(user);
                        }
                    }


                    JSONArray jsonItems = object.optJSONArray("ChatRoomData").getJSONObject(i).getJSONArray("Items");

                    if (jsonItems != null){
                        for (int j = 0; j <jsonItems.length() ; j++) {
                            JSONObject jsItems = jsonItems.getJSONObject(j);
                            ItemsModel itemsModel = new ItemsModel();

                            itemsModel.setTopicTH(jsItems.getString("itmes_topicThai"));
                            itemsModel.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                            itemsModel.setTopicLO(jsItems.getString("itmes_topicLaos"));
                            itemsModel.setTopicZH(jsItems.getString("itmes_topicChinese"));

                            CoverItemsModel coverItemsModel = new CoverItemsModel();
                            coverItemsModel.setCoverPaths(jsItems.getString("CoverItems"));
                            itemsModel.setCoverItemsModel(coverItemsModel);

                            message.setItemsModel(itemsModel);

                            JSONArray jsonBusiness = object.optJSONArray("ChatRoomData").getJSONObject(i).getJSONArray("Items").getJSONObject(j).getJSONArray("Business");
                            if (jsonBusiness != null){
                                for (int k = 0; k < jsonBusiness.length(); k++) {
                                    JSONObject jsBusiness = jsonBusiness.getJSONObject(k);
                                    BusinessModel businessModel = new BusinessModel();

                                    businessModel.setBusinessId(jsBusiness.getInt("business_id"));
                                    businessModel.setNameTH(jsBusiness.getString("business_nameThai"));
                                    businessModel.setNameEN(jsBusiness.getString("business_nameEnglish"));
                                    businessModel.setNameLO(jsBusiness.getString("business_nameLaos"));
                                    businessModel.setNameZH(jsBusiness.getString("business_nameChinese"));

                                    UserModel userModel = new UserModel();
                                    userModel.setUserId(jsBusiness.getInt("user_id"));
                                    userModel.setFirstName(jsBusiness.getString("user_firstName"));
                                    userModel.setLastName(jsBusiness.getString("user_lastName"));
                                    userModel.setProfilePicUrl(jsBusiness.getString("user_profile_pic_url"));

                                    UserRoleModel roleModel = new UserRoleModel();
                                    roleModel.setUserRoleId(jsBusiness.getInt("UserRole_userRole_id"));
                                    userModel.setUserRoleModel(roleModel);

                                    businessModel.setUserModel(userModel);

                                    MenuItemModel categoryModel = new MenuItemModel();
                                    categoryModel.setMenuItemId(jsBusiness.getInt("category_id"));
                                    categoryModel.setMenuItemTH(jsBusiness.getString("category_thai"));
                                    categoryModel.setMenuItemEN(jsBusiness.getString("category_english"));
                                    categoryModel.setMenuItemLO(jsBusiness.getString("category_laos"));
                                    categoryModel.setMenuItemZH(jsBusiness.getString("category_chinese"));

                                    businessModel.setMenuItemModel(categoryModel);
                                    message.setBusinessModel(businessModel);
                                }
                            }

                        }
                    }

                    list.add(message);

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
