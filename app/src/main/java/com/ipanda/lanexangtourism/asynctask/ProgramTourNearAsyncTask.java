package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.LikesModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.TouristAttractionsModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.interface_callback.ProgramTourNearCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ProgramTourNearAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<ProgramTourModel> arrayList;
    private ProgramTourNearCallBack callBackService;

    public ProgramTourNearAsyncTask(ProgramTourNearCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallServiceProgramTour();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallServiceProgramTour();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerProgramTour(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ProgramTourModel>  onParserContentToModel(String dataJSon) {
        ArrayList<ProgramTourModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonProgramTour = object.optJSONArray("ProgramTour");

            for (int i = 0; i < jsonProgramTour.length() ; i++) {
                JSONObject jsProgramTour = jsonProgramTour.getJSONObject(i);
                ProgramTourModel programTourModel = new ProgramTourModel();

                JSONArray jsonDatesTrip = object.optJSONArray("ProgramTour").getJSONObject(i).getJSONArray("DatesTrip");
                JSONArray jsonReview = object.optJSONArray("ProgramTour").getJSONObject(i).getJSONArray("Review");

                if (jsProgramTour != null) {
                    programTourModel.setProgramTourId(jsProgramTour.getInt("programTour_id"));
                    programTourModel.setProgramTourNameTH(jsProgramTour.getString("programTour_nameThai"));
                    programTourModel.setProgramTourNameEN(jsProgramTour.getString("programTour_nameEnglish"));
                    programTourModel.setProgramTourNameLO(jsProgramTour.getString("programTour_nameLaos"));
                    programTourModel.setProgramTourNameZH(jsProgramTour.getString("programTour_nameChinese"));
                    programTourModel.setInterestingThingsTH(jsProgramTour.getString("programTour_interestingThingsThai"));
                    programTourModel.setInterestingThingsEN(jsProgramTour.getString("programTour_interestingThingsEnglish"));
                    programTourModel.setInterestingThingsLO(jsProgramTour.getString("programTour_interestingThingsLaos"));
                    programTourModel.setInterestingThingsZH(jsProgramTour.getString("programTour_interestingThingsChinese"));
                    programTourModel.setCreatedDTTM(jsProgramTour.getString("programTour_createdDTTM"));
                    programTourModel.setUpdatedDTTM(jsProgramTour.getString("programTour_updatedDTTM"));
                    programTourModel.setCountView(jsProgramTour.getString("programTour_readcount"));

                    LikesModel likesModel = new LikesModel();
                    likesModel.setLikes(jsProgramTour.getInt("Like_count"));
                    likesModel.setLikeState(jsProgramTour.getBoolean("Like_state"));
                    programTourModel.setLikesModel(likesModel);

                    MenuItemModel menuItemModel = new MenuItemModel();
                    menuItemModel.setMenuItemId(jsProgramTour.getInt("menuItem_id"));
                    menuItemModel.setMenuItemTH(jsProgramTour.getString("menuItem_thai"));
                    menuItemModel.setMenuItemEN(jsProgramTour.getString("menuItem_english"));
                    menuItemModel.setMenuItemLO(jsProgramTour.getString("menuItem_laos"));
                    menuItemModel.setMenuItemZH(jsProgramTour.getString("menuItem_chinese"));
                    programTourModel.setMenuItemModel(menuItemModel);

                    CategoryModel categoryModel = new CategoryModel();
                    categoryModel.setCategoryId(jsProgramTour.getInt("category_id"));
                    categoryModel.setCategoryTH(jsProgramTour.getString("category_thai"));
                    categoryModel.setCategoryEN(jsProgramTour.getString("category_english"));
                    categoryModel.setCategoryLO(jsProgramTour.getString("category_laos"));
                    categoryModel.setCategoryZH(jsProgramTour.getString("category_chinese"));
                    programTourModel.setCategoryModel(categoryModel);

                    SubCategoryModel subCategoryModel = new SubCategoryModel();
                    subCategoryModel.setCategoryId(jsProgramTour.getInt("subcategory_id"));
                    subCategoryModel.setCategoryTH(jsProgramTour.getString("subcategory_thai"));
                    subCategoryModel.setCategoryEN(jsProgramTour.getString("subcategory_english"));
                    subCategoryModel.setCategoryLO(jsProgramTour.getString("subcategory_laos"));
                    subCategoryModel.setCategoryZH(jsProgramTour.getString("subcategory_chinese"));
                    programTourModel.setSubCategoryModel(subCategoryModel);

                    CoverItemsModel coverItemsModel = new CoverItemsModel();
                    coverItemsModel.setCoverId(jsProgramTour.getInt("coverItem_id"));
                    coverItemsModel.setCoverPaths(jsProgramTour.getString("coverItem_paths"));
                    coverItemsModel.setCoverURL(jsProgramTour.getString("coverItem_url"));
                    programTourModel.setCoverItemsModel(coverItemsModel);

                    ArrayList<DatesTripModel> datesTripModelArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonDatesTrip.length() ; j++) {
                        JSONObject jsDatesTrip = jsonDatesTrip.getJSONObject(j);
                        DatesTripModel datesTripModel = new DatesTripModel();

                        JSONArray jsonTouristAttractions = object.optJSONArray("ProgramTour").getJSONObject(i).getJSONArray("DatesTrip").getJSONObject(j).getJSONArray("TouristAttractions");

                        if (jsDatesTrip != null) {
                            datesTripModel.setDatesTripId(jsDatesTrip.getInt("datesTrip_id"));
                            datesTripModel.setDatesTripTopicThai(jsDatesTrip.getString("datesTrip_topicThai"));
                            datesTripModel.setDatesTripTopicEnglish(jsDatesTrip.getString("datesTrip_topicEnglish"));
                            datesTripModel.setDatesTripTopicLaos(jsDatesTrip.getString("datesTrip_topicLaos"));
                            datesTripModel.setDatesTripTopicChinese(jsDatesTrip.getString("datesTrip_topicChinese"));


                            ArrayList<TouristAttractionsModel> touristAttractionsModelArrayList = new ArrayList<>();
                            for (int k = 0; k <  jsonTouristAttractions.length() ; k++) {
                                JSONObject jsTouristAttractions = jsonTouristAttractions.getJSONObject(k);
                                TouristAttractionsModel touristAttractionsModel = new TouristAttractionsModel();

                                JSONArray jsonItemDetail = object.optJSONArray("ProgramTour").getJSONObject(i).getJSONArray("DatesTrip").getJSONObject(j).getJSONArray("TouristAttractions").getJSONObject(k).getJSONArray("ItemDetail");
                                JSONArray jsonPhotoTourist = object.optJSONArray("ProgramTour").getJSONObject(i).getJSONArray("DatesTrip").getJSONObject(j).getJSONArray("TouristAttractions").getJSONObject(k).getJSONArray("PhotoTourist");

                                if (jsTouristAttractions != null){
                                    touristAttractionsModel.setAttractionsId(jsTouristAttractions.getInt("touristAttractions_id"));
                                    touristAttractionsModel.setDetailTH(jsTouristAttractions.getString("detail_Thai"));
                                    touristAttractionsModel.setDetailEN(jsTouristAttractions.getString("detail_English"));
                                    touristAttractionsModel.setDetailLO(jsTouristAttractions.getString("detail_Laos"));
                                    touristAttractionsModel.setDetailZH(jsTouristAttractions.getString("detail_Chinese"));

                                    ItemsModel itemsModel = new ItemsModel();
                                    for (int l = 0; l < jsonItemDetail.length() ; l++) {
                                        JSONObject jsItemDetail = jsonItemDetail.getJSONObject(l);

                                        if (jsItemDetail != null) {
                                            itemsModel.setItemsId(jsItemDetail.getInt("items_id"));
                                            itemsModel.setTopicTH(jsItemDetail.getString("itmes_topicThai"));
                                            itemsModel.setTopicEN(jsItemDetail.getString("itmes_topicEnglish"));
                                            itemsModel.setTopicLO(jsItemDetail.getString("itmes_topicLaos"));
                                            itemsModel.setTopicZH(jsItemDetail.getString("itmes_topicChinese"));
                                            itemsModel.setContactTH(jsItemDetail.getString("items_contactThai"));
                                            itemsModel.setContactEN(jsItemDetail.getString("items_contactEnglish"));
                                            itemsModel.setContactLO(jsItemDetail.getString("items_contactLaos"));
                                            itemsModel.setContactZH(jsItemDetail.getString("items_contactChinese"));
/*                                            itemsModel.setDayOpenTH(jsItemDetail.getString(""));
                                            itemsModel.setDayOpenEN(jsItemDetail.getString(""));
                                            itemsModel.setDayOpenLO(jsItemDetail.getString(""));
                                            itemsModel.setDayOpenZH(jsItemDetail.getString(""));
                                            itemsModel.setDayCloseTH(jsItemDetail.getString(""));
                                            itemsModel.setDayCloseEN(jsItemDetail.getString(""));
                                            itemsModel.setDayCloseLO(jsItemDetail.getString(""));
                                            itemsModel.setDayCloseZH(jsItemDetail.getString(""));*/
                                            itemsModel.setTimeOpen(jsItemDetail.getString("items_timeOpen"));
                                            itemsModel.setTimeClose(jsItemDetail.getString("items_timeClose"));
                                            itemsModel.setPhone(jsItemDetail.getString("items_phone"));
                                            CoverItemsModel coverItems = new CoverItemsModel();
                                            coverItems.setCoverId(jsItemDetail.getInt("coverItem_id"));
                                            coverItems.setCoverPaths(jsItemDetail.getString("coverItem_paths"));
                                            coverItems.setCoverURL(jsItemDetail.getString("coverItem_url"));
                                            itemsModel.setCoverItemsModel(coverItems);
                                            CategoryModel category = new CategoryModel();
                                            category.setCategoryId(jsItemDetail.getInt("category_id"));
                                            category.setCategoryTH(jsItemDetail.getString("category_thai"));
                                            category.setCategoryEN(jsItemDetail.getString("category_english"));
                                            category.setCategoryLO(jsItemDetail.getString("category_laos"));
                                            category.setCategoryZH(jsItemDetail.getString("category_chinese"));
                                            itemsModel.setCategoryModel(category);


                                        }
                                    }
                                    touristAttractionsModel.setItemsModel(itemsModel);

                                    ArrayList<PhotoTourist> photoTouristArrayList = new ArrayList<>();
                                    for (int l = 0; l < jsonPhotoTourist.length() ; l++) {
                                        JSONObject jsPhotoTourist = jsonPhotoTourist.getJSONObject(l);
                                        PhotoTourist photoTourist = new PhotoTourist();

                                        if (jsPhotoTourist != null) {
                                            photoTourist.setPhotoTouristId(jsPhotoTourist.getInt("photoTourist_id"));
                                            photoTourist.setPhotoTouristPaths(jsPhotoTourist.getString("photoTourist_paths"));
                                            photoTourist.setPhotoTouristTH(jsPhotoTourist.getString("photoTourist_topicThai"));
                                            photoTourist.setPhotoTouristEN(jsPhotoTourist.getString("photoTourist_topicEnglish"));
                                            photoTourist.setPhotoTouristLO(jsPhotoTourist.getString("photoTourist_topicLaos"));
                                            photoTourist.setPhotoTouristZH(jsPhotoTourist.getString("photoTourist_topicChinese"));
                                            photoTouristArrayList.add(photoTourist);
                                        }
                                    }
                                    touristAttractionsModel.setPhotoTouristArrayList(photoTouristArrayList);

                                    touristAttractionsModelArrayList.add(touristAttractionsModel);
                                }

                                datesTripModel.setTouristAttractionsModelArrayList(touristAttractionsModelArrayList);
                            }
                            datesTripModelArrayList.add(datesTripModel);
                        }

                    }
                    programTourModel.setDatesTripModelArrayList(datesTripModelArrayList);

                    JSONArray jsonUser = object.optJSONArray("ProgramTour").getJSONObject(i).getJSONArray("User");
                    UserModel userModel = new UserModel();
                    for (int j = 0; j < jsonUser.length() ; j++) {
                        JSONObject jsUser = jsonUser.getJSONObject(j);
                        if (jsUser != null){
                            userModel.setUserId(jsUser.getInt("user_id"));
                            userModel.setFirstName(jsUser.getString("user_firstName"));
                            userModel.setLastName(jsUser.getString("user_lastName"));
                            userModel.setProfilePicUrl(jsUser.getString("user_profile_pic_url"));
                            programTourModel.setOfficial(jsUser.getBoolean("official"));
                            programTourModel.setUserModel(userModel);
                        }
                    }


                    list.add(programTourModel);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
