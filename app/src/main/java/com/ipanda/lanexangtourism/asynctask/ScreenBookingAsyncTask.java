package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BusinessModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.PartnerModel;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.ScreenBookingModel;
import com.ipanda.lanexangtourism.Model.ScreenNewsReleaseModel;
import com.ipanda.lanexangtourism.interface_callback.ScreenBookingCallBack;
import com.ipanda.lanexangtourism.interface_callback.ScreenNewsReleaseCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ScreenBookingAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<ScreenBookingModel> arrayList;
    private ScreenBookingCallBack callBackService;

    public ScreenBookingAsyncTask(ScreenBookingCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerScreenBooking(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ScreenBookingModel>  onParserContentToModel(String dataJSon) {
        ArrayList<ScreenBookingModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonObject = object.optJSONArray("Adsense");

            for (int j = 0; j < jsonObject.length() ; j++) {
                JSONObject jsObject = jsonObject.getJSONObject(j);
                ScreenBookingModel screen = new ScreenBookingModel();
                MenuItemModel menu = new MenuItemModel();
                ItemsModel items = new ItemsModel();
                ProgramTourModel programTour = new ProgramTourModel();
                BusinessModel business = new BusinessModel();

                if (jsObject != null){

                    screen.setId(jsObject.getInt("mediaAndAdvertising_id"));
                    screen.setScreenImage(jsObject.getString("coverItem_paths"));

                    switch (jsObject.getInt("menuItem_id")){
                        case 9:
                            items.setItemsId(jsObject.getInt("Items_items_id"));
                            menu.setMenuItemId(jsObject.getInt("menuItem_id"));
                            screen.setMenuItemModel(menu);
                            screen.setItemsModel(items);
                            break;
                        case 10:
                            items.setItemsId(jsObject.getInt("Items_items_id"));
                            menu.setMenuItemId(jsObject.getInt("menuItem_id"));
                            screen.setMenuItemModel(menu);
                            screen.setItemsModel(items);
                            break;
                        case 11:
                            business.setBusinessId(jsObject.getInt("Business_business_id"));
                            menu.setMenuItemId(jsObject.getInt("menuItem_id"));
                            screen.setMenuItemModel(menu);
                            screen.setBusinessModel(business);
                            break;
                    }

                    list.add(screen);

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
