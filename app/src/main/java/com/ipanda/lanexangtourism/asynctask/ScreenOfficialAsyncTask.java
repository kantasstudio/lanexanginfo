package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.ScreenOfficialModel;
import com.ipanda.lanexangtourism.Model.VideoContentModel;
import com.ipanda.lanexangtourism.interface_callback.ScreenOfficialCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ScreenOfficialAsyncTask extends AsyncTask<String,Integer,String> {

    private ArrayList<ScreenOfficialModel> arrayList;
    private ScreenOfficialCallBack callBackService;

    public ScreenOfficialAsyncTask(ScreenOfficialCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerScreenOfficial(arrayList);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ScreenOfficialModel>  onParserContentToModel(String dataJSon) {
        ArrayList<ScreenOfficialModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonOfficial = object.optJSONArray("Adsense");

            for (int j = 0; j < jsonOfficial.length() ; j++) {
                JSONObject jsOfficial = jsonOfficial.getJSONObject(j);
                ScreenOfficialModel screen = new ScreenOfficialModel();
                ProgramTourModel program = new ProgramTourModel();
                MenuItemModel menu = new MenuItemModel();
                if (jsOfficial != null){

                    screen.setId(jsOfficial.getInt("mediaAndAdvertising_id"));
                    screen.setScreenImage(jsOfficial.getString("coverItem_paths"));
                    menu.setMenuItemId(jsOfficial.getInt("menuItem_id"));
                    program.setProgramTourId(jsOfficial.getInt("ProgramTour_programTour_id"));
                    program.setMenuItemModel(menu);
                    screen.setProgramTourModel(program);

                    list.add(screen);

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

}
