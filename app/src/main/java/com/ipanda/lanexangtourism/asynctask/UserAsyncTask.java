package com.ipanda.lanexangtourism.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.CountryModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.interface_callback.UserCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class UserAsyncTask extends AsyncTask<String,Integer,String> {

    private UserModel userModel;
    private UserCallBack callBackService;

    public UserAsyncTask(UserCallBack callBackService) {
        this.callBackService = callBackService;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            userModel = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(userModel);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public UserModel onParserContentToModel(String dataJSon) {
        UserModel user = new UserModel();
        try {
            CountryModel country = new CountryModel();
            JSONArray array = new JSONArray(dataJSon);
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                if (jsonObject != null){
                    user.setUserId(jsonObject.getInt("user_id"));
                    user.setIdentification(jsonObject.getString("user_identification"));
                    user.setToken(jsonObject.getString("user_token"));
                    user.setEmail(jsonObject.getString("user_email"));
                    user.setFirstName(jsonObject.getString("user_firstName"));
                    user.setLastName(jsonObject.getString("user_lastName"));
                    user.setAge(jsonObject.getString("user_age"));
                    user.setGender(jsonObject.getString("user_gender"));
                    user.setPresentAddress(jsonObject.getString("user_presentAddress"));
                    user.setLoginWith(jsonObject.getString("user_loginWith"));
                    user.setProfilePicUrl(jsonObject.getString("user_profile_pic_url"));
                    user.setCountFollowers(jsonObject.getInt("count_followers"));
                    user.setCountFollowing(jsonObject.getInt("count_following"));
                    user.setCountReview(jsonObject.getInt("count_review"));
                    user.setCountRecordedList(jsonObject.getInt("count_recordedList"));
                    user.setCountProgramTour(jsonObject.getInt("count_programTour"));
                    country.setCountryId(jsonObject.getInt("Country_country_id"));
                    user.setCountryModel(country);

                    try {
                        user.setFollowState(jsonObject.getBoolean("follow_state"));
                    }catch (Exception e){
//                        e.printStackTrace();
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();

            try {
                CountryModel country = new CountryModel();
                 JSONObject jsonObject = new JSONObject(dataJSon);
                if (jsonObject != null){
                    user.setUserId(jsonObject.getInt("user_id"));
                    user.setIdentification(jsonObject.getString("user_identification"));
                    user.setToken(jsonObject.getString("user_token"));
                    user.setEmail(jsonObject.getString("user_email"));
                    user.setFirstName(jsonObject.getString("user_firstName"));
                    user.setLastName(jsonObject.getString("user_lastName"));
                    user.setAge(jsonObject.getString("user_age"));
                    user.setGender(jsonObject.getString("user_gender"));
                    user.setPresentAddress(jsonObject.getString("user_presentAddress"));
                    user.setLoginWith(jsonObject.getString("user_loginWith"));
                    user.setProfilePicUrl(jsonObject.getString("user_profile_pic_url"));
                    user.setCountFollowers(jsonObject.getInt("count_followers"));
                    user.setCountFollowing(jsonObject.getInt("count_following"));
                    user.setCountReview(jsonObject.getInt("count_review"));
                    user.setCountRecordedList(jsonObject.getInt("count_recordedList"));
                    user.setCountProgramTour(jsonObject.getInt("count_programTour"));
                    country.setCountryId(jsonObject.getInt("Country_country_id"));
                    user.setCountryModel(country);

                    try {
                        user.setFollowState(jsonObject.getBoolean("follow_state"));
                    }catch (Exception e1){
                        e1.printStackTrace();
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }

        }

        return user;
    }

}
