package com.ipanda.lanexangtourism.background_service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.ipanda.lanexangtourism.asynctask.UpdateLocatedPostRequest;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.IsStateUpdateLocatedCallBack;

import java.util.HashMap;


public class UpdateLocatedService extends Service implements LocationListener{

    String userId = null;
    double latitude;
    double longitude;
    LocationManager locationManager;
    Location locationResult;


    public UpdateLocatedService() {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        onTaskRemoved(intent);
//        userId = intent.getStringExtra("USER_ID");


        new Thread(new UpdateLocatedThread()).start();
        return START_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        startService(restartServiceIntent);
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    public class UpdateLocatedThread  implements Runnable, IsStateUpdateLocatedCallBack {

        public UpdateLocatedThread() {

        }


        @SuppressLint("MissingPermission")
        @Override
        public void run() {
            try {
                while (true) {
                    locationResult = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (locationResult == null) {
                        locationResult = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
/*                    Log.e("test","running in Background");
                    Log.e("test",userId+"");
                    Log.e("test",locationResult.getLatitude()+"");
                    Log.e("test",locationResult.getLongitude()+"");*/
                    setLocatedToMethodPost(userId,locationResult.getLatitude(),locationResult.getLongitude());
                    Thread.sleep(60000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        private void setLocatedToMethodPost(String userId, double latitude, double longitude){
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl("http://203.154.71.91/dasta_thailand/api/mobile/user/User/updateUserLocation");
            HashMap<String,String> paramsPost = new HashMap<>();
            paramsPost.put("user_id", userId);
            paramsPost.put("user_latitude", locationResult.getLatitude()+"");
            paramsPost.put("user_longitude", locationResult.getLongitude()+"");

            if (userId != null) {
                httpCallPost.setParams(paramsPost);
                new UpdateLocatedPostRequest(this).execute(httpCallPost);

            }

        }


        @Override
        public void onPreCallServiceUpdateLocated() {

        }

        @Override
        public void onCallServiceUpdateLocated() {

        }

        @Override
        public void onRequestCompleteUpdateLocated(boolean state) {
//            Log.e("check data",state+"");
            if (state){

            }

        }

        @Override
        public void onRequestFailedUpdateLocated(String result) {

        }


    }
}
