package com.ipanda.lanexangtourism.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DatabaseHelper extends SQLiteOpenHelper {

    private final String TAG = getClass().getSimpleName();

    private static String DB_NAME = "lanXangDB";

    private DatabaseSQL mSQL = new DatabaseSQL();

    public DatabaseHelper(Context context, int version) {
        super(context, DB_NAME, null, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG, "Create Tables");
        db.execSQL(mSQL.createTableProgramTour());
        db.execSQL(mSQL.createTableDatesTrip());
        db.execSQL(mSQL.createTableTouristAttractions());
        db.execSQL(mSQL.createTablePhotoTourist());
        db.execSQL(mSQL.createTableItems());
        db.execSQL(mSQL.createTableDetail());
        db.execSQL(mSQL.createTablePhotoDetail());
        db.execSQL(mSQL.createTableReviews());
        db.execSQL(mSQL.createTableDelicious());
        db.execSQL(mSQL.createTableProduct());
        db.execSQL(mSQL.createTablePhotoProduct());
        db.execSQL(mSQL.createTableFacilities());
        db.execSQL(mSQL.createTableRoom());
        db.execSQL(mSQL.createTablePhotoRoom());
        db.execSQL(mSQL.createTableCoverItems());
        db.execSQL(mSQL.createTableScopeArea());
        db.execSQL(mSQL.createTableTravelDetails());
        db.execSQL(mSQL.createTableTravelPeriod());
        db.execSQL(mSQL.createTableBookingCondition());
        db.execSQL(mSQL.createTableBusiness());
        db.execSQL(mSQL.createTableCar());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(TAG, "Upgrade Database from " + oldVersion + " to " + newVersion);
        db.execSQL(" DROP TABLE IF EXISTS ProgramTour");
        db.execSQL(" DROP TABLE IF EXISTS DatesTrip");
        db.execSQL(" DROP TABLE IF EXISTS TouristAttractions");
        db.execSQL(" DROP TABLE IF EXISTS PhotoTourist");
        db.execSQL(" DROP TABLE IF EXISTS Items");
        db.execSQL(" DROP TABLE IF EXISTS Detail");
        db.execSQL(" DROP TABLE IF EXISTS PhotoDetail");
        db.execSQL(" DROP TABLE IF EXISTS Reviews");
        db.execSQL(" DROP TABLE IF EXISTS Delicious");
        db.execSQL(" DROP TABLE IF EXISTS Product");
        db.execSQL(" DROP TABLE IF EXISTS PhotoProduct");
        db.execSQL(" DROP TABLE IF EXISTS Facilities");
        db.execSQL(" DROP TABLE IF EXISTS Room");
        db.execSQL(" DROP TABLE IF EXISTS PhotoRoom");
        db.execSQL(" DROP TABLE IF EXISTS CoverItems");
        db.execSQL(" DROP TABLE IF EXISTS ScopeArea");
        db.execSQL(" DROP TABLE IF EXISTS TravelDetails");
        db.execSQL(" DROP TABLE IF EXISTS TravelPeriod");
        db.execSQL(" DROP TABLE IF EXISTS BookingCondition");
        db.execSQL(" DROP TABLE IF EXISTS Business");
        db.execSQL(" DROP TABLE IF EXISTS Car");
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

}
