package com.ipanda.lanexangtourism.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.Model.BusinessModel;
import com.ipanda.lanexangtourism.Model.CarModel;
import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.DatesTripModel;
import com.ipanda.lanexangtourism.Model.DeliciousGuaranteeModel;
import com.ipanda.lanexangtourism.Model.FacilitiesModel;
import com.ipanda.lanexangtourism.Model.FoodModel;
import com.ipanda.lanexangtourism.Model.HotelAccommodationModel;
import com.ipanda.lanexangtourism.Model.ItemsContactModel;
import com.ipanda.lanexangtourism.Model.ItemsDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsHotelModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsRestaurantModel;
import com.ipanda.lanexangtourism.Model.ItemsShoppingModel;
import com.ipanda.lanexangtourism.Model.ItemsTopicDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsTourismModel;
import com.ipanda.lanexangtourism.Model.LikesModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.PeriodDayModel;
import com.ipanda.lanexangtourism.Model.PeriodTimeModel;
import com.ipanda.lanexangtourism.Model.PhotoTourist;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.ProductPhotoModel;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.RatingStarModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.Model.RoomPictureModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.TouristAttractionsModel;
import com.ipanda.lanexangtourism.Model.TravelDetailsModel;
import com.ipanda.lanexangtourism.Model.TravelPeriodModel;
import com.ipanda.lanexangtourism.Model.UserModel;

import java.util.ArrayList;

public class DatabaseManager {

    private Context mContext;
    private DatabaseHelper database;

    public DatabaseManager(Context mContext, DatabaseHelper database) {
        this.mContext = mContext;
        this.database = database;
    }

    //Insert
    public boolean addProgramTour(ArrayList<ProgramTourModel> programTourArrayList) {
        long result = -1;
        try {
            SQLiteDatabase db = database.getWritableDatabase();
            for (ProgramTourModel program : programTourArrayList) {
                ContentValues values = new ContentValues();
                values.put("programTour_id", program.getProgramTourId());
                values.put("programTour_nameThai", program.getProgramTourNameTH());
                values.put("programTour_nameEnglish", program.getProgramTourNameEN());
                values.put("programTour_nameChinese", program.getProgramTourNameZH());
                values.put("programTour_nameLaos", program.getProgramTourNameLO());
                values.put("programTour_interestingThingsThai", program.getInterestingThingsTH());
                values.put("programTour_interestingThingsEnglish", program.getInterestingThingsEN());
                values.put("programTour_interestingThingsChinese", program.getInterestingThingsZH());
                values.put("programTour_interestingThingsLaos", program.getInterestingThingsLO());
                values.put("programTour_readcount", program.getCountView());
                values.put("Like_count", program.getLikesModel().getLikes());
                values.put("Like_state", program.getLikesModel().isLikeState());
                values.put("menuItem_id", program.getMenuItemModel().getMenuItemId());
                values.put("menuItem_thai", program.getMenuItemModel().getMenuItemTH());
                values.put("menuItem_english", program.getMenuItemModel().getMenuItemEN());
                values.put("menuItem_chinese", program.getMenuItemModel().getMenuItemZH());
                values.put("menuItem_laos", program.getMenuItemModel().getMenuItemLO());
                values.put("category_id", program.getCategoryModel().getCategoryId());
                values.put("category_thai", program.getCategoryModel().getCategoryTH());
                values.put("category_english", program.getCategoryModel().getCategoryEN());
                values.put("category_chinese", program.getCategoryModel().getCategoryZH());
                values.put("category_laos", program.getCategoryModel().getCategoryLO());
                values.put("subcategory_id", program.getSubCategoryModel().getCategoryId());
                values.put("subcategory_thai", program.getSubCategoryModel().getCategoryTH());
                values.put("subcategory_english", program.getSubCategoryModel().getCategoryEN());
                values.put("subcategory_chinese", program.getSubCategoryModel().getCategoryZH());
                values.put("subcategory_laos", program.getSubCategoryModel().getCategoryLO());
                values.put("coverItem_id", program.getCoverItemsModel().getCoverId());
                values.put("coverItem_paths", program.getCoverItemsModel().getCoverPaths());
                values.put("coverItem_url", program.getCoverItemsModel().getCoverURL());
                values.put("programTour_createdDTTM", program.getCreatedDTTM());
                values.put("programTour_updatedDTTM", program.getUpdatedDTTM());
                values.put("user_id", program.getUserModel().getUserId());
                values.put("user_firstName", program.getUserModel().getFirstName());
                values.put("user_lastName", program.getUserModel().getLastName());
                values.put("user_profile_pic_url", program.getUserModel().getProfilePicUrl());
                values.put("official", program.isOfficial());
                result = db.insert("ProgramTour", null, values);

                for (DatesTripModel dt : program.getDatesTripModelArrayList()) {
                    ContentValues values_dates = new ContentValues();
                    values_dates.put("datesTrip_id", dt.getDatesTripId());
                    values_dates.put("datesTrip_topicThai", dt.getDatesTripTopicThai());
                    values_dates.put("datesTrip_topicEnglish", dt.getDatesTripTopicEnglish());
                    values_dates.put("datesTrip_topicLaos", dt.getDatesTripTopicLaos());
                    values_dates.put("datesTrip_topicChinese", dt.getDatesTripTopicChinese());
                    values_dates.put("programTour_id", program.getProgramTourId());
                    result = db.insert("DatesTrip", null, values_dates);


                    for (TouristAttractionsModel ta : dt.getTouristAttractionsModelArrayList()) {
                        ContentValues values_tourist = new ContentValues();
                        values_tourist.put("touristAttractions_id", ta.getAttractionsId());
                        values_tourist.put("detail_Thai", ta.getAttractionsTopicTH());
                        values_tourist.put("detail_English", ta.getAttractionsTopicEN());
                        values_tourist.put("detail_Chinese", ta.getAttractionsTopicZH());
                        values_tourist.put("detail_Laos", ta.getAttractionsTopicLO());
                        values_tourist.put("datesTrip_id", dt.getDatesTripId());
                        values_tourist.put("items_id", ta.getItemsModel().getItemsId());
                        result = db.insert("TouristAttractions", null, values_tourist);


                        for (PhotoTourist pt : ta.getPhotoTouristArrayList()) {
                            ContentValues values_photo = new ContentValues();
                            values_photo.put("photoTourist_id", pt.getPhotoTouristId());
                            values_photo.put("photoTourist_paths", pt.getPhotoTouristPaths());
                            values_photo.put("photoTourist_topicThai", pt.getPhotoTouristTH());
                            values_photo.put("photoTourist_topicEnglish", pt.getPhotoTouristEN());
                            values_photo.put("photoTourist_topicLaos", pt.getPhotoTouristLO());
                            values_photo.put("photoTourist_topicChinese", pt.getPhotoTouristZH());
                            values_photo.put("TouristAttractions_id", ta.getAttractionsId());
                            result = db.insert("PhotoTourist", null, values_photo);
                        }

                    }

                }

            }
            db.close(); // Closing database connection
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    public boolean addItems(ArrayList<ItemsModel> itemsModelArrayList) {
        long result = -1;
        try {
            SQLiteDatabase db = database.getWritableDatabase();
            for (ItemsModel item : itemsModelArrayList) {
                ContentValues values_items = new ContentValues();
                values_items.put("items_id",item.getItemsId());
                values_items.put("items_topicThai", item.getTopicTH());
                values_items.put("items_topicEnglish", item.getTopicEN());
                values_items.put("items_topicChinese", item.getTopicZH());
                values_items.put("items_topicLaos", item.getTopicLO());
                values_items.put("items_latitude", item.getLatitude());
                values_items.put("items_longitude", item.getLongitude());
                values_items.put("items_contactThai", item.getContactTH());
                values_items.put("items_contactEnglish", item.getContactEN());
                values_items.put("items_contactChinese", item.getContactZH());
                values_items.put("items_contactLaos", item.getContactLO());
                values_items.put("items_phone", item.getPhone());
                values_items.put("items_email", item.getEmail());
                values_items.put("items_line", item.getLine());
                values_items.put("items_facebookPage", item.getFacebookPage());
                values_items.put("items_www", "");
                values_items.put("item_ar_url", item.getArURL());
                values_items.put("coverItem_id", item.getCoverItemsModel().getCoverId());
                values_items.put("coverItem_paths", item.getCoverItemsModel().getCoverPaths());
                values_items.put("coverItem_url", item.getCoverItemsModel().getCoverURL());
                values_items.put("menuItem_id", item.getMenuItemModel().getMenuItemId());
                values_items.put("menuItem_thai", item.getMenuItemModel().getMenuItemTH());
                values_items.put("menuItem_english", item.getMenuItemModel().getMenuItemEN());
                values_items.put("menuItem_chinese", item.getMenuItemModel().getMenuItemZH());
                values_items.put("menuItem_laos", item.getMenuItemModel().getMenuItemLO());
                values_items.put("category_id", "");
                values_items.put("category_thai", "");
                values_items.put("category_english", "");
                values_items.put("category_chinese", "");
                values_items.put("category_laos", "");
                values_items.put("subcategory_id", item.getSubCategoryModelArrayList().get(0).getCategoryId());
                values_items.put("subcategory_thai", item.getSubCategoryModelArrayList().get(0).getCategoryTH());
                values_items.put("subcategory_english", item.getSubCategoryModelArrayList().get(0).getCategoryEN());
                values_items.put("subcategory_chinese", item.getSubCategoryModelArrayList().get(0).getCategoryZH());
                values_items.put("subcategory_laos", item.getSubCategoryModelArrayList().get(0).getCategoryLO());
                values_items.put("provinces_id", "");
                values_items.put("provinces_thai", "");
                values_items.put("provinces_english", "");
                values_items.put("provinces_laos", "");
                values_items.put("provinces_chinese", "");
                values_items.put("districts_id", "");
                values_items.put("districts_thai", "");
                values_items.put("districts_english", "");
                values_items.put("districts_laos", "");
                values_items.put("districts_chinese", "");
                values_items.put("subdistricts_id", "");
                values_items.put("subdistricts_thai", "");
                values_items.put("subdistricts_english", "");
                values_items.put("subdistricts_laos", "");
                values_items.put("subdistricts_chinese", "");
                values_items.put("items_price", item.getPrice());
                values_items.put("items_highlightsThai", item.getHighlightsTH());
                values_items.put("items_highlightsEnglish", item.getHighlightsEN());
                values_items.put("items_highlightsChinese", item.getHighlightsZH());
                values_items.put("items_highlightsLaos", item.getHighlightsLO());
                values_items.put("items_locationInformationThai", item.getLocationInformationTH());
                values_items.put("items_locationInformationEnglish", item.getLocationInformationEN());
                values_items.put("items_locationInformationChinese", item.getLocationInformationZH());
                values_items.put("items_locationInformationLaos", item.getLocationInformationLO());
                values_items.put("items_timeOpen", item.getTimeOpen());
                values_items.put("items_timeClose", item.getTimeClose());
                values_items.put("open_day_id", item.getPeriodDayModel().getPeriodOpenId());
                values_items.put("open_day_thai", item.getPeriodDayModel().getPeriodOpenTH());
                values_items.put("open_day_english", item.getPeriodDayModel().getPeriodOpenEN());
                values_items.put("open_day_chinese", item.getPeriodDayModel().getPeriodOpenZH());
                values_items.put("open_day_laos", item.getPeriodDayModel().getPeriodOpenLO());
                values_items.put("closed_day_id", item.getPeriodDayModel().getPeriodCloseId());
                values_items.put("closed_day_thai", item.getPeriodDayModel().getPeriodCloseTH());
                values_items.put("closed_day_english", item.getPeriodDayModel().getPeriodCloseEN());
                values_items.put("closed_day_chinese", item.getPeriodDayModel().getPeriodCloseZH());
                values_items.put("closed_day_laos", item.getPeriodDayModel().getPeriodCloseLO());
                if (item.getBookMarksModel() != null) {
                    values_items.put("BookMarks_state", item.getBookMarksModel().isBookmarksState());
                }else {
                    values_items.put("BookMarks_state", false);
                }


                for (ItemsDetailModel detail : item.getItemsDetailModelArrayList()) {
                    ContentValues values_detail = new ContentValues();
                    values_detail.put("detail_id", detail.getDetailId());
                    values_detail.put("detail_textThai", detail.getDetailTH());
                    values_detail.put("detail_textEnglish", detail.getDetailEN());
                    values_detail.put("detail_textLaos", detail.getDetailLO());
                    values_detail.put("detail_textChinese", detail.getDetailZH());
                    values_detail.put("topicdetail_id", detail.getItemsTopicDetailModel().getTopicDetailId());
                    values_detail.put("topicdetail_Thai", detail.getItemsTopicDetailModel().getTopicDetailTH());
                    values_detail.put("topicdetail_English", detail.getItemsTopicDetailModel().getTopicDetailEN());
                    values_detail.put("topicdetail_Laos", detail.getItemsTopicDetailModel().getTopicDetailLO());
                    values_detail.put("topicdetail_Chinese", detail.getItemsTopicDetailModel().getTopicDetailZH());
                    values_detail.put("items_id", item.getItemsId());
                    result = db.insert("Detail", null, values_detail);

                    for (ItemsPhotoDetailModel photo: detail.getItemsPhotoDetailModels()) {
                        ContentValues values_photo = new ContentValues();
                        values_photo.put("photo_id", photo.getPhotoId());
                        values_photo.put("photo_paths", photo.getPhotoPaths());
                        values_photo.put("photo_textThai", photo.getPhotoTextTH());
                        values_photo.put("photo_textEnglish", photo.getPhotoTextEN());
                        values_photo.put("photo_textChinese", photo.getPhotoTextZH());
                        values_photo.put("photo_textLaos", photo.getPhotoTextLO());
                        values_photo.put("detail_id", detail.getDetailId());
                        result = db.insert("PhotoDetail", null, values_photo);
                    }

                }

                values_items.put("AverageReview", item.getRatingStarModel().getAverageReview());
                values_items.put("CountReview", item.getRatingStarModel().getCountReview());
                values_items.put("OneStar", item.getRatingStarModel().getOneStar());
                values_items.put("TwoStar", item.getRatingStarModel().getTwoStar());
                values_items.put("ThreeStar", item.getRatingStarModel().getThreeStar());
                values_items.put("FourStar", item.getRatingStarModel().getFourStar());
                values_items.put("FiveStar", item.getRatingStarModel().getFiveStar());

                if (item.getRatingStarModel().getReviewModelArrayList() != null && item.getRatingStarModel().getReviewModelArrayList().size() != 0) {
                    for (ReviewModel review : item.getRatingStarModel().getReviewModelArrayList()) {
                        ContentValues values_review = new ContentValues();
                        values_review.put("review_id", review.getReviewId());
                        values_review.put("review_timestamp", review.getReviewTimestamp());
                        values_review.put("review_rating", review.getReviewRating());
                        values_review.put("review_text", review.getReviewText());
                        values_review.put("user_id", review.getUserModel().getUserId());
                        values_review.put("user_firstName", review.getUserModel().getFirstName());
                        values_review.put("user_lastName", review.getUserModel().getLastName());
                        values_review.put("user_profile_pic_url", review.getUserModel().getProfilePicUrl());
                        values_review.put("items_id", review.getItemsModel().getItemsId());
                        values_review.put("items_topicThai", review.getItemsModel().getTopicTH());
                        values_review.put("items_topicEnglish", review.getItemsModel().getTopicEN());
                        values_review.put("items_topicChinese", review.getItemsModel().getTopicZH());
                        values_review.put("items_topicLaos", review.getItemsModel().getTopicLO());
                        result = db.insert("Reviews", null, values_review);
                    }
                }

                if (item.getDeliciousGuaranteeModelArrayList() != null && item.getDeliciousGuaranteeModelArrayList().size() != 0){
                    for (DeliciousGuaranteeModel guarantee : item.getDeliciousGuaranteeModelArrayList()){
                        ContentValues values_guarantee = new ContentValues();
                        values_guarantee.put("delicious_id", guarantee.getGuaranteeId());
                        values_guarantee.put("guarantee_textThai", guarantee.getGuaranteeTH());
                        values_guarantee.put("guarantee_textEnglish", guarantee.getGuaranteeEN());
                        values_guarantee.put("guarantee_textChinese", guarantee.getGuaranteeZH());
                        values_guarantee.put("guarantee_textLaos", guarantee.getGuaranteeLO());
                        values_guarantee.put("guarantee_paths", guarantee.getGuaranteePaths());
                        values_guarantee.put("items_id", item.getItemsId());
                        result = db.insert("Delicious", null, values_guarantee);
                    }
                }

                if (item.getProductModelArrayList() != null && item.getProductModelArrayList().size() != 0){
                    for (ProductModel product : item.getProductModelArrayList()){
                        ContentValues values_product = new ContentValues();
                        values_product.put("product_id", product.getProductId());
                        values_product.put("product_namesThai", product.getProductNamesTH());
                        values_product.put("product_namesEnglish", product.getProductNamesEN());
                        values_product.put("product_namesChinese", product.getProductNamesZH());
                        values_product.put("product_namesLaos", product.getProductNamesLO());
                        values_product.put("product_price", product.getProductPrice());
                        values_product.put("product_descriptionThai", product.getDescriptionTH());
                        values_product.put("product_descriptionEnglish", product.getDescriptionEN());
                        values_product.put("product_descriptionChinese", product.getDescriptionZH());
                        values_product.put("product_descriptionLaos", product.getDescriptionLO());
                        values_product.put("items_id", item.getItemsId());
                        result = db.insert("Product", null, values_product);

                        if (product.getProductPhotoModelArrayList() != null && product.getProductPhotoModelArrayList().size() != 0) {
                            for (ProductPhotoModel photoProduct : product.getProductPhotoModelArrayList()) {
                                ContentValues values_photoProduct = new ContentValues();
                                values_photoProduct.put("photoProduct_id", photoProduct.getProductPhotoId());
                                values_photoProduct.put("photoProduct_paths", photoProduct.getProductPhotoPaths());
                                values_photoProduct.put("product_id", product.getProductId());
                                result = db.insert("PhotoProduct", null, values_photoProduct);
                            }
                        }

                    }
                }

                if (item.getFacilitiesModelArrayList() != null && item.getFacilitiesModelArrayList().size() != 0){
                    for (FacilitiesModel facilities : item.getFacilitiesModelArrayList()){
                        ContentValues values_facilities = new ContentValues();
                        values_facilities.put("facilities_id", facilities.getFacilitiesId());
                        values_facilities.put("facilities_textThai", facilities.getFacilitiesTH());
                        values_facilities.put("facilities_textEnglish", facilities.getFacilitiesEN());
                        values_facilities.put("facilities_textChinese", facilities.getFacilitiesZH());
                        values_facilities.put("facilities_textLaos", facilities.getFacilitiesLO());
                        values_facilities.put("items_id", item.getItemsId());
                        result = db.insert("Facilities", null, values_facilities);
                    }
                }

                if (item.getRoomModelArrayList() != null && item.getRoomModelArrayList().size() != 0){
                    for (RoomModel room : item.getRoomModelArrayList()){
                        ContentValues values_room = new ContentValues();
                        values_room.put("room_id", room.getRoomId());
                        values_room.put("room_topicThai", room.getTopicTH());
                        values_room.put("room_topicEnglish", room.getTopicEN());
                        values_room.put("room_topicChinese", room.getTopicZH());
                        values_room.put("room_topicLaos", room.getTopicLO());
                        values_room.put("room_descriptionThai", room.getDescriptionTH());
                        values_room.put("room_descriptionEnglish", room.getDescriptionEN());
                        values_room.put("room_descriptionChinese", room.getDescriptionZH());
                        values_room.put("room_descriptionLaos", room.getDescriptionLO());
                        values_room.put("room_detailsThai", room.getDetailsTH());
                        values_room.put("room_detailsEnglish", room.getDetailsEN());
                        values_room.put("room_detailsChinese", room.getDetailsZH());
                        values_room.put("room_detailsLaos", room.getDetailsLO());
                        values_room.put("room_price", room.getPrice());
                        values_room.put("room_breakfast", room.getBreakFast());
                        values_room.put("items_id", item.getItemsId());
                        result = db.insert("Room", null, values_room);

                        if (room.getRoomPictureModelArrayList() != null && room.getRoomPictureModelArrayList().size() != 0){
                            for (RoomPictureModel picture : room.getRoomPictureModelArrayList()){
                                ContentValues values_picture = new ContentValues();
                                values_picture.put("pictureRoom_id", picture.getPictureId());
                                values_picture.put("pictureRoom_paths",picture.getPicturePaths());
                                values_picture.put("room_id", room.getRoomId());
                                result = db.insert("PhotoRoom", null, values_picture);
                            }
                        }

                    }
                }

                result = db.insert("Items", null, values_items);
            }

            db.close(); // Closing database connection

        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    public boolean addPackageTour(ArrayList<ItemsModel> itemsModelArrayList) {
        long result = -1;
        try {
            SQLiteDatabase db = database.getWritableDatabase();
            for (ItemsModel item : itemsModelArrayList) {
                ContentValues values_items = new ContentValues();
                values_items.put("items_id",item.getItemsId());
                values_items.put("items_topicThai", item.getTopicTH());
                values_items.put("items_topicEnglish", item.getTopicEN());
                values_items.put("items_topicChinese", item.getTopicZH());
                values_items.put("items_topicLaos", item.getTopicLO());
                values_items.put("menuItem_id", item.getMenuItemModel().getMenuItemId());
                values_items.put("menuItem_thai", item.getMenuItemModel().getMenuItemTH());
                values_items.put("menuItem_english", item.getMenuItemModel().getMenuItemEN());
                values_items.put("menuItem_chinese", item.getMenuItemModel().getMenuItemZH());
                values_items.put("menuItem_laos", item.getMenuItemModel().getMenuItemLO());
                values_items.put("category_id", item.getCategoryModel().getCategoryId());
                values_items.put("category_thai", item.getCategoryModel().getCategoryTH());
                values_items.put("category_english", item.getCategoryModel().getCategoryEN());
                values_items.put("category_chinese", item.getCategoryModel().getCategoryZH());
                values_items.put("category_laos", item.getCategoryModel().getCategoryLO());
                values_items.put("subcategory_id", item.getSubCategoryModelArrayList().get(0).getCategoryId());
                values_items.put("subcategory_thai", item.getSubCategoryModelArrayList().get(0).getCategoryTH());
                values_items.put("subcategory_english", item.getSubCategoryModelArrayList().get(0).getCategoryEN());
                values_items.put("subcategory_chinese", item.getSubCategoryModelArrayList().get(0).getCategoryZH());
                values_items.put("subcategory_laos", item.getSubCategoryModelArrayList().get(0).getCategoryLO());

                if (item.getCoverItemsModelArrayList() != null && item.getCoverItemsModelArrayList().size() != 0){
                    for (CoverItemsModel cover : item.getCoverItemsModelArrayList()){
                        ContentValues values_cover = new ContentValues();
                        values_cover.put("coverItem_id", cover.getCoverId());
                        values_cover.put("coverItem_paths", cover.getCoverPaths());
                        values_cover.put("coverItem_url", cover.getCoverURL());
                        values_cover.put("items_id", item.getItemsId());
                        result = db.insert("CoverItems", null, values_cover);
                    }
                }

                if (item.getProvincesModelArrayList() != null && item.getProvincesModelArrayList().size() != 0){
                    for (ProvincesModel provinces : item.getProvincesModelArrayList()){
                        ContentValues values_provinces = new ContentValues();
                        values_provinces.put("provinces_id", provinces.getProvincesId());
                        values_provinces.put("provinces_thai", provinces.getProvincesTH());
                        values_provinces.put("provinces_english", provinces.getProvincesEN());
                        values_provinces.put("provinces_laos", provinces.getProvincesZH());
                        values_provinces.put("provinces_chinese", provinces.getProvincesLO());
                        values_provinces.put("items_id", item.getItemsId());
                        result = db.insert("ScopeArea", null, values_provinces);
                    }
                }

                if (item.getTravelDetailsModelArrayList() != null && item.getTravelDetailsModelArrayList().size() != 0){
                    int i = 0;
                    for (TravelDetailsModel details : item.getTravelDetailsModelArrayList()){
                        ContentValues values_details = new ContentValues();
                        values_details.put("travelDetails_id", details.getDetailsId());
                        values_details.put("travelDetails_dateThai", details.getDetailsDateTH());
                        values_details.put("travelDetails_dateEnglish", details.getDetailsDateEN());
                        values_details.put("travelDetails_dateChinese", details.getDetailsDateZH());
                        values_details.put("travelDetails_dateLaos", details.getDetailsDateLO());
                        values_details.put("travelDetails_textThai", details.getDetailsTextTH());
                        values_details.put("travelDetails_textEnglish", details.getDetailsTextEN());
                        values_details.put("travelDetails_textChinese", details.getDetailsTextZH());
                        values_details.put("travelDetails_textLaos", details.getDetailsTextLO());
                        values_details.put("items_id", item.getItemsId());

                        if (item.getHotelAccommodationModelArrayList() != null && item.getHotelAccommodationModelArrayList().size() != 0) {
                            values_details.put("travelDetails_hotel_nameThai", item.getHotelAccommodationModelArrayList().get(i).getAccommodationTH());
                            values_details.put("travelDetails_hotel_nameEnglish", item.getHotelAccommodationModelArrayList().get(i).getAccommodationEN());
                            values_details.put("travelDetails_hotel_nameChinese", item.getHotelAccommodationModelArrayList().get(i).getAccommodationLO());
                            values_details.put("travelDetails_hotel_nameLaos", item.getHotelAccommodationModelArrayList().get(i).getAccommodationZH());
                        }

                        if (item.getFoodModelArrayList() != null && item.getFoodModelArrayList().size() != 0){
                            values_details.put("travelDetails_food_breakfast", item.getFoodModelArrayList().get(i).isFoodBreakFast());
                            values_details.put("travelDetails_food_lunch", item.getFoodModelArrayList().get(i).isFoodLunch());
                            values_details.put("travelDetails_food_dinner", item.getFoodModelArrayList().get(i).isFoodDinner());
                        }

                        i++;
                        result = db.insert("TravelDetails", null, values_details);
                    }
                }

                if (item.getTravelPeriodModelArrayList() != null && item.getTravelPeriodModelArrayList().size() != 0){
                    for (TravelPeriodModel period : item.getTravelPeriodModelArrayList()){
                        ContentValues values_period = new ContentValues();
                        values_period.put("travelPeriod_id", period.getPeriodId());
                        values_period.put("travelPeriod_amount", period.getAmount());
                        values_period.put("travelPeriod_time_period_start", period.getPeriodStatus());
                        values_period.put("travelPeriod_time_period_end", period.getPeriodEnd());
                        values_period.put("travelPeriod_adult_price", period.getAdultPrice());
                        values_period.put("travelPeriod_adult_special_price", period.getAdultSpecialPrice());
                        values_period.put("travelPeriod_child_price", period.getChildPrice());
                        values_period.put("travelPeriod_child_special_price", period.getChildSpecialPrice());
                        values_period.put("items_id", item.getItemsId());
                        result = db.insert("TravelPeriod", null, values_period);
                    }
                }

                if (item.getBookingConditionModelArrayList() != null && item.getBookingConditionModelArrayList().size() != 0){
                    for (BookingConditionModel condition : item.getBookingConditionModelArrayList()){
                        ContentValues values_condition = new ContentValues();
                        values_condition.put("condition_id", condition.getConditionId());
                        values_condition.put("conditionCol_detailThai", condition.getConditionDetailTH());
                        values_condition.put("conditionCol_detailEnglish", condition.getConditionDetailEN());
                        values_condition.put("conditionCol_detailChinese", condition.getConditionDetailZH());
                        values_condition.put("conditionCol_detailLaos", condition.getConditionDetailLO());
                        values_condition.put("conditionCategory_topicThai", condition.getConditionTopicTH());
                        values_condition.put("conditionCategory_topicEnglish", condition.getConditionTopicEN());
                        values_condition.put("conditionCategory_topicChinese", condition.getConditionTopicZH());
                        values_condition.put("conditionCategory_topicLaos", condition.getConditionTopicLO());
                        values_condition.put("items_id", item.getItemsId());
                        result = db.insert("BookingCondition", null, values_condition);
                    }
                }

                if (item.getBusinessModel() != null){
                   ContentValues values_business = new ContentValues();
                   values_business.put("business_id", item.getBusinessModel().getBusinessId());
                   values_business.put("business_nameThai", item.getBusinessModel().getNameTH());
                   values_business.put("business_nameEnglish", item.getBusinessModel().getNameEN());
                   values_business.put("business_nameChinese", item.getBusinessModel().getNameZH());
                   values_business.put("business_nameLaos", item.getBusinessModel().getNameLO());
                   values_business.put("business_presentAddress", item.getBusinessModel().getPresentAddress());
                   values_business.put("business_license_paths", item.getBusinessModel().getLicensePaths());
                   values_business.put("business_operator_number", item.getBusinessModel().getOperatorNumber());
                   values_business.put("business_phone", item.getBusinessModel().getPhone());
                   values_business.put("business_www", item.getBusinessModel().getWeb());
                   values_business.put("business_facebook", item.getBusinessModel().getFacebook());
                   values_business.put("business_line", item.getBusinessModel().getLine());
                   values_business.put("items_id", item.getItemsId());
                   result = db.insert("Business", null, values_business);
                }

                result = db.insert("Items", null, values_items);
            }

            db.close(); // Closing database connection

        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    public boolean addTicketEvent(ArrayList<ItemsModel> itemsModelArrayList) {
        long result = -1;
        try {
            SQLiteDatabase db = database.getWritableDatabase();
            for (ItemsModel item : itemsModelArrayList) {
                ContentValues values_items = new ContentValues();
                values_items.put("items_id",item.getItemsId());
                values_items.put("items_topicThai", item.getTopicTH());
                values_items.put("items_topicEnglish", item.getTopicEN());
                values_items.put("items_topicChinese", item.getTopicZH());
                values_items.put("items_topicLaos", item.getTopicLO());
                values_items.put("items_latitude", item.getLatitude());
                values_items.put("items_longitude", item.getLongitude());
                values_items.put("items_contactThai", item.getContactTH());
                values_items.put("items_contactEnglish", item.getContactEN());
                values_items.put("items_contactChinese", item.getContactZH());
                values_items.put("items_contactLaos", item.getContactLO());
                values_items.put("items_phone", item.getPhone());
                values_items.put("items_email", item.getEmail());
                values_items.put("items_line", item.getLine());
                values_items.put("items_facebookPage", item.getFacebookPage());
                values_items.put("items_www", "");
                values_items.put("item_ar_url", item.getArURL());
                values_items.put("menuItem_id", item.getMenuItemModel().getMenuItemId());
                values_items.put("menuItem_thai", item.getMenuItemModel().getMenuItemTH());
                values_items.put("menuItem_english", item.getMenuItemModel().getMenuItemEN());
                values_items.put("menuItem_chinese", item.getMenuItemModel().getMenuItemZH());
                values_items.put("menuItem_laos", item.getMenuItemModel().getMenuItemLO());
                values_items.put("category_id", item.getCategoryModel().getCategoryId());
                values_items.put("category_thai", item.getCategoryModel().getCategoryTH());
                values_items.put("category_english", item.getCategoryModel().getCategoryEN());
                values_items.put("category_chinese", item.getCategoryModel().getCategoryZH());
                values_items.put("category_laos", item.getCategoryModel().getCategoryLO());
                values_items.put("subcategory_id", item.getSubCategoryModelArrayList().get(0).getCategoryId());
                values_items.put("subcategory_thai", item.getSubCategoryModelArrayList().get(0).getCategoryTH());
                values_items.put("subcategory_english", item.getSubCategoryModelArrayList().get(0).getCategoryEN());
                values_items.put("subcategory_chinese", item.getSubCategoryModelArrayList().get(0).getCategoryZH());
                values_items.put("subcategory_laos", item.getSubCategoryModelArrayList().get(0).getCategoryLO());
                values_items.put("provinces_id", item.getProvincesModel().getProvincesId());
                values_items.put("provinces_thai", item.getProvincesModel().getProvincesTH());
                values_items.put("provinces_english", item.getProvincesModel().getProvincesEN());
                values_items.put("provinces_laos", item.getProvincesModel().getProvincesLO());
                values_items.put("provinces_chinese", item.getProvincesModel().getProvincesZH());
                values_items.put("districts_id", item.getProvincesModel().getDistrictsModel().getDistrictsId());
                values_items.put("districts_thai", item.getProvincesModel().getDistrictsModel().getDistrictsTH());
                values_items.put("districts_english", item.getProvincesModel().getDistrictsModel().getDistrictsEN());
                values_items.put("districts_laos", item.getProvincesModel().getDistrictsModel().getDistrictsLO());
                values_items.put("districts_chinese", item.getProvincesModel().getDistrictsModel().getDistrictsZH());
                values_items.put("subdistricts_id", item.getProvincesModel().getDistrictsModel().getSubDistrictsModel().getSubDistrictsId());
                values_items.put("subdistricts_thai", item.getProvincesModel().getDistrictsModel().getSubDistrictsModel().getSubDistrictsTH());
                values_items.put("subdistricts_english", item.getProvincesModel().getDistrictsModel().getSubDistrictsModel().getSubDistrictsEN());
                values_items.put("subdistricts_laos", item.getProvincesModel().getDistrictsModel().getSubDistrictsModel().getSubDistrictsLO());
                values_items.put("subdistricts_chinese", item.getProvincesModel().getDistrictsModel().getSubDistrictsModel().getSubDistrictsZH());
                values_items.put("items_price", item.getPrice());
                values_items.put("items_highlightsThai", item.getHighlightsTH());
                values_items.put("items_highlightsEnglish", item.getHighlightsEN());
                values_items.put("items_highlightsChinese", item.getHighlightsZH());
                values_items.put("items_highlightsLaos", item.getHighlightsLO());
                values_items.put("items_timeOpen", item.getTimeOpen());
                values_items.put("items_timeClose", item.getTimeClose());
                values_items.put("open_day_id", item.getPeriodDayModel().getPeriodOpenId());
                values_items.put("open_day_thai", item.getPeriodDayModel().getPeriodOpenTH());
                values_items.put("open_day_english", item.getPeriodDayModel().getPeriodOpenEN());
                values_items.put("open_day_chinese", item.getPeriodDayModel().getPeriodOpenZH());
                values_items.put("open_day_laos", item.getPeriodDayModel().getPeriodOpenLO());
                values_items.put("closed_day_id", item.getPeriodDayModel().getPeriodCloseId());
                values_items.put("closed_day_thai", item.getPeriodDayModel().getPeriodCloseTH());
                values_items.put("closed_day_english", item.getPeriodDayModel().getPeriodCloseEN());
                values_items.put("closed_day_chinese", item.getPeriodDayModel().getPeriodCloseZH());
                values_items.put("closed_day_laos", item.getPeriodDayModel().getPeriodCloseLO());

                for (ItemsDetailModel detail : item.getItemsDetailModelArrayList()) {
                    ContentValues values_detail = new ContentValues();
                    values_detail.put("detail_id", detail.getDetailId());
                    values_detail.put("detail_textThai", detail.getDetailTH());
                    values_detail.put("detail_textEnglish", detail.getDetailEN());
                    values_detail.put("detail_textLaos", detail.getDetailLO());
                    values_detail.put("detail_textChinese", detail.getDetailZH());
                    values_detail.put("topicdetail_id", detail.getItemsTopicDetailModel().getTopicDetailId());
                    values_detail.put("topicdetail_Thai", detail.getItemsTopicDetailModel().getTopicDetailTH());
                    values_detail.put("topicdetail_English", detail.getItemsTopicDetailModel().getTopicDetailEN());
                    values_detail.put("topicdetail_Laos", detail.getItemsTopicDetailModel().getTopicDetailLO());
                    values_detail.put("topicdetail_Chinese", detail.getItemsTopicDetailModel().getTopicDetailZH());
                    values_detail.put("items_id", item.getItemsId());
                    result = db.insert("Detail", null, values_detail);

                    for (ItemsPhotoDetailModel photo: detail.getItemsPhotoDetailModels()) {
                        ContentValues values_photo = new ContentValues();
                        values_photo.put("photo_id", photo.getPhotoId());
                        values_photo.put("photo_paths", photo.getPhotoPaths());
                        values_photo.put("photo_textThai", photo.getPhotoTextTH());
                        values_photo.put("photo_textEnglish", photo.getPhotoTextEN());
                        values_photo.put("photo_textChinese", photo.getPhotoTextZH());
                        values_photo.put("photo_textLaos", photo.getPhotoTextLO());
                        values_photo.put("detail_id", detail.getDetailId());
                        result = db.insert("PhotoDetail", null, values_photo);
                    }

                }

                if (item.getCoverItemsModelArrayList() != null && item.getCoverItemsModelArrayList().size() != 0){
                    for (CoverItemsModel cover : item.getCoverItemsModelArrayList()){
                        ContentValues values_cover = new ContentValues();
                        values_cover.put("coverItem_id", cover.getCoverId());
                        values_cover.put("coverItem_paths", cover.getCoverPaths());
                        values_cover.put("coverItem_url", cover.getCoverURL());
                        values_cover.put("items_id", item.getItemsId());
                        result = db.insert("CoverItems", null, values_cover);
                    }
                }

                if (item.getBusinessModel() != null){
                   ContentValues values_business = new ContentValues();
                   values_business.put("business_id", item.getBusinessModel().getBusinessId());
                   values_business.put("business_nameThai", item.getBusinessModel().getNameTH());
                   values_business.put("business_nameEnglish", item.getBusinessModel().getNameEN());
                   values_business.put("business_nameChinese", item.getBusinessModel().getNameZH());
                   values_business.put("business_nameLaos", item.getBusinessModel().getNameLO());
                   values_business.put("business_presentAddress", item.getBusinessModel().getPresentAddress());
                   values_business.put("business_license_paths", item.getBusinessModel().getLicensePaths());
                   values_business.put("business_operator_number", item.getBusinessModel().getOperatorNumber());
                   values_business.put("business_phone", item.getBusinessModel().getPhone());
                   values_business.put("business_www", item.getBusinessModel().getWeb());
                   values_business.put("business_facebook", item.getBusinessModel().getFacebook());
                   values_business.put("business_line", item.getBusinessModel().getLine());
                   values_business.put("items_id", item.getItemsId());
                   result = db.insert("Business", null, values_business);
                }

                if (item.getBookingConditionModelArrayList() != null && item.getBookingConditionModelArrayList().size() != 0){
                    for (BookingConditionModel condition : item.getBookingConditionModelArrayList()){
                        ContentValues values_condition = new ContentValues();
                        values_condition.put("condition_id", condition.getConditionId());
                        values_condition.put("conditionCol_detailThai", condition.getConditionDetailTH());
                        values_condition.put("conditionCol_detailEnglish", condition.getConditionDetailEN());
                        values_condition.put("conditionCol_detailChinese", condition.getConditionDetailZH());
                        values_condition.put("conditionCol_detailLaos", condition.getConditionDetailLO());
                        values_condition.put("conditionCategory_topicThai", condition.getConditionTopicTH());
                        values_condition.put("conditionCategory_topicEnglish", condition.getConditionTopicEN());
                        values_condition.put("conditionCategory_topicChinese", condition.getConditionTopicZH());
                        values_condition.put("conditionCategory_topicLaos", condition.getConditionTopicLO());
                        values_condition.put("items_id", item.getItemsId());
                        result = db.insert("BookingCondition", null, values_condition);
                    }
                }

                result = db.insert("Items", null, values_items);
            }

            db.close(); // Closing database connection

        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    public boolean addCarRental(ArrayList<ItemsModel> itemsModelArrayList) {
        long result = -1;
        try {
            SQLiteDatabase db = database.getWritableDatabase();
            for (ItemsModel item : itemsModelArrayList) {
                ContentValues values_items = new ContentValues();
                values_items.put("items_id",item.getItemsId());
                values_items.put("items_topicThai", item.getTopicTH());
                values_items.put("items_topicEnglish", item.getTopicEN());
                values_items.put("items_topicChinese", item.getTopicZH());
                values_items.put("items_topicLaos", item.getTopicLO());
                values_items.put("items_latitude", item.getLatitude());
                values_items.put("items_longitude", item.getLongitude());
                values_items.put("items_contactThai", item.getContactTH());
                values_items.put("items_contactEnglish", item.getContactEN());
                values_items.put("items_contactChinese", item.getContactZH());
                values_items.put("items_contactLaos", item.getContactLO());
                values_items.put("items_phone", item.getPhone());
                values_items.put("items_email", item.getEmail());
                values_items.put("items_line", item.getLine());
                values_items.put("items_facebookPage", item.getFacebookPage());
                values_items.put("items_www", "");
                values_items.put("item_ar_url", item.getArURL());
                values_items.put("items_highlightsThai", item.getHighlightsTH());
                values_items.put("items_highlightsEnglish", item.getHighlightsEN());
                values_items.put("items_highlightsChinese", item.getHighlightsZH());
                values_items.put("items_highlightsLaos", item.getHighlightsLO());
                values_items.put("items_locationInformationThai", item.getLocationInformationTH());
                values_items.put("items_locationInformationEnglish", item.getLocationInformationEN());
                values_items.put("items_locationInformationChinese", item.getLocationInformationZH());
                values_items.put("items_locationInformationLaos", item.getLocationInformationLO());
                values_items.put("priceGuestAdult", item.getPriceGuestAdult());
                values_items.put("priceGuestChild", item.getPriceGuestChild());
                values_items.put("coverItem_id", item.getCoverItemsModel().getCoverId());
                values_items.put("coverItem_paths", item.getCoverItemsModel().getCoverPaths());
                values_items.put("coverItem_url", item.getCoverItemsModel().getCoverURL());
                values_items.put("menuItem_id", item.getMenuItemModel().getMenuItemId());
                values_items.put("menuItem_thai", item.getMenuItemModel().getMenuItemTH());
                values_items.put("menuItem_english", item.getMenuItemModel().getMenuItemEN());
                values_items.put("menuItem_chinese", item.getMenuItemModel().getMenuItemZH());
                values_items.put("menuItem_laos", item.getMenuItemModel().getMenuItemLO());

                if (item.getCarModel() != null){
                    ContentValues values_car = new ContentValues();
                    values_car.put("items_id", item.getItemsId());
                    values_car.put("carSeats", item.getCarModel().getSeats());
                    values_car.put("pricePerDay", item.getCarModel().getPricePerDay());
                    values_car.put("returnPrice", item.getCarModel().getReturnPrice());
                    values_car.put("pickUpPrice", item.getCarModel().getPickUpPrice());
                    values_car.put("depositPrice", item.getCarModel().getDepositPrice());
                    values_car.put("isBasicInsurance", item.getCarModel().isBasicInsurance());
                    values_car.put("basicInsuranceTH", item.getCarModel().getBasicInsuranceTH());
                    values_car.put("basicInsuranceEN", item.getCarModel().getBasicInsuranceEN());
                    values_car.put("basicInsuranceZH", item.getCarModel().getBasicInsuranceZH());
                    values_car.put("basicInsuranceLO", item.getCarModel().getBasicInsuranceLO());
                    values_car.put("typeOfCarTH", item.getCarModel().getTypeTH());
                    values_car.put("typeOfCarEN", item.getCarModel().getTypeEN());
                    values_car.put("typeOfCarZH", item.getCarModel().getTypeZH());
                    values_car.put("typeOfCarLO", item.getCarModel().getTypeLO());
                    values_car.put("gearSystemTH", item.getCarModel().getGearSystemTH());
                    values_car.put("gearSystemEN", item.getCarModel().getGearSystemEN());
                    values_car.put("gearSystemZH", item.getCarModel().getGearSystemZH());
                    values_car.put("gearSystemLO", item.getCarModel().getGearSystemZH());
                    result = db.insert("Car", null, values_car);
                }

                if (item.getBookingConditionModelArrayList() != null && item.getBookingConditionModelArrayList().size() != 0){
                    for (BookingConditionModel condition : item.getBookingConditionModelArrayList()){
                        ContentValues values_condition = new ContentValues();
                        values_condition.put("condition_id", condition.getConditionId());
                        values_condition.put("conditionCategory_id", condition.getConditionCategory_id());
                        values_condition.put("conditionCol_detailThai", condition.getConditionDetailTH());
                        values_condition.put("conditionCol_detailEnglish", condition.getConditionDetailEN());
                        values_condition.put("conditionCol_detailChinese", condition.getConditionDetailZH());
                        values_condition.put("conditionCol_detailLaos", condition.getConditionDetailLO());
                        values_condition.put("conditionCategory_topicThai", condition.getConditionTopicTH());
                        values_condition.put("conditionCategory_topicEnglish", condition.getConditionTopicEN());
                        values_condition.put("conditionCategory_topicChinese", condition.getConditionTopicZH());
                        values_condition.put("conditionCategory_topicLaos", condition.getConditionTopicLO());
                        values_condition.put("items_id", item.getItemsId());
                        result = db.insert("BookingCondition", null, values_condition);
                    }
                }

                if (item.getBusinessModel() != null){
                    ContentValues values_business = new ContentValues();
                    values_business.put("business_id", item.getBusinessModel().getBusinessId());
                    values_business.put("business_nameThai", item.getBusinessModel().getNameTH());
                    values_business.put("business_nameEnglish", item.getBusinessModel().getNameEN());
                    values_business.put("business_nameChinese", item.getBusinessModel().getNameZH());
                    values_business.put("business_nameLaos", item.getBusinessModel().getNameLO());
                    values_business.put("business_presentAddress", item.getBusinessModel().getPresentAddress());
                    values_business.put("business_license_paths", item.getBusinessModel().getLicensePaths());
                    values_business.put("business_operator_number", item.getBusinessModel().getOperatorNumber());
                    values_business.put("business_phone", item.getBusinessModel().getPhone());
                    values_business.put("business_www", item.getBusinessModel().getWeb());
                    values_business.put("business_facebook", item.getBusinessModel().getFacebook());
                    values_business.put("business_line", item.getBusinessModel().getLine());
                    values_business.put("items_id", item.getItemsId());
                    result = db.insert("Business", null, values_business);
                }



                result = db.insert("Items", null, values_items);
            }

            db.close(); // Closing database connection

        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    //Select
    public ArrayList<ProgramTourModel> getListProgramTours() {
        ArrayList<ProgramTourModel> list = new ArrayList<ProgramTourModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from ProgramTour", null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                ProgramTourModel program = new ProgramTourModel();
                program.setProgramTourId(Integer.parseInt(cursor.getString(0)));
                program.setProgramTourNameTH(cursor.getString(1));
                program.setProgramTourNameEN(cursor.getString(2));
                program.setProgramTourNameZH(cursor.getString(3));
                program.setProgramTourNameLO(cursor.getString(4));
                program.setInterestingThingsTH(cursor.getString(5));
                program.setInterestingThingsEN(cursor.getString(6));
                program.setInterestingThingsZH(cursor.getString(7));
                program.setInterestingThingsLO(cursor.getString(8));
                program.setCountView(cursor.getString(9));
                LikesModel likesModel = new LikesModel();
                likesModel.setLikes(cursor.getInt(10));
                likesModel.setLikeState(Boolean.valueOf(cursor.getString(11)));
                program.setLikesModel(likesModel);
                MenuItemModel menuItemModel = new MenuItemModel();
                menuItemModel.setMenuItemId(cursor.getInt(12));
                menuItemModel.setMenuItemTH(cursor.getString(13));
                menuItemModel.setMenuItemEN(cursor.getString(14));
                menuItemModel.setMenuItemZH(cursor.getString(15));
                menuItemModel.setMenuItemLO(cursor.getString(16));
                program.setMenuItemModel(menuItemModel);
                CategoryModel categoryModel = new CategoryModel();
                categoryModel.setCategoryId(cursor.getInt(17));
                categoryModel.setCategoryTH(cursor.getString(18));
                categoryModel.setCategoryEN(cursor.getString(19));
                categoryModel.setCategoryZH(cursor.getString(20));
                categoryModel.setCategoryLO(cursor.getString(21));
                program.setCategoryModel(categoryModel);

                SubCategoryModel subCategoryModel = new SubCategoryModel();
                subCategoryModel.setCategoryId(cursor.getInt(22));
                subCategoryModel.setCategoryTH(cursor.getString(23));
                subCategoryModel.setCategoryEN(cursor.getString(24));
                subCategoryModel.setCategoryLO(cursor.getString(25));
                subCategoryModel.setCategoryZH(cursor.getString(26));
                program.setSubCategoryModel(subCategoryModel);

                CoverItemsModel coverItemsModel = new CoverItemsModel();
                coverItemsModel.setCoverId(cursor.getInt(27));
                coverItemsModel.setCoverPaths(cursor.getString(28));
                coverItemsModel.setCoverURL(cursor.getString(29));
                program.setCoverItemsModel(coverItemsModel);

                program.setCreatedDTTM(cursor.getString(30));
                program.setUpdatedDTTM(cursor.getString(31));

                UserModel userModel = new UserModel();
                userModel.setUserId(cursor.getInt(32));
                userModel.setFirstName(cursor.getString(33));
                userModel.setLastName(cursor.getString(34));
                userModel.setProfilePicUrl(cursor.getString(35));
                program.setOfficial(Boolean.valueOf(cursor.getString(36)));
                program.setUserModel(userModel);


                list.add(program);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsModel> getItems() {
        ArrayList<ItemsModel> list = new ArrayList<ItemsModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items", null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                ItemsModel item = new ItemsModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));


                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsModel> getListPackageTours() {
        ArrayList<ItemsModel> list = new ArrayList<ItemsModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items where menuItem_id = 9", null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                ItemsModel item = new ItemsModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);
                CategoryModel category = new CategoryModel();
                category.setCategoryId(cursor.getInt(22));
                category.setCategoryTH(cursor.getString(23));
                category.setCategoryEN(cursor.getString(24));
                category.setCategoryZH(cursor.getString(25));
                category.setCategoryLO(cursor.getString(26));
                item.setCategoryModel(category);
                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                ArrayList<CoverItemsModel> coverItemsModelsArrayList = new ArrayList<>();
                SQLiteDatabase dbCover = database.getWritableDatabase();
                Cursor cursor_cover = dbCover.rawQuery("SELECT * from CoverItems where items_id = "+ item.getItemsId(), null);
                if (cursor_cover != null && cursor_cover.getCount() != 0) {
                    cursor_cover.moveToFirst();
                    do {
                        CoverItemsModel coverItemsModels = new CoverItemsModel();
                        coverItemsModels.setCoverId(cursor_cover.getInt(0));
                        coverItemsModels.setCoverPaths(cursor_cover.getString(1));
                        if (cursor_cover.getString(2) != null && cursor_cover.getString(2) != "" && !cursor_cover.getString(2).equals("")) {
                            coverItemsModels.setCoverURL(cursor_cover.getString(2));
                        } else {
                            coverItemsModels.setCoverURL(null);
                        }
                        coverItemsModelsArrayList.add(coverItemsModels);
                    }while (cursor_cover.moveToNext());
                }dbCover.close();
                item.setCoverItemsModelArrayList(coverItemsModelsArrayList);

                ArrayList<TravelDetailsModel> travelDetailsModelArrayList = new ArrayList<>();
                ArrayList<HotelAccommodationModel> hotelAccommodationModelsArrayList = new ArrayList<>();
                ArrayList<FoodModel> foodModelArrayList = new ArrayList<>();
                SQLiteDatabase dbDetails = database.getWritableDatabase();
                Cursor cursor_detail = dbDetails.rawQuery("Select * from TravelDetails where items_id =" + item.getItemsId(), null);
                if (cursor_detail != null && cursor_detail.getCount() != 0){
                    cursor_detail.moveToFirst();
                    do {
                        TravelDetailsModel travelDetailsModel = new TravelDetailsModel();
                        travelDetailsModel.setDetailsId(cursor_detail.getInt(0));
                        travelDetailsModel.setDetailsDateTH(cursor_detail.getString(1));
                        travelDetailsModel.setDetailsDateEN(cursor_detail.getString(2));
                        travelDetailsModel.setDetailsDateZH(cursor_detail.getString(3));
                        travelDetailsModel.setDetailsDateLO(cursor_detail.getString(4));
                        travelDetailsModel.setDetailsTextTH(cursor_detail.getString(5));
                        travelDetailsModel.setDetailsTextEN(cursor_detail.getString(6));
                        travelDetailsModel.setDetailsTextZH(cursor_detail.getString(7));
                        travelDetailsModel.setDetailsTextLO(cursor_detail.getString(8));
                        travelDetailsModelArrayList.add(travelDetailsModel);

                        HotelAccommodationModel hotelModel = new HotelAccommodationModel();
                        hotelModel.setAccommodationTH(cursor_detail.getString(9));
                        hotelModel.setAccommodationEN(cursor_detail.getString(10));
                        hotelModel.setAccommodationZH(cursor_detail.getString(11));
                        hotelModel.setAccommodationLO(cursor_detail.getString(12));
                        hotelAccommodationModelsArrayList.add(hotelModel);

                        FoodModel foodModel = new FoodModel();
                        if (cursor_detail.getString(13).equalsIgnoreCase("1")){
                            foodModel.setFoodBreakFast(true);
                        }else {
                            foodModel.setFoodBreakFast(false);
                        }

                        if (cursor_detail.getString(14).equalsIgnoreCase("1")){
                            foodModel.setFoodLunch(true);
                        }else {
                            foodModel.setFoodLunch(false);
                        }

                        if (cursor_detail.getString(15).equalsIgnoreCase("1")){
                            foodModel.setFoodDinner(true);
                        }else {
                            foodModel.setFoodDinner(false);
                        }
                        foodModelArrayList.add(foodModel);

                    }while (cursor_detail.moveToNext());
                }dbDetails.close();
                item.setTravelDetailsModelArrayList(travelDetailsModelArrayList);
                item.setHotelAccommodationModelArrayList(hotelAccommodationModelsArrayList);
                item.setFoodModelArrayList(foodModelArrayList);

                ArrayList<TravelPeriodModel> travelPeriodModelArrayList = new ArrayList<>();
                SQLiteDatabase dbPeriod = database.getWritableDatabase();
                Cursor cursor_period = dbPeriod.rawQuery("Select * from TravelPeriod where items_id =" + item.getItemsId(), null);
                if (cursor_period != null && cursor_period.getCount() != 0){
                    cursor_period.moveToFirst();
                    do {
                        TravelPeriodModel periodModel = new TravelPeriodModel();
                        periodModel.setPeriodId(cursor_period.getInt(0));
                        periodModel.setAmount(cursor_period.getInt(1));
                        periodModel.setPeriodStart(cursor_period.getString(2));
                        periodModel.setPeriodEnd(cursor_period.getString(3));
                        periodModel.setAdultPrice(cursor_period.getInt(4));
                        periodModel.setAdultSpecialPrice(cursor_period.getInt(5));
                        periodModel.setChildPrice(cursor_period.getInt(6));
                        periodModel.setChildSpecialPrice(cursor_period.getInt(7));
                    }while (cursor_period.moveToNext());
                }dbPeriod.close();
                item.setTravelPeriodModelArrayList(travelPeriodModelArrayList);

                ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                SQLiteDatabase dbCondition = database.getWritableDatabase();
                Cursor cursor_condition = dbCondition.rawQuery("Select * from BookingCondition where items_id =" + item.getItemsId(), null);
                if (cursor_condition != null && cursor_condition.getCount() != 0){
                    cursor_condition.moveToFirst();
                    do {
                        BookingConditionModel conditionModel = new BookingConditionModel();
                        conditionModel.setConditionId(cursor_condition.getInt(0));
                        conditionModel.setConditionDetailTH(cursor_condition.getString(1));
                        conditionModel.setConditionDetailEN(cursor_condition.getString(2));
                        conditionModel.setConditionDetailZH(cursor_condition.getString(3));
                        conditionModel.setConditionDetailLO(cursor_condition.getString(4));
                        conditionModel.setConditionTopicTH(cursor_condition.getString(5));
                        conditionModel.setConditionTopicEN(cursor_condition.getString(6));
                        conditionModel.setConditionTopicZH(cursor_condition.getString(7));
                        conditionModel.setConditionTopicLO(cursor_condition.getString(8));
                        bookingConditionModelArrayList.add(conditionModel);
                    }while (cursor_condition.moveToNext());
                }dbCondition.close();
                item.setBookingConditionModelArrayList(bookingConditionModelArrayList);

                BusinessModel business = new BusinessModel();
                SQLiteDatabase dbBusiness = database.getWritableDatabase();
                Cursor cursor_business = dbBusiness.rawQuery("Select * from Business where items_id =" + item.getItemsId(), null);
                if (cursor_business != null && cursor_business.getCount() != 0){
                    cursor_business.moveToFirst();
                    do {
                        business.setBusinessId(cursor_business.getInt(0));
                        business.setNameTH(cursor_business.getString(1));
                        business.setNameEN(cursor_business.getString(2));
                        business.setNameZH(cursor_business.getString(3));
                        business.setNameLO(cursor_business.getString(4));
                        business.setPresentAddress(cursor_business.getString(5));
                        business.setLicensePaths(cursor_business.getString(6));
                        business.setOperatorNumber(cursor_business.getString(7));
                        business.setPhone(cursor_business.getString(8));
                        business.setWeb(cursor_business.getString(9));
                        business.setFacebook(cursor_business.getString(10));
                        business.setLine(cursor_business.getString(11));
                    }while (cursor_business.moveToNext());
                }cursor_business.close();
                item.setBusinessModel(business);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsModel> getListTicketEvent() {
        ArrayList<ItemsModel> list = new ArrayList<ItemsModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items where menuItem_id = 10", null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                ItemsModel item = new ItemsModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setTimeOpen(cursor.getString(5));
                item.setTimeClose(cursor.getString(6));
                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);
                CategoryModel category = new CategoryModel();
                category.setCategoryId(cursor.getInt(22));
                category.setCategoryTH(cursor.getString(23));
                category.setCategoryEN(cursor.getString(24));
                category.setCategoryZH(cursor.getString(25));
                category.setCategoryLO(cursor.getString(26));
                item.setCategoryModel(category);
                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                ArrayList<ItemsDetailModel> detailModelArrayList = new ArrayList<>();
                SQLiteDatabase dbDetail = database.getWritableDatabase();
                Cursor cursor_detail = dbDetail.rawQuery("Select * from Detail Where items_id =" + item.getItemsId(), null);
                if (cursor_detail != null && cursor_detail.getCount() != 0) {
                    cursor_detail.moveToFirst();
                    do {
                        ItemsDetailModel itemsDetail = new ItemsDetailModel();
                        ItemsTopicDetailModel topicDetail = new ItemsTopicDetailModel();
                        itemsDetail.setDetailId(cursor_detail.getInt(0));
                        itemsDetail.setDetailTH(cursor_detail.getString(1));
                        itemsDetail.setDetailEN(cursor_detail.getString(2));
                        itemsDetail.setDetailLO(cursor_detail.getString(3));
                        itemsDetail.setDetailZH(cursor_detail.getString(4));
                        topicDetail.setTopicDetailId(cursor_detail.getInt(5));
                        topicDetail.setTopicDetailTH(cursor_detail.getString(6));
                        topicDetail.setTopicDetailEN(cursor_detail.getString(7));
                        topicDetail.setTopicDetailLO(cursor_detail.getString(8));
                        topicDetail.setTopicDetailZH(cursor_detail.getString(9));
                        itemsDetail.setItemsTopicDetailModel(topicDetail);

                        ArrayList<ItemsPhotoDetailModel> photoDetailModelArrayList = new ArrayList<>();
                        SQLiteDatabase dbPhoto = database.getWritableDatabase();
                        Cursor cursor_photo = dbPhoto.rawQuery("Select * from PhotoDetail Where detail_id =" + itemsDetail.getDetailId(), null);
                        if (cursor_photo != null && cursor_photo.getCount() != 0) {
                            cursor_photo.moveToFirst();
                            do {
                                ItemsPhotoDetailModel photoDetail = new ItemsPhotoDetailModel();
                                photoDetail.setPhotoId(cursor_photo.getInt(0));
                                photoDetail.setPhotoPaths(cursor_photo.getString(1));
                                photoDetail.setPhotoTextTH(cursor_photo.getString(2));
                                photoDetail.setPhotoTextEN(cursor_photo.getString(3));
                                photoDetail.setPhotoTextZH(cursor_photo.getString(4));
                                photoDetail.setPhotoTextLO(cursor_photo.getString(5));
                                photoDetailModelArrayList.add(photoDetail);
                            } while (cursor_photo.moveToNext());

                        }dbPhoto.close();
                        itemsDetail.setItemsPhotoDetailModels(photoDetailModelArrayList);

                        detailModelArrayList.add(itemsDetail);
                    } while (cursor_detail.moveToNext());

                }dbDetail.close();
                item.setItemsDetailModelArrayList(detailModelArrayList);

                ArrayList<CoverItemsModel> coverItemsModelsArrayList = new ArrayList<>();
                SQLiteDatabase dbCover = database.getWritableDatabase();
                Cursor cursor_cover = dbCover.rawQuery("SELECT * from CoverItems where items_id = "+ item.getItemsId(), null);
                if (cursor_cover != null && cursor_cover.getCount() != 0) {
                    cursor_cover.moveToFirst();
                    do {
                        CoverItemsModel coverItemsModels = new CoverItemsModel();
                        coverItemsModels.setCoverId(cursor_cover.getInt(0));
                        coverItemsModels.setCoverPaths(cursor_cover.getString(1));
                        if (cursor_cover.getString(2) != null && cursor_cover.getString(2) != "" && !cursor_cover.getString(2).equals("")) {
                            coverItemsModels.setCoverURL(cursor_cover.getString(2));
                        } else {
                            coverItemsModels.setCoverURL(null);
                        }
                        coverItemsModelsArrayList.add(coverItemsModels);
                    }while (cursor_cover.moveToNext());
                }dbCover.close();
                item.setCoverItemsModelArrayList(coverItemsModelsArrayList);

                ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                SQLiteDatabase dbCondition = database.getWritableDatabase();
                Cursor cursor_condition = dbCondition.rawQuery("Select * from BookingCondition where items_id =" + item.getItemsId(), null);
                if (cursor_condition != null && cursor_condition.getCount() != 0){
                    cursor_condition.moveToFirst();
                    do {
                        BookingConditionModel conditionModel = new BookingConditionModel();
                        conditionModel.setConditionId(cursor_condition.getInt(0));
                        conditionModel.setConditionDetailTH(cursor_condition.getString(1));
                        conditionModel.setConditionDetailEN(cursor_condition.getString(2));
                        conditionModel.setConditionDetailZH(cursor_condition.getString(3));
                        conditionModel.setConditionDetailLO(cursor_condition.getString(4));
                        conditionModel.setConditionTopicTH(cursor_condition.getString(5));
                        conditionModel.setConditionTopicEN(cursor_condition.getString(6));
                        conditionModel.setConditionTopicZH(cursor_condition.getString(7));
                        conditionModel.setConditionTopicLO(cursor_condition.getString(8));
                        bookingConditionModelArrayList.add(conditionModel);
                    }while (cursor_condition.moveToNext());
                }dbCondition.close();
                item.setBookingConditionModelArrayList(bookingConditionModelArrayList);
                
                BusinessModel business = new BusinessModel();
                SQLiteDatabase dbBusiness = database.getWritableDatabase();
                Cursor cursor_business = dbBusiness.rawQuery("Select * from Business where items_id =" + item.getItemsId(), null);
                if (cursor_business != null && cursor_business.getCount() != 0){
                    cursor_business.moveToFirst();
                    do {
                        business.setBusinessId(cursor_business.getInt(0));
                        business.setNameTH(cursor_business.getString(1));
                        business.setNameEN(cursor_business.getString(2));
                        business.setNameZH(cursor_business.getString(3));
                        business.setNameLO(cursor_business.getString(4));
                        business.setPresentAddress(cursor_business.getString(5));
                        business.setLicensePaths(cursor_business.getString(6));
                        business.setOperatorNumber(cursor_business.getString(7));
                        business.setPhone(cursor_business.getString(8));
                        business.setWeb(cursor_business.getString(9));
                        business.setFacebook(cursor_business.getString(10));
                        business.setLine(cursor_business.getString(11));
                    }while (cursor_business.moveToNext());
                }cursor_business.close();
                item.setBusinessModel(business);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsModel> getListCarRental() {
        ArrayList<ItemsModel> list = new ArrayList<ItemsModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items where menuItem_id = 11 ", null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                ItemsModel item = new ItemsModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);

                CarModel carModel = new CarModel();
                SQLiteDatabase dbCar = database.getWritableDatabase();
                Cursor cursor_car = dbCar.rawQuery("Select * from Car where items_id = " + item.getItemsId(), null);
                if (cursor_car != null && cursor_car.getCount() != 0){
                    cursor_car.moveToFirst();
                    do {
                        carModel.setSeats(cursor_car.getInt(1));
                        carModel.setPricePerDay(cursor_car.getInt(2));
                        carModel.setReturnPrice(cursor_car.getInt(3));
                        carModel.setPickUpPrice(cursor_car.getInt(4));
                        carModel.setDepositPrice(cursor_car.getInt(5));
                        if (cursor_car.getString(6).equalsIgnoreCase("1")){
                            carModel.setBasicInsurance(true);
                        }else {
                            carModel.setBasicInsurance(false);
                        }
                        carModel.setBasicInsuranceTH(cursor_car.getString(7));
                        carModel.setBasicInsuranceEN(cursor_car.getString(8));
                        carModel.setBasicInsuranceZH(cursor_car.getString(9));
                        carModel.setBasicInsuranceLO(cursor_car.getString(10));
                        carModel.setTypeTH(cursor_car.getString(11));
                        carModel.setTypeEN(cursor_car.getString(12));
                        carModel.setTypeZH(cursor_car.getString(13));
                        carModel.setTypeLO(cursor_car.getString(14));
                        carModel.setGearSystemTH(cursor_car.getString(15));
                        carModel.setGearSystemEN(cursor_car.getString(16));
                        carModel.setGearSystemZH(cursor_car.getString(17));
                        carModel.setGearSystemLO(cursor_car.getString(18));
                    }while (cursor_car.moveToNext());
                }dbCar.close();
                item.setCarModel(carModel);

                ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                SQLiteDatabase dbCondition = database.getWritableDatabase();
                Cursor cursor_condition = dbCondition.rawQuery("Select * from BookingCondition where items_id =" + item.getItemsId(), null);
                if (cursor_condition != null && cursor_condition.getCount() != 0){
                    cursor_condition.moveToFirst();
                    do {
                        BookingConditionModel conditionModel = new BookingConditionModel();
                        conditionModel.setConditionId(cursor_condition.getInt(0));
                        conditionModel.setConditionDetailTH(cursor_condition.getString(1));
                        conditionModel.setConditionDetailEN(cursor_condition.getString(2));
                        conditionModel.setConditionDetailZH(cursor_condition.getString(3));
                        conditionModel.setConditionDetailLO(cursor_condition.getString(4));
                        conditionModel.setConditionTopicTH(cursor_condition.getString(5));
                        conditionModel.setConditionTopicEN(cursor_condition.getString(6));
                        conditionModel.setConditionTopicZH(cursor_condition.getString(7));
                        conditionModel.setConditionTopicLO(cursor_condition.getString(8));
                        bookingConditionModelArrayList.add(conditionModel);
                    }while (cursor_condition.moveToNext());
                }dbCondition.close();
                item.setBookingConditionModelArrayList(bookingConditionModelArrayList);

                BusinessModel business = new BusinessModel();
                SQLiteDatabase dbBusiness = database.getWritableDatabase();
                Cursor cursor_business = dbBusiness.rawQuery("Select * from Business where items_id =" + item.getItemsId(), null);
                if (cursor_business != null && cursor_business.getCount() != 0){
                    cursor_business.moveToFirst();
                    do {
                        business.setBusinessId(cursor_business.getInt(0));
                        business.setNameTH(cursor_business.getString(1));
                        business.setNameEN(cursor_business.getString(2));
                        business.setNameZH(cursor_business.getString(3));
                        business.setNameLO(cursor_business.getString(4));
                        business.setPresentAddress(cursor_business.getString(5));
                        business.setLicensePaths(cursor_business.getString(6));
                        business.setOperatorNumber(cursor_business.getString(7));
                        business.setPhone(cursor_business.getString(8));
                        business.setWeb(cursor_business.getString(9));
                        business.setFacebook(cursor_business.getString(10));
                        business.setLine(cursor_business.getString(11));
                    }while (cursor_business.moveToNext());
                }cursor_business.close();
                item.setBusinessModel(business);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ItemsModel getItemsCarRentalById(int itemsID) {
        ItemsModel item = new ItemsModel();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items where menuItem_id = 11 and items_id = " + itemsID, null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);

                CarModel carModel = new CarModel();
                SQLiteDatabase dbCar = database.getWritableDatabase();
                Cursor cursor_car = dbCar.rawQuery("Select * from Car where items_id = " + item.getItemsId(), null);
                if (cursor_car != null && cursor_car.getCount() != 0){
                    cursor_car.moveToFirst();
                    do {
                        carModel.setSeats(cursor_car.getInt(1));
                        carModel.setPricePerDay(cursor_car.getInt(2));
                        carModel.setReturnPrice(cursor_car.getInt(3));
                        carModel.setPickUpPrice(cursor_car.getInt(4));
                        carModel.setDepositPrice(cursor_car.getInt(5));
                        if (cursor_car.getString(6).equalsIgnoreCase("1")){
                            carModel.setBasicInsurance(true);
                        }else {
                            carModel.setBasicInsurance(false);
                        }
                        carModel.setBasicInsuranceTH(cursor_car.getString(7));
                        carModel.setBasicInsuranceEN(cursor_car.getString(8));
                        carModel.setBasicInsuranceZH(cursor_car.getString(9));
                        carModel.setBasicInsuranceLO(cursor_car.getString(10));
                        carModel.setTypeTH(cursor_car.getString(11));
                        carModel.setTypeEN(cursor_car.getString(12));
                        carModel.setTypeZH(cursor_car.getString(13));
                        carModel.setTypeLO(cursor_car.getString(14));
                        carModel.setGearSystemTH(cursor_car.getString(15));
                        carModel.setGearSystemEN(cursor_car.getString(16));
                        carModel.setGearSystemZH(cursor_car.getString(17));
                        carModel.setGearSystemLO(cursor_car.getString(18));
                    }while (cursor_car.moveToNext());
                }dbCar.close();
                item.setCarModel(carModel);

                ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                SQLiteDatabase dbCondition = database.getWritableDatabase();
                Cursor cursor_condition = dbCondition.rawQuery("Select * from BookingCondition where items_id =" + item.getItemsId(), null);
                if (cursor_condition != null && cursor_condition.getCount() != 0){
                    cursor_condition.moveToFirst();
                    do {
                        BookingConditionModel conditionModel = new BookingConditionModel();
                        conditionModel.setConditionId(cursor_condition.getInt(10));
                        conditionModel.setConditionDetailTH(cursor_condition.getString(1));
                        conditionModel.setConditionDetailEN(cursor_condition.getString(2));
                        conditionModel.setConditionDetailZH(cursor_condition.getString(3));
                        conditionModel.setConditionDetailLO(cursor_condition.getString(4));
                        conditionModel.setConditionTopicTH(cursor_condition.getString(5));
                        conditionModel.setConditionTopicEN(cursor_condition.getString(6));
                        conditionModel.setConditionTopicZH(cursor_condition.getString(7));
                        conditionModel.setConditionTopicLO(cursor_condition.getString(8));
                        bookingConditionModelArrayList.add(conditionModel);
                    }while (cursor_condition.moveToNext());
                }dbCondition.close();
                item.setBookingConditionModelArrayList(bookingConditionModelArrayList);

                BusinessModel business = new BusinessModel();
                SQLiteDatabase dbBusiness = database.getWritableDatabase();
                Cursor cursor_business = dbBusiness.rawQuery("Select * from Business where items_id =" + item.getItemsId(), null);
                if (cursor_business != null && cursor_business.getCount() != 0){
                    cursor_business.moveToFirst();
                    do {
                        business.setBusinessId(cursor_business.getInt(0));
                        business.setNameTH(cursor_business.getString(1));
                        business.setNameEN(cursor_business.getString(2));
                        business.setNameZH(cursor_business.getString(3));
                        business.setNameLO(cursor_business.getString(4));
                        business.setPresentAddress(cursor_business.getString(5));
                        business.setLicensePaths(cursor_business.getString(6));
                        business.setOperatorNumber(cursor_business.getString(7));
                        business.setPhone(cursor_business.getString(8));
                        business.setWeb(cursor_business.getString(9));
                        business.setFacebook(cursor_business.getString(10));
                        business.setLine(cursor_business.getString(11));
                    }while (cursor_business.moveToNext());
                }cursor_business.close();
                item.setBusinessModel(business);


            }while (cursor.moveToNext());
        }
        db.close();
        return item;
    }

    public ItemsModel getItemsTicketEventById(int itemsID) {
        ItemsModel item = new ItemsModel();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items where menuItem_id = 10 and items_id = " + itemsID, null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setTimeOpen(cursor.getString(5));
                item.setTimeClose(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));
                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);
                CategoryModel category = new CategoryModel();
                category.setCategoryId(cursor.getInt(22));
                category.setCategoryTH(cursor.getString(23));
                category.setCategoryEN(cursor.getString(24));
                category.setCategoryZH(cursor.getString(25));
                category.setCategoryLO(cursor.getString(26));
                item.setCategoryModel(category);
                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                ArrayList<ItemsDetailModel> detailModelArrayList = new ArrayList<>();
                SQLiteDatabase dbDetail = database.getWritableDatabase();
                Cursor cursor_detail = dbDetail.rawQuery("Select * from Detail Where items_id =" + item.getItemsId(), null);
                if (cursor_detail != null && cursor_detail.getCount() != 0) {
                    cursor_detail.moveToFirst();
                    do {
                        ItemsDetailModel itemsDetail = new ItemsDetailModel();
                        ItemsTopicDetailModel topicDetail = new ItemsTopicDetailModel();
                        itemsDetail.setDetailId(cursor_detail.getInt(0));
                        itemsDetail.setDetailTH(cursor_detail.getString(1));
                        itemsDetail.setDetailEN(cursor_detail.getString(2));
                        itemsDetail.setDetailLO(cursor_detail.getString(3));
                        itemsDetail.setDetailZH(cursor_detail.getString(4));
                        topicDetail.setTopicDetailId(cursor_detail.getInt(5));
                        topicDetail.setTopicDetailTH(cursor_detail.getString(6));
                        topicDetail.setTopicDetailEN(cursor_detail.getString(7));
                        topicDetail.setTopicDetailLO(cursor_detail.getString(8));
                        topicDetail.setTopicDetailZH(cursor_detail.getString(9));
                        itemsDetail.setItemsTopicDetailModel(topicDetail);

                        ArrayList<ItemsPhotoDetailModel> photoDetailModelArrayList = new ArrayList<>();
                        SQLiteDatabase dbPhoto = database.getWritableDatabase();
                        Cursor cursor_photo = dbPhoto.rawQuery("Select * from PhotoDetail Where detail_id =" + itemsDetail.getDetailId(), null);
                        if (cursor_photo != null && cursor_photo.getCount() != 0) {
                            cursor_photo.moveToFirst();
                            do {
                                ItemsPhotoDetailModel photoDetail = new ItemsPhotoDetailModel();
                                photoDetail.setPhotoId(cursor_photo.getInt(0));
                                photoDetail.setPhotoPaths(cursor_photo.getString(1));
                                photoDetail.setPhotoTextTH(cursor_photo.getString(2));
                                photoDetail.setPhotoTextEN(cursor_photo.getString(3));
                                photoDetail.setPhotoTextZH(cursor_photo.getString(4));
                                photoDetail.setPhotoTextLO(cursor_photo.getString(5));
                                photoDetailModelArrayList.add(photoDetail);
                            } while (cursor_photo.moveToNext());

                        }dbPhoto.close();
                        itemsDetail.setItemsPhotoDetailModels(photoDetailModelArrayList);

                        detailModelArrayList.add(itemsDetail);
                    } while (cursor_detail.moveToNext());

                }dbDetail.close();
                item.setItemsDetailModelArrayList(detailModelArrayList);

                ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                SQLiteDatabase dbCondition = database.getWritableDatabase();
                Cursor cursor_condition = dbCondition.rawQuery("Select * from BookingCondition where items_id =" + item.getItemsId(), null);
                if (cursor_condition != null && cursor_condition.getCount() != 0){
                    cursor_condition.moveToFirst();
                    do {
                        BookingConditionModel conditionModel = new BookingConditionModel();
                        conditionModel.setConditionId(cursor_condition.getInt(0));
                        conditionModel.setConditionDetailTH(cursor_condition.getString(1));
                        conditionModel.setConditionDetailEN(cursor_condition.getString(2));
                        conditionModel.setConditionDetailZH(cursor_condition.getString(3));
                        conditionModel.setConditionDetailLO(cursor_condition.getString(4));
                        conditionModel.setConditionTopicTH(cursor_condition.getString(5));
                        conditionModel.setConditionTopicEN(cursor_condition.getString(6));
                        conditionModel.setConditionTopicZH(cursor_condition.getString(7));
                        conditionModel.setConditionTopicLO(cursor_condition.getString(8));
                        bookingConditionModelArrayList.add(conditionModel);
                    }while (cursor_condition.moveToNext());
                }dbCondition.close();
                item.setBookingConditionModelArrayList(bookingConditionModelArrayList);

                ArrayList<CoverItemsModel> coverItemsModelsArrayList = new ArrayList<>();
                SQLiteDatabase dbCover = database.getWritableDatabase();
                Cursor cursor_cover = dbCover.rawQuery("SELECT * from CoverItems where items_id = "+ item.getItemsId(), null);
                if (cursor_cover != null && cursor_cover.getCount() != 0) {
                    cursor_cover.moveToFirst();
                    do {
                        CoverItemsModel coverItemsModels = new CoverItemsModel();
                        coverItemsModels.setCoverId(cursor_cover.getInt(0));
                        coverItemsModels.setCoverPaths(cursor_cover.getString(1));
                        if (cursor_cover.getString(2) != null && cursor_cover.getString(2) != "" && !cursor_cover.getString(2).equals("")) {
                            coverItemsModels.setCoverURL(cursor_cover.getString(2));
                        } else {
                            coverItemsModels.setCoverURL(null);
                        }
                        coverItemsModelsArrayList.add(coverItemsModels);
                    }while (cursor_cover.moveToNext());
                }dbCover.close();
                item.setCoverItemsModelArrayList(coverItemsModelsArrayList);

                BusinessModel business = new BusinessModel();
                SQLiteDatabase dbBusiness = database.getWritableDatabase();
                Cursor cursor_business = dbBusiness.rawQuery("Select * from Business where items_id =" + item.getItemsId(), null);
                if (cursor_business != null && cursor_business.getCount() != 0){
                    cursor_business.moveToFirst();
                    do {
                        business.setBusinessId(cursor_business.getInt(0));
                        business.setNameTH(cursor_business.getString(1));
                        business.setNameEN(cursor_business.getString(2));
                        business.setNameZH(cursor_business.getString(3));
                        business.setNameLO(cursor_business.getString(4));
                        business.setPresentAddress(cursor_business.getString(5));
                        business.setLicensePaths(cursor_business.getString(6));
                        business.setOperatorNumber(cursor_business.getString(7));
                        business.setPhone(cursor_business.getString(8));
                        business.setWeb(cursor_business.getString(9));
                        business.setFacebook(cursor_business.getString(10));
                        business.setLine(cursor_business.getString(11));
                    }while (cursor_business.moveToNext());
                }cursor_business.close();
                item.setBusinessModel(business);

            }while (cursor.moveToNext());
        }
        db.close();
        return item;
    }

    public ItemsModel getItemsPackageToursById(int itemsID) {
        ItemsModel item = new ItemsModel();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items where menuItem_id = 9 and items_id = " + itemsID, null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);
                CategoryModel category = new CategoryModel();
                category.setCategoryId(cursor.getInt(22));
                category.setCategoryTH(cursor.getString(23));
                category.setCategoryEN(cursor.getString(24));
                category.setCategoryZH(cursor.getString(25));
                category.setCategoryLO(cursor.getString(26));
                item.setCategoryModel(category);
                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                ArrayList<CoverItemsModel> coverItemsModelsArrayList = new ArrayList<>();
                SQLiteDatabase dbCover = database.getWritableDatabase();
                Cursor cursor_cover = dbCover.rawQuery("SELECT * from CoverItems where items_id = "+ item.getItemsId(), null);
                if (cursor_cover != null && cursor_cover.getCount() != 0) {
                    cursor_cover.moveToFirst();
                    do {
                        CoverItemsModel coverItemsModels = new CoverItemsModel();
                        coverItemsModels.setCoverId(cursor_cover.getInt(0));
                        coverItemsModels.setCoverPaths(cursor_cover.getString(1));
                        if (cursor_cover.getString(2) != null && cursor_cover.getString(2) != "" && !cursor_cover.getString(2).equals("")) {
                            coverItemsModels.setCoverURL(cursor_cover.getString(2));
                        } else {
                            coverItemsModels.setCoverURL(null);
                        }
                        coverItemsModelsArrayList.add(coverItemsModels);
                    }while (cursor_cover.moveToNext());
                }dbCover.close();
                item.setCoverItemsModelArrayList(coverItemsModelsArrayList);

                ArrayList<TravelDetailsModel> travelDetailsModelArrayList = new ArrayList<>();
                ArrayList<HotelAccommodationModel> hotelAccommodationModelsArrayList = new ArrayList<>();
                ArrayList<FoodModel> foodModelArrayList = new ArrayList<>();
                SQLiteDatabase dbDetails = database.getWritableDatabase();
                Cursor cursor_detail = dbDetails.rawQuery("Select * from TravelDetails where items_id =" + item.getItemsId(), null);
                if (cursor_detail != null && cursor_detail.getCount() != 0){
                    cursor_detail.moveToFirst();
                    do {
                        TravelDetailsModel travelDetailsModel = new TravelDetailsModel();
                        travelDetailsModel.setDetailsId(cursor_detail.getInt(0));
                        travelDetailsModel.setDetailsDateTH(cursor_detail.getString(1));
                        travelDetailsModel.setDetailsDateEN(cursor_detail.getString(2));
                        travelDetailsModel.setDetailsDateZH(cursor_detail.getString(3));
                        travelDetailsModel.setDetailsDateLO(cursor_detail.getString(4));
                        travelDetailsModel.setDetailsTextTH(cursor_detail.getString(5));
                        travelDetailsModel.setDetailsTextEN(cursor_detail.getString(6));
                        travelDetailsModel.setDetailsTextZH(cursor_detail.getString(7));
                        travelDetailsModel.setDetailsTextLO(cursor_detail.getString(8));
                        travelDetailsModelArrayList.add(travelDetailsModel);

                        HotelAccommodationModel hotelModel = new HotelAccommodationModel();
                        hotelModel.setAccommodationTH(cursor_detail.getString(9));
                        hotelModel.setAccommodationEN(cursor_detail.getString(10));
                        hotelModel.setAccommodationZH(cursor_detail.getString(11));
                        hotelModel.setAccommodationLO(cursor_detail.getString(12));
                        hotelAccommodationModelsArrayList.add(hotelModel);

                        FoodModel foodModel = new FoodModel();
                        if (cursor_detail.getString(13).equalsIgnoreCase("1")){
                            foodModel.setFoodBreakFast(true);
                        }else {
                            foodModel.setFoodBreakFast(false);
                        }

                        if (cursor_detail.getString(14).equalsIgnoreCase("1")){
                            foodModel.setFoodLunch(true);
                        }else {
                            foodModel.setFoodLunch(false);
                        }

                        if (cursor_detail.getString(15).equalsIgnoreCase("1")){
                            foodModel.setFoodDinner(true);
                        }else {
                            foodModel.setFoodDinner(false);
                        }
                        foodModelArrayList.add(foodModel);

                    }while (cursor_detail.moveToNext());
                }dbDetails.close();
                item.setTravelDetailsModelArrayList(travelDetailsModelArrayList);
                item.setHotelAccommodationModelArrayList(hotelAccommodationModelsArrayList);
                item.setFoodModelArrayList(foodModelArrayList);

                ArrayList<TravelPeriodModel> travelPeriodModelArrayList = new ArrayList<>();
                SQLiteDatabase dbPeriod = database.getWritableDatabase();
                Cursor cursor_period = dbPeriod.rawQuery("Select * from TravelPeriod where items_id =" + item.getItemsId(), null);
                if (cursor_period != null && cursor_period.getCount() != 0){
                    cursor_period.moveToFirst();
                    do {
                        TravelPeriodModel periodModel = new TravelPeriodModel();
                        periodModel.setPeriodId(cursor_period.getInt(0));
                        periodModel.setAmount(cursor_period.getInt(1));
                        periodModel.setPeriodStart(cursor_period.getString(2));
                        periodModel.setPeriodEnd(cursor_period.getString(3));
                        periodModel.setAdultPrice(cursor_period.getInt(4));
                        periodModel.setAdultSpecialPrice(cursor_period.getInt(5));
                        periodModel.setChildPrice(cursor_period.getInt(6));
                        periodModel.setChildSpecialPrice(cursor_period.getInt(7));
                    }while (cursor_period.moveToNext());
                }dbPeriod.close();
                item.setTravelPeriodModelArrayList(travelPeriodModelArrayList);

                ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                SQLiteDatabase dbCondition = database.getWritableDatabase();
                Cursor cursor_condition = dbCondition.rawQuery("Select * from BookingCondition where items_id =" + item.getItemsId(), null);
                if (cursor_condition != null && cursor_condition.getCount() != 0){
                    cursor_condition.moveToFirst();
                    do {
                        BookingConditionModel conditionModel = new BookingConditionModel();
                        conditionModel.setConditionId(cursor_condition.getInt(0));
                        conditionModel.setConditionDetailTH(cursor_condition.getString(1));
                        conditionModel.setConditionDetailEN(cursor_condition.getString(2));
                        conditionModel.setConditionDetailZH(cursor_condition.getString(3));
                        conditionModel.setConditionDetailLO(cursor_condition.getString(4));
                        conditionModel.setConditionTopicTH(cursor_condition.getString(5));
                        conditionModel.setConditionTopicEN(cursor_condition.getString(6));
                        conditionModel.setConditionTopicZH(cursor_condition.getString(7));
                        conditionModel.setConditionTopicLO(cursor_condition.getString(8));
                        bookingConditionModelArrayList.add(conditionModel);
                    }while (cursor_condition.moveToNext());
                }dbCondition.close();
                item.setBookingConditionModelArrayList(bookingConditionModelArrayList);

                BusinessModel business = new BusinessModel();
                SQLiteDatabase dbBusiness = database.getWritableDatabase();
                Cursor cursor_business = dbBusiness.rawQuery("Select * from Business where items_id =" + item.getItemsId(), null);
                if (cursor_business != null && cursor_business.getCount() != 0){
                    cursor_business.moveToFirst();
                    do {
                        business.setBusinessId(cursor_business.getInt(0));
                        business.setNameTH(cursor_business.getString(1));
                        business.setNameEN(cursor_business.getString(2));
                        business.setNameZH(cursor_business.getString(3));
                        business.setNameLO(cursor_business.getString(4));
                        business.setPresentAddress(cursor_business.getString(5));
                        business.setLicensePaths(cursor_business.getString(6));
                        business.setOperatorNumber(cursor_business.getString(7));
                        business.setPhone(cursor_business.getString(8));
                        business.setWeb(cursor_business.getString(9));
                        business.setFacebook(cursor_business.getString(10));
                        business.setLine(cursor_business.getString(11));
                    }while (cursor_business.moveToNext());
                }cursor_business.close();
                item.setBusinessModel(business);

            }while (cursor.moveToNext());
        }
        db.close();
        return item;
    }

    public ProgramTourModel getItemsProgramToursById(int itemID) {
        ProgramTourModel program = new ProgramTourModel();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from ProgramTour where ProgramTour_id = " + itemID, null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                program.setProgramTourId(Integer.parseInt(cursor.getString(0)));
                program.setProgramTourNameTH(cursor.getString(1));
                program.setProgramTourNameEN(cursor.getString(2));
                program.setProgramTourNameZH(cursor.getString(3));
                program.setProgramTourNameLO(cursor.getString(4));
                program.setInterestingThingsTH(cursor.getString(5));
                program.setInterestingThingsEN(cursor.getString(6));
                program.setInterestingThingsZH(cursor.getString(7));
                program.setInterestingThingsLO(cursor.getString(8));
                program.setCountView(cursor.getString(9));
                LikesModel likesModel = new LikesModel();
                likesModel.setLikes(cursor.getInt(10));
                likesModel.setLikeState(Boolean.valueOf(cursor.getString(11)));
                program.setLikesModel(likesModel);
                MenuItemModel menuItemModel = new MenuItemModel();
                menuItemModel.setMenuItemId(cursor.getInt(12));
                menuItemModel.setMenuItemTH(cursor.getString(13));
                menuItemModel.setMenuItemEN(cursor.getString(14));
                menuItemModel.setMenuItemZH(cursor.getString(15));
                menuItemModel.setMenuItemLO(cursor.getString(16));
                program.setMenuItemModel(menuItemModel);
                CategoryModel categoryModel = new CategoryModel();
                categoryModel.setCategoryId(cursor.getInt(17));
                categoryModel.setCategoryTH(cursor.getString(18));
                categoryModel.setCategoryEN(cursor.getString(19));
                categoryModel.setCategoryZH(cursor.getString(20));
                categoryModel.setCategoryLO(cursor.getString(21));
                program.setCategoryModel(categoryModel);

                SubCategoryModel subCategoryModel = new SubCategoryModel();
                subCategoryModel.setCategoryId(cursor.getInt(22));
                subCategoryModel.setCategoryTH(cursor.getString(23));
                subCategoryModel.setCategoryEN(cursor.getString(24));
                subCategoryModel.setCategoryLO(cursor.getString(25));
                subCategoryModel.setCategoryZH(cursor.getString(26));
                program.setSubCategoryModel(subCategoryModel);

                CoverItemsModel coverItemsModel = new CoverItemsModel();
                coverItemsModel.setCoverId(cursor.getInt(27));
                coverItemsModel.setCoverPaths(cursor.getString(28));
                coverItemsModel.setCoverURL(cursor.getString(29));
                program.setCoverItemsModel(coverItemsModel);

                program.setCreatedDTTM(cursor.getString(30));
                program.setUpdatedDTTM(cursor.getString(31));

                UserModel userModel = new UserModel();
                userModel.setUserId(cursor.getInt(32));
                userModel.setFirstName(cursor.getString(33));
                userModel.setLastName(cursor.getString(34));
                userModel.setProfilePicUrl(cursor.getString(35));
                program.setOfficial(Boolean.valueOf(cursor.getString(36)));
                program.setUserModel(userModel);

                ArrayList<DatesTripModel> datesTripModelArrayList = new ArrayList<>();
                SQLiteDatabase dbDatesTrip = database.getWritableDatabase();
                Cursor cursor_dates = dbDatesTrip.rawQuery("SELECT * from DatesTrip where programTour_id = " + itemID, null);
                if (cursor_dates != null && cursor_dates.getCount() != 0) {
                    cursor_dates.moveToFirst();
                    do {
                        DatesTripModel datesTripModel = new DatesTripModel();
                        datesTripModel.setDatesTripId(cursor_dates.getInt(0));
                        datesTripModel.setDatesTripTopicThai(cursor_dates.getString(1));
                        datesTripModel.setDatesTripTopicEnglish(cursor_dates.getString(2));
                        datesTripModel.setDatesTripTopicLaos(cursor_dates.getString(3));
                        datesTripModel.setDatesTripTopicChinese(cursor_dates.getString(4));

                        ArrayList<TouristAttractionsModel> touristAttractionsModelArrayList = new ArrayList<>();
                        SQLiteDatabase dbTourist = database.getWritableDatabase();
                        Cursor cursor_tourist = dbTourist.rawQuery("SELECT * from TouristAttractions where datesTrip_id = " + datesTripModel.getDatesTripId(), null);
                        if (cursor_tourist != null && cursor_tourist.getCount() != 0){
                            cursor_tourist.moveToFirst();
                            do {
                                TouristAttractionsModel touristAttractionsModel = new TouristAttractionsModel();
                                touristAttractionsModel.setAttractionsId(cursor_tourist.getInt(0));
                                touristAttractionsModel.setDetailTH(cursor_tourist.getString(1));
                                touristAttractionsModel.setDetailEN(cursor_tourist.getString(2));
                                touristAttractionsModel.setDetailZH(cursor_tourist.getString(3));
                                touristAttractionsModel.setDetailLO(cursor_tourist.getString(4));

                                ItemsModel itemsModel = new ItemsModel();
                                SQLiteDatabase dbItems = database.getWritableDatabase();
                                Cursor cursor_items = dbItems.rawQuery("SELECT * from Items where items_id =  " + cursor_tourist.getString(6), null);
                                if (cursor_items != null && cursor_items.getCount() != 0){
                                    cursor_items.moveToFirst();
                                    do {
                                        itemsModel.setItemsId(cursor_items.getInt(0));
                                        itemsModel.setTopicTH(cursor_items.getString(1));
                                        itemsModel.setTopicEN(cursor_items.getString(2));
                                        itemsModel.setTopicZH(cursor_items.getString(3));
                                        itemsModel.setTopicLO(cursor_items.getString(4));
                                        itemsModel.setContactTH(cursor_items.getString(7));
                                        itemsModel.setContactEN(cursor_items.getString(8));
                                        itemsModel.setContactZH(cursor_items.getString(9));
                                        itemsModel.setContactLO(cursor_items.getString(10));
                                        itemsModel.setTimeOpen(cursor_items.getString(57));
                                        itemsModel.setTimeClose(cursor_items.getString(58));
                                        itemsModel.setPhone(cursor_items.getString(11));
                                        MenuItemModel menu = new MenuItemModel();
                                        menu.setMenuItemId(cursor.getInt(17));
                                        menu.setMenuItemTH(cursor.getString(18));
                                        menu.setMenuItemEN(cursor.getString(19));
                                        menu.setMenuItemZH(cursor.getString(20));
                                        menu.setMenuItemLO(cursor.getString(21));
                                        itemsModel.setMenuItemModel(menu);
                                        CoverItemsModel coverItems = new CoverItemsModel();
                                        coverItems.setCoverId(cursor_items.getInt(69));
                                        coverItems.setCoverPaths(cursor_items.getString(70));
                                        coverItems.setCoverURL(cursor_items.getString(71));
                                        itemsModel.setCoverItemsModel(coverItems);
                                        CategoryModel category = new CategoryModel();
                                        category.setCategoryId(cursor_items.getInt(27));
                                        category.setCategoryTH(cursor_items.getString(28));
                                        category.setCategoryEN(cursor_items.getString(29));
                                        category.setCategoryLO(cursor_items.getString(30));
                                        category.setCategoryZH(cursor_items.getString(31));
                                        itemsModel.setCategoryModel(category);
                                    }while (cursor_items.moveToNext());
                                }dbItems.close();
                                touristAttractionsModel.setItemsModel(itemsModel);

                                ArrayList<PhotoTourist> photoTouristArrayList = new ArrayList<>();
                                SQLiteDatabase dbPhotoTourist = database.getWritableDatabase();
                                Cursor cursor_photoTourist = dbPhotoTourist.rawQuery("SELECT * from PhotoTourist where TouristAttractions_id =  " + touristAttractionsModel.getAttractionsId(), null);
                                if (cursor_photoTourist != null && cursor_photoTourist.getCount() != 0) {
                                    cursor_photoTourist.moveToFirst();
                                    do {
                                        PhotoTourist photoTourist = new PhotoTourist();
                                        photoTourist.setPhotoTouristId(cursor_photoTourist.getInt(0));
                                        photoTourist.setPhotoTouristPaths(cursor_photoTourist.getString(1));
                                        photoTourist.setPhotoTouristTH(cursor_photoTourist.getString(2));
                                        photoTourist.setPhotoTouristEN(cursor_photoTourist.getString(3));
                                        photoTourist.setPhotoTouristLO(cursor_photoTourist.getString(4));
                                        photoTourist.setPhotoTouristZH(cursor_photoTourist.getString(5));
                                        photoTouristArrayList.add(photoTourist);

                                    }while (cursor_photoTourist.moveToNext());

                                }dbPhotoTourist.close();
                                touristAttractionsModel.setPhotoTouristArrayList(photoTouristArrayList);
                                touristAttractionsModelArrayList.add(touristAttractionsModel);

                            }while (cursor_tourist.moveToNext());
                        }dbTourist.close();
                        datesTripModel.setTouristAttractionsModelArrayList(touristAttractionsModelArrayList);
                        datesTripModelArrayList.add(datesTripModel);
                    }while (cursor_dates.moveToNext());
                }dbDatesTrip.close();
                program.setDatesTripModelArrayList(datesTripModelArrayList);

            }while (cursor.moveToNext());
        }
        db.close();
        return program;
    }

    public ItemsModel getItemsById(int itemsID, int menuID) {
        ItemsModel item = new ItemsModel();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM Items WHERE items_id = " + itemsID + " AND menuItem_id = " + menuID, null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);

                ArrayList<ItemsDetailModel> detailModelArrayList = new ArrayList<>();
                SQLiteDatabase dbDetail = database.getWritableDatabase();
                Cursor cursor_detail = dbDetail.rawQuery("Select * from Detail Where items_id =" + item.getItemsId(), null);
                if (cursor_detail != null && cursor_detail.getCount() != 0) {
                    cursor_detail.moveToFirst();
                    do {
                        ItemsDetailModel itemsDetail = new ItemsDetailModel();
                        ItemsTopicDetailModel topicDetail = new ItemsTopicDetailModel();
                        itemsDetail.setDetailId(cursor_detail.getInt(0));
                        itemsDetail.setDetailTH(cursor_detail.getString(1));
                        itemsDetail.setDetailEN(cursor_detail.getString(2));
                        itemsDetail.setDetailLO(cursor_detail.getString(3));
                        itemsDetail.setDetailZH(cursor_detail.getString(4));
                        topicDetail.setTopicDetailId(cursor_detail.getInt(5));
                        topicDetail.setTopicDetailTH(cursor_detail.getString(6));
                        topicDetail.setTopicDetailEN(cursor_detail.getString(7));
                        topicDetail.setTopicDetailLO(cursor_detail.getString(8));
                        topicDetail.setTopicDetailZH(cursor_detail.getString(9));
                        itemsDetail.setItemsTopicDetailModel(topicDetail);

                        ArrayList<ItemsPhotoDetailModel> photoDetailModelArrayList = new ArrayList<>();
                        SQLiteDatabase dbPhoto = database.getWritableDatabase();
                        Cursor cursor_photo = dbPhoto.rawQuery("Select * from PhotoDetail Where detail_id =" + itemsDetail.getDetailId(), null);
                        if (cursor_photo != null && cursor_photo.getCount() != 0) {
                            cursor_photo.moveToFirst();
                            do {
                                ItemsPhotoDetailModel photoDetail = new ItemsPhotoDetailModel();
                                photoDetail.setPhotoId(cursor_photo.getInt(0));
                                photoDetail.setPhotoPaths(cursor_photo.getString(1));
                                photoDetail.setPhotoTextTH(cursor_photo.getString(2));
                                photoDetail.setPhotoTextEN(cursor_photo.getString(3));
                                photoDetail.setPhotoTextZH(cursor_photo.getString(4));
                                photoDetail.setPhotoTextLO(cursor_photo.getString(5));
                                photoDetailModelArrayList.add(photoDetail);
                            } while (cursor_photo.moveToNext());

                        }dbPhoto.close();
                        itemsDetail.setItemsPhotoDetailModels(photoDetailModelArrayList);

                        detailModelArrayList.add(itemsDetail);
                    } while (cursor_detail.moveToNext());

                }dbDetail.close();
                item.setItemsDetailModelArrayList(detailModelArrayList);

                RatingStarModel ratingStarModel = new RatingStarModel();
                ratingStarModel.setAverageReview(cursor.getInt(72));
                ratingStarModel.setCountReview(cursor.getInt(73));
                ratingStarModel.setOneStar(cursor.getInt(74));
                ratingStarModel.setTwoStar(cursor.getInt(75));
                ratingStarModel.setThreeStar(cursor.getInt(76));
                ratingStarModel.setFourStar(cursor.getInt(77));
                ratingStarModel.setFiveStar(cursor.getInt(78));

                ArrayList<ReviewModel> reviewModelArrayList = new ArrayList<>();
                SQLiteDatabase dbReview = database.getWritableDatabase();
                Cursor cursor_Review = dbReview.rawQuery("Select * from Reviews Where items_id = " + item.getItemsId(), null);
                if (cursor_Review != null && cursor_Review.getCount() != 0) {
                    cursor_Review.moveToFirst();
                    do {
                        ReviewModel reviewModel = new ReviewModel();
                        UserModel userModel = new UserModel();
                        reviewModel.setReviewId(cursor_Review.getInt(0));
                        reviewModel.setReviewTimestamp(cursor_Review.getString(1));
                        reviewModel.setReviewRating(cursor_Review.getInt(2));
                        reviewModel.setReviewText(cursor_Review.getString(3));
                        userModel.setUserId(cursor_Review.getInt(4));
                        userModel.setFirstName(cursor_Review.getString(5));
                        userModel.setLastName(cursor_Review.getString(6));
                        userModel.setProfilePicUrl(cursor_Review.getString(7));
                        ItemsModel it = new ItemsModel();
                        it.setItemsId(cursor_Review.getInt(8));
                        it.setTopicTH(cursor_Review.getString(9));
                        it.setTopicEN(cursor_Review.getString(10));
                        it.setTopicZH(cursor_Review.getString(11));
                        it.setTopicLO(cursor_Review.getString(12));
                        reviewModel.setItemsModel(it);
                        reviewModel.setUserModel(userModel);
                        reviewModelArrayList.add(reviewModel);

                    } while (cursor_Review.moveToNext());
                }dbReview.close();
                ratingStarModel.setReviewModelArrayList(reviewModelArrayList);
                item.setRatingStarModel(ratingStarModel);


                ArrayList<DeliciousGuaranteeModel> deliciousGuaranteeModelArrayList = new ArrayList<>();
                SQLiteDatabase dbGuarantee = database.getWritableDatabase();
                Cursor cursor_guarantee = dbGuarantee.rawQuery("SELECT * from Delicious where items_id = " + item.getItemsId(), null);
                if (cursor_guarantee != null && cursor_guarantee.getCount() != 0){
                    cursor_guarantee.moveToFirst();
                    do {
                        DeliciousGuaranteeModel guaranteeModel = new DeliciousGuaranteeModel();
                        guaranteeModel.setGuaranteeId(cursor_guarantee.getInt(0));
                        guaranteeModel.setGuaranteeTH(cursor_guarantee.getString(1));
                        guaranteeModel.setGuaranteeEN(cursor_guarantee.getString(2));
                        guaranteeModel.setGuaranteeZH(cursor_guarantee.getString(3));
                        guaranteeModel.setGuaranteeLO(cursor_guarantee.getString(4));
                        guaranteeModel.setGuaranteePaths(cursor_guarantee.getString(5));
                        deliciousGuaranteeModelArrayList.add(guaranteeModel);
                    }while (cursor_guarantee.moveToNext());
                }dbGuarantee.close();
                item.setDeliciousGuaranteeModelArrayList(deliciousGuaranteeModelArrayList);

                ArrayList<ProductModel> productModelArrayList = new ArrayList<>();
                SQLiteDatabase dbProduct = database.getWritableDatabase();
                Cursor cursor_product = dbProduct.rawQuery("SELECT * from Product where items_id = " + item.getItemsId(), null);
                if (cursor_product != null && cursor_product.getCount() != 0){
                    cursor_product.moveToFirst();
                    do {
                        ProductModel productModel = new ProductModel();
                        productModel.setProductId(cursor_product.getInt(0));
                        productModel.setProductNamesTH(cursor_product.getString(1));
                        productModel.setProductNamesEN(cursor_product.getString(2));
                        productModel.setProductNamesZH(cursor_product.getString(3));
                        productModel.setProductNamesLO(cursor_product.getString(4));
                        productModel.setProductPrice(cursor_product.getDouble(5));
                        productModel.setDescriptionTH(cursor_product.getString(6));
                        productModel.setDescriptionEN(cursor_product.getString(7));
                        productModel.setDescriptionZH(cursor_product.getString(8));
                        productModel.setDescriptionLO(cursor_product.getString(9));
                        productModelArrayList.add(productModel);

                        ArrayList<ProductPhotoModel> productPhotoModelArrayList = new ArrayList<>();
                        SQLiteDatabase dbPhoto = database.getWritableDatabase();
                        Cursor cursor_photo = dbPhoto.rawQuery("SELECT * from PhotoProduct where product_id = " + productModel.getProductId(), null);
                        if (cursor_photo != null && cursor_photo.getCount() != 0){
                            cursor_photo.moveToFirst();
                            do {
                                ProductPhotoModel productPhotoModel = new ProductPhotoModel();
                                productPhotoModel.setProductPhotoId(cursor_photo.getInt(0));
                                productPhotoModel.setProductPhotoPaths(cursor_photo.getString(1));
                                productPhotoModelArrayList.add(productPhotoModel);
                            }while (cursor_photo.moveToNext());
                        }dbPhoto.close();
                        productModel.setProductPhotoModelArrayList(productPhotoModelArrayList);

                    }while (cursor_product.moveToNext());
                }dbProduct.close();
                item.setProductModelArrayList(productModelArrayList);

                ArrayList<FacilitiesModel> facilitiesModelArrayList = new ArrayList<>();
                SQLiteDatabase dbFacilities = database.getWritableDatabase();
                Cursor cursor_facilities = dbFacilities.rawQuery("SELECT * from Facilities where items_id = " + item.getItemsId(), null);
                if (cursor_facilities != null && cursor_facilities.getCount() != 0){
                    cursor_facilities.moveToFirst();
                    do {
                        FacilitiesModel facilitiesModel = new FacilitiesModel();
                        facilitiesModel.setFacilitiesId(cursor_facilities.getInt(0));
                        facilitiesModel.setFacilitiesTH(cursor_facilities.getString(1));
                        facilitiesModel.setFacilitiesEN(cursor_facilities.getString(2));
                        facilitiesModel.setFacilitiesZH(cursor_facilities.getString(3));
                        facilitiesModel.setFacilitiesLO(cursor_facilities.getString(4));
                        facilitiesModelArrayList.add(facilitiesModel);
                    }while (cursor_facilities.moveToNext());
                }dbFacilities.close();
                item.setFacilitiesModelArrayList(facilitiesModelArrayList);

                ArrayList<RoomModel> roomModelArrayList = new ArrayList<>();
                SQLiteDatabase dbRoom = database.getWritableDatabase();
                Cursor cursor_room = dbRoom.rawQuery("SELECT * from Room where items_id = " + item.getItemsId(), null);
                if (cursor_room != null && cursor_room.getCount() != 0){
                    cursor_room.moveToFirst();
                    do {
                        RoomModel roomModel = new RoomModel();
                        roomModel.setRoomId(cursor_room.getInt(0));
                        roomModel.setTopicTH(cursor_room.getString(1));
                        roomModel.setTopicEN(cursor_room.getString(2));
                        roomModel.setTopicZH(cursor_room.getString(3));
                        roomModel.setTopicLO(cursor_room.getString(4));
                        roomModel.setDescriptionTH(cursor_room.getString(5));
                        roomModel.setDescriptionEN(cursor_room.getString(6));
                        roomModel.setDescriptionZH(cursor_room.getString(7));
                        roomModel.setDescriptionLO(cursor_room.getString(8));
                        roomModel.setDetailsTH(cursor_room.getString(9));
                        roomModel.setDetailsEN(cursor_room.getString(10));
                        roomModel.setDetailsZH(cursor_room.getString(11));
                        roomModel.setDetailsLO(cursor_room.getString(12));
                        roomModel.setPrice(cursor_room.getDouble(13));
                        roomModel.setBreakFast(cursor_room.getInt(14));

                        ArrayList<RoomPictureModel> roomPictureModelArrayList = new ArrayList<>();
                        SQLiteDatabase dbPhotoRoom = database.getWritableDatabase();
                        Cursor cursor_photoRoom = dbPhotoRoom.rawQuery("SELECT * from PhotoRoom where room_id = " + roomModel.getRoomId(), null);
                        if (cursor_photoRoom != null && cursor_photoRoom.getCount() != 0){
                            cursor_photoRoom.moveToFirst();
                            do {
                                RoomPictureModel pictureModel = new RoomPictureModel();
                                pictureModel.setPictureId(cursor_photoRoom.getInt(0));
                                pictureModel.setPicturePaths(cursor_photoRoom.getString(1));
                                roomPictureModelArrayList.add(pictureModel);
                            }while (cursor_photoRoom.moveToNext());
                        }dbPhotoRoom.close();
                        roomModel.setRoomPictureModelArrayList(roomPictureModelArrayList);
                        roomModelArrayList.add(roomModel);
                    }while (cursor_room.moveToNext());
                }dbRoom.close();
                item.setRoomModelArrayList(roomModelArrayList);

            }while (cursor.moveToNext());
        }
        db.close();
        return item;
    }

    public ArrayList<ItemsTourismModel> getItemsNearTourist() {
        ArrayList<ItemsTourismModel> list = new ArrayList<ItemsTourismModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items Where menuItem_id = 2 LIMIT 10", null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                ItemsTourismModel item = new ItemsTourismModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsRestaurantModel> getItemsNearRestaurants() {
        ArrayList<ItemsRestaurantModel> list = new ArrayList<ItemsRestaurantModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items Where menuItem_id = 3 LIMIT 10", null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                ItemsRestaurantModel item = new ItemsRestaurantModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);


                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsHotelModel> getItemsNearHotels() {
        ArrayList<ItemsHotelModel> list = new ArrayList<ItemsHotelModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items Where menuItem_id = 5 LIMIT 10", null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                ItemsHotelModel item = new ItemsHotelModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);


                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsTourismModel> getItemsListTourist() {
        ArrayList<ItemsTourismModel> list = new ArrayList<ItemsTourismModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items Where menuItem_id = 2 ", null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                ItemsTourismModel item = new ItemsTourismModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);


                RatingStarModel ratingStarModel = new RatingStarModel();
                ratingStarModel.setAverageReview(cursor.getInt(72));
                ratingStarModel.setCountReview(cursor.getInt(73));
                ratingStarModel.setOneStar(cursor.getInt(74));
                ratingStarModel.setTwoStar(cursor.getInt(75));
                ratingStarModel.setThreeStar(cursor.getInt(76));
                ratingStarModel.setFourStar(cursor.getInt(77));
                ratingStarModel.setFiveStar(cursor.getInt(78));

                ArrayList<ReviewModel> reviewModelArrayList = new ArrayList<>();
                SQLiteDatabase dbReview = database.getWritableDatabase();
                Cursor cursor_Review = dbReview.rawQuery("Select * from Reviews Where items_id = " + item.getItemsId(), null);
                if (cursor_Review != null && cursor_Review.getCount() != 0) {
                    cursor_Review.moveToFirst();
                    do {
                        ReviewModel reviewModel = new ReviewModel();
                        UserModel userModel = new UserModel();
                        reviewModel.setReviewId(cursor_Review.getInt(0));
                        reviewModel.setReviewTimestamp(cursor_Review.getString(1));
                        reviewModel.setReviewRating(cursor_Review.getInt(2));
                        reviewModel.setReviewText(cursor_Review.getString(3));
                        userModel.setUserId(cursor_Review.getInt(4));
                        userModel.setFirstName(cursor_Review.getString(5));
                        userModel.setLastName(cursor_Review.getString(6));
                        userModel.setProfilePicUrl(cursor_Review.getString(7));
                        ItemsModel it = new ItemsModel();
                        it.setItemsId(cursor_Review.getInt(8));
                        it.setTopicTH(cursor_Review.getString(9));
                        it.setTopicEN(cursor_Review.getString(10));
                        it.setTopicZH(cursor_Review.getString(11));
                        it.setTopicLO(cursor_Review.getString(12));
                        reviewModel.setItemsModel(it);
                        reviewModel.setUserModel(userModel);
                        reviewModelArrayList.add(reviewModel);

                    } while (cursor_Review.moveToNext());
                }dbReview.close();
                ratingStarModel.setReviewModelArrayList(reviewModelArrayList);
                item.setRatingStarModel(ratingStarModel);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsRestaurantModel> getItemsListRestaurants() {
        ArrayList<ItemsRestaurantModel> list = new ArrayList<ItemsRestaurantModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items Where menuItem_id = 3 ", null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                ItemsRestaurantModel item = new ItemsRestaurantModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);

                RatingStarModel ratingStarModel = new RatingStarModel();
                ratingStarModel.setAverageReview(cursor.getInt(72));
                ratingStarModel.setCountReview(cursor.getInt(73));
                ratingStarModel.setOneStar(cursor.getInt(74));
                ratingStarModel.setTwoStar(cursor.getInt(75));
                ratingStarModel.setThreeStar(cursor.getInt(76));
                ratingStarModel.setFourStar(cursor.getInt(77));
                ratingStarModel.setFiveStar(cursor.getInt(78));

                ArrayList<ReviewModel> reviewModelArrayList = new ArrayList<>();
                SQLiteDatabase dbReview = database.getWritableDatabase();
                Cursor cursor_Review = dbReview.rawQuery("Select * from Reviews Where items_id = " + item.getItemsId(), null);
                if (cursor_Review != null && cursor_Review.getCount() != 0) {
                    cursor_Review.moveToFirst();
                    do {
                        ReviewModel reviewModel = new ReviewModel();
                        UserModel userModel = new UserModel();
                        reviewModel.setReviewId(cursor_Review.getInt(0));
                        reviewModel.setReviewTimestamp(cursor_Review.getString(1));
                        reviewModel.setReviewRating(cursor_Review.getInt(2));
                        reviewModel.setReviewText(cursor_Review.getString(3));
                        userModel.setUserId(cursor_Review.getInt(4));
                        userModel.setFirstName(cursor_Review.getString(5));
                        userModel.setLastName(cursor_Review.getString(6));
                        userModel.setProfilePicUrl(cursor_Review.getString(7));
                        ItemsModel it = new ItemsModel();
                        it.setItemsId(cursor_Review.getInt(8));
                        it.setTopicTH(cursor_Review.getString(9));
                        it.setTopicEN(cursor_Review.getString(10));
                        it.setTopicZH(cursor_Review.getString(11));
                        it.setTopicLO(cursor_Review.getString(12));
                        reviewModel.setItemsModel(it);
                        reviewModel.setUserModel(userModel);
                        reviewModelArrayList.add(reviewModel);

                    } while (cursor_Review.moveToNext());
                }dbReview.close();
                ratingStarModel.setReviewModelArrayList(reviewModelArrayList);
                item.setRatingStarModel(ratingStarModel);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsShoppingModel> getItemsListShopping() {
        ArrayList<ItemsShoppingModel> list = new ArrayList<ItemsShoppingModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items Where menuItem_id = 4 ", null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                ItemsShoppingModel item = new ItemsShoppingModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);

                RatingStarModel ratingStarModel = new RatingStarModel();
                ratingStarModel.setAverageReview(cursor.getInt(72));
                ratingStarModel.setCountReview(cursor.getInt(73));
                ratingStarModel.setOneStar(cursor.getInt(74));
                ratingStarModel.setTwoStar(cursor.getInt(75));
                ratingStarModel.setThreeStar(cursor.getInt(76));
                ratingStarModel.setFourStar(cursor.getInt(77));
                ratingStarModel.setFiveStar(cursor.getInt(78));

                ArrayList<ReviewModel> reviewModelArrayList = new ArrayList<>();
                SQLiteDatabase dbReview = database.getWritableDatabase();
                Cursor cursor_Review = dbReview.rawQuery("Select * from Reviews Where items_id = " + item.getItemsId(), null);
                if (cursor_Review != null && cursor_Review.getCount() != 0) {
                    cursor_Review.moveToFirst();
                    do {
                        ReviewModel reviewModel = new ReviewModel();
                        UserModel userModel = new UserModel();
                        reviewModel.setReviewId(cursor_Review.getInt(0));
                        reviewModel.setReviewTimestamp(cursor_Review.getString(1));
                        reviewModel.setReviewRating(cursor_Review.getInt(2));
                        reviewModel.setReviewText(cursor_Review.getString(3));
                        userModel.setUserId(cursor_Review.getInt(4));
                        userModel.setFirstName(cursor_Review.getString(5));
                        userModel.setLastName(cursor_Review.getString(6));
                        userModel.setProfilePicUrl(cursor_Review.getString(7));
                        ItemsModel it = new ItemsModel();
                        it.setItemsId(cursor_Review.getInt(8));
                        it.setTopicTH(cursor_Review.getString(9));
                        it.setTopicEN(cursor_Review.getString(10));
                        it.setTopicZH(cursor_Review.getString(11));
                        it.setTopicLO(cursor_Review.getString(12));
                        reviewModel.setItemsModel(it);
                        reviewModel.setUserModel(userModel);
                        reviewModelArrayList.add(reviewModel);

                    } while (cursor_Review.moveToNext());
                }dbReview.close();
                ratingStarModel.setReviewModelArrayList(reviewModelArrayList);
                item.setRatingStarModel(ratingStarModel);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsHotelModel> getItemsListHotels() {
        ArrayList<ItemsHotelModel> list = new ArrayList<ItemsHotelModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items Where menuItem_id = 5 ", null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                ItemsHotelModel item = new ItemsHotelModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);

                RatingStarModel ratingStarModel = new RatingStarModel();
                ratingStarModel.setAverageReview(cursor.getInt(72));
                ratingStarModel.setCountReview(cursor.getInt(73));
                ratingStarModel.setOneStar(cursor.getInt(74));
                ratingStarModel.setTwoStar(cursor.getInt(75));
                ratingStarModel.setThreeStar(cursor.getInt(76));
                ratingStarModel.setFourStar(cursor.getInt(77));
                ratingStarModel.setFiveStar(cursor.getInt(78));

                ArrayList<ReviewModel> reviewModelArrayList = new ArrayList<>();
                SQLiteDatabase dbReview = database.getWritableDatabase();
                Cursor cursor_Review = dbReview.rawQuery("Select * from Reviews Where items_id = " + item.getItemsId(), null);
                if (cursor_Review != null && cursor_Review.getCount() != 0) {
                    cursor_Review.moveToFirst();
                    do {
                        ReviewModel reviewModel = new ReviewModel();
                        UserModel userModel = new UserModel();
                        reviewModel.setReviewId(cursor_Review.getInt(0));
                        reviewModel.setReviewTimestamp(cursor_Review.getString(1));
                        reviewModel.setReviewRating(cursor_Review.getInt(2));
                        reviewModel.setReviewText(cursor_Review.getString(3));
                        userModel.setUserId(cursor_Review.getInt(4));
                        userModel.setFirstName(cursor_Review.getString(5));
                        userModel.setLastName(cursor_Review.getString(6));
                        userModel.setProfilePicUrl(cursor_Review.getString(7));
                        ItemsModel it = new ItemsModel();
                        it.setItemsId(cursor_Review.getInt(8));
                        it.setTopicTH(cursor_Review.getString(9));
                        it.setTopicEN(cursor_Review.getString(10));
                        it.setTopicZH(cursor_Review.getString(11));
                        it.setTopicLO(cursor_Review.getString(12));
                        reviewModel.setItemsModel(it);
                        reviewModel.setUserModel(userModel);
                        reviewModelArrayList.add(reviewModel);

                    } while (cursor_Review.moveToNext());
                }dbReview.close();
                ratingStarModel.setReviewModelArrayList(reviewModelArrayList);
                item.setRatingStarModel(ratingStarModel);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ItemsContactModel> getItemsListContact() {
        ArrayList<ItemsContactModel> list = new ArrayList<ItemsContactModel>();
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Items Where menuItem_id = 6 ", null);
        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            do {
                ItemsContactModel item = new ItemsContactModel();
                item.setItemsId(Integer.parseInt(cursor.getString(0)));
                item.setTopicTH(cursor.getString(1));
                item.setTopicEN(cursor.getString(2));
                item.setTopicZH(cursor.getString(3));
                item.setTopicLO(cursor.getString(4));
                item.setLatitude(cursor.getString(5));
                item.setLongitude(cursor.getString(6));
                item.setContactTH(cursor.getString(7));
                item.setContactEN(cursor.getString(8));
                item.setContactZH(cursor.getString(9));
                item.setContactLO(cursor.getString(10));
                item.setPhone(cursor.getString(11));
                item.setEmail(cursor.getString(12));
                item.setLine(cursor.getString(13));
                item.setFacebookPage(cursor.getString(14));
                item.setArURL(cursor.getString(16));

                MenuItemModel menu = new MenuItemModel();
                menu.setMenuItemId(cursor.getInt(17));
                menu.setMenuItemTH(cursor.getString(18));
                menu.setMenuItemEN(cursor.getString(19));
                menu.setMenuItemZH(cursor.getString(20));
                menu.setMenuItemLO(cursor.getString(21));
                item.setMenuItemModel(menu);

                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                SubCategoryModel subCategory = new SubCategoryModel();
                subCategory.setCategoryId(cursor.getInt(27));
                subCategory.setCategoryTH(cursor.getString(28));
                subCategory.setCategoryEN(cursor.getString(29));
                subCategory.setCategoryZH(cursor.getString(30));
                subCategory.setCategoryLO(cursor.getColumnName(31));
                subCategoryArrayList.add(subCategory);
                item.setSubCategoryModelArrayList(subCategoryArrayList);

                item.setPrice(cursor.getDouble(48));
                item.setHighlightsTH(cursor.getString(49));
                item.setHighlightsEN(cursor.getString(50));
                item.setHighlightsZH(cursor.getString(51));
                item.setHighlightsLO(cursor.getString(52));
                item.setLocationInformationTH(cursor.getString(53));
                item.setLocationInformationEN(cursor.getString(54));
                item.setLocationInformationZH(cursor.getString(55));
                item.setLocationInformationLO(cursor.getString(56));
                item.setTimeOpen(cursor.getString(57));
                item.setTimeClose(cursor.getString(58));

                PeriodTimeModel periodTime = new PeriodTimeModel();
                PeriodDayModel periodDay = new PeriodDayModel();

                periodTime.setPeriodId(0);
                periodTime.setPeriodOpen(cursor.getString(57));
                periodTime.setPeriodClose(cursor.getString(58));

                periodDay.setPeriodOpenId(cursor.getInt(59));
                periodDay.setPeriodOpenTH(cursor.getString(60));
                periodDay.setPeriodOpenEN(cursor.getString(61));
                periodDay.setPeriodOpenZH(cursor.getString(62));
                periodDay.setPeriodOpenLO(cursor.getString(63));

                periodDay.setPeriodCloseId(cursor.getInt(64));
                periodDay.setPeriodCloseTH(cursor.getString(65));
                periodDay.setPeriodCloseEN(cursor.getString(66));
                periodDay.setPeriodCloseZH(cursor.getString(67));
                periodDay.setPeriodCloseLO(cursor.getString(68));

                item.setPeriodTimeModel(periodTime);
                item.setPeriodDayModel(periodDay);

                CoverItemsModel cover = new CoverItemsModel();
                cover.setCoverId(cursor.getInt(69));
                cover.setCoverPaths(cursor.getString(70));
                cover.setCoverURL(cursor.getString(71));
                item.setCoverItemsModel(cover);

                RatingStarModel ratingStarModel = new RatingStarModel();
                ratingStarModel.setAverageReview(cursor.getInt(72));
                ratingStarModel.setCountReview(cursor.getInt(73));
                ratingStarModel.setOneStar(cursor.getInt(74));
                ratingStarModel.setTwoStar(cursor.getInt(75));
                ratingStarModel.setThreeStar(cursor.getInt(76));
                ratingStarModel.setFourStar(cursor.getInt(77));
                ratingStarModel.setFiveStar(cursor.getInt(78));

                ArrayList<ReviewModel> reviewModelArrayList = new ArrayList<>();
                SQLiteDatabase dbReview = database.getWritableDatabase();
                Cursor cursor_Review = dbReview.rawQuery("Select * from Reviews Where items_id = " + item.getItemsId(), null);
                if (cursor_Review != null && cursor_Review.getCount() != 0) {
                    cursor_Review.moveToFirst();
                    do {
                        ReviewModel reviewModel = new ReviewModel();
                        UserModel userModel = new UserModel();
                        reviewModel.setReviewId(cursor_Review.getInt(0));
                        reviewModel.setReviewTimestamp(cursor_Review.getString(1));
                        reviewModel.setReviewRating(cursor_Review.getInt(2));
                        reviewModel.setReviewText(cursor_Review.getString(3));
                        userModel.setUserId(cursor_Review.getInt(4));
                        userModel.setFirstName(cursor_Review.getString(5));
                        userModel.setLastName(cursor_Review.getString(6));
                        userModel.setProfilePicUrl(cursor_Review.getString(7));
                        ItemsModel it = new ItemsModel();
                        it.setItemsId(cursor_Review.getInt(8));
                        it.setTopicTH(cursor_Review.getString(9));
                        it.setTopicEN(cursor_Review.getString(10));
                        it.setTopicZH(cursor_Review.getString(11));
                        it.setTopicLO(cursor_Review.getString(12));
                        reviewModel.setItemsModel(it);
                        reviewModel.setUserModel(userModel);
                        reviewModelArrayList.add(reviewModel);

                    } while (cursor_Review.moveToNext());
                }dbReview.close();
                ratingStarModel.setReviewModelArrayList(reviewModelArrayList);
                item.setRatingStarModel(ratingStarModel);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

}
