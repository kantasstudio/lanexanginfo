package com.ipanda.lanexangtourism.database;

public class DatabaseSQL {

    public static String createTableProgramTour(){
        String sql = "Create Table IF NOT EXISTS ProgramTour " +
                "("+
                " programTour_id INTEGER PRIMARY KEY, programTour_nameThai TEXT null, programTour_nameEnglish TEXT null, programTour_nameChinese TEXT null, programTour_nameLaos TEXT null"+
                ", programTour_interestingThingsThai TEXT null, programTour_interestingThingsEnglish TEXT null, programTour_interestingThingsChinese TEXT null, programTour_interestingThingsLaos TEXT null"+
                ", programTour_readcount INTEGER, Like_count INTEGER, Like_state NUMERIC, menuItem_id INTEGER, menuItem_thai TEXT, menuItem_english TEXT, menuItem_chinese TEXT, menuItem_laos TEXT "+
                ", category_id INTEGER, category_thai TEXT null, category_english TEXT null, category_chinese TEXT null, category_laos TEXT null"+
                ", subcategory_id INTEGER, subcategory_thai TEXT null, subcategory_english TEXT null, subcategory_chinese TEXT null, subcategory_laos TEXT null"+
                ", coverItem_id INTEGER, coverItem_paths TEXT null, coverItem_url TEXT null"+
                ", programTour_createdDTTM TEXT null, programTour_updatedDTTM TEXT null"+
                ", user_id INTEGER, user_firstName TEXT null, user_lastName TEXT null, user_profile_pic_url TEXT null"+
                ", official NUMERIC"+
                ")";
        return sql;
    }

    public static String createTableDatesTrip(){
        String sql = "Create Table IF NOT EXISTS DatesTrip "+
                "("+
                "datesTrip_id INTEGER PRIMARY KEY, datesTrip_topicThai TEXT null, datesTrip_topicEnglish TEXT null, datesTrip_topicLaos TEXT null, datesTrip_topicChinese TEXT null"+
                ", programTour_id INTEGER not null"+
                ")";
        return sql;
    }

    public static String createTableTouristAttractions(){
        String sql = "Create Table IF NOT EXISTS TouristAttractions "+
                "("+
                "touristAttractions_id INTEGER PRIMARY KEY"+
                ", detail_Thai TEXT null, detail_English TEXT null, detail_Chinese TEXT null, detail_Laos TEXT null"+
                ", datesTrip_id INTEGER not null, items_id INTEGER not null"+
                ")";
        return sql;
    }

    public static String createTablePhotoTourist(){
        String sql = "Create Table IF NOT EXISTS PhotoTourist "+
                "("+
                "photoTourist_id INTEGER PRIMARY KEY, photoTourist_paths TEXT null"+
                ", photoTourist_topicThai TEXT null, photoTourist_topicEnglish TEXT null, photoTourist_topicLaos TEXT null, photoTourist_topicChinese TEXT null"+
                ", TouristAttractions_id INTEGER not null"+
                ")";
        return sql;
    }

    public static String createTableItems(){
        String sql = "Create Table IF NOT EXISTS Items "+
                "("+
                "items_id INTEGER PRIMARY KEY"+
                ", items_topicThai TEXT null, items_topicEnglish TEXT null, items_topicChinese TEXT null, items_topicLaos TEXT null"+
                ", items_latitude REAL null, items_longitude REAL null, items_contactThai TEXT null, items_contactEnglish TEXT null, items_contactChinese TEXT null, items_contactLaos TEXT null"+
                ", items_phone TEXT null, items_email TEXT null, items_line TEXT null, items_facebookPage TEXT null, items_www TEXT null, item_ar_url TEXT null"+
                ", menuItem_id INTEGER, menuItem_thai TEXT null, menuItem_english TEXT null, menuItem_chinese TEXT null, menuItem_laos TEXT null "+
                ", category_id INTEGER, category_thai TEXT null, category_english TEXT null, category_chinese TEXT null, category_laos TEXT null"+
                ", subcategory_id INTEGER, subcategory_thai TEXT null, subcategory_english TEXT null, subcategory_chinese TEXT null, subcategory_laos TEXT null"+
                ", provinces_id INTEGER, provinces_thai TEXT null, provinces_english TEXT null, provinces_laos TEXT null, provinces_chinese TEXT null"+
                ", districts_id INTEGER, districts_thai TEXT null, districts_english TEXT null, districts_laos TEXT null, districts_chinese TEXT null"+
                ", subdistricts_id INTEGER, subdistricts_thai TEXT null, subdistricts_english TEXT null, subdistricts_laos TEXT null, subdistricts_chinese TEXT null"+
                ", BookMarks_state NUMERIC, items_price INTEGER null"+
                ", items_highlightsThai TEXT null, items_highlightsEnglish TEXT null, items_highlightsChinese TEXT null, items_highlightsLaos TEXT null"+
                ", items_locationInformationThai TEXT null, items_locationInformationEnglish TEXT null, items_locationInformationChinese TEXT null, items_locationInformationLaos TEXT null"+
                ", items_timeOpen TEXT null, items_timeClose TEXT null, open_day_id INTEGER, open_day_thai TEXT null, open_day_english TEXT null, open_day_chinese TEXT null, open_day_laos TEXT null"+
                ", closed_day_id INTEGER, closed_day_thai TEXT null, closed_day_english TEXT null, closed_day_chinese TEXT null, closed_day_laos TEXT null"+
                ", coverItem_id INTEGER, coverItem_paths TEXT null, coverItem_url TEXT null"+
                ", AverageReview INTEGER, CountReview INTEGER, OneStar INTEGER, TwoStar INTEGER, ThreeStar INTEGER, FourStar INTEGER, FiveStar INTEGER"+
                ", items_carSeats INTEGER, items_PricePerDay INTEGER, items_CarDeliveryPrice INTEGER, items_PickUpPrice INTEGER, items_DepositPrice INTEGER, items_isBasicInsurance INTEGER"+
                ", items_BissicInsurance_detailThai TEXT, items_BissicInsurance_detailEnglish TEXT, items_BissicInsurance_detailChinese TEXT, items_BissicInsurance_detailLaos TEXT "+
                ", priceGuestAdult INTEGER, priceGuestChild INTEGER"+
                ")";
        return sql;
    }

    public static String createTableDetail(){
        String sql = "Create Table IF NOT EXISTS Detail "+
                "("+
                "detail_id INTEGER PRIMARY KEY"+
                ", detail_textThai TEXT null, detail_textEnglish TEXT null, detail_textLaos TEXT null, detail_textChinese TEXT null"+
                ", topicdetail_id INTEGER, topicdetail_Thai TEXT null, topicdetail_English TEXT null, topicdetail_Laos TEXT null, topicdetail_Chinese TEXT null"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTablePhotoDetail(){
        String sql = "Create Table IF NOT EXISTS PhotoDetail "+
                "("+
                "photo_id INTEGER PRIMARY KEY"+
                ", photo_paths TEXT, photo_textThai TEXT, photo_textEnglish TEXT, photo_textChinese TEXT, photo_textLaos TEXT"+
                ", detail_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableReviews(){
        String sql = "Create Table IF NOT EXISTS Reviews "+
                "("+
                "review_id INTEGER PRIMARY KEY"+
                ", review_timestamp TEXT, review_rating INTEGER, review_text TEXT, user_id INTEGER, user_firstName TEXT, user_lastName TEXT, user_profile_pic_url TEXT"+
                ", items_id INTEGER, items_topicThai TEXT,items_topicEnglish TEXT, items_topicChinese TEXT, items_topicLaos TEXT"+
                ", programTour_id INTEGER, programTour_nameThai TEXT,programTour_nameEnglish TEXT, programTour_nameChinese TEXT, programTour_nameLaos TEXT"+
                ")";
        return sql;
    }

    public static String createTableDelicious(){
        String sql = "Create Table IF NOT EXISTS Delicious "+
                "("+
                "delicious_id INTEGER PRIMARY KEY"+
                ", guarantee_textThai TEXT, guarantee_textEnglish TEXT, guarantee_textChinese TEXT, guarantee_textLaos TEXT, guarantee_paths TEXT"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableProduct(){
        String sql = "Create Table IF NOT EXISTS Product "+
                "("+
                "product_id INTEGER PRIMARY KEY"+
                ", product_namesThai TEXT, product_namesEnglish TEXT, product_namesChinese TEXT, product_namesLaos TEXT, product_price REAL"+
                ", product_descriptionThai TEXT, product_descriptionEnglish TEXT, product_descriptionChinese TEXT, product_descriptionLaos TEXT"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTablePhotoProduct(){
        String sql = "Create Table IF NOT EXISTS PhotoProduct "+
                "("+
                "photoProduct_id INTEGER PRIMARY KEY"+
                ", photoProduct_paths TEXT"+
                ", product_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableFacilities(){
        String sql = "Create Table IF NOT EXISTS Facilities "+
                "("+
                "facilities_id INTEGER PRIMARY KEY"+
                ", facilities_textThai TEXT, facilities_textEnglish TEXT, facilities_textChinese TEXT, facilities_textLaos TEXT"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableRoom(){
        String sql = "Create Table IF NOT EXISTS Room "+
                "("+
                "room_id INTEGER PRIMARY KEY"+
                ", room_topicThai TEXT null, room_topicEnglish TEXT null, room_topicChinese TEXT null, room_topicLaos TEXT null"+
                ", room_descriptionThai TEXT null, room_descriptionEnglish TEXT null, room_descriptionChinese TEXT null, room_descriptionLaos TEXT null"+
                ", room_detailsThai TEXT null, room_detailsEnglish TEXT null, room_detailsChinese TEXT null, room_detailsLaos TEXT null"+
                ", room_price INTEGER, room_breakfast INTEGER"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTablePhotoRoom(){
        String sql = "Create Table IF NOT EXISTS PhotoRoom "+
                "("+
                "pictureRoom_id INTEGER PRIMARY KEY"+
                ", pictureRoom_paths TEXT"+
                ", room_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableCoverItems(){
        String sql = "Create Table IF NOT EXISTS CoverItems "+
                "("+
                "coverItem_id INTEGER PRIMARY KEY"+
                ", coverItem_paths TEXT, coverItem_url TEXT"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableScopeArea(){
        String sql = "Create Table IF NOT EXISTS ScopeArea "+
                "("+
                "provinces_id INTEGER PRIMARY KEY"+
                ", provinces_thai TEXT, provinces_english TEXT, provinces_laos TEXT, provinces_chinese TEXT"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableTravelDetails(){
        String sql = "Create Table IF NOT EXISTS TravelDetails "+
                "("+
                "travelDetails_id INTEGER PRIMARY KEY"+
                ", travelDetails_dateThai TEXT, travelDetails_dateEnglish TEXT, travelDetails_dateChinese TEXT, travelDetails_dateLaos TEXT"+
                ", travelDetails_textThai TEXT, travelDetails_textEnglish TEXT, travelDetails_textChinese TEXT, travelDetails_textLaos TEXT"+
                ", travelDetails_hotel_nameThai TEXT, travelDetails_hotel_nameEnglish TEXT, travelDetails_hotel_nameChinese TEXT, travelDetails_hotel_nameLaos TEXT"+
                ", travelDetails_food_breakfast REAL, travelDetails_food_lunch REAL, travelDetails_food_dinner REAL"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableTravelPeriod(){
        String sql = "Create Table IF NOT EXISTS TravelPeriod "+
                "("+
                "travelPeriod_id INTEGER PRIMARY KEY"+
                ", travelPeriod_amount INTEGER, travelPeriod_time_period_start TEXT, travelPeriod_time_period_end TEXT"+
                ", travelPeriod_adult_price INTEGER, travelPeriod_adult_special_price INTEGER, travelPeriod_child_price INTEGER, travelPeriod_child_special_price INTEGER"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableBookingCondition(){
        String sql = "Create Table IF NOT EXISTS BookingCondition "+
                "("+
                "condition_id INTEGER PRIMARY KEY"+
                ", conditionCol_detailThai TEXT, conditionCol_detailEnglish TEXT, conditionCol_detailChinese TEXT, conditionCol_detailLaos TEXT"+
                ", conditionCategory_topicThai TEXT, conditionCategory_topicEnglish TEXT, conditionCategory_topicChinese TEXT, conditionCategory_topicLaos TEXT"+
                ", items_id INTEGER, conditionCategory_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableBusiness(){
        String sql = "Create Table IF NOT EXISTS Business "+
                "("+
                "business_id INTEGER PRIMARY KEY"+
                ", business_nameThai TEXT, business_nameEnglish TEXT, business_nameChinese TEXT, business_nameLaos TEXT"+
                ", business_presentAddress TEXT, business_license_paths TEXT, business_operator_number TEXT, business_phone TEXT, business_www TEXT"+
                ", business_facebook TEXT, business_line TEXT"+
                ", items_id INTEGER"+
                ")";
        return sql;
    }

    public static String createTableCar(){
        String sql = "Create Table IF NOT EXISTS Car "+
                "("+
                "items_id INTEGER PRIMARY KEY"+
                ", carSeats INTEGER, pricePerDay INTEGER, returnPrice INTEGER, pickUpPrice INTEGER, depositPrice INTEGER"+
                ", isBasicInsurance REAL, basicInsuranceTH TEXT, basicInsuranceEN TEXT, basicInsuranceZH TEXT, basicInsuranceLO TEXT"+
                ", typeOfCarTH TEXT, typeOfCarEN TEXT, typeOfCarZH TEXT, typeOfCarLO TEXT"+
                ", gearSystemTH TEXT, gearSystemEN TEXT, gearSystemZH TEXT, gearSystemLO TEXT"+
                ")";
        return sql;
    }


}
