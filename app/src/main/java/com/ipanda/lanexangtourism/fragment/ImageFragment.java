package com.ipanda.lanexangtourism.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;


public class ImageFragment extends Fragment {

    //variables
    private static String TAG = ImageFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private CoverItemsModel coverItemsModel;

    private ImageView img_cover_image_id;

    public ImageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {
            coverItemsModel = getArguments().getParcelable("CoverItemsModel");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);

        img_cover_image_id = view.findViewById(R.id.img_cover_image_id);


        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+coverItemsModel.getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_cover_image_id);

        return view;
    }
}
