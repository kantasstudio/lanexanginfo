package com.ipanda.lanexangtourism.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Adapter.MultipleImagePagerAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;


public class MultipleImageFragment extends Fragment {

    //variables
    private static String TAG = MultipleImageFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ArrayList<CoverItemsModel> coverItemsModel;

    private ImageView img_cover_image_id;

    private MultipleImagePagerAdapter mAdapter;

    private ViewPager mViewPager;

    private TabLayout tabIndicator;


    public MultipleImageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {
            coverItemsModel = getArguments().getParcelableArrayList("CoverItemsModel");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_multiple, container, false);

        img_cover_image_id = view.findViewById(R.id.img_cover_image_id);
        mViewPager = view.findViewById(R.id.cover_viewpager_id);
        tabIndicator = view.findViewById(R.id.tab_indicator);

        mAdapter = new MultipleImagePagerAdapter(getContext(), coverItemsModel);
        mViewPager.setAdapter(mAdapter);

        tabIndicator.setupWithViewPager(mViewPager);
//        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
//        Picasso.get().load(paths+coverItemsModel.get(0).getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_cover_image_id);

        return view;
    }
}
