package com.ipanda.lanexangtourism.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.ipanda.lanexangtourism.Activity.ListMessageActivity;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.R;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;

import java.util.ArrayList;


public class MultipleVideoFragment extends Fragment implements View.OnClickListener {

    //variables
    private static String TAG = MultipleVideoFragment.class.getSimpleName();

    private ChangeLanguageLocale languageLocale;

    private ArrayList<CoverItemsModel> coverItemsModel;

    private ConstraintLayout con_show_cover_videos_id, con_show_videos_id;

    private ImageView img_cover_video_id, img_play_video_id;

    private WebView youtube_web_view_id;

    private YouTubePlayerView youTubePlayerView;

    public MultipleVideoFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get language
        languageLocale = new ChangeLanguageLocale(getContext());
        languageLocale.getLoadLocale();

        if (getArguments() != null) {
            coverItemsModel = getArguments().getParcelableArrayList("CoverItemsModel");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_multiple, container, false);

        //views
        con_show_cover_videos_id = view.findViewById(R.id.con_show_cover_videos_id);
        con_show_videos_id = view.findViewById(R.id.con_show_videos_id);
        img_cover_video_id = view.findViewById(R.id.img_cover_video_id);
        img_play_video_id = view.findViewById(R.id.img_play_video_id);
        youtube_web_view_id = view.findViewById(R.id.youtube_web_view_id);
        youTubePlayerView = view.findViewById(R.id.youtube_view);

        //set on click
//        img_play_video_id.setOnClickListener(this);

        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
//        Picasso.get().load(paths+coverItemsModel.getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_cover_video_id);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_play_video_id:
                con_show_cover_videos_id.setVisibility(View.GONE);
                con_show_videos_id.setVisibility(View.VISIBLE);
                onLoadVideoYoutube();
                break;
        }
    }

    private void onLoadVideoYoutube() {
//        String videoId = coverItemsModel.getCoverURL().replace("https://www.youtube.com/watch?v=","");
//        youTubePlayerView.initialize(
//                initializedYouTubePlayer -> initializedYouTubePlayer.addListener(
//                        new AbstractYouTubePlayerListener() {
//                            @Override
//                            public void onReady() {
//                                initializedYouTubePlayer.loadVideo(videoId, 0);
//                            }
//                        }), true);
//        youTubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
//            @Override
//            public void onYouTubePlayerEnterFullScreen() {
//
//            }
//
//            @Override
//            public void onYouTubePlayerExitFullScreen() {
//
//            }
//        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        youTubePlayerView.release();
    }
}
