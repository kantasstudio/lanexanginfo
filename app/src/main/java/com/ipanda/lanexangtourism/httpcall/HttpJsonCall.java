package com.ipanda.lanexangtourism.httpcall;

import org.json.JSONObject;

public class HttpJsonCall {
    public static final int GET = 1;
    public static final int POST = 2;

    private String url;
    private int methodType;
    private JSONObject params;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getMethodType() {
        return methodType;
    }

    public void setMethodType(int methodType) {
        this.methodType = methodType;
    }

    public JSONObject getParams() {
        return params;
    }

    public void setParams(JSONObject params) {
        this.params = params;
    }

}
