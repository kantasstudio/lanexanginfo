package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.BookingModel;

import java.util.ArrayList;

public interface BookingItemsCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<BookingModel> bookingArrayList);
    void onRequestFailed(String result);
}
