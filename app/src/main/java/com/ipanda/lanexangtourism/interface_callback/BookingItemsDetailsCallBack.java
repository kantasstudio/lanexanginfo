package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.BookingModel;

public interface BookingItemsDetailsCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(BookingModel booking);
    void onRequestFailed(String result);
}
