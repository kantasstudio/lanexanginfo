package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.BookingModel;

import java.util.ArrayList;

public interface BookingOrderDetailsCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerOrderDetails(ArrayList<BookingModel> bookingArrayList);
    void onRequestFailed(String result);
}
