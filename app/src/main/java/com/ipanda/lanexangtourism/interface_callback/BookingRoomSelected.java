package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.RoomModel;

public interface BookingRoomSelected {

    void selectedRoom(RoomModel rooms);

}
