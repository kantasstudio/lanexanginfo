package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.BookMarksModel;

import java.util.ArrayList;

public interface BookmarksCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<BookMarksModel> bookMarksModelArrayList);
    void onRequestFailed(String result);

}
