package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.CategoryModel;

import java.util.ArrayList;

public interface CategoryCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerCategory(ArrayList<CategoryModel> categoryArrayList);
    void onRequestFailed(String result);

}
