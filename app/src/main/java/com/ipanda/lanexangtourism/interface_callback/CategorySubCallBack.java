package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.CategoryModel;

public interface CategorySubCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerCategorySub(CategoryModel categoryModel);
    void onRequestFailed(String result);

}
