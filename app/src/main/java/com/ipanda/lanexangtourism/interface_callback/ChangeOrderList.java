package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ProductModel;

import java.util.ArrayList;

public interface ChangeOrderList {

    void changeOrderList(int defaultValue, int position);

    void changeDeletedOrderList(ArrayList<ProductModel> productArrayList);

}
