package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ReviewModel;

import java.util.ArrayList;

public interface CommentCallBack {

    void onPreCallServiceComment();
    void onCallServiceComment();
    void onRequestCompleteComment(ArrayList<ReviewModel> reviewArrayList);
    void onRequestFailedComment(String result);

}
