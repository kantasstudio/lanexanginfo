package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.CountryModel;

import java.util.ArrayList;

public interface CountryCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerCountry(ArrayList<CountryModel> countryModel);
    void onRequestFailed(String result);

}
