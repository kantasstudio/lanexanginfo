package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.DistrictsModel;

import java.util.ArrayList;

public interface DistrictCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerDistrict(ArrayList<DistrictsModel> districtsList);
    void onRequestFailed(String result);

}
