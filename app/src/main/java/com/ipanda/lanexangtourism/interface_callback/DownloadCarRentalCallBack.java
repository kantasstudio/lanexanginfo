package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

import java.util.ArrayList;

public interface DownloadCarRentalCallBack {

    void onPreCallServiceDownload();
    void onCallServiceDownload();
    void onRequestCompleteListenerDownloadCarRental(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailedDownload(String result);

}
