package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;

import java.util.ArrayList;

public interface DownloadItemsCallBack {

    void onPreCallServiceDownload();
    void onCallServiceDownload();
    void onRequestCompleteListenerDownloadItems(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailedDownload(String result);

}
