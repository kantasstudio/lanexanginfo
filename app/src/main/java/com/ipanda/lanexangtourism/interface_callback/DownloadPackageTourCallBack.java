package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;

import java.util.ArrayList;

public interface DownloadPackageTourCallBack {

    void onPreCallServiceDownload();
    void onCallServiceDownload();
    void onRequestCompleteListenerDownloadPackageTour(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailedDownload(String result);

}
