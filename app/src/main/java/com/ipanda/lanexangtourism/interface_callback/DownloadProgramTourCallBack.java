package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ProgramTourModel;

import java.util.ArrayList;

public interface DownloadProgramTourCallBack {

    void onPreCallServiceDownload();
    void onCallServiceDownload();
    void onRequestCompleteListenerDownloadProgramTour(ArrayList<ProgramTourModel> programTourModelArrayList);
    void onRequestFailedDownload(String result);

}
