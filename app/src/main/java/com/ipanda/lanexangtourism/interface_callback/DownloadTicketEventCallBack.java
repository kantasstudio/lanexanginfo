package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

import java.util.ArrayList;

public interface DownloadTicketEventCallBack {

    void onPreCallServiceDownload();
    void onCallServiceDownload();
    void onRequestCompleteListenerDownloadTicketEvent(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailedDownload(String result);

}
