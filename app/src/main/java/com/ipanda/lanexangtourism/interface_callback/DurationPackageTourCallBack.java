package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.DurationPackageTourModel;

import java.util.ArrayList;

public interface DurationPackageTourCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<DurationPackageTourModel> durationArrayList);
    void onRequestFailed(String result);

}
