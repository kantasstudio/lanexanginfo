package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.RateModel;

public interface ExchangeRateCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(RateModel rateModel);
    void onRequestFailed(String result);
}
