package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.DeliciousGuaranteeModel;

import java.util.ArrayList;

public interface FilterDeliciousCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerDelicious(ArrayList<DeliciousGuaranteeModel> guaranteeArrayList);
    void onRequestFailed(String result);
}
