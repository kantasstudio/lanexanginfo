package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.FacilitiesModel;

import java.util.ArrayList;

public interface FilterFacilitiesCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerFacilities(ArrayList<FacilitiesModel> facilitiesArrayList);
    void onRequestFailed(String result);
}
