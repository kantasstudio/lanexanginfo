package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.CategoryModel;

public interface FilterItemsCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(CategoryModel categoryModel);
    void onRequestFailed(String result);
}
