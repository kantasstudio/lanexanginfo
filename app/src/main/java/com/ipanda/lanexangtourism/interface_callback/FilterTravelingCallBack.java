package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.CategoryModel;

public interface FilterTravelingCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerTraveling(CategoryModel categoryModel);
    void onRequestFailed(String result);
}
