package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.UserModel;

import java.util.ArrayList;

public interface FollowCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<UserModel> userModelArrayList);
    void onRequestFailed(String result);
}
