package com.ipanda.lanexangtourism.interface_callback;

public interface GetChatRoomCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerChatRoom(String chatRoomId);
    void onRequestFailed(String result);

}
