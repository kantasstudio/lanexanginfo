package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.UserModel;

public interface GetUserProfileCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerGetUserProfile(UserModel userModels);
    void onRequestFailed(String result);
}
