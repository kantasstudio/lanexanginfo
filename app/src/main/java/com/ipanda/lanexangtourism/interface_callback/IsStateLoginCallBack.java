package com.ipanda.lanexangtourism.interface_callback;

public interface IsStateLoginCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(boolean state, String userId);
    void onRequestFailed(String result);
}
