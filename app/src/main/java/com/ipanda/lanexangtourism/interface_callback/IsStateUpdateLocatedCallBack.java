package com.ipanda.lanexangtourism.interface_callback;

public interface IsStateUpdateLocatedCallBack {

    void onPreCallServiceUpdateLocated();
    void onCallServiceUpdateLocated();
    void onRequestCompleteUpdateLocated(boolean state);
    void onRequestFailedUpdateLocated(String result);

}
