package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

import java.util.ArrayList;

public interface ItemsCarRentCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailed(String result);

}
