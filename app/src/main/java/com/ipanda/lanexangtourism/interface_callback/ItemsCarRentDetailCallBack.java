package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

public interface ItemsCarRentDetailCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ItemsModel itemsModel);
    void onRequestFailed(String result);

}
