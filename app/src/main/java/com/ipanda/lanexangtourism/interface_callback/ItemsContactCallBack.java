package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsContactModel;

import java.util.ArrayList;

public interface ItemsContactCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteContact(ArrayList<ItemsContactModel> contactArrayList);
    void onRequestFailed(String result);

}
