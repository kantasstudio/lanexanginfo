package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

import java.util.ArrayList;

public interface ItemsHomeSearchPostCallBack {

    void onPreCallServicePost();
    void onCallServicePost();
    void onRequestCompleteListenerPost(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailedPost(String result);

}
