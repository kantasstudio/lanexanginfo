package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsHotelModel;

import java.util.ArrayList;

public interface ItemsHotelCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteHotel(ArrayList<ItemsHotelModel> hotelArrayList);
    void onRequestFailed(String result);

}
