package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

import java.util.ArrayList;

public interface ItemsLastSearchCarRentCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerLastSearchCarRent(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailed(String result);

}
