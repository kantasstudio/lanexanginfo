package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

import java.util.ArrayList;

public interface ItemsLastSearchHomeCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerLastSearch(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailed(String result);

}
