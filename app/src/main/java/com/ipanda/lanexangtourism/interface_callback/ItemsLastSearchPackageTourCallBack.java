package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

import java.util.ArrayList;

public interface ItemsLastSearchPackageTourCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerLastSearchPackageTour(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailed(String result);

}
