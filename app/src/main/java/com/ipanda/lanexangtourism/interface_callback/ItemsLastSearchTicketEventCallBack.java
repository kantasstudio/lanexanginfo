package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

import java.util.ArrayList;

public interface ItemsLastSearchTicketEventCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerLastSearchTicket(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailed(String result);

}
