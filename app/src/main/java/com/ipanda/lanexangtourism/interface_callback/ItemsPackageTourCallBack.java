package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

import java.util.ArrayList;

public interface ItemsPackageTourCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ItemsModel> itemsModelArrayList);
    void onRequestFailed(String result);

}
