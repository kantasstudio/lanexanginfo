package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsRestaurantModel;

import java.util.ArrayList;

public interface ItemsRestaurantCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteRestaurant(ArrayList<ItemsRestaurantModel> restaurantArrayList);
    void onRequestFailed(String result);

}
