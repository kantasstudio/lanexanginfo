package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsShoppingModel;

import java.util.ArrayList;

public interface ItemsShoppingCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteShopping(ArrayList<ItemsShoppingModel> shoppingArrayList);
    void onRequestFailed(String result);

}
