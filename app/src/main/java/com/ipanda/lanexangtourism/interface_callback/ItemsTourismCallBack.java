package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsTourismModel;

import java.util.ArrayList;

public interface ItemsTourismCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteTourism(ArrayList<ItemsTourismModel> tourismArrayList);
    void onRequestFailed(String result);

}
