package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ItemsModel;

public interface ItemsViewCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ItemsModel itemsModel);
    void onRequestFailed(String result);

}
