package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ProgramTourModel;

import java.util.ArrayList;

public interface LastSearchProgramTourCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerLastSearchProgramTour(ArrayList<ProgramTourModel> programTourModelArrayList);
    void onRequestFailed(String result);

}
