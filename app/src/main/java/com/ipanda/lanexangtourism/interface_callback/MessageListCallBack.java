package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.MessageModel;

import java.util.ArrayList;

public interface MessageListCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<MessageModel> messageArrayList);
    void onRequestFailed(String result);

}
