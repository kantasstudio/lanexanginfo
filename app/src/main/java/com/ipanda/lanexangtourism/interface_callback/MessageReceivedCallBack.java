package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.BookingModel;

import java.util.ArrayList;

public interface MessageReceivedCallBack {

    void onCallServiceMessageReceived(String state);

}
