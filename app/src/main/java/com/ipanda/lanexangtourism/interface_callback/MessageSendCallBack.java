package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.MessageModel;

import java.util.ArrayList;

public interface MessageSendCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerSendMessage(ArrayList<MessageModel> messageArrayList);
    void onRequestFailed(String result);

}
