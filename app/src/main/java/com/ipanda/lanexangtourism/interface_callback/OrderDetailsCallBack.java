package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ProductOrderDetails;

import java.util.ArrayList;

public interface OrderDetailsCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteOrderDetails(ArrayList<ProductOrderDetails> orderDetailsArrayList);
    void onRequestFailed(String result);
}
