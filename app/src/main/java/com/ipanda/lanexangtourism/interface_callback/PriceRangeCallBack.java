package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.CategoryModel;
import com.ipanda.lanexangtourism.Model.RangeModel;

import java.util.ArrayList;

public interface PriceRangeCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(RangeModel rangeModel);
    void onRequestFailed(String result);

}
