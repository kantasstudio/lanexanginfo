package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ProgramTourModel;

import java.util.ArrayList;

public interface ProgramTourCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ProgramTourModel> programTourModelArrayList );
    void onRequestFailed(String result);

}
