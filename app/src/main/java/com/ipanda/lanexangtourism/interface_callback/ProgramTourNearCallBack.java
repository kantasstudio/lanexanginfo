package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ProgramTourModel;

import java.util.ArrayList;

public interface ProgramTourNearCallBack {

    void onPreCallServiceProgramTour();
    void onCallServiceProgramTour();
    void onRequestCompleteListenerProgramTour(ArrayList<ProgramTourModel> programTourModelArrayList);
    void onRequestFailedProgramTour(String result);

}
