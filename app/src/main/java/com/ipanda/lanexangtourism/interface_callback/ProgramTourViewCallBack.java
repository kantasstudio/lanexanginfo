package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ProgramTourModel;

public interface ProgramTourViewCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ProgramTourModel programTourModel);
    void onRequestFailed(String result);

}
