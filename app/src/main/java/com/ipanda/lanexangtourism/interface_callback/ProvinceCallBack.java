package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ProvincesModel;

import java.util.ArrayList;

public interface ProvinceCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerProvinces(ArrayList<ProvincesModel> provincesList);
    void onRequestFailed(String result);

}
