package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.RatingStarModel;

public interface RatingStarCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteRatingStar(RatingStarModel ratingStarModel);
    void onRequestFailed(String result);

}
