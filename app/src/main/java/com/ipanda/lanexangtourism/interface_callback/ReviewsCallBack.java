package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ReviewModel;

import java.util.ArrayList;

public interface ReviewsCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteReviews(ArrayList<ReviewModel> reviewArrayList);
    void onRequestFailed(String result);

}
