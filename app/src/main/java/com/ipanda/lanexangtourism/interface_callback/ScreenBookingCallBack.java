package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ScreenBookingModel;
import com.ipanda.lanexangtourism.Model.ScreenOfficialModel;

import java.util.ArrayList;

public interface ScreenBookingCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerScreenBooking(ArrayList<ScreenBookingModel> screenBookingArrayList);
    void onRequestFailed(String result);

}
