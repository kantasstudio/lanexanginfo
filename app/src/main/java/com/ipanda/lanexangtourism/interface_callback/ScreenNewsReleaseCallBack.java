package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ScreenNewsReleaseModel;

import java.util.ArrayList;

public interface ScreenNewsReleaseCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerNewsRelease(ArrayList<ScreenNewsReleaseModel> releaseArrayList);
    void onRequestFailed(String result);

}
