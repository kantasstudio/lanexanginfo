package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ScreenOfficialModel;

import java.util.ArrayList;

public interface ScreenOfficialCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerScreenOfficial(ArrayList<ScreenOfficialModel> screenOfficialArrayList);
    void onRequestFailed(String result);

}
