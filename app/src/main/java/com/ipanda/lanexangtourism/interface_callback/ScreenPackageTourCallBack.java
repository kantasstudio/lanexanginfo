package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ScreenOfficialModel;
import com.ipanda.lanexangtourism.Model.ScreenPackageTourModel;

import java.util.ArrayList;

public interface ScreenPackageTourCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerPackageTour(ArrayList<ScreenPackageTourModel> screenPackageTourArrayList);
    void onRequestFailed(String result);

}
