package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.SeatsModel;

import java.util.ArrayList;

public interface SeatsCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<SeatsModel> seatsArrayList);
    void onRequestFailed(String result);

}
