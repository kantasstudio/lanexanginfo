package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.StickerModel;

import java.util.ArrayList;

public interface StickerCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<StickerModel> stickerArrayList);
    void onRequestFailed(String result);

}
