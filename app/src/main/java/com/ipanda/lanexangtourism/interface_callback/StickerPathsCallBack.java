package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.StickerModel;
import com.ipanda.lanexangtourism.Model.StickerPathsModel;

import java.util.ArrayList;

public interface StickerPathsCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerStickerPaths(ArrayList<StickerPathsModel> stickerPathsArrayList);
    void onRequestFailed(String result);

}
