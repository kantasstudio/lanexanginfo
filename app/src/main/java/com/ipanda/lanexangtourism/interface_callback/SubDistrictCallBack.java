package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.SubDistrictsModel;

import java.util.ArrayList;

public interface SubDistrictCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerSubDistricts(ArrayList<SubDistrictsModel> subDistrictsList);
    void onRequestFailed(String result);

}
