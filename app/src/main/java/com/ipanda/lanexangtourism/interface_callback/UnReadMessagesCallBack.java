package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.CategoryModel;

import java.util.ArrayList;

public interface UnReadMessagesCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerUnReadMessages(String count);
    void onRequestFailed(String result);

}
