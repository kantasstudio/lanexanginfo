package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.UserModel;

public interface UserCallBack {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(UserModel userModels);
    void onRequestFailed(String result);
}
