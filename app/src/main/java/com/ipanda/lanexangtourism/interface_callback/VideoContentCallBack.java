package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.Model.VideoContentModel;

import java.util.ArrayList;

public interface VideoContentCallBack {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteVideoContent(ArrayList<VideoContentModel> videoContentArrayList);
    void onRequestFailed(String result);

}
