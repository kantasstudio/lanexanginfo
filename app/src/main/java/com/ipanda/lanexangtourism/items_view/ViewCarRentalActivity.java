package com.ipanda.lanexangtourism.items_view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.CarRentalDetailsDriverActivity;
import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Adapter.ItemsCarRentalConditionPetrolAdapter;
import com.ipanda.lanexangtourism.Adapter.ItemsCarRentalDocumentAdapter;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ItemsCarRentalDetailAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.interface_callback.ItemsCarRentDetailCallBack;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ViewCarRentalActivity extends AppCompatActivity implements View.OnClickListener, ItemsCarRentDetailCallBack {

    //variables
    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private CardView cardView_onclick_selected_car;

    private ItemsModel items;

    private BookingModel bookingModel = new BookingModel();

    private String userId;

    private String startDate, startDay, startMonth, startTime;
    private String endDate, endDay, endMonth, endTime;
    private String startYears, endYears;

    private RateModel rateModel;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    private ItemsModel newItemsOffline = new ItemsModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_rental);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        isModeOnline = getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);

        //views
        mToolbar = findViewById(R.id.toolbar);
        cardView_onclick_selected_car = findViewById(R.id.cardView_onclick_selected_car);



        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }


        //set event onclick
        cardView_onclick_selected_car.setOnClickListener(this);


        String url ="";
        if (getIntent() != null) {
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            items = getIntent().getParcelableExtra("ITEMS_MODEL");
            bookingModel = getIntent().getParcelableExtra("BOOKING");
            startDate = getIntent().getStringExtra("START_DATE");
            startDay = getIntent().getStringExtra("START_DAY");
            startMonth = getIntent().getStringExtra("START_MONTH");
            startTime = getIntent().getStringExtra("START_TIME");
            startYears = getIntent().getStringExtra("START_YEAR");
            endDate = getIntent().getStringExtra("END_DATE");
            endDay = getIntent().getStringExtra("END_DAY");
            endMonth = getIntent().getStringExtra("END_MONTH");
            endTime = getIntent().getStringExtra("END_TIME");
            endYears = getIntent().getStringExtra("END_YEAR");
            url = getString(R.string.app_api_ip)+ "dasta_thailand/api/mobile/user/Reservations/CarRent?items_id="+items.getItemsId();
        }

        if (isModeOnline){
            new ItemsCarRentalDetailAsyncTask(this).execute(url);
        }else {
            getVersion = getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
            if (getVersion != 0) {
                mDatabase = new DatabaseHelper(this, getVersion);
                mManager = new DatabaseManager(this, mDatabase);
                setItemsOfflineMode();
            }else {
                alertDialog();
            }
        }


    }

    private void setItemsOfflineMode() {
        if (items != null){
            newItemsOffline = mManager.getItemsCarRentalById(items.getItemsId());
            if (newItemsOffline != null) {
                setUpTitle(newItemsOffline);
                setUpPrice(newItemsOffline);
                setUpPetrol(newItemsOffline);
                setUpDocument(newItemsOffline);
            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cardView_onclick_selected_car:
                if (isModeOnline) {
                    Intent intent = new Intent(this, CarRentalDetailsDriverActivity.class);
                    intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                    intent.putExtra("ITEMS_MODEL", items);
                    intent.putExtra("BOOKING", bookingModel);
                    intent.putExtra("START_DATE", startDate);
                    intent.putExtra("START_DAY", startDay);
                    intent.putExtra("START_MONTH", startMonth);
                    intent.putExtra("START_TIME", startTime);
                    intent.putExtra("START_YEAR", startYears);
                    intent.putExtra("END_DATE", endDate);
                    intent.putExtra("END_DAY", endDay);
                    intent.putExtra("END_MONTH", endMonth);
                    intent.putExtra("END_TIME", endTime);
                    intent.putExtra("END_YEAR", endYears);
                    if (userId != null) {
                        startActivity(intent);
                    } else {
                        startActivity(new Intent(this, LoginActivity.class));
                    }
                }else {
                    Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ItemsModel itemsModel) {
        if (itemsModel != null){
            setUpTitle(itemsModel);
            setUpPrice(itemsModel);
            setUpPetrol(itemsModel);
            setUpDocument(itemsModel);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private void setUpTitle(ItemsModel items) {
        ImageView img = findViewById(R.id.img_items);
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        TextView txt_name_car = findViewById(R.id.txt_name_car);
        TextView txt_type_car = findViewById(R.id.txt_type_car);
        TextView txt_transmission_car = findViewById(R.id.txt_transmission_car);
        TextView txt_passenger = findViewById(R.id.txt_passenger);
        TextView txt_company_name = findViewById(R.id.txt_company_name);
        TextView txt_price_car = findViewById(R.id.txt_price_car);
        TextView txt_insurance_fee = findViewById(R.id.txt_insurance_fee);
        TextView txt_exchange_rate_title_id = findViewById(R.id.txt_exchange_rate_title_id);
        TextView txt_exchange_rate_fee_id = findViewById(R.id.txt_exchange_rate_fee_id);

        if (isModeOnline){

        }else {
            img.setImageResource(R.drawable.offline_mode);
        }

        String paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths+items.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img);
        txt_passenger.setText(getResources().getString(R.string.text_passenger)+" : "+items.getCarModel().getSeats()+"");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String insuranceFee = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    insuranceFee = decimalFormat.format(items.getCarModel().getDepositPrice());
                    txt_price_car.setText(items.getCarModel().getPricePerDay()+"");
                    txt_insurance_fee.setText(insuranceFee);
                    txt_exchange_rate_title_id.setText(currency);
                    txt_exchange_rate_fee_id.setText(currency);
                    break;
                case "USD":
                    insuranceFee = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getDepositPrice(),rateModel.getRateUSD()));
                    txt_price_car.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateUSD())));
                    txt_insurance_fee.setText(insuranceFee);
                    txt_exchange_rate_title_id.setText(currency);
                    txt_exchange_rate_fee_id.setText(currency);
                    break;
                case "CNY":
                    insuranceFee = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getDepositPrice(),rateModel.getRateCNY()));
                    txt_price_car.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateCNY())));
                    txt_insurance_fee.setText(insuranceFee);
                    txt_exchange_rate_title_id.setText(currency);
                    txt_exchange_rate_fee_id.setText(currency);
                    break;
            }
        }else {
            insuranceFee = decimalFormat.format(items.getCarModel().getDepositPrice());
            txt_price_car.setText(items.getCarModel().getPricePerDay()+"");
            txt_insurance_fee.setText(insuranceFee);
        }


        switch (languageLocale.getLanguage()){
            case "th":
                toolbar_title.setText(items.getTopicTH());
                txt_name_car.setText(items.getTopicTH());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeTH());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemTH());
                break;
            case "en":
                toolbar_title.setText(items.getTopicEN());
                txt_name_car.setText(items.getTopicEN());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeEN());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemEN());
                break;
            case "lo":
                toolbar_title.setText(items.getTopicLO());
                txt_name_car.setText(items.getTopicLO());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeLO());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemLO());
                break;
            case "zh":
                toolbar_title.setText(items.getTopicZH());
                txt_name_car.setText(items.getTopicZH());
                txt_type_car.setText(getResources().getString(R.string.text_type)+" : "+items.getCarModel().getTypeZH());
                txt_transmission_car.setText(getResources().getString(R.string.text_gear)+" : "+items.getCarModel().getGearSystemZH());
                break;
        }

        if (items.getBusinessModel() != null){
            switch (languageLocale.getLanguage()){
                case "th":
                    txt_company_name.setText(items.getBusinessModel().getNameTH());
                    break;
                case "en":
                    txt_company_name.setText(items.getBusinessModel().getNameEN());
                    break;
                case "lo":
                    txt_company_name.setText(items.getBusinessModel().getNameLO());
                    break;
                case "zh":
                    txt_company_name.setText(items.getBusinessModel().getNameZH());
                    break;
            }
        }
    }

    private void setUpPrice(ItemsModel items) {
        TextView txt_delivery_fee = findViewById(R.id.txt_delivery_fee);
        TextView txt_pick_up_fee = findViewById(R.id.txt_pick_up_fee);
        TextView txt_car_rental_fee_per_day = findViewById(R.id.txt_car_rental_fee_per_day);
        TextView txt_car_delivery_fee = findViewById(R.id.txt_car_delivery_fee);
        TextView txt_delivery_fee_ex = findViewById(R.id.txt_delivery_fee_ex);
        TextView txt_pick_up_fee_ex = findViewById(R.id.txt_pick_up_fee_ex);
        TextView txt_car_rental_fee_per_day_ex = findViewById(R.id.txt_car_rental_fee_per_day_ex);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);

        ExchangeRate exchangeRate = new ExchangeRate();
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    txt_delivery_fee_ex.setText(currency);
                    txt_pick_up_fee_ex.setText(currency);
                    txt_car_rental_fee_per_day_ex.setText(currency);
                    txt_delivery_fee.setText(items.getCarModel().getReturnPrice()+"");
                    txt_pick_up_fee.setText(items.getCarModel().getPickUpPrice()+"");
                    txt_car_rental_fee_per_day.setText(items.getCarModel().getPricePerDay()+"");
                    txt_car_delivery_fee.setText("ฟรี!");
                    break;
                case "USD":
                    txt_delivery_fee_ex.setText(currency);
                    txt_pick_up_fee_ex.setText(currency);
                    txt_car_rental_fee_per_day_ex.setText(currency);
                    txt_delivery_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getReturnPrice(),rateModel.getRateUSD())));
                    txt_pick_up_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPickUpPrice(),rateModel.getRateUSD())));
                    txt_car_rental_fee_per_day.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateUSD())));
                    txt_car_delivery_fee.setText("ฟรี!");
                    break;
                case "CNY":
                    txt_delivery_fee_ex.setText(currency);
                    txt_pick_up_fee_ex.setText(currency);
                    txt_car_rental_fee_per_day_ex.setText(currency);
                    txt_delivery_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getReturnPrice(),rateModel.getRateCNY())));
                    txt_pick_up_fee.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPickUpPrice(),rateModel.getRateCNY())));
                    txt_car_rental_fee_per_day.setText(decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getCarModel().getPricePerDay(),rateModel.getRateCNY())));
                    txt_car_delivery_fee.setText("ฟรี!");
                    break;
            }
        }else {
            txt_delivery_fee.setText(items.getCarModel().getReturnPrice()+"");
            txt_pick_up_fee.setText(items.getCarModel().getPickUpPrice()+"");
            txt_car_rental_fee_per_day.setText(items.getCarModel().getPricePerDay()+"");
            txt_car_delivery_fee.setText("ฟรี!");
        }


    }

    private void setUpPetrol(ItemsModel items) {
        ArrayList<BookingConditionModel> arrayList = new ArrayList<>();
        for (BookingConditionModel booking : items.getBookingConditionModelArrayList()){
            if (booking.getConditionId() == 13){
                arrayList.add(booking);
            }
        }
        RecyclerView  recycler_condition_petrol = findViewById(R.id.recycler_condition_petrol);
        ItemsCarRentalConditionPetrolAdapter adapter = new ItemsCarRentalConditionPetrolAdapter(this, arrayList, languageLocale.getLanguage());
        recycler_condition_petrol.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycler_condition_petrol.setAdapter(adapter);

    }

    private void setUpDocument(ItemsModel items) {
        ArrayList<BookingConditionModel> arrayList = new ArrayList<>();
        for (BookingConditionModel booking : items.getBookingConditionModelArrayList()){
            if (booking.getConditionId() == 14){
                arrayList.add(booking);
            }
        }
        RecyclerView  recycler_document = findViewById(R.id.recycler_document);
        ItemsCarRentalDocumentAdapter adapter = new ItemsCarRentalDocumentAdapter(this, arrayList, languageLocale.getLanguage());
        recycler_document.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycler_document.setAdapter(adapter);
    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

}
