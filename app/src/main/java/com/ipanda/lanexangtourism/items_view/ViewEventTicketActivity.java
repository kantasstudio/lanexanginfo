package com.ipanda.lanexangtourism.items_view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ipanda.lanexangtourism.Activity.EventTicketDetailsActivity;
import com.ipanda.lanexangtourism.Activity.EventTicketMapsActivity;
import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Adapter.CoverViewPagerAdapter;
import com.ipanda.lanexangtourism.DialogFragment.ViewTermsConditionsDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.ViewThingsMeetDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ItemsTicketEventDetailAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.fragment.MultipleImageFragment;
import com.ipanda.lanexangtourism.fragment.VideoFragment;
import com.ipanda.lanexangtourism.interface_callback.ItemsTicketEventDetailCallBack;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ViewEventTicketActivity extends AppCompatActivity implements View.OnClickListener, ItemsTicketEventDetailCallBack, OnMapReadyCallback {

    //variables
    private static String TAG = ViewEventTicketActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private Button btn_confirm_booking;

    private MapView mapView;

    private GoogleMap googleMap;

    private Marker marker;

    private ConstraintLayout con_things_meet_event, con_conditions_event;

    private TextView txt_title_event_ticket, txt_company, txt_booking, txt_price, txt_price_booking;

    private TextView txt_highlights_description;

    private TextView txt_places_information;

    private TextView txt_currency;

    private TextView txt_type_activity;

    private ViewPager mViewPager;

    private ItemsModel items = new ItemsModel();

    private String userId;

    private RateModel rateModel;

    private Collection<String> lastSearchId = new ArrayList<>();

    private List<String> distinctLastSearchId = new ArrayList<>();

    private ItemsModel items_model;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event_ticket);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        isModeOnline = getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);

        //views
        mToolbar = findViewById(R.id.toolbar);
        btn_confirm_booking = findViewById(R.id.btn_confirm_booking);
        mapView = findViewById(R.id.mapView_mark_location);
        con_things_meet_event = findViewById(R.id.con_things_meet_event);
        con_conditions_event = findViewById(R.id.con_conditions_event);
        mViewPager = findViewById(R.id.cover_viewpager_id);
        txt_title_event_ticket = findViewById(R.id.txt_title_event_ticket);
        txt_company = findViewById(R.id.txt_company);
        txt_booking = findViewById(R.id.txt_booking);
        txt_price = findViewById(R.id.txt_price);
        txt_price_booking = findViewById(R.id.txt_price_booking);
        txt_currency = findViewById(R.id.txt_currency);
        txt_highlights_description = findViewById(R.id.txt_highlights_description);
        txt_places_information = findViewById(R.id.txt_places_information);
        txt_type_activity = findViewById(R.id.txt_type_activity);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        //set onclick event
        btn_confirm_booking.setOnClickListener(this);
        con_things_meet_event.setOnClickListener(this);
        con_conditions_event.setOnClickListener(this);
        txt_booking.setOnClickListener(this);

        String url ="";
        if (getIntent() != null) {
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");
            items_model = getIntent().getParcelableExtra("ITEMS_MODEL");

            if (isModeOnline) {
                url = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Reservations/EventTicket?items_id=" + items_model.getItemsId();
                new ItemsTicketEventDetailAsyncTask(this).execute(url);
                if (items_model != null) {
                    lastSearchId.add(items_model.getItemsId() + "");
                    distinctLastSearchId = lastSearchId.stream().distinct().collect(Collectors.toList());
                    setLastViews();
                }
            }else {
                getVersion = getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
                if (getVersion != 0) {
                    mDatabase = new DatabaseHelper(this, getVersion);
                    mManager = new DatabaseManager(this, mDatabase);
                    setItemsOfflineMode();
                }else {
                    alertDialog();
                }
            }

        }

    }

    private void setItemsOfflineMode() {
        if (items_model != null){
            items = mManager.getItemsTicketEventById(items_model.getItemsId());
            setupViewPager(mViewPager, items);
            setupTitleTicketEvent();
            setupHighlights();
            setupPlacesInfo();
        }
        try {
            ImageView imgModeOffline = findViewById(R.id.img_mode_offline);
            imgModeOffline.setImageResource(R.drawable.offline_mode);
            imgModeOffline.setVisibility(View.VISIBLE);
        }catch (Exception e){
            Log.e(TAG, e.getMessage().toString());
        }
    }

    private void setLastViews(){
        String getLastSearch = getSharedPreferences("PREF_APP_VIEW_TICKET", Context.MODE_PRIVATE).getString("APP_VIEW_TICKET",null);
        if (getLastSearch == null){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String result = String.join(",", distinctLastSearchId);
                getSharedPreferences("PREF_APP_VIEW_TICKET", Context.MODE_PRIVATE).edit().putString("APP_VIEW_TICKET", result).apply();
            }
        }else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String result = String.join(",", distinctLastSearchId);
                getSharedPreferences("PREF_APP_VIEW_TICKET", Context.MODE_PRIVATE).edit().putString("APP_VIEW_TICKET", result).apply();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        DialogFragment dialogFragment = null;
        Intent intent = new Intent(this, EventTicketDetailsActivity.class);
        switch (view.getId()){
            case R.id.btn_confirm_booking:
                intent.putExtra("ITEMS_MODEL",items);
                intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
                if (isModeOnline) {
                    if (userId != null) {
                        startActivity(intent);
                    } else {
                        startActivity(new Intent(this, LoginActivity.class));
                    }
                }
                break;
            case R.id.con_things_meet_event:
                dialogFragment = ViewThingsMeetDialogFragment.newInstance();
                dialogFragment.setArguments(bundle);
                bundle.putParcelable("ITEMS_MODEL",items);
                bundle.putParcelable("EXCHANGE_RATE_MODEL",rateModel);
                bundle.putParcelableArrayList("DETAIL_THING",items.getItemsDetailModelArrayList());
                dialogFragment.show(getSupportFragmentManager(),"ViewEventTicketActivity");
                break;
            case R.id.con_conditions_event:
                dialogFragment = ViewTermsConditionsDialogFragment.newInstance();
                dialogFragment.setArguments(bundle);
                bundle.putString("TYPE_BOOKING","Event");
                bundle.putParcelable("ITEMS_MODEL",items);
                bundle.putParcelable("EXCHANGE_RATE_MODEL",rateModel);
                bundle.putParcelableArrayList("BOOKING_CONDITION",items.getBookingConditionModelArrayList());
                dialogFragment.show(getSupportFragmentManager(), "ViewEventTicketActivity");
                break;
            case R.id.txt_booking:
                intent.putExtra("ITEMS_MODEL",items);
                intent.putExtra("EXCHANGE_RATE_MODEL",rateModel);
                if (isModeOnline) {
                    if (userId != null) {
                        startActivity(intent);
                    } else {
                        startActivity(new Intent(this, LoginActivity.class));
                    }
                }else {
                    Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ItemsModel itemsModel) {
        if (itemsModel != null){
            Log.e("check data", itemsModel+"");
            this.items = itemsModel;
            setupViewPager(mViewPager, itemsModel);
            setupTitleTicketEvent();
            setupHighlights();
            setupPlacesInfo();
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }


    private void setupViewPager(ViewPager mViewPager , ItemsModel itemsModel) {
        CoverViewPagerAdapter pagerAdapter = new CoverViewPagerAdapter(getSupportFragmentManager(),0);
        if (itemsModel.getCoverItemsModelArrayList() != null){
            if (itemsModel.getCoverItemsModelArrayList().get(0).getCoverPaths() != null){
                Bundle bdImage = new Bundle();
                Fragment image = new MultipleImageFragment();
                bdImage.putParcelableArrayList("CoverItemsModel",itemsModel.getCoverItemsModelArrayList());
                image.setArguments(bdImage);
                pagerAdapter.addFragment(image,"Image");
            }
            if (itemsModel.getCoverItemsModelArrayList().get(0).getCoverURL() != null){
                Bundle bdVideo = new Bundle();
                Fragment video = new VideoFragment();
                bdVideo.putParcelable("CoverItemsModel", itemsModel.getCoverItemsModelArrayList().get(0));
                video.setArguments(bdVideo);
                pagerAdapter.addFragment(video,"Video");
            }
        }
        mViewPager.setAdapter(pagerAdapter);
    }

    private void setupTitleTicketEvent() {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        TextView txt_exchange_rate_id = findViewById(R.id.txt_exchange_rate_id);
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(items.getPriceGuestAdult());
                    txt_exchange_rate_id.setText(currency);
                    txt_price.setText(price);
                    txt_price_booking.setText(price);
                    txt_currency.setText("THB");
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getPriceGuestAdult(),rateModel.getRateUSD()));
                    txt_exchange_rate_id.setText(currency);
                    txt_price.setText(price);
                    txt_price_booking.setText(price);
                    txt_currency.setText("USD");
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(items.getPriceGuestAdult(),rateModel.getRateCNY()));
                    txt_exchange_rate_id.setText(currency);
                    txt_price.setText(price);
                    txt_price_booking.setText(price);
                    txt_currency.setText("CNY");
                    break;
            }
        }else {
            price = decimalFormat.format(items.getPriceGuestAdult());
            txt_price.setText(price);
            txt_price_booking.setText(price);
            txt_currency.setText("THB");
        }



        switch (languageLocale.getLanguage()){
            case "th":
                txt_title_event_ticket.setText(items.getTopicTH());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
                break;
            case "en":
                txt_title_event_ticket.setText(items.getTopicEN());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                break;
            case "lo":
                txt_title_event_ticket.setText(items.getTopicLO());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                break;
            case "zh":
                txt_title_event_ticket.setText(items.getTopicZH());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                break;
        }

        if (items.getBusinessModel() != null){
            switch (languageLocale.getLanguage()){
                case "th":
                    txt_company.setText(items.getBusinessModel().getNameTH());
                    break;
                case "en":
                    txt_company.setText(items.getBusinessModel().getNameEN());
                    break;
                case "lo":
                    txt_company.setText(items.getBusinessModel().getNameLO());
                    break;
                case "zh":
                    txt_company.setText(items.getBusinessModel().getNameZH());
                    break;
            }
        }
    }

    private void setupHighlights(){
        switch (languageLocale.getLanguage()){
            case "th":
                txt_highlights_description.setText(items.getHighlightsTH());
                break;
            case "en":
                txt_highlights_description.setText(items.getHighlightsEN());
                break;
            case "lo":
                txt_highlights_description.setText(items.getHighlightsLO());
                break;
            case "zh":
                txt_highlights_description.setText(items.getHighlightsZH());
                break;
        }
    }

    private void setupPlacesInfo(){
        switch (languageLocale.getLanguage()){
            case "th":
                txt_places_information.setText(items.getContactTH());
                break;
            case "en":
                txt_places_information.setText(items.getContactEN());
                break;
            case "lo":
                txt_places_information.setText(items.getContactLO());
                break;
            case "zh":
                txt_places_information.setText(items.getContactZH());
                break;
        }
        if (isModeOnline) {
            float Latitude = Float.parseFloat(items.getLatitude());
            float Longitude = Float.parseFloat(items.getLongitude());
            LatLng sydney = new LatLng(Latitude, Longitude);
            googleMap.addMarker(new MarkerOptions()
                    .position(sydney));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

            LatLng location = new LatLng(Latitude, Longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14), 2000, null);

            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    Intent intent = new Intent(ViewEventTicketActivity.this, EventTicketMapsActivity.class);
                    intent.putExtra("ITEMS_MODEL", items);
                    startActivity(intent);
                }
            });
        }
    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
