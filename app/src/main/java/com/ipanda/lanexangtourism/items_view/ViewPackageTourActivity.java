package com.ipanda.lanexangtourism.items_view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.BookingDetailsActivity;
import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Adapter.CoverViewPagerAdapter;
import com.ipanda.lanexangtourism.Adapter.FoodTravelDetailsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.HotelTravelDetailsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.TitleTravelDetailsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.DialogFragment.ViewDetailTravelDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.ViewTermsConditionsDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Helper.ExchangeRate;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.TravelPeriodModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ItemsPackageTourDetailAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.fragment.MultipleImageFragment;
import com.ipanda.lanexangtourism.fragment.VideoFragment;
import com.ipanda.lanexangtourism.interface_callback.ItemsPackageTourDetailCallBack;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ViewPackageTourActivity extends AppCompatActivity implements View.OnClickListener , ItemsPackageTourDetailCallBack {

    //Variables
    private static String TAG = ViewPackageTourActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private ChangeLanguageLocale languageLocale;

    private ConstraintLayout con_detail_travel, con_terms_conditions;

    private Button btn_confirm_booking;

    private TextView txt_title_package_tour, txt_company, txt_price, txt_price_booking, txt_booking, txt_period;

    private TextView txt_type_activity;

    private TextView txt_mark_location;

    private TextView txt_currency;

    private ViewPager mViewPager;

    private ItemsModel items = new ItemsModel();

    private RecyclerView mRecyclerDetailTravel, mRecyclerHotel, mRecyclerFood;

    private TitleTravelDetailsRecyclerViewAdapter mAdapterTitleTravelDetails;

    private HotelTravelDetailsRecyclerViewAdapter mAdapterHotel;

    private FoodTravelDetailsRecyclerViewAdapter mAdapterFood;

    private String userId;

    private RateModel rateModel;

    private Collection<String> lastSearchId = new ArrayList<>();

    private List<String> distinctLastSearchId = new ArrayList<>();

    private ItemsModel items_model;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_package_tour);

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        isModeOnline = getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);

        //views
        mToolbar = findViewById(R.id.toolbar);
        con_detail_travel = findViewById(R.id.con_detail_travel);
        con_terms_conditions = findViewById(R.id.con_terms_conditions);
        btn_confirm_booking = findViewById(R.id.btn_confirm_booking);
        txt_booking = findViewById(R.id.txt_booking);
        mViewPager = findViewById(R.id.cover_viewpager_id);
        txt_title_package_tour = findViewById(R.id.txt_title_package_tour);
        txt_price = findViewById(R.id.txt_price);
        txt_price_booking = findViewById(R.id.txt_price_booking);
        txt_company = findViewById(R.id.txt_company);
        txt_period = findViewById(R.id.txt_period_package_tour);
        mRecyclerDetailTravel = findViewById(R.id.recycler_detail_travel_package_tour);
        mRecyclerHotel = findViewById(R.id.recycler_hotel_package_tour);
        mRecyclerFood = findViewById(R.id.recycler_food_package_tour);
        txt_type_activity = findViewById(R.id.txt_type_activity);
        txt_currency = findViewById(R.id.txt_currency);
        txt_mark_location = findViewById(R.id.txt_mark_location);


        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_arrow);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }


        //set on click event
        con_detail_travel.setOnClickListener(this);
        con_terms_conditions.setOnClickListener(this);
        btn_confirm_booking.setOnClickListener(this);
        txt_booking.setOnClickListener(this);
        String url ="";
        if (getIntent() != null) {
            items_model = getIntent().getParcelableExtra("ITEMS_MODEL");
            rateModel = getIntent().getParcelableExtra("EXCHANGE_RATE_MODEL");

            if (isModeOnline) {
                url = getResources().getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Reservations/PackageTours?items_id=" + items_model.getItemsId();
                new ItemsPackageTourDetailAsyncTask(this).execute(url);
                if (items_model != null) {
                    lastSearchId.add(items_model.getItemsId() + "");
                    distinctLastSearchId = lastSearchId.stream().distinct().collect(Collectors.toList());
                    setLastViews();
                }
            }else {
                getVersion = getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
                if (getVersion != 0) {
                    mDatabase = new DatabaseHelper(this, getVersion);
                    mManager = new DatabaseManager(this, mDatabase);
                    setItemsOfflineMode();
                }else {
                    alertDialog();
                }
            }

        }


    }

    private void setItemsOfflineMode() {
        if (items_model != null) {
            items = mManager.getItemsPackageToursById(items_model.getItemsId());
            setupViewPager(mViewPager, items);
            setupTitlePackageTour();
            setupDetailTravelPackageTour();
            setupHotelPackageTour();
            setupFoodPackageTour();
            setupPrice(items);

        }

        try {
            ImageView imgModeOffline = findViewById(R.id.img_mode_offline);
            imgModeOffline.setImageResource(R.drawable.offline_mode);
            imgModeOffline.setVisibility(View.VISIBLE);
        }catch (Exception e){
            Log.e(TAG, e.getMessage().toString());
        }
    }

    private void setLastViews(){
        String getLastSearch = getSharedPreferences("PREF_APP_VIEW_PACKAGE_TOUR", Context.MODE_PRIVATE).getString("APP_VIEW_PACKAGE_TOUR",null);
        if (getLastSearch == null){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String result = String.join(",", distinctLastSearchId);
                getSharedPreferences("PREF_APP_VIEW_PACKAGE_TOUR", Context.MODE_PRIVATE).edit().putString("APP_VIEW_PACKAGE_TOUR", result).apply();
            }
        }else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String result = String.join(",", distinctLastSearchId);
                getSharedPreferences("PREF_APP_VIEW_PACKAGE_TOUR", Context.MODE_PRIVATE).edit().putString("APP_VIEW_PACKAGE_TOUR", result).apply();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        DialogFragment dialogFragment = null;
        switch (view.getId()){
            case R.id.con_detail_travel:
                dialogFragment = ViewDetailTravelDialogFragment.newInstance();
                dialogFragment.setArguments(bundle);
                bundle.putString("TYPE_BOOKING","PackageTour");
                bundle.putParcelable("ITEMS_MODEL", items);
                bundle.putParcelable("EXCHANGE_RATE_MODEL", rateModel);
                bundle.putParcelableArrayList("TRAVEL_DETAILS",items.getTravelDetailsModelArrayList());
                dialogFragment.show(getSupportFragmentManager(),"ViewPackageTourActivity");
                break;
            case R.id.con_terms_conditions:
                dialogFragment = ViewTermsConditionsDialogFragment.newInstance();
                dialogFragment.setArguments(bundle);
                bundle.putString("TYPE_BOOKING","PackageTour");
                bundle.putParcelable("ITEMS_MODEL", items);
                bundle.putParcelable("EXCHANGE_RATE_MODEL", rateModel);
                bundle.putParcelableArrayList("BOOKING_CONDITION",items.getBookingConditionModelArrayList());
                dialogFragment.show(getSupportFragmentManager(),"ViewPackageTourActivity");
                break;
            case R.id.btn_confirm_booking:
                if (isModeOnline) {
                    Intent intent = new Intent(this, BookingDetailsActivity.class);
                    intent.putExtra("ITEMS_MODEL", items);
                    intent.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                    if (userId != null) {
                        startActivity(intent);
                    } else {
                        startActivity(new Intent(this, LoginActivity.class));
                    }
                }else {
                    Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.txt_booking:
                if (isModeOnline) {
                    Intent intentBooking = new Intent(this, BookingDetailsActivity.class);
                    intentBooking.putExtra("ITEMS_MODEL", items);
                    intentBooking.putExtra("EXCHANGE_RATE_MODEL", rateModel);
                    if (userId != null) {
                        startActivity(intentBooking);
                    } else {
                        startActivity(new Intent(this, LoginActivity.class));
                    }
                }else {
                    Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ItemsModel itemsModel) {
        if (itemsModel != null){
            Log.e("check data ", itemsModel + "");
            this.items = itemsModel;
            setupViewPager(mViewPager, itemsModel);
            setupTitlePackageTour();
            setupDetailTravelPackageTour();
            setupHotelPackageTour();
            setupFoodPackageTour();
            setupPrice(items);

        }
    }

    @Override
    public void onRequestFailed(String result) {

    }


    private void setupViewPager(ViewPager mViewPager , ItemsModel itemsModel) {
        CoverViewPagerAdapter pagerAdapter = new CoverViewPagerAdapter(getSupportFragmentManager(),0);
        if (itemsModel.getCoverItemsModelArrayList() != null){
            if (itemsModel.getCoverItemsModelArrayList().get(0).getCoverPaths() != null){
                Bundle bdImage = new Bundle();
                Fragment image = new MultipleImageFragment();
                bdImage.putParcelableArrayList("CoverItemsModel",itemsModel.getCoverItemsModelArrayList());
                image.setArguments(bdImage);
                pagerAdapter.addFragment(image,"Image");
            }
            if (itemsModel.getCoverItemsModelArrayList().get(0).getCoverURL() != null){
                Bundle bdVideo = new Bundle();
                Fragment video = new VideoFragment();
                bdVideo.putParcelable("CoverItemsModel", itemsModel.getCoverItemsModelArrayList().get(0));
                video.setArguments(bdVideo);
                pagerAdapter.addFragment(video,"Video");
            }
        }
        mViewPager.setAdapter(pagerAdapter);
    }

    private void setupTitlePackageTour() {
        switch (languageLocale.getLanguage()){
            case "th":
                txt_title_package_tour.setText(items.getTopicTH());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryTH());
               break;
            case "en":
                txt_title_package_tour.setText(items.getTopicEN());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryEN());
                break;
            case "lo":
                txt_title_package_tour.setText(items.getTopicLO());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryLO());
                break;
            case "zh":
                txt_title_package_tour.setText(items.getTopicZH());
                txt_type_activity.setText(items.getSubCategoryModelArrayList().get(0).getCategoryZH());
                break;
        }

        if (items.getBusinessModel() != null){
            switch (languageLocale.getLanguage()){
                case "th":
                    txt_company.setText(items.getBusinessModel().getNameTH());
                    break;
                case "en":
                    txt_company.setText(items.getBusinessModel().getNameEN());
                    break;
                case "lo":
                    txt_company.setText(items.getBusinessModel().getNameLO());
                    break;
                case "zh":
                    txt_company.setText(items.getBusinessModel().getNameZH());
                    break;
            }
        }

        if (items.getProvincesModelArrayList() != null && items.getProvincesModelArrayList().size() != 0){
            String province = "";
            for (ProvincesModel pro : items.getProvincesModelArrayList())
                switch (languageLocale.getLanguage()){
                    case "th":
                        province += pro.getProvincesTH()+" ";
                        break;
                    case "en":
                        province += pro.getProvincesEN()+" ";
                        break;
                    case "lo":
                        province += pro.getProvincesLO()+" ";
                        break;
                    case "zh":
                        province += pro.getProvincesZH()+" ";
                        break;
                }
            txt_mark_location.setText(province);
        }

        if (items.getTravelDetailsModelArrayList() != null && items.getTravelDetailsModelArrayList().size() != 0) {
            int day = items.getTravelDetailsModelArrayList().size();
            TextView txt_period_title = findViewById(R.id.txt_period);
            txt_period_title.setText(day+ " " +getResources().getString(R.string.text_day) + " " + (day - 1) + " " + getResources().getString(R.string.text_night));
        }

    }

    private void setupDetailTravelPackageTour() {
        if (items.getTravelDetailsModelArrayList() != null && items.getTravelDetailsModelArrayList().size() != 0) {
            int day = items.getTravelDetailsModelArrayList().size();
            txt_period.setText(getString(R.string.text_time_period) + " : " + (day) + " " + getString(R.string.text_day));
        }
        mAdapterTitleTravelDetails = new TitleTravelDetailsRecyclerViewAdapter(this,items.getTravelDetailsModelArrayList(), languageLocale.getLanguage());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerDetailTravel.setLayoutManager(layoutManager);
        mRecyclerDetailTravel.setAdapter(mAdapterTitleTravelDetails);
    }



    private void setupHotelPackageTour() {
        mAdapterHotel = new HotelTravelDetailsRecyclerViewAdapter(this,items.getHotelAccommodationModelArrayList(), languageLocale.getLanguage());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerHotel.setLayoutManager(layoutManager);
        mRecyclerHotel.setAdapter(mAdapterHotel);
    }

    private void setupFoodPackageTour(){
        mAdapterFood = new FoodTravelDetailsRecyclerViewAdapter(this,items.getFoodModelArrayList(), languageLocale.getLanguage());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerFood.setLayoutManager(layoutManager);
        mRecyclerFood.setAdapter(mAdapterFood);
    }

    private void setupPrice(ItemsModel items){
        int totals = 0;
        if (items.getTravelPeriodModelArrayList() != null && items.getTravelPeriodModelArrayList().size() != 0) {
            for (TravelPeriodModel tp : items.getTravelPeriodModelArrayList()) {
                if (tp.getAdultPrice() < tp.getAdultSpecialPrice()) {
                    totals = tp.getAdultPrice();
                } else {
                    totals = tp.getAdultSpecialPrice();
                }
            }
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###.-", symbols);
        String price  = null;

        ExchangeRate exchangeRate = new ExchangeRate();
        TextView txt_exchange_rate_id = findViewById(R.id.txt_exchange_rate_id);
        if (rateModel != null) {
            String currency = getSharedPreferences("PREF_APP_CURRENCY", Context.MODE_PRIVATE).getString("APP_CURRENCY", "THB");
            switch (currency) {
                case "THB":
                    price = decimalFormat.format(totals);
                    txt_exchange_rate_id.setText(currency);
                    txt_price.setText(price);
                    txt_price_booking.setText(price);
                    txt_currency.setText(currency);
                    break;
                case "USD":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateUSD()));
                    txt_exchange_rate_id.setText(currency);
                    txt_price.setText(price);
                    txt_price_booking.setText(price);
                    txt_currency.setText(currency);
                    break;
                case "CNY":
                    price = decimalFormat.format(exchangeRate.getExchangeRateUSD(totals,rateModel.getRateCNY()));
                    txt_exchange_rate_id.setText(currency);
                    txt_price.setText(price);
                    txt_price_booking.setText(price);
                    txt_currency.setText(currency);
                    break;
            }
        }else {
            price = decimalFormat.format(totals);
            txt_price.setText(price);
            txt_price_booking.setText(price);
        }

    }


    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
