package com.ipanda.lanexangtourism.items_view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.Activity.ProgramTourCreateActivity;
import com.ipanda.lanexangtourism.Adapter.CommentRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.DatesTripAdapterRecyclerView;
import com.ipanda.lanexangtourism.Adapter.DatesTripItemsTitleTextAdapterRecyclerView;
import com.ipanda.lanexangtourism.BuildConfig;
import com.ipanda.lanexangtourism.DialogFragment.CommentDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.ViewProfileDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ReviewClickListener;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.CommentAsyncTask;
import com.ipanda.lanexangtourism.asynctask.GetUserProfileAsyncTask;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ProgramTourViewAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.CommentCallBack;
import com.ipanda.lanexangtourism.interface_callback.GetUserProfileCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProgramTourViewCallBack;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class ViewProgramTourActivity extends AppCompatActivity implements View.OnClickListener , ProgramTourViewCallBack , CommentCallBack, ReviewClickListener , GetUserProfileCallBack {

    //variables

    private Toolbar mToolbar;

    private ProgramTourModel tourModel;

    private RecyclerView recycler_days;

    private DatesTripAdapterRecyclerView datesTripAdapter;

    private RecyclerView recycler_tour_details;

    private DatesTripItemsTitleTextAdapterRecyclerView datesTripItemsAdapter;

    private RecyclerView recycler_comment;

    private CommentRecyclerViewAdapter commentAdapter;

    private ImageView img_cover_program_tour, img_profile, img_share, img_like, img_bookmarks;

    private TextView txt_title_program_tour, txt_time_stamp_program_tour, txt_name_person, txt_count_like, txt_count_bookmarks, txt_interesting_things;

    private ChangeLanguageLocale languageLocale;

    private RelativeLayout rl_official;

    private String url = "",textComment = null;

    private String userId ="";

    private Button btn_create_program_tour;

    private ImageView img_user_profile;

    private TextView txt_count_comment;

    private TextView txt_category_program_tour;

    private TextView txt_sub_category_program_tour;

    private EditText edt_comment;

    private RelativeLayout ry_send_comment;

    private ArrayList<ReviewModel> reviewModelArrayList;

    private boolean isExpandable = false;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    private ProgramTourModel getIntentProgramTour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_program_tour);

        setUpContentView();

        setOnClickEvent();

        //setup Toolbar
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable drawable = getResources().getDrawable(R.drawable.ic_back_light);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Drawable newDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 45, 70, true));
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setHomeAsUpIndicator(newDrawable);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }


        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        isModeOnline = getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);

        if (getIntent() != null){
            getIntentProgramTour = getIntent().getParcelableExtra("PROGRAM_TOUR");

            if (userId != null){
                url = getString(R.string.app_api_ip)+ "dasta_thailand/api/mobile/user/ProgramTour?programTour_id="+getIntentProgramTour.getProgramTourId()+"&user_id="+userId;
            }else {
                url = getString(R.string.app_api_ip)+ "dasta_thailand/api/mobile/user/ProgramTour?programTour_id="+getIntentProgramTour.getProgramTourId();
            }
        }

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();



        //set on Click Event
        img_share.setOnClickListener(this);
        img_like.setOnClickListener(this);
        img_bookmarks.setOnClickListener(this);
        ry_send_comment.setOnClickListener(this);
        btn_create_program_tour.setOnClickListener(this);

        if (isModeOnline) {
            new ProgramTourViewAsyncTask(this).execute(url);
            setImageProfileURLComment();
        }else {
            getVersion = getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
            if (getVersion != 0) {
                mDatabase = new DatabaseHelper(this, getVersion);
                mManager = new DatabaseManager(this, mDatabase);
                setItemsOfflineMode();
            }else {
                alertDialog();
            }
        }
    }

    private void setItemsOfflineMode() {
        if (getIntentProgramTour != null){
            tourModel = mManager.getItemsProgramToursById(getIntentProgramTour.getProgramTourId());
        }
        if (tourModel != null) {
            setContentProgramTour();
            getComment();
        }
        img_cover_program_tour.setImageResource(R.drawable.offline_mode);
    }

    private void setImageProfileURLComment() {
        String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        String url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/User/"+userId;
        if (userId != null){
            new GetUserProfileAsyncTask(this).execute(url);
        }
    }

    private void setContentProgramTour() {
        String paths = getString(R.string.app_api_ip) + "dasta_thailand/assets/img/uploadfile/";
        Picasso.get().load(paths + tourModel.getCoverItemsModel().getCoverPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_cover_program_tour);

        switch (languageLocale.getLanguage()) {
            case "th":
                txt_title_program_tour.setText(tourModel.getProgramTourNameTH());
                txt_interesting_things.setText(tourModel.getInterestingThingsTH());
                txt_category_program_tour.setText(tourModel.getCategoryModel().getCategoryTH()+" : ");
                txt_sub_category_program_tour.setText(tourModel.getSubCategoryModel().getCategoryTH());
                break;
            case "en":
                txt_title_program_tour.setText(tourModel.getProgramTourNameEN());
                txt_interesting_things.setText(tourModel.getInterestingThingsEN());
                txt_category_program_tour.setText(tourModel.getCategoryModel().getCategoryEN()+" : ");
                txt_sub_category_program_tour.setText(tourModel.getSubCategoryModel().getCategoryEN());
                break;
            case "lo":
                txt_title_program_tour.setText(tourModel.getProgramTourNameLO());
                txt_interesting_things.setText(tourModel.getInterestingThingsLO());
                txt_category_program_tour.setText(tourModel.getCategoryModel().getCategoryLO()+" : ");
                txt_sub_category_program_tour.setText(tourModel.getSubCategoryModel().getCategoryLO());
                break;
            case "zh":
                txt_title_program_tour.setText(tourModel.getProgramTourNameZH());
                txt_interesting_things.setText(tourModel.getInterestingThingsZH());
                txt_category_program_tour.setText(tourModel.getCategoryModel().getCategoryZH()+" : ");
                txt_sub_category_program_tour.setText(tourModel.getSubCategoryModel().getCategoryZH());
                break;
        }

        txt_interesting_things.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isExpandable){
                    isExpandable = false;
                    txt_interesting_things.setMaxLines(2);
                }else {
                    isExpandable = true;
                    txt_interesting_things.setMaxLines(Integer.MAX_VALUE);
                    txt_interesting_things.setEllipsize(null);
                }

            }
        });

        Picasso.get().load(tourModel.getUserModel().getProfilePicUrl()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_profile);
        txt_name_person.setText(tourModel.getUserModel().getFirstName()+" "+tourModel.getUserModel().getLastName());

        if (tourModel.isOfficial()){
            rl_official.setVisibility(View.VISIBLE);
        }else {
            rl_official.setVisibility(View.GONE);
        }

        txt_count_like.setText(getString(R.string.text_like)+" "+tourModel.getLikesModel().getLikes());
        if (tourModel.getLikesModel().isLikeState()){
            img_like.setImageResource(R.drawable.ic_like_red);
        }else {
            img_like.setImageResource(R.drawable.ic_like);
        }

        if (tourModel.getCountView() != null && !tourModel.getCountView().equals("")){
            txt_count_bookmarks.setText(getString(R.string.text_read)+" "+tourModel.getCountView());
        }else {
            txt_count_bookmarks.setText(getString(R.string.text_read)+" "+0);
        }


        datesTripAdapter = new DatesTripAdapterRecyclerView(this, tourModel.getDatesTripModelArrayList(), getSupportFragmentManager(), languageLocale.getLanguage());
        recycler_days.setLayoutManager(new LinearLayoutManager(this));
        recycler_days.setAdapter(datesTripAdapter);

        datesTripItemsAdapter = new DatesTripItemsTitleTextAdapterRecyclerView(this, tourModel.getDatesTripModelArrayList(), languageLocale.getLanguage());
        recycler_tour_details.setLayoutManager(new LinearLayoutManager(this));
        recycler_tour_details.setAdapter(datesTripItemsAdapter);

        setContView();
    }

    private void setContView() {
        HttpCall httpCallPost = new HttpCall();
        httpCallPost.setMethodType(HttpCall.POST);
        httpCallPost.setUrl(getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ProgramTour/updateProgramTourReadCount");
        HashMap<String,String> paramsPost = new HashMap<>();
        paramsPost.put("programTour_id", String.valueOf(tourModel.getProgramTourId()));
        httpCallPost.setParams(paramsPost);
        new HttpPostRequestAsyncTask().execute(httpCallPost);
    }


    private void setUpContentView() {
        mToolbar = findViewById(R.id.toolbar);
        btn_create_program_tour = findViewById(R.id.btn_create_program_tour);
        img_cover_program_tour = findViewById(R.id.img_cover_program_tour);
        img_profile = findViewById(R.id.img_profile);
        rl_official = findViewById(R.id.rl_official);
        img_share = findViewById(R.id.img_share);
        img_like = findViewById(R.id.img_like);
        img_bookmarks = findViewById(R.id.img_bookmarks);
        txt_title_program_tour = findViewById(R.id.txt_title_program_tour);
        txt_time_stamp_program_tour = findViewById(R.id.txt_time_stamp_program_tour);
        txt_name_person = findViewById(R.id.txt_name_person);
        txt_count_like = findViewById(R.id.txt_count_like);
        txt_count_bookmarks = findViewById(R.id.txt_count_bookmarks);
        txt_interesting_things = findViewById(R.id.txt_interesting_things);
        recycler_days = findViewById(R.id.recycler_days);
        recycler_tour_details = findViewById(R.id.recycler_tour_details);
        edt_comment = findViewById(R.id.edt_comment);
        txt_count_comment = findViewById(R.id.txt_count_comment);
        ry_send_comment = findViewById(R.id.ry_send_comment);
        recycler_comment = findViewById(R.id.recycler_comment);
        img_user_profile = findViewById(R.id.img_user_profile);
        txt_category_program_tour = findViewById(R.id.txt_category_program_tour);
        txt_sub_category_program_tour = findViewById(R.id.txt_sub_category_program_tour);

    }

    private void setOnClickEvent() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_share:
                onClickShareProgramTour(view);
                break;
            case R.id.img_like:
                onClickLikeProgramTour();
                break;
            case R.id.img_bookmarks:
                onClickBookmarksProgramTour();
                break;
            case R.id.ry_send_comment:
                onClickSendComment();
                break;
            case R.id.btn_create_program_tour:
                if (isModeOnline) {
                    startActivity(new Intent(ViewProgramTourActivity.this, ProgramTourCreateActivity.class));
                }else {
                    Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean getTextComment() {
        if (TextUtils.isEmpty(edt_comment.getText().toString())){
            Toast.makeText(this, "Empty field not allowed!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void onClickSendComment() {
        if (isModeOnline) {
            String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Review");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("review_rating", String.valueOf(0));
            paramsPost.put("review_text", edt_comment.getText().toString());
            paramsPost.put("ProgramTour_programTour_id", String.valueOf(tourModel.getProgramTourId()));
            httpCallPost.setParams(paramsPost);

            if (getTextComment()) {
                new HttpPostRequestAsyncTask().execute(httpCallPost);
                getComment();
                edt_comment.clearFocus();
                edt_comment.clearComposingText();
                edt_comment.getText().clear();
                Toast.makeText(this, "comment..", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
        }

    }

    private void onClickBookmarksProgramTour() {
    }

    private void onClickLikeProgramTour() {
        if (isModeOnline) {
            String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Likes");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("ProgramTour_programTour_id", String.valueOf(tourModel.getProgramTourId()));
            httpCallPost.setParams(paramsPost);
            if (userId != null) {
                new HttpPostRequestAsyncTask().execute(httpCallPost);
                finish();
                startActivity(getIntent());
            } else {

            }
        }


    }

    private void onClickShareProgramTour(View view) {
        PopupMenu menu = new PopupMenu(this,view);
        menu.getMenuInflater().inflate(R.menu.menu_share,menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_share:
                        if (isModeOnline) {
                            try {
                                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                shareIntent.setType("text/plain");
                                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "LANEXANG INFO");
                                String shareMessage = "\n Let me recommend you this application \n \n ";
                                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "";
                                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                                startActivity(Intent.createChooser(shareIntent, "choose one"));
                            } catch (Exception e) {
                                Log.e("ProgramTour", e.toString());
                            }
                        }
                        break;
                }
                return true;
            }
        });
        menu.show();
    }


    private void getComment(){
        String url = getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/Review?programTour_id="+tourModel.getProgramTourId();
        new CommentAsyncTask(this).execute(url);
    }

    private void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("No support information");
        builder.setMessage("Please download data for offline use.");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerGetUserProfile(UserModel userModels) {
        if (userModels != null){
            Log.e("check data",userModels+"");
            Picasso.get().load(userModels.getProfilePicUrl()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_user_profile);
        }
    }

    @Override
    public void onRequestCompleteListener(ProgramTourModel programTourModel) {
        if (programTourModel != null){
            tourModel = programTourModel;
            setContentProgramTour();
            getComment();
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onPreCallServiceComment() {

    }

    @Override
    public void onCallServiceComment() {

    }

    @Override
    public void onRequestCompleteComment(ArrayList<ReviewModel> reviewArrayList) {
        if (reviewArrayList != null && reviewArrayList.size() != 0){
            this.reviewModelArrayList = reviewArrayList;
            Log.e("check data",reviewArrayList+"");
            int count = reviewModelArrayList.size();
            txt_count_comment.setText(count+"");
            commentAdapter = new CommentRecyclerViewAdapter(this,reviewModelArrayList,languageLocale.getLanguage(),this,false);
            recycler_comment.setLayoutManager(new LinearLayoutManager(this));
            recycler_comment.setAdapter(commentAdapter);
        }else {

        }
    }

    @Override
    public void onRequestFailedComment(String result) {

    }

    @Override
    public void itemClicked(View view, ReviewModel reviewModel) {
        if (isModeOnline) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("REVIEW_MODEL", reviewModel);
            bundle.putParcelable("PROGRAM_TOUR_MODEL", tourModel);
            DialogFragment fragment = CommentDialogFragment.newInstance();
            fragment.setArguments(bundle);
            fragment.show(getSupportFragmentManager(), "Activity");
        }
    }

    @Override
    public void itemClickedDelete(View view, ReviewModel reviewModel, int position) {
        if (isModeOnline) {
            String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Review/delete");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("review_id", String.valueOf(reviewModel.getReviewId()));
            httpCallPost.setParams(paramsPost);
            new AlertDialog.Builder(this)
                    .setTitle("คุณต้องการลบข้อมูลหรือไม่")
                    .setMessage("....")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new HttpPostRequestAsyncTask().execute(httpCallPost);
                            reviewModelArrayList.remove(position);
                            commentAdapter.notifyItemRemoved(position);
                            commentAdapter.notifyDataSetChanged();
                            getComment();

                            int count = reviewModelArrayList.size();
                            txt_count_comment.setText(count + "");

                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .create().show();
        }
    }

    @Override
    public void itemClickedProfile(View view, ReviewModel reviewModel) {
        if (isModeOnline) {
            Bundle bundle = new Bundle();
            bundle.putString("USER_ID", String.valueOf(reviewModel.getUserModel().getUserId()));
            DialogFragment dialogFragment = ViewProfileDialogFragment.newInstance();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getSupportFragmentManager(), "ViewTourismActivity");
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
