package com.ipanda.lanexangtourism.items_view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.ipanda.lanexangtourism.Activity.CameraActivity;
import com.ipanda.lanexangtourism.Activity.CameraTwoActivity;
import com.ipanda.lanexangtourism.Activity.ChooseDateHotelMainActivity;
import com.ipanda.lanexangtourism.Activity.EventTicketMapsActivity;
import com.ipanda.lanexangtourism.Activity.HotelDetailsActivity;
import com.ipanda.lanexangtourism.Activity.LoginActivity;
import com.ipanda.lanexangtourism.Activity.MainActivity;
import com.ipanda.lanexangtourism.Activity.NavigateActivity;
import com.ipanda.lanexangtourism.Activity.ProgramTourActivity;
import com.ipanda.lanexangtourism.Activity.ProgramTourCreateActivity;
import com.ipanda.lanexangtourism.Adapter.CoverViewPagerAdapter;
import com.ipanda.lanexangtourism.Adapter.FacilitiesRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.GuaranteeRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.InterestingRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.PlacesRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ProductHotMenuRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ProductPopularRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ProgramTourMainRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.ReviewsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.RoomRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Adapter.TravellingRecyclerViewAdapter;
import com.ipanda.lanexangtourism.BuildConfig;
import com.ipanda.lanexangtourism.ClickListener.ItemImageClickListener;
import com.ipanda.lanexangtourism.DialogFragment.OrderDetailsDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.ReviewAllDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.ReviewDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.SlidingHotMenuDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.SlidingHotelDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.SlidingImageDialogFragment;
import com.ipanda.lanexangtourism.DialogFragment.ViewProfileDialogFragment;
import com.ipanda.lanexangtourism.Helper.ChangeLanguageLocale;
import com.ipanda.lanexangtourism.Interface_click.ProgramTourClickListener;
import com.ipanda.lanexangtourism.Interface_click.PurchaseOrder;
import com.ipanda.lanexangtourism.Interface_click.ReviewClickListener;
import com.ipanda.lanexangtourism.MenuFragment.HomeFragment;
import com.ipanda.lanexangtourism.Model.BookingModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.ProgramTourModel;
import com.ipanda.lanexangtourism.Model.RateModel;
import com.ipanda.lanexangtourism.Model.RatingStarModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.NavigationFragment.ListViewNavigationFragment;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.asynctask.ExchangeRateAsyncTask;
import com.ipanda.lanexangtourism.asynctask.HttpPostRequestAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsContactAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsHotelAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsRestaurantAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsShoppingAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsTourismAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ItemsViewAsyncTask;
import com.ipanda.lanexangtourism.asynctask.ProgramTourNearAsyncTask;
import com.ipanda.lanexangtourism.database.DatabaseHelper;
import com.ipanda.lanexangtourism.database.DatabaseManager;
import com.ipanda.lanexangtourism.fragment.ImageFragment;
import com.ipanda.lanexangtourism.fragment.VideoFragment;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.BookingRoomSelected;
import com.ipanda.lanexangtourism.interface_callback.ExchangeRateCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsViewCallBack;
import com.ipanda.lanexangtourism.interface_callback.ProgramTourNearCallBack;
import com.ipanda.lanexangtourism.utils.SetMarkerGoogleUtils;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class ViewTourismActivity extends AppCompatActivity implements ItemImageClickListener, OnMapReadyCallback, ReviewClickListener, ItemsViewCallBack, View.OnClickListener
        , ProgramTourNearCallBack , ProgramTourClickListener , BookingRoomSelected , PurchaseOrder , ExchangeRateCallBack {


    //Variables
    private static String TAG = ViewTourismActivity.class.getSimpleName();

    private Toolbar mToolbar;

    private AppBarLayout appBarLayout;

    private DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle toggle;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    private ChangeLanguageLocale languageLocale;

    private CardView showToolbar;

    private ViewPager mViewPager;

    private MapView mapView;

    private GoogleMap googleMap;

    private Marker marker;

    private RecyclerView recycler_interesting, recycler_travelling, recycler_hot_menu, recycler_guarantee, recycler_popular_product
            , recycler_facilities,recycler_ex_hotel,recycler_reviews;

    private InterestingRecyclerViewAdapter interestingAdapter;

    private TravellingRecyclerViewAdapter travellingViewAdapter;

    private ProductHotMenuRecyclerViewAdapter hotMenuAdapter;

    private GuaranteeRecyclerViewAdapter guaranteeViewAdapter;

    private ProductPopularRecyclerViewAdapter popularAdapter;

    private FacilitiesRecyclerViewAdapter facilitiesAdapter;

    private RoomRecyclerViewAdapter roomAdapter;

    private ReviewsRecyclerViewAdapter reviewsAdapter;

    private TextView btn_create_program_tour;

    private CardView show_hot_menu_view, show_guarantee_view, show_popular_product_view ,show_facilities_view,show_exHotel_view;

    private ImageView ar_back_toolbar_id, ar_app_toolbar_id, ar_menu_toolbar_id;

    private ImageView img_cover_menu_id, img_cover_back_id,img_items_bookmarks;

    private ImageView img_item_found_one_id, img_item_found_two_id, img_item_found_three_id, img_item_found_gradient_id;

    private TextView ar_title_toolbar_id;

    private TextView txt_item_topic_id, txt_see_more_program_tour;

    private TextView  txt_item_subcategory_id, txt_item_open_close_id, txt_item_kg_id;

    private TextView txt_item_found_id, txt_item_found_detail_id, txt_item_found_count_id;

    private TextView txt_contact_address_id, txt_contact_phone_id, txt_contact_email_id, txt_contact_line_id, txt_contact_facebook_id;

    private TextView txt_interesting_detail_id, txt_travelling_detail_id;

    private TextView txt_count_review_id,txt_menu_item_id;

    private TextView txt_items_check_in_out_id;

    private CardView onClickGiveRating;

    private String paths = "";

    private int category;

    private ConstraintLayout con_item_navigation_id, con_show_footer, con_show_item_hotel, con_show_item_detail;

    private RelativeLayout rl_bg_star,rl_bg_star_review;

    private ProgressBar progress_five_count_id, progress_four_count_id, progress_three_count_id, progress_two_count_id, progress_one_count_id;

    private TextView txt_five_count_id, txt_four_count_id, txt_three_count_id, txt_two_count_id, txt_one_count_id;

    private TextView txt_all_reviews,txt_items_rating,txt_items_ratings;

    private TextView txt_popular_total_id;

    private RecyclerView recycler_program_tour;

    private ItemsModel itemsModel;

    private ItemsModel itemsModelGetIntent;

    private String userId;

    private RatingStarModel ratingStarModel;

    public static int navItemIndex = 0;

    private Bundle savedInstanceState;

    private BookingModel bookingModel = new BookingModel();

    private ArrayList<ProductModel> productList = new ArrayList<>();

    private CoordinatorLayout itemsViews;

    private CircularSticksLoader loaderItems;

    private ImageView btnCloseBurger;

    private ImageView img_load_field_id;

    private TextView txt_load_field_id;

    private TextView txtStatusOpen;

    private TextView txtOpenEveryday;

    private String url = "";

    private RateModel rateModel;

    private LayoutTransition transition;

    //Location
    private static final int PERMISSION_ID = 1;
    private FusedLocationProviderClient client;
    private Location locationResult;

    private Boolean isModeOnline;

    private DatabaseHelper mDatabase;

    private DatabaseManager mManager;

    private int getVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_tourism);
        this.savedInstanceState = savedInstanceState;
        //views
//        mToolbar = findViewById(R.id.toolbar_view_items);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        collapsingToolbarLayout = findViewById(R.id.CollapsingToolbar_Item);
        appBarLayout = findViewById(R.id.AppBarLayout_Item);
        showToolbar = findViewById(R.id.show_toolbar_item);
        mViewPager = findViewById(R.id.cover_viewpager_id);
        btn_create_program_tour = findViewById(R.id.btn_create_program_tour);
        mapView = findViewById(R.id.mapView);
        onClickGiveRating = findViewById(R.id.onClickGiveRating);
        txt_popular_total_id = findViewById(R.id.txt_popular_total_id);
        txtStatusOpen = findViewById(R.id.txt_status_open);
        txtOpenEveryday = findViewById(R.id.txt_open_everyday);
        btnCloseBurger = findViewById(R.id.btn_burger_close);

        setContentItemsView();

        languageLocale = new ChangeLanguageLocale(this);
        languageLocale.getLoadLocale();

        client = LocationServices.getFusedLocationProviderClient(this);
        getPermissionLocation();

        isModeOnline = getSharedPreferences("PREF_APP_MODE_ONLINE", Context.MODE_PRIVATE).getBoolean("APP_MODE_ONLINE", true);

        img_cover_menu_id.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.END);
                //set on click drawer menu
                drawerMenu();
            }
        });

        ar_menu_toolbar_id.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.END);
                //set on click drawer menu
                drawerMenu();
            }
        });

        btnCloseBurger.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(Gravity.END);
            }
        });

        btn_create_program_tour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isModeOnline) {
                    startActivity(new Intent(ViewTourismActivity.this, ProgramTourCreateActivity.class));
                }else {
                    Toast.makeText(ViewTourismActivity.this, "Offline Mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        onClickGiveRating.setOnClickListener(this);
        txt_see_more_program_tour.setOnClickListener(this);
        img_cover_back_id.setOnClickListener(this);


        //setup ActionBarDrawerToggle
        toggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        paths = getString(R.string.app_api_ip)+"dasta_thailand/assets/img/uploadfile/";
        userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        //set changed collapsing
        setChangedCollapsing();
        //setup transition
        transition = new LayoutTransition();

        String urlRate = getResources().getString(R.string.app_api_ip)+"dasta_thailand/api/mobile/user/ExchangeRate";
        if (getIntent() != null){
            itemsModelGetIntent = getIntent().getParcelableExtra("ITEMS_MODEL");
            category = getIntent().getIntExtra("CATEGORY",0);
            bookingModel = getIntent().getParcelableExtra("BOOKING");
        }

        con_item_navigation_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemsModel != null){
                    Uri gmmIntentUri = Uri.parse("google.navigation:q="+itemsModel.getLatitude()+","+itemsModel.getLongitude()+"&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            }
        });

        setupMapsView();

        if (isModeOnline){

            new ExchangeRateAsyncTask(this).execute(urlRate);

            if (itemsModelGetIntent != null){
                if (userId != null) {
                    url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?items_id=" + itemsModelGetIntent.getItemsId() + "&user_id=" + userId;
                }else {
                    url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?items_id=" + itemsModelGetIntent.getItemsId() + "&user_id=";
                }
            }else {
                Toast.makeText(this, "Failed Items...", Toast.LENGTH_SHORT).show();
            }

            loadAsyncTask();

        }else {
            getVersion = getSharedPreferences("PREF_APP_VERSION_DB", Context.MODE_PRIVATE).getInt("PREF_APP_VERSION_DB",0);
            if (getVersion != 0) {
                mDatabase = new DatabaseHelper(this, getVersion);
                mManager = new DatabaseManager(this, mDatabase);
                if (itemsModelGetIntent != null){
                    this.itemsModel = mManager.getItemsById(itemsModelGetIntent.getItemsId(),itemsModelGetIntent.getMenuItemModel().getMenuItemId());

                    try {
                        ImageView imgModeOffline = findViewById(R.id.img_mode_offline);
                        imgModeOffline.setImageResource(R.drawable.offline_mode);
                        imgModeOffline.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        Log.e(TAG, e.getMessage().toString());
                    }

                    //check category show view
                    checkCategoryShowView(category);

                    //setup data detail
                    setDataDetail();

                    //setup data things to meet
                    setDataThingToMeet();

                    //setup data detail interesting
                    setDataDetailInteresting();

                    //setup data travelling
                    setDataTravelling();

                    //setup data contact
                    setDataContact();

                    //setup data rating
                    setDataRating();

                    //setup data reviews
                    setDataReviews();

                    //setup data hot menu product
                    setDataHotMenuProduct();

                    //setup data guarantee
                    setDataGuarantee();

                    //setup data popular product
                    setDataPopularProduct();

                    //setup data facilities
                    setDataFacilities();

                    //setup data example hotel
                    setDataExampleHotel();

                }
            }
        }


    }

    @SuppressLint("NewApi")
    private void getPermissionLocation(){
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            getCurrentLocation();
        }else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_ID);
        }
    }


    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            client.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location != null){
                        locationResult = location;
                        if (itemsModelGetIntent != null) {
                            if (userId != null) {
                                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?items_id=" + itemsModelGetIntent.getItemsId() + "&user_id=" + userId + "&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                            } else {
                                url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items?items_id=" + itemsModelGetIntent.getItemsId() + "&user_id=&latitude=" + locationResult.getLatitude() + "&longitude=" + locationResult.getLongitude();
                            }
                        }else {
                            Toast.makeText(ViewTourismActivity.this, "Failed Items...", Toast.LENGTH_SHORT).show();
                        }
                        loadAsyncTask();
                        setOnMapMarker();
                    }else {
                        LocationRequest locationRequest = new LocationRequest()
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setInterval(10000)
                                .setFastestInterval(1000)
                                .setNumUpdates(1);
                        LocationCallback locationCallback = new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                super.onLocationResult(locationResult);
                                Location location1 = locationResult.getLastLocation();
                                Toast.makeText(ViewTourismActivity.this, location1.getLatitude()+"  "+location1.getLongitude(), Toast.LENGTH_SHORT).show();
                            }
                        };
                        client.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper());
                    }
                }
            });
        }else {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    private void loadAsyncTask(){
        if (isModeOnline) {
            new ItemsViewAsyncTask(this).execute(url);
        }else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show();
            getCurrentLocation();
        }else {
            Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_drawer_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void setupMapsView() {
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this::onMapReady);
    }


    private void checkCategoryShowView(int category) {
        switch (category){
            case 2:
                con_show_item_detail.setVisibility(View.VISIBLE);
                con_show_footer.setVisibility(View.VISIBLE);
                rl_bg_star.setBackgroundResource(R.drawable.shape_star_tour);
                rl_bg_star_review.setBackgroundResource(R.drawable.shape_star_tour);
                break;
            case 3:
                show_hot_menu_view.setVisibility(View.VISIBLE);
                show_guarantee_view.setVisibility(View.VISIBLE);
                con_show_item_detail.setVisibility(View.VISIBLE);
                rl_bg_star.setBackgroundResource(R.drawable.shape_star_restaurant);
                rl_bg_star_review.setBackgroundResource(R.drawable.shape_star_restaurant);
                //setup data hot menu product
                setDataHotMenuProduct();
                //setup data guarantee
                setDataGuarantee();
                break;
            case 4:
                show_popular_product_view.setVisibility(View.VISIBLE);
                con_show_item_detail.setVisibility(View.VISIBLE);
                rl_bg_star.setBackgroundResource(R.drawable.shape_star_shopping);
                rl_bg_star_review.setBackgroundResource(R.drawable.shape_star_shopping);
                //setup data popular product
                setDataPopularProduct();
                break;
            case 5:
                con_show_item_hotel.setVisibility(View.VISIBLE);
                show_facilities_view.setVisibility(View.VISIBLE);
                show_exHotel_view.setVisibility(View.VISIBLE);
                con_show_item_detail.setVisibility(View.GONE);
                rl_bg_star.setBackgroundResource(R.drawable.shape_star_hotel);
                rl_bg_star_review.setBackgroundResource(R.drawable.shape_star_hotel);
                //setup data facilities
                setDataFacilities();
                //setup data example hotel
                setDataExampleHotel();
                if (bookingModel != null) {
                    txt_items_check_in_out_id.setText(bookingModel.getScheduleCheckIn());
                }
                break;
            case 6:
                con_show_item_detail.setVisibility(View.VISIBLE);
                rl_bg_star.setBackgroundResource(R.drawable.shape_star_contact);
                rl_bg_star_review.setBackgroundResource(R.drawable.shape_star_contact);
                break;
        }
    }

    private void setupViewPager(ViewPager mViewPager) {
        CoverViewPagerAdapter pagerAdapter = new CoverViewPagerAdapter(getSupportFragmentManager(),0);
        if (itemsModel.getCoverItemsModel().getCoverPaths() != null){
            Bundle bdImage = new Bundle();
            Fragment image = new ImageFragment();
            bdImage.putParcelable("CoverItemsModel",itemsModel.getCoverItemsModel());
            image.setArguments(bdImage);
            pagerAdapter.addFragment(image,"Image");
        }
        if (itemsModel.getCoverItemsModel().getCoverURL() != null){
            Bundle bdVideo = new Bundle();
            Fragment video = new VideoFragment();
            bdVideo.putParcelable("CoverItemsModel", itemsModel.getCoverItemsModel());
            video.setArguments(bdVideo);
            pagerAdapter.addFragment(video,"Video");
        }
        mViewPager.setAdapter(pagerAdapter);
    }

    private void setContentItemsView() {
        img_items_bookmarks = findViewById(R.id.img_items_bookmarks);
        ar_title_toolbar_id = findViewById(R.id.ar_title_toolbar_id);
        ar_back_toolbar_id = findViewById(R.id.ar_back_toolbar_id);
        ar_app_toolbar_id = findViewById(R.id.ar_app_toolbar_id);
        ar_menu_toolbar_id = findViewById(R.id.ar_menu_toolbar_id);
        img_cover_menu_id = findViewById(R.id.img_cover_menu_id);
        img_cover_back_id = findViewById(R.id.img_cover_back_id);
        txt_item_topic_id = findViewById(R.id.txt_item_topic_id);
        txt_item_subcategory_id = findViewById(R.id.txt_item_subcategory_id);
        txt_item_open_close_id = findViewById(R.id.txt_item_open_close_id);
        txt_item_kg_id = findViewById(R.id.txt_item_kg_id);
        txt_item_found_id = findViewById(R.id.txt_item_found_id);
        txt_item_found_detail_id = findViewById(R.id.txt_item_found_detail_id);
        txt_item_found_count_id = findViewById(R.id.txt_item_found_count_id);
        img_item_found_one_id = findViewById(R.id.img_item_found_one_id);
        img_item_found_two_id = findViewById(R.id.img_item_found_two_id);
        img_item_found_three_id = findViewById(R.id.img_item_found_three_id);
        img_item_found_gradient_id = findViewById(R.id.img_item_found_gradient_id);
        txt_contact_address_id = findViewById(R.id.txt_contact_address_id);
        txt_contact_phone_id = findViewById(R.id.txt_contact_phone_id);
        txt_contact_email_id = findViewById(R.id.txt_contact_email_id);
        txt_contact_line_id = findViewById(R.id.txt_contact_line_id);
        txt_contact_facebook_id = findViewById(R.id.txt_contact_facebook_id);
        recycler_interesting = findViewById(R.id.recycler_interesting);
        txt_interesting_detail_id = findViewById(R.id.txt_interesting_detail_id);
        recycler_travelling = findViewById(R.id.recycler_travelling);
        txt_travelling_detail_id = findViewById(R.id.txt_travelling_detail_id);
        show_hot_menu_view = findViewById(R.id.show_hot_menu_view);
        show_guarantee_view = findViewById(R.id.show_guarantee_view);
        show_popular_product_view = findViewById(R.id.show_popular_product_view);
        con_show_footer = findViewById(R.id.con_show_footer);
        con_show_item_hotel = findViewById(R.id.con_show_item_hotel);
        con_show_item_detail = findViewById(R.id.con_show_item_detail);
        show_facilities_view = findViewById(R.id.show_facilities_view);
        recycler_facilities = findViewById(R.id.recycler_facilities);
        show_exHotel_view = findViewById(R.id.show_exHotel_view);
        recycler_ex_hotel = findViewById(R.id.recycler_ex_hotel);
        recycler_hot_menu = findViewById(R.id.recycler_hot_menu);
        recycler_guarantee = findViewById(R.id.recycler_guarantee);
        recycler_popular_product = findViewById(R.id.recycler_popular_product);
        rl_bg_star = findViewById(R.id.rl_bg_star);
        txt_count_review_id = findViewById(R.id.txt_count_review_id);
        txt_menu_item_id = findViewById(R.id.txt_menu_item_id);
        con_item_navigation_id = findViewById(R.id.con_item_navigation_id);
        rl_bg_star_review = findViewById(R.id.rl_bg_star_review);
        progress_five_count_id = findViewById(R.id.progress_five_count_id);
        progress_four_count_id = findViewById(R.id.progress_four_count_id);
        progress_three_count_id = findViewById(R.id.progress_three_count_id);
        progress_two_count_id = findViewById(R.id.progress_two_count_id);
        progress_one_count_id = findViewById(R.id.progress_one_count_id);
        txt_five_count_id = findViewById(R.id.txt_five_count_id);
        txt_four_count_id = findViewById(R.id.txt_four_count_id);
        txt_three_count_id = findViewById(R.id.txt_three_count_id);
        txt_two_count_id = findViewById(R.id.txt_two_count_id);
        txt_one_count_id = findViewById(R.id.txt_one_count_id);
        txt_all_reviews = findViewById(R.id.txt_all_reviews);
        recycler_reviews = findViewById(R.id.recycler_reviews);
        txt_items_rating = findViewById(R.id.txt_items_rating);
        txt_items_ratings = findViewById(R.id.txt_items_ratings);
        txt_see_more_program_tour = findViewById(R.id.txt_see_more_program_tour);
        recycler_program_tour = findViewById(R.id.recycler_program_tour);
        txt_items_check_in_out_id = findViewById(R.id.txt_items_check_in_out_id);
        itemsViews = findViewById(R.id.con_items_view_id);
        loaderItems = findViewById(R.id.loader_items_id);
        img_load_field_id = findViewById(R.id.img_load_field_id);
        txt_load_field_id = findViewById(R.id.txt_load_field_id);

    }

    @Override
    public void onPreCallService() {

        itemsViews.setVisibility(View.GONE);
        loaderItems.setVisibility(View.VISIBLE);
        img_load_field_id.setVisibility(View.GONE);
        txt_load_field_id.setVisibility(View.GONE);

    }

    @Override
    public void onCallService() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestCompleteListener(ItemsModel itemsModel) {
        if (itemsModel != null){
            Log.e("check data", itemsModel+"");
            this.itemsModel = itemsModel;

            itemsViews.setVisibility(View.VISIBLE);
            loaderItems.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.GONE);
            txt_load_field_id.setVisibility(View.GONE);

            //check category show view
            checkCategoryShowView(category);

            //setup ViewPager
            setupViewPager(mViewPager);

            //setup data detail
            setDataDetail();

            //setup data things to meet
            setDataThingToMeet();

            //setup data detail interesting
            setDataDetailInteresting();

            //setup data travelling
            setDataTravelling();

            //setup data contact
            setDataContact();

            //setup data rating
            setDataRating();

            //setup data reviews
            setDataReviews();

            //setup data date
            setDataDate();
            mapView.setClickable(false);

            setOnMapMarker();

            if (itemsModel.getBookMarksModel().isBookmarksState()){
                img_items_bookmarks.setImageResource(R.drawable.ic_bookmark_red);
            }else {
                img_items_bookmarks.setImageResource(R.drawable.ic_bookmark_inactive);
            }


            txt_all_reviews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isModeOnline) {
                        Bundle bundle = new Bundle();
                        DialogFragment dialogFragment = ReviewAllDialogFragment.newInstance();
                        bundle.putParcelable("ITEMS_MODEL", itemsModel);
                        dialogFragment.setArguments(bundle);
                        dialogFragment.show(getSupportFragmentManager(), "ViewTourismActivity");
                    }
                }
            });

            if (isModeOnline) {
                userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
                String url = "";
                if (userId != null) {
                    url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/tourist_program_talks_about_this_place?items_id=" + itemsModel.getItemsId() + "&user_id=" + userId;
                } else {
                    url = getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Items/tourist_program_talks_about_this_place?items_id=" + itemsModel.getItemsId() + "&user_id=";
                }
                new ProgramTourNearAsyncTask(this).execute(url);
            }

            con_show_item_hotel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int REQ_CODE = 1009;
                    Intent intent = new Intent(ViewTourismActivity.this, ChooseDateHotelMainActivity.class);
                    if (isModeOnline) {
                        startActivityForResult(intent, REQ_CODE);
                    }
                }
            });

        }else {
            itemsViews.setVisibility(View.GONE);
            loaderItems.setVisibility(View.GONE);
            img_load_field_id.setVisibility(View.VISIBLE);
            txt_load_field_id.setVisibility(View.VISIBLE);
        }
    }

    private void setOnMapMarker() {
        if (itemsModel != null) {
            if (googleMap != null) {
                SetMarkerGoogleUtils googleUtils = new SetMarkerGoogleUtils(this, googleMap, marker, itemsModel, languageLocale.getLanguage());
                googleUtils.setShowIconMarkerMapsSingle();
                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        Intent intent = new Intent(ViewTourismActivity.this, NavigateActivity.class);
                        intent.putExtra("ITEMS_MODEL", itemsModel);
                        intent.putExtra("CATEGORY_ID", category);
                        startActivity(intent);
                    }
                });
            }
        }
    }

    private void setDataDate() {
        if (itemsModel.getPeriodDayModel() != null && itemsModel.getPeriodTimeModel() != null) {
            if (itemsModel.getPeriodDayModel().getPeriodOpenId() == itemsModel.getPeriodDayModel().getPeriodCloseId()) {
                switch (languageLocale.getLanguage()) {
                    case "th":
                        txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenTH());
                        break;
                    case "en":
                        txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenEN());
                        break;
                    case "lo":
                        txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenLO());
                        break;
                    case "zh":
                        txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenZH());
                        break;
                }
            } else {
                switch (languageLocale.getLanguage()) {
                    case "th":
                        txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenTH() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseTH());
                        break;
                    case "en":
                        txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenEN() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseEN());
                        break;
                    case "lo":
                       txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenLO() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseLO());
                        break;
                    case "zh":
                        txtOpenEveryday.setText(itemsModel.getPeriodDayModel().getPeriodOpenZH() + " - " + itemsModel.getPeriodDayModel().getPeriodCloseZH());
                        break;
                }
            }
        }
        try {
            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
            Date currentTime  = parser.parse(getCurrentTime());
            Date timeOpen = parser.parse(itemsModel.getTimeOpen());
            Date timeClosed = parser.parse(itemsModel.getTimeClose());

            int diffOpen = currentTime.compareTo(timeOpen);
            int diffClosed = currentTime.compareTo(timeClosed);

            if(diffOpen >= 0 && diffClosed <= 0) {
               txtStatusOpen.setText(getString(R.string.text_open));
               txtStatusOpen.setTextColor(getResources().getColor(R.color.colorFontOpen));
            } else if (diffOpen <= 0 && diffClosed >= 0) {
               txtStatusOpen.setText(getString(R.string.text_closed));
               txtStatusOpen.setTextColor(getResources().getColor(R.color.status_refuse));
            } else {
               txtStatusOpen.setText(getString(R.string.text_closed));
               txtStatusOpen.setTextColor(getResources().getColor(R.color.status_refuse));
            }

        }catch (ParseException ex){
            Log.e("error diff", ex.toString());
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    private void setDataDetail() {
        if (itemsModel.getSubCategoryModelArrayList() != null && itemsModel.getSubCategoryModelArrayList().size() != 0) {
            switch (languageLocale.getLanguage()) {
                case "th":
                    ar_title_toolbar_id.setText(itemsModel.getTopicTH());
                    txt_item_topic_id.setText(itemsModel.getTopicTH());
                    txt_item_subcategory_id.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryTH());
                    txt_item_kg_id.setText(itemsModel.getDistance() + getResources().getString(R.string.text_km));
                    break;
                case "en":
                    ar_title_toolbar_id.setText(itemsModel.getTopicEN());
                    txt_item_topic_id.setText(itemsModel.getTopicEN());
                    txt_item_subcategory_id.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryEN());
                    txt_item_kg_id.setText(itemsModel.getDistance() + getResources().getString(R.string.text_km));
                    break;
                case "lo":
                    ar_title_toolbar_id.setText(itemsModel.getTopicLO());
                    txt_item_topic_id.setText(itemsModel.getTopicLO());
                    txt_item_subcategory_id.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryLO());
                    txt_item_kg_id.setText(itemsModel.getDistance() + getResources().getString(R.string.text_km));
                    break;
                case "zh":
                    ar_title_toolbar_id.setText(itemsModel.getTopicZH());
                    txt_item_topic_id.setText(itemsModel.getTopicZH());
                    txt_item_subcategory_id.setText(itemsModel.getSubCategoryModelArrayList().get(0).getCategoryZH());
                    txt_item_kg_id.setText(itemsModel.getDistance() + getResources().getString(R.string.text_km));
                    break;
            }
        }
        txt_item_open_close_id.setText(itemsModel.getTimeOpen()+"-"+itemsModel.getTimeClose());
    }

    private void setDataThingToMeet() {
        Bundle bundle = new Bundle();
        if (itemsModel.getItemsDetailModelArrayList() != null && itemsModel.getItemsDetailModelArrayList().size() != 0) {
            switch (languageLocale.getLanguage()) {
                case "th":
                    txt_item_found_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(0).getDetailTH());
                    break;
                case "en":
                    txt_item_found_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(0).getDetailEN());
                    break;
                case "lo":
                    txt_item_found_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(0).getDetailLO());
                    break;
                case "zh":
                    txt_item_found_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(0).getDetailZH());
                    break;
            }

            if (itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels() != null && itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().size() != 0) {
                if (itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().size() == 1) {
                    Picasso.get().load(paths + itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().get(0).getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_item_found_one_id);
                    img_item_found_two_id.setVisibility(View.GONE);
                    img_item_found_three_id.setVisibility(View.GONE);
                } else if (itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().size() == 2) {
                    Picasso.get().load(paths + itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().get(0).getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_item_found_one_id);
                    Picasso.get().load(paths + itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().get(1).getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_item_found_two_id);
                    img_item_found_three_id.setVisibility(View.GONE);
                } else if (itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().size() == 3) {
                    Picasso.get().load(paths + itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().get(0).getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_item_found_one_id);
                    Picasso.get().load(paths + itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().get(1).getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_item_found_two_id);
                    Picasso.get().load(paths + itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().get(2).getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_item_found_three_id);
                } else {
                    int count = itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().size();
                    Picasso.get().load(paths + itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().get(0).getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_item_found_one_id);
                    Picasso.get().load(paths + itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().get(1).getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_item_found_two_id);
                    Picasso.get().load(paths + itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels().get(2).getPhotoPaths()).placeholder(R.drawable.img_placeholder).error(R.drawable.img_laceholder_error).into(img_item_found_three_id);
                    img_item_found_gradient_id.setVisibility(View.VISIBLE);
                    txt_item_found_count_id.setVisibility(View.VISIBLE);
                    txt_item_found_count_id.setText(count + "+");
                }
            }

            img_item_found_one_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickedImage(bundle, 0);
                }
            });
            img_item_found_two_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickedImage(bundle, 1);
                }
            });
            img_item_found_three_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickedImage(bundle, 2);
                }
            });

            img_item_found_gradient_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickedImage(bundle, 2);
                }
            });
        } else {
            txt_item_found_detail_id.setVisibility(View.GONE);
            img_item_found_one_id.setVisibility(View.GONE);
            img_item_found_two_id.setVisibility(View.GONE);
            img_item_found_three_id.setVisibility(View.GONE);
        }

    }

    private void setDataDetailInteresting() {
        if (itemsModel.getItemsDetailModelArrayList() != null && itemsModel.getItemsDetailModelArrayList().size() != 0) {
            switch (languageLocale.getLanguage()) {
                case "th":
                    txt_interesting_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(1).getDetailTH());
                    break;
                case "en":
                    txt_interesting_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(1).getDetailEN());
                    break;
                case "lo":
                    txt_interesting_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(1).getDetailLO());
                    break;
                case "zh":
                    txt_interesting_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(1).getDetailZH());
                    break;
            }
            if (itemsModel.getItemsDetailModelArrayList().get(1).getItemsPhotoDetailModels() != null && itemsModel.getItemsDetailModelArrayList().get(1).getItemsPhotoDetailModels().size() != 0) {
                interestingAdapter = new InterestingRecyclerViewAdapter(this, itemsModel.getItemsDetailModelArrayList().get(1).getItemsPhotoDetailModels(), languageLocale.getLanguage(), this);
                recycler_interesting.setLayoutManager(new LinearLayoutManager(this));
                recycler_interesting.setAdapter(interestingAdapter);
            }
        }
    }

    private void setDataTravelling() {
        if (itemsModel.getItemsDetailModelArrayList() != null && itemsModel.getItemsDetailModelArrayList().size() != 0) {
            switch (languageLocale.getLanguage()) {
                case "th":
                    txt_travelling_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(2).getDetailTH());
                    break;
                case "en":
                    txt_travelling_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(2).getDetailEN());
                    break;
                case "lo":
                    txt_travelling_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(2).getDetailLO());
                    break;
                case "zh":
                    txt_travelling_detail_id.setText(itemsModel.getItemsDetailModelArrayList().get(2).getDetailZH());
                    break;
            }
            if (itemsModel.getItemsDetailModelArrayList().get(2).getItemsPhotoDetailModels() != null && itemsModel.getItemsDetailModelArrayList().get(2).getItemsPhotoDetailModels().size() != 0) {
                travellingViewAdapter = new TravellingRecyclerViewAdapter(this, itemsModel.getItemsDetailModelArrayList().get(2).getItemsPhotoDetailModels(), this);
                recycler_travelling.setLayoutManager(new LinearLayoutManager(this));
                recycler_travelling.setAdapter(travellingViewAdapter);
            }
        }
    }

    private void setDataContact() {
        switch (languageLocale.getLanguage()){
            case "th":
                txt_contact_address_id.setText(itemsModel.getContactTH());
                break;
            case "en":
                txt_contact_address_id.setText(itemsModel.getContactEN());
                break;
            case "lo":
                txt_contact_address_id.setText(itemsModel.getContactLO());
                break;
            case "zh":
                txt_contact_address_id.setText(itemsModel.getContactZH());
                break;
        }
        txt_contact_phone_id.setText(itemsModel.getPhone());
        txt_contact_email_id.setText(itemsModel.getEmail());
        txt_contact_line_id.setText(itemsModel.getLine());
        txt_contact_facebook_id.setText(itemsModel.getFacebookPage());
    }

    private void setDataRating() {
        if (itemsModel.getRatingStarModel() != null) {
            txt_count_review_id.setText(itemsModel.getRatingStarModel().getCountReview() + " " + getResources().getString(R.string.text_list));
            txt_items_rating.setText(itemsModel.getRatingStarModel().getAverageReview() + "");
            txt_items_ratings.setText(itemsModel.getRatingStarModel().getAverageReview() + "");
            progress_one_count_id.setProgress(itemsModel.getRatingStarModel().getOneStar());
            progress_two_count_id.setProgress(itemsModel.getRatingStarModel().getTwoStar());
            progress_three_count_id.setProgress(itemsModel.getRatingStarModel().getThreeStar());
            progress_four_count_id.setProgress(itemsModel.getRatingStarModel().getFourStar());
            progress_five_count_id.setProgress(itemsModel.getRatingStarModel().getFiveStar());
            txt_one_count_id.setText(itemsModel.getRatingStarModel().getOneStar() + "");
            txt_two_count_id.setText(itemsModel.getRatingStarModel().getTwoStar() + "");
            txt_three_count_id.setText(itemsModel.getRatingStarModel().getThreeStar() + "");
            txt_four_count_id.setText(itemsModel.getRatingStarModel().getFourStar() + "");
            txt_five_count_id.setText(itemsModel.getRatingStarModel().getFiveStar() + "");
        }
        if (itemsModel.getMenuItemModel() != null) {
            switch (languageLocale.getLanguage()) {
                case "th":
                    txt_menu_item_id.setText("ภาพรวมสถานที่ " + itemsModel.getMenuItemModel().getMenuItemTH());
                    break;
                case "en":
                    txt_menu_item_id.setText("Location overview " + itemsModel.getMenuItemModel().getMenuItemEN());
                    break;
                case "lo":
                    txt_menu_item_id.setText("ພາບລວມຂອງສະຖານທີ່ " + itemsModel.getMenuItemModel().getMenuItemLO());
                    break;
                case "zh":
                    txt_menu_item_id.setText("位置概述 " + itemsModel.getMenuItemModel().getMenuItemZH());
                    break;
            }
        }
    }

    private void setDataReviews() {
        if (itemsModel.getRatingStarModel() != null) {
            if (itemsModel.getRatingStarModel().getReviewModelArrayList() != null && itemsModel.getRatingStarModel().getReviewModelArrayList().size() != 0) {
                reviewsAdapter = new ReviewsRecyclerViewAdapter(this, itemsModel.getRatingStarModel().getReviewModelArrayList(), languageLocale.getLanguage(), this, false);
                recycler_reviews.setLayoutManager(new LinearLayoutManager(this));
                recycler_reviews.setAdapter(reviewsAdapter);
            }
        }
    }


    private void setDataHotMenuProduct() {
        if (itemsModel.getProductModelArrayList() != null && itemsModel.getProductModelArrayList().size() != 0) {
            hotMenuAdapter = new ProductHotMenuRecyclerViewAdapter(this, itemsModel.getProductModelArrayList(), this, languageLocale.getLanguage(), rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recycler_hot_menu.setLayoutManager(layoutManager);
            recycler_hot_menu.setAdapter(hotMenuAdapter);
        }
    }

    private void setDataGuarantee() {
        if (itemsModel.getDeliciousGuaranteeModelArrayList() != null && itemsModel.getDeliciousGuaranteeModelArrayList().size() != 0) {
            guaranteeViewAdapter = new GuaranteeRecyclerViewAdapter(this, itemsModel.getDeliciousGuaranteeModelArrayList(), this, languageLocale.getLanguage());
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recycler_guarantee.setLayoutManager(layoutManager);
            recycler_guarantee.setAdapter(guaranteeViewAdapter);
        }
    }

    private void setDataPopularProduct() {
        TextView txt_exchange_rate_id = findViewById(R.id.txt_exchange_rate_id);
        if (itemsModel.getProductModelArrayList() != null && itemsModel.getProductModelArrayList().size() != 0) {
            popularAdapter = new ProductPopularRecyclerViewAdapter(this, itemsModel.getProductModelArrayList(), this, languageLocale.getLanguage(),txt_popular_total_id,this,rateModel, txt_exchange_rate_id);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recycler_popular_product.setLayoutManager(layoutManager);
            recycler_popular_product.setAdapter(popularAdapter);
        }
    }

    private void setDataFacilities() {
        if (itemsModel.getFacilitiesModelArrayList() != null && itemsModel.getFacilitiesModelArrayList().size() != 0) {
            facilitiesAdapter = new FacilitiesRecyclerViewAdapter(this, itemsModel.getFacilitiesModelArrayList(), this, languageLocale.getLanguage());
            recycler_facilities.setLayoutManager(new GridLayoutManager(this, 2));
            recycler_facilities.setAdapter(facilitiesAdapter);
        }
    }

    private void setDataExampleHotel() {
        if (itemsModel.getRoomModelArrayList() != null && itemsModel.getRoomModelArrayList().size() != 0) {
            roomAdapter = new RoomRecyclerViewAdapter(this, itemsModel.getRoomModelArrayList(), this, languageLocale.getLanguage(), this,rateModel);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recycler_ex_hotel.setLayoutManager(layoutManager);
            recycler_ex_hotel.setAdapter(roomAdapter);
        }
    }

    private void onClickedImage(Bundle bundle,int position) {
        if (isModeOnline) {
            DialogFragment fragment = SlidingImageDialogFragment.newInstance();
            bundle.putParcelableArrayList("ItemsPhotoDetailModels", itemsModel.getItemsDetailModelArrayList().get(0).getItemsPhotoDetailModels());
            bundle.putInt("SelectedImage", position);
            fragment.setArguments(bundle);
            fragment.show(getSupportFragmentManager(), "ViewTourismActivity");
        }
    }

    private void setChangedCollapsing() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    showToolbar.setVisibility(View.GONE);
                    showToolbar.setLayoutTransition(transition);
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    if (itemsModel != null) {
                        if (itemsModel.getArURL() != null && !itemsModel.getArURL().equals("")) {
                            showToolbar.setVisibility(View.VISIBLE);
                            showToolbar.setLayoutTransition(transition);
                            ar_app_toolbar_id.setVisibility(View.VISIBLE);
                            ar_app_toolbar_id.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    try {
                                        String url = itemsModel.getArURL()/*"https://play.google.com/store/apps/details?id=fr.smarquis.applinks"*/;
                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                        i.setData(Uri.parse(url));
                                        startActivity(i);
                                    } catch (Exception e) {
                                        Log.e("ViewTourismActivity", e.getMessage());
                                        Toast.makeText(ViewTourismActivity.this, "URL using bad", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            ar_app_toolbar_id.setVisibility(View.GONE);
                        }
                    }
                } else if(isShow) {
                    showToolbar.setVisibility(View.GONE);
                    showToolbar.setLayoutTransition(transition);
                    isShow = false;
                }
            }

        });
    }

    @Override
    public void itemClickedInteresting(int position) {
        if (isModeOnline) {
            Bundle bundle = new Bundle();
            DialogFragment fragment = SlidingImageDialogFragment.newInstance();
            bundle.putParcelableArrayList("ItemsPhotoDetailModels", itemsModel.getItemsDetailModelArrayList().get(1).getItemsPhotoDetailModels());
            bundle.putInt("SelectedImage", position);
            fragment.setArguments(bundle);
            fragment.show(getSupportFragmentManager(), "ViewTourismActivity");
        }
    }

    @Override
    public void itemClickedTravelling(int position) {
        if (isModeOnline) {
            Bundle bundle = new Bundle();
            DialogFragment fragment = SlidingImageDialogFragment.newInstance();
            bundle.putParcelableArrayList("ItemsPhotoDetailModels", itemsModel.getItemsDetailModelArrayList().get(2).getItemsPhotoDetailModels());
            bundle.putInt("SelectedImage", position);
            fragment.setArguments(bundle);
            fragment.show(getSupportFragmentManager(), "ViewTourismActivity");
        }
    }

    @Override
    public void itemClickedHotel(int position) {
        if (isModeOnline) {
            Bundle bundle = new Bundle();
            DialogFragment fragment = SlidingHotelDialogFragment.newInstance();
            bundle.putParcelableArrayList("RoomPictureModel", itemsModel.getRoomModelArrayList().get(position).getRoomPictureModelArrayList());
            bundle.putInt("SelectedImage", position);
            fragment.setArguments(bundle);
            fragment.show(getSupportFragmentManager(), "ViewTourismActivity");
        }
    }

    @Override
    public void itemClickedHotMenu(int position) {
        if (isModeOnline) {
            Bundle bundle = new Bundle();
            DialogFragment fragment = SlidingHotMenuDialogFragment.newInstance();
            bundle.putParcelableArrayList("RoomPictureModel", itemsModel.getProductModelArrayList());
            bundle.putParcelable("EXCHANGE_RATE_MODEL", rateModel);
            bundle.putInt("SelectedImage", position);
            fragment.setArguments(bundle);
            fragment.show(getSupportFragmentManager(), "ViewTourismActivity");
        }
    }

    @Override
    public void itemClickedPopularProduct(int position) {
        if (isModeOnline) {
            Bundle bundle = new Bundle();
            DialogFragment fragment = SlidingHotMenuDialogFragment.newInstance();
            bundle.putParcelableArrayList("RoomPictureModel", itemsModel.getProductModelArrayList());
            bundle.putParcelable("EXCHANGE_RATE_MODEL", rateModel);
            bundle.putInt("SelectedImage", position);
            fragment.setArguments(bundle);
            fragment.show(getSupportFragmentManager(), "ViewTourismActivity");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onClickGiveRating(){
        String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        if (isModeOnline) {
            if (userId != null) {
                Bundle bundle = new Bundle();
                DialogFragment fragment = ReviewDialogFragment.newInstance();
                bundle.putParcelable("ITEMS_MODEL", itemsModel);
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(), "ViewTourismActivity");
            } else {
                new AlertDialog.Builder(this)
                        .setTitle("กรุณาล็อกอินก่อนใช้งานการให้คะแนนรีวิว")
                        .setMessage("....")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(ViewTourismActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create().show();
            }
        }else {
            Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
        }
    }


    //Menu Drawer
    public void drawerMenu() {
        ImageView menuHome = findViewById(R.id.menu_home);
        ConstraintLayout menuProgramTour = findViewById(R.id.menu_program_tour);
        ConstraintLayout menuTourismLocation = findViewById(R.id.menu_tourism_location);
        ConstraintLayout menuRestaurant = findViewById(R.id.menu_restaurant);
        ConstraintLayout menuShopping = findViewById(R.id.menu_shopping);
        ConstraintLayout menuHotel = findViewById(R.id.menu_hotel);
        ConstraintLayout menuContact = findViewById(R.id.menu_contact);
        ConstraintLayout menuBooking = findViewById(R.id.menu_booking);
        Intent intent = new Intent(this, MainActivity.class);
        menuHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("NavItemIndex","0");
                startActivity(intent);
            }
        });
        menuProgramTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("NavItemIndex","1");
                startActivity(intent);
            }
        });
        menuTourismLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 2;
                intent.putExtra("NavItemIndex","2");
                startActivity(intent);
            }
        });
        menuRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 3;
                intent.putExtra("NavItemIndex","3");
                startActivity(intent);
            }
        });
        menuShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 4;
                intent.putExtra("NavItemIndex","4");
                startActivity(intent);
            }
        });
        menuHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 5;
                intent.putExtra("NavItemIndex","5");
                startActivity(intent);
            }
        });
        menuContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 6;
                intent.putExtra("NavItemIndex","6");
                startActivity(intent);
            }
        });
        menuBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 7;
                intent.putExtra("NavItemIndex","7");
                startActivity(intent);
            }
        });

    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    public static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }

    public void onClickTakingPhotos(View view){
        Intent intent = new Intent(this, CameraTwoActivity.class);
        if (isModeOnline) {
            if (itemsModel != null) {
                intent.putExtra("ITEMS_MODEL", itemsModel);
                startActivity(intent);
            }
        }else {
            Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickShare(View view){
        String ShareSub = "";
        switch (languageLocale.getLanguage()){
            case "th":
                ShareSub = getResources().getString(R.string.text_new_places_name)+" "+itemsModel.getTopicTH()+"\n";
                break;
            case "en":
                ShareSub = getResources().getString(R.string.text_new_places_name)+" "+itemsModel.getTopicEN()+"\n";
                break;
            case "lo":
                ShareSub = getResources().getString(R.string.text_new_places_name)+" "+itemsModel.getTopicLO()+"\n";
                break;
            case "zh":
                ShareSub = getResources().getString(R.string.text_new_places_name)+" "+itemsModel.getTopicZH()+"\n";
                break;
        }
        if (isModeOnline) {
            String uri = "http://maps.google.com/maps?saddr=" + itemsModel.getLatitude() + "," + itemsModel.getLongitude() + "&iwloc=A";
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, ShareSub);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, uri);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }else {
            Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickBookmark(View view){
        String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
        if (isModeOnline) {
            if (itemsModel != null && userId != null) {
                HttpCall httpCallPost = new HttpCall();
                httpCallPost.setMethodType(HttpCall.POST);
                httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/BookMarks");
                HashMap<String, String> paramsPost = new HashMap<>();
                paramsPost.put("User_user_id", userId);
                paramsPost.put("Items_items_id", String.valueOf(itemsModel.getItemsId()));
                httpCallPost.setParams(paramsPost);
                new HttpPostRequestAsyncTask().execute(httpCallPost);
            } else {
                new AlertDialog.Builder(this)
                        .setTitle("กรุณาล็อกอินก่อน")
                        .setMessage("....")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(ViewTourismActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create().show();
            }
            if (itemsModel.getBookMarksModel() != null) {
                if (itemsModel.getBookMarksModel().isBookmarksState()) {
                    itemsModel.getBookMarksModel().setBookmarksState(false);
                    img_items_bookmarks.setImageResource(R.drawable.ic_bookmark_inactive);
                } else {
                    itemsModel.getBookMarksModel().setBookmarksState(true);
                    img_items_bookmarks.setImageResource(R.drawable.ic_bookmark_red);
                }
            }
        }else {
            Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void itemClicked(View view, ReviewModel reviewModel) {
        if (isModeOnline) {
            Bundle bundle = new Bundle();
            DialogFragment fragment = ReviewDialogFragment.newInstance();
            bundle.putParcelable("ITEMS_MODEL", itemsModel);
            bundle.putParcelable("REVIEW_MODEL", reviewModel);
            fragment.setArguments(bundle);
            fragment.show(getSupportFragmentManager(), "Activity");
        }
    }

    @Override
    public void itemClickedDelete(View view, ReviewModel reviewModel,int position) {
        if (isModeOnline) {
            String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Review/delete");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("review_id", String.valueOf(reviewModel.getReviewId()));
            httpCallPost.setParams(paramsPost);
            new AlertDialog.Builder(this)
                    .setTitle("คุณต้องการลบข้อมูลหรือไม่")
                    .setMessage("....")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new HttpPostRequestAsyncTask().execute(httpCallPost);
                            itemsModel.getRatingStarModel().getReviewModelArrayList().remove(position);
                            reviewsAdapter.notifyItemRemoved(position);
                            reviewsAdapter.notifyDataSetChanged();
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .create().show();
        }
    }

    @Override
    public void itemClickedProfile(View view, ReviewModel reviewModel) {
        if (isModeOnline) {
            Bundle bundle = new Bundle();
            bundle.putString("USER_ID", String.valueOf(reviewModel.getUserModel().getUserId()));
            DialogFragment dialogFragment = ViewProfileDialogFragment.newInstance();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getSupportFragmentManager(), "ViewTourismActivity");
        }

    }

    public void onClickBackPressedView(View view){
        onBackPressed();
    }



    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bookingModel = new BookingModel();
        if (requestCode == 1009) {
            if (data != null) {
                String sDate = data.getStringExtra("CHOOSE_START_DATE");
                String eDate = data.getStringExtra("CHOOSE_END_DATE");
                String showDate = data.getStringExtra("CHOOSE_SHOW_DATE");
                String showMonth = data.getStringExtra("CHOOSE_SHOW_MONTH");
                txt_items_check_in_out_id.setText(showMonth);
                if (bookingModel != null) {
                    if (sDate != null && eDate != null && showMonth != null) {
                        bookingModel.setCheckIn(sDate);
                        bookingModel.setCheckOut(eDate);
                        bookingModel.setScheduleCheckIn(showMonth);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.onClickGiveRating:
                onClickGiveRating();
                break;
            case R.id.txt_see_more_program_tour:
                startActivity(new Intent(this,ProgramTourActivity.class));
                break;
            case R.id.img_cover_back_id:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onPreCallServiceProgramTour() {

    }

    @Override
    public void onCallServiceProgramTour() {

    }

    @Override
    public void onRequestCompleteListenerProgramTour(ArrayList<ProgramTourModel> programTourModelArrayList) {
        if (programTourModelArrayList != null && programTourModelArrayList.size() != 0){
            Log.e("check data",programTourModelArrayList+"");
            ProgramTourMainRecyclerViewAdapter adapter = new ProgramTourMainRecyclerViewAdapter(this,programTourModelArrayList,languageLocale.getLanguage(),this);
            recycler_program_tour.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));
            recycler_program_tour.setAdapter(adapter);
        }
    }

    @Override
    public void onRequestFailedProgramTour(String result) {

    }

    @Override
    public void itemClicked(ProgramTourModel tourModel) {
        Intent intent = new Intent(this, ViewProgramTourActivity.class);
        intent.putExtra("PROGRAM_TOUR", tourModel);
        startActivity(intent);
    }

    @Override
    public void itemClickedLikeProgramTour(ImageView img_like, TextView txt_count_like, ProgramTourModel tourModel) {
        if (isModeOnline) {
            String userId = getSharedPreferences("PREF_APP_USER_ID", Context.MODE_PRIVATE).getString("APP_USER_ID", null);
            HttpCall httpCallPost = new HttpCall();
            httpCallPost.setMethodType(HttpCall.POST);
            httpCallPost.setUrl(getString(R.string.app_api_ip) + "dasta_thailand/api/mobile/user/Likes");
            HashMap<String, String> paramsPost = new HashMap<>();
            paramsPost.put("User_user_id", userId);
            paramsPost.put("ProgramTour_programTour_id", String.valueOf(tourModel.getProgramTourId()));
            httpCallPost.setParams(paramsPost);
            if (userId != null) {
                new HttpPostRequestAsyncTask().execute(httpCallPost);
                finish();
                startActivity(getIntent());
            } else {

            }
        }

    }

    @Override
    public void selectedRoom(RoomModel rooms) {
        if (isModeOnline) {
            Intent intent = new Intent(this, HotelDetailsActivity.class);
            if (TextUtils.isEmpty(txt_items_check_in_out_id.getText().toString())) {
                Toast.makeText(this, "The date you entered is not valid. \nPlease select from one of the dates…", Toast.LENGTH_SHORT).show();
            } else {
                if (bookingModel != null) {
                    intent.putExtra("BOOKING", bookingModel);
                    intent.putExtra("ITEMS", itemsModel);
                    intent.putExtra("ROOMS", rooms);
                    startActivity(intent);
                }
            }
        }
    }

    public void onClickOrderDetailsList(View view){
        if (isModeOnline) {
            ArrayList<ProductModel> list = new ArrayList<>();
            try {
                for (ProductModel p : productList){
                    if (p.isSelected()){
                        list.add(p);
                    }
                }
            }catch (Exception e){
                Log.e("Error OrderDetails",e.getMessage().toString());
            }finally {

            }
            if (userId != null) {
                Bundle bundle = new Bundle();
                DialogFragment dialogFragment = OrderDetailsDialogFragment.newInstance();
                bundle.putParcelableArrayList("PRODUCT_LIST", list);
                bundle.putParcelable("EXCHANGE_RATE_MODEL", rateModel);
                bundle.putParcelable("ITEMS", itemsModel);
                dialogFragment.setArguments(bundle);
                dialogFragment.show(getSupportFragmentManager(), "ViewTourismActivity");
            } else {
                startActivity(new Intent(this, LoginActivity.class));
            }
        }else {
            Toast.makeText(this, "Offline Mode.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getPopularOrderDetails(ArrayList<ProductModel> list, ProductModel product) {
        try {
            this.productList = list;
        }catch (Exception e){
            Log.e("Error OrderDetails",e.getMessage().toString());
        }finally {

        }

    }

    public static String getCurrentTime() {
        String DATE_FORMAT_1 = "HH:mm";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_1);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    @Override
    public void onRequestCompleteListener(RateModel rateModel) {
        if (rateModel != null){
            this.rateModel = rateModel;
        }
    }

}
