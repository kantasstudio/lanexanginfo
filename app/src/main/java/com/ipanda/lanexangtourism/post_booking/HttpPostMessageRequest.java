package com.ipanda.lanexangtourism.post_booking;

import android.os.AsyncTask;
import android.util.Log;


import com.ipanda.lanexangtourism.Model.MessageModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.MessageCallBack;
import com.ipanda.lanexangtourism.interface_callback.MessageSendCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class HttpPostMessageRequest extends AsyncTask<HttpCall ,Integer, String> {

    private static final String UTF_8 = "UTF-8";
    private ArrayList<MessageModel> arrayList;
    private MessageSendCallBack callBackService;


    public HttpPostMessageRequest(MessageSendCallBack callBackService) {
        this.callBackService = callBackService;
    }


    @Override
    protected String doInBackground(HttpCall... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return uploadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerSendMessage(arrayList);
        }
    }

    public ArrayList<MessageModel> onParserContentToModel(String dataJSon) {
        ArrayList<MessageModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonMessages = object.optJSONArray("ChatMessages");

            if (jsonMessages != null && jsonMessages.length() != 0) {

                for (int i = 0; i < jsonMessages.length() ; i++) {
                    JSONObject jsMessages = jsonMessages.getJSONObject(i);
                    MessageModel message = new MessageModel();
                    UserModel user = new UserModel();

                    message.setMessageId(jsMessages.getInt("messages_id"));
                    message.setMessage(jsMessages.getString("message"));
                    message.setCreatedAt(jsMessages.getString("message_created_at"));
                    message.setGcmId(jsMessages.getString("gcm_registration_id"));
                    if (jsMessages.getString("messages_read").equals("1")) {
                        message.setRead(true);
                    }else {
                        message.setRead(false);
                    }
                    message.setRoomId(jsMessages.getInt("ChatRooms_chatroom_id"));

                    user.setUserId(jsMessages.getInt("user_id"));
                    user.setFirstName(jsMessages.getString("user_firstName"));
                    user.setLastName(jsMessages.getString("user_lastName"));
                    if (!jsMessages.getString("user_profile_pic_url").equals("") && jsMessages.getString("user_profile_pic_url") != null){
                        user.setProfilePicUrl(jsMessages.getString("user_profile_pic_url"));
                    }else {
                        user.setProfilePicUrl(null);
                    }

                    message.setUserModel(user);
                    list.add(message);

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    private String uploadContent(HttpCall params) throws IOException{
        HttpURLConnection urlConnection = null;
        HttpCall httpCall = params;
        StringBuilder response = new StringBuilder();
        try{
            String dataParams = getDataObject(httpCall.getParams(), httpCall.getMethodType());
            URL url = new URL(httpCall.getMethodType() == HttpCall.GET ? httpCall.getUrl() + dataParams : httpCall.getUrl());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(httpCall.getMethodType() == HttpCall.GET ? "GET":"POST");
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            if(httpCall.getParams() != null && httpCall.getMethodType() == HttpCall.POST){
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, UTF_8));
                writer.append(dataParams);
                writer.flush();
                writer.close();
                os.close();
            }
            int responseCode = urlConnection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK){
                String line ;
                BufferedReader br = new BufferedReader( new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null){
                    response.append(line);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            urlConnection.disconnect();
        }
        return response.toString();
    }

    private String getDataObject(HashMap<String, String> params, int methodType) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean isFirst = true;
        for(Map.Entry<String,String> entry : params.entrySet()){
            if (isFirst){
                isFirst = false;
                if(methodType == HttpCall.GET){
                    result.append("?");
                }
            }else{
                result.append("&");
            }
            result.append(URLEncoder.encode(entry.getKey(), UTF_8));
            result.append("=");
            result.append(URLEncoder.encode(String.valueOf(entry.getValue()), UTF_8));
        }
        return result.toString();
    }


}
