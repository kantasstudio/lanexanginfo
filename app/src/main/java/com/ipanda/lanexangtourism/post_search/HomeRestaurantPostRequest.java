package com.ipanda.lanexangtourism.post_search;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BookMarksModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.DeliciousGuaranteeModel;
import com.ipanda.lanexangtourism.Model.FacilitiesModel;
import com.ipanda.lanexangtourism.Model.ItemsDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsRestaurantModel;
import com.ipanda.lanexangtourism.Model.ItemsTopicDetailModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.PeriodDayModel;
import com.ipanda.lanexangtourism.Model.PeriodTimeModel;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.ProductPhotoModel;
import com.ipanda.lanexangtourism.Model.RatingStarModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.Model.RoomPictureModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ItemsHomeSearchPostCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsRestaurantCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class HomeRestaurantPostRequest extends AsyncTask<HttpCall ,Integer, String> {

    private static final String UTF_8 = "UTF-8";
    private ArrayList<ItemsRestaurantModel> arrayList;
    private ItemsRestaurantCallBack callBackService;


    public HomeRestaurantPostRequest(ItemsRestaurantCallBack callBackService) {
        this.callBackService = callBackService;
    }


    @Override
    protected String doInBackground(HttpCall... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return uploadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteRestaurant(arrayList);
        }
    }

    public ArrayList<ItemsRestaurantModel> onParserContentToModel(String dataJSon) {
        ArrayList<ItemsRestaurantModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonItems = object.optJSONArray("Items");

            if (jsonItems != null){
                for (int i = 0; i < jsonItems.length(); i++) {
                    JSONObject jsItems = jsonItems.getJSONObject(i);
                    ItemsRestaurantModel items = new ItemsRestaurantModel();
                    ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                    SubCategoryModel subCategory = new SubCategoryModel();
                    MenuItemModel menuItem = new MenuItemModel();
                    CoverItemsModel cover = new CoverItemsModel();
                    JSONArray jsonDetail = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Detail");
                    JSONArray jsonDelicious = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Delicious");
                    JSONArray jsonProduct = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Product");
                    JSONArray jsonRating = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Review");

                    if (jsItems != null){
                        items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                        items.setTopicTH(jsItems.getString("itmes_topicThai"));
                        items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                        items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                        items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                        items.setLatitude(jsItems.getString("items_latitude"));
                        items.setLongitude(jsItems.getString("items_longitude"));
                        items.setContactTH(jsItems.getString("items_contactThai"));
                        items.setContactEN(jsItems.getString("items_contactEnglish"));
                        items.setContactLO(jsItems.getString("items_contactLaos"));
                        items.setContactZH(jsItems.getString("items_contactChinese"));
                        items.setPhone(jsItems.getString("items_phone"));
                        items.setEmail(jsItems.getString("items_email"));
                        items.setLine(jsItems.getString("items_line"));
                        items.setFacebookPage(jsItems.getString("items_facebookPage"));
                        items.setArURL(jsItems.getString("item_ar_url"));
//                            items.setPrice(jsItems.getDouble("items_price"));
                        items.setIsActive(jsItems.getInt("items_isActive"));
                        items.setIsPublish(jsItems.getInt("items_isPublish"));
                        items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                        items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                        items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                        items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                        items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                        items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                        items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                        items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
                        items.setCreateDatetime(jsItems.getString("items_createdDTTM"));
                        items.setUpdateDatetime(jsItems.getString("items_updatedDTTM"));
                        items.setTimeOpen(jsItems.getString("items_timeOpen"));
                        items.setTimeClose(jsItems.getString("items_timeClose"));
                        menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                        menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                        menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                        menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                        menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                        items.setMenuItemModel(menuItem);
                        subCategory.setCategoryId(Integer.parseInt(jsItems.getString("subcategory_id")));
                        subCategory.setCategoryTH(jsItems.getString("subcategory_thai"));
                        subCategory.setCategoryEN(jsItems.getString("subcategory_english"));
                        subCategory.setCategoryLO(jsItems.getString("subcategory_laos"));
                        subCategory.setCategoryZH(jsItems.getString("subcategory_chinese"));
                        subCategoryArrayList.add(subCategory);
                        items.setSubCategoryModelArrayList(subCategoryArrayList);
                        cover.setCoverId(jsItems.getInt("coverItem_id"));
                        cover.setCoverPaths(jsItems.getString("coverItem_paths"));
                        if(jsItems.getString("coverItem_url") != null && jsItems.getString("coverItem_url") != "" && !jsItems.getString("coverItem_url").equals("")){
                            cover.setCoverURL(jsItems.getString("coverItem_url"));
                        }else {
                            cover.setCoverURL(null);
                        }
                        items.setCoverItemsModel(cover);

                        try {
                            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
                            Date currentTime  = parser.parse(getCurrentTime());
                            Date timeOpen = parser.parse(jsItems.getString("items_timeOpen"));
                            Date timeClosed = parser.parse(jsItems.getString("items_timeClose"));

                            int diffOpen = currentTime.compareTo(timeOpen);
                            int diffClosed = currentTime.compareTo(timeClosed);

                            if (timeOpen.equals(timeClosed)){
                                items.setOpenOrClosed(true);
                            }else {
                                if(diffOpen >= 0 && diffClosed <= 0) {
                                    items.setOpenOrClosed(true);
                                } else if (diffOpen <= 0 && diffClosed >= 0) {
                                    items.setOpenOrClosed(false);
                                } else {
                                    items.setOpenOrClosed(false);
                                }
                            }

                        }catch (ParseException ex){
                            Log.e("error diff", ex.toString());
                        }

                        BookMarksModel bookMarks = new BookMarksModel();
                        bookMarks.setBookmarksState(jsItems.getBoolean("BookMarks_state"));
                        items.setBookMarksModel(bookMarks);
                        items.setDistance(jsItems.getInt("Distance"));
                        items.setDistance_metr(jsItems.getInt("Distance_metr"));

                        ArrayList<ItemsDetailModel> detailModelArrayList = new ArrayList<>();
                        for (int j = 0; j < jsonDetail.length(); j++){
                            JSONObject jsDetail = jsonDetail.getJSONObject(j);
                            ItemsDetailModel itemsDetail = new ItemsDetailModel();
                            ItemsTopicDetailModel topicDetail = new ItemsTopicDetailModel();
                            JSONArray jsonPhoto = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Detail").getJSONObject(j).getJSONArray("Photo");

                            if (jsDetail != null){
                                itemsDetail.setDetailId(jsDetail.getInt("detail_id"));
                                itemsDetail.setDetailTH(jsDetail.getString("detail_textThai"));
                                itemsDetail.setDetailEN(jsDetail.getString("detail_textEnglish"));
                                itemsDetail.setDetailLO(jsDetail.getString("detail_textLaos"));
                                itemsDetail.setDetailZH(jsDetail.getString("detail_textChinese"));
                                topicDetail.setTopicDetailId(jsDetail.getInt("topicdetail_id"));
                                topicDetail.setTopicDetailTH(jsDetail.getString("topicdetail_Thai"));
                                topicDetail.setTopicDetailEN(jsDetail.getString("topicdetail_English"));
                                topicDetail.setTopicDetailLO(jsDetail.getString("topicdetail_Laos"));
                                topicDetail.setTopicDetailZH(jsDetail.getString("topicdetail_Chinese"));
                                itemsDetail.setItemsTopicDetailModel(topicDetail);
                                detailModelArrayList.add(itemsDetail);

                                ArrayList<ItemsPhotoDetailModel> photoDetailModelArrayList = new ArrayList<>();
                                for (int k = 0; k < jsonPhoto.length(); k++){
                                    JSONObject jsPhoto = jsonPhoto.getJSONObject(k);
                                    ItemsPhotoDetailModel photoDetail = new ItemsPhotoDetailModel();

                                    if (jsPhoto != null){
                                        photoDetail.setPhotoId(jsPhoto.getInt("photo_id"));
                                        photoDetail.setPhotoPaths(jsPhoto.getString("photo_paths"));
                                        photoDetail.setPhotoTextTH(jsPhoto.getString("photo_textThai"));
                                        photoDetail.setPhotoTextEN(jsPhoto.getString("photo_textEnglish"));
                                        photoDetail.setPhotoTextLO(jsPhoto.getString("photo_textLaos"));
                                        photoDetail.setPhotoTextZH(jsPhoto.getString("photo_textChinese"));
                                        photoDetailModelArrayList.add(photoDetail);
                                    }

                                }
                                itemsDetail.setItemsPhotoDetailModels(photoDetailModelArrayList);

                            }
                            items.setItemsDetailModelArrayList(detailModelArrayList);
                        }

                        ArrayList<DeliciousGuaranteeModel> deliciousGuaranteeModelArrayList = new ArrayList<>();
                        for (int j = 0; j < jsonDelicious.length() ; j++) {
                            JSONObject jsDelicious = jsonDelicious.getJSONObject(j);
                            DeliciousGuaranteeModel guaranteeModel = new DeliciousGuaranteeModel();

                            if (jsDelicious != null){
                                guaranteeModel.setGuaranteeId(jsDelicious.getInt("DeliciousGuarantee_deliciousGuarantee_id"));
                                guaranteeModel.setGuaranteeTH(jsDelicious.getString("deliciousGuarantee_textThai"));
                                guaranteeModel.setGuaranteeEN(jsDelicious.getString("deliciousGuarantee_textEnglish"));
                                guaranteeModel.setGuaranteeLO(jsDelicious.getString("deliciousGuarantee_textLaos"));
                                guaranteeModel.setGuaranteeZH(jsDelicious.getString("deliciousGuarantee_textChinese"));
                                guaranteeModel.setGuaranteePaths(jsDelicious.getString("deliciousGuarantee_paths"));
                                deliciousGuaranteeModelArrayList.add(guaranteeModel);
                            }
                            items.setDeliciousGuaranteeModelArrayList(deliciousGuaranteeModelArrayList);
                        }

                        ArrayList<ProductModel> productModelArrayList = new ArrayList<>();
                        for (int j = 0; j < jsonProduct.length() ; j++) {
                            JSONObject jsProduct = jsonProduct.getJSONObject(j);
                            ProductModel productModel = new ProductModel();
                            JSONArray jsonPhotoProduct = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Product").getJSONObject(j).getJSONArray("PhotoProduct");

                            if (jsProduct != null){
                                productModel.setProductId(jsProduct.getInt("product_id"));
                                productModel.setProductNamesTH(jsProduct.getString("product_namesThai"));
                                productModel.setProductNamesEN(jsProduct.getString("product_namesEnglish"));
                                productModel.setProductNamesLO(jsProduct.getString("product_namesLaos"));
                                productModel.setProductNamesZH(jsProduct.getString("product_namesChinese"));
                                productModel.setProductPrice(jsProduct.getDouble("product_price"));
                                productModel.setDescriptionTH(jsProduct.getString("product_descriptionThai"));
                                productModel.setDescriptionEN(jsProduct.getString("product_descriptionEnglish"));
                                productModel.setDescriptionLO(jsProduct.getString("product_descriptionLaos"));
                                productModel.setDescriptionZH(jsProduct.getString("product_descriptionChinese"));
                                productModelArrayList.add(productModel);

                                ArrayList<ProductPhotoModel> productPhotoModelArrayList = new ArrayList<>();
                                for (int k = 0; k < jsonPhotoProduct.length() ; k++) {
                                    JSONObject jsPhotoProduct = jsonPhotoProduct.getJSONObject(k);
                                    ProductPhotoModel productPhotoModel = new ProductPhotoModel();

                                    if (jsPhotoProduct != null){
                                        productPhotoModel.setProductPhotoId(jsPhotoProduct.getInt("photoProduct_id"));
                                        productPhotoModel.setProductPhotoPaths(jsPhotoProduct.getString("photoProduct_paths"));
                                        productPhotoModelArrayList.add(productPhotoModel);
                                    }
                                    productModel.setProductPhotoModelArrayList(productPhotoModelArrayList);

                                }

                            }
                            items.setProductModelArrayList(productModelArrayList);
                        }

                        RatingStarModel ratingStarModel = new RatingStarModel();
                        for (int j = 0; j < jsonRating.length() ; j++) {
                            JSONObject jsRating = jsonRating.getJSONObject(j);
                            JSONArray jsonReview = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Review").getJSONObject(j).getJSONArray("Reviews");

                            if (jsRating != null){
                                ratingStarModel.setAverageReview(jsRating.getInt("AverageReview"));
                                ratingStarModel.setCountReview(jsRating.getInt("CountReview"));
                                ratingStarModel.setOneStar(jsRating.getInt("OneStar"));
                                ratingStarModel.setTwoStar(jsRating.getInt("TwoStar"));
                                ratingStarModel.setThreeStar(jsRating.getInt("ThreeStar"));
                                ratingStarModel.setFourStar(jsRating.getInt("FourStar"));
                                ratingStarModel.setFiveStar(jsRating.getInt("FiveStar"));

                                ArrayList<ReviewModel> reviewModelArrayList = new ArrayList<>();
                                for (int k = 0; k < jsonReview.length() ; k++) {
                                    JSONObject jsReview = jsonReview.getJSONObject(k);
                                    ReviewModel reviewModel = new ReviewModel();
                                    UserModel userModel = new UserModel();

                                    if (jsReview != null){
                                        reviewModel.setReviewId(jsReview.getInt("review_id"));
                                        reviewModel.setReviewTimestamp(jsReview.getString("review_timestamp"));
                                        reviewModel.setReviewRating(jsReview.getInt("review_rating"));
                                        reviewModel.setReviewText(jsReview.getString("review_text"));
                                        userModel.setUserId(jsReview.getInt("User_user_id"));
                                        userModel.setFirstName(jsReview.getString("user_firstName"));
                                        userModel.setLastName(jsReview.getString("user_lastName"));
                                        userModel.setProfilePicUrl(jsReview.getString("user_profile_pic_url"));
                                        ItemsModel it = new ItemsModel();
                                        it.setItemsId(jsReview.getInt("Items_items_id"));
                                        it.setTopicTH(jsReview.getString("itmes_topicThai"));
                                        it.setTopicEN(jsReview.getString("itmes_topicEnglish"));
                                        it.setTopicLO(jsReview.getString("itmes_topicLaos"));
                                        it.setTopicZH(jsReview.getString("itmes_topicChinese"));
                                        reviewModel.setItemsModel(it);
                                        reviewModel.setUserModel(userModel);
                                    }
                                    reviewModelArrayList.add(reviewModel);
                                }
                                ratingStarModel.setReviewModelArrayList(reviewModelArrayList);
                            }
                            items.setRatingStarModel(ratingStarModel);
                        }

                        try {
                            ArrayList<PeriodTimeModel> periodTimeModelArrayList = new ArrayList<>();
                            PeriodTimeModel periodTime = new PeriodTimeModel();
                            PeriodDayModel periodDay = new PeriodDayModel();

                            periodTime.setPeriodId(0);
                            periodTime.setPeriodOpen(jsItems.getString("items_timeOpen"));
                            periodTime.setPeriodClose(jsItems.getString("items_timeClose"));

                            JSONArray jsonOpen = object.optJSONArray("Items").getJSONObject(i).getJSONArray("item_dayOpen_data");
                            for (int l = 0; l < jsonOpen.length() ; l++) {
                                JSONObject jsOpen = jsonOpen.getJSONObject(l);
                                if (jsOpen != null){
                                    periodDay.setPeriodOpenId(jsOpen.getInt("dayofweek_id"));
                                    periodDay.setPeriodOpenTH(jsOpen.getString("dayofweek_thai"));
                                    periodDay.setPeriodOpenEN(jsOpen.getString("dayofweek_English"));
                                    periodDay.setPeriodOpenLO(jsOpen.getString("dayofweek_Chinese"));
                                    periodDay.setPeriodOpenZH(jsOpen.getString("dayofweek_Laos"));
                                }
                            }

                            JSONArray jsonClose = object.optJSONArray("Items").getJSONObject(i).getJSONArray("item_dayClose_data");
                            for (int l = 0; l < jsonClose.length() ; l++) {
                                JSONObject jsClose = jsonClose.getJSONObject(l);
                                if (jsClose != null){
                                    periodDay.setPeriodCloseId(jsClose.getInt("dayofweek_id"));
                                    periodDay.setPeriodCloseTH(jsClose.getString("dayofweek_thai"));
                                    periodDay.setPeriodCloseEN(jsClose.getString("dayofweek_English"));
                                    periodDay.setPeriodCloseLO(jsClose.getString("dayofweek_Chinese"));
                                    periodDay.setPeriodCloseZH(jsClose.getString("dayofweek_Laos"));
                                }
                            }

                            periodTime.setPeriodDayModel(periodDay);
                            periodTimeModelArrayList.add(periodTime);

                            items.setPeriodTimeModelArrayList(periodTimeModelArrayList);
                            items.setPeriodDayModel(periodDay);
                            items.setPeriodTimeModel(periodTime);


                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }

                        list.add(items);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    private String uploadContent(HttpCall params) throws IOException{
        HttpURLConnection urlConnection = null;
        HttpCall httpCall = params;
        StringBuilder response = new StringBuilder();
        try{
            String dataParams = getDataObject(httpCall.getParams(), httpCall.getMethodType());
            URL url = new URL(httpCall.getMethodType() == HttpCall.GET ? httpCall.getUrl() + dataParams : httpCall.getUrl());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(httpCall.getMethodType() == HttpCall.GET ? "GET":"POST");
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            if(httpCall.getParams() != null && httpCall.getMethodType() == HttpCall.POST){
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, UTF_8));
                writer.append(dataParams);
                writer.flush();
                writer.close();
                os.close();
            }
            int responseCode = urlConnection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK){
                String line ;
                BufferedReader br = new BufferedReader( new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null){
                    response.append(line);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            urlConnection.disconnect();
        }
        return response.toString();
    }

    private String getDataObject(HashMap<String, String> params, int methodType) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean isFirst = true;
        for(Map.Entry<String,String> entry : params.entrySet()){
            if (isFirst){
                isFirst = false;
                if(methodType == HttpCall.GET){
                    result.append("?");
                }
            }else{
                result.append("&");
            }
            result.append(URLEncoder.encode(entry.getKey(), UTF_8));
            result.append("=");
            result.append(URLEncoder.encode(String.valueOf(entry.getValue()), UTF_8));
        }
        return result.toString();
    }

    public static String getCurrentTime() {
        String DATE_FORMAT_1 = "HH:mm";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_1);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

}
