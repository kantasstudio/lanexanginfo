package com.ipanda.lanexangtourism.post_search;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BusinessModel;
import com.ipanda.lanexangtourism.Model.CarModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ItemsCarRentCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsLastSearchCarRentCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class LastSearchCarRental extends AsyncTask<HttpCall ,Integer, String> {

    private static final String UTF_8 = "UTF-8";
    private ArrayList<ItemsModel> arrayList;
    private ItemsLastSearchCarRentCallBack callBackService;


    public LastSearchCarRental(ItemsLastSearchCarRentCallBack callBackService) {
        this.callBackService = callBackService;
    }


    @Override
    protected String doInBackground(HttpCall... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return uploadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerLastSearchCarRent(arrayList);
        }
    }

    public ArrayList<ItemsModel> onParserContentToModel(String dataJSon) {
        ArrayList<ItemsModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonItems = object.optJSONArray("Items");

            for (int i = 0; i < jsonItems.length(); i++) {
                JSONObject jsItems = jsonItems.getJSONObject(i);
                ItemsModel items = new ItemsModel();
                ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                MenuItemModel menuItem = new MenuItemModel();
                SubCategoryModel subCategory = new SubCategoryModel();

                if (jsItems != null) {
                    items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                    items.setTopicTH(jsItems.getString("itmes_topicThai"));
                    items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                    items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                    items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                    items.setLatitude(jsItems.getString("items_latitude"));
                    items.setLongitude(jsItems.getString("items_longitude"));
                    items.setContactTH(jsItems.getString("items_contactThai"));
                    items.setContactEN(jsItems.getString("items_contactEnglish"));
                    items.setContactLO(jsItems.getString("items_contactLaos"));
                    items.setContactZH(jsItems.getString("items_contactChinese"));
                    items.setPhone(jsItems.getString("items_phone"));
                    items.setEmail(jsItems.getString("items_email"));
                    items.setLine(jsItems.getString("items_line"));
                    items.setFacebookPage(jsItems.getString("items_facebookPage"));
                    items.setArURL(jsItems.getString("item_ar_url"));
//                    items.setPrice(jsItems.getDouble("items_price"));
                    items.setIsActive(jsItems.getInt("items_isActive"));
                    items.setIsPublish(jsItems.getInt("items_isPublish"));
                    items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                    items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                    items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                    items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                    items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                    items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                    items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                    items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
//                    items.setTimeOpen(jsItems.getString("items_timeOpen"));
//                    items.setTimeClose(jsItems.getString("items_timeClose"));
                    items.setPriceGuestAdult(Integer.parseInt(jsItems.getString("items_priceguestAdult")));
                    items.setPriceGuestChild(Integer.parseInt(jsItems.getString("items_priceguestChild")));
                    menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                    menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                    menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                    menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                    menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                    items.setMenuItemModel(menuItem);

                    CoverItemsModel coverItemsModels = new CoverItemsModel();
                    coverItemsModels.setCoverId(jsItems.getInt("coverItem_id"));
                    coverItemsModels.setCoverPaths(jsItems.getString("coverItem_paths"));
                    if (jsItems.getString("coverItem_url") != null && jsItems.getString("coverItem_url") != "" && !jsItems.getString("coverItem_url").equals("")) {
                        coverItemsModels.setCoverURL(jsItems.getString("coverItem_url"));
                    } else {
                        coverItemsModels.setCoverURL(null);
                    }
                    items.setCoverItemsModel(coverItemsModels);


                    CarModel carModel = new CarModel();
                    carModel.setSeats(jsItems.getInt("items_carSeats"));
                    carModel.setPricePerDay(jsItems.getInt("items_PricePerDay"));
                    carModel.setReturnPrice(jsItems.getInt("items_CarDeliveryPrice"));
                    carModel.setPickUpPrice(jsItems.getInt("items_PickUpPrice"));
                    carModel.setDepositPrice(jsItems.getInt("items_DepositPrice"));
                    if (jsItems.getString("items_isBasicInsurance").equalsIgnoreCase("1")){
                        carModel.setBasicInsurance(true);
                    }else {
                        carModel.setBasicInsurance(false);
                    }
                    carModel.setBasicInsuranceTH(jsItems.getString("items_BissicInsurance_detailThai"));
                    carModel.setBasicInsuranceEN(jsItems.getString("items_BissicInsurance_detailEnglish"));
                    carModel.setBasicInsuranceLO(jsItems.getString("items_BissicInsurance_detailLaos"));
                    carModel.setBasicInsuranceZH(jsItems.getString("items_BissicInsurance_detailChinese"));
                    carModel.setTypeTH(jsItems.getString("typesofCars_Thai"));
                    carModel.setTypeEN(jsItems.getString("typesofCars_English"));
                    carModel.setTypeLO(jsItems.getString("typesofCars_Laos"));
                    carModel.setTypeZH(jsItems.getString("typesofCars_Chinese"));
                    carModel.setGearSystemTH(jsItems.getString("gearSystem_Thai"));
                    carModel.setGearSystemEN(jsItems.getString("gearSystem_English"));
                    carModel.setGearSystemLO(jsItems.getString("gearSystem_Laos"));
                    carModel.setGearSystemZH(jsItems.getString("gearSystem_Chinese"));
                    items.setCarModel(carModel);

                    JSONArray jsonBusiness = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Business");
                    for (int j = 0; j < jsonBusiness.length() ; j++) {
                        JSONObject jsBusiness = jsonBusiness.getJSONObject(j);
                        if (jsBusiness != null) {
                            BusinessModel business = new BusinessModel();
                            business.setBusinessId(jsBusiness.getInt("business_id"));
                            business.setNameTH(jsBusiness.getString("business_nameThai"));
                            business.setNameEN(jsBusiness.getString("business_nameEnglish"));
                            business.setNameLO(jsBusiness.getString("business_nameLaos"));
                            business.setNameZH(jsBusiness.getString("business_nameChinese"));
                            business.setLatitude((float) jsBusiness.getDouble("business_latitude"));
                            business.setLongitude((float) jsBusiness.getDouble("business_longitude"));
                            business.setPresentAddress(jsBusiness.getString("business_presentAddress"));
                            business.setLicensePaths(jsBusiness.getString("business_license_paths"));
                            business.setOperatorNumber(jsBusiness.getString("business_operator_number"));
                            business.setPhone(jsBusiness.getString("business_phone"));
                            business.setWeb(jsBusiness.getString("business_www"));
                            business.setFacebook(jsBusiness.getString("business_facebook"));
                            business.setLine(jsBusiness.getString("business_line"));
                            items.setBusinessModel(business);
                        }
                    }

                    list.add(items);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    private String uploadContent(HttpCall params) throws IOException{
        HttpURLConnection urlConnection = null;
        HttpCall httpCall = params;
        StringBuilder response = new StringBuilder();
        try{
            String dataParams = getDataObject(httpCall.getParams(), httpCall.getMethodType());
            URL url = new URL(httpCall.getMethodType() == HttpCall.GET ? httpCall.getUrl() + dataParams : httpCall.getUrl());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(httpCall.getMethodType() == HttpCall.GET ? "GET":"POST");
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            if(httpCall.getParams() != null && httpCall.getMethodType() == HttpCall.POST){
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, UTF_8));
                writer.append(dataParams);
                writer.flush();
                writer.close();
                os.close();
            }
            int responseCode = urlConnection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK){
                String line ;
                BufferedReader br = new BufferedReader( new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null){
                    response.append(line);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            urlConnection.disconnect();
        }
        return response.toString();
    }

    private String getDataObject(HashMap<String, String> params, int methodType) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean isFirst = true;
        for(Map.Entry<String,String> entry : params.entrySet()){
            if (isFirst){
                isFirst = false;
                if(methodType == HttpCall.GET){
                    result.append("?");
                }
            }else{
                result.append("&");
            }
            result.append(URLEncoder.encode(entry.getKey(), UTF_8));
            result.append("=");
            result.append(URLEncoder.encode(String.valueOf(entry.getValue()), UTF_8));
        }
        return result.toString();
    }


}
