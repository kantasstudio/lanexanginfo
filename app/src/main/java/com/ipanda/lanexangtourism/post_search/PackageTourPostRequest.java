package com.ipanda.lanexangtourism.post_search;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.Model.BookMarksModel;
import com.ipanda.lanexangtourism.Model.BookingConditionModel;
import com.ipanda.lanexangtourism.Model.BusinessModel;
import com.ipanda.lanexangtourism.Model.CoverItemsModel;
import com.ipanda.lanexangtourism.Model.DeliciousGuaranteeModel;
import com.ipanda.lanexangtourism.Model.FacilitiesModel;
import com.ipanda.lanexangtourism.Model.FoodModel;
import com.ipanda.lanexangtourism.Model.HotelAccommodationModel;
import com.ipanda.lanexangtourism.Model.ItemsDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.Model.ItemsPhotoDetailModel;
import com.ipanda.lanexangtourism.Model.ItemsTopicDetailModel;
import com.ipanda.lanexangtourism.Model.MenuItemModel;
import com.ipanda.lanexangtourism.Model.ProductModel;
import com.ipanda.lanexangtourism.Model.ProductPhotoModel;
import com.ipanda.lanexangtourism.Model.ProvincesModel;
import com.ipanda.lanexangtourism.Model.RatingStarModel;
import com.ipanda.lanexangtourism.Model.ReviewModel;
import com.ipanda.lanexangtourism.Model.RoomModel;
import com.ipanda.lanexangtourism.Model.RoomPictureModel;
import com.ipanda.lanexangtourism.Model.SubCategoryModel;
import com.ipanda.lanexangtourism.Model.TravelDetailsModel;
import com.ipanda.lanexangtourism.Model.TravelPeriodModel;
import com.ipanda.lanexangtourism.Model.UserModel;
import com.ipanda.lanexangtourism.httpcall.HttpCall;
import com.ipanda.lanexangtourism.interface_callback.ItemsHomeSearchPostCallBack;
import com.ipanda.lanexangtourism.interface_callback.ItemsPackageTourCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class PackageTourPostRequest extends AsyncTask<HttpCall ,Integer, String> {

    private static final String UTF_8 = "UTF-8";
    private ArrayList<ItemsModel> arrayList;
    private ItemsPackageTourCallBack callBackService;


    public PackageTourPostRequest(ItemsPackageTourCallBack callBackService) {
        this.callBackService = callBackService;
    }


    @Override
    protected String doInBackground(HttpCall... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return uploadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayList = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayList);
        }
    }

    public ArrayList<ItemsModel> onParserContentToModel(String dataJSon) {
        ArrayList<ItemsModel> list = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(dataJSon);
            JSONArray jsonItems = object.optJSONArray("Items");

            if (jsonItems != null && jsonItems.length() != 0) {

                for (int i = 0; i < jsonItems.length(); i++) {
                    JSONObject jsItems = jsonItems.getJSONObject(i);
                    ItemsModel items = new ItemsModel();
                    ArrayList<SubCategoryModel> subCategoryArrayList = new ArrayList<>();
                    MenuItemModel menuItem = new MenuItemModel();
                    SubCategoryModel subCategory = new SubCategoryModel();

                    JSONArray jsonCover = object.optJSONArray("Items").getJSONObject(i).getJSONArray("CoverItems");
                    JSONArray jsonDetails = object.optJSONArray("Items").getJSONObject(i).getJSONArray("TravelDetails");
                    JSONArray jsonPeriod = object.optJSONArray("Items").getJSONObject(i).getJSONArray("TravelPeriod");
                    JSONArray jsonCondition = object.optJSONArray("Items").getJSONObject(i).getJSONArray("BookingCondition");

                    if (jsItems != null) {
                        items.setItemsId(Integer.parseInt(jsItems.getString("items_id")));
                        items.setTopicTH(jsItems.getString("itmes_topicThai"));
                        items.setTopicEN(jsItems.getString("itmes_topicEnglish"));
                        items.setTopicLO(jsItems.getString("itmes_topicLaos"));
                        items.setTopicZH(jsItems.getString("itmes_topicChinese"));
                        items.setLatitude(jsItems.getString("items_latitude"));
                        items.setLongitude(jsItems.getString("items_longitude"));
                        items.setContactTH(jsItems.getString("items_contactThai"));
                        items.setContactEN(jsItems.getString("items_contactEnglish"));
                        items.setContactLO(jsItems.getString("items_contactLaos"));
                        items.setContactZH(jsItems.getString("items_contactChinese"));
                        items.setPhone(jsItems.getString("items_phone"));
                        items.setEmail(jsItems.getString("items_email"));
                        items.setLine(jsItems.getString("items_line"));
                        items.setFacebookPage(jsItems.getString("items_facebookPage"));
                        items.setArURL(jsItems.getString("item_ar_url"));
//                    items.setPrice(jsItems.getDouble("items_price"));
                        items.setIsActive(jsItems.getInt("items_isActive"));
                        items.setIsPublish(jsItems.getInt("items_isPublish"));
                        items.setHighlightsTH(jsItems.getString("items_highlightsThai"));
                        items.setHighlightsEN(jsItems.getString("items_highlightsEnglish"));
                        items.setHighlightsLO(jsItems.getString("items_highlightsLaos"));
                        items.setHighlightsZH(jsItems.getString("items_highlightsChinese"));
                        items.setLocationInformationTH(jsItems.getString("items_locationInformationThai"));
                        items.setLocationInformationEN(jsItems.getString("items_locationInformationEnglish"));
                        items.setLocationInformationLO(jsItems.getString("items_locationInformationLaos"));
                        items.setLocationInformationZH(jsItems.getString("items_locationInformationChinese"));
//                    items.setTimeOpen(jsItems.getString("items_timeOpen"));
//                    items.setTimeClose(jsItems.getString("items_timeClose"));
                        menuItem.setMenuItemId(jsItems.getInt("menuItem_id"));
                        menuItem.setMenuItemTH(jsItems.getString("menuItem_thai"));
                        menuItem.setMenuItemEN(jsItems.getString("menuItem_english"));
                        menuItem.setMenuItemZH(jsItems.getString("menuItem_chinese"));
                        menuItem.setMenuItemLO(jsItems.getString("menuItem_laos"));
                        items.setMenuItemModel(menuItem);
                        subCategory.setCategoryId(Integer.parseInt(jsItems.getString("subcategory_id")));
                        subCategory.setCategoryTH(jsItems.getString("subcategory_thai"));
                        subCategory.setCategoryEN(jsItems.getString("subcategory_english"));
                        subCategory.setCategoryLO(jsItems.getString("subcategory_laos"));
                        subCategory.setCategoryZH(jsItems.getString("subcategory_chinese"));
                        subCategoryArrayList.add(subCategory);
                        items.setSubCategoryModelArrayList(subCategoryArrayList);

                        ArrayList<CoverItemsModel> coverItemsModelsArrayList = new ArrayList<>();
                        for (int j = 0; j < jsonCover.length(); j++) {
                            JSONObject jsCover = jsonCover.getJSONObject(j);
                            CoverItemsModel coverItemsModels = new CoverItemsModel();

                            if (jsCover != null) {
                                coverItemsModels.setCoverId(jsCover.getInt("coverItem_id"));
                                coverItemsModels.setCoverPaths(jsCover.getString("coverItem_paths"));
                                if (jsCover.getString("coverItem_url") != null && jsCover.getString("coverItem_url") != "" && !jsCover.getString("coverItem_url").equals("")) {
                                    coverItemsModels.setCoverURL(jsCover.getString("coverItem_url"));
                                } else {
                                    coverItemsModels.setCoverURL(null);
                                }
                                coverItemsModelsArrayList.add(coverItemsModels);
                            }
                        }
                        items.setCoverItemsModelArrayList(coverItemsModelsArrayList);

                        ArrayList<TravelDetailsModel> travelDetailsModelArrayList = new ArrayList<>();
                        ArrayList<HotelAccommodationModel> hotelAccommodationModelsArrayList = new ArrayList<>();
                        ArrayList<FoodModel> foodModelArrayList = new ArrayList<>();
                        for (int j = 0; j < jsonDetails.length(); j++) {
                            JSONObject jsDetails = jsonDetails.getJSONObject(j);
                            TravelDetailsModel travelDetailsModel = new TravelDetailsModel();
                            HotelAccommodationModel hotelModel = new HotelAccommodationModel();
                            FoodModel foodModel = new FoodModel();

                            if (jsDetails != null) {
                                travelDetailsModel.setDetailsId(jsDetails.getInt("travelDetails_id"));
                                travelDetailsModel.setDetailsDateTH(jsDetails.getString("travelDetails_dateThai"));
                                travelDetailsModel.setDetailsDateEN(jsDetails.getString("travelDetails_dateEnglish"));
                                travelDetailsModel.setDetailsDateLO(jsDetails.getString("travelDetails_dateLaos"));
                                travelDetailsModel.setDetailsDateZH(jsDetails.getString("travelDetails_dateChinese"));
                                travelDetailsModel.setDetailsTextTH(jsDetails.getString("travelDetails_textThai"));
                                travelDetailsModel.setDetailsTextEN(jsDetails.getString("travelDetails_textEnglish"));
                                travelDetailsModel.setDetailsTextLO(jsDetails.getString("travelDetails_textLaos"));
                                travelDetailsModel.setDetailsTextZH(jsDetails.getString("travelDetails_textChinese"));

                                hotelModel.setAccommodationTH(jsDetails.getString("travelDetails_hotel_nameThai"));
                                hotelModel.setAccommodationEN(jsDetails.getString("travelDetails_hotel_nameEnglish"));
                                hotelModel.setAccommodationLO(jsDetails.getString("travelDetails_hotel_nameLaos"));
                                hotelModel.setAccommodationZH(jsDetails.getString("travelDetails_hotel_nameChinese"));

                                if (jsDetails.getString("travelDetails_food_breakfast").equalsIgnoreCase("1")) {
                                    foodModel.setFoodBreakFast(true);
                                } else {
                                    foodModel.setFoodBreakFast(false);
                                }

                                if (jsDetails.getString("travelDetails_food_lunch").equalsIgnoreCase("1")) {
                                    foodModel.setFoodLunch(true);
                                } else {
                                    foodModel.setFoodLunch(false);
                                }

                                if (jsDetails.getString("travelDetails_food_dinner").equalsIgnoreCase("1")) {
                                    foodModel.setFoodDinner(true);
                                } else {
                                    foodModel.setFoodDinner(false);
                                }

                                travelDetailsModelArrayList.add(travelDetailsModel);
                                hotelAccommodationModelsArrayList.add(hotelModel);
                                foodModelArrayList.add(foodModel);
                            }

                        }
                        items.setTravelDetailsModelArrayList(travelDetailsModelArrayList);
                        items.setHotelAccommodationModelArrayList(hotelAccommodationModelsArrayList);
                        items.setFoodModelArrayList(foodModelArrayList);


                        ArrayList<BookingConditionModel> bookingConditionModelArrayList = new ArrayList<>();
                        for (int j = 0; j < jsonCondition.length(); j++) {
                            JSONObject jsCondition = jsonCondition.getJSONObject(j);
                            BookingConditionModel conditionModel = new BookingConditionModel();

                            if (jsCondition != null) {
                                conditionModel.setConditionTopicTH(jsCondition.getString("bookingConditionCategory_topicThai"));
                                conditionModel.setConditionTopicEN(jsCondition.getString("bookingConditionCategory_topicEnglish"));
                                conditionModel.setConditionTopicLO(jsCondition.getString("bookingConditionCategory_topicChinese"));
                                conditionModel.setConditionTopicZH(jsCondition.getString("bookingConditionCategory_topicLaos"));
                                conditionModel.setConditionDetailTH(jsCondition.getString("bookingConditioncol_detailThai"));
                                conditionModel.setConditionDetailEN(jsCondition.getString("bookingConditioncol_detailEnglish"));
                                conditionModel.setConditionDetailLO(jsCondition.getString("bookingConditioncol_detailChinese"));
                                conditionModel.setConditionDetailZH(jsCondition.getString("bookingConditioncol_detailLaos"));

                                bookingConditionModelArrayList.add(conditionModel);
                            }
                        }
                        items.setBookingConditionModelArrayList(bookingConditionModelArrayList);

                        ArrayList<TravelPeriodModel> travelPeriodModelArrayList = new ArrayList<>();
                        for (int j = 0; j < jsonPeriod.length(); j++) {
                            JSONObject jsPeriod = jsonPeriod.getJSONObject(j);
                            TravelPeriodModel periodModel = new TravelPeriodModel();
                            if (jsPeriod != null) {
                                periodModel.setPeriodId(jsPeriod.getInt("travelPeriod_id"));
                                periodModel.setAmount(jsPeriod.getInt("travelPeriod_amount"));
                                periodModel.setPeriodStart(jsPeriod.getString("travelPeriod_time_period_start"));
                                periodModel.setPeriodEnd(jsPeriod.getString("travelPeriod_time_period_end"));
                                periodModel.setAdultPrice(jsPeriod.getInt("travelPeriod_adult_price"));
                                periodModel.setAdultSpecialPrice(jsPeriod.getInt("travelPeriod_adult_special_price"));
                                periodModel.setChildPrice(jsPeriod.getInt("travelPeriod_child_price"));
                                periodModel.setChildSpecialPrice(jsPeriod.getInt("travelPeriod_child_special_price"));

                                travelPeriodModelArrayList.add(periodModel);
                            }
                        }
                        items.setTravelPeriodModelArrayList(travelPeriodModelArrayList);

                        JSONArray jsonBusiness = object.optJSONArray("Items").getJSONObject(i).getJSONArray("Business");
                        for (int j = 0; j < jsonBusiness.length(); j++) {
                            JSONObject jsBusiness = jsonBusiness.getJSONObject(j);
                            if (jsBusiness != null) {
                                BusinessModel business = new BusinessModel();
                                business.setBusinessId(jsBusiness.getInt("business_id"));
                                business.setNameTH(jsBusiness.getString("business_nameThai"));
                                business.setNameEN(jsBusiness.getString("business_nameEnglish"));
                                business.setNameLO(jsBusiness.getString("business_nameLaos"));
                                business.setNameZH(jsBusiness.getString("business_nameChinese"));
                                business.setLatitude((float) jsBusiness.getDouble("business_latitude"));
                                business.setLongitude((float) jsBusiness.getDouble("business_longitude"));
                                business.setPresentAddress(jsBusiness.getString("business_presentAddress"));
                                business.setLicensePaths(jsBusiness.getString("business_license_paths"));
                                business.setOperatorNumber(jsBusiness.getString("business_operator_number"));
                                business.setPhone(jsBusiness.getString("business_phone"));
                                business.setWeb(jsBusiness.getString("business_www"));
                                business.setFacebook(jsBusiness.getString("business_facebook"));
                                business.setLine(jsBusiness.getString("business_line"));
                                items.setBusinessModel(business);
                            }
                        }
                        try {
                            JSONArray jsonScopeArea = object.optJSONArray("Items").getJSONObject(i).getJSONArray("ScopeArea");
                            ArrayList<ProvincesModel> provincesModelArrayList = new ArrayList<>();
                            for (int k = 0; k < jsonScopeArea.length(); k++) {
                                JSONObject jsScopeArea = jsonScopeArea.getJSONObject(k);
                                ProvincesModel pro = new ProvincesModel();
                                if (jsScopeArea != null) {
                                    pro.setProvincesId(jsScopeArea.getInt("provinces_id"));
                                    pro.setProvincesCode(jsScopeArea.getInt("provinces_code"));
                                    pro.setProvincesTH(jsScopeArea.getString("provinces_thai"));
                                    pro.setProvincesEN(jsScopeArea.getString("provinces_english"));
                                    pro.setProvincesLO(jsScopeArea.getString("provinces_laos"));
                                    pro.setProvincesZH(jsScopeArea.getString("provinces_chinese"));
                                    provincesModelArrayList.add(pro);
                                }
                            }
                            items.setProvincesModelArrayList(provincesModelArrayList);
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }

                        list.add(items);
                    }
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    private String uploadContent(HttpCall params) throws IOException{
        HttpURLConnection urlConnection = null;
        HttpCall httpCall = params;
        StringBuilder response = new StringBuilder();
        try{
            String dataParams = getDataObject(httpCall.getParams(), httpCall.getMethodType());
            URL url = new URL(httpCall.getMethodType() == HttpCall.GET ? httpCall.getUrl() + dataParams : httpCall.getUrl());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(httpCall.getMethodType() == HttpCall.GET ? "GET":"POST");
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            if(httpCall.getParams() != null && httpCall.getMethodType() == HttpCall.POST){
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, UTF_8));
                writer.append(dataParams);
                writer.flush();
                writer.close();
                os.close();
            }
            int responseCode = urlConnection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK){
                String line ;
                BufferedReader br = new BufferedReader( new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null){
                    response.append(line);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            urlConnection.disconnect();
        }
        return response.toString();
    }

    private String getDataObject(HashMap<String, String> params, int methodType) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean isFirst = true;
        for(Map.Entry<String,String> entry : params.entrySet()){
            if (isFirst){
                isFirst = false;
                if(methodType == HttpCall.GET){
                    result.append("?");
                }
            }else{
                result.append("&");
            }
            result.append(URLEncoder.encode(entry.getKey(), UTF_8));
            result.append("=");
            result.append(URLEncoder.encode(String.valueOf(entry.getValue()), UTF_8));
        }
        return result.toString();
    }


}
