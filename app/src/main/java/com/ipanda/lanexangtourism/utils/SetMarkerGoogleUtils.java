package com.ipanda.lanexangtourism.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.ipanda.lanexangtourism.Adapter.InfoWindowAdapter;
import com.ipanda.lanexangtourism.Adapter.ItemsMapsRecyclerViewAdapter;
import com.ipanda.lanexangtourism.Interface_click.ItemsMapsClickListener;
import com.ipanda.lanexangtourism.Model.ItemsModel;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.items_view.ViewTourismActivity;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class SetMarkerGoogleUtils implements ItemsMapsClickListener {

    private Context context;
    private GoogleMap googleMap;
    private Marker marker;
    private ItemsModel itemsModel;
    private ArrayList<ItemsModel> itemsList;
    private ArrayList<Marker> markersList;
    private String language;
    private RecyclerView recycler_maps_items_sliding;
    private CheckBox checkOpenClose;
    boolean isCategoryTwo = false, isCategoryThree = false, isCategoryFour = false, isCategoryFive = false, isCategorySix;
    private ArrayList<ItemsModel> newItemsList;
    private ItemsMapsRecyclerViewAdapter mapsAdapter;
    private LinearLayoutManager layoutManager;
    private SearchView edt_title_search_filter;
    private ImageView btnSearchFilterIcon;
    private String filterOpenClosed = "";

    public SetMarkerGoogleUtils() {

    }

    public SetMarkerGoogleUtils(Context context, GoogleMap googleMap, Marker marker, ItemsModel itemsModel, String language) {
        this.context = context;
        this.googleMap = googleMap;
        this.marker = marker;
        this.itemsModel = itemsModel;
        this.language = language;
    }

    public SetMarkerGoogleUtils(Context context, GoogleMap googleMap, Marker marker, ArrayList<ItemsModel> itemsList, String language, RecyclerView recycler_maps_items_sliding , CheckBox checkOpenClose,SearchView edt_title_search_filter,ImageView btnSearchFilterIcon) {
        this.context = context;
        this.googleMap = googleMap;
        this.marker = marker;
        this.itemsList = itemsList;
        this.language = language;
        this.markersList = new ArrayList<>();
        this.recycler_maps_items_sliding = recycler_maps_items_sliding;
        this.checkOpenClose = checkOpenClose;
        this.edt_title_search_filter = edt_title_search_filter;
        this.btnSearchFilterIcon = btnSearchFilterIcon;
    }

    public void setShowIconMarkerMaps(){
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
        String price  = null;
        for (ItemsModel item : itemsList) {
            if (item.getLatitude() != null && item.getLongitude() != null) {
                float Latitude = Float.parseFloat(item.getLatitude());
                float Longitude = Float.parseFloat(item.getLongitude());
                LatLng location = new LatLng(Latitude, Longitude);
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 6), 2000, null);
                int resId = 0;
                if (item.getMenuItemModel().getMenuItemId() == 3) {
                    if (item.getDeliciousGuaranteeModelArrayList() != null && item.getDeliciousGuaranteeModelArrayList().size() != 0) {
                        resId = getResourceByFilename(context, "ic_mark_garantee");
                    } else {
                        resId = getResourceByFilename(context, "ic_menu" + item.getMenuItemModel().getMenuItemId());
                    }
                } else {
                    resId = getResourceByFilename(context, "ic_menu" + item.getMenuItemModel().getMenuItemId());
                }
                Bitmap imageBitmap = BitmapFactory.decodeResource(context.getResources(), resId);
                Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 140, 155, false);
                MarkerOptions options = new MarkerOptions();
                options.position(location);

                if (item.getMenuItemModel().getMenuItemId() == 5) {
                    price = decimalFormat.format(item.getPrice());
                    switch (language) {
                        case "th":
                            options.title("THB " + price + ".-" + item.getTopicTH());
                            break;
                        case "en":
                            options.title("THB " + price + ".-" + item.getTopicEN());
                            break;
                        case "lo":
                            options.title("THB " + price + ".- " + item.getTopicLO());
                            break;
                        case "zh":
                            options.title("THB " + price + ".-" + item.getTopicZH());
                            break;
                    }
                } else {
                    switch (language) {
                        case "th":
                            options.title(item.getTopicTH());
                            break;
                        case "en":
                            options.title(item.getTopicEN());
                            break;
                        case "lo":
                            options.title(item.getTopicLO());
                            break;
                        case "zh":
                            options.title(item.getTopicZH());
                            break;
                    }
                }

                options.snippet(String.valueOf(item.getMenuItemModel().getMenuItemId()));
                options.zIndex(1.0f);
                options.icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));
                options.draggable(false);

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.info_title, null);
                googleMap.setInfoWindowAdapter(new InfoWindowAdapter(context, view, item));
                googleMap.setOnInfoWindowClickListener(OnInfoWindowClickListener);
                googleMap.setOnMarkerClickListener(onMarkerClickedListener);
                marker = googleMap.addMarker(options);
                marker.setVisible(false);
                markersList.add(marker);
            }
        }

    }

    public void setShowIconMarkerMapsSingle(){
        try {
            float Latitude = Float.parseFloat(itemsModel.getLatitude());
            float Longitude = Float.parseFloat(itemsModel.getLongitude());
            LatLng location = new LatLng(Latitude, Longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 12), 2000, null);
            int resId = getResourceByFilename(context, "ic_menu" + itemsModel.getMenuItemModel().getMenuItemId());
            Bitmap imageBitmap = BitmapFactory.decodeResource(context.getResources(), resId);
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 110, 145, false);
            MarkerOptions options = new MarkerOptions();
            options.position(location);
            switch (language) {
                case "th":
                    options.title(itemsModel.getTopicTH());
                    break;
                case "en":
                    options.title(itemsModel.getTopicEN());
                    break;
                case "lo":
                    options.title(itemsModel.getTopicLO());
                    break;
                case "zh":
                    options.title(itemsModel.getTopicZH());
                    break;
            }
            options.snippet(String.valueOf(itemsModel.getMenuItemModel().getMenuItemId()));
            options.icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));
            options.draggable(true);

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.info_title,null);
            googleMap.setInfoWindowAdapter(new InfoWindowAdapter(context,view,itemsModel));
//            googleMap.setOnInfoWindowClickListener(OnInfoWindowClickListener);
//            googleMap.setOnMarkerClickListener(onMarkerClickedListener);
            googleMap.getUiSettings().setMapToolbarEnabled(true);
            marker = googleMap.addMarker(options);
            marker.setVisible(true);
        }catch (Exception e){
            Log.e("error",e.getMessage().toString());
        }finally {

        }


    }

    public void setShowIconMarkerMaps(String getCategory){
        for (Marker marker : markersList) {
            if (marker.getSnippet().equals(getCategory)){
                marker.setVisible(true);
//                marker.showInfoWindow();
                marker.showInfoWindow();
            }
        }

        switch (getCategory){
            case "2":
                isCategoryTwo = true;
                break;
            case "3":
                isCategoryThree = true;
                break;
            case "4":
                isCategoryFour = true;
                break;
            case "5":
                isCategoryFive = true;
                break;
            case "6":
                isCategorySix = true;
                break;
        }

        setContentItemsViewList();
    }

    public void setHideIconMarkerMaps(String getCategory){
        for (Marker marker : markersList) {
            if (marker.getSnippet().equals(getCategory)){
                marker.setVisible(false);
            }
        }

        switch (getCategory){
            case "2":
                isCategoryTwo = false;
                break;
            case "3":
                isCategoryThree = false;
                break;
            case "4":
                isCategoryFour = false;
                break;
            case "5":
                isCategoryFive = false;
                break;
            case "6":
                isCategorySix = false;
                break;
        }

        setContentItemsViewList();
    }

    private void setFilterOpenAndClosed(){
        checkOpenClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkOpenClose.isChecked()) {
                    filterOpenClosed = "open";
                    checkOpenClose.setChecked(true);
                    edt_title_search_filter.setQuery(filterOpenClosed,true);
                    checkOpenClose.setTextColor(context.getResources().getColor(R.color.white));
                }else {
                    filterOpenClosed = "";
                    checkOpenClose.setChecked(false);
                    edt_title_search_filter.setQuery(filterOpenClosed,true);
                    checkOpenClose.setTextColor(context.getResources().getColor(R.color.colorTextFilter));

                }
            }
        });
    }

    private void moveOnZoomToLocation(LatLng location) {
        if (location != null) {
            double latitude = location.latitude;
            double longitude = location.longitude;
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16), 3000, null);
        }
    }

    private void moveOnZoomOutToLocation(LatLng location){
        if (location != null) {
            double latitude = location.latitude;
            double longitude = location.longitude;
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10), 3000, null);
        }
    }

    public void moveOnZoomToLocationByItems(LatLng location, ItemsModel items) {
        if (location != null) {
            double latitude = location.latitude;
            double longitude = location.longitude;
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16), 3000, null);

            for (Marker marker : markersList) {
                switch (language){
                    case "th":
                        if (marker.getTitle().equalsIgnoreCase(items.getTopicTH())){
                            marker.setVisible(true);
                            marker.showInfoWindow();
                        }
                        break;
                    case "en":
                        if (marker.getTitle().equalsIgnoreCase(items.getTopicEN())){
                            marker.setVisible(true);
                            marker.showInfoWindow();
                        }
                        break;
                    case "lo":
                        if (marker.getTitle().equalsIgnoreCase(items.getTopicLO())){
                            marker.setVisible(true);
                            marker.showInfoWindow();
                        }
                        break;
                    case "zh":
                        if (marker.getTitle().equalsIgnoreCase(items.getTopicZH())){
                            marker.setVisible(true);
                            marker.showInfoWindow();
                        }
                        break;
                }

            }
        }
    }

    private void moveOnZoomToLocationByItemsAs(LatLng location, ItemsModel items) {
        if (location != null) {
            double latitude = location.latitude;
            double longitude = location.longitude;
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16), 3000, null);

            for (Marker marker : markersList) {
                switch (language){
                    case "th":
                        if (marker.getTitle().equalsIgnoreCase(items.getTopicTH())){
                            marker.setVisible(true);
                            marker.showInfoWindow();
                        }
                        break;
                    case "en":
                        if (marker.getTitle().equalsIgnoreCase(items.getTopicEN())){
                            marker.setVisible(true);
                            marker.showInfoWindow();
                        }
                        break;
                    case "lo":
                        if (marker.getTitle().equalsIgnoreCase(items.getTopicLO())){
                            marker.setVisible(true);
                            marker.showInfoWindow();
                        }
                        break;
                    case "zh":
                        if (marker.getTitle().equalsIgnoreCase(items.getTopicZH())){
                            marker.setVisible(true);
                            marker.showInfoWindow();
                        }
                        break;
                }

            }
        }
    }



    public static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }

    private void setContentItemsViewList() {
        newItemsList = new ArrayList<>();
        for (ItemsModel items: itemsList) {
            if (isCategoryTwo){
                if (items.getMenuItemModel().getMenuItemId() == 2){
                    newItemsList.add(items);
                }
            }else {
                if (items.getMenuItemModel().getMenuItemId() == 2){
                    newItemsList.remove(items);
                }
            }
            if (isCategoryThree){
                if (items.getMenuItemModel().getMenuItemId() == 3){
                    newItemsList.add(items);
                }
            }else {
                if (items.getMenuItemModel().getMenuItemId() == 3){
                    newItemsList.remove(items);
                }
            }
            if (isCategoryFour){
                if (items.getMenuItemModel().getMenuItemId() == 4){
                    newItemsList.add(items);
                }
            }else {
                if (items.getMenuItemModel().getMenuItemId() == 4){
                    newItemsList.remove(items);
                }
            }
            if (isCategoryFive){
                if (items.getMenuItemModel().getMenuItemId() == 5){
                    newItemsList.add(items);
                }
            }else {
                if (items.getMenuItemModel().getMenuItemId() == 5){
                    newItemsList.remove(items);
                }

            }
            if (isCategorySix){
                if (items.getMenuItemModel().getMenuItemId() == 6){
                    newItemsList.add(items);
                }
            }else {
                if (items.getMenuItemModel().getMenuItemId() == 6){
                    newItemsList.remove(items);
                }
            }
        }

        checkOpenClose.setVisibility(View.GONE);
        recycler_maps_items_sliding.setVisibility(View.GONE);
        mapsAdapter = new ItemsMapsRecyclerViewAdapter(context,newItemsList,language,this);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        recycler_maps_items_sliding.setHasFixedSize(true);
        recycler_maps_items_sliding.setLayoutManager(layoutManager);
        recycler_maps_items_sliding.setAdapter(mapsAdapter);

        edt_title_search_filter.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edt_title_search_filter.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mapsAdapter != null) {
                    mapsAdapter.getFilter().filter(query);
                    recycler_maps_items_sliding.setVisibility(View.VISIBLE);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mapsAdapter != null) {
                    mapsAdapter.getFilter().filter(newText);
                    recycler_maps_items_sliding.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
    }

    private GoogleMap.OnInfoWindowClickListener OnInfoWindowClickListener = new GoogleMap.OnInfoWindowClickListener() {
        @Override
        public void onInfoWindowClick(Marker marker) {
            float Latitude;
            float Longitude;
            LatLng location = null;
            if (marker.isInfoWindowShown()){
                marker.hideInfoWindow();
//                Toast.makeText(context, "Hide use", Toast.LENGTH_SHORT).show();
                if (checkOpenClose != null) {
                    checkOpenClose.setVisibility(View.GONE);
                    recycler_maps_items_sliding.setVisibility(View.GONE);
                    if (marker.getTitle() != null) {
                        for (int i = 0; i < newItemsList.size(); i++) {
                            switch (language) {
                                case "th":
                                    if (marker.getTitle().equals(newItemsList.get(i).getTopicTH())) {
                                        Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                        Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                        location = new LatLng(Latitude, Longitude);
                                    }
                                    break;
                                case "en":
                                    if (marker.getTitle().equals(newItemsList.get(i).getTopicEN())) {
                                        Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                        Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                        location = new LatLng(Latitude, Longitude);
                                    }
                                    break;
                                case "lo":
                                    if (marker.getTitle().equals(newItemsList.get(i).getTopicLO())) {
                                        Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                        Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                        location = new LatLng(Latitude, Longitude);
                                    }
                                    break;
                                case "zh":
                                    if (marker.getTitle().equals(newItemsList.get(i).getTopicZH())) {
                                        Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                        Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                        location = new LatLng(Latitude, Longitude);
                                    }
                                    break;
                            }
                        }
                    }
                }

                moveOnZoomOutToLocation(location);
            }else {
                marker.showInfoWindow();
            }
        }
    };

    private GoogleMap.OnMarkerClickListener onMarkerClickedListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            float Latitude;
            float Longitude;
            LatLng location = null;
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            DecimalFormat decimalFormat = new DecimalFormat("#,###", symbols);
            String price  = null;
            if (marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
            } else {
                marker.showInfoWindow();
//                Toast.makeText(context, "Show use", Toast.LENGTH_SHORT).show();
                if (checkOpenClose != null) {
                    setFilterOpenAndClosed();
                    checkOpenClose.setVisibility(View.VISIBLE);
                    recycler_maps_items_sliding.setVisibility(View.VISIBLE);
                    if (marker.getTitle() != null) {
                        for (int i = 0; i < newItemsList.size(); i++) {
                            switch (language) {
                                case "th":
                                    if (marker.getSnippet() != null) {
                                        if (marker.getSnippet().equals("5")) {
                                            String string = marker.getTitle();
                                            String[] parts = string.split(".-");
                                            String part = parts[1];
                                            if (part.equals(newItemsList.get(i).getTopicTH())) {
                                                layoutManager.scrollToPositionWithOffset(i, 0);
                                                Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                                Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                                location = new LatLng(Latitude, Longitude);
                                            }
                                        } else {
                                            if (marker.getTitle().equals(newItemsList.get(i).getTopicTH())) {
                                                layoutManager.scrollToPositionWithOffset(i, 0);
                                                Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                                Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                                location = new LatLng(Latitude, Longitude);
                                            }
                                        }
                                    }
                                    break;
                                case "en":
                                    if (marker.getSnippet() != null) {
                                        if (marker.getSnippet().equals("5")) {
                                            String string = marker.getTitle();
                                            String[] parts = string.split(".-");
                                            String part = parts[1];
                                            if (part.equals(newItemsList.get(i).getTopicEN())) {
                                                layoutManager.scrollToPositionWithOffset(i, 0);
                                                Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                                Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                                location = new LatLng(Latitude, Longitude);
                                            }
                                        } else {
                                            if (marker.getTitle().equals(newItemsList.get(i).getTopicEN())) {
                                                layoutManager.scrollToPositionWithOffset(i, 0);
                                                Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                                Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                                location = new LatLng(Latitude, Longitude);
                                            }
                                        }
                                    }
                                    break;
                                case "lo":
                                    if (marker.getSnippet() != null) {
                                        if (marker.getSnippet().equals("5")) {
                                            String string = marker.getTitle();
                                            String[] parts = string.split(".-");
                                            String part = parts[1];
                                            if (part.equals(newItemsList.get(i).getTopicLO())) {
                                                layoutManager.scrollToPositionWithOffset(i, 0);
                                                Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                                Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                                location = new LatLng(Latitude, Longitude);
                                            }
                                        } else {
                                            if (marker.getTitle().equals(newItemsList.get(i).getTopicLO())) {
                                                layoutManager.scrollToPositionWithOffset(i, 0);
                                                Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                                Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                                location = new LatLng(Latitude, Longitude);
                                            }
                                        }
                                    }
                                    break;
                                case "zh":
                                    if (marker.getSnippet() != null) {
                                        if (marker.getSnippet().equals("5")) {
                                            String string = marker.getTitle();
                                            String[] parts = string.split(".-");
                                            String part = parts[1];
                                            if (part.equals(newItemsList.get(i).getTopicZH())) {
                                                layoutManager.scrollToPositionWithOffset(i, 0);
                                                Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                                Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                                location = new LatLng(Latitude, Longitude);
                                            }
                                        } else {
                                            if (marker.getTitle().equals(newItemsList.get(i).getTopicZH())) {
                                                layoutManager.scrollToPositionWithOffset(i, 0);
                                                Latitude = Float.parseFloat(newItemsList.get(i).getLatitude());
                                                Longitude = Float.parseFloat(newItemsList.get(i).getLongitude());
                                                location = new LatLng(Latitude, Longitude);
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    } else {
                        marker.setTitle("i here.");
                    }
                }

            }

            moveOnZoomToLocation(location);
            return true;
        }
    };

    @Override
    public void clickedItemsMaps(ItemsModel items) {
        Intent intent = new Intent(context, ViewTourismActivity.class);
        intent.putExtra("ITEMS_MODEL", items);
        intent.putExtra("CATEGORY", items.getMenuItemModel().getMenuItemId());
        context.startActivity(intent);

    }

    @Override
    public void clickedItemsMove(ItemsModel items) {
        float Latitude = Float.parseFloat(items.getLatitude());
        float Longitude = Float.parseFloat(items.getLongitude());
        LatLng location = new LatLng(Latitude, Longitude);
//        moveOnZoomToLocation(location);
        moveOnZoomToLocationByItemsAs(location,items);
    }
}
